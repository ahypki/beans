package net.beanscode.web.view.dataset;

import static org.rendersnake.HtmlAttributesFactory.class_;

import java.io.IOException;
import java.util.List;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.datasets.Table;
import net.beanscode.cli.datasets.TableGateway;
import net.beanscode.pojo.dataset.Dataset;
import net.beanscode.web.view.ui.BetaTableComponent;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.search.SearchResults;
import net.hypki.libs5.utils.LibsLogger;

import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

public class DatasetTablesBetaTableComponent extends BetaTableComponent {
	
	@Expose
	private UUID datasetId = null;
	
	private Dataset dataset = null;
	
	private SearchResults<Table> tables = null;

	public DatasetTablesBetaTableComponent() {
		
	}
	
	public DatasetTablesBetaTableComponent(Component parent, UUID datasetId) {
		super(parent);
		setDatasetId(datasetId);
	}
	
	@Override
	protected String getSettingsPrefix() {
		return "tables-table-";
	}
	
	@Override
	protected boolean isExapandable() throws IOException {
		return true;
	}
	
	@Override
	protected String getRowCss(int row) {
		if (row % getPerPage() < getTables().getObjects().size()) {
			Table table = getTables().getObjects().get(row % getPerPage());
			return super.getRowCss(row) + " table-" + table.getId();
		} else
			return "";
	}
	
	@Override
	protected void renderExpandable(int row, HtmlCanvas html) throws IOException {
		Table table = getTables().getObjects().get(row % getPerPage());
		
		html
			.div(class_("table-" + table.getId()))
				.render(new TableBetaPanel(this, getTables().getObjects().get(row % getPerPage())))
			._div();
	}
	
	private SearchResults<Table> getTables() {
		if (tables == null) {
			try {
				tables = TableGateway.getTables(getSessionBean(), 
						getDatasetId().getId(), 
						getFilter(), 
						getPage() * getPerPage(), 
						getPerPage());
//				ApiCallCommand call = new ApiCallCommand(this)
//					.setApi("api/table/search")
//					.addParam("datasetQuery", getDatasetId().getId())
//					.addParam("tableQuery", getFilter())
//					.addParam("from", getPage() * getPerPage())
//					.addParam("size", getPerPage())
//					.run();
//				
//				tables = new SearchResults<Table>(call.getResponse().getAsList("tables", Table.class), 
//						call.getResponse().getAsLong("maxHits"));
			} catch (CLIException | IOException e) {
				LibsLogger.error(DatasetTablesBetaTableComponent.class, "Cannot search for Tables", e);
			}
		}
		return tables;
	}

	@Override
	protected void renderTitle(HtmlCanvas html) throws IOException {
		html
			.write("Tables for dataset " + getDataset().getName());
	}

	@Override
	protected boolean isFilterVisible() throws IOException {
		return true;
	}

	@Override
	protected boolean isHeaderColumnVisible() throws IOException {
		return true;
	}

	@Override
	protected int getNumberOfColumns() throws IOException {
		return 3;
	}

	@Override
	protected String getColumnName(int col) throws IOException {
		if (col == 0)
			return "Table name";
		else if (col == 1)
			return "Created";
		else if (col == 2)
			return "";
		else
			return "";
	}

	@Override
	protected long getMaxHits() throws IOException {
		return getTables().maxHits();
	}

	@Override
	protected void renderCell(int row, int col, HtmlCanvas html) throws IOException {
		Table table = getTables().getObjects().get(row % getPerPage());
		
		if (col == 0)
			html
				.write(table.getName());
		else if (col == 1)
			html
				.write(table.getCreate().toStringHuman());
		else if (col == 2)
			html
//				.i(class_("fa fa-chevron-left beans-chevron-left")
//						.onClick(new OpList()
//							.add(new OpHide("#" + getId() + " .beans-chevron-left"))
//							.add(new OpShow("#" + getId() + " .beans-chevron-down"))
//							.toStringOnclick()
//							))
//				._i()
				.i(class_("fa fa-chevron-down beans-chevron-down")
//						.add("style", "display: none;")
//						.onClick(new OpList()
//							.add(new OpHide("#" + getId() + " .beans-chevron-down"))
//							.add(new OpShow("#" + getId() + " .beans-chevron-left"))
//							.toStringOnclick()
//							)
							)
				._i()
				;
	}

	@Override
	protected List<Button> getMenu() throws IOException {
		return null;
	}

	public UUID getDatasetId() {
		return datasetId;
	}

	public void setDatasetId(UUID datasetId) {
		this.datasetId = datasetId;
	}

	public Dataset getDataset() {
		if (dataset == null && getDatasetId() != null) {
			try {
				dataset = new ApiCallCommand(this)
							.setApi("api/dataset")
							.addParam("dataset-id", getDatasetId().getId())
							.run()
							.getResponse()
							.getAsObject(Dataset.class);
			} catch (CLIException | IOException e) {
				LibsLogger.error(DatasetTablesBetaTableComponent.class,
						"Cannot get Dataset from database", e);
			}
		}
		return dataset;
	}

	public void setDataset(Dataset dataset) {
		this.dataset = dataset;
	}

	@Override
	protected String getTableCss() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
