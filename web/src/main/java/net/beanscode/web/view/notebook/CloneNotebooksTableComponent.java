package net.beanscode.web.view.notebook;

import java.io.IOException;

import net.beanscode.cli.notebooks.Notebook;
import net.beanscode.web.view.ui.BetaButtonGroup;
import net.beanscode.web.view.ui.modals.OpModalQuestion;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.utils.url.Params;

import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

public class CloneNotebooksTableComponent extends NotebooksTableComponent {
	
	@Expose
	private String parentComponentId = null;
	
	@Expose
	private String sourceNotebookId = null;

	public CloneNotebooksTableComponent(Component parent,
			String parentComponentId, String sourceNotebookId) {
		super(parent);
		this.parentComponentId = parentComponentId;
		this.sourceNotebookId = sourceNotebookId;
	}
	
	@Override
	public boolean isCompact() {
		return false;
	}
	
	@Override
	protected void renderCell(int row, int col, HtmlCanvas html) throws IOException {
		Notebook notebook = getNotebookSearchResults().getNotebooks().get(row % getPerPage());
		
		if (col == 0) {
			html
				.write(notebook.getName());
		} else if (col == 4) {
			html
				.render(new BetaButtonGroup()
						.add(new Button("Clone")
								.add(new OpModalQuestion("Cloning notebook", 
										"Do you want to clone all entries from notebook "
											+ notebook.getName()
											+ "?", 
										new OpComponentClick(this, 
												"onClone",
												new Params()
													.add("sourceNotebookId", notebook.getId().getId())
													.add("destinationNotebookId", sourceNotebookId)))
									)
							)
						);
		} else
			super.renderCell(row, col, html);
	}
	
	private OpList onClone() {
//		String notebookId = getParams().getString("notebookId");
		String sourceNotebookId = getParams().getString("sourceNotebookId");
		String destinationNotebookId = getParams().getString("destinationNotebookId");
		
		return new OpList()
			.add(new OpComponentClick(NotebookContent.class,
										parentComponentId,
										"onAddEntry",
										new Params()
//											.add("notebookId", notebookId)
											.add("sourceNotebookId", sourceNotebookId)
											.add("destinationNotebookId", destinationNotebookId)
				));
	}
}
