package net.beanscode.model.unittests.dataset;

import static net.hypki.libs5.utils.date.SimpleDate.now;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.DatasetFactory;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.plugins.ColumnDef;
import net.beanscode.model.tests.BeansTestCase;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.weblibs.SearchManager;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.user.User;

import org.junit.Test;

public class DatasetUnitTest extends BeansTestCase {
	
	@Test
	public void validateDatasetNames() {		
		assertTrue(Dataset.isDatasetNameValid("ds1"));
		assertTrue(Dataset.isDatasetNameValid("DS1"));
		assertTrue(Dataset.isDatasetNameValid("ds-1"));
		assertTrue(Dataset.isDatasetNameValid("ds_1"));
		assertTrue(Dataset.isDatasetNameValid("1"));
		assertTrue(Dataset.isDatasetNameValid("ds 1"));
		assertTrue(Dataset.isDatasetNameValid("ds1 "));
		assertTrue(Dataset.isDatasetNameValid(" ds1"));
		assertTrue(Dataset.isDatasetNameValid("ds$1"));
	
		assertTrue(Dataset.isDatasetNameValid("") == false);
	}
	
	@Test
	public void testCreate() throws ValidationException, IOException {
		final User user = testUser();
		
		Dataset ds1 = new Dataset(user.getUserId(), "ds 1");
		ds1.save();

		addToRemove(ds1);

		runAllJobs();
		takeANap(1000);

		Dataset dsFromDB = DatasetFactory.getDataset(ds1.getId());
		assertTrue(dsFromDB != null);
		assertTrue(dsFromDB.getCreationDate().equals(ds1.getCreationDate()));
		assertTrue(dsFromDB.getMeta().containsMeta("fracb") == false);
		assertTrue(dsFromDB.getMeta().size() == 0);
	}

	@Test
	public void testCreateWithMeta() throws ValidationException, IOException {
		final User user = testUser();
		
		Dataset ds2 = new Dataset(user.getUserId(), "ds2", null, now(), "fracb", 0.2, "zini", 0.004);
		ds2.save();

		Table tb1 = new Table(ds2, "tb1");
		tb1.getColumnDefs().add(new ColumnDef("x", "desc", ColumnType.DOUBLE));
		tb1.save();

		addToRemove(tb1);
		addToRemove(ds2);

		runAllJobs();
		takeANap(1000);

		Dataset dsFromDB = DatasetFactory.getDataset(ds2.getId());
		assertTrue(dsFromDB != null);
		assertTrue(ds2.getCreationDate().equals(dsFromDB.getCreationDate()));
		assertTrue(dsFromDB.getMeta().get("fracb").getAsDouble() == 0.2);
		
		ds2.remove();
		runAllJobs(1000);
	}

	@Test
	public void testSearchByMeta() throws ValidationException, IOException {
		SearchManager.searchInstance().clearIndexType(WeblibsConst.KEYSPACE_LOWERCASE, Dataset.COLUMN_FAMILY.toLowerCase());
		
		final User user = testUser();
		final UUID userId = user.getUserId();
		
		Dataset ds1 = new Dataset(userId, "ds1", null, now(), 
				"fracb", 0.2, "zini", 0.004, "n", 40000, "evol", -1);
		ds1.save();

		Table tb1 = new Table(ds1, "tb1");
//		tb1.setColumnDefs(new ColumnDefList());
		tb1.getColumnDefs().add(new ColumnDef("x", "desc", ColumnType.DOUBLE));
		tb1.save();

		addToRemove(tb1);
		addToRemove(ds1);

		runAllJobs(1500);

		Dataset dsFromDB = DatasetFactory.getDataset(ds1.getId());
		assertTrue(dsFromDB != null);
		assertTrue(ds1.getCreationDate().equals(dsFromDB.getCreationDate()));
		assertTrue(dsFromDB.getMeta().get("fracb").getAsDouble() == 0.2);

		List<Dataset> ds = DatasetFactory.searchDatasets(userId, "ds1", 0, 10).getObjects();
		assertTrue(ds.size() == 1, "Size is " + ds.size());
		assertTrue(ds.get(0).getId().equals(ds1.getId()), "Dataset found incorrectly");
		
		ds = DatasetFactory.searchDatasets(userId, "fracb == 0.2", 0, 10).getObjects();
		assertTrue(ds.size() == 1, "Size is " + ds.size());
		assertTrue(ds.get(0).getId().equals(ds1.getId()), "Dataset found incorrectly");
		
		ds = DatasetFactory.searchDatasets(userId, "n == 40000", 0, 10).getObjects();
		assertTrue(ds.size() == 1, "Size is " + ds.size());
		assertTrue(ds.get(0).getId().equals(ds1.getId()), "Dataset found incorrectly");
		
		ds = DatasetFactory.searchDatasets(userId, "fracb == 0.2 AND zini == 0.004", 0, 10).getObjects();
		assertTrue(ds.size() == 1, "Size is " + ds.size());
		assertTrue(ds.get(0).getId().equals(ds1.getId()), "Dataset found incorrectly");
		
		ds = DatasetFactory.searchDatasets(userId, "fracb > 0.1 AND zini < 0.008", 0, 10).getObjects();
		assertTrue(ds.size() == 1, "Size is " + ds.size());
		assertTrue(ds.get(0).getId().equals(ds1.getId()), "Dataset found incorrectly");
		
		ds = DatasetFactory.searchDatasets(userId, "fracb > 0.3 AND zini > 0.008", 0, 10).getObjects();
		assertTrue(ds.size() == 0, "Size is " + ds.size());
		
		ds = DatasetFactory.searchDatasets(userId, "evol < 0", 0, 10).getObjects();
		assertTrue(ds.size() == 1, "Size is " + ds.size());
		
		ds = DatasetFactory.searchDatasets(userId, "evol == -1", 0, 10).getObjects();
		assertTrue(ds.size() == 1, "Size is " + ds.size());
		
		ds1.remove();
		runAllJobs(1000);
	}
	
	@Test
	public void testSearchBySpecialCharacters() throws ValidationException, IOException {
		SearchManager.searchInstance().clearIndexType(WeblibsConst.KEYSPACE_LOWERCASE, Dataset.COLUMN_FAMILY.toLowerCase());
		
		final User user = testUser();
		final UUID userId = user.getUserId();
		
		Dataset ds1 = new Dataset(userId, "ds$");
		ds1.save();
		addToRemove(ds1);

		runAllJobs(1000);

		List<Dataset> ds = DatasetFactory.searchDatasets(userId, "ds", 0, 10).getObjects();
		assertTrue(ds.size() > 0, "No datasets found, size is " + ds.size());
		
		Dataset found = null;
		for (Dataset dataset : ds) {
			if (dataset.getName().equals("ds$")) {
				assertTrue(found == null, "Too many datasets found, size is " + ds.size());
				found = dataset;
			}
		}
		assertTrue(found.getId().equals(ds1.getId()), "Incorrect dataset found");
		
		ds1.remove();
	}
	
	@Test
	public void testSearchByPath() throws ValidationException, IOException {
		SearchManager.searchInstance().clearIndexType(WeblibsConst.KEYSPACE_LOWERCASE, Dataset.COLUMN_FAMILY.toLowerCase());
		
		final User user = testUser();
		final UUID userId = user.getUserId();
		
		Dataset ds1 = new Dataset(userId, "ds1", null, now(), 
				"fracb", 0.2, 
				"zini", 0.004, 
				"n", 40000, 
				"evol", -1,
				"path", "/home/abc/temp/mocca/Survey2/n=4000_4000");
		ds1.save();

		Table tb1 = new Table(ds1, "tb1");
//		tb1.setColumnDefs(new ColumnDefList());
		tb1.getColumnDefs().add(new ColumnDef("x", "desc", ColumnType.DOUBLE));
		tb1.save();

		addToRemove(tb1);
		addToRemove(ds1);

		runAllJobs(1500);

		Dataset dsFromDB = DatasetFactory.getDataset(ds1.getId());
		assertTrue(dsFromDB != null);
		assertTrue(ds1.getCreationDate().equals(dsFromDB.getCreationDate()));
		assertTrue(dsFromDB.getMeta().get("fracb").getAsDouble() == 0.2);

		List<Dataset> ds = DatasetFactory.searchDatasets(userId, "path==/home/abc/temp/mocca/survey2/n=4000_4000", 0, 10).getObjects();
		assertTrue(ds.size() == 1, "Size is " + ds.size());
		assertTrue(ds.get(0).getId().equals(ds1.getId()), "Dataset found incorrectly");
		
		ds1.remove();
		runAllJobs(1000);
	}
}
