package net.beanscode.model.plugins;

import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.NotImplementedException;
import org.apache.pig.data.DataType;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.annotations.Expose;

import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;
import net.sf.oval.constraint.CheckWith;
import net.sf.oval.constraint.CheckWithCheck.SimpleCheck;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

public class ColumnDef implements SimpleCheck, Serializable {
	
	@Expose
	@NotNull
	@NotEmpty
	@CheckWith(value = ColumnDef.class)
	private String name = null;
	
	@Expose
//	@NotNull
//	@NotEmpty
	private String description = null;
	
	@Expose
	@NotNull
	private ColumnType type = null;
	
	public ColumnDef() {

//		@NotEmpt
	}
	
	public ColumnDef(String name, String desc, ColumnType type) {
		setName(name);
		setDescription(desc);
		setType(type);
	}
	
	public ColumnDef(String name) {
		setName(name);
		setType(ColumnType.UNKNOWN);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ColumnDef) {
			ColumnDef colDef = (ColumnDef) obj;
			return colDef.getName().equals(this.getName());
		}
		return false;
	}
	
	@Override
	public String toString() {
		return String.format("%s %s%s", 
				getName() != null ? getName() : "", 
				getType(), 
				getDescription() != null ? " (" + getDescription() + ")" : "");
	}
	
	/**
	 * Valid name contains only letters, digits and underscore.
	 * @param columnName
	 * @return
	 */
	public static boolean isColumnNameValid(String columnName) {
		boolean valid = columnName.matches("^[\\w\\d\\_\\:\\.]+$");
		
		if (!valid)
			LibsLogger.error(ColumnDef.class, "Column name ", columnName, " is invalid");
		
		return valid;
	}
	
	@Override
	public boolean isSatisfied(Object validatedObject, Object value) {
		return isColumnNameValid(value.toString());
	}

	public String getName() {
		return name;
	}

	public ColumnDef setName(String name) {
		this.name = name;
		return this;
	}

	public String getDescription() {
		return description;
	}

	public ColumnDef setDescription(String description) {
		this.description = description;
		return this;
	}

	public ColumnType getType() {
		return type;
	}
	
	public byte getPigType() {
		if (getType() == ColumnType.LONG)
			return DataType.LONG;
		else if (getType() == ColumnType.STRING)
			return DataType.CHARARRAY;
		else if (getType() == ColumnType.DOUBLE)
			return DataType.DOUBLE;
		else if (getType() == ColumnType.INTEGER)
			return DataType.INTEGER;
		else
			throw new NotImplementedException();
	}

	public ColumnDef setType(ColumnType type) {
		this.type = type;
		return this;
	}
	
	public static ColumnType determineType(Object toCheck) {
		if (toCheck == null)
			return null;
		else if (toCheck instanceof String)
			return ColumnType.STRING;
		else if (toCheck instanceof Double)
			return ColumnType.DOUBLE;
		else if (toCheck instanceof Float)
			return ColumnType.FLOAT;
		else if (toCheck instanceof Integer)
			return ColumnType.INTEGER;
		else if (toCheck instanceof Long)
			return ColumnType.LONG;
		else
			throw new RuntimeException("Unknown case for " + toCheck);
	}
	
	public static ColumnDefList parse(String jsonString) {
		try {
			final Set<String> columnNames = new HashSet<>();
			ColumnDefList columnDefs = new ColumnDefList();
			
			JsonArray json = (JsonArray) JsonUtils.parseJson(jsonString);
			for (int i = 0; i < json.size(); i++) {
				JsonElement column = json.get(i);
				if (column.getAsJsonObject().get("name") != null) {
					try {
						String name = column.getAsJsonObject().get("name").getAsString().trim();
						JsonElement desc = column.getAsJsonObject().get("description");
						JsonElement type = column.getAsJsonObject().get("type");
						
						assertTrue(columnNames.contains(name) == false, "Column '" + name + "' duplicated in the header of the file");
						
						columnNames.add(name);
						
						ColumnType columnType = ColumnType.parse(type.getAsString().trim());
						
						StringBuilder description = new StringBuilder();
						if (desc == null) {
							// no description, do nothing
						} else if (desc.isJsonPrimitive())
							description.append(desc.getAsString().trim());
						else {
							for (JsonElement e : desc.getAsJsonArray()) {
								if (!(e instanceof JsonNull))
									description.append(e.getAsString().trim());
							}
						}
						
						columnDefs.add(new ColumnDef(name, 
								description.toString(), 
								columnType));
					} catch (Exception e) {
						LibsLogger.error(ColumnDef.class, "Problem with parsing column: " + column, e);
						throw e;
					}
				}
			}
			return columnDefs;
		} catch (Exception e) {
			LibsLogger.error(ColumnDef.class, "Cannot parse JSON to get columns list", e);
			return null;
		}
		
	}
	
	public static void main(String[] args) {
		LibsLogger.info(ColumnDef.class, isColumnNameValid("org.apache.pig.builtin.bagtotuple_6::binpop1c"));
	}
}
