package net.beanscode.cli.plugin;

import com.google.gson.annotations.Expose;

public class DbProviderPOJO {
	
	@Expose
	private String plugin = null;
	
	@Expose
	private String clazz = null;
	
	public DbProviderPOJO() {
		
	}

	public String getPlugin() {
		return plugin;
	}

	public void setPlugin(String plugin) {
		this.plugin = plugin;
	}

	public String getClazz() {
		return clazz;
	}

	public void setClazz(String clazz) {
		this.clazz = clazz;
	}

}
