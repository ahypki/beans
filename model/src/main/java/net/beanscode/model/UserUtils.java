package net.beanscode.model;

import java.util.ArrayList;
import java.util.List;

import net.beanscode.model.rights.Rights;
import net.hypki.libs5.weblibs.user.User;
import net.hypki.libs5.weblibs.user.UserFactory;

public class UserUtils {

	public static List<User> getAdminUsers() {
		List<User> admins = new ArrayList<>();
		
		for (User user : UserFactory.iterateUsers()) {
			if (User.isAllowed(user, Rights.ADMIN))
				admins.add(user);
		}
		
		return admins;
	}
}
