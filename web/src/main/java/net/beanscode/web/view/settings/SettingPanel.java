package net.beanscode.web.view.settings;

import static net.beanscode.cli.BeansCliCommand.API_SETTING_GETSETTING;
import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.id;
import static org.rendersnake.HtmlAttributesFactory.type;

import java.io.IOException;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.beanscode.web.view.ui.modals.OpModalQuestion;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.buttons.ButtonGroup;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.utils.LibsLogger;

import org.rendersnake.HtmlCanvas;

public class SettingPanel extends BeansComponent {
	
	private Setting setting = null;

	public SettingPanel() {
		
	}
	
	public SettingPanel(Setting setting) {
		setSetting(setting);
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.div(id(getId())
					.class_("setting-panel"))
				.span(class_("title"))
					.content(getSetting().getName())
				.if_(getSetting().isSystem())
					.span()
						.content(String.format(" (key: %s, last edit: %s%s)", 
								getSetting().getId(), 
								getSetting().getLastEdit().toStringHuman(),
								getSetting().getUserId() != null ? ", userId: " + getSetting().getUserId() : ""))
				._if()
				.if_(!getSetting().isSystem()
						&& getSetting().getUser(getSessionBean()) != null)
					.span()
						.content(String.format(" (user: %s)", 
								getSetting().getUser(getSessionBean()) != null ?
										getSetting().getUser(getSessionBean()).getEmail()
										: ""))
				._if()
				.p()
					.content(getSetting().getDescription())
				.input(type("hidden")
						.name("user")
						.value(getSetting().getUserId() != null ? getSetting().getUserId().getId() : ""))
				.input(type("hidden")
						.name("settingId")
						.value(getSetting().getId()));
		
		html
			.render(new SettingsValuesTableComponent(getSetting().getValues()));
		
//		for (SettingValue val : getSetting().getValues()) {
//			String name = "newVal-" + val.getName();
//			
//			
//		}
		
		html
				.render(new ButtonGroup()
						.addButton(new Button("Save: " + getSetting().getName())
									.add(new OpModalQuestion("Saving setting",
											"Do you want to save the setting?",
											new OpComponentClick(this, "onSave")))))
			._div();
	}
	
	private OpList onSave() throws CLIException, IOException {
		ApiCallCommand cmd = new ApiCallCommand(getSessionBean())
			.setApi("api/setting/setSetting")
			.addParam("id", getSetting().getId());
		
		if (getSetting().getUserId() != null)
			cmd.addParam("userEmail", getSetting().getUserId());
		
		for (SettingValue val : getSetting().getValues()) {
			String newVal = getParams().getString("newVal-" + val.getName());
			
			if (val.getType() == SettingType.BOOLEAN)
				newVal = newVal != null && newVal.equalsIgnoreCase("on") ? "true" : "false";
			
			cmd.addParam(val.getName(), newVal);
			
//			new NewSettingsSet(getSessionBean())
//				.run(getSetting().getUserId() != null ? getSetting().getUserId().getId() : null, 
//					getSetting().getId(), 
//					val.getName(), 
//					newVal);
		}
		
		cmd.run();
		
		return new OpList()
			.add(new OpBeansInfo("Setting saved"));
	}

	public Setting getSetting() {
		if (setting == null) {
			try {
				String settingId = getParams().getString("settingId");
				String user = getParams().getString("user");
				this.setting = new ApiCallCommand(this)
								.setApi(API_SETTING_GETSETTING)
								.addParam("id", settingId)
								.addParam("userEmail", user)
								.run()
								.getResponse()
								.getAsObject(Setting.class);
			} catch (CLIException | IOException e) {
				LibsLogger.error(SettingPanel.class, "Cannot get setting", e);
			}
		}
		return setting;
	}

	public void setSetting(Setting setting) {
		this.setting = setting;
	}
}
