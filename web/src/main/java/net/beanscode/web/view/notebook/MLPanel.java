package net.beanscode.web.view.notebook;

import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.id;
import static org.rendersnake.HtmlAttributesFactory.name;
import static org.rendersnake.HtmlAttributesFactory.src;

import java.io.IOException;

import javax.ws.rs.core.SecurityContext;

import org.rendersnake.HtmlAttributes;
import org.rendersnake.HtmlAttributesFactory;
import org.rendersnake.HtmlCanvas;
import org.rendersnake.Renderable;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.notebooks.AwkEntry;
import net.beanscode.cli.notebooks.MLEntry;
import net.beanscode.web.view.awk.AwkPanel;
import net.beanscode.web.view.plots.PlotPanel;
import net.beanscode.web.view.ui.DatasetTableSelector;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.ops.OpComponentTimer;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpReplace;
import net.hypki.libs5.pjf.op.OpSet;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.api.APICallType;
import net.hypki.libs5.utils.string.RandomUtils;
import net.hypki.libs5.utils.url.Params;

public class MLPanel extends NotebookEntryEditorPanel {
	
	private HtmlCanvas renderView(HtmlCanvas html) throws IOException {
		return html
				.div(class_("ml-panel"))
					.p()
						.b()
							.content("Training on ")
						.span()
							.content("datasets: " + getMLEntry().getDsTrainQuery())
						.span()
							.content(" and tables: " + getMLEntry().getTbTrainQuery())
						.span()
							.content(" on columns: " + getMLEntry().getTrainColumns())
						.span()
							.content(" to predict column: " + getMLEntry().getPredictColumn())
					._p()
					.p()
						.b()
							.content("Testing on ")
						.span()
							.content(" datasets: " + getMLEntry().getDsTestQuery())
						.span()
							.content(" and table: " + getMLEntry().getTbTestQuery())
					._p()
					.p()
						.b()
							.content("Saving predictions ")
						.span()
							.content("to table: " + getMLEntry().getOutputTable())
						.span()
							.content(" with prediction column name: " + getMLEntry().getOutputColumn())
					._p()
				._div()
				
				;
	}

	@Override
	public Renderable getView() {
		return new Renderable() {
			
			@Override
			public void renderOn(HtmlCanvas html) throws IOException { 
				renderView(html);
			}
		};
	}
	
	private HtmlCanvas renderEditor(HtmlCanvas html) throws IOException {
		return html
			.div(class_("ml-panel"))
				.span()
					.content("Title")
				.input(name("name")
						.type("text")
						.add("placeholder", "Name of the ML script")
						.class_("query-name form-control")
						.value(getMLEntry().getName()))
				.div(class_("").style("border-left: 3px solid gray; margin: 10px; padding: 10px; "))
					.p()
						.content("<b>Training</b> table(s)", false)
					.render(new DatasetTableSelector(MLPanel.this, getMLEntry().getDsTrainQuery(), getMLEntry().getTbTrainQuery(), 
							MLEntry.META_DS_TRAIN_QUERY, MLEntry.META_TB_TRAIN_QUERY))
					.span()
						.content("Column names for training (separated by comma)")
					.input(name(MLEntry.META_TRAIN_COLUMNS)
							.type("text")
							.add("placeholder", "Table name")
							.class_("form-control")
							.value(getMLEntry().getTrainColumns()))
					.span()
						.content("Column name to predict")
					.input(name(MLEntry.META_PREDICT_COLUMN)
							.type("text")
							.add("placeholder", "Table name")
							.class_("form-control")
							.value(getMLEntry().getPredictColumn()))
				._div()
				.div(class_("").style("border-left: 3px solid gray; margin: 10px; padding: 10px; "))
					.p()
						.content("<b>Test</b> table(s)", false)
					.render(new DatasetTableSelector(MLPanel.this, getMLEntry().getDsTestQuery(), getMLEntry().getTbTestQuery(), 
							MLEntry.META_DS_TEST_QUERY, MLEntry.META_TB_TEST_QUERY))
					.span()
						.content("Column names for testing should be the same as for learning and should exist in the test tables")
				._div()
				.div(class_("").style("border-left: 3px solid gray; margin: 10px; padding: 10px; "))
					.p()
						.content("<b>Output</b> table", false)
					.span()
						.content("Output table name with predictions")
					.input(name(MLEntry.META_OUTPUT_TABLE)
							.type("text")
							.add("placeholder", "Table name")
							.class_("form-control")
							.value(getMLEntry().getOutputTable()))
					.span()
						.content("Column name with predictions")
					.input(name(MLEntry.META_OUTPUT_COLUMN)
							.type("text")
							.add("placeholder", "Table name")
							.class_("form-control")
							.value(getMLEntry().getOutputColumn()))
				._div()
			._div()
			;
	}

	@Override
	protected Renderable getEditor() {
		return new Renderable() {
			
			@Override
			public void renderOn(HtmlCanvas html) throws IOException {
				renderEditor(html);
			}
		};
	}
	
	private MLEntry getMLEntry() {
		return (MLEntry) getNotebookEntry();
	}

	@Override
	protected Button getMenu() throws IOException {
		return null;
	}

	@Override
	protected OpList opOnPlay(SecurityContext sc, Params params) throws IOException {
		new ApiCallCommand(this)
			.setApi("api/notebook/entry")
			.addParam("entryId", getMLEntry().getId().getId())
			.addParam("name", getParams().getString("name"))
			.addParam(MLEntry.META_DS_TRAIN_QUERY, getParams().getString(MLEntry.META_DS_TRAIN_QUERY).trim())
			.addParam(MLEntry.META_TB_TRAIN_QUERY, getParams().getString(MLEntry.META_TB_TRAIN_QUERY).trim())
			.addParam(MLEntry.META_TRAIN_COLUMNS, getParams().getString(MLEntry.META_TRAIN_COLUMNS).trim())
			.addParam(MLEntry.META_PREDICT_COLUMN, getParams().getString(MLEntry.META_PREDICT_COLUMN).trim())
			.addParam(MLEntry.META_DS_TEST_QUERY, getParams().getString(MLEntry.META_DS_TEST_QUERY).trim())
			.addParam(MLEntry.META_TB_TEST_QUERY, getParams().getString(MLEntry.META_TB_TEST_QUERY).trim())
			.addParam(MLEntry.META_OUTPUT_TABLE, getParams().getString(MLEntry.META_OUTPUT_TABLE).trim())
			.addParam(MLEntry.META_OUTPUT_COLUMN, getParams().getString(MLEntry.META_OUTPUT_COLUMN).trim())
			.run();
		
		new ApiCallCommand(this)
			.setApi("api/notebook/entry/start")
			.addParam("entryId", getMLEntry().getId().getId())
			.run();
			
		return new OpList()
			.add(new OpBeansInfo("ML script scheduled to run"))
			.add(new OpComponentTimer(this, "onRefresh", 400, new Params().add("entryId", getMLEntry().getId().getId())));
	}

	@Override
	protected OpList opOnEdit(SecurityContext sc, Params params) throws IOException {
		return null;
	}

	@Override
	protected OpList opOnStop(SecurityContext sc, Params params) throws IOException {
		return null;
	}

	@Override
	protected OpList opOnRemove(SecurityContext sc, Params params) throws IOException {
		try {
			new ApiCallCommand(this)
				.setApi("api/notebook/entry/remove")
				.setApiCallType(APICallType.DELETE)
				.addParam("entryId", params.getString("entryId"))
				.run();
		} catch (IOException e) {
			LibsLogger.error(PlotPanel.class, "Cannot remove Notebook Entry", e);
		}
		
		return null;
	}

	@Override
	protected OpList onRefresh(SecurityContext sc, Params params) throws IOException {
		return new OpList()
				.add(new OpReplace("#" + getId() + " .ml-panel", renderView(new HtmlCanvas()).toHtml()));
	}

}
