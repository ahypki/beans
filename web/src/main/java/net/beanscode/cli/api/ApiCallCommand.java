package net.beanscode.cli.api;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;
import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.StreamingOutput;

import com.google.gson.JsonElement;
import com.google.gson.JsonNull;

import net.beanscode.cli.BeansCliCommand;
import net.beanscode.web.BeansWebConst;
import net.beanscode.web.view.ui.BeansComponent;
import net.hypki.libs5.cli.noninteractivecli.CLICommand;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.cli.noninteractivecli.CliArg;
import net.hypki.libs5.pjf.components.jersey.SessionBean;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.api.API;
import net.hypki.libs5.utils.api.APIArg;
import net.hypki.libs5.utils.api.APIArgType;
import net.hypki.libs5.utils.api.APICall;
import net.hypki.libs5.utils.api.APICallType;
import net.hypki.libs5.utils.args.ArgsUtils;
import net.hypki.libs5.utils.file.FileUtils;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.reflection.SystemUtils;
import net.hypki.libs5.utils.string.Base64Utils;
import net.hypki.libs5.utils.url.Params;
import net.hypki.libs5.utils.url.UrlUtilities;

public class ApiCallCommand extends BeansCliCommand<ApiCallCommand> {
	
	private static API beansAPI = null;
	
	@CliArg(name = "api",
			description = "BEANS API path to be called",
			required = false)
	private String api = null;
		
	private Object body = null;
	
	private APICallType apiCallType = null;
	
	private Params adhocParams = null;
	
	private List<InputStream> paramInputStreams = null;
	private List<String> paramInputStreamsNames = null;
	
//	public ApiCallCommand(SecurityContext sc) {
//		super(null, (SessionBean) sc.getUserPrincipal());
//	}
	
	public ApiCallCommand(SessionBean sb) {
		super(null, sb);
	}
	
	public ApiCallCommand(BeansComponent parent) {
		super(null, parent != null ? parent.getSessionBean() : null);
		
		if (parent != null && parent.getParams().contains(MAGIC_KEY))
			getAdhocParams().add(MAGIC_KEY, parent.getParams().get(MAGIC_KEY));
	}
	
	@Override
	public String getBody() {
		if (body == null)
			return null;
		
		return body instanceof String ? (String) body : JsonUtils.objectToString(body);
	}
	
	@Override
	public CLICommand run(String[] args) throws CLIException, IOException {
		String api = ArgsUtils.getString(args, "api");
		
		if (nullOrEmpty(api)
				|| getBeansAPI().getCall(api, getApiCallType()) == null) {
			LibsLogger.error(ApiCallCommand.class, "API '" + api + "' not recognized");
			
			LibsLogger.info(ApiCallCommand.class, "");
			LibsLogger.info(ApiCallCommand.class, "Possible API calls: ");
			for (APICall call : getBeansAPI().getCalls()) {
				LibsLogger.info(ApiCallCommand.class, "\t" + call.toString());
			}
			return this;
		}
		
		// dealing with command like: api --path localhost:25000 --api api/file/upload --file /home/ahypki/programy/lto/ejp-new.beans --datasetId 9d4b3a60e8f68545a62310178a58a29b
		for (String key : ArgsUtils.iterateKeys(args)) {
			if (key.equals("api")
					|| key.equals("path")) {
				// do nothing
				continue;
			}
			
			String value = ArgsUtils.getString(args, key);
			if (notEmpty(value)) {
				if (FileUtils.exist(value)) {
					//  it is a file
					addStream(key, new FileInputStream(new File(value)));
				} else 
					getAdhocParams().add(key, value);
			}
		}
		
		return super.run(args);
	}

	public ApiCallCommand setBody(Object body) {
		this.body = body;
		return this;
	}
	
	@Override
	public String getLoginCookieName() {
		return BeansWebConst.COOKIE_LOGIN;
	}
	
	public static API getBeansAPI() {
		if (beansAPI == null) {
			try {
				beansAPI = API.buildAPI(Class.forName("net.beanscode.rest.BeansServerMain"));
			} catch (Exception e) {
				LibsLogger.error(ApiCallCommand.class, "Cannot build API based on classes", e);
			}
			
			if (beansAPI == null) {
				try {
					beansAPI = API.buildAPI(SystemUtils.readFileContent(ApiCallCommand.class, "beans-api.json"));
				} catch (Exception e) {
					LibsLogger.error(ApiCallCommand.class, "Cannot build BEANS API based on file beans-api.json", e);
				}
			}
		}
		return beansAPI;
	}
	
	@Override
	protected String getRestMethodType() {
		return getBeansAPI()
				.getCall(getPath(), getApiCallType())
				.getType()
				.toString();
	}

	@Override
	public String getUrlPath() {
		return getPath();
	}

	@Override
	public void processResponse() {
//		LibsLogger.debug(ApiCallCommand.class, "Response: " + getResponse());
	}

	public <T extends BeansCliCommand> T run() throws CLIException, IOException {
		return (T) super.run(getArgs());
	}
	
	public StreamingOutput runStream() throws IOException {
		
		
		return new StreamingOutput() {
			@Override
			public void write(OutputStream out) throws IOException, WebApplicationException {
//				IOUtils.pipe(is, out);
				UrlUtilities.getStream(getURL(), 
						new String[] {BeansWebConst.COOKIE_LOGIN, getUserLC()}, 
						getRESTParams(), 
						out);
			}
		};
	}
	
	@Override
	public Params getRESTParams() {
		Params params = new Params();
		
		List<String> usedArgs = new ArrayList<String>();
		boolean isMap = false;
		
		for (APIArg arg : getBeansAPI().getCall(getPath(), getApiCallType()).getArgs()) {
			if (arg.getType() == APIArgType.MAP) {
				// deal with it later
				isMap = true;
			} else if (arg.getType() == APIArgType.LIST) {
				usedArgs.add(arg.getName());
				params.add(arg.getName(), ArgsUtils.getStrings(getArgs(), arg.getName()));
			} else {
				usedArgs.add(arg.getName());
				params.add(arg.getName(), ArgsUtils.getString(getArgs(), arg.getName()));
			}
		}
		
		if (isMap)
			for (String name : ArgsUtils.iterateKeys(getArgs())) {
				if (!usedArgs.contains(name))
					params.add(name, ArgsUtils.getString(getArgs(), name));
			}
		
		params.addAll(getAdhocParams());
		
		return params;
	}

	@Override
	public String getCLICommandName() {
		return "api";
	}

	@Override
	public String getCLICommandDescription() {
		return "Call any of the BEANS API method";
	}
	
	public static String normalizeApiPath(String api) {
		if (api != null) {
			api = api.trim();
			if (api.startsWith("/"))
				api = api.substring(1);
			if (api.endsWith("/"))
				api = api.substring(0, api.length() - 1);
		}
		return api;
	}

	public String getPath() {
		if (api != null) {
			api = normalizeApiPath(api);
		}
		return api;
	}

	public ApiCallCommand setApi(String api) {
		this.api = normalizeApiPath(api);
		return this;
	}
	
	public ApiCallCommand addParam(String key, Object value) {
		if (key == null)
			return this;
		
//		if (!key.startsWith("--"))
//			key = "--" + key;
		
//		setArgs((String[]) ArrayUtils.concat(getArgs(), new String[] {key, String.valueOf(value)}));
		
		if (key != null && key.startsWith("--"))
			key = key.substring(2);
		
//		if (value instanceof List)
//			getAdhocParams().add(key, nullOrEmpty(value) ? "" : Base64Utils.encode(value.toString()));
//		else
			getAdhocParams().add(key, nullOrEmpty(value) ? "" : value);
		
		return this;
	}
	
	public APICall getApiCall() {
		return getBeansAPI().getCall(getUrlPath(), getApiCallType());
	}

	public APICallType getApiCallType() {
		return apiCallType;
	}

	public ApiCallCommand setApiCallType(APICallType apiCallType) {
		this.apiCallType = apiCallType;
		return this;
	}

	private Params getAdhocParams() {
		if (adhocParams == null)
			adhocParams = new Params();
		return adhocParams;
	}

	private void setAdhocParams(Params adhocParams) {
		this.adhocParams = adhocParams;
	}

	public JsonElement getBodyAsJson() {
		if (getBody() != null) {
			return JsonUtils.parseJson(getBody()); 
		}
		return new JsonNull();
	}

	public ApiCallCommand addStream(String name, InputStream is) {
		if (paramInputStreams == null) {
			paramInputStreams = new ArrayList<InputStream>();
			paramInputStreamsNames = new ArrayList<String>();
		}
		
		paramInputStreams.add(is);
		paramInputStreamsNames.add(name);
		return this;
	}
	

	@Override
	protected InputStream[] getPostInputStreams() {
		if (paramInputStreams != null) {
			InputStream [] streams = new InputStream[this.paramInputStreams.size()];
			int i = 0;
			for (InputStream inputStream : paramInputStreams) {
				streams[i++] = inputStream;
			}
			return streams;
		} else
			return null;
	}
	
	@Override
	protected String[] getPostInputStreamsNames() {
		if (paramInputStreams != null) {
			String [] streams = new String[this.paramInputStreams.size()];
			int i = 0;
			for (String name : paramInputStreamsNames) {
				streams[i++] = name;
			}
			return streams;
		} else
			return null;
	}
}
