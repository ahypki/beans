package net.beanscode.web.beta;

import java.io.IOException;

import javax.annotation.security.PermitAll;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

import net.beanscode.web.BetaPage;
import net.beanscode.web.view.datamanger.BulkContent;
import net.beanscode.web.view.ui.BetaButtonGroup;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.components.jersey.SessionBean;

import org.rendersnake.HtmlCanvas;

@Path(DashboardContent.PATH)
public class DashboardContent extends BetaContent {
	
	public final static String PATH = "dashboard";

	public DashboardContent() {
		
	}
	
	public DashboardContent(Component parent) {
		super(parent);
	}
	
	@Override
	protected String getAjaxPath() {
		return PATH;
	}
		
	@Override
	protected void renderHeader(HtmlCanvas html) throws IOException {
		html
			.h1()
				.content("Dashboard");
	}
	
	@Override
	protected void renderContent(HtmlCanvas html) throws IOException {
		html
			.render(new net.beanscode.web.beta.DashboardPanel(this));
	}

	@Override
	protected BetaButtonGroup getMenu() throws IOException {
		return null;
	}

	@Override
	protected String getMenuName() throws IOException {
		return null;
	}
}
