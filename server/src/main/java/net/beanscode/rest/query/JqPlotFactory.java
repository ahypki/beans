package net.beanscode.rest.query;

import static java.lang.String.format;

import net.beanscode.model.notebook.NotebookEntry;

public class JqPlotFactory {

	private static final String DATA_URL = "view/data";
	
	public static String createJqPlot(NotebookEntry plot, int plotNr) {
		return format("beansJqplot('figure-%s', "
				+ "'%s?queryId=%s&plotNr=%d', "
				+ "{title: '%s', dataRenderer: ajaxDataRenderer, resetAxes:['yaxis', 'y2axis'], highlighter: {show: true,sizeAdjust: 7.5},cursor : {show : true,zoom : true,showTooltip : false},legend: {show: true}} )", 
				plot.getId(), 
				DATA_URL, 
				plot.getId(),
				plotNr,
				"" 
				);
	}
	
//	public static String createJqPlotMulti(Plot plot, int plotNr) {
//		return format("beansJqplot('figure-%s', "
//				+ "[  [    [1,1],[2,2],[3,3],[4,4],[5,5]  ],  [    [5,1],[4,2],[3,3],[2,4],[1,5]  ]], "
////				+ "['%s?queryId=%s&plotNr=%d', '%s?queryId=%s&plotNr=%d'], "
////				+ "{title: '%s', dataRenderer: ajaxDataRenderer, highlighter: {show: true,sizeAdjust: 7.5},cursor : {show : true,zoom : true,showTooltip : false},legend: {show: true}} )", 
//				+ "{title: '%s', highlighter: {show: true,sizeAdjust: 7.5},cursor : {show : true,zoom : true,showTooltip : false},legend: {show: true}} )",
//				plot.getQueryId().getId(), 
////				QueryData.URL,
////				plot.getQueryId().getId(),
////				plotNr,
////				QueryData.URL, 
////				plot.getQueryId().getId(),
////				plotNr,
//				plot.getPlotName() 
//				);
//	}
}
