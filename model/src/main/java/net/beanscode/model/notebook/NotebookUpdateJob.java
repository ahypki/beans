package net.beanscode.model.notebook;

import java.io.IOException;
import java.util.List;

import com.google.gson.annotations.Expose;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.weblibs.jobs.Job;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

public class NotebookUpdateJob extends Job {
	
	@Expose
	@NotNull
	@NotEmpty
	private UUID notebookId = null;
	
	@Expose
	private List<UUID> doneEntryIDs = null;
	
	@Expose
	private UUID currentEntryID = null;
	
	private Notebook notebookCache = null;

	public NotebookUpdateJob() {
		
	}
	
	public NotebookUpdateJob(Notebook notebook) {
		setNotebookId(notebook.getId());
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		for (NotebookEntry ne : getNotebook().iterateEntries()) {
			if (getDoneEntryIDs().contains(ne.getId())) {
				
			}
			
//			ne.update();
		}
	}
	
	public Notebook getNotebook() throws IOException {
		if (notebookCache == null) {
			notebookCache = Notebook.getNotebook(getNotebookId());
		}
		return notebookCache;
	}

	public UUID getNotebookId() {
		return notebookId;
	}

	public NotebookUpdateJob setNotebookId(UUID notebookId) {
		this.notebookId = notebookId;
		return this;
	}

	public List<UUID> getDoneEntryIDs() {
		return doneEntryIDs;
	}

	public NotebookUpdateJob setDoneEntryIDs(List<UUID> doneEntryIDs) {
		this.doneEntryIDs = doneEntryIDs;
		return this;
	}

	public UUID getCurrentEntryID() {
		return currentEntryID;
	}

	public NotebookUpdateJob setCurrentEntryID(UUID currentEntryID) {
		this.currentEntryID = currentEntryID;
		return this;
	}
}
