package net.beanscode.model.dataset;

import java.io.IOException;
import java.io.InputStream;

import net.hypki.libs5.utils.json.JsonUtils;

import org.apache.commons.io.IOUtils;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.hazelcast.nio.serialization.StreamSerializer;

public class ClearTableJobSerializer implements StreamSerializer<ClearTableJob> {
	
	public ClearTableJobSerializer() {
		
	}
	
	@Override
	public ClearTableJob read(ObjectDataInput in) throws IOException {
		final InputStream inputStream = (InputStream) in;
		
		return JsonUtils.fromJson(IOUtils.toString(inputStream), ClearTableJob.class);
	}

	@Override
	public void write(ObjectDataOutput out, ClearTableJob object) throws IOException {
		out.write(JsonUtils.objectToString(object).getBytes());
	}

	@Override
	public int getTypeId() {
		return 854568745;
	}
	
	@Override
	public void destroy() {
	}
}
