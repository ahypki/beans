package net.beanscode.web.view.settings;

import static net.hypki.libs5.pjf.OpsFactory.opsCall;
import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.id;
import static org.rendersnake.HtmlAttributesFactory.type;

import java.awt.Color;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.QueryParam;
import javax.ws.rs.core.SecurityContext;

import net.beanscode.pojo.colorpalette.ColorPalette;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.Op;
import net.hypki.libs5.pjf.op.OpJs;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpReplace;
import net.hypki.libs5.pjf.op.OpSetAttr;
import net.hypki.libs5.pjf.op.OpSetVal;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.url.Params;
import net.hypki.libs5.utils.url.UrlUtilities;
import net.hypki.libs5.utils.utils.ColorUtils;

import org.rendersnake.HtmlCanvas;

public class ColorPalettePanel extends BeansComponent {

	private ColorPalette colorPalette = null;
	
	public ColorPalettePanel() {
		
	}

	public ColorPalettePanel(BeansComponent parent) {
		super(parent);
	}
	
	public ColorPalettePanel(BeansComponent parent, ColorPalette colorPalette) {
		super(parent);
		setColorPalette(colorPalette);
	}

	public OpList demoReloadPlot(Params params) throws IOException, ValidationException {
		List<String> colorId = params.getList("colorId");
		
		List<String> colorsDecoded = new ArrayList<>();
		for (String color : colorId) {
			colorsDecoded.add(UrlUtilities.urlDecode(color));
		}
		return new OpList()
			.add(new OpJs("beansDemoPlot('figure-demo', " + JsonUtils.objectToString(colorsDecoded) + ")"));
	}
	
	public OpList generateRandomPalette() throws IOException, ValidationException {		
		ColorPalettePanel palette = new ColorPalettePanel(this);

		return new OpList()
			.add(new OpReplace("#" + getId(), palette.toHtml()))
			.add(new OpComponentClick(palette, "demoReloadPlot"))
			;
	}
	
	public OpList generateDefaultPalette() throws IOException, ValidationException {
		ColorPalette palette = null;//new ColorPaletteDefault(getSessionBean()).run(getUserEmail());
		ColorPalettePanel panel = new ColorPalettePanel(this, palette);
		
		return new OpList()
			.add(new OpReplace("#" + getId(), panel.toHtml()))
			.add(new OpComponentClick(panel, "demoReloadPlot"));
	}
	
	public OpList generateRandomColor(@QueryParam("colorId") String colorId) throws IOException, ValidationException {
		final String colorHex = "#" + ColorUtils.colorToHex(ColorUtils.randomPastelColor3());
		
		return new OpList()
			.add(new OpSetAttr("#" + colorId, "style", "background-color: " + colorHex))
			.add(new OpSetVal("#color-" + colorId, colorHex));
	}
	
	public Op save(SecurityContext sc, Params params) throws IOException, ValidationException {
		List<String> colorId = params.getList("colorId");
		
		final UUID userId = getSessionBean(sc).getUserId();
		
//		ColorPalette palette = ColorPalette.getColorPalette(userId);
//		
//		if (palette == null)
//			palette = new ColorPalette();
//		
//		palette.getColors().clear();
		List<String> colors = new ArrayList<String>();
		for (String color : colorId) {
//			color = UrlUtilities.urlDecode(color);
			colors.add(UrlUtilities.urlDecode(color));
//			palette.getColors().add(ColorUtils.parseCssRgb(color).getRGB());
		}
//		
//		ColorPalette.saveColorPalette(userId, palette);
		
		// new ApiCallCommand(.....
//		new ColorPaletteSet(getSessionBean()).run(getUserEmail(), colors);
		
		return new OpBeansInfo("Color palette saved");
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.div(id(getId()).class_("color-palette"))
				.p()
					.content("Choose the most clear palette of colors for your plots.")
				.div(class_("btn-group menu"))
					.button(class_("btn btn-default btn-sm")
							.onClick(opsCall(new OpComponentClick(this, "generateRandomPalette"))))
						.i(class_("glyphicon glyphicon-refresh"))
			            ._i()
			            .span()
			            	.content(" Generate random palette")
					._button()
					.button(class_("btn btn-default btn-sm")
							.onClick(opsCall(new OpComponentClick(this, "generateDefaultPalette"))))
						.span()
			            	.content("Reset to default")
					._button()
					.button(class_("btn btn-default btn-sm")
							.onClick(opsCall(new OpComponentClick(this, "save"))))
			            .span()
			            	.content("Save")
					._button()
					.button(class_("btn btn-default btn-sm")
							.onClick(opsCall(new OpComponentClick(this, "demoReloadPlot"))))
						.i(class_("glyphicon glyphicon-refresh"))
			            ._i()
			            .span()
			            	.content(" Refresh plot")
					._button()
				._div()
			.form()
				.div(class_("palette-table"))
//					.tr()
//						.td()
				;
		
		int line = 1;
		for (Integer color : getColorPalette().getColors()) {
			final String colorId = UUID.random().getId();
			final String colorHex = "#" + ColorUtils.colorToHex(new Color(color));
			
			html
				.input(type("hidden")
						.name("colorId")
						.value(colorHex)
						.id("color-" + colorId))
				.span(class_("color-palette-sample-text")
						.id(colorId)
						.add("style", "background-color: " + colorHex))
					.content("Line " + line++ + ", color " + colorHex)
				.a(class_("btn btn-default ")
						.onClick(opsCall(new OpComponentClick(this, "generateRandomColor", new Params().add("colorId", colorId)))))
					.i(class_("glyphicon glyphicon-refresh"))
		            ._i()
		        ._a()
		        .br();
		}
		
		html
//						._td()
//						.td()
//						._td()
//					._tr()
				._div()
				.div(id("figure-demo"))
					
//					.write("<svg />")
				._div()
			._form()
			._div();
	}

	public ColorPalette getColorPalette() {
		if (colorPalette == null) {
			try {
				colorPalette = null;//new ColorPaletteDefault(getSessionBean()).run(getUserEmail());
			} catch (CLIException e) {
				LibsLogger.error(ColorPalettePanel.class, "Cannot get default color palette", e);
			}
		}
		return colorPalette;
	}

	public void setColorPalette(ColorPalette colorPalette) {
		this.colorPalette = colorPalette;
	}
}
