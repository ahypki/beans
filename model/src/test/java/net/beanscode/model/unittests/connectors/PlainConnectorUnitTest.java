package net.beanscode.model.unittests.connectors;

import java.io.IOException;

import net.beanscode.model.connectors.PlainConnector;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;

import java.io.IOException;

import jodd.io.FileEx;

import org.junit.Test;

public class PlainConnectorUnitTest {

	@Test
	public void testReadWritePlainConnector() throws IOException {
		FileExt source = new FileExt("src/main/resources/testdata/line-1-10.json");
		FileExt dest = new FileExt("line-1-10_copy.json");
		
		dest.deleteIfExists();
		
		PlainConnector reader = new PlainConnector(source);
		PlainConnector writer = new PlainConnector(dest, reader.getColumnDefs());
		for (Row row : reader) {
			writer.write(row);
		}
		writer.close();
		
		LibsLogger.debug(PlainConnectorUnitTest.class, "Reading ", source.getAbsolutePath());
		reader = new PlainConnector(source);
		for (Row row : reader) {
			LibsLogger.debug(PlainConnectorUnitTest.class, "Read row from Plain file ", row);
		}
		
		LibsLogger.debug(PlainConnectorUnitTest.class, "Finished!");
	}
	
//	@Test
//	public void testRead2() throws IOException {
//		PlainConnector con = new PlainConnector(new File())
//	}
}
