package net.beanscode.pojo;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;

import com.google.gson.annotations.Expose;

import net.beanscode.pojo.autoupdate.AutoUpdateSettings;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.api.APICall;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.string.Meta;
import net.hypki.libs5.utils.string.MetaList;
import net.hypki.libs5.utils.string.StringUtilities;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

public class NotebookEntryHistory extends NotebookEntry {
	
	@Expose
	private UUID sourceEntryId = null;
	
	public NotebookEntryHistory() {
		
	}


	public UUID getSourceEntryId() {
		return sourceEntryId;
	}


	public void setSourceEntryId(UUID sourceEntryId) {
		this.sourceEntryId = sourceEntryId;
	}

}
