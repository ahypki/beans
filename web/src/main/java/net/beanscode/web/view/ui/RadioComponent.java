package net.beanscode.web.view.ui;

import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.for_;
import static org.rendersnake.HtmlAttributesFactory.id;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import net.hypki.libs5.pjf.components.Component;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

public class RadioComponent extends BeansComponent {
	
	@Expose
	@NotNull
	private String name = null;
	
	@Expose
	@NotNull
	@NotEmpty
	private Map<String, String> values = null;
	
	private String selectedValue = null;
	
	public RadioComponent() {
		
	}
	
	public RadioComponent(Component parent) {
		super(parent);
	}
	
	public RadioComponent(Component parent, String name) {
		super(parent);
		setName(name);
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.div(class_("radio-component"));
		for (String val : getValues().keySet()) {
			html
				.input(id(name + "-" + val)
						.name(getName())
						.type("radio")
						.value(val)
						.checked_if(selectedValue != null && selectedValue.equals(val)))
				.span()
					.content(" ")
				.label(for_(name + "-" + val))
					.content(getValues().get(val))
				.br();
		}
		html
			._div();
	}
	
	public RadioComponent addValue(String value, String label) {
		getValues().put(value, label);
		return this;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Map<String, String> getValues() {
		if (values == null)
			values = new HashMap<>();
		return values;
	}

	public void setValues(Map<String, String> values) {
		this.values = values;
	}

	public String getSelectedValue() {
		return selectedValue;
	}

	public RadioComponent setSelectedValue(String selectedValue) {
		this.selectedValue = selectedValue;
		return this;
	}


}
