package net.beanscode.web.view.backup;

import static org.rendersnake.HtmlAttributesFactory.*;

import java.io.IOException;

import org.rendersnake.HtmlAttributesFactory;
import org.rendersnake.HtmlCanvas;

import net.beanscode.web.view.ui.BeansComponent;
import net.hypki.libs5.pjf.components.CheckboxComponent;
import net.hypki.libs5.pjf.components.ChooseComponent;
import net.hypki.libs5.pjf.components.InputComponent;
import net.hypki.libs5.pjf.op.OpInfo;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.utils.string.Base64Utils;

public class RestoreDialogPanel extends BeansComponent {
	
	private String restoreFile = null;

	public RestoreDialogPanel() {
		
	}
	
	public RestoreDialogPanel(String restoreFile) {
		setRestoreFile(restoreFile);
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.div(class_("restore-panel"))
			
				.dl(class_("row"))
			
					.dt(class_("col-sm-4"))
						.content("File to restore:")
					.dd(class_("col-sm-8"))
						.content(getRestoreFile())
						
					.input(type("hidden")
							.name("restoreFile")
							.value(getRestoreFile()))
							
					.dt(class_("col-sm-4"))
						.content("Restore:")
					.dd(class_("col-sm-8"))
						.render(new CheckboxComponent("users", "Users, Groups and Permissions"))
						.render(new CheckboxComponent("datasets", "Datasets and Tables"))
						.render(new CheckboxComponent("notebooks", "Notebooks"))
					._dd()
					
					.dt(class_("col-sm-4"))
						.content("Restore strategy")
					.dd(class_("col-sm-8"))
						.render(new ChooseComponent("create objects if they do not exist", 
							"override objects", 
							"clone objects as new"))
					._dd()
						
					.dt(class_("col-sm-4"))
						.content("Restore objects")
					.dd(class_("col-sm-8"))
						.div()
							.span()
								.content("with prefix")
							.input(class_("text form-control inline")
									.type("text")
									.name("restore-prefix")
									.value("")
									.add("style", "width: 100px; margin-left: 10px;"))
						._div()
						.div()
							.span()
								.content("with suffix")
							.input(class_("text form-control inline")
									.type("text")
									.name("restore-suffix")
									.value(" [Cloned]")
									.add("style", "width: 100px; margin-left: 12px;"))
						._div()
					._dd()
				._dl()
			.div();
	}

	private String getRestoreFile() {
		return restoreFile;
	}

	private void setRestoreFile(String restoreFile) {
		this.restoreFile = restoreFile;
	}
}
