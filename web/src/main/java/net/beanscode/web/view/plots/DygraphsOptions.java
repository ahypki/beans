package net.beanscode.web.view.plots;

import net.hypki.libs5.utils.json.JsonUtils;

import com.google.gson.JsonElement;
import com.google.gson.annotations.Expose;

/**
 * http://dygraphs.com/index.html
 * 
 * @author ahypki
 *
 */
public class DygraphsOptions {
	
	
	@Expose
	private String legend = "always";
	
	@Expose
	private boolean animatedZooms = true;
	
	@Expose
	private double strokeWidth = 0.0;
	
	@Expose
	private boolean drawPoints = false;
	
	@Expose
	private boolean logscale = false;
	
	@Expose
	private boolean connectSeparatedPoints = true; // default: true
	
	@Expose
	private String title = "";
	
	@Expose
	private boolean labelsSeparateLines = true;
	
	@Expose
	private String labelsDiv = null;
	
	@Expose
	private String ylabel = null;
	
	@Expose
	private String xlabel = null;
	
	private boolean highlight = true;

	public DygraphsOptions() {
		
	}
	
	@Override
	public String toString() {
		JsonElement je = JsonUtils.toJson(this);
		
		if (isHighlight()) {
			JsonElement add = JsonUtils.parseJson("{strokeWidth: 2, strokeBorderWidth: 1, highlightCircleSize: 3}");
			je.getAsJsonObject().add("highlightSeriesOpts", add);
		}
		
//		JsonArray reverseY = new JsonArray();
//		reverseY.add("30");
//		reverseY.add("-10");
//		je.getAsJsonObject().add("valueRange", reverseY);
		
//		return JsonUtils.objectToStringPretty(this);
		
		return je.toString();
	}

	public boolean isAnimatedZooms() {
		return animatedZooms;
	}

	public void setAnimatedZooms(boolean animatedZooms) {
		this.animatedZooms = animatedZooms;
	}

	public String getTitle() {
		return title;
	}

	public DygraphsOptions setTitle(String title) {
		this.title = title;
		return this;
	}

	public DygraphsOptions setLines(boolean lines) {
//		this.drawPoints = !lines;
		this.strokeWidth = lines ? 1.0 : 0.0;
		this.drawPoints = lines ? false : true;
		setHighlight(lines);
		return this;
	}

	public String getLabelsDiv() {
		return labelsDiv;
	}

	public DygraphsOptions setLabelsDiv(String labelsDiv) {
		this.labelsDiv = labelsDiv;
		return this;
	}

	public String getYlabel() {
		return ylabel;
	}

	public DygraphsOptions setYlabel(String ylabel) {
		this.ylabel = ylabel;
		return this;
	}

	public String getXlabel() {
		return xlabel;
	}

	public DygraphsOptions setXlabel(String xlabel) {
		this.xlabel = xlabel;
		return this;
	}

	public boolean isLogscale() {
		return logscale;
	}

	public DygraphsOptions setLogscale(boolean logscale) {
		this.logscale = logscale;
		return this;
	}

	public boolean isHighlight() {
		return highlight;
	}

	public DygraphsOptions setHighlight(boolean highlight) {
		this.highlight = highlight;
		return this;
	}
}
