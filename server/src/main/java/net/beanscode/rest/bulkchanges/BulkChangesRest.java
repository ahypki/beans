package net.beanscode.rest.bulkchanges;

import java.io.IOException;
import java.util.List;

import javax.annotation.security.PermitAll;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.SecurityContext;

import net.beanscode.model.bulk.BulkChangeJob;
import net.beanscode.model.bulk.BulkChangesJobs;
import net.beanscode.rest.BeansRest;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.string.Base64Utils;

import org.quartz.SchedulerException;

@Path("api")
public class BulkChangesRest extends BeansRest {

	@POST
    @Path("bulkchange")
	public void postBulkchange(@Context SecurityContext sc,
			@FormParam("searchType") String searchType,
			@FormParam("searchDatasets") String searchDs,
			@FormParam("searchTables") String searchTb,
			@FormParam("searchNotebooks") String searchNote,
			@FormParam("job") String bulkChangeJob,
			@FormParam("autoUpdate") String autoUpdate,
			MultivaluedMap<String, String> meta) throws IOException, ValidationException, SchedulerException {
		
		BulkChangesJobs jobType = BulkChangesJobs.valueOf(bulkChangeJob);
		BulkChangeJob job = new BulkChangeJob();
		
		if (searchType.equalsIgnoreCase("Datasets")) {
			job
				.setDatasetQuery(searchDs)
				.addJob(jobType);
		} else if (searchType.equalsIgnoreCase("Tables")) {
			job
				.setDatasetQuery(searchDs)
				.setTableQuery(searchTb)
				.addJob(jobType);
		} else if (searchType.equalsIgnoreCase("Notebooks")) {
			job
				.setNotebookQuery(searchNote)
				.addJob(jobType);
		}
		
		if (meta != null) {
			meta.remove("searchType");
			meta.remove("searchDatasets");
			meta.remove("searchTables");
			meta.remove("searchNotebooks");
			meta.remove("job");
			
			for (String key : meta.keySet()) {
				List<String> values = meta.get(key);
				
				validateBadRequest(values.size() == 1, "In Bulk Change only one value is allowed for each key in meta list");
				
				job.getParams().add(key, Base64Utils.decode(values.get(0)));
			}
		}
		
		job
			.setUserId(getUserId(sc))
			.save();
	}
}
