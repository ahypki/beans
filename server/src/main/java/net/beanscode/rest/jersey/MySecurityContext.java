package net.beanscode.rest.jersey;

import java.security.Principal;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.jersey.SessionBean;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.user.Allowed;
import net.hypki.libs5.weblibs.user.CookieUUID;

public class MySecurityContext implements javax.ws.rs.core.SecurityContext {
	
//	private final String app;

	private final SessionBean session;

	public MySecurityContext(UUID userLC, UUID userId) {
		if (userLC != null) {
			SessionBean sessionBean = new SessionBean();
			sessionBean.setSessionId(userLC.getId());
			sessionBean.setUserId(userId.getId());
	
			this.session = sessionBean;
		} else
			this.session = null;
		
//		this.app = app;
	}

	@Override
	public String getAuthenticationScheme() {
		return SecurityContext.BASIC_AUTH;
	}

	@Override
	public Principal getUserPrincipal() {
		return session;
	}

	@Override
	public boolean isSecure() {
		return (null != session);// ? session.isSecure() : false;
	}

	@Override
	public boolean isUserInRole(String role) {

		if (null == session) { // || !session.isActive()) {
			// Forbidden
			Response denied = Response.status(Response.Status.FORBIDDEN).entity("Permission Denied").build();
			throw new WebApplicationException(denied);
		}

		try {
			// this user has this role?
			return Allowed.isAllowed(session.getUserId(), role);
//			return session.hasRole(role);// getRoles().contains(SessionBean.Role.valueOf(role));
		} catch (Exception e) {
			LibsLogger.error(MySecurityContext.class, "Cannot check role", e);
		}

		return false;
	}

//	public String getApp() {
//		return app;
//	}
}