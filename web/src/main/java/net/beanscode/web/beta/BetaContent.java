package net.beanscode.web.beta;

import static net.beanscode.web.BeansWebConst.MAGIC_KEY;
import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;

import java.io.IOException;

import javax.annotation.security.PermitAll;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

import org.rendersnake.HtmlCanvas;

import net.beanscode.web.BeansWebConst;
import net.beanscode.web.BetaPage;
import net.beanscode.web.view.backup.BackupBetaContent;
import net.beanscode.web.view.datamanger.BulkContent;
import net.beanscode.web.view.dataset.DatasetBetaContent;
import net.beanscode.web.view.dataset.DatasetsBetaContent;
import net.beanscode.web.view.dataset.TablesBetaContent;
import net.beanscode.web.view.help.HelpBetaContent;
import net.beanscode.web.view.notebook.NotebookContent;
import net.beanscode.web.view.notebook.NotebooksBetaContent;
import net.beanscode.web.view.notification.NotificationsBetaContent;
import net.beanscode.web.view.plugins.PluginsBetaContent;
import net.beanscode.web.view.settings.AccountSettingsBetaContent;
import net.beanscode.web.view.settings.AppOptionsBetaContent;
import net.beanscode.web.view.settings.DatabaseBetaContent;
import net.beanscode.web.view.settings.InternalsBetaContent;
import net.beanscode.web.view.settings.MyAccountBetaContent;
import net.beanscode.web.view.settings.SearchBetaContent;
import net.beanscode.web.view.settings.jobspanel.JobsBetaContent;
import net.beanscode.web.view.summary.DiagnosticsContent;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.BetaButtonGroup;
import net.beanscode.web.view.ui.SidebarMenu;
import net.beanscode.web.view.users.AllUsersBetaContent;
import net.beanscode.web.view.users.GroupsBetaContent;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpAddClass;
import net.hypki.libs5.pjf.op.OpAfter;
import net.hypki.libs5.pjf.op.OpJs;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpRemove;
import net.hypki.libs5.pjf.op.OpRemoveClass;
import net.hypki.libs5.pjf.op.OpSet;

public abstract class BetaContent extends BeansComponent {
	
	protected abstract void renderHeader(HtmlCanvas html) throws IOException;
	
	protected abstract void renderContent(HtmlCanvas html) throws IOException;
	
	protected abstract BetaButtonGroup getMenu() throws IOException;
	
	protected abstract String getMenuName() throws IOException;

	public BetaContent() {
		
	}
	
	public BetaContent(Component parent) {
		super(parent);
	}
	
	protected String getAjaxPath() {
		return null;
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		
	}
	
	@GET
	@PermitAll
	@Produces(MediaType.TEXT_HTML)
    public String init(@Context SecurityContext sc, @Context UriInfo uriInfo) throws IOException {
		return new BetaPage().init(sc, uriInfo, getClass().getSimpleName());
	}
	
	public OpList onShow() throws IOException {
		// TODO this compiles the html, make is lazy
		HtmlCanvas header = new HtmlCanvas();
//		HtmlCanvas title = new HtmlCanvas();
		HtmlCanvas content = new HtmlCanvas();
		
		renderHeader(header);
//		renderTitle(title);
		renderContent(content);
		
		HtmlCanvas menu = new HtmlCanvas();
		menu
			.div()
				.render(new BetaButtonGroup()
					.add(new Button()
							.setTooltip("Dashboard")
							.setAwesomeicon("fas fa-tachometer-alt")
							.add(new OpComponentClick(this, "onDashboardClick")))
					
					.add(new Button()
							.setTooltip("Notebooks, datasets, tables")
							.setAwesomeicon("fa fa-list")
							.addButton(new Button("Browse notebooks")
									.setAwesomeicon("fas fa-book")
									.add(new OpComponentClick(this, "onNotebooksClick")))
							.addButton(new Button("New notebok")
									.setAwesomeicon("fa fa-plus")
									.add(new OpComponentClick(NotebookContent.class, UUID.random(), "create", null)))
							.addButton(new Button()
									.setSeparator(true))
							.addButton(new Button("Browse datasets")
									.setAwesomeicon("fa fa-hdd")
									.add(new OpComponentClick(this, "onDatasetsClick")))
							.addButton(new Button("Browse tables")
									.setAwesomeicon("fa fa-table")
									.add(new OpComponentClick(this, "onTablesClick")))
							.addButton(new Button("New dataset")
									.setAwesomeicon("fa fa-plus")
									.add(new OpComponentClick(this, "onNewDatasetClick")))
							.addButton(new Button()
									.setSeparator(true))
							.addButton(new Button("Notifications")
									.setAwesomeicon("fas fa-comment")
									.add(new OpComponentClick(this, "onNotificationsClick")))
							.addButton(new Button("Bulk changes")
									.setAwesomeicon("fas fa-play")
									.add(new OpComponentClick(this, "onBulkClick")))
							)
					
					.add(new Button()
							.setTooltip("Account")
							.setAwesomeicon("fa fa-user")
							.addButton(new Button("My account")
									.setAwesomeicon("fa fa-user")
									.add(new OpComponentClick(this, "onMyAccountClick")))
							.addButton(new Button()
									.setSeparator(true))
							.addButton(new Button("Groups")
									.setAwesomeicon("fa fa-user-friends")
									.add(new OpComponentClick(this, "onGroupsClick")))
							.addButton(new Button("Settings")
									.setAwesomeicon("fa fa-cog")
									.add(new OpComponentClick(this, "onSettingsClick")))
							.addButton(new Button()
									.setSeparator(true))
							.addButton(new Button("Help")
									.setAwesomeicon("fas fa-info")
									.add(new OpComponentClick(this, "onHelpClick")))
							)
					
					.add(new Button()
							.setVisible(isAdmin())
							.setTooltip("Administration")
							.setAwesomeicon("fa fa-cog")
							.setHtmlClass("mainmenu-administration")
							.addButton(new Button("Diagnostics")
									.setAwesomeicon("fa fa-stethoscope")
									.add(new OpComponentClick(this, "onDiagnosticsClick")))
							.addButton(new Button("Users")
									.setAwesomeicon("fa fa-users")
									.add(new OpComponentClick(this, "onUsersClick")))
							.addButton(new Button("Application options")
									.setAwesomeicon("fa fa-cog")
									.add(new OpComponentClick(this, "onOptionsClick")))
							.addButton(new Button("Plugins")
									.setAwesomeicon("fa fa-plug")
									.add(new OpComponentClick(this, "onPluginsClick")))
							.addButton(new Button("Jobs")
									.setAwesomeicon("fa fa-tasks")
									.add(new OpComponentClick(this, "onJobsClick")))
							.addButton(new Button("Backup")
									.setAwesomeicon("fa fa-archive")
									.add(new OpComponentClick(this, "onBackupClick")))
							.addButton(new Button("Database")
									.setAwesomeicon("fa fa-database")
									.add(new OpComponentClick(this, "onDatabaseClick")))
							.addButton(new Button("Search index")
									.setAwesomeicon("fa fa-search")
									.add(new OpComponentClick(this, "onSearchClick")))
							.addButton(new Button("BEANS internals")
									.setAwesomeicon("fa fa-sitemap")
									.add(new OpComponentClick(this, "onInternalsClick")))
							)
					)
			._div()
			;
		
		String url = (notEmpty(getAjaxPath()) ? "/" + getAjaxPath() + (getParams().contains(BeansWebConst.MAGIC_KEY) ? "?" + BeansWebConst.MAGIC_KEY + "=" + getParams().getString(BeansWebConst.MAGIC_KEY) : ""): "/");
		
		return new OpList()
			.add(new OpSet(".beans-content-header", header.toHtml()))
			.add(new OpSet("#mainmenu", menu.toHtml()))
//			.add(new OpSet(".beans-content-title", title.toHtml()))
			.add(new OpSet(".beans-content-body", content.toHtml()))
//			.add(new OpSet(".beans-submenu-header", getMenuName()))
//			.add(new OpRemove(".beans-submenu-content"))
//			.add(new OpAfter(".beans-submenu-header", new SidebarMenu(this, getMenu()).toHtml()))
//			.add(new OpShow(".beans-submenu-content"))
			.add(new OpJs("history.pushState(undefined, "
					+ "'" + UUID.random() + "', "
					+ "'" + url + "');"))
			;
	}

	
	public OpList onMyAccountClick() throws IOException {
		return new OpList()
			.add(new OpRemoveClass(".sidebar .nav-link", "active"))
			.add(new OpAddClass(".beans-sidebar-myaccount .nav-link", "active"))
			.add(new MyAccountBetaContent(this).onShow());
	}
	
	public OpList onInternalsClick() throws IOException {
		return new OpList()
			.add(new OpRemoveClass(".sidebar .nav-link", "active"))
			.add(new OpAddClass(".beans-sidebar-internals .nav-link", "active"))
			.add(new InternalsBetaContent(this).onShow());
	}
	
	public OpList onSearchClick() throws IOException {
		return new OpList()
			.add(new OpRemoveClass(".sidebar .nav-link", "active"))
			.add(new OpAddClass(".beans-sidebar-search .nav-link", "active"))
			.add(new SearchBetaContent(this).onShow());
	}
	
	public OpList onDatabaseClick() throws IOException {
		return new OpList()
			.add(new OpRemoveClass(".sidebar .nav-link", "active"))
			.add(new OpAddClass(".beans-sidebar-database .nav-link", "active"))
			.add(new DatabaseBetaContent(this).onShow());
	}
	
	public OpList onBackupClick() throws IOException {
		return new OpList()
			.add(new OpRemoveClass(".sidebar .nav-link", "active"))
			.add(new OpAddClass(".beans-sidebar-backup .nav-link", "active"))
			.add(new BackupBetaContent(this).onShow());
	}
	
	public OpList onJobsClick() throws IOException {
		return new OpList()
			.add(new OpRemoveClass(".sidebar .nav-link", "active"))
			.add(new OpAddClass(".beans-sidebar-jobs .nav-link", "active"))
			.add(new JobsBetaContent(this).onShow());
	}
	
	public OpList onPluginsClick() throws IOException {
		return new OpList()
			.add(new OpRemoveClass(".sidebar .nav-link", "active"))
			.add(new OpAddClass(".beans-sidebar-plugins .nav-link", "active"))
			.add(new PluginsBetaContent(this).onShow());
	}
	

	public OpList onOptionsClick() throws IOException {
		return new OpList()
			.add(new OpRemoveClass(".sidebar .nav-link", "active"))
			.add(new OpAddClass(".beans-sidebar-options .nav-link", "active"))
			.add(new AppOptionsBetaContent(this).onShow());
	}
	
	public OpList onUsersClick() throws IOException {
		return new OpList()
			.add(new OpRemoveClass(".sidebar .nav-link", "active"))
			.add(new OpAddClass(".beans-sidebar-users .nav-link", "active"))
			.add(new AllUsersBetaContent(this).onShow());
	}
	
	public OpList onDiagnosticsClick() throws IOException {
		return new OpList()
			.add(new OpRemoveClass(".sidebar .nav-link", "active"))
			.add(new OpAddClass(".beans-sidebar-diagnostics .nav-link", "active"))
			.add(new DiagnosticsContent(this).onShow());
	}
	
	public OpList onGroupsClick() throws IOException {
		return new OpList()
			.add(new OpRemoveClass(".sidebar .nav-link", "active"))
			.add(new OpAddClass(".beans-sidebar-groups .nav-link", "active"))
			.add(new GroupsBetaContent(this).onShow());
	}
	
	public OpList onSettingsClick() throws IOException {
		return new OpList()
			.add(new OpRemoveClass(".sidebar .nav-link", "active"))
			.add(new OpAddClass(".beans-sidebar-settings .nav-link", "active"))
			.add(new AccountSettingsBetaContent(this).onShow());
	}
	
	public OpList onBulkClick() throws IOException {
		return new OpList()
			.add(new OpRemoveClass(".sidebar .nav-link", "active"))
			.add(new OpAddClass(".beans-sidebar-bulk .nav-link", "active"))
			.add(new BulkContent(this).onShow());
	}

	
	public OpList onHelpClick() throws IOException {
		return new OpList()
			.add(new OpRemoveClass(".sidebar .nav-link", "active"))
			.add(new OpAddClass(".beans-sidebar-help .nav-link", "active"))
			.add(new HelpBetaContent(this).onShow());
	}
	
	public OpList onNotificationsClick() throws IOException {
		return new OpList()
			.add(new OpRemoveClass(".sidebar .nav-link", "active"))
			.add(new OpAddClass(".beans-sidebar-notifications .nav-link", "active"))
			.add(new NotificationsBetaContent(this).onShow());
	}
	
	public OpList onDashboardClick() throws IOException {
		return new OpList()
			.add(new OpRemoveClass(".sidebar .nav-link", "active"))
			.add(new OpAddClass(".beans-sidebar-dashboard .nav-link", "active"))
			.add(new DashboardContent(this).onShow());
	}
	
	public OpList onNotebooksClick() throws IOException {
		return new OpList()
			.add(new OpRemoveClass(".sidebar .nav-link", "active"))
			.add(new OpAddClass(".beans-sidebar-notebooks .nav-link", "active"))
			.add(new NotebooksBetaContent(this).onShow());
	}
	
	public OpList onDatasetsClick() throws IOException {
		return new OpList()
			.add(new OpRemoveClass(".sidebar .nav-link", "active"))
			.add(new OpAddClass(".beans-sidebar-datasets .nav-link", "active"))
			.add(new DatasetsBetaContent(this).onShow());
	}
	
	public OpList onTablesClick() throws IOException {
		return new OpList()
			.add(new OpRemoveClass(".sidebar .nav-link", "active"))
			.add(new OpAddClass(".beans-sidebar-tables .nav-link", "active"))
			.add(new TablesBetaContent(this).onShow());
	}
	
	public OpList onNewDatasetClick() throws IOException, ValidationException {
		return new OpList()
			.add(new OpRemoveClass(".sidebar .nav-link", "active"))
			.add(new OpAddClass(".beans-sidebar-newdataset .nav-link", "active"))
			.add(new DatasetBetaContent(this).createDataset(getSecurityContext(), getParams()));
	}
}
