package net.beanscode.model.plugins;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.NotImplementedException;

import net.beanscode.model.connectors.ConnectorList;
import net.beanscode.model.connectors.H5Connector;
import net.hypki.libs5.db.db.Results;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.search.query.Query;

public class RewindableConnector implements Connector {
	
	private long memoryConnectorSizeBytes = 128_000_000;
	
	private List<H5Connector> h5Connectors = new ArrayList<>();
	private MemoryConnector memoryConnector = new MemoryConnector();

	@Override
	public String getName() {
		return "Connector which caches ~100 MB of rows, spills them to the disk if needed, and is "
				+ "able to rewind to the beginning of cached rows";
	}
	
	public RewindableConnector() {
		
	}
	
	public RewindableConnector(ColumnDefList connDefList) {
		setColumnDefs(connDefList);
	}
	
	public RewindableConnector(long memoryConnectorSizeBytes) {
		this.memoryConnectorSizeBytes = memoryConnectorSizeBytes;
	}

	@Override
	public ConnectorInitParams getInitParams() {
		return new ConnectorInitParams();
	}

	@Override
	public Connector setInitParams(ConnectorInitParams initParams) {
		return null;
	}

	@Override
	public Iterable<Row> iterateRows(Query filter) {
		ConnectorList connList = new ConnectorList();
		for (H5Connector conn : h5Connectors)
			connList.addConnector(conn);
		connList.addConnector(memoryConnector);
		return connList;
	}

	@Override
	public Iterable<Row> iterateRows(Query filter, int splitNr, int splitCount) {
		if (splitNr == 0)
			return iterateRows(filter);
		return null;
	}

	@Override
	public Results iterateRows(Query filter, String state, int size) {
		throw new NotImplementedException();
	}

	@Override
	public void write(Row row) throws IOException {
		memoryConnector.write(row);
		
		// check if memory connector run out of maximum capacity
		if (memoryConnector.getAproxBytesSize() > memoryConnectorSizeBytes) {
			// spill the memory connector to the disk
			H5Connector h5 = new H5Connector(UUID.random(), new File(UUID.random() + ".h5"));
			h5.setColumnDefs(getColumnDefs());
			for (Row r : memoryConnector.iterateRows(null))
				h5.write(r);
			h5.close();
			
			h5Connectors.add(h5);
			memoryConnector.clear();
		}
	}

	@Override
	public void close() throws IOException {
//		for (H5Connector h5 : h5Connectors)
//			h5.close();
	}

	@Override
	public long getAproxBytesSize() throws IOException {
		long b = 0;
		for (H5Connector h5 : h5Connectors)
			b += h5.getAproxBytesSize();
		b += memoryConnector.getAproxBytesSize();
		return b;
	}

	@Override
	public void clear() throws IOException {
		for (H5Connector h5 : h5Connectors)
			h5.clear();
		h5Connectors.clear();
		memoryConnector.clear();
	}

	@Override
	public void setColumnDefs(ColumnDefList defs) {
		memoryConnector.setColumnDefs(defs);
	}

	@Override
	public ColumnDefList getColumnDefs() {
		return memoryConnector.getColumnDefs();
	}

	@Override
	public void validate() throws IOException {
		
	}

	@Override
	public String getVersion() {
		return "0.1.0";
	}
	
//	public void rewind() {
//		
//	};
}
