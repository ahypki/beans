package net.beanscode.web.view.notification;

import java.io.IOException;

import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

import net.beanscode.cli.notification.Notification;
import net.beanscode.cli.notification.NotificationType;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.badge.BadgeError;
import net.beanscode.web.view.ui.badge.BadgeInfo;
import net.beanscode.web.view.ui.badge.BadgeSuccess;
import net.beanscode.web.view.ui.badge.BadgeWarning;
import net.hypki.libs5.pjf.components.labels.MessageType;

public class NotificationLabel extends BeansComponent {
	
	@Expose
	private Notification notification = null;

	public NotificationLabel(Notification notification) {
		this.notification = notification;
//		super(toMessageType(notification.getNotificationType()), notification.getDescription());
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		NotificationType notType = notification.getNotificationType();
		
		if (notType == NotificationType.ERROR)
			html
				.render(new BadgeError(notification.getTitle()));
		else if (notType == NotificationType.INFO)
			html
			.render(new BadgeInfo(notification.getTitle()));
		else if (notType == NotificationType.OK)
			html
				.render(new BadgeSuccess(notification.getTitle()));
		else if (notType == NotificationType.QUESTION)
			html
				.render(new BadgeInfo(notification.getTitle()));
		else if (notType == NotificationType.WARN)
			html
				.render(new BadgeWarning(notification.getTitle()));
		else
			html
				.render(new BadgeInfo(notification.getTitle()));
	}
	
	private static MessageType toMessageType(NotificationType notType) {
		if (notType == NotificationType.ERROR)
			return MessageType.DANGER;
		else if (notType == NotificationType.INFO)
			return MessageType.INFO;
		else if (notType == NotificationType.OK)
			return MessageType.SUCCESS;
		else if (notType == NotificationType.QUESTION)
			return MessageType.WARNING;
		else if (notType == NotificationType.WARN)
			return MessageType.WARNING;
		else
			return MessageType.INFO;
	}
}
