package net.beanscode.model.dataset;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.jobs.Job;

import com.google.gson.annotations.Expose;

public class TableRemoveJob extends Job {
	
	@Expose
	private UUID userIdCall = null;
	
	@Expose
	private List<UUID> tablesToRemove = null;

	public TableRemoveJob() {
		
	}
	
	public TableRemoveJob(UUID userId) {
		setUserIdCall(userId);
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		if (getUserIdCall() == null) {
			LibsLogger.error(TableRemoveJob.class, "There is no User specified who wishes to remove tables");
			return;
		}
		
		// first remove from index - to let user know that tables are removed
		for (UUID tableId : getTablesToRemove()) {
			Table table = TableFactory.getTable(tableId);
			if (table != null
					&& table.isWriteAllowed(getUserIdCall()))
				table.deindex();
		}
		
		// now remove everything slowly
		for (UUID tableId : getTablesToRemove()) {
			Table table = TableFactory.getTable(tableId);
			if (table != null
					&& table.isWriteAllowed(getUserIdCall()))
				table.remove();
		}
	}

	public List<UUID> getTablesToRemove() {
		if (tablesToRemove == null)
			tablesToRemove = new ArrayList<>();
		return tablesToRemove;
	}

	public TableRemoveJob setTablesToRemove(List<UUID> tablesToRemove) {
		this.tablesToRemove = tablesToRemove;
		return this;
	}

	public UUID getUserIdCall() {
		return userIdCall;
	}

	public void setUserIdCall(UUID userIdCall) {
		this.userIdCall = userIdCall;
	}
}
