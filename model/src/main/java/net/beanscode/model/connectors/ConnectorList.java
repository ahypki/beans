package net.beanscode.model.connectors;

import static java.lang.String.valueOf;
import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;
import static net.hypki.libs5.utils.utils.NumberUtils.toInt;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.NotImplementedException;

import com.google.gson.annotations.Expose;

import net.beanscode.model.dataset.Table;
import net.beanscode.model.plugins.ColumnDefList;
import net.beanscode.model.plugins.Connector;
import net.beanscode.model.plugins.ConnectorInitParams;
import net.hypki.libs5.db.db.Results;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.search.query.Query;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.collections.UniqueList;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.sha.SHAManager;
import net.hypki.libs5.weblibs.CacheManager;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

public class ConnectorList implements Connector, Serializable, Iterable<Row> {
	
	@Expose
	private int threadCount = 1;
	
	private static final int QUEUE_MAX_SIZE = 50000;
	
	private static final String CONNECTOR_LINKS_DESER = "connectorLinks"; 
	
	@Expose
	@NotNull
	@NotEmpty
	private List<ConnectorLink> connectorsLinks = null;
	
	private ConnectorInitParams connectorInitParams = null; 
	
	public ConnectorList() {
		
	}
	
	public ConnectorList(int threadCount) {
		setThreadCount(threadCount);
	}
	
	public ConnectorList(Connector ... connectors) {
		for (Connector connector : connectors) {
			addConnector(connector);
		}
	}
	
	public ConnectorList(Iterable<Table> tables) {
		for (Table table : tables) {
			for (Connector c : table.iterateConnectors()) {				
				addConnector(c);
			}
		}
	}
	
	public ConnectorList(Table ... tables) {
		for (Table table : tables) {
			for (Connector c : table.iterateConnectors()) {				
				addConnector(c);
			}
		}
	}
	
	public ConnectorList(List<Connector> connectors) {
		for (Connector connector : connectors) {
			addConnector(connector);
		}
	}
	
	@Override
	public void validate() throws IOException {
		// do nothing
	}
	
	@Override
	public String getVersion() {
		StringBuilder sb = new StringBuilder();
		for (ConnectorLink connectorLink : connectorsLinks) {
			sb.append(connectorLink.getConnectorInstance().getVersion());
			sb.append("; ");
		}
		return sb.toString();
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		if (connectorsLinks != null)
			for (ConnectorLink connectorLink : connectorsLinks) {
				sb.append(connectorLink + "; ");
			}
		return sb.toString();
	}
	
	public int size() {
		return getConnectorsLinks().size();
	}

	public List<ConnectorLink> getConnectorsLinks() {
		if (connectorsLinks == null)
			connectorsLinks = new ArrayList<>();
		return connectorsLinks;
	}

	private void setConnectorsLink(List<ConnectorLink> connectorsLinks) {
		this.connectorsLinks = connectorsLinks;
	}
	
	public ConnectorList addConnector(ConnectorList connector) {
		for (ConnectorLink cl : connector.getConnectorsLinks())
			addConnector(cl);
		return this;
	}
	
	public ConnectorList addConnector(Connector ... connectors) {
		if (connectors != null)
			for (Connector cl : connectors)
				addConnector(cl);
		return this;
	}
	
	public ConnectorList addConnector(Connector connector) {
		if (connector == null)
			return this;
		
		ConnectorLink cl = new ConnectorLink(connector);
		if (exists(cl)) {
			LibsLogger.warn(ConnectorList.class, "Connector " + cl + " already exists in this instance");
			return this;
		}
		getConnectorsLinks().add(cl);
		return this;
	}
	
	public ConnectorList addConnector(ConnectorLink connectorLink) {
		if (exists(connectorLink)) {
			LibsLogger.warn(ConnectorList.class, "Connector " + connectorLink + " already exists in this instance");
			return this;
		}
		getConnectorsLinks().add(connectorLink);
		return this;
	}

	private boolean exists(ConnectorLink connectorLink) {
		for (ConnectorLink cl : getConnectorsLinks()) {
			if (cl.equals(connectorLink))
				return true;
		}
		return false;
	}
	
//	@Override
//	public Results iterateRows(String state, int size) {
//		int connLinkId = 0;
//		String connState = null;
//		if (notEmpty(state)) {
//			JsonObject jo = JsonUtils.parseJson(state).getAsJsonObject();
//			connLinkId = JsonUtils.readInt(jo, "connectorLinkId", 0);
//			connState = JsonUtils.readString(jo, "connectorLinkState", null);
//		}
//		
//		Results res = new Results();
//		
//		try {
//			while (true) {
//				if (getConnectorsLinks().size() == 0)
//					break;
//				
//				Results newRows = getConnectorsLinks()
//						.get(connLinkId)
//						.getConnectorInstance()
//						.iterateRows(connState, size);
//			
//				res.getRows().addAll(newRows.getRows());
//				
//				connState = newRows.getNextPage();
//				
//				size -= newRows.size();
//				
//				if (size <= 0)
//					break;
//				
//				connLinkId++;
//				
//				if (connLinkId >= getConnectorsLinks().size())
//					break;
//			}
//		} catch (Exception e) {
//			LibsLogger.error(ConnectorList.class, "Cannot iterate rows over " + ConnectorList.class.getSimpleName(), e);
//		}
//		
//		JsonObject jo = new JsonObject();
//		jo.addProperty("connectorLinkId", connLinkId);
//		jo.addProperty("connectorLinkState", connState);
//		res.setNextPage(jo.toString());
//		return res;
//	}
	
	@Override
	public Iterator<Row> iterator() {
		return iterator(null, 0, 0);
	}
	
	@Override
	public Iterable<Row> iterateRows(Query filter, int splitNr, int splitCount) {
		return new Iterable<Row>() {
			@Override
			public Iterator<Row> iterator() {
				return ConnectorList.this.iterator(filter, splitNr, splitCount);
			}
		};
	}
	
	/**
	 * Iterates over this ConnectorList.
	 * @param filter
	 * @param splitNr
	 * @param splitCount
	 * @return
	 */
	public Iterator<Row> iterator(Query filter, int splitNr, int splitCount) {
		if (getThreadCount() > 1) {
			LibsLogger.debug(ConnectorList.class, "Iterating ", getClass().getSimpleName(), " with ", getThreadCount(), 
					" Threads");
			
			final ExecutorService es = Executors.newCachedThreadPool();
			final BlockingQueue<Row> queue = new ArrayBlockingQueue<>(QUEUE_MAX_SIZE);
			
			final Map<Integer, Integer> connDone = new HashMap<>();
			for (int i = 0; i < getConnectorsLinks().size(); i++)
				connDone.put(i, 0);
			
			return new Iterator<Row>() {
				private Row rowCached = null;
				
				private boolean allConnectorsRead = false;
				
				@Override
				public Row next() {
					try {
						if (rowCached != null) {
							Row row = rowCached;
							rowCached = null;
							return row;
						}
							
						Row row = queue.take();
						
						if (row == null) {
//							LibsLogger.debug(ConnectorList.class, "Bla ");
							row = queue.poll(1, TimeUnit.SECONDS);
							if (row != null)
								return row;
							
							// wait longer this time
							row = queue.poll(5, TimeUnit.MINUTES);
							if (row == null)
								throw new RuntimeException("Queue is empty but there should be more elements to read");
						}
						
						return row;
					} catch (InterruptedException e) {
						LibsLogger.error(ConnectorList.class, "Cannot retrieve more rows", e);
						return null;
					}
				}
				
				@Override
				public boolean hasNext() {
					try {
						if (queue.size() > 0)
							return true;
						
						rowCached = queue.poll(1, TimeUnit.SECONDS);
						if (rowCached != null)
							return true;
												
						while (true) {						
							boolean closing = false;
							
							// check if all connectors were fully read 
							if (allConnectorsRead) {
								closing = true;
							} else {
								rowCached = queue.poll(1, TimeUnit.SECONDS);
								if (rowCached != null)
									return true;
								
								// check if there are more Connectors to read
								int openedConnectors = getOpenedConnectors();
								if (openedConnectors < getThreadCount()) {
									if (openMoreConnectors(getThreadCount() - openedConnectors) == false)
										closing = true;
								}
							}
							
							if (closing) {
								if (es.isShutdown() == false)
									es.shutdown();
								
								LibsLogger.debug(ConnectorList.class, "awaitTermination");
								if (es.awaitTermination(100, TimeUnit.SECONDS)) {
									// double check is the queue is empty
									if (queue.size() > 0)
										return true;
									
									// all connectors are finished
									LibsLogger.debug(ConnectorList.class, "Finished");
									return false;
								}
									
//								}
							} else {
								rowCached = queue.poll(1, TimeUnit.SECONDS);
								if (rowCached != null)
									return true;
							}
						}
					} catch (InterruptedException e) {
						LibsLogger.error(ConnectorList.class, "Cannot read more Rows from connectors", e);
						return false;
					}
				}

				private int getOpenedConnectors() {
					int c = 0;
					for (Map.Entry<Integer, Integer> e : connDone.entrySet()) {
						if (e.getValue() == 1)
							c++;
					}
					return c;
				}
				
				private boolean allConnectorsRead() {
					for (Map.Entry<Integer, Integer> e : connDone.entrySet()) {
						if (e.getValue() < 2)
							return false;
					}
					return true;
				}
				
				private boolean openMoreConnectors(int toOpen) {
					boolean opened = false;
					
					for (int i = 0; i < getConnectorsLinks().size(); i++) {
						if (connDone.get(i) == 0) {
							connDone.put(i, 1);
							opened = true;
							final int thisConnector = i;
							
							es.execute(new Runnable() {
								@Override
								public void run() {
									try {
										for (Row row : getConnectorsLinks()
												.get(thisConnector)
												.getConnectorInstance()
												.iterateRows(filter, splitNr, splitCount))
											queue.offer(row, 1, TimeUnit.MINUTES);
									} catch (InterruptedException e) {
										LibsLogger.error(ConnectorList.class, "Cannot offer Row to queue", e);
									}
									LibsLogger.debug(ConnectorList.class, "Finished ", 
											getConnectorsLinks().get(thisConnector).getConnectorInitParams());
									connDone.put(thisConnector, 2);
									
									allConnectorsRead = allConnectorsRead();
								}
							});
							
							if (--toOpen == 0)
								return true;
						}
					}
					
					return opened;
				}
			};
		} else {
			return new Iterator<Row>() { 
				long readTotal = 0;
				
				private Iterator<ConnectorLink> connectorsIterator = getConnectorsLinks().iterator();
				private Iterator<Row> currentConnectorIter = null;
				private Connector currConn = null;
				
				@Override
				public Row next() {
					if (currentConnectorIter == null)
						hasNext();
					
					if (currentConnectorIter == null)
						return null;
					
					readTotal++;
					
					if (readTotal == 1)
						LibsLogger.debug(ConnectorList.class, "Read first row");
					
					return currentConnectorIter.next();
				}
				
				@Override
				public boolean hasNext() {
					if (currentConnectorIter != null && currentConnectorIter.hasNext())
						return true;
					
					if (currConn != null) {
						try {
							currConn.close();
						} catch (Throwable e) {
							LibsLogger.error("Cannot close Connector", e);
						}
					}
					
					while (connectorsIterator.hasNext()) {
						try {
							currConn = connectorsIterator
									.next()
									.getConnectorInstance();
							
							currentConnectorIter = currConn
									.iterateRows(filter, splitNr, splitCount)
									.iterator();
								
							if (currentConnectorIter != null && currentConnectorIter.hasNext())
								return true;
						} catch (Throwable e) {
							LibsLogger.error(ConnectorList.class, "Cannot open connector " + currConn + 
									", moving to the next one", e);
						}
					}
			
					LibsLogger.debug(ConnectorList.class, "Read total ", readTotal, " rows");
					return false;
				}
			};
		}
	}
	
	private String getSha() throws IOException {
		return SHAManager.getSHA(JsonUtils.objectToString(getConnectorsLinks()).getBytes());
	}
	
	public List<Object> getUniqueValues(String column) {
		return getUniqueValues(column, null, null);
	}
	
	/**
	 * Caution: it iterates over all rows of this table (not the cached ones!)
	 * @param column
	 * @return
	 */
	public List<Object> getUniqueValues(String column, String splitColumn, Object splitValue) {
		if (nullOrEmpty(column))
			return null;
		
		try {
			List cached = CacheManager.cacheInstance().get(List.class, "UNIQUE_VALUES", getSha() + "__" + column);
			if (cached != null && cached.size() > 0)
				return cached;
		} catch (IOException e) {
			LibsLogger.error(ConnectorList.class, "Cannot check in cache unique values", e);
		}
		
		List<Object> uniqueValues = new UniqueList<>();
		for (Row row : this) {// DataFactory.iterateRows(getKeyspace(), getDataId(), getFilter(), isDefaultIfColumnIsMissing())) {
			Object o = row.get(column);
			
			if (splitColumn != null) {
				Object s = row.get(splitColumn);
				if (s != null && ((Comparable) s).compareTo((Comparable) splitValue) == 0)
					uniqueValues.add(o);
			} else {
				uniqueValues.add(o);
			}
		}
		
		try {
			CacheManager.cacheInstance().put("UNIQUE_VALUES", getSha(), uniqueValues);
		} catch (IOException e) {
			LibsLogger.error(ConnectorList.class, "Cannot save to cache unique values", e);
		}
		
		return uniqueValues;
	}
	
//	public Iterable<Row> filter(final C3Where additionalFilter, final boolean defaultIfColumnIsMissing) {
//		return new Iterable<Row>() {
//			@Override
//			public Iterator<Row> iterator() {
//				return new Iterator<Row>() {
//					Iterator<Row> allRowsIter = ConnectorList.this.iterator(additionalFilter);
//					Row r = null;
//					
//					@Override
//					public void remove() {
//						throw new NotImplementedException();
//					}
//					
//					@Override
//					public Row next() {
//						if (r == null)
//							hasNext();
//						return r;
//					}
//					
//					@Override
//					public boolean hasNext() {
//						while (allRowsIter.hasNext()) {
//							Row row = allRowsIter.next();
//							if (additionalFilter == null || row.isFullfiled(additionalFilter, defaultIfColumnIsMissing)) {
//								r = row;
//								return true;
//							}
//						}
//						return false;
//					}
//				};
//			}
//		};
//	}
	
//	public Iterable<Row> filter(final C3Where additionalFilter) {
//		return filter(additionalFilter, false);
//	}

//	public void saveToFile(File outputFile, PlotSeries columns, C3Where filters, boolean defaultIfColumnIsMissing) throws IOException {
//		FileWriter fw = new FileWriter(outputFile, false);
//		
//		// write header in the form: # column1  column2....
//		fw.append("# ");
//		for (String col : columns.iterateColumnExpr()) {
//			fw.append(col);
//			fw.append(" \t ");
//		}
//		fw.append("\n");
		
		// write values
//		boolean [] needComputing = new boolean[columns.getColumnsSize()];
//		int i = 0;
//		for (String columnExpr : columns.iterateColumnExpr()) {
//			needComputing[i++] = (!columnExpr.matches("[\\w]+"));
//		}
//		for (Row row : this) {
//			if (filters == null || filters.size() == 0 || row.isFullfiled(filters, defaultIfColumnIsMissing)) {
//				i = 0;
//				for (String columnExpr : columns.iterateColumnExpr()) {
//					if (needComputing[i++])
//						fw.append(compute(columnExpr, columns.getParams(), row).toString());
//					else
//						fw.append(row.get(columnExpr).toString());
//					fw.append(" ");
//				}
//				fw.append("\n");
//			}
//		}
//		fw.close();
//	}
	
	public ColumnDefList getColumnDefs() {
		if (getConnectorsLinks().size() > 0) {
			for (ConnectorLink connectorLink : getConnectorsLinks()) {
				ColumnDefList columns = connectorLink.getConnectorInstance().getColumnDefs();
				if (columns != null)
					return columns;
			}
		}
		return null;
	}

	@Override
	public String getName() {
		return "ConnectorList";
	}

	@Override
	public ConnectorInitParams getInitParams() {
		return connectorInitParams;
	}

	@Override
	public Connector setInitParams(ConnectorInitParams initParams) {
		this.connectorInitParams = initParams;
		setConnectorsLink(JsonUtils.fromJson(initParams.getMeta().get(CONNECTOR_LINKS_DESER).getAsString(), ConnectorList.class).getConnectorsLinks());
		return this;
	}

//	@Override
//	public List<Row> readRows(String... pk) throws IOException {
//		List<Row> rows = new ArrayList<Row>();
//		for (ConnectorLink cl : getConnectorsLinks()) {
//			rows.addAll(cl.getConnectorInstance().readRows(pk));
//		}
//		return rows;
//	}

	@Override
	public void write(Row row) throws IOException {
		throw new NotImplementedException("Class " + this.getClass().getSimpleName() + " is not designed to be a Connector for writing");
	}
	
	public void clear() {
		getConnectorsLinks().clear();
	}

	@Override
	public void close() throws IOException {
//		throw new NotImplementedException("Class " + this.getClass().getSimpleName() + " is not designed to be a Connector for writing");
	}

	@Override
	public void setColumnDefs(ColumnDefList defs) {
		throw new NotImplementedException("Class " + this.getClass().getSimpleName() + " is not designed to be a Connector for writing");
	}

	public boolean contains(Connector connector) {
		for (ConnectorLink connectorLink : getConnectorsLinks()) {
			if (connectorLink.equals(connector))
				return true;
		}
		return false;
	}

	public int getThreadCount() {
		return threadCount;
	}

	public ConnectorList setThreadCount(int threadCount) {
		this.threadCount = threadCount;
		return this;
	}
	
	@Override
	public Iterable<Row> iterateRows(Query filter) {
		return new Iterable<Row>() {
			@Override
			public Iterator<Row> iterator() {
				return ConnectorList.this.iterator(filter, 0, 0);
			}
		};
	}
	
	@Override
	public Results iterateRows(Query filter, String state, int size) {
		int from = 0;
		if (notEmpty(state))
			from = toInt(state);
		
		int i = 0;
		Results res = new Results();
		for (Row row : this.iterateRows(filter)) {
			if (i >= from)
				res.addRow(row);
			
			i++;
			
			if (res.size() == size)
				break;
		}
		res.setNextPage(valueOf(i));
		return res;
	}

	@Override
	public long getAproxBytesSize() throws IOException {
		// TODO Auto-generated method stub
		return 0;
	}
}
