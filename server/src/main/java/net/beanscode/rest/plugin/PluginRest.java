package net.beanscode.rest.plugin;

import static net.beanscode.rest.BeansServevrConst.MAGIC_KEY;
import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;

import java.io.IOException;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import mjson.Json;
import net.beanscode.cli.web.utils.MarkdownUtils;
import net.beanscode.model.BeansSettings;
import net.beanscode.model.notebook.NotebookEntry;
import net.beanscode.model.plugins.Func;
import net.beanscode.model.plugins.Plugin;
import net.beanscode.model.plugins.PluginManager;
import net.beanscode.model.plugins.SelfTestJob;
import net.beanscode.model.rights.Rights;
import net.beanscode.rest.BeansRest;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.op.OpError;
import net.hypki.libs5.pjf.op.OpInfo;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.Watch;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.file.FileUtils;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.reflection.ReflectionException;
import net.hypki.libs5.utils.reflection.ReflectionUtility;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.url.HttpResponse;
import net.hypki.libs5.utils.url.Params;

import com.google.gson.JsonElement;

@Path("api/plugin")
public class PluginRest extends BeansRest {

	@GET
	@Path("editorClass")
	@Produces(MediaType.APPLICATION_JSON)
	public Response editorClass(@Context SecurityContext sc, 
			@QueryParam("type") String type,
			@QueryParam(MAGIC_KEY) UUID mk) throws IOException, ValidationException {
		validateUnauthorized(getUserId(sc) != null
				|| mk != null, "Permission denied");

		String editorClass = PluginManager.getEditorClass(type);
		return Response
				.ok()
				.entity(Json
						.object()
						.set("editorClass", editorClass)
						.toString())
				.build();
	}
	
	@GET
	@Path("db/list")
	@Produces(MediaType.APPLICATION_JSON)
	public Response dbList(@Context SecurityContext sc) throws IOException, ValidationException {
		validateUnauthorized(getUserId(sc) != null, "Permission denied");
		
		Json db = Json.array();
		
		for (Plugin plugin : PluginManager.iterateAllPlugins()) {
			if (plugin.getDatabaseProviderList() != null)
				for (Class clazz : plugin.getDatabaseProviderList()) {
					db.add(Json
							.object()
							.set("plugin", plugin.getName())
							.set("clazz", clazz.getName()));
				}
		}
		
		return Response
				.ok()
				.entity(db.toString())
				.build();
	}
	
	@GET
	@Path("search/list")
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchList(@Context SecurityContext sc) throws IOException, ValidationException {
		validateUnauthorized(getUserId(sc) != null, "Permission denied");
		
		Json db = Json.array();
		
		for (Plugin plugin : PluginManager.iterateAllPlugins()) {
			if (plugin.getSearchEngineProviderList() != null)
				for (Class clazz : plugin.getSearchEngineProviderList()) {
					db.add(Json
							.object()
							.set("plugin", plugin.getName())
							.set("clazz", clazz.getName()));
				}
		}
		
		return Response
				.ok()
				.entity(db.toString())
				.build();
	}
	
	@GET
	@Path("func/list")
	@Produces(MediaType.APPLICATION_JSON)
	public Response funcList(@Context SecurityContext sc, 
			@QueryParam("filter") String filter) throws IOException, ValidationException {
		validateUnauthorized(getUserId(sc) != null, "Permission denied");
		
		Json funcs = Json.array();
		
		for (Plugin plugin : PluginManager.iterateAllPlugins()) {
			if (plugin.getFuncList() != null)
				for (Func func : plugin.getFuncList()) {
					funcs.add(Json
							.object()
							.set("plugin", plugin.getName())
							.set("name", func.getName())
							.set("help", MarkdownUtils.toHtml(func.getHelp())));
				}
		}
		
		return Response
				.ok()
				.entity(funcs.toString())
				.build();
	}
	
	@GET
	@Path("entry/list")
	@Produces(MediaType.APPLICATION_JSON)
	public Response entryList(@Context SecurityContext sc, 
			@QueryParam("userId") UUID userId,
			@QueryParam("filter") String filter) throws IOException, ValidationException {
		validateUnauthorized(getUserId(sc) != null, "Permission denied");
		
		Json plugins = Json.array();
		
		for (NotebookEntry ne : PluginManager.iterateAllNotebookEntries()) {
			boolean add = false;
			
			if (notEmpty(filter)) {
				// TODO search engine
				for (String term : StringUtilities.split(filter)) {
					term = term.trim().toLowerCase();
					
					if ((ne.getName() != null && ne.getName().toLowerCase().contains(term))
							|| (ne.getType().toLowerCase().contains(term))
							|| (ne.getShortDescription() != null && ne.getShortDescription().toLowerCase().contains(term))) {
						add = true;
					}
				}
			} else
				add = true;
			
			if (add) {
				JsonElement neJson = JsonUtils.toJson(ne);
				neJson.getAsJsonObject().addProperty("description", ne.getShortDescription());
				
				plugins.add(Json.read(neJson.toString()));
			}
		}
		
//		return plugins.toString();
		return Response
				.ok()
				.entity(plugins.toString())
				.build();
	}
	
	@GET
	@Path("list")
	@Produces(MediaType.APPLICATION_JSON)
	public Response list(@Context SecurityContext sc, 
			@QueryParam("userId") UUID userId) throws IOException, ValidationException {
		validateUnauthorized(getUserId(sc) != null, "Permission denied");
		
		Json plugins = Json.array();
		
		for (Plugin plugin : PluginManager.iterateAllPlugins()) {
			plugins.add(Json
					.object()
					.set("name", plugin.getName())
					.set("version", plugin.getVersion())
					.set("description", plugin.getDescription()));
		}
		
		return Response
				.ok()
				.entity(plugins.toString())
				.build();
	}
	
	@GET
	@Path("get")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@Context SecurityContext sc, 
			@QueryParam("userId") UUID userId,
			@QueryParam("pluginName") String pluginName) throws IOException, ValidationException {
		validateUnauthorized(getUserId(sc) != null, "Permission denied");
		
		Plugin plugin = PluginManager.getPlugin(pluginName);
		
		Json onePlugin = Json
							.object()
							.set("name", plugin.getName())
							.set("version", plugin.getVersion())
							.set("description", plugin.getDescription());
		
		return Response
				.ok()
				.entity(onePlugin.toString())
				.build();
	}
	
	@GET
	@Path("panel")
	@Produces(MediaType.APPLICATION_JSON)
	public Response panel(@Context SecurityContext sc, 
			@QueryParam("userId") UUID userId,
			@QueryParam("pluginName") String pluginName) throws IOException, ValidationException {
		validateUnauthorized(getUserId(sc) != null, "Permission denied");
		
		Plugin plugin = PluginManager.getPlugin(pluginName);
		
		return Response
				.ok()
				.entity(Json
						.object()
						.set("panel", plugin.getPanel(userId))
						.toString())
				.build();
	}
	
	@POST
	@Path("enable")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed(Rights.ADMIN)
	public void enable(@Context SecurityContext sc, 
			@FormParam("userId") UUID userId,
			@FormParam("pluginName") String pluginName) throws IOException, ValidationException {
		validateUnauthorized(getUserId(sc) != null, "Permission denied");
		validateUnauthorized(isAdmin(sc), "Permission denied");
		
		Plugin plugin = PluginManager.getPlugin(pluginName);
		
		PluginManager.enablePlugin(plugin);
	}
	
	@POST
	@Path("call")
	@Produces(MediaType.APPLICATION_JSON)
	public String callMethod(@Context SecurityContext sc, 
			@FormParam("userId") UUID userId,
			@FormParam("pluginName") String pluginName,
			@FormParam("methodName") String pluginMethod,
			@FormParam("params") String params) throws IOException, ValidationException {
		validateUnauthorized(getUserId(sc) != null, "Permission denied");
		
		try {
			Plugin plugin = PluginManager.getPlugin(pluginName);
			
			if (plugin == null) {
				LibsLogger.error(PluginRest.class, "Cannot find plugin ", pluginName);
				return new OpInfo("Error", "Internal error: cannot find plugin").toString();
			}
			
			if (!PluginManager.isPluginEnabled(plugin)) {
				LibsLogger.error(PluginRest.class, "Plugin ", pluginName, " is disabled");
				return new OpInfo("Error", "Plugin " + pluginName + " is disabled").toString();
			}
			
			Params p = new Params(params);
			p.add("userId", userId.getId());
//			for (Map.Entry<String, List<String>> entry : params.entrySet()) {
//				p.add(entry.getKey(), entry.getValue());
//			}
//			p.remove("pluginName");
//			p.remove("methodName");
//			p.remove("userId");
			
//			params.add(BeansConst.LOGIN_COOKIE_NAME, getSessionBean(sc).getSessionId());
			
			LibsLogger.debug(PluginRest.class, "Running method ", pluginMethod, " for plugin ", pluginName);
			return new HttpResponse()
				.setBody(ReflectionUtility.invokeMethod(plugin, pluginMethod, new Object[]{p}, true, false, true).toString())
				.toString();
			
		} catch (ReflectionException e) {
			LibsLogger.error(PluginRest.class, "Cannot call method " + pluginName + " for plugin " + pluginMethod, e);
			return new HttpResponse()
				.setBody(new OpList()
					.add(new OpError("Error", e.getCause() != null ? e.getCause().getMessage() : e.getMessage()))
					.toString())
				.toString();
		}
	}
	
	@GET
	@Path("isEnabled")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed(Rights.ADMIN)
	public String isEnabled(@Context SecurityContext sc, 
			@QueryParam("userId") UUID userId,
			@QueryParam("pluginName") String pluginName) throws IOException, ValidationException {
		validateUnauthorized(getUserId(sc) != null, "Permission denied");
		validateUnauthorized(isAdmin(sc), "Permission denied");
		
		Plugin plugin = PluginManager.getPlugin(pluginName);
		boolean enabled = PluginManager.isPluginEnabled(plugin);
		
		return new HttpResponse()
			.set("enabled", enabled)
			.toString();
	}
	
	@POST
	@Path("disable")
	@RolesAllowed(Rights.ADMIN)
	public void disable(@Context SecurityContext sc,
			@FormParam("pluginName") String pluginName) throws IOException, ValidationException {
		validateUnauthorized(getUserId(sc) != null, "Permission denied");
		validateUnauthorized(isAdmin(sc), "Permission denied");
		
		Plugin plugin = PluginManager.getPlugin(pluginName);
		
		PluginManager.disablePlugin(plugin);
	}
	
	@POST
	@Path("reload")
	@RolesAllowed(Rights.ADMIN)
	public void reloadPlugins(@Context SecurityContext sc) throws IOException, ValidationException {
		validateUnauthorized(getUserId(sc) != null, "Permission denied");
		validateUnauthorized(isAdmin(sc), "Permission denied");
		
		PluginManager.reload();
	}
	
	@POST
	@Path("selftest/start")
	@RolesAllowed(Rights.ADMIN)
	public void selfTestStart(@Context SecurityContext sc,
			@FormParam("pluginName") String pluginName) throws IOException, ValidationException {
		validateUnauthorized(isAdmin(sc), "Permission denied");
		
		PluginManager.selfTestPluginInBackground(getLoggedUser(sc).getUserId(), pluginName);
	}
	
	@POST
	@Path("selftest/all")
	@RolesAllowed(Rights.ADMIN)
	public void selfTestStartAll(@Context SecurityContext sc) throws IOException, ValidationException {
		validateUnauthorized(isAdmin(sc), "Permission denied");
		
		PluginManager.selfTestPluginInBackground(getLoggedUser(sc).getUserId());
	}
	
	@GET
	@Path("selftest")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed(Rights.ADMIN)
	public Response selfTestGet(@Context SecurityContext sc,
			@QueryParam("pluginName") String pluginName) throws IOException, ValidationException {
		validateUnauthorized(isAdmin(sc), "Permission denied");
		
		return Response
			.ok()
			.entity(Json
					.object()
					.set("status", PluginManager.selfTestStatus(pluginName))
					.set("log", PluginManager.selfTestLog(pluginName))
					.toString()
					)
			.build();
	}
}
