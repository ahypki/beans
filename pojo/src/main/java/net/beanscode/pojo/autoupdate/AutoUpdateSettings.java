package net.beanscode.pojo.autoupdate;

import com.google.gson.annotations.Expose;

import net.sf.oval.constraint.NotNull;

public class AutoUpdateSettings {

	@Expose
	@NotNull
	private AutoUpdateOption option = null;
	
	public AutoUpdateSettings() {
		setOption(AutoUpdateOption.NO_CONFIRMATION_NEEDED);
	}

	public AutoUpdateOption getOption() {
		return option;
	}

	public AutoUpdateSettings setOption(AutoUpdateOption option) {
		this.option = option;
		return this;
	}
}
