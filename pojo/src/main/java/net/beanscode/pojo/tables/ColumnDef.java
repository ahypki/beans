package net.beanscode.pojo.tables;

import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.utils.LibsLogger;
import net.sf.oval.constraint.CheckWithCheck.SimpleCheck;

import org.apache.commons.lang.NotImplementedException;
//import org.apache.pig.data.DataType;

import com.google.gson.annotations.Expose;

public class ColumnDef implements SimpleCheck {
	
	@Expose
	private String name = null;
	
	@Expose
	private String description = null;
	
	@Expose
	private ColumnType type = null;
	
	public ColumnDef() {

//		@NotEmpt
	}
	
	public ColumnDef(String name, String desc, ColumnType type) {
		setName(name);
		setDescription(desc);
		setType(type);
	}
	
	public ColumnDef(String name) {
		setName(name);
		setType(ColumnType.UNKNOWN);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ColumnDef) {
			ColumnDef colDef = (ColumnDef) obj;
			return colDef.getName().equals(this.getName());
		}
		return false;
	}
	
	@Override
	public String toString() {
		return String.format("%s %s%s", getName(), getType(), getDescription() != null ? " (" + getDescription() + ")" : "");
	}
	
	/**
	 * Valid name contains only letters, digits and underscore.
	 * @param columnName
	 * @return
	 */
	public static boolean isColumnNameValid(String columnName) {
		boolean valid = columnName.matches("^[\\w\\d\\_]+$");
		
		if (!valid)
			LibsLogger.error(ColumnDef.class, "Column name ", columnName, " is invalid");
		
		return valid;
	}
	
	@Override
	public boolean isSatisfied(Object validatedObject, Object value) {
		return isColumnNameValid(value.toString());
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ColumnType getType() {
		return type;
	}
	
//	public byte getPigType() {
//		if (getType() == ColumnType.LONG)
//			return DataType.LONG;
//		else if (getType() == ColumnType.STRING)
//			return DataType.CHARARRAY;
//		else if (getType() == ColumnType.DOUBLE)
//			return DataType.DOUBLE;
//		else if (getType() == ColumnType.INTEGER)
//			return DataType.INTEGER;
//		else
//			throw new NotImplementedException();
//	}

	public void setType(ColumnType type) {
		this.type = type;
	}
}
