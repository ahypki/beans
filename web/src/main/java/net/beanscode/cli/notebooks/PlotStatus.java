package net.beanscode.cli.notebooks;

import java.util.HashMap;

import net.hypki.libs5.utils.string.StringUtilities;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class PlotStatus {
	
	@Expose
	@NotNull
	@NotEmpty
	private boolean started = false;
	
	@Expose
	@NotNull
	@NotEmpty
	private boolean finished = false;
	
	@Expose
	@NotNull
	@NotEmpty
	private String errorMsg = null;
	
	@Expose
	@NotNull
	@NotEmpty
	private int maxPlots = Integer.MAX_VALUE;

	public PlotStatus() {
		
	}
	
	public boolean isFinishedWithErrors() {
		return isFinished() && StringUtilities.notEmpty(getErrorMsg());
	}
	
	public int getMaxPlots() {
		return maxPlots;
	}

	public void setMaxPlots(int maxPlots) {
		this.maxPlots = maxPlots;
	}
	
	public boolean isMaxPlotsSet() {
		return !(getMaxPlots() == Integer.MAX_VALUE);
	}

	public boolean isStarted() {
		return started;
	}

	public void setStarted(boolean started) {
		this.started = started;
		setFinished(false);
		setErrorMsg(null);
	}

	public boolean isFinished() {
		return finished;
	}

	public void setFinished(boolean finished) {
		this.finished = finished;
		setErrorMsg(null);
	}
	
	public void setFinished(boolean finished, String errorMsg) {
		this.finished = finished;
		setErrorMsg(errorMsg);
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
}
