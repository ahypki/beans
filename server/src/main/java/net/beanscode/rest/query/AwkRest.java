package net.beanscode.rest.query;

import java.io.IOException;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;

import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.notebook.NotebookEntry;
import net.beanscode.model.notebook.NotebookEntryFactory;
import net.beanscode.rest.BeansRest;
import net.hypki.libs5.db.db.weblibs.ValidationException;

@Path("api/awk")
@Deprecated // TODO remove this class
public class AwkRest extends BeansRest {
	
	@POST
	@Path("edit")
	public void edit(@Context SecurityContext sc,
			@FormParam("awkId") String awkId,
			@FormParam("name") String name,
			@FormParam("script") String script,
			@FormParam("dsQuery") String dsQuery,
			@FormParam("tbQuery") String tbQuery,
			@FormParam("outTable") String outTable) throws IOException, ValidationException {
		NotebookEntry awk = NotebookEntryFactory.getNotebookEntry(awkId);
		
		final Notebook note = awk.getNotebook();
		
		validateNotFound(note != null, "Notebook is not found");
		validateUnauthorized(isAdmin(sc) || note.isWriteAllowed(getUserId(sc)), "Permission denied");
		
		awk.setMeta("META_SCRIPT", script);
		awk.setMeta("META_DS_QUERY", dsQuery);
		awk.setMeta("META_TB_QUERY", tbQuery);
		awk.setName(name);
		awk.setMeta("META_OUT_TABLE", outTable);
		
		awk.save();
		
		awk.start();
	}
}
