package net.beanscode.model.backup;

public enum RestoreStrategy {
	CREATE_OBJECTS_IF_NOT_EXIST,
	OVERRIDE_OBJECTS,
	CLONE_OBJECTS
}
