package net.beanscode.cli.plugin;

import com.google.gson.annotations.Expose;

public class FuncPOJO {
	@Expose
	private String plugin = null;
	
	@Expose
	private String name = null;
	
	@Expose
	private String help = null;
	
	public FuncPOJO() {
		
	}

	public String getPlugin() {
		return plugin;
	}

	public void setPlugin(String plugin) {
		this.plugin = plugin;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHelp() {
		return help;
	}

	public void setHelp(String help) {
		this.help = help;
	}

}
