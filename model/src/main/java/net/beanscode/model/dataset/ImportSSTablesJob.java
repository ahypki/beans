package net.beanscode.model.dataset;

import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.File;
import java.io.IOException;

import com.datastax.driver.core.Host;
import com.google.gson.annotations.Expose;

import jodd.io.FileUtil;
import net.beanscode.model.BeansChannels;
import net.beanscode.model.BeansDbManager;
import net.hypki.libs5.db.cassandra.BulkLoaderNoExit;
import net.hypki.libs5.db.cassandra.JmxBulkLoader;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.weblibs.Settings;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.Watch;
import net.hypki.libs5.weblibs.jobs.Job;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

public class ImportSSTablesJob extends Job {
	
	@Expose
	@NotNull
	@NotEmpty
	private String pathToImport = null;
	
	@Expose
	@NotNull
	@NotEmpty
	private boolean useJMX = false;
	
	@Expose
	@NotNull
	@NotEmpty
	private boolean useSSBulkLoader = true;

	public ImportSSTablesJob() {
		super(BeansChannels.CHANNEL_LONG_NORMAL);
	}
	
	public ImportSSTablesJob(final String path, boolean useJMX) {
		super(BeansChannels.CHANNEL_LONG_NORMAL);
		setPathToImport(path);
		setUseJMX(useJMX);
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		assertTrue(isUseJMX() || isUseSSBulkLoader(), "Either JMX or SSBulkLoader was selected as a method to import files");
		assertTrue(isUseJMX() == false || isUseSSBulkLoader() == false, "Only one option (JMX or SSBulkLoader) can be selected as a method to import files");
		
		if (!new File(getPathToImport()).exists()) {
			LibsLogger.error(ImportSSTablesJob.class, "Folder ", getPathToImport(), " does not exist, cannot import files to BEANS");
			return;
		}
		
		try {
			LibsLogger.debug(ImportSSTablesJob.class, "Importing tables from ", getPathToImport(), " into database using JMX ", isUseJMX(), " | SSLoader ", isUseSSBulkLoader());
			
			// settings files is more important in determining the way of importing
			String bulkMethod = Settings.getString("Cassandra/bulkMethod", null);
			
			boolean jmx = bulkMethod != null && bulkMethod.equals("jmx");
			boolean ss = bulkMethod != null && bulkMethod.equals("ss");
			
			if (jmx == false && ss == false) {
				jmx = isUseJMX();
				ss = isUseSSBulkLoader();
			}
			
			// 1. JMX solution, by default only for localhost
			if (jmx) {
				Watch jmxtime = new Watch();
				JmxBulkLoader.main(new String[] {"--path", getPathToImport() + "/",
						"--host", BeansDbManager.getCassandraProvider().getCassandraHost(), 
						"--port", String.valueOf(Settings.getInt("Cassandra/jmxPort", 7199))});
				LibsLogger.debug(ImportSSTablesJob.class, "JMX import done in ", jmxtime.toString());
			}
			
			// 2. this solution works too but it seems it takes the same time as with JMX
			else if (ss) {
				StringBuilder hosts = new StringBuilder();
				
				for (Host host : BeansDbManager.getCassandraProvider().getCluster().getMetadata().getAllHosts()) {
					if (hosts.length() > 0)
						hosts.append(",");
					hosts.append(host.getAddress().getHostName());
				}
				
				LibsLogger.debug(ImportSSTablesJob.class, "Cass hosts: ", hosts.toString());
			
				Watch loadertime = new Watch();
				BulkLoaderNoExit.main(new String[] {
						"-v",
						"-q", // no-progress == quiet mode
						"-d", hosts.toString(),//BeansDbManager.getCassandraProvider().getCassandraHost(),
						"-p", String.valueOf(BeansDbManager.getCassandraProvider().getCassandraRpcPort()),
						"--conf-path", BeansDbManager.getCassandraProvider().getCassandraHome() + "/conf/cassandra.yaml",
						getPathToImport()
				});
				LibsLogger.debug(ImportSSTablesJob.class, "SSTableLoader import done in ", loadertime.toString());
			}
			
			LibsLogger.debug(ImportSSTablesJob.class, "Deleting folder from bulk import: " + new File(getPathToImport()).getParentFile().getParentFile());
			FileUtil.deleteDir(new File(getPathToImport()).getParentFile().getParentFile());
		} catch (Exception e) {
			throw new IOException("Cannot import files from "+ getPathToImport() + " into BEANS", e);
		}
	}

	public String getPathToImport() {
		return pathToImport;
	}

	public void setPathToImport(String pathToImport) {
		this.pathToImport = pathToImport;
	}

	public boolean isUseJMX() {
		return useJMX;
	}

	public void setUseJMX(boolean useJMX) {
		this.useJMX = useJMX;
		this.useSSBulkLoader = !useJMX;
	}

	public boolean isUseSSBulkLoader() {
		return useSSBulkLoader;
	}

	public void setUseSSBulkLoader(boolean useSSBulkLoader) {
		this.useSSBulkLoader = useSSBulkLoader;
		this.useJMX = !useSSBulkLoader;
	}
}
