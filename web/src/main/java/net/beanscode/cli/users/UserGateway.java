package net.beanscode.cli.users;

import java.io.IOException;

import net.beanscode.cli.api.ApiCallCommand;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.jersey.SessionBean;

public class UserGateway {

	public static User getUser(SessionBean sb, UUID userId) throws CLIException, IOException {
		return new ApiCallCommand(sb)
			.setApi("/api/user/get")
			.addParam("userId", userId)
			.run()
			.getResponse()
			.getAsObject(User.class);
	}
}
