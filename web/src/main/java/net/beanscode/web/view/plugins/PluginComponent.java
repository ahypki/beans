package net.beanscode.web.view.plugins;

import java.io.IOException;

import javax.ws.rs.core.SecurityContext;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.plugin.PluginCallMethod;
import net.beanscode.cli.plugin.PluginPOJO;
import net.beanscode.web.view.ui.BeansComponent;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.utils.url.Params;

import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

public class PluginComponent extends BeansComponent {
	
	@Expose
	private PluginPOJO plugin = null;
	
	public PluginComponent() {
		
	}
	
	public PluginComponent(BeansComponent parent, PluginPOJO plugin) {
		super(parent);
		setPlugin(plugin);
	}
	
//	@Override
//	public String getContent() throws IOException {
//		return plugin.getPanel();
//	}
//	
//	@Override
//	public String getTitle() throws IOException {
//		return plugin.getName();
//	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		String htmlPanel = new ApiCallCommand(this)
			.setApi("api/plugin/panel")
			.addParam("userId", getUserId())
			.addParam("pluginName", getPlugin().getName())
			.run()
			.getResponse()
			.getAsString("panel");
					//new PluginGetPanel(getSessionBean()).run(getUserEmail(), getPlugin().getName());
		html.write(htmlPanel, false);
	}
	
	public OpList runPluginMethod(Params params, SecurityContext sc) throws CLIException, IOException {
		final UUID userId = new UUID(params.getString("userId"));
		final String pluginClass = params.getString("pluginClass");
		final String pluginMethod = params.getString("pluginMethod");
		
		Params other = new Params();
		for (String key : params.keys()) {
			if (key.equals("userId") || key.equals("pluginClass") || key.equals("pluginMethod")
					|| key.equals("componentClass") || key.equals("method"))
				continue;
			other.add(key, params.get(key));
		}
		
		return new PluginCallMethod(getSessionBean()).run(getUserEmail(), pluginClass, pluginMethod, other.toString(true)); 
		
	}

	public PluginPOJO getPlugin() {
		return plugin;
	}

	public void setPlugin(PluginPOJO plugin) {
		this.plugin = plugin;
	}
}
