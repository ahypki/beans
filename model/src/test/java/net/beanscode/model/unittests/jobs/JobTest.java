package net.beanscode.model.unittests.jobs;

import java.io.IOException;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.jobs.Job;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class JobTest extends Job {
	
	@Expose
	@NotNull
	@NotEmpty
	private int test = 0;

	public JobTest() {
		
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		LibsLogger.debug(JobTest.class, "Running JobTest ", getTest());
	}

	public int getTest() {
		return test;
	}

	public JobTest setTest(int test) {
		this.test = test;
		return this;
	}
}
