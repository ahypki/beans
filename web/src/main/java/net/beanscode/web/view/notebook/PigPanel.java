package net.beanscode.web.view.notebook;

import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;
import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.id;
import static org.rendersnake.HtmlAttributesFactory.name;
import static org.rendersnake.HtmlAttributesFactory.type;

import java.io.IOException;
import java.util.regex.Pattern;

import javax.ws.rs.core.SecurityContext;

import org.rendersnake.HtmlCanvas;
import org.rendersnake.Renderable;

import ch.qos.logback.classic.pattern.ExtendedThrowableProxyConverter;
import net.beanscode.cli.BeansCliCommand;
import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.datasets.Table;
import net.beanscode.cli.notebooks.PigScript;
import net.beanscode.cli.notification.Notification;
import net.beanscode.cli.users.User;
import net.beanscode.pojo.NotebookEntryProgress;
import net.beanscode.web.BeansAPI;
import net.beanscode.web.BeansWebConst;
import net.beanscode.web.view.dataset.TableDataViewPanel;
import net.beanscode.web.view.ui.Link;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.beanscode.web.view.ui.modals.OpModalQuestion;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.HtmlSpecial;
import net.hypki.libs5.pjf.components.ComboComponent;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.components.ops.OpComponentTimer;
import net.hypki.libs5.pjf.op.OpAddClass;
import net.hypki.libs5.pjf.op.OpHide;
import net.hypki.libs5.pjf.op.OpJs;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpRemoveClass;
import net.hypki.libs5.pjf.op.OpReplace;
import net.hypki.libs5.pjf.op.OpSet;
import net.hypki.libs5.pjf.op.OpShow;
import net.hypki.libs5.pjf.op.OpToggle;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.api.APICallType;
import net.hypki.libs5.utils.string.RegexUtils;
import net.hypki.libs5.utils.url.Params;

public class PigPanel extends NotebookEntryEditorPanel {
	
	private final int REFRESH_TABLES_MS = 2000;
	private final int REFRESH_AUTOSAVE_MS = 5000;
		
	public PigPanel() {
		
	}

	public PigPanel(Component parent) {
		super(parent);
	}

	@Override
	public Renderable getView() {
//		if (getNotificationEntry() == null || getNotificationEntry().isDone() == false)
//			getInitOpList().add(new OpComponentTimer(this, "onRefresh", BeansWebConst.PIG_AUTOREFRESH_INTERVAL_MS, new Params().add("entryId", getNotebookEntry().getId().getId())));
		
		return new Renderable() {
			@Override
			public void renderOn(HtmlCanvas html) throws IOException {
				html
					.span(class_("name"))
						.content(getPigScript().getName());
				
				html
					.div(class_("notebooktable-view")
							.add("style", "display: none;"))
						.content("No tables selected yet")
					;
			}
		};
	}

	@Override
	protected Renderable getEditor() {
		return new Renderable() {
			@Override
			public void renderOn(HtmlCanvas html) throws IOException {
				ApiCallCommand call = new ApiCallCommand(PigPanel.this)
					.setApi("api/setting")
					.setApiCallType(APICallType.GET)
					.addParam("id", "PIG_LOCAL_ENABLED")
//					.addParam("userEmail", "")
					.addParam("name", "enabled")
					.run();
				
				boolean localPig = call.getResponse().getAsBoolean("value");
				
				html
					.input(name("query-name")
							.class_("query-name form-control")
							.type("text")
							.value(getPigScript().getName())
							.add("placeholder", "Query name"))
					
					.span()
						.content("Pig mode ")
					.render(new ComboComponent(getId() + "-pig-mode")
							.setAdditionalCssClass(" beans-pig-mode")
							.addValue(PigScript.PIG_MODE_TEZ.toUpperCase())
							.addValue(PigScript.PIG_MODE_MAPREDUCE.toUpperCase())
							.addValue(PigScript.PIG_MODE_TEZ_LOCAL.toUpperCase())
							.addValue(localPig, PigScript.PIG_MODE_LOCAL.toUpperCase())
							.setSelectedValue(getPigScript().getPigMode())
							)
					
					// draft
					.write(HtmlSpecial.BULLET, false)
					.span(class_("draft-info"))
						.content(" Auto-save in 5 seconds...")
					
					.input(type("hidden")
							.name(getId() + "-query-text")
							.id(getId() + "-query-text")
							.value(" "))
							
					.textarea(id(getId() + "-query"))
						.content(getPigScript().isDraftAvailable() ? getPigScript().getDraft() : getPigScript().getQuery())
						
					.write(new OpJs("window.myCodeMirror" + getId() + " = CodeMirror.fromTextArea(document.getElementById(\"" + getId() + "-query\"), "
							+ "{ "
//							+ "	value: \"function myScript(){return 100;}\", "
							+ "	mode:  \"text/x-pig\", "
							+ "	indentUnit: 4,"
							+ "	lineNumbers: true "
							+ "});").toStringAdhock(), false)
					
					.write(new OpComponentClick(PigPanel.this, 
							"onUpdatePigScript", 
							new Params()
								.add("entryId", getEntryId())).toStringAdhock(), 
							false)
					
					.div(class_("will-read-from"))
						.content("")
//					._div()	
							
//					.write(new OpPrepend("#" + getId() + ".play", html))
					;
			}
		};
	}
	
	private OpList onUpdatePigScript() {
//		String pigScript = 
		
		return new OpList()
				.add(new OpJs("document.getElementById(\"" + getId() + "-query-text\").value = "
						+ "window.myCodeMirror" + getId() + ".getValue(); "))
				.add(new OpComponentClick(this, "onUpdateReadTables", new Params().add("entryId", getEntryId())));
	}
	
	private OpList onUpdateReadTables() throws CLIException, IOException {
		StringBuilder sb = new StringBuilder();
		
		try {
			String pig = getParams().getString(getId() + "-query-text");
			
			// if entry is not in entry mode, exit
			if (pig == null)
				return new OpList();
			
			// if the draft did not change, exit
			if (getPigScript() != null
					&& getPigScript().getDraft() != null
					&& getPigScript().getDraft().equals(pig))
				return new OpList()
					.add(new OpSet("#" + getId() + " .draft-info", " Auto-save every 5 seconds..."))
					.add(new OpComponentTimer(this, "onUpdatePigScript", REFRESH_TABLES_MS, new Params().add("entryId", getEntryId())));
			
			// save new draft
			getPigScript().setDraft(pig);
			new ApiCallCommand(this)
				.setApi(BeansCliCommand.API_NOTEBOOK_ENTRY)
				.addParam("entryId", getEntryId())
				.addParam("name", getPigScript().getName())
				.addParam(PigScript.META_DRAFT, pig)
				.run();
			
			for (String g : RegexUtils.allGroups("(load[\\s]+'.*'[\\s]+using[\\s]+BeansTable)", 
					pig, 
					Pattern.CASE_INSENSITIVE)) {
				ApiCallCommand api = new ApiCallCommand(this)
						.setApi(BeansAPI.API_TABLE_SEARCH)
						.addParam("datasetQuery", RegexUtils.firstGroup("datasets[\\s]*=[\\s]*\"([^\"]*)\"", g, Pattern.CASE_INSENSITIVE))
						.addParam("tableQuery", RegexUtils.firstGroup("tables[\\s]*=[\\s]*\"([^\\\"]*)\"", g, Pattern.CASE_INSENSITIVE))
						.addParam("size", "" + 5)
						.run();
				
				long maxHits = api.getResponse().getAsLong("maxHits");
				sb.append("<b>" + g + "</b> will read from " + "<a class=\"beans-link\">" + maxHits + "</a>" + "tables: ");
				for (Table t : api.getResponse().getAsList("tables", Table.class))
					sb.append("<a class=\"beans-link\">" + 
								t.getName() + " (" + t.getDataset(this.getSessionBean()).getName() + ")</a>");
				if (maxHits > 5)
					sb.append("<a class=\"beans-link\">...</a><br/>");
			}
		} catch (Throwable t) {
			LibsLogger.error(PigPanel.class, "Cannot adhoc check which tables will be read", t);
		}
		
		return new OpList()
				.add(new OpSet("#" + getId() + " .will-read-from", sb.toString()))
				.add(new OpSet("#" + getId() + " .draft-info", " Auto-saved successfully"))
				.add(new OpComponentTimer(this, "onUpdatePigScript", REFRESH_TABLES_MS, new Params().add("entryId", getEntryId())));
	}
		
	@Override
	protected OpList onBeforeCancel() {
		return onBeforePlay();
	};
	
	@Override
	protected OpList onBeforePlay() {
//		if (getPigScript().getStatus() == NotebookEntryStatus.RUNNING) {
//			return new OpList()
//					.add(new OpBeansWarning(getPigScript().getName() + " is " + getPigScript().getStatus() + " - stop it first"));
//		}
		
		OpList ops = new OpList();
		
		ops.add(new OpJs("document.getElementById(\"" + getId() + "-query-text\").value = "
				+ "window.myCodeMirror" + getId() + ".getValue(); "));
		
		return ops;
	}
	
	@Override
	protected Button getMenu() throws IOException {
		return new Button()
			.addButton(new Button()
				.setAwesomeicon("fa fa-save")
				.setHtmlClass("save-only")
				.setVisible(false)
//				.setAwesomeicon("fa fa-save")
				.setTooltip("Save only the query (do not run)")
				.add(onBeforePlay())
				.add(new OpComponentClick(this, "onSaveOnly", new Params().add("entryId", getPigScript().getId())))
			)
			.addButton(new Button()
				.setAwesomeicon("fa fa-cog")
				.setTooltip("Properties")
				.add(new OpComponentClick(this, "onProperties",
						new Params()
							.add("entryId", getNotebookEntry().getId().getId())))
//				.setOpList(new OpList()
//								.add(new OpDialogMsg("Pig query - properties", getPigPanelProperties().toHtml()))
//				)
					)
			.addButton(new Button()
				.setHtmlClass("refresh")
				.setAwesomeicon("fa fa-sync")
				.setTooltip("Refresh query status")
				.setStopPropagation(true)
//				.setVisible(getQueryStatus() != null && !getQueryStatus().isDone())
				.add(new OpComponentClick(PigPanel.this, "onRefresh", 
						new Params()
							.add("entryId", getNotebookEntry().getId().getId())))
						)
			.addButton(new Button()
				.setAwesomeicon("fa fa-table")
//				.setVisible(getNotificationEntry() != null && getNotificationEntry().isDone())
				.setHtmlClass("pig-panel-tables pig-panel-tables-show")
				.setTooltip("Show tables")
//				.add(new OpToggle("#" + getId() + " .pig-panel-table-button"))
//				.add(new OpToggle("#" + getId() + " .pig-panel-table-previous"))
//				.add(new OpToggle("#" + getId() + " .pig-panel-table-next"))
				.add(new OpToggle("#" + getId() + " .notebooktable-view"))
				.add(new OpComponentClick(PigPanel.this, "onShowTables", 
						new Params()
							.add("entryId", getNotebookEntry().getId().getId())
							.add("tableIndex", 0)))
				)
//			.addButton(new Button()
//				.setGlyphicon("chevron-left")
//				.setVisible(false)
//				.setHtmlClass("pig-panel-tables pig-panel-table-previous")
//				.add(new OpComponentClick(PigPanel.this, "showTable", 
//						new Params()
//							.add("entryId", getNotebookEntry().getId().getId())
//							.add("tableIndex", 0)))
//						)
//			.addButton(new Button()
//				.setGlyphicon("chevron-right")
//				.setVisible(false)
//				.setHtmlClass("pig-panel-tables pig-panel-table-next")
//				.add(new OpComponentClick(PigPanel.this, "showTable", 
//						new Params()
//							.add("entryId", getNotebookEntry().getId().getId())
//							.add("tableIndex", 0)))
//						)
				
			.addButton(new Button()
				.setAwesomeicon("fa fa-stop")
				.setTooltip("Stops the script if it is running")
				.add(new OpComponentClick(this, 
						"onStopQuestion",
						new Params()
							.add("entryId", getNotebookEntry().getId().getId())))
				)
				
			;
	}
	
	private OpList onProperties() {
		
		return new OpList()
			.add(new OpSet("#" + getId() + " .content", getPigPanelProperties().toHtml()));
	}
	
	private OpList onStopQuestion() throws CLIException, IOException {
		return new OpList()
				.add(new OpModalQuestion("Stopping the script", 
						"Do you want to stop the script <b>" + getPigScript().getName() + "</b>?", 
						new OpComponentClick(this, 
								"onStop",
								new Params().add("entryId", getPigScript().getId()))));
	}
	
	private OpList onStop() throws CLIException, IOException {
		String entryId = getParams().getString("entryId");
		
		new ApiCallCommand(this)
			.setApi("api/notebook/entry/stop")
			.addParam("entryId", entryId)
			.run();
		
		return new OpList()
			.add(new OpBeansInfo("Stopping script..."));
	}
	
	private HtmlCanvas getPigPanelProperties() {
		try {
			User user = new ApiCallCommand(getSessionBean())
							.setApi("api/user/get")
							.addParam("userId", getPigScript().getUserId().getId())
							.run()
							.getResponse()
							.getAsObject(User.class);
			
			return new HtmlCanvas()
				.dl(class_("row"))
					.dt(class_("col-sm-3"))
						.content("ID")
					.dd(class_("col-sm-9"))
						.content(getPigScript().getId().getId())
					.dt(class_("col-sm-3"))
						.content("Owner")
					.dd(class_("col-sm-9"))
						.content(user != null ? user.getEmail() : "")
					.dt(class_("col-sm-3"))
						.content("Creation date")
					.dd(class_("col-sm-9"))
						.content(getPigScript().getCreate().toStringHuman())
				._dl();
		} catch (IOException e) {
			LibsLogger.error(PigPanel.class, "Cannot render PigPanel properties panel", e);
			return new HtmlCanvas();
		}
	}

	public OpList onSaveOnly(SecurityContext sc, Params params) throws IOException {
		try {
			final String name = params.getString("query-name");
			final String query = params.getString(getId() + "-query-text");
			final String queryId = params.getString("entryId");
			
			new ApiCallCommand(this)
				.setApi(BeansCliCommand.API_NOTEBOOK_ENTRY)
				.addParam("entryId", queryId)
				.addParam("name", name)
				.addParam("META_QUERY", query)
				.run();
		} catch (IOException e) {
			LibsLogger.error(PigPanel.class, "Cannot save pig script");
		}
		
		return new OpList()
			.add(new OpShow("#" + getId() + " .edit"))
			.add(new OpHide("#" + getId() + " .play"))
			.add(new OpComponentClick(this, "stopClick", params));
	}
	
	@Override
	protected OpList opOnPlay(SecurityContext sc, Params params) throws IOException {
		try {
			String name = params.getString("query-name");
			final String query = params.getString(getId() + "-query-text");
			final String pigMode = params.getString(getId() + "-pig-mode");
			final String queryId = params.getString("entryId");
			
			// setting default name
			if (nullOrEmpty(name))
				name = "no name";
			
			// save entry
			new ApiCallCommand(this)
				.setApi(BeansCliCommand.API_NOTEBOOK_ENTRY)
				.setApiCallType(APICallType.POST)
				.addParam("entryId", queryId)
				.addParam("name", name)
				.addParam("META_MODE", pigMode)
				.addParam("META_QUERY", query)
				.run();
			
			// start entry
			new ApiCallCommand(getSessionBean())
				.setApi("api/notebook/entry/start")
				.addParam("entryId", queryId)
				.run();
			
			return new OpList()
				.add(new OpHide("#" + getId() + " .save-only"))
				.add(new OpHide("#" + getId() + " .pig-panel-tables"))
				.add(new OpShow("#" + getId() + " .refresh"))
				.add(new OpComponentTimer(this, "onRefresh", REFRESH_TABLES_MS, new Params().add("entryId", getNotebookEntry().getId().getId())))
				;
		} catch (Exception e) {
			throw new IOException("Cannot start Pig script: " + e.getMessage());
//			LibsLogger.error(TextPanel.class, "Cannot start Pig script", e);
//			return new OpList()
//				.add(new OpBeansError("Cannot start Pig script: " + e.getMessage()));
		}
	}
	
	@Override
	protected OpList opOnEdit(SecurityContext sc, Params params) throws IOException {
		return new OpList()
			.add(new OpShow("#" + getId() + " .save-only"))
			.add(new OpHide("#" + getId() + " .pig-panel-tables"));
	}
	
	@Override
	protected OpList opOnStop(SecurityContext sc, Params params) throws IOException {
//		final String query = params.getString(getId() + "-query-text");
		try {
			getPigScript()
				.clearDraft();
			new ApiCallCommand(this)
				.setApi(BeansCliCommand.API_NOTEBOOK_ENTRY)
				.addParam("entryId", getEntryId())
				.addParam("name", getPigScript().getName())
				.addParam(PigScript.META_DRAFT, null)
				.run();
		} catch (CLIException | IOException e) {
			LibsLogger.error(PigPanel.class, "Cannot revert draft", e);
		}
		
		return new OpList()
			.add(new OpHide("#" + getId() + " .save-only"))
			.add(new OpShow("#" + getId() + " .pig-panel-tables"))
			.add(new OpHide("#" + getId() + " .notebooktable-view"));
	}

	@Override
	protected OpList opOnRemove(SecurityContext sc, Params params) throws IOException {
		try {
			new ApiCallCommand(this)
				.setApi("api/notebook/entry/remove")
				.setApiCallType(APICallType.DELETE)
				.addParam("entryId", params.getString("entryId"))
				.run();
		} catch (IOException e) {
			LibsLogger.error(PigPanel.class, "Cannot remove Notebook Entry", e);
		}
		
		return new OpList();
	}
	
//	@Override
//	public OpList getInitOpList() {
//		return super.getInitOpList()
//				.add(new OpComponentClick(this, "refresh", new Params().add("plotId", getPlot().getId())));
//	}
	
	public OpList onShowTable() throws IOException {
		final String tableId = getParams().getString("tableId", null);
		
		if (nullOrEmpty(tableId))
			return null;
		
		HtmlCanvas notebookView = new HtmlCanvas();
		notebookView
			.div(class_("notebooktable-view"))
//				.render(nextPrevButtons)
				.render(new TableDataViewPanel(this, new UUID(tableId))
						.setPerPage(10)
						.setPerPageVisible(false))
			._div();
		
		return new OpList()
			.add(new OpReplace("#" + getId() + " .notebooktable-view", notebookView.toHtml()))
			;
	}
	
	public OpList onShowTables() throws IOException {
//		final String entryId = getParams().getString("entryId");
//		final String tableId = getParams().getString("tableId", null);
//		
//		Table table = new TableGetByIndex(getSessionBean()).run(getNotebookEntry().getNotebookId(), 
//				getNotebookEntry().getId(), tableIndex);
//		boolean hasMore = table != null;
//				
//		if (table == null) {
//			LibsLogger.debug(PigPanel.class, "No more tables for pig query ", getPigScript().getId());
//			return new OpList()
//				.add(new OpDisable("#" + getId() + " .next-table"))
////				.add(new OpBeansInfo("No more tables in that Pig query"))
//				;
//		}
//		
//		final int previousIndex = tableIndex > 0 ? tableIndex - 1 : 0;
//		final int nextIndex = hasMore ? tableIndex + 1 : tableIndex;
//		
//		ButtonGroup nextPrevButtons = new ButtonGroup()
//			.setAdditionalCssClasses("tables-buttons")
//			.addButton(new Button("Previous Table")
//					.setHtmlClass("prev-table")
//					.setEnabled(tableIndex > 0)
//					.add(new OpComponentClick(this, "onShowTable",
//							new Params()
//								.add("entryId", getNotebookEntry().getId().getId())
//								.add("tableIndex", previousIndex))
//							)
//					)
//			.addButton(new Button("Next Table")
//					.setHtmlClass("next-table")
//					.setEnabled(hasMore)
//					.add(new OpComponentClick(this, "onShowTable",
//								new Params()
//									.add("entryId", getNotebookEntry().getId().getId())
//									.add("tableIndex", nextIndex))
//						)
//					);
		
		HtmlCanvas tableData = new HtmlCanvas();
		
		ApiCallCommand cmd = (ApiCallCommand) new ApiCallCommand(this)
			.setApi("api/table/list")
			.addParam("notebookId", getNotebookEntry().getNotebookId().getId())
			.addParam("entryId", getNotebookEntry().getId().getId())
			.run();
		
		Table first = null;
		for (Table t : cmd.getResponse().getAsList("tables", Table.class)) {
			tableData
				.render(new Link(t.getName(), 
						new OpList()
							.add(new OpComponentClick(this, 
									"onShowTable",
									new Params()
										.add("tableId", t.getId().getId())))
							.add(new OpRemoveClass("#" + getId() + " .beans-table-link", "beans-table-link-selected"))
							.add(new OpAddClass("#" + getId() + " .beans-table-link-" + t.getId().getId(), "beans-table-link-selected"))
							)
							.setHtmlClass("beans-table-link beans-table-link-" + t.getId().getId() 
									+ (first == null ? " beans-table-link-selected" : ""))
					);
			if (first == null)
				first = t;
		}
		
		if (first != null)
			tableData
				.div(class_("notebooktable-view"))
	//				.render(nextPrevButtons)
					.render(new TableDataViewPanel(this, first.getId())
							.setPerPage(10)
							.setPerPageVisible(false))
				._div();
		
		return new OpList()
			.add(new OpSet("#" + getId() + " .content", tableData.toHtml()))
			;
	}
	
	@Override
	protected OpList onRefresh(SecurityContext sc, Params params) throws IOException {
//		NotebookEntryProgress prog = getNotebookEntryProgress();
//		return new OpList()
				
		
//		Notification notification = null;
//		if (getPigScript() != null) {
//			notification = new ApiCallCommand(PigPanel.this)
//				.setApi(ApiCallCommand.API_NOTIFICATION_GET)
//				.addParam("id", getPigScript().getId().getId())
//				.run()
//				.getResponse()
//				.getAsObject(Notification.class);
//		} else {
//			LibsLogger.error(PigPanel.class, "Cannot check for notification, no pig script found");
//		}
		
		boolean isFinished = getNotebookEntryProgress().isFinished();
				
		return new OpList()
//			.add(isFinished ? onReloadStatus(notification.getTitle()) : null)//new OpReplace("#" + getId() + " .label", getStatusBadge(notification, new HtmlCanvas()).toHtml()))
			.add(new OpSet("#" + getId() + " .name", getPigScript().getName()))
			.add(isFinished ? new OpShow("#" + getId() + " .pig-panel-tables-show") : null)
			.add(!isFinished ? new OpComponentTimer(this, "onRefresh", BeansWebConst.PIG_AUTOREFRESH_INTERVAL_MS, new Params().add("entryId", getNotebookEntry().getId().getId())) : null)
			;
	}
	
//	protected HtmlCanvas getStatusBadge(Notification qs, HtmlCanvas html) throws IOException {
//		if (qs == null) {
//			
//			html
//				.span(class_(Badge.INFO + " label"))
//					.content("Not executed");
//			
//		} else if (qs.getNotificationType() == NotificationType.INFO) {
//			
//			html
//				.span(class_(Badge.INFO + " label"))
//					.content(qs.getTitle());
//			
////			addToResponse(new OpComponentTimer(this, "refresh", PIG_AUTOREFRESH_INTERVAL_MS, new Params().add("entryId", getNotebookEntry().getId().getId())));
//			
////		} else if (qs.getStatus() == QueryStatusType.RUNNING) {
////			
////			html
////				.span(class_(labellabel-info"))
////					.content("Running " + qs.getProgress() + "%");
//			
////			addToResponse(new OpComponentTimer(this, "refresh", PIG_AUTOREFRESH_INTERVAL_MS, new Params().add("entryId", getNotebookEntry().getId().getId())));
//			
//		} else if (qs.getNotificationType() == NotificationType.ERROR) {
//			
//			html
//				.span(class_(Badge.ERROR + " label"))
//					.content("Failed: " + qs.getDescriptionNotNull());
//			
//		} else if (qs.getNotificationType() == NotificationType.OK) {
//			
//			html
//				.span(class_(Badge.SUCCESS + " label"))
//					.content("Done");
//			
//		} else {
//			LibsLogger.error(QueryCheckpoints.class, "Unimplemented case for " + qs.getNotificationType());
//		}
//		return html;
//	}
	
	public PigScript getPigScript() {
		return (PigScript) getNotebookEntry();
	}
}
