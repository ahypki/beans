package net.beanscode.web.view.dataset;

import java.io.IOException;

import javax.ws.rs.core.SecurityContext;

import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

import net.beanscode.cli.datasets.DatasetSearchResults;
import net.beanscode.cli.notebooks.Notebook;
import net.beanscode.cli.notebooks.NotebookGateway;
import net.beanscode.cli.notebooks.NotebookSearchResults;
import net.beanscode.pojo.dataset.Dataset;
import net.beanscode.web.view.notebook.NotebookContent;
import net.beanscode.web.view.notebook.NotebooksListComponent;
import net.beanscode.web.view.notebook.NotebooksTableComponent;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.Link;
import net.beanscode.web.view.ui.ListComponent;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.url.Params;

public class DatasetsListComponent extends ListComponent {
	
	@Expose
	private OpList onClick = null;
	
	private DatasetSearchResults datasetSearchResults = null;

	public DatasetsListComponent() {
		
	}
	
	public DatasetsListComponent(BeansComponent parent) {
		super(parent);
	}
	
	public DatasetsListComponent(SecurityContext securityContext, Params params) {
		super(securityContext, params);
	}
	
	protected DatasetSearchResults getDatasetSearchResults() {
		if (datasetSearchResults == null) {
			try {
				datasetSearchResults = DatasetGateway.getDatasetSearchResults(this.getSessionBean(), getFilter(), getPage(), getPerPage());
			} catch (CLIException | IOException e) {
				LibsLogger.error(NotebooksTableComponent.class, "Cannot search for Notebooks", e);
				datasetSearchResults = new DatasetSearchResults();
			}
		}
		return datasetSearchResults;
	}
	
	@Override
	protected String getSettingsPrefix() {
		return "datasets-list-";
	}
	
	@Override
	protected String getFilterTooltip() {
		return "Filter datasets...";
	}
	
	@Override
	protected long getMaxHits() throws IOException {
		return getDatasetSearchResults().getMaxHits();
	}
	
	@Override
	protected void renderItem(int idx, HtmlCanvas html) throws IOException {
		Dataset d = getDatasetSearchResults().getDatasets().get(idx % getPerPage());
		
		html
//			.span(class_("").add("style", "background-color: rgb(" + nextInt(255) + "," + nextInt(255) + "," + nextInt(255) + ")"))
//				.content(new Link(n.getName(), getOnClick().addParamParam("notebookId", n.getId().getId())).toHtml(), false);
			.render(new Link(d.getName(), 
					getOnClick()
						.addParamParam("datasetId", d.getId().getId())))
			.render(new Link(null, 
					"link",
					new OpComponentClick(DatasetBetaContent.class, 
							"onShow",
							new Params().add("datasetId", d.getId().getId())))
					.addHtmlClass("link"));
	}

	public OpList getOnClick() {
		if (onClick == null)
			onClick = new OpList();
		return onClick;
	}

	public DatasetsListComponent setOnClick(OpList onClick) {
		this.onClick = onClick;
		return this;
	}
}
