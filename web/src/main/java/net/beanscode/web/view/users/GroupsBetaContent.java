package net.beanscode.web.view.users;

import java.io.IOException;

import net.beanscode.web.beta.BetaContent;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.BetaButtonGroup;

import org.rendersnake.HtmlCanvas;

public class GroupsBetaContent extends BetaContent {
	
	public GroupsBetaContent() {
		
	}

	public GroupsBetaContent(BeansComponent parent) {
		super(parent);
	}
		
	@Override
	protected void renderHeader(HtmlCanvas html) throws IOException {
		html
			.h1()
				.content("Groups");
	}
	
	protected void renderBreadcrumb(HtmlCanvas html) throws IOException {
		
	};
	
	@Override
	protected void renderContent(HtmlCanvas html) throws IOException {
		html
			.render(new GroupsBetaTableComponent(this));
	}

	@Override
	protected BetaButtonGroup getMenu() throws IOException {
		return null;
	}

	@Override
	protected String getMenuName() throws IOException {
		return null;
	};
}
