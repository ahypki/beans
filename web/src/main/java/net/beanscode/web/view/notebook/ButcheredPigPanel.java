package net.beanscode.web.view.notebook;

import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;
import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.id;
import static org.rendersnake.HtmlAttributesFactory.name;
import static org.rendersnake.HtmlAttributesFactory.type;

import java.io.IOException;

import javax.ws.rs.core.SecurityContext;

import org.rendersnake.HtmlCanvas;
import org.rendersnake.Renderable;

import net.beanscode.cli.BeansCliCommand;
import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.datasets.Table;
import net.beanscode.cli.notebooks.ButcheredPigEntry;
import net.beanscode.cli.notification.Notification;
import net.beanscode.cli.users.User;
import net.beanscode.web.BeansWebConst;
import net.beanscode.web.view.dataset.TableDataViewPanel;
import net.beanscode.web.view.ui.Link;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.beanscode.web.view.ui.modals.OpModalQuestion;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.components.ops.OpComponentTimer;
import net.hypki.libs5.pjf.op.OpAddClass;
import net.hypki.libs5.pjf.op.OpHide;
import net.hypki.libs5.pjf.op.OpJs;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpRemoveClass;
import net.hypki.libs5.pjf.op.OpReplace;
import net.hypki.libs5.pjf.op.OpSet;
import net.hypki.libs5.pjf.op.OpShow;
import net.hypki.libs5.pjf.op.OpToggle;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.api.APICallType;
import net.hypki.libs5.utils.url.Params;

public class ButcheredPigPanel extends NotebookEntryEditorPanel {
		
	public ButcheredPigPanel() {
		
	}

	public ButcheredPigPanel(Component parent) {
		super(parent);
	}
	
	@Override
	public Renderable getView() {
//		if (getNotificationEntry() == null || getNotificationEntry().isDone() == false)
//			getInitOpList().add(new OpComponentTimer(this, "onRefresh", BeansWebConst.PIG_AUTOREFRESH_INTERVAL_MS, new Params().add("entryId", getNotebookEntry().getId().getId())));
		
		return new Renderable() {
			@Override
			public void renderOn(HtmlCanvas html) throws IOException {
				html
					.span()
						.content(getButcheredPigScript().getName());
				
				html
					.div(class_("notebooktable-view")
							.add("style", "display: none;"))
						.content("No tables selected yet")
					;
			}
		};
	}

	@Override
	protected Renderable getEditor() {
		return new Renderable() {
			@Override
			public void renderOn(HtmlCanvas html) throws IOException {
				html
					.input(name("query-name")
							.class_("query-name form-control")
							.type("text")
							.value(getButcheredPigScript().getName())
							.add("placeholder", "Query name"))
										
					.input(type("hidden")
							.name(getId() + "-query-text")
							.id(getId() + "-query-text")
							.value(" "))
							
					.textarea(id(getId() + "-query"))
						.content(getButcheredPigScript().getQuery())
						
					.write(new OpJs("window.myCodeMirror" + getId() + " = CodeMirror.fromTextArea(document.getElementById(\"" + getId() + "-query\"), "
							+ "{ "
//							+ "	value: \"function myScript(){return 100;}\", "
							+ "	mode:  \"text/x-pig\", "
							+ "	indentUnit: 4,"
							+ "	lineNumbers: true "
							+ "});").toStringAdhock(), false)
							
//					.write(new OpPrepend("#" + getId() + ".play", html))
					;
			}
		};
	}
	
	@Override
	protected OpList onBeforeCancel() {
		return onBeforePlay();
	};
	
	@Override
	protected OpList onBeforePlay() {
		OpList ops = new OpList();
		
		ops.add(new OpJs("document.getElementById(\"" + getId() + "-query-text\").value = "
				+ "window.myCodeMirror" + getId() + ".getValue(); "));
		
		return ops;
	}
	
	@Override
	protected Button getMenu() throws IOException {
		return new Button()
			.addButton(new Button()
				.setAwesomeicon("fa fa-save")
				.setHtmlClass("save-only")
				.setVisible(false)
//				.setAwesomeicon("fa fa-save")
				.setTooltip("Save only the query (do not run)")
				.add(onBeforePlay())
				.add(new OpComponentClick(this, "saveOnly", new Params().add("entryId", getButcheredPigScript().getId())))
			)
			.addButton(new Button()
				.setAwesomeicon("fa fa-cog")
				.setTooltip("Properties")
				.add(new OpComponentClick(this, "onProperties",
						new Params()
							.add("entryId", getNotebookEntry().getId().getId())))
//				.setOpList(new OpList()
//								.add(new OpDialogMsg("Pig query - properties", getPigPanelProperties().toHtml()))
//				)
					)
			.addButton(new Button()
				.setHtmlClass("refresh")
				.setAwesomeicon("fa fa-sync")
				.setTooltip("Refresh query status")
				.setStopPropagation(true)
//				.setVisible(getQueryStatus() != null && !getQueryStatus().isDone())
				.add(new OpComponentClick(ButcheredPigPanel.this, "onRefresh", 
						new Params()
							.add("entryId", getNotebookEntry().getId().getId())))
						)
			.addButton(new Button()
				.setAwesomeicon("fa fa-table")
//				.setVisible(getNotificationEntry() != null && getNotificationEntry().isDone())
				.setHtmlClass("pig-panel-tables pig-panel-tables-show")
				.setTooltip("Show tables")
//				.add(new OpToggle("#" + getId() + " .pig-panel-table-button"))
//				.add(new OpToggle("#" + getId() + " .pig-panel-table-previous"))
//				.add(new OpToggle("#" + getId() + " .pig-panel-table-next"))
				.add(new OpToggle("#" + getId() + " .notebooktable-view"))
				.add(new OpComponentClick(ButcheredPigPanel.this, "onShowTables", 
						new Params()
							.add("entryId", getNotebookEntry().getId().getId())
							.add("tableIndex", 0)))
				)
//			.addButton(new Button()
//				.setGlyphicon("chevron-left")
//				.setVisible(false)
//				.setHtmlClass("pig-panel-tables pig-panel-table-previous")
//				.add(new OpComponentClick(PigPanel.this, "showTable", 
//						new Params()
//							.add("entryId", getNotebookEntry().getId().getId())
//							.add("tableIndex", 0)))
//						)
//			.addButton(new Button()
//				.setGlyphicon("chevron-right")
//				.setVisible(false)
//				.setHtmlClass("pig-panel-tables pig-panel-table-next")
//				.add(new OpComponentClick(PigPanel.this, "showTable", 
//						new Params()
//							.add("entryId", getNotebookEntry().getId().getId())
//							.add("tableIndex", 0)))
//						)
				
			.addButton(new Button()
				.setAwesomeicon("fa fa-stop")
				.setTooltip("Stops the script if it is running")
				.add(new OpModalQuestion("Stopping the script", 
						"Do you want to stop the script <b>" + getButcheredPigScript().getName() + "</b>?", 
						new OpComponentClick(this, 
								"onStop",
								new Params().add("entryId", getButcheredPigScript().getId()))))
				)
			;
	}
	
	private OpList onProperties() {
		
		return new OpList()
			.add(new OpSet("#" + getId() + " .content", getPigPanelProperties().toHtml()));
	}
	
	private OpList onStop() throws CLIException, IOException {
		String entryId = getParams().getString("entryId");
		
		new ApiCallCommand(this)
			.setApi("api/notebook/entry/stop")
			.addParam("entryId", entryId)
			.run();
		
		return new OpList()
			.add(new OpBeansInfo("Stopping script..."));
	}
	
	

	private HtmlCanvas getPigPanelProperties() {
		try {
			User user = new ApiCallCommand(getSessionBean())
							.setApi("api/user/get")
							.addParam("userId", getButcheredPigScript().getUserId().getId())
							.run()
							.getResponse()
							.getAsObject(User.class);
			
			return new HtmlCanvas()
				.dl(class_("row"))
					.dt(class_("col-sm-3"))
						.content("ID")
					.dd(class_("col-sm-9"))
						.content(getButcheredPigScript().getId().getId())
					.dt(class_("col-sm-3"))
						.content("Owner")
					.dd(class_("col-sm-9"))
						.content(user != null ? user.getEmail() : "")
					.dt(class_("col-sm-3"))
						.content("Creation date")
					.dd(class_("col-sm-9"))
						.content(getButcheredPigScript().getCreate().toStringHuman())
				._dl();
		} catch (IOException e) {
			LibsLogger.error(ButcheredPigPanel.class, "Cannot render PigPanel properties panel", e);
			return new HtmlCanvas();
		}
	}

	public OpList saveOnly(SecurityContext sc, Params params) throws IOException {
		try {
			final String name = params.getString("query-name");
			final String query = params.getString(getId() + "-query-text");
			final String queryId = params.getString("entryId");
			
			new ApiCallCommand(this)
				.setApi(BeansCliCommand.API_NOTEBOOK_META_SET)
				.addParam("entryId", queryId)
				.addParam("name", name)
				.addParam("META_QUERY", query)
				.run();
		} catch (IOException e) {
			LibsLogger.error(ButcheredPigPanel.class, "Cannot save pig script");
		}
		
		return new OpList()
			.add(new OpShow("#" + getId() + " .edit"))
			.add(new OpHide("#" + getId() + " .play"))
			.add(new OpComponentClick(this, "stopClick", params));
	}
	
	@Override
	protected OpList opOnPlay(SecurityContext sc, Params params) throws IOException {
		try {
			String name = params.getString("query-name");
			final String query = params.getString(getId() + "-query-text");
			final String pigMode = params.getString(getId() + "-pig-mode");
			final String queryId = params.getString("entryId");
			
			if (nullOrEmpty(name))
				name = "no name";
			
			ApiCallCommand call = (ApiCallCommand) new ApiCallCommand(this)
				.setApi("api/butcheredpig/edit")
				.addParam("queryId", queryId)
				.addParam("name", name)
				.addParam("pigMode", pigMode)
				.addParam("query", query)
				.run();
			
//			new PigSave(getSessionBean())
//				.run(new UUID(queryId), name, query);
			
			new ApiCallCommand(this)
				.setApi("api/butcheredpig/run")
				.addParam("queryId", queryId)
				.run();
			
			return new OpList()
				.add(new OpHide("#" + getId() + " .save-only"))
				.add(new OpHide("#" + getId() + " .pig-panel-tables"))
				.add(new OpShow("#" + getId() + " .refresh"))
				.add(new OpComponentTimer(this, "onRefresh", 1000, new Params().add("entryId", getNotebookEntry().getId().getId())))
				;
		} catch (Exception e) {
			throw new IOException("Cannot start Pig script: " + e.getMessage());
//			LibsLogger.error(TextPanel.class, "Cannot start Pig script", e);
//			return new OpList()
//				.add(new OpBeansError("Cannot start Pig script: " + e.getMessage()));
		}
	}
	
	@Override
	protected OpList opOnEdit(SecurityContext sc, Params params) throws IOException {
		return new OpList()
			.add(new OpShow("#" + getId() + " .save-only"))
			.add(new OpHide("#" + getId() + " .pig-panel-tables"));
	}
	
	@Override
	protected OpList opOnStop(SecurityContext sc, Params params) throws IOException {
		final String query = params.getString(getId() + "-query-text");
		
		return new OpList()
			.add(new OpHide("#" + getId() + " .save-only"))
			.add(new OpShow("#" + getId() + " .pig-panel-tables"))
			.add(new OpHide("#" + getId() + " .notebooktable-view"));
	}

	@Override
	protected OpList opOnRemove(SecurityContext sc, Params params) throws IOException {
		try {
			new ApiCallCommand(this)
				.setApi("api/notebook/entry/remove")
				.setApiCallType(APICallType.DELETE)
				.addParam("entryId", params.getString("entryId"))
				.run();
		} catch (IOException e) {
			LibsLogger.error(ButcheredPigPanel.class, "Cannot remove Notebook Entry", e);
		}
		
		return new OpList();
	}
	
//	@Override
//	public OpList getInitOpList() {
//		return super.getInitOpList()
//				.add(new OpComponentClick(this, "refresh", new Params().add("plotId", getPlot().getId())));
//	}
	
	public OpList onShowTable() throws IOException {
		final String tableId = getParams().getString("tableId", null);
		
		if (nullOrEmpty(tableId))
			return null;
		
		HtmlCanvas notebookView = new HtmlCanvas();
		notebookView
			.div(class_("notebooktable-view"))
//				.render(nextPrevButtons)
				.render(new TableDataViewPanel(this, new UUID(tableId))
						.setPerPage(10)
						.setPerPageVisible(false))
			._div();
		
		return new OpList()
			.add(new OpReplace("#" + getId() + " .notebooktable-view", notebookView.toHtml()))
			;
	}
	
	public OpList onShowTables() throws IOException {
//		final String entryId = getParams().getString("entryId");
//		final String tableId = getParams().getString("tableId", null);
//		
//		Table table = new TableGetByIndex(getSessionBean()).run(getNotebookEntry().getNotebookId(), 
//				getNotebookEntry().getId(), tableIndex);
//		boolean hasMore = table != null;
//				
//		if (table == null) {
//			LibsLogger.debug(PigPanel.class, "No more tables for pig query ", getPigScript().getId());
//			return new OpList()
//				.add(new OpDisable("#" + getId() + " .next-table"))
////				.add(new OpBeansInfo("No more tables in that Pig query"))
//				;
//		}
//		
//		final int previousIndex = tableIndex > 0 ? tableIndex - 1 : 0;
//		final int nextIndex = hasMore ? tableIndex + 1 : tableIndex;
//		
//		ButtonGroup nextPrevButtons = new ButtonGroup()
//			.setAdditionalCssClasses("tables-buttons")
//			.addButton(new Button("Previous Table")
//					.setHtmlClass("prev-table")
//					.setEnabled(tableIndex > 0)
//					.add(new OpComponentClick(this, "onShowTable",
//							new Params()
//								.add("entryId", getNotebookEntry().getId().getId())
//								.add("tableIndex", previousIndex))
//							)
//					)
//			.addButton(new Button("Next Table")
//					.setHtmlClass("next-table")
//					.setEnabled(hasMore)
//					.add(new OpComponentClick(this, "onShowTable",
//								new Params()
//									.add("entryId", getNotebookEntry().getId().getId())
//									.add("tableIndex", nextIndex))
//						)
//					);
		
		HtmlCanvas tableData = new HtmlCanvas();
		
		ApiCallCommand cmd = (ApiCallCommand) new ApiCallCommand(this)
			.setApi("api/table/list")
			.addParam("notebookId", getNotebookEntry().getNotebookId().getId())
			.addParam("entryId", getButcheredPigScript().getId().getId())
			.run();
		
		Table first = null;
		for (Table t : cmd.getResponse().getAsList("tables", Table.class)) {
			tableData
				.render(new Link(t.getName(), 
						new OpList()
							.add(new OpComponentClick(this, 
									"onShowTable",
									new Params()
										.add("tableId", t.getId().getId())))
							.add(new OpRemoveClass("#" + getId() + " .beans-table-link", "beans-table-link-selected"))
							.add(new OpAddClass("#" + getId() + " .beans-table-link-" + t.getId().getId(), "beans-table-link-selected"))
							)
							.setHtmlClass("beans-table-link beans-table-link-" + t.getId().getId() 
									+ (first == null ? " beans-table-link-selected" : ""))
					);
			if (first == null)
				first = t;
		}
		
		if (first != null)
			tableData
				.div(class_("notebooktable-view"))
	//				.render(nextPrevButtons)
					.render(new TableDataViewPanel(this, first.getId())
							.setPerPage(10)
							.setPerPageVisible(false))
				._div();
		
		return new OpList()
			.add(new OpSet("#" + getId() + " .content", tableData.toHtml()))
			;
	}
	
	@Override
	protected OpList onRefresh(SecurityContext sc, Params params) throws IOException {
//		setNotebookEntry(NotebookEntry.getNotebookEntry(new UUID(params.getString("entryId"))));
		
//		Notification notification = null;
//		if (getButcheredPigScript() != null) {
//			notification = new ApiCallCommand(ButcheredPigPanel.this)
//				.setApi(ApiCallCommand.API_NOTIFICATION_GET)
//				.addParam("id", getButcheredPigScript().getId().getId())
//				.run()
//				.getResponse()
//				.getAsObject(Notification.class);
//		} else {
//			LibsLogger.error(ButcheredPigPanel.class, "Cannot check for notification, no pig script found");
//		}

		boolean isFinished = getNotebookEntryProgress().isFinished();
				
		return new OpList()
//			.add(new OpSet("#" + getId() + " .status .badge", notification.getTitle()))
//			.add(onReloadStatus(notification != null ? notification.getTitle() : ""))//new OpReplace("#" + getId() + " .label", getStatusBadge(notification, new HtmlCanvas()).toHtml()))
			.add(isFinished ? new OpShow("#" + getId() + " .pig-panel-tables-show") : null)
			.add(!isFinished ? new OpComponentTimer(this, "onRefresh", 
					BeansWebConst.PIG_AUTOREFRESH_INTERVAL_MS / 5, new Params().add("entryId", getNotebookEntry().getId().getId())) : null)
			;
	}
	
//	protected HtmlCanvas getStatusBadge(Notification qs, HtmlCanvas html) throws IOException {
//		if (qs == null) {
//			
//			html
//				.span(class_(Badge.INFO + " label"))
//					.content("Not executed");
//			
//		} else if (qs.getNotificationType() == NotificationType.INFO) {
//			
//			html
//				.span(class_(Badge.INFO + " label"))
//					.content(qs.getTitle());
//			
////			addToResponse(new OpComponentTimer(this, "refresh", PIG_AUTOREFRESH_INTERVAL_MS, new Params().add("entryId", getNotebookEntry().getId().getId())));
//			
////		} else if (qs.getStatus() == QueryStatusType.RUNNING) {
////			
////			html
////				.span(class_(labellabel-info"))
////					.content("Running " + qs.getProgress() + "%");
//			
////			addToResponse(new OpComponentTimer(this, "refresh", PIG_AUTOREFRESH_INTERVAL_MS, new Params().add("entryId", getNotebookEntry().getId().getId())));
//			
//		} else if (qs.getNotificationType() == NotificationType.ERROR) {
//			
//			html
//				.span(class_(Badge.ERROR + " label"))
//					.content("Failed: " + qs.getDescriptionNotNull());
//			
//		} else if (qs.getNotificationType() == NotificationType.OK) {
//			
//			html
//				.span(class_(Badge.SUCCESS + " label"))
//					.content("Done");
//			
//		} else {
//			LibsLogger.error(QueryCheckpoints.class, "Unimplemented case for " + qs.getNotificationType());
//		}
//		return html;
//	}
	
	public ButcheredPigEntry getButcheredPigScript() {
		return (ButcheredPigEntry) getNotebookEntry();
	}
}
