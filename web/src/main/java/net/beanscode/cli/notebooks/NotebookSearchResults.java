package net.beanscode.cli.notebooks;

import java.util.List;

import net.hypki.libs5.db.db.weblibs.utils.UUID;

import com.google.gson.annotations.Expose;

public class NotebookSearchResults {

	@Expose
	private List<Notebook> notebooks = null;
	
	@Expose
	private int maxHits = 0;
	
	public NotebookSearchResults() {
		
	}

	public List<Notebook> getNotebooks() {
		return notebooks;
	}

	public NotebookSearchResults setNotebooks(List<Notebook> notebooks) {
		this.notebooks = notebooks;
		return this;
	}

	public int getMaxHits() {
		return maxHits;
	}

	public NotebookSearchResults setMaxHits(int maxHits) {
		this.maxHits = maxHits;
		return this;
	}
	
	public NotebookSearchResults setMaxHits(long maxHits) {
		this.maxHits = (int) maxHits;
		return this;
	}
	
	public Notebook getNotebook(UUID notebookId) {
		for (Notebook notebook : notebooks) {
			if (notebook.getId().equals(notebookId))
				return notebook;
		}
		return null;
	}
	
	public void remove(UUID notebookId) {
		if (notebookId == null)
			return;
		
		int toRemove = -1;
		for (int i = 0; i < notebooks.size(); i++) {
			if (notebooks.get(i).getId().equals(notebookId)) {
				toRemove = i;
				break;
			}
		}
		
		if (toRemove >= 0)
			notebooks.remove(toRemove);
	}
}
