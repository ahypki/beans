package net.beanscode.web.view.settings;

import static net.hypki.libs5.pjf.OpsFactory.opsCall;
import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.onClick;

import java.io.IOException;

import javax.ws.rs.core.SecurityContext;

import net.beanscode.web.view.ui.modals.OpBeansError;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.beanscode.web.view.ui.modals.OpModalQuestion;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpSet;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.url.Params;

import org.rendersnake.HtmlCanvas;

public class DemoContent extends Component {
	
	public OpList demoDatasets(SecurityContext sc, Params params) {
//		try {
//			new DemoData(getSessionBean()).run();
//			new DemoDataJob(getSessionBean(sc).getUserId()).save();
			
			return new OpList()
				.add(new OpBeansInfo("Demo datasets will be created shortly"));
//		} catch (IOException e) {
//			LibsLogger.error(DemoContent.class, "Cannot create job to start demo datasets creation", e);
//			return new OpList()
//				.add(new OpBeansError("Demo datasets cannot be created, try later"));
//		}
	}
	
	public OpList demoQueries(SecurityContext sc, Params params) {
//		try {
//			new DemoQueries(getSessionBean()).run();
//			new DemoQueriesJob(getSessionBean(sc).getUserId()).save();
			return new OpList()
				.add(new OpBeansInfo("Demo queries will be created shortly"));
//		} catch (IOException e) {
//			LibsLogger.error(DemoContent.class, "Cannot create job to start demo datasets creation", e);
//			return new OpList()
//				.add(new OpBeansError("Demo queries cannot be created, try later"));
//		}
	}
	
	public OpList showContent(Params params) throws IOException {
		HtmlCanvas content = new HtmlCanvas()
			.div(class_("options-page"))
				.h3()
					.content("Demo data")
				.div()
					.a(onClick(opsCall(
							new OpModalQuestion("Demo datasets", 
									"Do you want to create demo datasets?", 
									new OpComponentClick(this, "demoDatasets", null)))))
						.content("Generate demo datasets")
					.br()
					.a(onClick(opsCall(
							new OpModalQuestion("Demo queries", 
									"Do you want to create demo notebooks?", 
									new OpComponentClick(this, "demoQueries", null)))))
						.content("Generate demo notebooks")
				._div()
			._div()
			;
		
		return new OpList()
			.add(new OpSet("#page-title", "Demo data and queries"))
			.add(new OpSet("#main", content.toHtml()))
			;
	}
}
