package net.beanscode.web.view.data;

import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;

import java.io.IOException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.StreamingOutput;

import mjson.Json;
import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.notebooks.GnuplotEntry;
import net.beanscode.web.BeansPage;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.file.FileExt;

@Path("view")
public class DataRest extends BeansPage {
	
	@GET
	@Path("status")
	@Produces(MediaType.APPLICATION_JSON)
	public Response status(@Context SecurityContext sc) throws IOException, ValidationException {
		    return Response
					.ok()
					.entity(Json
							.object()
							.set("database", "OK")
							.set("searchIndex", "OK")
							.toString())
					.build();
	}
	
	@GET
	@Path("table/data")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response tableData(@Context SecurityContext sc,
			@QueryParam("tableId") String tableId) throws IOException, ValidationException {
		StreamingOutput stream = new ApiCallCommand(getSessionBean(sc))
									.setApi("api/table/data")
									.addParam("tableId", tableId)
									.runStream();
	
	    return Response.ok(stream, MediaType.APPLICATION_OCTET_STREAM)
	        .header("Content-Disposition", "inline; filename=\"" + tableId + ".dat\"") 
	        .build();
	}
	
	@GET
	@Path("notebook/entry/pdf")
	@Produces("application/pdf")
	public Response notebookEntryPdf(@Context SecurityContext sc,
			@QueryParam("entryId") String entryId,
			@QueryParam("metaName") String metaName) throws IOException, ValidationException {
		StreamingOutput stream = new ApiCallCommand(getSessionBean(sc))
										.setApi("api/notebook/entry/download")
										.addParam("entryId", entryId)
										.addParam("metaName", metaName)
										.runStream();
//				new NotebookEntryStreamGet(getSessionBean(sc)).run(entryId, metaName);
		
		GnuplotEntry ge = new ApiCallCommand(getSessionBean(sc))
			.setApi("api/notebook/entry/get")
			.addParam("entryId", entryId)
			.run()
			.getResponse()
			.getAsObject(GnuplotEntry.class);
		
		String filename = ge.getMetaAsString(metaName);
		if (nullOrEmpty(filename))
			filename = "file.pdf";
		else
			filename = new FileExt(filename).getFilenameOnly();

		return Response
				.ok(stream)
				.header("content-disposition", "inline; filename = " + filename)
				.build();
	}
	
	@GET
	@Path("notebook/entry/download")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response notebookEntryDownload(@Context SecurityContext sc,
			@QueryParam("entryId") String entryId,
			@QueryParam("metaName") String metaName) throws IOException, ValidationException {
		StreamingOutput stream = new ApiCallCommand(getSessionBean(sc))
			.setApi("api/notebook/entry/download")
			.addParam("entryId", entryId)
			.addParam("metaName", metaName)
			.runStream();
//		StreamingOutput stream = new NotebookEntryStreamGet(getSessionBean(sc)).run(entryId, metaName);
		
		GnuplotEntry ge = new ApiCallCommand(getSessionBean(sc))
			.setApi("api/notebook/entry/get")
			.addParam("entryId", entryId)
			.run()
			.getResponse()
			.getAsObject(GnuplotEntry.class);
		
		String filename = ge.getMetaAsString(metaName);
		if (nullOrEmpty(filename))
			filename = "file";
		else
			filename = new FileExt(filename).getFilenameOnly();

		return Response
				.ok(stream)
				.header("content-disposition", "inline; filename = " + filename)
				.build();
	}

	@GET
	@Path("data")
	@Produces(MediaType.APPLICATION_JSON)
	public StreamingOutput data(@Context SecurityContext sc,
			@QueryParam("queryId") String queryId,
			@QueryParam("plotNr") final int plotNr,
			@QueryParam("type") String type,
			@QueryParam("sample") double sample) throws IOException, ValidationException {
//		return new DataStreamGet(getSessionBean(sc), "api/plot/data").run(queryId, Math.max(0, plotNr), type);
		return new ApiCallCommand(getSessionBean(sc))
			.setApi(ApiCallCommand.API_PLOT_DATA_HISTOGRAM)
			.addParam("entryId", queryId)
//			.addParam("split", Math.max(0, plotNr))
			.addParam("type", type)
			.addParam("sample", sample)
			.runStream();
	}
	
	@GET
	@Path("data/parallel")
	@Produces(MediaType.APPLICATION_JSON)
	public StreamingOutput dataParallel(@Context SecurityContext sc,
			@QueryParam("queryId") String queryId,
			@QueryParam("plotNr") final int plotNr,
			@QueryParam("type") String type) throws IOException, ValidationException {
//		return new DataStreamGet(getSessionBean(sc), "api/plot/data/parallel").run(queryId, plotNr, type);
		return new ApiCallCommand(getSessionBean(sc))
			.setApi(ApiCallCommand.API_PLOT_DATA_PARALLEL)
			.addParam("entryId", queryId)
			.addParam("plotNr", Math.max(0, plotNr))
			.addParam("type", type)
			.runStream();
	}
	
	@GET
	@Path("data/histogram")
	@Produces(MediaType.APPLICATION_JSON)
	public StreamingOutput dataHistogram(@Context SecurityContext sc,
			@QueryParam("queryId") String queryId,
			@QueryParam("plotNr") final int plotNr,
			@QueryParam("type") String type) throws IOException, ValidationException {
//		return new DataStreamGet(getSessionBean(sc), "api/plot/data/histogram").run(queryId, plotNr, type);
		return new ApiCallCommand(getSessionBean(sc))
			.setApi(ApiCallCommand.API_PLOT_DATA_HISTOGRAM)
			.addParam("queryId", queryId)
			.addParam("plotNr", Math.max(0, plotNr))
			.addParam("type", type)
			.runStream();
	}
	
	@GET
	@Path("image")
	@Produces(MediaType.APPLICATION_JSON)
	public StreamingOutput image(@Context SecurityContext sc,
			@QueryParam("plotId") String queryId,
			@QueryParam("plotNr") final int plotNr,
			@QueryParam("type") String type) throws IOException, ValidationException {
//		return new ImageStreamGet(getSessionBean(sc)).run(queryId, plotNr, type);
		return new ApiCallCommand(getSessionBean(sc))
			.setApi(ApiCallCommand.API_PLOT_IMAGE)
			.addParam("entryId", queryId)
			.addParam("split", Math.max(0, plotNr))
			.addParam("type", type)
			.runStream();
	}
}
