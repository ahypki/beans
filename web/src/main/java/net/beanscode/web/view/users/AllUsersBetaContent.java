package net.beanscode.web.view.users;

import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.IOException;

import net.beanscode.web.beta.BetaContent;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.BetaButtonGroup;

import org.rendersnake.HtmlCanvas;

public class AllUsersBetaContent extends BetaContent {
	
	public AllUsersBetaContent() {
		
	}

	public AllUsersBetaContent(BeansComponent parent) {
		super(parent);
	}
		
	@Override
	protected void renderHeader(HtmlCanvas html) throws IOException {
		html
			.h1()
				.content("All users");
	}
	
	@Override
	protected void renderContent(HtmlCanvas html) throws IOException {
		assertTrue(isAdmin(), "You have no rights to see this content");
		
		html
			.render(new UsersBetaTableComponent(this));
	}

	@Override
	protected BetaButtonGroup getMenu() throws IOException {
		return null;
	}

	@Override
	protected String getMenuName() throws IOException {
		return null;
	};
}
