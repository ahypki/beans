package net.beanscode.web.view.ui.modals;

import java.io.IOException;

import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.op.Op;
import net.hypki.libs5.pjf.op.OpAppend;
import net.hypki.libs5.pjf.op.OpJs;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpReplace;

public class OpModalQuestion extends OpList {

	private String question = null;
	
	public OpModalQuestion(String title,
			String question,
			OpList onYes) throws IOException {
		setQuestion(question);
		init(title, onYes);
	}

	public OpModalQuestion(String title,
			String question,
			Op onYes) throws IOException {
		setQuestion(question);
		init(title, new OpList().add(onYes));
	}

	private void init(String title, OpList onYes) throws IOException {
		ModalWindow modal = new ModalWindow();
		modal
			.setTitle(title)
			.add(new Button("Yes")
					.setReplacedHtmlClass("btn btn-outline-primary")
					.add(onYes
							.add(modal.onClose())))
			.add(new Button("No")
					.setReplacedHtmlClass("btn btn-outline-primary")
					.add(modal.onClose()))
			.getContent()
				.span()
					.content(getQuestion(), false)
			;
		
		add(new OpAppend(".beans-modal-windows", modal.toHtml()));
		add(modal.onClose());
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}
}
