package net.beanscode.model.agent;

public enum CassandraStatus {
	UP,
	DOWN,
	STARTING
}
