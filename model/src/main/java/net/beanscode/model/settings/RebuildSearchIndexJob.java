package net.beanscode.model.settings;

import java.io.IOException;
import java.util.List;

import net.beanscode.model.BeansSearchManager;
import net.beanscode.model.UserUtils;
import net.beanscode.model.backup.AutoBackupJob;
import net.beanscode.model.backup.BackupJob;
import net.beanscode.model.users.BeansMailJob;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.Watch;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.jobs.Job;
import net.hypki.libs5.weblibs.user.User;
import net.hypki.libs5.weblibs.user.UserFactory;

public class RebuildSearchIndexJob extends Job {

	public RebuildSearchIndexJob() {
		
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		try {
			Watch w = new Watch();
			
			BeansSearchManager.reloadIndex();
			LibsLogger.info(RebuildSearchIndexJob.class, "Reloading search index done");

			boolean sentAtLeastOne = false;
			for (User admin : UserUtils.getAdminUsers()) {
				try {
					new BeansMailJob(admin.getEmail(), 
							"[BEANS] Reload search index successfull", 
							"Reload search index done for " + WeblibsConst.KEYSPACE + " keyspace in " + w)
						.save();
					
					sentAtLeastOne = true;
				} catch (Exception e) {
					LibsLogger.error(RebuildSearchIndexJob.class, "Cannot send email to " + admin.getEmail() + ", but job will not be repeated", e);
				}
			}
			
			if (!sentAtLeastOne)
				LibsLogger.error(RebuildSearchIndexJob.class, "No admin was notfified about reload search index");
		} catch (Throwable t) {
			LibsLogger.error(RebuildSearchIndexJob.class, "Cannot reload index, fix the code and try again later", t);
		}
	}
}
