package net.beanscode.pojo.autoupdate;

public enum AutoUpdateOption {
	IGNORE,
	NO_CONFIRMATION_NEEDED,
	CONFIRMATION_NEEDED
}
