package net.beanscode.cli.users;

import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.date.SimpleDate;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class User {

	@Expose
	private UUID id = null;
	
	@NotNull
	@NotEmpty
	@AssertValid
	@Expose
	private String email = null;
	
	@NotNull
	@NotEmpty
	@AssertValid
	@Expose
	private String status = null;
	
	@Expose
	private long creationDate = 0;
	
	public User() {
		
	}

	public UUID getId() {
		return id;
	}
	
	@Override
	public String toString() {
		return getEmail() + " (" + getId() + "), status: " + getStatus() + ", created: " + getCreationDate().toStringHuman();
	}
	
	public boolean isBlocked() {
		return getStatus().equalsIgnoreCase("BLOCKED");
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public SimpleDate getCreationDate() {
		return new SimpleDate(creationDate);
	}

	public void setCreationDate(long creationDate) {
		this.creationDate = creationDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
