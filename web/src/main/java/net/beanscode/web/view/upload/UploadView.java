package net.beanscode.web.view.upload;

import java.io.IOException;
import java.io.InputStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.datasets.Table;
import net.beanscode.web.BeansPage;
import net.beanscode.web.view.dataset.TableBetaPanel;
import net.beanscode.web.view.ui.modals.OpBeansError;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.op.OpJs;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpPrepend;
import net.hypki.libs5.utils.LibsLogger;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

@Path(UploadView.PATH)
public class UploadView extends BeansPage {

	public static final String PATH = "ops/file";
	
	public static final String UPLOAD = "upload";
	public static final String UPLOAD_URL = "/" + PATH + "/" + UPLOAD;

	@POST
	@Path(UPLOAD)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response upload(@Context SecurityContext sc,
			@FormDataParam("file") InputStream uploadedInputStream,
			@FormDataParam("file") FormDataContentDisposition fileDetail,
			@QueryParam("datasetId") UUID datasetId,
			@QueryParam("tableId") UUID tableId) throws IOException, ValidationException {
		
		try {
			Table table = new ApiCallCommand(getSessionBean(sc))
				.setApi(ApiCallCommand.API_FILE_UPLOAD)
				.addParam("datasetId", datasetId)
				.addParam("tableId", tableId)
				.addParam("tableName", fileDetail.getFileName())
				.addStream(fileDetail.getFileName(), uploadedInputStream)
				.run()
				.getResponse()
				.getAsObject(Table.class);
			
			OpList opList = new OpList();
			opList
				.add(new OpBeansInfo("File uploaded successfully!"))
				;
			
			if (table != null) {
//				Table table = new TableGet().run(newTableId);
				opList.add(new OpPrepend("#dataset-tables .tables", new TableBetaPanel(null, table).setSessionBean(sc).toHtml()));
				opList.add(new OpJs("new Dropzone('#dropzone-" + table.getId() + "', " +
						"{url: '" + "/api/file/upload?datasetId=" + table.getDatasetId() + "&tableId=" + table.getId() + "', addRemoveLinks: true, maxFilesize: 1000, maxFiles: 1, clickable: true})"
						+ ".on('success', function(file, response) { pjfOps(response); });"));
			}
			
			return Response.status(200).entity(opList.toString()).build();
		} catch (Exception e) {
			LibsLogger.error(UploadView.class, "Cannot upload a file", e);
			return Response.status(500).entity(new OpList().add(new OpBeansError(e.getMessage())).toString()).build();
		}
	}
}
