package net.beanscode.rest.settings;

import static java.lang.String.format;
import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;
import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.IOException;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import mjson.Json;
import net.beanscode.model.BeansSettings;
import net.beanscode.model.plugins.Connector;
import net.beanscode.model.plugins.Plugin;
import net.beanscode.model.plugins.PluginManager;
import net.beanscode.model.rights.Rights;
import net.beanscode.model.settings.ColorPalette;
import net.beanscode.rest.BeansRest;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.search.SearchResults;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.url.HttpResponse;
import net.hypki.libs5.utils.utils.AssertUtils;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.mail.MailCredentials;
import net.hypki.libs5.weblibs.mail.Mailer;
import net.hypki.libs5.weblibs.property.Property;
import net.hypki.libs5.weblibs.property.PropertyFactory;
import net.hypki.libs5.weblibs.settings.Setting;
import net.hypki.libs5.weblibs.settings.SettingFactory;
import net.hypki.libs5.weblibs.settings.SettingValue;
import net.hypki.libs5.weblibs.user.Email;
import net.hypki.libs5.weblibs.user.User;
import net.hypki.libs5.weblibs.user.UserFactory;

@Path("api/setting")
public class SettingsRest extends BeansRest {
	
	@GET
	@Path("connector/default")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getConnectorDefault(@Context SecurityContext sc,
			@QueryParam("userEmail") String userEmail) throws IOException, ValidationException {
		validateUnauthorized(isAdmin(sc) || getLoggedUser(sc).getEmail().equals(userEmail), "Permission denied");
		
		Connector connector = BeansSettings.getDefaultConnector(UUID.random(), UUID.random().getId());
		return Response
				.ok()
				.entity(JsonUtils.objectToString(connector))
				.build();
	}
	
	@POST
	@Path("connector/default")
	@Produces(MediaType.APPLICATION_JSON)
	public void setConnectorDefault(@Context SecurityContext sc,
			@FormParam("userEmail") String userEmail,
			@FormParam("connector") String newDefaultConnector) throws IOException, ValidationException {
		validateUnauthorized(isAdmin(sc) 
				|| getLoggedUser(sc).getEmail().equals(userEmail), "Permission denied");
		
		BeansSettings.saveNewDefaultConnector(getUserId(userEmail), newDefaultConnector);
	}
	
	@GET
	@Path("connector/list")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getConnectorAll(@Context SecurityContext sc) throws IOException, ValidationException {
		Json connectors = Json.array();
		
		for (Plugin plugin : PluginManager.iterateAllPlugins()) {
			Json c = Json.object();
			c.set("name", plugin.getConnectorClasses());
			
			connectors.add(c);
		}
		
		return Response
				.ok()
				.entity(JsonUtils.objectToString(connectors))
				.build();
	}
	
//	@GET
//	@Path("search")
//	@RolesAllowed(Rights.ADMIN)
//	@Produces(MediaType.APPLICATION_JSON)
//	public String search(@Context SecurityContext sc, 
//			@QueryParam("filter") String filter,
//			@QueryParam("from") int from,
//			@QueryParam("size") int size
//			) throws IOException, ValidationException {
//		filter = filter.trim();
//		
//		SearchResults<Property> properties = PropertyFactory.search(filter, from, size);
//		
//		mjson.Json propsArray = Json.array();
//		for (Property p : properties.getObjects()) {
//			propsArray.add(propToJson(p));
//		}
//		
//		mjson.Json ret = Json.object();
//		ret.set("maxHits", properties.maxHits());
//		ret.set("properties", propsArray);
//		
//		return new HttpResponse()
//			.setBody(ret.toString())
//			.toString();
//	}
	
//	private mjson.Json propToJson(Property p) {
//		mjson.Json oneProp = Json.object();
//		
//		oneProp.set("lang", p.getLangId().toString());
//		oneProp.set("key", p.getPropKey());
//		oneProp.set("value", p.getMsg());
//		
//		mjson.Json id = Json.object();
//		id.set("id", p.getUserId() != null ? p.getUserId().getId() : "");
//		
//		oneProp.set("userId", id);
//		oneProp.set("description", p.getDescription() != null ? p.getDescription() : "");
//		
//		return oneProp;
//	}
	
	@POST
	@Path("setSetting")
	public void setSetting(@Context SecurityContext sc,
			@FormParam("id") String settingId,
//			@FormParam("name") String name,
//			@FormParam("value") String value,
			@FormParam("userEmail") String userEmail,
			MultivaluedMap<String, String> meta
			) throws IOException, ValidationException {
		validateUnauthorized(isAdmin(sc) || getLoggedUser(sc).getEmail().equals(userEmail), "Permission denied");
		
		meta.remove("id");
		meta.remove("userEmail");
		
		UUID userId = null;
		if (notEmpty(userEmail)) {
			if (UUID.isValid(userEmail))
				userId = new UUID(userEmail);
			else
				userId = User.getUserId(new Email(userEmail));
		}

		Setting s = SettingFactory.getSetting(userId, settingId);
		
		if (s == null) {
			s = new Setting();
			s.setId(settingId);
			s.setSystem(false);
			s.setName("User created setting");
			s.setUserId(userId);
		}
		
		for (String name : meta.keySet()) {
			SettingValue val = s.getValue(name);
			
			if (val == null) {
				LibsLogger.info(SettingsRest.class, "Name " + name + " not found in setting " + settingId + ", creating one");
				
				val = new SettingValue(name, null);
				s.addValue(val);
			}
			
			List<String> newValue = meta.get(name);
//			if (newValue.matches("\\[.*\\]"))
//				newValue = newValue.substring(1, newValue.length() - 1);
			
			val.setValue(newValue.size() == 1 ? newValue.get(0) : newValue);
		}
		
		s.save();
	}
	
	@GET
	@Path("getSetting")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSetting(@Context SecurityContext sc,
			@QueryParam("id") String settingId,
			@QueryParam("userEmail") String userEmail) throws IOException, ValidationException {
//		validateUnauthorized(isAdmin(sc) || getLoggedUser(sc).getEmail().equals(userEmail), "Permission denied");
		
		UUID userId = null;
		if (notEmpty(userEmail)) {
			if (UUID.isValid(userEmail))
				userId = new UUID(userEmail);
			else
				userId = User.getUserId(new Email(userEmail));
		}
		
		Setting s = SettingFactory.getSetting(userId, settingId);
		
		if (s == null)
			return Response
					.noContent()
					.build();
		
		if (s.getUserId() != null)
			validateUnauthorized(isAdmin(sc) || getLoggedUser(sc).getUserId().equals(s.getUserId()), "Permission denied");
		
		return Response
				.ok()
				.entity(JsonUtils.objectToString(s))
				.build();
	}
	
	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSetting(@Context SecurityContext sc,
			@QueryParam("id") String settingId,
			@QueryParam("userEmail") String userEmail,
			@QueryParam("name") String settingName) throws IOException, ValidationException {
		
		UUID userId = null;
		if (notEmpty(userEmail)) {
			if (UUID.isValid(userEmail))
				userId = new UUID(userEmail);
			else
				userId = User.getUserId(new Email(userEmail));
		}
		
		Setting s = SettingFactory.getSetting(userId, settingId);
		
		if (s == null)
			return Response
					.ok()
					.entity(Json
							.object()
							.set("value", "")
							.toString())
					.build()
					;
		
		if (s.getUserId() != null)
			validateUnauthorized(isAdmin(sc) || getLoggedUser(sc).getUserId().equals(s.getUserId()), "Permission denied");
		
		return Response
				.ok()
				.entity(Json
						.object()
						.set("value", String.valueOf(s.getValue(settingName).getValue()))
						.toString())
				.build()
				;
	}
	
	@GET
	@Path("getSettingList")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSettingList(@Context SecurityContext sc,
			@QueryParam("filter") String filter,
			@QueryParam("from") int from,
			@QueryParam("size") int size,
			@QueryParam("userEmail") String userEmail) throws IOException, ValidationException {
		if (nullOrEmpty(userEmail))
			validateUnauthorized(isAdmin(sc), "Permission denied");
		else
			validateUnauthorized(isAdmin(sc) || getLoggedUser(sc).getEmail().equals(userEmail), "Permission denied");
		
		UUID userId = notEmpty(userEmail) ? User.getUserId(new Email(userEmail)) : null;
		
		Boolean system = notEmpty(userEmail) ? false : null;
		
		SearchResults<Setting> found = SettingFactory.searchSettings(userId, filter, system, from, size);
		
		Json settingsArray = Json.array();
		for (Setting s : found.getObjects()) {
			settingsArray.add(Json.read(JsonUtils.objectToString(s)));
		}
		
		mjson.Json ret = Json.object();
		ret.set("maxHits", found.maxHits());
		ret.set("settings", settingsArray);
		
		return Response
				.ok()
				.entity(ret.toString())
				.build();
	}
		
//	@GET
//	@Path("get")
//	@Produces(MediaType.APPLICATION_JSON)
//	public String get(@Context SecurityContext sc,
//			@QueryParam("propName") String propName,
//			@QueryParam("defaultValue") String defaultValue,
//			@QueryParam("userEmail") String userEmail) throws IOException, ValidationException {
//		validateUnauthorized(isAdmin(sc) || getLoggedUser(sc).getEmail().equals(userEmail), "Permission denied");
//		
//		String value = defaultValue != null ? defaultValue : "";
//		
//		if (StringUtilities.notEmpty(userEmail)) {
//			
//			User user = User.getUser(new Email(userEmail));
//			
//			if (user != null) {
//				value = BeansSettings.getSettingAsString(user.getUserId(), propName, defaultValue);
//			}
//			
//		
//		} else {
//		
//			AssertUtils.assertTrue(nullOrEmpty(propName) == false, "Property name cannot be empty");
//			
//			Property prop = PropertyFactory.getProperty(propName);
//			
//			if (prop == null)
//				return new HttpResponse()
//					.set("key", propName)
//					.set("name", propName)
//					.set("value", "")
//					.set("desc", "")
//					.toString();
//			
//			return new HttpResponse()
//				.setBody(propToJson(prop).toString())
//				.toString();
//		}
//	}
	
//	@POST
//	@Path("set")
//	public void set(@Context SecurityContext sc,
//			@FormParam("propName") String propName,
//			@FormParam("propValue") String propValue,
//			@FormParam("userEmail") String userEmail) throws IOException, ValidationException {
//		validateUnauthorized(isAdmin(sc) || getLoggedUser(sc).getEmail().equals(userEmail), "Permission denied");
//		
//		if (StringUtilities.notEmpty(userEmail)) {
//			
//			if (UUID.isValid(userEmail))
//				BeansSettings.putProperty(new UUID(userEmail), propName, propValue);
//			else
//				BeansSettings.putSetting(userEmail, propName, propValue);
//			
//		} else {
//			assertTrue(nullOrEmpty(propName) == false, "Property name cannot be empty");
//			assertTrue(nullOrEmpty(propValue) == false, "Property value cannot be empty");
//			assertTrue(isAdmin(sc), "Global properties can be changed only by administrators");
//			
//			PropertyFactory.saveProperty(propName, propValue);
//
//			LibsLogger.debug(SettingsRest.class, format("Property %s with value %s saved successfully", propName, propValue));
//		}
//	}

	@POST
	@Path("registration")
	@RolesAllowed(Rights.ADMIN)
	@Produces(MediaType.APPLICATION_JSON)
	public Response registrationSet(@Context SecurityContext sc,
			@FormParam("registration-enable") boolean registrationEnabled) throws IOException, ValidationException {
		validateUnauthorized(isAdmin(sc), "Permission denied");
		
		BeansSettings.setRegistrationEnabled(registrationEnabled);
		
		LibsLogger.debug(SettingsRest.class, format("New value for WeblibsConst.REGISTRATION_ENABLED: %b", WeblibsConst.REGISTRATION_ENABLED));
		
		return Response
				.ok()
				.entity("OK")
				.build();
	}
	
	@GET
	@Path("registration")
	@Produces(MediaType.APPLICATION_JSON)
	public Response registrationGet(@Context SecurityContext sc) throws IOException, ValidationException {
		boolean registrationOn = BeansSettings.isRegistrationEnabled();
		
		return Response
				.ok()
				.entity(Json
						.object()
						.set("registration", registrationOn)
						.toString())
				.build();
	}
	
	@POST
	@Path("mail/conf")
	@RolesAllowed(Rights.ADMIN)
	@Produces(MediaType.APPLICATION_JSON)
	public void saveMailConf(@Context SecurityContext sc,
			@FormParam("email-configuration-host") String host,
			@FormParam("email-configuration-username") String username,
			@FormParam("email-configuration-pass") String pass,
			@FormParam("email-configuration-port") int port) throws IOException, ValidationException {
		validateUnauthorized(isAdmin(sc), "Permission denied");
		
		host = nullOrEmpty(host) ? "localhost" : host;
		port = port <= 0 ? 25 : port;
		
		assertTrue(notEmpty(host), "Host cannot be empty");
		assertTrue(port > 0, "Port must be > 0");
		
		MailCredentials mc = new MailCredentials(host, port, username, pass, true, 20000);
		
		LibsLogger.debug(SettingsRest.class, "First testing email configuration " + mc);
		
		try {
			Setting mailSett = SettingFactory.getSetting("MAIL_CONFIGURATION");
			mailSett.getValue("host").setValue(host);
			mailSett.getValue("port").setValue(port);
			mailSett.getValue("username").setValue(username);
			mailSett.getValue("pass").setValue(pass);
			mailSett.save();
		} catch (IOException e) {
			throw e;
		} catch (Exception e) {
			throw new IOException("Cannot open email transport with following settings " + mc.toString(), e);
//			return "FAIL";
		} finally {
			if (mc != null)
				mc.closeTransport();
		}
	}
	
	@GET
	@Path("mail/conf")
	@RolesAllowed(Rights.ADMIN)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMailConf(@Context SecurityContext sc) throws IOException, ValidationException {
		validateUnauthorized(isAdmin(sc), "Permission denied");
		
		final MailCredentials confFromDb = BeansSettings.getMailCredentials();
		
		mjson.Json conf = Json.object();
		conf.set("host", confFromDb.getHost());
		conf.set("port", confFromDb.getPort());
		conf.set("from", confFromDb.getFrom());
		conf.set("pass", confFromDb.getPass());
		conf.set("ssl", confFromDb.isSsl());
		conf.set("timeoutMs", confFromDb.getTimeoutMs());
		
		return Response
				.ok()
				.entity(conf.toString())
				.build();
	}

	@POST
	@Path("mail/test")
	@RolesAllowed(Rights.ADMIN)
//	@Produces(MediaType.APPLICATION_JSON)
	public void saveMailTest(@Context SecurityContext sc) throws IOException, ValidationException {
		validateUnauthorized(isAdmin(sc), "Permission denied");
		
//		LibsLogger.debug(SettingsRest.class, "Testing email configuration " + mc);
		
		try {
			
//			new MailTest().testSend();
			
			User user = UserFactory.getUser(getUserId(sc));
			
			Mailer.sendMail(BeansSettings.getMailCredentials(), 
					user.getEmail().getEmail(), 
					null, 
					null, 
					"Test email from BEANS", 
					"This is just a test email sent on " 
							+ (new SimpleDate().toStringHuman()) 
							+ " to test whether email configuration is properly set up. \n\n"
							+ "If you see this message it means it is.", 
					false, 
					null);
			
		} catch (Exception e) {
			throw new IOException("Cannot send an email", e);
		}
	}
}
