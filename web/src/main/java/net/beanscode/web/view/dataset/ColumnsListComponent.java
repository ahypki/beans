package net.beanscode.web.view.dataset;

import static net.hypki.libs5.utils.string.RandomUtils.nextInt;
import static org.rendersnake.HtmlAttributesFactory.class_;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.SecurityContext;

import org.rendersnake.HtmlAttributesFactory;
import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

import net.beanscode.cli.BeansCLISettings;
import net.beanscode.cli.datasets.Table;
import net.beanscode.cli.datasets.TableGateway;
import net.beanscode.cli.notebooks.Notebook;
import net.beanscode.cli.notebooks.NotebookGateway;
import net.beanscode.cli.notebooks.NotebookSearchResults;
import net.beanscode.pojo.tables.ColumnDef;
import net.beanscode.web.view.notebook.NotebooksTableComponent;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.Link;
import net.beanscode.web.view.ui.ListComponent;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.jersey.SessionBean;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.Op;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpReplace;
import net.hypki.libs5.search.SearchResults;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.RandomUtils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.url.Params;

public class ColumnsListComponent extends ListComponent {
	
	@Expose
	private OpList onClick = null;
	
	@Expose
	private UUID tableId = null;
	
	private Table tableCache = null;
	private List<ColumnDef> selectedColumns = null;

	public ColumnsListComponent() {
		
	}
	
	public ColumnsListComponent(BeansComponent parent) {
		super(parent);
	}
	
	public ColumnsListComponent(SecurityContext securityContext, Params params) {
		super(securityContext, params);
	}
	
	protected Table getTable() {
		if (tableCache == null) {
			try {
				tableCache = TableGateway.getTable(this.getSessionBean(), getTableId().getId());
			} catch (CLIException | IOException e) {
				LibsLogger.error(NotebooksTableComponent.class, "Cannot search for tables", e);
			}
		}
		return tableCache;
	}
	
	public List<ColumnDef> getSelectedColumns() {
		if (this.selectedColumns == null) {
			this.selectedColumns = new ArrayList<ColumnDef>();
			String [] filters = getFilter() != null ? getFilter().toLowerCase().split("[\\s]+") : null;
			
			if (getTable().getColumns() != null)
				for (ColumnDef col : getTable().getColumns()) {
					String colName = col.getName();
					String colDescription = col.getDescription();
					String colType = col.getType().toString();
					
					if (filters == null
							|| filterFulfilled(filters, colName, colDescription, colType)) {
						this.selectedColumns.add(col);
					}	
				}
		}
		return selectedColumns;
	}
	
	private static boolean filterFulfilled(String [] filters, String colName, String colDescription, String colType) {
		for (String filter : filters) {
			if (StringUtilities.nullOrEmpty(filter) == false
					&& colName.toLowerCase().contains(filter) == false
					&& (colDescription == null || (colDescription != null && colDescription.toLowerCase().contains(filter) == false))
					&& colType.toLowerCase().contains(filter) == false)
				return false;
		}
		return true;
	}
	
	@Override
	protected String getSettingsPrefix() {
		return "table-columns-list-";
	}
	
	@Override
	protected String getFilterTooltip() {
		return "Filter columns...";
	}
	
	@Override
	protected long getMaxHits() throws IOException {
		return getSelectedColumns().size();
	}
	
	@Override
	protected void renderItem(int row, HtmlCanvas html) throws IOException {
		row = row + (getPage() * getPerPage());
		ColumnDef n = getSelectedColumns().get(row);
		
		html
//			.span(class_("").add("style", "background-color: rgb(" + nextInt(255) + "," + nextInt(255) + "," + nextInt(255) + ")"))
//				.content(new Link(n.getName(), getOnClick().addParamParam("notebookId", n.getId().getId())).toHtml(), false);
			.render(new Link(n.getName(), new OpComponentClick(this, 
					"onColumn",
					new Params()
						.add("row", row)))
					.setHtmlClass("row-idx-" + row))
			.span()
				.content(" ");
	}
	
	private OpList onColumn() throws IOException {
		int row = getParams().getInteger("row", 0);
//		row = row + (getPage() * getPerPage());
		ColumnDef n = getSelectedColumns().get(row);
		
		HtmlCanvas html = new HtmlCanvas();
		html
			.b()
				.content(n.getName())
			.span()
				.content(" [")
			.span()
				.content("" + n.getType() + ", ")
			.span()
				.content(n.getDescription())
			.span()
				.content("]");
		
		return new OpList()
				.add(new OpReplace("#" + getId() + " .row-idx-" + row, html.toHtml()));
	}

	public OpList getOnClick() {
		if (onClick == null)
			onClick = new OpList();
		return onClick;
	}

	public ColumnsListComponent setOnClick(OpList onClick) {
		this.onClick = onClick;
		return this;
	}
	
	public ColumnsListComponent setOnClick(Op onClick) {
		getOnClick().add(onClick);
		return this;
	}

	public UUID getTableId() {
		return tableId;
	}

	public ColumnsListComponent setTableId(UUID tableId) {
		this.tableId = tableId;
		return this;
	}
}
