package net.beanscode.cli.importing;

import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.File;
import java.io.IOException;

import net.beanscode.cli.BeansCliCommand;
import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.datasets.DatasetSearchResults;
import net.beanscode.pojo.dataset.Dataset;
import net.beanscode.web.BeansWebConst;
import net.hypki.libs5.cli.noninteractivecli.CliArg;
import net.hypki.libs5.pjf.RestMethod;
import net.hypki.libs5.pjf.components.jersey.SessionBean;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileUtils;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.url.HttpResponse;
import net.hypki.libs5.utils.url.Params;
import net.hypki.libs5.utils.url.UrlUtilities;

public class ImportFile extends BeansCliCommand {
		
	@CliArg(name = "file",
			description = "path of the file to import",
			required = true)
	private String file = null;
	
	@CliArg(name = "dataset",
			description = "ID or other query for the dataset, where file should be imported",
			required = true)
	private String dataset = null;
	
	@CliArg(name = "create-dataset",
			description = "ID or other query for the dataset, where file should be imported",
			required = false)
	private boolean createDataset = false;
	
	@CliArg(name = "table-name",
			description = "name of the table in dataset, where file will be imported",
			required = true)
	private String tableName = null;
	
	@CliArg(name = "clear-first",
			description = "if true then the table is cleared first before importing",
			required = false)
	private boolean clearFirst = false;

	public ImportFile(SessionBean sb) {
		super(RestMethod.POST, sb);
	}
	
	@Override
	public String getCLICommandName() {
		return "import";
	}
	
	@Override
	public String getCLICommandDescription() {
		return "imports file to existing dataset";
	}
	
	@Override
	public String getUrlPath() {
		return "api/file/upload";
	}
	
	@Override
	public Params getRESTParams() {
		return new Params()
			.add("dataset", getDataset())
			.add("create-dataset", isCreateDataset())
			.add("table-name", getTableName())
			.add("clear-first", isClearFirst());
	}
	
	@Override
	protected HttpResponse post(Params params) throws IOException {
		assertTrue(FileUtils.exist(getFile()), "File " + getFile() + " must exist");
		
		DatasetSearchResults foundDatasets = new ApiCallCommand(getSessionBean())
								.setApi("api/dataset/query")
								.addParam("query", getDataset())
								.addParam("from", 0)
								.addParam("size", 10)
								.run()
								.getResponse()
								.getAsObject(DatasetSearchResults.class);
		
		// determine the datasetId - based on query
		for (Dataset ds : foundDatasets.getDatasets()) {
			LibsLogger.debug(ImportFile.class, "Found dataset ", ds);
		}
		
		assertTrue(foundDatasets.getDatasets().size() <= 1, "Too many datasets found");
		
		String datasetId = null;
		if (foundDatasets.getDatasets().size() == 0) {
			if (isCreateDataset()) {
//				DatasetCreate create = new DatasetCreate(getSessionBean());
//				create.setUserLC(getUserLC());
//				create.run(new String[] {"--name", getDataset()});
//				datasetId = create.getDataset().getId().getId();
				
				Dataset dataset = new ApiCallCommand(this.getSessionBean())
							.setApi("/api/dataset/create")
							.addParam("name", getDataset())
							.run()
							.getResponse()
							.getAsObject(Dataset.class);
				datasetId = dataset.getId().getId();
			} else {
				assertTrue(foundDatasets.getDatasets().size() > 0, "No datasets found");
			}
		} else
			datasetId = foundDatasets.getDatasets().get(0).getId().getId();
				
		// upload file
		return UrlUtilities.post(getURL(), 
				new String[] {BeansWebConst.COOKIE_LOGIN, getUserLC()}, 
				new File[]{new File(getFile())},
				new String[] {"datasetId", datasetId, 
						"tableName", getTableName()}
				);
	}
	
	@Override
	public void processResponse() {
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public String getDataset() {
		return dataset;
	}

	public void setDataset(String dataset) {
		this.dataset = dataset;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public boolean isClearFirst() {
		return clearFirst;
	}

	public void setClearFirst(boolean clearFirst) {
		this.clearFirst = clearFirst;
	}

	public boolean isCreateDataset() {
		return createDataset;
	}

	public void setCreateDataset(boolean createDataset) {
		this.createDataset = createDataset;
	}

	
//	@Override
//	public CLICommand run(HashMap<String, Arg> args) throws CLIException {
//		try {
//			String filename = args.get(PARAM_FILE).stringValue();
//			String tableName = args.get(PARAM_TABLE_NAME).stringValue();
//			List<Dataset> datasets = MoccaSearchManager.searchDatasets(args.get(PARAM_DATASET).stringValue());
//			
//			if (datasets.size() > 1) {
//				error("Too many datasets found:");
//				for (Dataset dataset : datasets) {
//					error("\t" + dataset.toString());
//				}
//				return this;
//			} else if (datasets.size() == 0) {
//				error("No datasets found");
//				return this;
//			}
//			
//			Dataset ds = datasets.get(0);
//			Table table = Table.getTable(ds.getId(), tableName);
//			
//			if (table != null && table.isTableEmpty() == false) {
//				error(format("Table %s exists in dataset %s and already contains data", table.getName(), ds.getName()));
//				return this;
//			} else if (table == null) {
//				table = new Table(ds, tableName);
//			}
//			
//			table.importFile(new File(filename));
//			
//		} catch (IOException | ValidationException e) {
//			LibsLogger.error(ImportFile.class, "Cannot search for datasets or tables", e);
//		}
//		return this;
//	}
}
