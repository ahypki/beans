package net.beanscode.model.unittests.connectors;

import net.beanscode.model.connectors.ConnectorList;
import net.beanscode.model.connectors.PlainConnector;
import net.beanscode.model.tests.BeansTestCase;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;

import org.junit.Test;

public class ConnectorDataUnitTest extends ConnectorTestCase {

	@Test
	public void testReadingFromTwoConnectors() {
		final FileExt source1 = new FileExt("src/main/resources/testdata/line-1-10.json");
		final FileExt source2 = new FileExt("src/main/resources/testdata/line-5-25.json");
		
		int counter = 0;
		for (Row row : new ConnectorList(new PlainConnector(source1), new PlainConnector(source2))) {
			LibsLogger.debug(ConnectorDataUnitTest.class, "Row: ", row);
			counter++;
		}
		
		assertTrue(counter == 31, "Expected to read 30 rows but read " + counter + " rows");
		
		final int sum1 = (int) sumColumn(new PlainConnector(source1), "x");
		final int sum2 = (int) sumColumn(new PlainConnector(source2), "x");
		final int sum12 = (int) sumColumn(new ConnectorList(new PlainConnector(source1), new PlainConnector(source2)), "x");
		LibsLogger.debug(ConnectorDataUnitTest.class, "Sum1: ", sum1, " sum2", sum2);
		assertTrue((sum1 + sum2) == sum12, "Cannot read all rows from two connectors");
	}
}
