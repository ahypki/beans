package net.beanscode.model.dataset;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.NotImplementedException;

import net.beanscode.model.BeansSearchManager;
import net.beanscode.model.notebook.NotebookEntry;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.search.SearchResults;
import net.hypki.libs5.search.query.Bracket;
import net.hypki.libs5.search.query.Query;
import net.hypki.libs5.search.query.StringTerm;
import net.hypki.libs5.search.query.Term;
import net.hypki.libs5.search.query.TermRequirement;
import net.hypki.libs5.search.query.WildcardTerm;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.weblibs.CacheManager;
import net.hypki.libs5.weblibs.SearchManager;
import net.hypki.libs5.weblibs.WeblibsConst;

public class TableFactory {
	
	public static Iterable<Table> iterateTables() throws IOException {
		final Iterator<Row> rowIter = DbObject.getDatabaseProvider().tableIterable(WeblibsConst.KEYSPACE, Table.COLUMN_FAMILY).iterator();
		
		return new Iterable<Table>() {
			@Override
			public Iterator<Table> iterator() {
				return new Iterator<Table>() {	
					@Override
					public Table next() {
						return JsonUtils.fromJson(rowIter.next().getAsString(DbObject.COLUMN_DATA), Table.class);
					}
					
					@Override
					public boolean hasNext() {
						return rowIter.hasNext();
					}
				};
			}
		};
	}
	
//	public static Iterable<Table> iterateTables(ButcheredPigEntry pigScript) throws IOException {
//		return iterateTables(pigScript.getNotebookId(), pigScript.getId());
//	}
	
	public static Iterable<Table> iterateTables(NotebookEntry pigScript) throws IOException {
		return iterateTables(pigScript.getNotebookId(), pigScript.getId());
	}

	public static Iterable<Table> iterateTables(final UUID notebookId, final UUID queryId) throws IOException {
		return new Iterable<Table>() {
			@Override
			public Iterator<Table> iterator() {
				return new Iterator<Table>() {
					
					private Iterator<Table> iter = null;
					
					private Table tableCache = null;
					
					private Iterator<Table> getIter() {
						if (iter == null) {
							try {
								Dataset ds = DatasetFactory.getDataset(notebookId);
								if (ds != null)
									iter = ds.iterateTables().iterator();
							} catch (IOException e) {
								LibsLogger.error(TableFactory.class, "Cannot create and run iterator over tables", e);
							}
						}
						return iter;
					}
					
					@Override
					public Table next() {
						return tableCache;
					}
					
					@Override
					public boolean hasNext() {
						if (getIter() == null)
							return false;
						
						while (getIter().hasNext()) {
							Table t = getIter().next();
							
							if (t.getMeta().get("queryId") != null
									&& t.getMeta().get("queryId").getValue().equals(queryId.getId())) {
								tableCache = t;
								return true;
							}
						}
						return false;
					}
				};
			}
		};
	}
	
	public static void clearCache(UUID tableId) {
//		CacheManager.cacheInstance().put(Table.COLUMN_FAMILY, table.getId().getId(), table.getData());
		CacheManager.cacheInstance().remove(Table.COLUMN_FAMILY, tableId.getId());
	}
	
	public static Table getTable(UUID tableId) throws IOException {
		return getTable(tableId, true);
	}

	public static Table getTable(UUID tableId, boolean useCache) throws IOException {
		Table t = null;
		
		if (useCache) {
			t = getFromCache(tableId);
			
			if (t != null)
				return t;
		}
		
		t = DbObject.getDatabaseProvider().get(WeblibsConst.KEYSPACE, 
				Table.COLUMN_FAMILY, 
				new C3Where()
						.addClause(new C3Clause(DbObject.COLUMN_PK, ClauseType.EQ, tableId.getId())), 
				DbObject.COLUMN_DATA, 
				Table.class);
		
		if (t != null)
			updateCache(t);
		
		return t;
	}
	
	private static Table getFromCache(UUID tableId) {
		return CacheManager
			.cacheInstance()
			.get(Table.class, Table.COLUMN_FAMILY, tableId.getId());
	}

	public static void updateCache(Table table) {
		if (table == null)
			return;
		
		CacheManager
			.cacheInstance()
			.put(Table.COLUMN_FAMILY, table.getId().getId(), table);
	}

	public static SearchResults<Table> searchTables(UUID userId, final String datasetsQuery, final String tableQuery, int from, int size) throws IOException {
		Query dsQ = BeansSearchManager.parseDatasetsQuery(datasetsQuery, userId, "dataset");
		Query tbQ = Query.parseQuery(tableQuery);
		Query esQuery = new Query();
		
		// validation
		if (size <= 0)
			size = 20;
		if (from < 0)
			from = 0;
		
		// parsing query
		if (dsQ != null
				&& dsQ.getTerms().size() == 2
				&& dsQ.isShouldAll()) {
			esQuery.addTerm(new Bracket(TermRequirement.MUST)
							.addTerms(dsQ.getTerms()));
		} else {
			for (Term t : dsQ.getTerms()) {
				esQuery.addTerm(t);
			}
		}
		
		if (tbQ.getTerms().size() > 0) {
			Bracket tbBracket = new Bracket(TermRequirement.MUST);
			for (Term term : tbQ.getTerms()) {
				if (term.getField() == null) {
					if (term instanceof WildcardTerm) {
						WildcardTerm wild = (WildcardTerm) term;
						tbBracket.addTerm(new Bracket(wild.getRequirement())
											.addTerm(new WildcardTerm("name", wild.getTerm(), TermRequirement.SHOULD))
											.addTerm(new WildcardTerm("id.id", wild.getTerm(), TermRequirement.SHOULD))
	//										.addTerm(new WildcardTerm("description", wild.getTerm(), TermRequirement.SHOULD))
								);
					}
				} else {
					term.setField("meta.meta." + term.getField() + ".value");
					tbBracket.addTerm(term);
				}
			}
			esQuery.addTerm(tbBracket);
		}
		
		if (tbQ.getTerms().size() == 0)
			esQuery.addTerm(new WildcardTerm("name", "*", TermRequirement.SHOULD));
		
		if (userId != null)
			BeansSearchManager.addPermissionTerms(esQuery, userId);
				
		return SearchManager.searchInstance().search(Table.class, 
				WeblibsConst.KEYSPACE_LOWERCASE, 
				Table.COLUMN_FAMILY.toLowerCase(), 
				esQuery, from, size);
	}

	public static SearchResults<Table> searchTables(UUID userId, UUID datasetId, String tablebQuery, int from, int size) throws IOException {
			Query tbQ = Query.parseQuery(tablebQuery);
			Query esQuery = new Query();
			
			// validation
			if (size <= 0)
				size = 20;
			if (from < 0)
				from = 0;
			
			// parsing query
			for (Term term : tbQ.getTerms()) {
				if (term.getField() == null) {
					if (term instanceof WildcardTerm) {
						WildcardTerm wild = (WildcardTerm) term;
						esQuery.addTerm(new Bracket(TermRequirement.MUST)
											.addTerm(new WildcardTerm("name", wild.getTerm(), TermRequirement.SHOULD))
											.addTerm(new WildcardTerm("id.id", wild.getTerm(), TermRequirement.SHOULD))
	//										.addTerm(new WildcardTerm("description", wild.getTerm(), TermRequirement.SHOULD))
								);
					}
				} else {
					term.setField("meta.meta." + term.getField() + ".value");
					esQuery.addTerm(term);
				}
			}
			
			if (tbQ.getTerms().size() == 0)
				esQuery.addTerm(new WildcardTerm("name", "*", TermRequirement.SHOULD));
			
			if (userId != null)
				BeansSearchManager.addPermissionTerms(esQuery, userId);
			
			if (datasetId != null)
				esQuery.addTerm(new StringTerm("datasetId.id", datasetId.getId(), TermRequirement.MUST));
			
			return SearchManager.searchInstance().search(Table.class, 
					WeblibsConst.KEYSPACE_LOWERCASE, 
					Table.COLUMN_FAMILY.toLowerCase(), 
					esQuery, from, size);
		}

	public static Iterable<Table> iterateTables(final UUID userId, final UUID datasetId, final String keywords) {
		return new Iterable<Table>() {
			@Override
			public Iterator<Table> iterator() {
				return new Iterator<Table>() {
					private int from = 0;
					
					private int tablesIdx = 0;
					
					private List<Table> tables = null;
					
					@Override
					public void remove() {
						throw new NotImplementedException();
					}
					
					@Override
					public Table next() {
						return tables.get(tablesIdx++);
					}
					
					@Override
					public boolean hasNext() {
						try {
							if (tables == null || tables.size() == tablesIdx) {
								tables = searchTables(userId, datasetId, keywords, from, BeansSearchManager.PER_PAGE).getObjects();
								tablesIdx = 0;
								from += tables.size();
							}
							return tablesIdx < tables.size();
						} catch (IOException e) {
							LibsLogger.error(BeansSearchManager.class, "Cannot get the next page with results", e);
							return false;
						}
					}
				};
			}
		};
	}

	public static Iterable<Table> iterateTables(final UUID userId, final String datasetQuery, final String tableQuery) {
		return new Iterable<Table>() {
			@Override
			public Iterator<Table> iterator() {
				return new Iterator<Table>() {
					private int from = 0;
					
					private int tablesIdx = 0;
					
					private List<Table> tables = null;
					
					@Override
					public void remove() {
						throw new NotImplementedException();
					}
					
					@Override
					public Table next() {
						if (hasNext())
							return tables.get(tablesIdx++);
						else
							return null;
					}
					
					@Override
					public boolean hasNext() {
						try {
							if (tables == null || tables.size() == tablesIdx) {
								LibsLogger.debug(BeansSearchManager.class, "iterateTables iter ", userId, datasetQuery, tableQuery, from, BeansSearchManager.PER_PAGE);
								tables = searchTables(userId, datasetQuery, tableQuery, from, BeansSearchManager.PER_PAGE).getObjects();
								tablesIdx = 0;
								from += tables.size();
							}
							return tablesIdx < tables.size();
						} catch (IOException e) {
							LibsLogger.error(BeansSearchManager.class, "Cannot get the next page with results", e);
							return false;
						}
					}
				};
			}
		};
	}
}
