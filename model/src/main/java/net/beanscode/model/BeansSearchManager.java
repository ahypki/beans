package net.beanscode.model;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import net.beanscode.model.cass.notification.Notification;
import net.beanscode.model.cass.notification.NotificationFactory;
import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.DatasetFactory;
import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.notebook.NotebookFactory;
import net.beanscode.model.rights.Group;
import net.beanscode.model.rights.GroupFactory;
import net.beanscode.model.settings.RebuildSearchIndexJob;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.search.SearchEngineProvider;
import net.hypki.libs5.search.SearchResults;
import net.hypki.libs5.search.elastic.ElasticSearchProvider;
import net.hypki.libs5.search.lucene.LuceneSearchEngineProvider;
import net.hypki.libs5.search.query.Bracket;
import net.hypki.libs5.search.query.Query;
import net.hypki.libs5.search.query.SortOrder;
import net.hypki.libs5.search.query.StringTerm;
import net.hypki.libs5.search.query.Term;
import net.hypki.libs5.search.query.TermRequirement;
import net.hypki.libs5.search.query.WildcardTerm;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.reflection.ReflectionUtility;
import net.hypki.libs5.weblibs.SearchManager;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.search.IndexUserOperation;
import net.hypki.libs5.weblibs.settings.Setting;
import net.hypki.libs5.weblibs.settings.SettingFactory;
import net.hypki.libs5.weblibs.user.User;
import net.hypki.libs5.weblibs.user.UserFactory;

import org.apache.commons.lang.NotImplementedException;

public class BeansSearchManager {
	
	private static SearchEngineProvider searchManager = null;
	
	public static int PER_PAGE = 1000;
	private static int PER_PAGE_1000 = 1000;
	
	public static SearchEngineProvider getSearchManager() {
		if (searchManager == null) {
			String clazzName = LocalSettings.getSettingLocalAsString("SearchEngineProvider/class");
			Class searchEngineClass = ReflectionUtility.findClass(clazzName);
			
			if (searchEngineClass == null && clazzName != null && clazzName.toLowerCase().contains("Lucene".toLowerCase()))
				searchEngineClass = LuceneSearchEngineProvider.class;
			else if (searchEngineClass == null && clazzName != null && clazzName.toLowerCase().contains("Elastic".toLowerCase()))
				searchEngineClass = ElasticSearchProvider.class;
			else if (searchEngineClass == null) {
				// default search engine: Lucene
				searchEngineClass = LuceneSearchEngineProvider.class;
			}
			
			setSearchManager(searchEngineClass);
		}
		return searchManager;
	}
	
	public static void setSearchManager(Class clazz) {
		if (searchManager != null)
			searchManager.close();
		
		try {
			LocalSettings.saveSettingLocal("SearchEngineProvider/class", clazz.getName());
		} catch (IOException e) {
			LibsLogger.error(BeansSearchManager.class, "Cannot save local setting SearchEngineProvider/class", e);
		}
		
		searchManager = null;
		
		LibsLogger.debug(BeansSearchManager.class, "Initializing search engine with class ", clazz.getName());
		SearchManager.init(clazz.getName());
		searchManager = SearchManager.searchInstance();
	}

	public static SearchResults<Group> searchGroups(UUID userId, String keywords, int from, int size) throws IOException {
		net.hypki.libs5.search.query.Query q = new net.hypki.libs5.search.query.Query();
		
		if (keywords != null)
			for (String term : keywords.toLowerCase().split("[^\\w]+")) {
				q.addTerm(new Bracket()
					.addTerm(new WildcardTerm("name", term, TermRequirement.SHOULD))
					.addTerm(new WildcardTerm("owner.id", term, TermRequirement.SHOULD))
					.addTerm(new WildcardTerm("groupId.id", term, TermRequirement.SHOULD)));
			}
		
		if (q.getTerms().size() == 0)
			q.addTerm(new WildcardTerm("name", "*", TermRequirement.SHOULD));
		
		q.addTerm(new StringTerm("owner.id", userId.getId(), TermRequirement.MUST));
//		q.setSortBy("creationMs");

		return SearchManager
				.searchInstance()
				.search(Group.class, 
						WeblibsConst.KEYSPACE_LOWERCASE, 
						Group.COLUMN_FAMILY.toLowerCase(), 
						q, 
						from, 
						size);
	}
	
	public static SearchResults<Group> searchGroupsWithMember(UUID userId, int from, int size) throws IOException {
		net.hypki.libs5.search.query.Query q = new net.hypki.libs5.search.query.Query();

		q.addTerm(new StringTerm("userIds.id", userId.getId(), TermRequirement.MUST));

		return SearchManager.searchInstance().search(Group.class, 
				WeblibsConst.KEYSPACE_LOWERCASE, 
				Group.COLUMN_FAMILY.toLowerCase(), 
				q,
				from, size);

	}

	public static Iterable<Group> iterateGroups(final UUID userId, final String groupQuery) {
		return new Iterable<Group>() {
			@Override
			public Iterator<Group> iterator() {
				return new Iterator<Group>() {
					private int from = 0;
					
					private int groupsIdx = 0;
					
					private List<Group> groups = null;
					
					@Override
					public void remove() {
						throw new NotImplementedException();
					}
					
					@Override
					public Group next() {
						return groups.get(groupsIdx++);
					}
					
					@Override
					public boolean hasNext() {
						try {
							if (groups == null || groups.size() == groupsIdx) {
								groups = searchGroups(userId, groupQuery, from, PER_PAGE_1000).getObjects();
								groupsIdx = 0;
								from += groups.size();
							}
							return groupsIdx < groups.size();
						} catch (IOException e) {
							LibsLogger.error(BeansSearchManager.class, "Cannot get the next page with results", e);
							return false;
						}
					}
				};
			}
		};
	}
	
	public static Iterable<Group> iterateGroupsWithMember(final UUID userId) {
		return new Iterable<Group>() {
			@Override
			public Iterator<Group> iterator() {
				return new Iterator<Group>() {
					private int from = 0;
					
					private int groupsIdx = 0;
					
					private List<Group> groups = null;
					
					@Override
					public void remove() {
						throw new NotImplementedException();
					}
					
					@Override
					public Group next() {
						return groups.get(groupsIdx++);
					}
					
					@Override
					public boolean hasNext() {
						try {
							if (groups == null || groups.size() == groupsIdx) {
								groups = searchGroupsWithMember(userId, from, PER_PAGE_1000).getObjects();
								groupsIdx = 0;
								from += groups.size();
							}
							return groupsIdx < groups.size();
						} catch (IOException e) {
							LibsLogger.error(BeansSearchManager.class, "Cannot get the next page with results", e);
							return false;
						}
					}
				};
			}
		};
	}
	
	public static Query parseDatasetsQuery(final String queryToParse, final UUID userId, String datasetQueryPrefix) {
		Query q = Query.parseQuery(queryToParse);
		Query esQuery = new Query();
		
		datasetQueryPrefix = datasetQueryPrefix == null ? "" : datasetQueryPrefix;
		if (notEmpty(datasetQueryPrefix) && !datasetQueryPrefix.endsWith("."))
			datasetQueryPrefix += ".";
		
		for (Term term : q.getTerms()) {
			if (term.getField() == null) {
				if (term instanceof WildcardTerm) {
					WildcardTerm wild = (WildcardTerm) term;
					esQuery.addTerm(new Bracket(term.getRequirement())
										.addTerm(new WildcardTerm(datasetQueryPrefix + "name", wild.getTerm(), TermRequirement.SHOULD))
										.addTerm(new WildcardTerm(datasetQueryPrefix + "id.id", wild.getTerm(), TermRequirement.SHOULD))
										.addTerm(new WildcardTerm(datasetQueryPrefix + "description", wild.getTerm(), TermRequirement.SHOULD))
							);
				}
			} else {
				term.setField(datasetQueryPrefix + "meta.meta." + term.getField() + ".value");
				esQuery.addTerm(term);
			}
		}
		
		if (q.getTerms().size() == 0)
			esQuery.addTerm(new WildcardTerm(datasetQueryPrefix + "name", "*", TermRequirement.SHOULD));
		
		if (userId != null && nullOrEmpty(datasetQueryPrefix))
			addPermissionTerms(esQuery, userId);
		
		esQuery.setSortBy("editMs", SortOrder.DESCENDING);
		return esQuery;
	}
	
	public static void addPermissionTerms(Query esQuery, UUID userId) {
		Bracket br = new Bracket(TermRequirement.MUST)
			.addTerm(new Bracket(TermRequirement.SHOULD)
				.addTerm(new StringTerm("userId.id", userId.getId(), TermRequirement.MUST))
				.addTerm(new WildcardTerm("sharedGroup", "*", TermRequirement.MUST_NOT))
				);
				
		for (Group gr : BeansSearchManager.iterateGroupsWithMember(userId)) {
			br.addTerm(new StringTerm("sharedGroup", gr.getId().getId(), TermRequirement.SHOULD));
		}
		
		esQuery.addTerm(br);
	}
	
	public static void reloadIndexForUsers() throws IOException, ValidationException {
		getSearchManager().clearIndexType(WeblibsConst.KEYSPACE_LOWERCASE, User.COLUMN_FAMILY.toLowerCase());
		
		for (User user : UserFactory.iterateUsers()) {
			user.index();
//			try {
//				new IndexUserOperation(user).save();
//			} catch (Exception e) {
//				LibsLogger.error(RebuildSearchIndexJob.class, "Cannot reindex " + user, e);
//			}
		}
		LibsLogger.debug(BeansSearchManager.class, "All users reindexed");
	}
	
	public static void clearIndex() throws IOException, ValidationException {
		getSearchManager().clearIndex(WeblibsConst.KEYSPACE_LOWERCASE);
	}

	public static void reloadIndex() throws IOException, ValidationException {
//		getSearchManager().clearIndex(WeblibsConst.KEYSPACE_LOWERCASE);
		
		// reindex Notebooks
		for (Notebook notebook : NotebookFactory.iterateNotebooks()) {
			try {
				notebook.setReindexEntries(true);
				notebook.index();
			} catch (Exception e) {
				LibsLogger.error(RebuildSearchIndexJob.class, "Cannot reindex " + notebook, e);
			}
		}
		LibsLogger.debug(BeansSearchManager.class, "All notebooks reindexed");
		
		// redindex Datasets and Tables
		for (Dataset ds : DatasetFactory.iterateDatasets()) {
			try {
				ds.index();
			} catch (Exception e) {
				LibsLogger.error(RebuildSearchIndexJob.class, "Cannot reindex " + ds, e);
			}
		}
		LibsLogger.debug(BeansSearchManager.class, "All datasets and Tables reindexed");
					
		// reindex Users
		reloadIndexForUsers();
		
		// reindex Settings
		for (Setting sett : SettingFactory.iterateSettings()) {
			sett.index();
		}
		LibsLogger.debug(BeansSearchManager.class, "All settings reindexed");
		
		// reindex Notifications
		for (User user : UserFactory.iterateUsers()) {
			for (Notification notification : NotificationFactory.iterateNotifications(user.getUserId(), true, null)) {
				notification.index();
			}
		}
		LibsLogger.debug(BeansSearchManager.class, "All notifications reindexed");
		
		// reindex Groups
		for (Group group : GroupFactory.iterateGroups()) {
			group.index();
		}
		LibsLogger.debug(BeansSearchManager.class, "All groups reindexed");
	}
}
