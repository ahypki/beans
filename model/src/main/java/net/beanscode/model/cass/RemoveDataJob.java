package net.beanscode.model.cass;

import java.io.IOException;

import net.beanscode.model.BeansChannels;
import net.beanscode.model.BeansConst;
import net.beanscode.model.dataset.Table;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.jobs.Job;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class RemoveDataJob extends Job {
	
	@Expose
	@NotNull
	@NotEmpty
	private String dataId = null;

	public RemoveDataJob() {
		setChannel(BeansChannels.CHANNEL_LONG_HIDDEN);
	}
	
	public RemoveDataJob(String dataId) {
		setDataId(dataId);
		setChannel(BeansChannels.CHANNEL_LONG_HIDDEN);
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		DbObject.getDatabaseProvider().remove(WeblibsConst.KEYSPACE, Data.COLUMN_FAMILY, 
				new C3Where()
					.addClause(new C3Clause(Table.COLUMN_TBID, ClauseType.EQ, getDataId())));
	}

	public String getDataId() {
		return dataId;
	}

	public void setDataId(String dataId) {
		this.dataId = dataId;
	}
}
