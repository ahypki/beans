package net.beanscode.model.notebook;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.NotImplementedException;

import com.google.gson.annotations.Expose;

import net.beanscode.model.BeansDTNObject;
import net.beanscode.model.autostart.IncreaseFieldsCountJob;
import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.DatasetFactory;
import net.beanscode.pojo.tags.Tags;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.db.db.weblibs.MutationList;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.string.MetaList;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.weblibs.SearchManager;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.db.Indexable;
import net.hypki.libs5.weblibs.user.User;
import net.hypki.libs5.weblibs.user.UserFactory;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

public class Notebook extends BeansDTNObject<Notebook> implements Indexable {
	
	public final static String COLUMN_FAMILY = "notebook";
	
	private static boolean nrOfFieldsIncreased = false;

	@Expose
	@NotNull
	@NotEmpty
	private String name = null;
	
	@Expose
	private String description = null;
	
	@Expose
	private List<UUID> entries = null;
	
//	@Expose
//	@NotNull
//	@AssertValid
	private SimpleDate creationDate = null;
	
	@Expose
	private long lastEditMs = 0;
	
	@Expose
	private long creationMs = 0;
	
	private List<UUID> removedEntries = null;
	
	@Expose
	@AssertValid
	private MetaList meta = null;
	
	private boolean reindexEntries = false;
	
	@Expose
	private Tags tags = null;
	
	@Expose
	private UUID lastReloadedEntryId = null;
	
	public Notebook() {
		setId(UUID.random());
		setCreationDate(SimpleDate.now());
		setReindexEntries(false);
	}
	
	public Notebook(UUID userId, String name) {
		setId(UUID.random());
		setCreationDate(SimpleDate.now());
		setUserId(userId);
		setName(name);
		setReindexEntries(false);
	}
	
	@Override
	public String getColumnFamily() {
		return COLUMN_FAMILY;
	}
	
	@Override
	public String getKey() {
//		return getUserId().getId();
		return getId().getId();
	}
	
	@Override
	public MutationList getSaveMutations() {
		return super.getSaveMutations()
				.addInsertMutations(new NotebookIndexJob(getUserId(), getId(), isReindexEntries()))
				.addInsertMutations(isRemovedEntriesClear() ? null : new RemoveNotebookEntriesJob(this, getRemovedEntries()))
				.addInsertMutations(nrOfFieldsIncreased == false ? new IncreaseFieldsCountJob() : null)
				;
	}
	
//	public Notebook clone() {
//		return JsonUtils.fromJson(getData(), this.getClass());
//	}
	
	/**
	 * Makes a copy of this Notebook and all entries and changes all data
	 * that they are as new (e.g. they have unique IDs).
	 * @return
	 */
	public Notebook copyAsNew(UUID ownerUserId) {
		try {
			// clone notebook
			Notebook clonedNotebook = copy();
			
			clonedNotebook.setName(clonedNotebook.getName() + " (cloned)");
			clonedNotebook.setCreationDate(SimpleDate.now());
			clonedNotebook.setId(UUID.random());
			clonedNotebook.setUserId(ownerUserId);
			clonedNotebook.getEntries().clear();
			
			clonedNotebook.save();
			
			clonedNotebook.addAllEntries(this);
			
			return clonedNotebook;
		} catch (Exception e) {
			LibsLogger.error(Notebook.class, "Cannot prepare a clone of the entire Notebook", e);
			return null;
		}
	}
	
	@Override
	public Notebook save() throws ValidationException, IOException {
		setLastEditMs(System.currentTimeMillis());

		if (getCreationDate() == null)
			setCreationMs(getLastEdit().getTimeInMillis());
		
		super.save();
		
		try {
			indexThis();
		} catch (Exception e) {
			LibsLogger.error(Notebook.class, "Cannot index ad-hoc notebook", e);
		}
		
		nrOfFieldsIncreased = true;
		
		return this;
	}
	
	@Override
	public MutationList getRemoveMutations() {
		return super.getRemoveMutations()
				.addInsertMutations(new RemoveNotebookDependenciesJob(this));
	}
	
	@Override
	public void remove() throws ValidationException, IOException {
		super.remove();
		
		LibsLogger.debug(Notebook.class, "Notebook ", getName(), " removed");
		
		try {
			SearchManager.removeObject(getCombinedKey(), Notebook.COLUMN_FAMILY);
		} catch (Exception e) {
			LibsLogger.error(Notebook.class, "Cannot ad-hoc remove notebook from search index", e);
		}
	}
	
	@Override
	public void index() throws IOException {
		indexThis();
		
		if (isReindexEntries())
			indexEntries();
		
		indexPermissions();
		
		// reindexing dataset's tables too
		try {
			Dataset ds = DatasetFactory.getDataset(getId());
			if (ds != null
					&& ds.getName().equals(getName()) == false) {
				ds.setName(getName());
				ds.save();
			}
		} catch (Exception e) {
			LibsLogger.error(Notebook.class, "Cannot update Datasets name", e);
		}
	}
	
	private void indexThis() throws IOException {
		SearchManager.index(this);
	}
	
	private void indexEntries() throws IOException {
		for (UUID entryID : getEntries()) {
			NotebookEntry ne = NotebookEntryFactory.getNotebookEntry(entryID);
			if (ne != null)
				ne.index();
		}
	}
	
	@Override
	public void deindex() throws IOException {
		// do nothing, removing dependency objects works in the background
	}
	
	public static Notebook getNotebook(String notebookId) throws IOException {
		return getNotebook(new UUID(notebookId));
	}
	
	public static Notebook getNotebook(UUID notebookId) throws IOException {
		// TODO cache 
		return DbObject.getDatabaseProvider().get(WeblibsConst.KEYSPACE, 
				COLUMN_FAMILY, 
				new C3Where()
						.addClause(new C3Clause(DbObject.COLUMN_PK, ClauseType.EQ, notebookId.getId())), 
				DbObject.COLUMN_DATA, 
				Notebook.class);
	}

	public String getName() {
		return name;
	}

	public Notebook setName(String name) {
		if (StringUtilities.isTheSameNullSafe(this.name, name))
			setReindexEntries(true);
		this.name = name;
		return this;
	}

//	public UUID getId() {
//		return id;
//	}
//
//	public Notebook setId(UUID id) {
//		this.id = id;
//		return this;
//	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		if (StringUtilities.isTheSameNullSafe(this.description, description))
			setReindexEntries(true);
		this.description = description;
	}

	public List<UUID> getEntries() {
		if (entries == null)
			entries = new ArrayList<UUID>();
		return entries;
	}

	public void setEntries(List<UUID> entries) {
		this.entries = entries;
	}

	public boolean removeEntry(UUID entryId) {
		if (getEntries().remove(entryId)) {
			getRemovedEntries().add(entryId);
			return true;
		}
		return false;
	}
	
	public void clearEntries() {
		getRemovedEntries().addAll(getEntries());
		getEntries().clear();
	}
	
	private boolean isRemovedEntriesClear() {
		return removedEntries == null || removedEntries.size() == 0;
	}

	private List<UUID> getRemovedEntries() {
		if (removedEntries == null)
			removedEntries = new ArrayList<UUID>();
		return removedEntries;
	}

	private void setRemovedEntries(List<UUID> removedEntries) {
		this.removedEntries = removedEntries;
	}

	public SimpleDate getCreationDate() {
		return new SimpleDate(creationMs);
	}

	public void setCreationDate(SimpleDate creationDate) {
		if (this.creationMs != creationDate.getTimeInMillis())
			setReindexEntries(true);
		this.creationMs = creationDate.getTimeInMillis();
	}

	public boolean addEntryId(UUID entryId) {
		if (getEntries().contains(entryId) == false) {
			getEntries().add(entryId);
			return true;
		}
		return false;
	}
	
	public boolean addEntryId(int position, UUID entryId) {
		if (position < 0)
			position = 0;
		else if (position >= getEntries().size())
			position = getEntries().size();
		
		if (getEntries().contains(entryId) == false) {
			getEntries().add(position, entryId);
			return true;
		}
		return false;
	}

	public MetaList getMeta() {
		if (meta == null)
			meta = new MetaList();
		return meta;
	}

	public void setMeta(MetaList meta) {
		setReindexEntries(true);
		this.meta = meta;
	}
	
	public Iterable<NotebookEntry> iterateEntries() {
		return iterateEntries(null);
	}
	
	public Iterable<NotebookEntry> iterateEntries(final UUID fromEntryId) {
		return new Iterable<NotebookEntry>() {
			@Override
			public Iterator<NotebookEntry> iterator() {
				return new Iterator<NotebookEntry>() {
					private int uuidCounter = 0;
					private boolean rewinded = false;
					
					@Override
					public void remove() {
						throw new NotImplementedException();
					}
					
					@Override
					public NotebookEntry next() {
						try {
							if (hasNext())
								return NotebookEntryFactory.getNotebookEntry(getEntries().get(uuidCounter++));
							else
								return null;
						} catch (IOException e) {
							LibsLogger.error(Notebook.class, "Cannot get the next " + NotebookEntry.class.getSimpleName() + " in an iterator", e);
							return null;
						}
					}
					
					@Override
					public boolean hasNext() {
						if (fromEntryId != null
								&& rewinded == false) {
							for (int i = 0; i < getEntries().size(); i++) {
								if (getEntries().get(i).equals(fromEntryId)) {
									uuidCounter = i;
									break;
								}
							}
							rewinded = true;
						}
						return uuidCounter < getEntries().size();
					}
				};
			}
		};
	}

	public boolean moveUpEntry(UUID entryId) {
		int idx = getEntries().indexOf(entryId);
		if (idx == -1) {
			LibsLogger.error(Notebook.class, "Entry ", entryId, " not found in notebook ", getId(), ", cannot move the entry up");
			return false;
		}
		if (idx == 0)
			return false;
		getEntries().add(idx - 1, getEntries().get(idx));
		getEntries().remove(idx + 1);
		return true;
	}
	
	public boolean moveDownEntry(UUID entryId) {
		int idx = getEntries().indexOf(entryId);
		if (idx == -1) {
			LibsLogger.error(Notebook.class, "Entry ", entryId, " not found in notebook ", getId(), ", cannot move the entry up");
			return false;
		}
		if (idx == getEntries().size() - 1)
			return false;
		UUID tmp = getEntries().get(idx);
		getEntries().remove(idx);
		getEntries().add(idx + 1, tmp);
		return true;
	}

	public void update() throws ValidationException, IOException {
		new NotebookUpdateJob(this)
			.save();
	}

	public long getCreationMs() {
		return creationMs;
	}

	public void setCreationMs(long creationMs) {
		this.creationMs = creationMs;
	}
	
	public UUID getPreviousEntry(UUID referenceEntry) {
		int pos = getEntryPosition(referenceEntry);
		return pos > 0 ? getEntries().get(pos - 1) : null;
	}

	public int getEntryPosition(UUID entryId) {
		for (int i = 0; i < getEntries().size(); i++) {
			if (getEntries().get(i).equals(entryId))
				return i;
		}
		return getEntries().size();
	}

	public User getOwner() throws IOException {
		return UserFactory.getUser(getUserId());
	}
	
	public void reloadClear() throws IOException, ValidationException {
		for (NotebookEntry ne : iterateEntries()) {
			ne
				.getProgress()
				.running(0.0, "Running...")
				.save();
		}
		
		setLastReloadedEntryId(getEntries().size() > 0 ? getEntries().get(0) : null);
		save();
	}
	
	/**
	 * 
	 * @return If 'true' returned then reload is not complete, 'false' it is complete
	 * @throws IOException 
	 * @throws ValidationException 
	 */
	public ReloadPropagator reloadStep() throws IOException, ValidationException {
		// going over all entries and reloading if needed
		for (NotebookEntry ne : iterateEntries(getLastReloadedEntryId())) {
			if (ne.getStatus() == NotebookEntryStatus.SUCCESS
							|| ne.getStatus() == NotebookEntryStatus.FAIL) {
				
				// entry failed or succeeded, go to the next entry
				setLastReloadedEntryId(ne.getId());
				continue;
				
			} else if (ne.getStatus() == NotebookEntryStatus.NEW) {
				
				// starting the reload for the entry 
				ReloadPropagator prop = ne.reload();
				if (prop == ReloadPropagator.BLOCK) {
					ne
						.getProgress()
						.running(0.0, "Running...");
//					getEntryStatus().put(ne.getId(), NotebookEntryStatus.RUNNING);
					LibsLogger.debug(Notebook.class, "Reloading ", ne.getName(), " blocks other "
							+ "entries, waiting...");
					// stop this function, one has to wait for the entry to finish
					setLastReloadedEntryId(ne.getId());
					save();
					return ReloadPropagator.BLOCK;
				} else {
					setLastReloadedEntryId(ne.getId());
					continue; // go to the next entry
				}
				
			} else if (ne.getStatus() == NotebookEntryStatus.RUNNING) {
				
				// still running, postponin
				setLastReloadedEntryId(ne.getId());
				save();
				return ReloadPropagator.BLOCK;
			
			}
		}
		save();
		return ReloadPropagator.NONBLOCK;
	}

	public void reload() throws ValidationException, IOException {
		reloadClear();
		
		new NotebookReloadJob(this)
			.save();
	}

	private boolean isReindexEntries() {
		return reindexEntries;
	}

	public void setReindexEntries(boolean reindexEntries) {
		this.reindexEntries = reindexEntries;
	}

	public List<UUID> addAllEntries(Notebook notebookToClone) throws ValidationException, IOException {
		// clone all fields
		List<NotebookEntry> toSave = new ArrayList<NotebookEntry>();
		List<UUID> clonedEntriesIds = new ArrayList<UUID>();
		
		for (NotebookEntry entry : notebookToClone.iterateEntries()) {
			NotebookEntry clonedEntry = entry.cloneAsNew(this.getId());
			
			toSave.add(clonedEntry);
			clonedEntriesIds.add(clonedEntry.getId());
			
			this.addEntryId(clonedEntry.getId());
		}
		
		for (NotebookEntry entry : toSave) {				
			entry.save();
		}
		
		this.save();
		
		return clonedEntriesIds;
	}

	public void order(List<UUID> entryId) {
		int newIdx = 0;
		for (UUID id : entryId) {
			int currentIdx = getEntries().indexOf(id);
			
			UUID tmp = getEntries().get(newIdx);
			getEntries().set(newIdx, getEntries().get(currentIdx));
			getEntries().set(currentIdx, tmp);
			
			newIdx++;
		}
	}
	
	public SimpleDate getLastEdit() {
		return new SimpleDate(getLastEditMs());
	}

	public long getLastEditMs() {
		return lastEditMs;
	}

	public void setLastEditMs(long lastEditMs) {
		this.lastEditMs = lastEditMs;
	}

	public Tags getTags() {
		if (tags == null)
			tags = new Tags();
		return tags;
	}

	public void setTags(Tags tags) {
		this.tags = tags;
	}

	private UUID getLastReloadedEntryId() {
		return lastReloadedEntryId;
	}

	private void setLastReloadedEntryId(UUID lastReloadedEntryId) {
		this.lastReloadedEntryId = lastReloadedEntryId;
	}

//	public String getTagsjoined() {
//		return tagsjoined;
//	}
//
//	public void setTagsjoined(String tagsjoined) {
//		this.tagsjoined = tagsjoined;
//	}
}
