package net.beanscode.model;

import java.io.IOException;

import net.beanscode.model.dataset.Dataset;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.db.db.weblibs.MutationList;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.reflection.ReflectionUtility;
import net.hypki.libs5.weblibs.SearchManager;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.db.Indexable;
import net.hypki.libs5.weblibs.db.WeblibsObject;
import net.hypki.libs5.weblibs.db.WeblibsObjectIndexable;
import net.hypki.libs5.weblibs.search.DeindexJob;
import net.hypki.libs5.weblibs.search.IndexJob;

public abstract class BeansObject<T> extends WeblibsObjectIndexable<T> implements Indexable {

	public static <T extends BeansDTNObject> T getBeansObject(UUID objectId, String objectClass) throws IOException {
		try {
			Class<T> clazz = (Class<T>) Class.forName(objectClass);
			
			return DbObject.getDatabaseProvider().get(WeblibsConst.KEYSPACE,
					ReflectionUtility.getStaticFieldValue(clazz, "COLUMN_FAMILY"), 
					new C3Where()
							.addClause(new C3Clause(DbObject.COLUMN_PK, ClauseType.EQ, objectId.getId())), 
					DbObject.COLUMN_DATA, 
					clazz);
		} catch (ClassNotFoundException e) {
			throw new IOException("Cannot get BeansObject from DB", e);
		}
	}
}
