package net.beanscode.model.dataset;

import java.io.IOException;

import net.beanscode.model.BeansChannels;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.jobs.Job;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class ReindexDataset extends Job {
	
	@Expose
	@NotNull
	@NotEmpty
	private UUID userId = null;
	
	@Expose
	@NotNull
	@NotEmpty
	private UUID datasetId = null;
	
	public ReindexDataset() {
		super(BeansChannels.CHANNEL_INDEX);
	}
	
	public ReindexDataset(Dataset ds) {
		super(BeansChannels.CHANNEL_INDEX);
		setUserId(ds.getUserId());
		setDatasetId(ds.getId());
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		try {
			Dataset ds = DatasetFactory.getDataset(getDatasetId());
			
			if (ds == null) {
				LibsLogger.error(ReindexDataset.class, "Dataset ", getDatasetId(), " does not exist anymore");
				return;
			}
			
			ds.index();
			
			for (Table table : ds.iterateTables()) {
				table.index();
			}
			
		} catch (Throwable t) {
			LibsLogger.error(ReindexDataset.class, "Cannot reindex dataset", t);
			throw new IOException("Cannot reindex dataset", t);
		}
	}

	public UUID getDatasetId() {
		return datasetId;
	}

	public void setDatasetId(UUID datasetId) {
		this.datasetId = datasetId;
	}

	public UUID getUserId() {
		return userId;
	}

	public void setUserId(UUID userId) {
		this.userId = userId;
	}
}
