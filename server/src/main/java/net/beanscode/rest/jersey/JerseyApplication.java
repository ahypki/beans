package net.beanscode.rest.jersey;

import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;

import net.beanscode.rest.BeansServerMain;
import net.beanscode.web.BetaPage;
import net.beanscode.web.InitHomePage;
import net.beanscode.web.beta.DashboardContent;
import net.beanscode.web.jersey.StaticContent;
import net.beanscode.web.view.data.DataRest;
import net.beanscode.web.view.dataset.DatasetPage;
import net.beanscode.web.view.notebook.NotebookPage;
import net.beanscode.web.view.upload.UploadView;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.Watch;

public class JerseyApplication extends ResourceConfig {
 
    public JerseyApplication() {
    	super();
    	
    	Watch w  = new Watch();
    	
        // Register resources and providers using package-scanning.
        packages(true, BeansServerMain.class.getPackage().getName());
        packages(true, MySecurityContext.class.getPackage().getName());
        packages(true, Component.class.getPackage().getName());
        
//        packages(true, BeansAPI.class.getPackage().getName());
        register(StaticContent.class);
        register(DashboardContent.class);
        register(BetaPage.class);
        register(DataRest.class);
        register(DatasetPage.class);
        register(NotebookPage.class);
        register(UploadView.class);
        register(InitHomePage.class);
        
        register(RolesAllowedDynamicFeature.class);
        
        register(MultiPartFeature.class);
        
        LibsLogger.debug(JerseyApplication.class, "JerseyApplication in " + w);
    }

}
