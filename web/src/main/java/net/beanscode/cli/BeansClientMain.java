package net.beanscode.cli;

import java.util.Comparator;

import net.beanscode.cli.api.ApiCallCommand;
import net.hypki.libs5.cli.noninteractivecli.CLICommand;
import net.hypki.libs5.cli.noninteractivecli.CLIManager;
import net.hypki.libs5.utils.LibsConst;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.args.ArgsUtils;
import net.hypki.libs5.utils.url.UrlUtilities;

public class BeansClientMain {
	
	public static void main(String[] args) {
		// logger
		if (ArgsUtils.exists(args, "verbose"))
			System.setProperty("log4j", "log4j-debug.properties");
		else
			System.setProperty("log4j", "log4j.properties");
				
		System.setProperty("javax.xml.parsers.DocumentBuilderFactory", "com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl");
		
		
		CLIManager manager = new CLIManager()
								.searchForCLIClasses(BeansClientMain.class)
//								.addCommand(Web.class)
								;
				
		try {
			manager.run(args);
		} catch (Exception e) {
			LibsLogger.error(BeansClientMain.class, "Command failed", e);
			System.out.println("ERROR Command failed: " + e.getMessage());
			
			if (args.length == 0)
				manager.printHelp(false);
		}
	}
	
}
