package net.beanscode.web.view.settings;

public enum SettingType {
	STRING,
	MULTILINE,
	INTEGER,
	FLOAT,
	BOOLEAN,
	DATE,
	PASS
}
