package net.beanscode.web.view.backup;

import java.io.IOException;

import javax.ws.rs.core.SecurityContext;

import net.beanscode.web.beta.BetaContent;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.BetaButtonGroup;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.utils.url.Params;

import org.rendersnake.HtmlCanvas;

public class BackupBetaContent extends BetaContent {
	
	public BackupBetaContent() {
		
	}

	public BackupBetaContent(BeansComponent parent) {
		super(parent);
	}
		
	@Override
	protected void renderHeader(HtmlCanvas html) throws IOException {
		html
			.h1()
				.content("Backup/restore data");
	}
	
	@Override
	protected void renderContent(HtmlCanvas html) throws IOException {	
		html
			.render(new BackupPanel(this));
	}

	@Override
	protected BetaButtonGroup getMenu() throws IOException {
		return null;
	}

	@Override
	protected String getMenuName() throws IOException {
		return null;
	}	
}
