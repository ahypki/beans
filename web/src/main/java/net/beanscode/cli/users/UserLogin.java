package net.beanscode.cli.users;

import static net.hypki.libs5.utils.args.ArgsUtils.exists;
import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.IOException;

import net.beanscode.cli.BeansCliCommand;
import net.beanscode.cli.BeansCLISettings;
import net.hypki.libs5.cli.noninteractivecli.CLICommand;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.cli.noninteractivecli.CliArg;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.RestMethod;
import net.hypki.libs5.pjf.components.jersey.SessionBean;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.args.ArgsUtils;
import net.hypki.libs5.utils.io.UserIO;
import net.hypki.libs5.utils.url.Params;

public class UserLogin extends BeansCliCommand {
	
	@CliArg(name = "user-email",
			description = "ID of a user. This parameter is required or --user-email",
			required = true)
	private String userEmail = null;
	
	@CliArg(name = "user-pass",
			description = "",
			required = false)
	private String pass = null;
	
	public UserLogin() {
		super(RestMethod.POST, null);
	}
	
	public UserLogin(SessionBean sb) {
		super(RestMethod.POST, sb);
	}

	@Override
	public String getUrlPath() {
		return "api/user/login";
	}
	
	@Override
	public boolean isLoginRequired() {
		return false;
	}

	@Override
	public void processResponse() {
//		try {
//			BeansCLISettings.set("userEmail", getUserEmail());
//			BeansCLISettings.set("userLC", getResponse().getAsString("userLC"));
//			BeansCLISettings.set("userId", getResponse().getAsString("userId"));
//			BeansCLISettings.save();
//		} catch (IOException e) {
//			LibsLogger.error(UserLogin.class, "Cannot save user credentials", e);
//		}
		
		LibsLogger.debug(UserLogin.class, "User logged in successfully");
	}

	@Override
	public Params getRESTParams() {
		return new Params()
			.add("email", getUserEmail())
			.add("pass", getPass());
	}

	@Override
	public String getCLICommandName() {
		return "user login";
	}

	@Override
	public String getCLICommandDescription() {
		return "Login user to BEANS server";
	}
	
	@Override
	public CLICommand run(String[] args) throws CLIException, IOException {
		if (exists(args, "user-email") && !exists(args, "user-pass"))
			setPass(UserIO.getPassword("Password: ", null));
		
		setUserEmail(ArgsUtils.getString(args, "user-email"));
		
		return super.run(new String[]{"--user-email", getUserEmail(), 
				"--user-pass", getPass()});
	}
	
	@Override
	public String getUserLC() {
		return null;
	}
	
	public BeansCliCommand run(String email, String pass) throws CLIException, IOException {
		assertTrue(notEmpty(pass), "Password cannot be empty");
		
		setUserEmail(email);
		setPass(pass);
		
		return (BeansCliCommand) run(new String[]{"--user-email", email, "--user-pass", pass});
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}
}
