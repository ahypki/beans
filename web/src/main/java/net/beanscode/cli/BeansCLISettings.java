package net.beanscode.cli;

import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;

import java.io.File;
import java.io.IOException;

import net.beanscode.cli.users.UserLogin;
import net.beanscode.web.BeansWebConst;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.jersey.SessionBean;
import net.hypki.libs5.pjf.op.OpCookieCreate;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileUtils;
import net.hypki.libs5.utils.io.UserIO;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.reflection.SystemUtils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.url.HttpResponse;
import net.hypki.libs5.utils.url.Params;
import net.hypki.libs5.utils.url.UrlUtilities;

import com.google.gson.JsonElement;

public class BeansCLISettings {
	
	private static JsonElement settings = null;
	
	private static JsonElement getSettings() {
		if (settings == null) {
			try {
				settings = JsonUtils.readJsonFile(new File(getSettingsPath()).getAbsolutePath());
			} catch (IOException e) {
				LibsLogger.error(BeansCLISettings.class, "Cannot find or read settings file for BEANS CLI", e);
				settings = null;
			}
		}
		return settings;
	}
	
	public static String getSettingsPath() {
		return BeansCLIConst.SETTINGS_FILENAME_PATH;
	}
	
	public static void save() throws IOException {
		JsonUtils.saveJson(getSettings(), getSettingsPath(), true);
		FileUtils.setFilePerm(getSettingsPath(), "go-rwx");
	}
	
	public static void set(String key, String value) {
		if (getSettings() != null) {
			getSettings().getAsJsonObject().addProperty(key, value);
			try {
				save();
			} catch (IOException e) {
				LibsLogger.error(BeansCLISettings.class, "Cannot save settings file", e);
			}
		}
	}

	public static String get(String path) {
		return getSettings() != null ? JsonUtils.readString(getSettings(), path) : null;
	}
	
	public static String get(String path, String defaultValue) {
		return getSettings() != null ? JsonUtils.readString(getSettings(), path, defaultValue) : defaultValue;
	}
	
	public static int get(String path, int defaultValue) {
		return JsonUtils.readInt(getSettings(), path, defaultValue);
	}

	public static SessionBean getSessionBeans() throws CLIException, IOException {
		// read userLC and userId from settings file
		String userLC = get("userLC");
		String userId = get("userId");
		
		if (nullOrEmpty(userLC)) {
			// log in user
			final String userEmail = UserIO.getString("Email: ", null);
			final String userPass = UserIO.getPassword("Password: ", null);
			
			UserLogin u = (UserLogin) new UserLogin().run(userEmail, userPass);
			
			if (u.getResponse().isOK()) {
				userLC = u.getResponse().getAsString(BeansWebConst.COOKIE_LOGIN);
				userId = u.getResponse().getAsString(BeansWebConst.COOKIE_USERID);
				
				BeansCLISettings.set("userLC", userLC);
				BeansCLISettings.set("userId", userId);
				
				return new SessionBean(userLC, new UUID(userId));
			}	
		} else {
			return new SessionBean(userLC, new UUID(userId));
		}
		
		return null;
	}

}
