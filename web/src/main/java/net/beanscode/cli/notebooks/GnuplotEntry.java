package net.beanscode.cli.notebooks;

import net.beanscode.pojo.NotebookEntry;
import net.hypki.libs5.plot.gnuplot.GnuplotWrapper;
import net.hypki.libs5.utils.json.JsonUtils;

public class GnuplotEntry extends NotebookEntry {

	public static final String META_SCRIPT 		= "script";
	public static final String META_WRAPPER		= "wrapper";
	public static final String META_STATUS		= "status";
	public static final String META_COMPILEDSCRIPT 		= "compiledscript";
	
	private PlotStatus plotStatus = null;

	public GnuplotEntry() {
		
	}
	
	public GnuplotEntry(Notebook notebook) {
		setNotebookId(notebook.getId());
		setUserId(notebook.getUserId());
	}

	public PlotStatus getPlotStatus() {
		if (plotStatus == null)
			plotStatus = getMetaAsObject(META_STATUS, PlotStatus.class);
		if (plotStatus == null)
			plotStatus = new PlotStatus();
		return plotStatus;
	}

	private void setPlotStatus(PlotStatus plotStatus) {
		setMeta(META_STATUS, JsonUtils.objectToStringPretty(plotStatus));
		this.plotStatus = plotStatus;
	}

	public GnuplotWrapper getGnuplotWrapper() {
		return getMetaAsObject(META_WRAPPER, GnuplotWrapper.class);
	}

	public void setGnuplotWrapper(GnuplotWrapper gnuplotWrapper) {
		setMeta(META_WRAPPER, JsonUtils.objectToStringPretty(gnuplotWrapper));
	}

	public String getGnuplotScript() {
		return getMetaAsString(META_SCRIPT, null);
	}
	
	public String getGnuplotCompiledScript() {
		return getMetaAsString(META_COMPILEDSCRIPT, null);
	}

	public void setGnuplotScript(String gnuplotScript) {
		getMeta().clear();
		
		setMeta(META_SCRIPT, gnuplotScript);
	}
}
