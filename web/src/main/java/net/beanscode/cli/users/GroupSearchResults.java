package net.beanscode.cli.users;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

public class GroupSearchResults {

	@Expose
	private List<Group> groups = null;
	
	@Expose
	private int maxHits = 0;
	
	public GroupSearchResults() {
		
	}

	public List<Group> getGroups() {
		if (groups == null)
			groups = new ArrayList<Group>();
		return groups;
	}

	public GroupSearchResults setGroups(List<Group> groups) {
		this.groups = groups;
		return this;
	}

	public int getMaxHits() {
		return maxHits;
	}

	public GroupSearchResults setMaxHits(int maxHits) {
		this.maxHits = maxHits;
		return this;
	}
	
	public GroupSearchResults setMaxHits(long maxHits) {
		this.maxHits = (int) maxHits;
		return this;
	}
}
