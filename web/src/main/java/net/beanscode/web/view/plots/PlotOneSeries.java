package net.beanscode.web.view.plots;

import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.id;
import static org.rendersnake.HtmlAttributesFactory.name;

import java.io.IOException;

import net.beanscode.web.view.ui.BeansComponent;
import net.hypki.libs5.pjf.components.ComboComponent;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.buttons.ButtonGroup;
import net.hypki.libs5.pjf.op.OpRemove;
import net.hypki.libs5.plot.Plot;
import net.hypki.libs5.plot.PlotType;
import net.hypki.libs5.utils.string.StringUtilities;

import org.rendersnake.HtmlCanvas;

public class PlotOneSeries extends BeansComponent {
	
	private Plot plotSeries = null;
	
	public PlotOneSeries() {
		
	}

	public PlotOneSeries(BeansComponent parent) {
		super(parent);
	}
	
	public PlotOneSeries(BeansComponent parent, Plot plotSeries) {
		super(parent);
		setPlotSeries(plotSeries);
	}
	
	private String getType() {
		if (getPlotSeries() == null || getPlotSeries().getPlotType() == null)
			return "Points";
		else if (getPlotSeries().getPlotType() == PlotType.BOXES)
			return "Boxes";
		else if (getPlotSeries().getPlotType() == PlotType.LINES)
			return "Lines";
//		else if (getPlotSeries().getPlotType() == PlotType.MAP)
//			return "Map";
//		else if (getPlotSeries().getPlotType() == PlotType.PARALLEL_COORDINATES)
//			return "Parallel";
		else if (getPlotSeries().getPlotType() == PlotType.POINTS)
			return "Points";
//		else if (getPlotSeries().getPlotType() == PlotType.SCATTERPLOT_MATRIX)
//			return "Scatter";
		else if (getPlotSeries().getPlotType() == PlotType.STEPS)
			return "Steps";
		else
			throw new RuntimeException("Unknown plot series type " + getPlotSeries().getPlotType());
	}

	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.div(id(getId())
					.class_("row"))
				.div(class_("col-sm-2"))
					.render(new ButtonGroup()
							.addButton(new Button()
							.setAwesomeicon("fa fa-trash")
							.add(new OpRemove("#" + getId()))))
					.render(new ComboComponent("plot-type")
							.setAdditionalCssClass("beans-pig-mode")
							.setValues("Points", "Lines")
							.setSelectedValue(getType())
							)
				._div()
				.div(class_("col-sm-2"))
//					.label()
//						.content(" with columns ")
					.input(name("plot-column-x")
							.type("text")
							.class_("form-control")
							.add("placeholder", "Column on X axis (name, or math expression")
							.title("Column on X axis (name, or math expression")
							.value(getPlotSeries() != null ? getPlotSeries().getX() : ""))
				._div()
				.div(class_("col-sm-2"))
	//				.label()
	//					.content(" with columns ")
					.input(name("plot-column-y")
							.type("text")
							.class_("form-control")
							.add("placeholder", "Column on Y axis (name, or math expression")
							.title("Column on Y axis  (name, or math expression")
							.value(getPlotSeries() != null ? getPlotSeries().getY(): ""))
				._div()
				.div(class_("col-sm-4"))
//					.label()
//						.content(" with title ")
					.input(name("plot-series-title")
							.type("text")
							.class_("form-control")
							.add("placeholder", "Title of the line")
							.title("Title of the line")
							.value(getPlotSeries() != null ? getPlotSeries().getLegend() : ""))
				._div()
				.div(class_("col-sm-2"))
				.input(name("plot-series-sample")
						.type("text")
						.class_("form-control")
						.add("placeholder", "Sample rate")
						.title("Sample rate")
						.value(getPlotSeries() != null ? "" + getPlotSeries().getSample() : ""))
			._div()
			._div()
			;
	}

	public Plot getPlotSeries() {
		return plotSeries;
	}

	public void setPlotSeries(Plot plotSeries) {
		this.plotSeries = plotSeries;
	}
}
