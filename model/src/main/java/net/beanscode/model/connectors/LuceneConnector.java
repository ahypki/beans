package net.beanscode.model.connectors;

import static java.lang.String.valueOf;
import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.utils.NumberUtils.toInt;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.beanscode.model.BeansSettings;
import net.beanscode.model.plugins.ColumnDef;
import net.beanscode.model.plugins.ColumnDefList;
import net.beanscode.model.plugins.Connector;
import net.beanscode.model.plugins.ConnectorInitParams;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.Results;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.db.lucene.LuceneDatabaseProvider;
import net.hypki.libs5.search.query.Query;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.string.MetaList;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

import com.google.gson.JsonElement;
import com.google.gson.annotations.Expose;

public class LuceneConnector implements Connector {
	
	private static final String KEYSPACE_COLUMN_DEFS = "_CONN_KEYSPACE_COLUMN_DEFS";
	private static final String COLUMN_DEFS = "COLUMN_DEFS";
	
	@Expose
	@NotNull
	@AssertValid
	private ConnectorInitParams connectorInitParams = null;
	
	private static LuceneDatabaseProvider luceneDatabaseProvider = null;

	public LuceneConnector() {
		
	}

	@Override
	public String getName() {
		return "Lucene connector";
	}

	@Override
	public ConnectorInitParams getInitParams() {
		if (this.connectorInitParams == null)
			this.connectorInitParams = new ConnectorInitParams();
		return this.connectorInitParams;
	}
	
	private LuceneDatabaseProvider getLuceneDatabaseProvider() {
		if (luceneDatabaseProvider == null) {
			try {
				luceneDatabaseProvider = new LuceneDatabaseProvider();
				
				MetaList meta = new MetaList();
				meta.add(LuceneDatabaseProvider.INIT_DIRECTORY, 
						new FileExt(BeansSettings.getConnectorDefaultFolder().getAbsolutePath(), "luceneconnector").getAbsolutePath());
				luceneDatabaseProvider.init(meta);
			} catch (IOException e) {
				LibsLogger.error(LuceneConnector.class, "Cannot initialize Lucene connector", e);
			}
		}
		return luceneDatabaseProvider;
	}

	@Override
	public Connector setInitParams(ConnectorInitParams initParams) {
		this.connectorInitParams = initParams;
		return this;
	}
	
	@Override
	public void validate() throws IOException {
		// do nothing
	}
	
	@Override
	public String getVersion() {
		return getClass().getSimpleName() + ", ver. 1.0";
	}
	
	private String getKeyspace() {
		return "__CONN_KEYSPACE";
	}
	
	private String getTableId() {
		UUID tableId = getInitParams().getTableId();
		return tableId != null ? tableId.getId() : null;
	}
	
	private String getTable() {
		UUID tableId = getInitParams().getTableId();
		return tableId != null ? "__CONN_" + tableId.getId() : null;
	}

//	@Override
//	public Results iterateRows(String state, int size) {
//		return LuceneDatabaseProvider
//			.getInstance()
//			.getList(getKeyspace(), 
//					getTable(), 
//					new String[] {DbObject.COLUMN_DATA}, 
//					null, 
//					size, 
//					state, 
//					null);
//	}

	@Override
	public void write(Row row) throws IOException {
		getLuceneDatabaseProvider()
			.saveRow(getKeyspace(), getTable(), row);
	}

	@Override
	public void close() throws IOException {
		// do nothing
	}

	@Override
	public void clear() throws IOException {
		getLuceneDatabaseProvider()
			.clearTable(getKeyspace(), getTable());
	}

	@Override
	public void setColumnDefs(ColumnDefList defs) {
		try {
			getLuceneDatabaseProvider()
				.saveRow(KEYSPACE_COLUMN_DEFS, getTable(), new Row(getTableId())
						.addColumn(COLUMN_DEFS, JsonUtils.objectToString(defs)));
		} catch (IOException e) {
			LibsLogger.error(LuceneConnector.class, "Cannot save column defs to Lucene", e);
		}
	}

	@Override
	public ColumnDefList getColumnDefs() {
		try {
			Row row = getLuceneDatabaseProvider()
				.getRow(KEYSPACE_COLUMN_DEFS, getTable(), getTableId());
			
			String tmp = row != null && row.contains(COLUMN_DEFS) ? row.getAsString(COLUMN_DEFS) : null;
			ColumnDefList defs = new ColumnDefList();
			if (tmp != null) {
				for (JsonElement je : JsonUtils.parseJson(tmp).getAsJsonArray()) {
					defs.add(JsonUtils.fromJson(je, ColumnDef.class));
				}
			}
			return defs;
		} catch (IOException e) {
			LibsLogger.error(LuceneConnector.class, "Cannot get column defs from Lucene", e);
			return null;
		}
	}
	
	@Override
	public Iterable<Row> iterateRows(Query filter, int splitNr, int splitCount) {
		// TODO implement it
		if (splitNr == 0)
			return iterateRows(filter);
		else
			return new Iterable<Row>() {
				
				@Override
				public Iterator<Row> iterator() {
					return new Iterator<Row>() {
						
						@Override
						public Row next() {
							return null;
						}
						
						@Override
						public boolean hasNext() {
							return false;
						}
					};
				}
			};
	}
	
	@Override
	public Iterable<Row> iterateRows(Query filter) {
		return new Iterable<Row>() {
			
			@Override
			public Iterator<Row> iterator() {
				return new Iterator<Row>() {
					
					private Iterator<Row> allRowsIter = iterateRows(filter).iterator();
					private Row row = null;
					
					@Override
					public Row next() {
						return row;
					}
					
					@Override
					public boolean hasNext() {
						while (allRowsIter.hasNext()) {
							row = allRowsIter.next();
							
							if (row == null)
								return false;
							else if (filter.isFulfilled(row))
								return true;
						}
						
						return row != null;
					}
				};
			}
		};
	}
	
	@Override
	public Results iterateRows(Query filter, String state, int size) {
		int from = 0;
		if (notEmpty(state))
			from = toInt(state);
		
		int i = 0;
		Results res = new Results();
		for (Row row : this.iterateRows(filter)) {
			if (i >= from)
				res.addRow(row);
			
			i++;
			
			if (res.size() == size)
				break;
		}
		res.setNextPage(valueOf(i));
		return res;
	}
	
	@Override
	public long getAproxBytesSize() throws IOException {
		return 1_000_000_000; // TODO general value
	}
}
