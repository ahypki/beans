package net.beanscode.model.unittests.connectors;

import java.io.IOException;

import net.beanscode.model.connectors.LuceneConnector;
import net.hypki.libs5.db.db.weblibs.utils.UUID;

import org.junit.Test;

public class LuceneConnectorTestCase extends ConnectorTestCase {

	public LuceneConnectorTestCase() {
		
	}
	
	@Test
	public void testConnector() throws IOException {
		LuceneConnector conn = new LuceneConnector();
		conn.getInitParams().setTableId(UUID.random());
		
		testSimple(conn);
		
		conn.clear();
		conn.close();
	}
}
