package net.beanscode.web.view.plugins;

import static org.rendersnake.HtmlAttributesFactory.id;

import java.io.IOException;

import org.rendersnake.HtmlAttributesFactory;
import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.BeansRefreshableComponent;
import net.beanscode.web.view.ui.badge.BadgeError;
import net.beanscode.web.view.ui.badge.BadgeInfo;
import net.beanscode.web.view.ui.badge.BadgeSuccess;
import net.hypki.libs5.utils.date.SimpleDate;

public class SelfTestStatusComponent extends BeansRefreshableComponent {
	
	@Expose
	private String pluginName = null;
	
	private String pluginStatus = null;
	
	public SelfTestStatusComponent(BeansComponent parent) {
		super(parent);
	}
	
	public SelfTestStatusComponent(BeansComponent parent, String pluginName) {
		super(parent);
		setPluginName(pluginName);
	}

	@Override
	protected HtmlCanvas renderContent(HtmlCanvas html) throws IOException {
		ApiCallCommand selftTest = new ApiCallCommand(this)
				.setApi(ApiCallCommand.API_PLUGIN_SELFTEST_GET)
				.addParam("userId", getUserEmail())
				.addParam("pluginName", pluginName)
				.run();
		
		pluginStatus = selftTest.getResponse().getAsString("status", "unknown");
		
		if (pluginStatus.equals("ok"))
			html.render(new BadgeSuccess(pluginStatus));
		else if (pluginStatus.equals("error"))
			html.render(new BadgeError(pluginStatus));
		else
			html.render(new BadgeInfo(pluginStatus));
		
//		html
//			.write(" (" + SimpleDate.now().toStringISO() + ")");
		
		return html;
	}

	@Override
	protected boolean isRefreshNeeded() {
		return pluginStatus == null 
				|| (!pluginStatus.equals("ok") && !pluginStatus.equals("error"));
	}

	public String getPluginName() {
		return pluginName;
	}

	public void setPluginName(String pluginName) {
		this.pluginName = pluginName;
	}

}
