package net.beanscode.model;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.hazelcast.config.Config;
import com.hazelcast.config.InterfacesConfig;
import com.hazelcast.config.JoinConfig;
import com.hazelcast.config.NetworkConfig;
import com.hazelcast.config.TcpIpConfig;

import net.beanscode.model.backup.AutoBackupJob;
import net.beanscode.model.backup.BackupManager;
import net.beanscode.model.cass.Data;
import net.beanscode.model.cass.notification.Notification;
import net.beanscode.model.connectors.BeansConnector;
import net.beanscode.model.connectors.CassandraConnector;
import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.init.ReloadSettingsJob;
import net.beanscode.model.notebook.MagicKey;
import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.notebook.NotebookEntry;
import net.beanscode.model.notebook.NotebookEntryFactory;
import net.beanscode.model.notebook.NotebookEntryHistory;
import net.beanscode.model.notebook.NotebookEntryProgress;
import net.beanscode.model.notebook.autoupdate.CreateAutoUpdatesJobsForUsersJob;
import net.beanscode.model.plugins.MetaDeser;
import net.beanscode.model.plugins.Plugin;
import net.beanscode.model.plugins.PluginManager;
import net.beanscode.model.rights.Group;
import net.beanscode.model.rights.Permission;
import net.beanscode.model.users.CreateAdminAccountJob;
import net.beanscode.model.users.IsAdminPasswordChangedJob;
import net.hypki.libs5.db.cassandra.CassandraProvider;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.schema.TableSchema;
import net.hypki.libs5.db.db.weblibs.TxObject;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.weblibs.Settings;
import net.hypki.libs5.distributed.hazelcast.DistributedHazelcast;
import net.hypki.libs5.plot.Figure;
import net.hypki.libs5.plot.Plot;
import net.hypki.libs5.plot.Range;
import net.hypki.libs5.utils.LibsConst;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.args.ArgsUtils;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.string.Base64Utils;
import net.hypki.libs5.utils.string.Meta;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.jobs.Job;
import net.hypki.libs5.weblibs.property.Property;
import net.hypki.libs5.weblibs.settings.Setting;
import net.hypki.libs5.weblibs.user.Allowed;
import net.hypki.libs5.weblibs.user.CookieUUID;
import net.hypki.libs5.weblibs.user.ResetPassUUID;
import net.hypki.libs5.weblibs.user.User;
import net.hypki.libs5.weblibs.user.UserConfirmationUUID;
import net.hypki.libs5.weblibs.user.UserUUID;

public class BeansConst {
	public static final String VERSION = "1.0.2";
	
	public static final String DEFAULT_NOTEBOOK_NAME = "New notebook";
	public static final String DEFAULT_DATASET_NAME = "New dataset";
	public static final String DEFAULT_TABLE_NAME = "New table";
	
	public static final String COLUMN_TBID = "tbid";
	
	private static boolean initiated = false;

	private static List<String> channelNames = null;
	
	private static List<TableSchema> tableSchemas = null;
	
	public static final String PLOT_CACHE_PATH = "plotCache";
	
//	public static final String LOGIN_COOKIE_NAME = "beansLC";

	public static final int COOKIE_VALID_DAYS_DEFAULT = 120;
	
//	public static final int PLOT_DATA_INMEMORY_MAX = 100000;
	
	public static boolean UPGRADE_DATABASE = true;
	
	public static final int PLOT_MAX_BAR_COUNT = 10000;
	
	private static final String BEANS_DB_VERSION = "1.0.2";
	
	private static final boolean REDIRECT_SYSOUT_TO_LOGGER = true;
	
	private static String[] args = null;
	
	public synchronized static void init(String [] args) throws IOException, ValidationException {
		BeansConst.args = args;
		
		if (initiated == false) {
			initiated = true;
			
			// default logger
			if (args != null && ArgsUtils.exists(args, "log4j"))
				System.setProperty("log4j", ArgsUtils.getString(args, "log4j"));
			else if (args != null && ArgsUtils.exists(args, "verbose"))
				System.setProperty("log4j", "log4j-debug.properties");
			else
				System.setProperty("log4j", "log4j.properties");
			
//			System.setProperty("org.apache.commons.logging.Log","org.apache.commons.logging.impl.NoOpLog");
			
			final String settings = ArgsUtils.getString(args, "settings", "beans-dev.json");
			
			LibsLogger.debug(BeansConst.class, "Settings set to ", settings);
			LibsLogger.debug(BeansConst.class, "Initalizing BEANS...");
			
			Settings.init(args);
				
			// locale
			Locale.setDefault(Locale.ENGLISH);
			
			LibsLogger.debug(BeansConst.class, "Libs4 code version " + LibsConst.VERSION);
			LibsLogger.debug(BeansConst.class, "BEANS code version " + BeansConst.VERSION);
			
			System.setProperty("javax.xml.parsers.DocumentBuilderFactory", "com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl");
			
			if (!new File(PLOT_CACHE_PATH).exists())
				new File(PLOT_CACHE_PATH).mkdirs();
			LibsLogger.debug(BeansConst.class, "Initializing search engine ", 
					BeansSearchManager.getSearchManager().getClass().getSimpleName());
			
			initDb();
			
			// reloading plugins
			PluginManager.reload();
			for (Plugin plugin : PluginManager.iterateAllPlugins())
				LibsLogger.debug(BeansConst.class, "Plugin ", plugin.getName(), " loaded");
			
			JsonUtils.registerTypeAdapter(Meta.class, new MetaDeser());
			
			// TODO DEBUG
//			BeansSearchManager.clearIndex();// 
//			BeansSearchManager.getSearchManager().clearIndexType(WeblibsConst.KEYSPACE_LOWERCASE, Setting.COLUMN_FAMILY.toLowerCase());
			
			// initial jobs
			new CreateAdminAccountJob().save();
			new CreateAutoUpdatesJobsForUsersJob().save();
			new AutoBackupJob().save();
//			new BackupNotebooksJob().save();
			new ReloadSettingsJob().save();
			new IsAdminPasswordChangedJob().save();
			
			// TODO debug
//			new TestingBackupJob().save();
			
			if (ArgsUtils.exists(args, "restore-backup"))
				BackupManager.restoreBackup(ArgsUtils.getString(args, "restore-backup"));
						
			if (REDIRECT_SYSOUT_TO_LOGGER) {
				redirectOutToLogger();
				redirectErrToLogger();
			}
			
			// INFO necessary because of problem with certificates at esa
			// https://dzone.com/articles/troubleshooting-javaxnetsslsslhandshakeexception-r
			System.setProperty("https.protocols", "SSLv3,TLSv1,SSLv2Hello,TLSv1.2");

			// TODO DEBUG
			
			LibsLogger.debug(BeansConst.class, "BEANS base path: ", BeansSettings.getExecutionPath());
			
			
//			BeansSearchManager.reloadIndex();
		}
	}

	public static void redirectOutToLogger() {
		System.setOut(new PrintStream(new OutputStream() {
			
		    StringBuilder stringBuilder = new StringBuilder();
		    
			@Override
			public void write(int i) throws IOException {
				char c = (char) i;
				if (c == '\r' || c == '\n') {
					if (stringBuilder.length() > 0) {
						LibsLogger.debug(BeansConst.class, stringBuilder.toString());
						stringBuilder = new StringBuilder();
					}
				} else
					stringBuilder.append(c);
			}
		}));
	}
	
	public static void redirectErrToLogger() {
		System.setErr(new PrintStream(new OutputStream() {
			
		    StringBuilder stringBuilder = new StringBuilder();
		    
			@Override
			public void write(int i) throws IOException {
				char c = (char) i;
				if (c == '\r' || c == '\n') {
					if (stringBuilder.length() > 0) {
						LibsLogger.debug(BeansConst.class, stringBuilder.toString());
						stringBuilder = new StringBuilder();
					}
				} else
					stringBuilder.append(c);
			}
		}));
	}
	
	public static String getDatabaseVersion() {
		return BeansSettings
				.getSettingAsString(BeansSettings.SETTING_DATABASE_VERSION, 
						"version", 
						"1.0.0");
	}
	
	private static void setDatabaseVersion(String newVersion) {
		LibsLogger.debug(BeansConst.class, "Upgrading database to version " + newVersion);

		BeansSettings
			.setSetting(BeansSettings.SETTING_DATABASE_VERSION, 
					"version", 
					newVersion);
	}

	private static void upgradeDatabase() throws IOException, ValidationException {
		if (!UPGRADE_DATABASE)
			return;
		
		LibsLogger.debug(BeansConst.class, "Current Database version " + getDatabaseVersion());
		
		upgradeTo1_0_0();
		upgradeTo1_0_1();
	}
	
	private static void upgradeTo1_0_1() throws ValidationException, IOException {
		if (getDatabaseVersion() != null
				&& getDatabaseVersion().equals("1.0.0")) {
			// upgrade
		} else {
			return; // do nothing
		}
		
		int convertedOK = 0;
		int convertedFail = 0;
		
		for (NotebookEntry ne : NotebookEntryFactory.iterateNotebookEntry()) {
			if (ne != null && ne.getClass().getSimpleName().equals("Plot")) {
				try {
					Figure f = ne.getMetaAsObject("FIGURE", Figure.class);
					
//					LibsLogger.info(BeansConst.class, "fig: " + f.getTitle());
//					LibsLogger.info(BeansConst.class, "ent: " + ne.getName());
					
					if (ne.getName() != null 
							&& !ne.getName().equals("Plot title")) {
						f.setTitle(ne.getName());
						
						ne.setMeta("FIGURE", JsonUtils.objectToString(f));
						ne.save();
						
						convertedOK++;
					}
				} catch (Exception e) {
					LibsLogger.error(BeansConst.class, "Cannot convert PlotEntry to a new format", e);
					convertedFail++;
				}
			}
		}
		
		LibsLogger.info(BeansConst.class, "Converted OK " + convertedOK);
		LibsLogger.info(BeansConst.class, "Converted FAIL " + convertedFail);
		
		setDatabaseVersion("1.0.1");
	}

	private static void upgradeTo1_0_0() throws ValidationException, IOException {
		if (getDatabaseVersion() != null)
			return;
		
		int convertedOK = 0;
		int convertedFail = 0;
		
		for (NotebookEntry ne : NotebookEntryFactory.iterateNotebookEntry()) {
			if (ne.getClass().getSimpleName().equals("Plot")) {
				if (ne.getMetaAsString("META_SERIES") != null) {
					try {
						// converting Plot entry to use the class Figure
						Figure fig = new Figure();
						
						toNewPlotFormat(ne, fig);
						
						ne.save();
						
						convertedOK++;
					} catch (Exception e) {
						LibsLogger.error(BeansConst.class, "Cannot convert PlotEntry to a new format", e);
						convertedFail++;
					}
				}
			}
		}
		
		LibsLogger.info(BeansConst.class, "Converted OK " + convertedOK);
		LibsLogger.info(BeansConst.class, "Converted FAIL " + convertedFail);
		
		setDatabaseVersion("1.0.0");
	}

	public static void toNewPlotFormat(NotebookEntry ne, Figure fig) {
		ne.getMeta().remove("META_PLOT_STATUS");

		// source data
		String source = ne.getMetaAsString("META_DS_QUERY", "");
		source += ne.getMetaAsString("META_NOTEBOOK_QUERY", "");
		source += "@@";
		source += ne.getMetaAsString("META_TB_QUERY", "");
		fig.getMeta().add(ne.getMeta().get("META_DS_QUERY"));
		fig.getMeta().add(ne.getMeta().get("META_NOTEBOOK_QUERY"));
		fig.getMeta().add(ne.getMeta().get("META_TB_QUERY"));
		ne.getMeta().remove("META_DS_QUERY");
		ne.getMeta().remove("META_NOTEBOOK_QUERY");
		ne.getMeta().remove("META_TB_QUERY");

		// series
		String series = ne.getMetaAsString("META_SERIES");
		if (series == null) {
			// do nothing
			series = "";
		} else if (series.endsWith("==")) {
			series = Base64Utils.decode(series);
		}
		if (JsonUtils.isJsonSyntax(series)) {
			for (JsonElement je : JsonUtils.parseJson(series).getAsJsonArray()) {
				JsonObject jo = je.getAsJsonObject();
				Plot p = new Plot();
				JsonArray ja = null;
				if (jo.get("columnsExpr") != null) {
					ja = jo.get("columnsExpr").getAsJsonArray();
					p.setX(ja.get(0).getAsString());
					if (ja.size() > 1)
						p.setY(ja.get(1).getAsString());
				}
				if (jo.get("legend") != null)
					p.setLegend(jo.get("legend").getAsString());
				p.setPlotType(jo.get("plotType").getAsString());
				if (jo.get("params") != null) {
					fig.getMeta().add("params from META_SERIES", jo.get("params").toString());
				}
				fig.addPlot(p);
			}
		} else if (series.startsWith("[") && series.endsWith("]")) {
			String[] xy = StringUtilities.split(series, ":");
			Plot p = new Plot();
			p.setX(xy[0]);
			if (xy.length > 0)
				p.setY(xy[1]);
			fig.addPlot(p);
		}
		ne.getMeta().remove("META_SERIES");

		// title
		if (ne.getMeta().isNotEmpty("META_TITLE"))
			fig.setTitle(ne.getMetaAsString("META_TITLE"));
		ne.getMeta().remove("META_TITLE");

		// color by
		if (ne.getMeta().isNotEmpty("META_COLORBY")) {
//			JsonArray ja = JsonUtils.parseJson().getAsJsonArray();
//			if (ja.size() > 0) {
				String colorBy = ne.getMeta().get("META_COLORBY").getAsString().trim();
				if (notEmpty(colorBy)) {
					fig.getMeta().add("META_COLORBY", colorBy);
					for (Plot p : fig.getPlots())
						p.setColorBy(colorBy);
				}
//			}
		}
		ne.getMeta().remove("META_COLORBY");

		// split by
		if (ne.getMeta().isNotEmpty("META_SPLITBY")) {
//			JsonArray ja = JsonUtils.parseJson().getAsJsonArray();
//			if (ja.size() > 0) {
				String splitBy = ne.getMeta().get("META_SPLITBY").getAsString().trim();
				if (notEmpty(splitBy)) {
					fig.getMeta().add("META_SPLITBY", splitBy);
					for (Plot p : fig.getPlots())
						p.setColorBy(splitBy);
				}
//			}
		}
		ne.getMeta().remove("META_SPLITBY");

		// legend - not used actually
		ne.getMeta().remove("META_LEGEND");

		// box width
		if (ne.getMeta().isNotEmpty("META_BOXWIDTH")) {
			double bw = ne.getMetaAsDouble("META_BOXWIDTH", 0.0);
			for (Plot p : fig.getPlots())
				p.setBoxWidth(bw);
		}
		ne.getMeta().remove("META_BOXWIDTH");

		ne.getMeta().remove("META_READ_TABLES");

		// sort by
		if (ne.getMeta().isNotEmpty("META_SORTBY")) {
			JsonArray ja = JsonUtils.parseJson(ne.getMeta().get("META_SORTBY").getAsString()).getAsJsonArray();
			if (ja.size() > 0) {
				String sortBy = ja.get(0).getAsString().trim();
				if (notEmpty(sortBy)) {
					fig.getMeta().add("META_SORTBY", sortBy);
					for (Plot p : fig.getPlots())
						p.setSortBy(sortBy);
				}
			}
		}
		ne.getMeta().remove("META_SORTBY");

		// sizes
		if (ne.getMeta().isNotEmpty("META_SIZES")) {
			double ps = ne.getMetaAsDouble("META_SIZES", 0.0);
			for (Plot p : fig.getPlots()) {
				p.setDataSource(source);
				p.setPointSize(ps);
			}
		}
		ne.getMeta().remove("META_SIZES");

		// x range
		if (ne.getMeta().isNotEmpty("META_XRANGE"))
			fig.setXRange(new Range(ne.getMetaAsDouble("META_XRANGE", 0.0), ne.getMetaAsDouble("META_XRANGE", 0.0)));
		ne.getMeta().remove("META_XRANGE");

		// logscale
		if (ne.getMeta().isNotEmpty("META_LOGSCALE_ON"))
			fig.setxLogscale(ne.getMetaAsBoolean("META_LOGSCALE_ON", false));
		ne.getMeta().remove("META_LOGSCALE_ON");

		// y range
		if (ne.getMeta().isNotEmpty("META_YRANGE"))
			fig.setYRange(new Range(ne.getMetaAsDouble("META_YRANGE", 0.0), ne.getMetaAsDouble("META_YRANGE", 0.0)));
		ne.getMeta().remove("META_YRANGE");

		ne.getMeta().remove("META_TABLES");

		// labels
		if (ne.getMeta().isNotEmpty("META_LABELS")) {
			String labelsMeta = ne.getMetaAsString("META_LABELS");
			if (labelsMeta.endsWith("=="))
				labelsMeta = Base64Utils.decode(labelsMeta);
			if (JsonUtils.isJsonSyntax(labelsMeta)) {
				JsonArray labelsArr = JsonUtils.parseJson(labelsMeta).getAsJsonArray();
				if (labelsArr.size() > 0)
					fig.setXLabel(labelsArr.get(0).getAsString());
				if (labelsArr.size() > 1)
					fig.setYLabel(labelsArr.get(1).getAsString());
			} else {
				labelsMeta = Base64Utils.decode(labelsMeta);
				JsonArray labelsArr = JsonUtils.parseJson(labelsMeta).getAsJsonArray();
				if (labelsArr.size() > 0)
					fig.setXLabel(labelsArr.get(0).getAsString());
				if (labelsArr.size() > 1)
					fig.setYLabel(labelsArr.get(1).getAsString());
			}
		}
		ne.getMeta().remove("META_LABELS");

		if (ne.getMeta().containsMeta("META_FILENAME_TEMPLATE"))
			fig.getMeta().add(ne.getMeta().get("META_FILENAME_TEMPLATE"));
		ne.getMeta().remove("META_FILENAME_TEMPLATE");
		ne.getMeta().remove("META_TYPES");
		ne.getMeta().remove("META_COLORS");

		if (ne.getMeta().size() > 0) {
			LibsLogger.error(BeansConst.class, "Still some meta present: " + ne.getMeta());
		}

		ne.setMeta("FIGURE", JsonUtils.objectToString(fig));
	}

	public static List<String> getAllChannelsNames() {
		if (channelNames == null) {
			channelNames = new ArrayList<>();
			channelNames.add(WeblibsConst.CHANNEL_NORMAL);
			channelNames.add(WeblibsConst.CHANNEL_NORMAL);
			channelNames.add(WeblibsConst.CHANNEL_NORMAL);
			channelNames.add(WeblibsConst.CHANNEL_NORMAL);
			channelNames.add(BeansChannels.CHANNEL_LONG_NORMAL);
			channelNames.add(BeansChannels.CHANNEL_LONG_NORMAL);
			channelNames.add(BeansChannels.CHANNEL_LONG_NORMAL);
			channelNames.add(BeansChannels.CHANNEL_LONG_NORMAL);
			channelNames.add(BeansChannels.CHANNEL_LONG_HIDDEN);
			channelNames.add(BeansChannels.CHANNEL_LONG_HIDDEN);
			channelNames.add(BeansChannels.CHANNEL_LONG_HIDDEN);
			channelNames.add(BeansChannels.CHANNEL_LONG_HIDDEN);
			channelNames.add(BeansChannels.CHANNEL_LONG_HIDDEN);
			channelNames.add(BeansChannels.CHANNEL_LONG_HIDDEN);
			channelNames.add(BeansChannels.CHANNEL_INDEX);
		}
		return channelNames;
	}
	
	public static List<TableSchema> getAllTableSchemas() {
		if (tableSchemas == null) {
			tableSchemas = new ArrayList<TableSchema>();
			
			tableSchemas.add(new Property().getTableSchema());
			tableSchemas.add(new TxObject().getTableSchema());
			tableSchemas.add(new Allowed().getTableSchema());
			tableSchemas.add(new UserUUID().getTableSchema());
			tableSchemas.add(new CookieUUID().getTableSchema());
			tableSchemas.add(new User().getTableSchema());
			tableSchemas.add(new ResetPassUUID().getTableSchema());
			tableSchemas.add(new UserConfirmationUUID().getTableSchema());
			tableSchemas.add(new Job() {
				@Override
				public void run() throws IOException, ValidationException {}
			}.getTableSchema());
			tableSchemas.add(new Table().getTableSchema());
			tableSchemas.add(new Dataset().getTableSchema());
			tableSchemas.add(new Data().getSchema());
			tableSchemas.add(new Notebook().getTableSchema());
//			tableSchemas.add(new TextEntry().getTableSchema());
//			tableSchemas.add(new QueryStatus().getTableSchema());
//			tableSchemas.add(new QueryStatusSorted().getTableSchema());
			tableSchemas.add(new Notification().getTableSchema());
			tableSchemas.add(new Group().getTableSchema());
			tableSchemas.add(new Permission().getTableSchema());
			tableSchemas.add(new Setting().getTableSchema());
			tableSchemas.add(new BeansConnector().getTableSchema());
			tableSchemas.add(new NotebookEntry().getTableSchema());
			tableSchemas.add(new NotebookEntryProgress().getTableSchema());
			tableSchemas.add(new NotebookEntryHistory().getTableSchema());
			tableSchemas.add(new MagicKey().getTableSchema());
			
			// TODO think about it
//			tableSchemas.add(new ButcheredPigScript().getTableSchema());
//			tableSchemas.add(new Distinct().getTableSchema());
			
			// columndefs table
			tableSchemas.add(new TableSchema(CassandraConnector.COLUMN_FAMILY_COLUMN_DEFS)
					.addColumn(DbObject.COLUMN_PK, ColumnType.STRING, true)
					.addColumn(DbObject.COLUMN_DATA, ColumnType.STRING));
		}
		return tableSchemas;
	}

	public static void initHazelcast() {
		Config config = new Config();
		config.setProperty("hazelcast.logging.type", "log4j");
		NetworkConfig netConf = config.getNetworkConfig();
		
		JoinConfig join = netConf.getJoin();
		join
			.getMulticastConfig()
			.setEnabled(false);
		TcpIpConfig tcpConfig = join.getTcpIpConfig();
		
		final String hosts = Settings.getString("BEANS/Agent/hosts", "127.0.0.1");
		
		for (String host : StringUtilities.split(hosts, ',')) {
			if (StringUtilities.notEmpty(host)) {
				tcpConfig.addMember(host);
				LibsLogger.debug(BeansConst.class, "Added member ", host, " to Hazelcast");
			}
//			.addMember("127.0.0.1") // INFO Hazelcast does not like this IP
		}
		
		tcpConfig
			.setEnabled(true);
		
		InterfacesConfig interConfig = netConf
			.getInterfaces()
			.setEnabled(true);
		
		for (String host : StringUtilities.split(hosts, ',')) {
			if (StringUtilities.notEmpty(host)) {
				interConfig.addInterface(host);
				LibsLogger.debug(BeansConst.class, "Added interface ", host, " to Hazelcast");
			}
		}
		
		DistributedHazelcast.getInstance(config);
	}

	public static void initDb() throws IOException, ValidationException {

		DbObject.init(BeansDbManager.getMainDatabaseProvider());
		
		// default database provider
//		String dbDriver = ArgsUtils.getString(args, "db");
//		if (dbDriver.startsWith("cass")) {
//			System.setProperty(DatabaseProvider.class.getSimpleName(), CassandraProvider.class.getName());
//			LibsLogger.debug(BeansConst.class, DatabaseProvider.class.getSimpleName(), " set to ", CassandraProvider.class.getName());
//		} else if (dbDriver.startsWith("mapdb")) {
//			System.setProperty(DatabaseProvider.class.getSimpleName(), MapDbManager.class.getName());
//			LibsLogger.debug(BeansConst.class, DatabaseProvider.class.getSimpleName(), " set to ", MapDbManager.class.getName());
//		} else
//			throw new NotImplementedException("Unknown ");
		
		// cassandra home
		String cassHome = null;
		if (BeansDbManager.isCassandraProvider()) {
			cassHome = ((CassandraProvider) DbObject.getDatabaseProvider()).getCassandraHome();
			if (cassHome != null)
				System.setProperty("cassandra.config", cassHome + "/conf/cassandra.yaml");
		}
		
		// reloading settings
		if (ArgsUtils.exists(args, "reload-settings-force"))
			BeansSettings.reloadSettings(true);
		if (ArgsUtils.exists(args, "reload-settings"))
			BeansSettings.reloadSettings(false);
		
		// create keyspace if not exist
		DbObject.getDatabaseProvider().createKeyspace(WeblibsConst.KEYSPACE);
		
		// creating table in DB
		for (TableSchema schema : getAllTableSchemas()) {
			DbObject.getDatabaseProvider().createTable(WeblibsConst.KEYSPACE, schema);
		}
		
		// TODO DEBUG
//		DbObject.getDatabaseProvider().clearTable(WeblibsConst.KEYSPACE, "bp");
//		DbObject.getDatabaseProvider().clearTable(WeblibsConst.KEYSPACE, Job.COLUMN_FAMILY);
		
		// for Elastic search engine (and actually others too) disable "_store" field
//		SearchManager.searchInstance().settings(JsonUtils.parseJson("{ \"disable_store\" : \"" + WeblibsConst.KEYSPACE_LOWERCASE + "." + Data.COLUMN_FAMILY.toLowerCase() + "\" }"));
		
		upgradeDatabase();
	}
}
