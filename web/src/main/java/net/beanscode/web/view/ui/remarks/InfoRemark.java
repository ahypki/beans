package net.beanscode.web.view.ui.remarks;

import static org.rendersnake.HtmlAttributesFactory.class_;

import java.io.IOException;

import org.rendersnake.HtmlAttributesFactory;
import org.rendersnake.HtmlCanvas;

import net.hypki.libs5.pjf.components.Component;

public class InfoRemark extends Component {
	
	private String title = null;
	
	private String message = null;

	public InfoRemark() {
		
	}
	
	public InfoRemark(String title, String message) {
		setTitle(title);
		setMessage(message);
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.div(class_("alert alert-info"))
				.h5()
					.i(class_("icon fas fa-info"))
					._i()
					.write(getTitle())
				._h5()
				.write(getMessage())
			._div();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
