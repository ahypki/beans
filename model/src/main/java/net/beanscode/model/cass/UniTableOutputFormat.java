package net.beanscode.model.cass;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import net.beanscode.model.BeansSettings;
import net.beanscode.model.connectors.BeansConnector;
import net.beanscode.model.connectors.BeansConnectorFactory;
import net.beanscode.model.connectors.BeansConnectorStatus;
import net.beanscode.model.plugins.ColumnDef;
import net.beanscode.model.plugins.ColumnDefList;
import net.beanscode.model.plugins.Connector;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.LocalLogger;

import org.apache.hadoop.mapreduce.JobContext;
import org.apache.hadoop.mapreduce.OutputCommitter;
import org.apache.hadoop.mapreduce.OutputFormat;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

public class UniTableOutputFormat extends OutputFormat {
	
	private String beansTableId = null;
	
	private UUID tableId = null;
	
	private String udfcSignature = null;
	
	private ColumnDefList columnDefs = null;
	
	private HashMap<String, Connector> idToConnector = new HashMap<String, Connector>();

	public UniTableOutputFormat(UUID tableId, String udfcSignature, ColumnDefList columnDefs) {
		setTableId(tableId);
		this.columnDefs = columnDefs;
		this.udfcSignature = udfcSignature;
	}

	@Override
	public void checkOutputSpecs(JobContext arg0) throws IOException, InterruptedException {
		LibsLogger.debug(UniTableOutputFormat.class, "checkOutputSpecs");
	}

	@Override
	public OutputCommitter getOutputCommitter(TaskAttemptContext taskAttempt) throws IOException, InterruptedException {
		return new BeansOutputCommitter(tableId);
	}

	@Override
	public RecordWriter getRecordWriter(TaskAttemptContext taskAttempt) throws IOException, InterruptedException {
		setBeansTableId(taskAttempt.getTaskAttemptID().toString());
		
		Connector tableConnector = idToConnector.get(getBeansTableId());
		
		if (tableConnector == null) {
			tableConnector = BeansSettings.getDefaultConnector(getTableId(), getBeansTableId());
			idToConnector.put(getBeansTableId(), tableConnector);
		}
		
		try {
			new BeansConnector(tableConnector, getBeansTableId())
				.save();
		} catch (ValidationException e) {
			LibsLogger.error(UniTableOutputFormat.class, "Cannot save BeansConnector to DB", e);
		}
		
		return new UniTableRecordWriter(tableConnector, columnDefs);
	}

	private UUID getTableId() {
		return tableId;
	}

	private void setTableId(UUID tableId) {
		this.tableId = tableId;
	}

	private String getBeansTableId() {
		return beansTableId;
	}

	private void setBeansTableId(String beansTableId) {
		if (this.beansTableId == null)
			this.beansTableId = beansTableId;
		else if (this.beansTableId != null
				&& beansTableId != null
				&& this.beansTableId.equals(beansTableId) == false)
			throw new RuntimeException("BeansTableId is already set in OutputFormat");
		else
			throw new RuntimeException("Unknown case");
	}

	/**
	 * An {@link OutputCommitter} that does nothing.
	 */
	private static class BeansOutputCommitter extends OutputCommitter {
		private UUID tableId = null;
		
		public BeansOutputCommitter() {
			
		}
		
		public BeansOutputCommitter(UUID taskId) {
			this.tableId = taskId;
		}
		
		public void abortTask(TaskAttemptContext taskContext) {
//			try {
//				String beansConnId = taskContext.getTaskAttemptID().toString();
//				BeansConnector bc = BeansConnectorFactory.getTable(tableId, beansConnId);
//				bc.remove();
//				
//				LocalLogger.debug("Aborted Task ", beansConnId);
//			} catch (IOException | ValidationException e) {
//				LibsLogger.error(UniTableOutputFormat.BeansOutputCommitter.class,
//						"Cannot remove BeansConnector for " + taskContext.getTaskAttemptID().toString(), e);
//			}
		}

		public void cleanupJob(JobContext jobContext) {
		}

		public void commitTask(TaskAttemptContext taskContext) {
			String beansConnId = taskContext.getTaskAttemptID().toString();
			
			try {
				LibsLogger.info(UniTableOutputFormat.BeansOutputCommitter.class,
						"Commiting ", taskContext);
				
				BeansConnector conn = BeansConnectorFactory.getBeansConnector(tableId, beansConnId);
				if (conn != null) {
					conn
						.setStatus(BeansConnectorStatus.COMMITTED)
						.save();
					
					LocalLogger.debug("Commited Task ", beansConnId);
				} else {
					LocalLogger.error("BeansConnector ", beansConnId, " not found");
				}
			} catch (Exception e) {
				throw new RuntimeException("Cannot commit TaskAttempt " + beansConnId, e);
			}
		}

		public boolean needsTaskCommit(TaskAttemptContext taskContext) {
			return true;
		}

		public void setupJob(JobContext jobContext) {
		}

		public void setupTask(TaskAttemptContext taskContext) {
		}
	}
}
