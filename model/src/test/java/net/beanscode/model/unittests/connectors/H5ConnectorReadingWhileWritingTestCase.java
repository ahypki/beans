package net.beanscode.model.unittests.connectors;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.beanscode.model.connectors.H5Connector;
import net.beanscode.model.plugins.ColumnDef;
import net.beanscode.model.plugins.ColumnDefList;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.Watch;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.string.RandomUtils;

import org.junit.Test;

public class H5ConnectorReadingWhileWritingTestCase extends ConnectorTestCase {

	public H5ConnectorReadingWhileWritingTestCase() {
		
	}
	
	@Test
	public void testConnector() throws IOException {
		FileExt h5File = new FileExt("test.h5");
		h5File.deleteIfExists();
		
		H5Connector conn = new H5Connector(UUID.random(), h5File)
			.setUseCache(true);
		
		// writing
		ColumnDefList defs = new ColumnDefList();
		defs.add(new ColumnDef("c1", "c1 desc", ColumnType.INTEGER));
		defs.add(new ColumnDef("c2", "c2 desc", ColumnType.STRING));
		conn.setColumnDefs(defs);
		int rowCount = 66124;
		for (int i = 0; i < rowCount; i++) {
			Row r = new Row(UUID.random());
			
			r.addColumn("c1", RandomUtils.nextInt(100));
			r.addColumn("c2", UUID.random().getId() + UUID.random().getId());
			
			conn.write(r);
			
			if (i % 22343 == 0) {
				LibsLogger.info(H5ConnectorParallelReadTestCase.class, "Breaking writing..");
				break;
			}
		}
		
		for (Row row : conn.iterateRows(null)) {
			LibsLogger.info(H5ConnectorReadingWhileWritingTestCase.class, "Row ", row);
		}
		
		LibsLogger.info(H5ConnectorReadingWhileWritingTestCase.class, "Finished");
	}
}
