package net.beanscode.cli.settings;

import java.io.IOException;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.web.view.settings.Setting;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.pjf.components.jersey.SessionBean;

public class SettingGateway {
	
	public static void setUserSetting(SessionBean sb, String settingId, String settingKey, String settingValue) throws CLIException, IOException {
		new ApiCallCommand(sb)
			.setApi("api/setting/setSetting")
			.addParam("id", settingId)
			.addParam(settingKey, settingValue)
			.addParam("userEmail", sb.getUserId())
			.run();
	}

	public static Setting getUserSetting(SessionBean sb, String settingId) throws CLIException, IOException {
		if (sb == null)
			return null;
		return new ApiCallCommand(sb)
			.setApi("/api/setting/getSetting")
			.addParam("id", settingId)
			.addParam("userEmail", sb.getUserId())
			.run()
			.getResponse()
			.getAsObject(Setting.class);
	}
	
	public static Setting getSetting(SessionBean sb, String settingId) throws CLIException, IOException {
		return new ApiCallCommand(sb)
			.setApi("/api/setting/getSetting")
			.addParam("id", settingId)
//			.addParam("userEmail", sb.getUserId())
			.run()
			.getResponse()
			.getAsObject(Setting.class);
	}
	
	public static String getSettingAsString(SessionBean sb, String settingId, 
			String settingName, String defaultValue) throws CLIException, IOException {
		Setting setting = getUserSetting(sb, settingId);
		
		if (setting != null) {
			if (setting.getValue(settingName) != null) {
				return setting.getValue(settingName).getValueAsString();
			}
		}
		
		return defaultValue;
	}
}
