package net.beanscode.model.cass;

import static net.hypki.libs5.utils.string.StringUtilities.deserialize;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.beanscode.model.dataset.Table;
import net.hypki.libs5.db.cassandra.MapReduceUtils;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.Results;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.WeblibsConst;

public class DataFactory {
	
	public static Data getData(final List<Table> tables) {
		List<String> dataIds = new ArrayList<>();
		for (Table ts : tables) {
			dataIds.add(ts.getId().getId());
		}
		return getData(WeblibsConst.KEYSPACE, dataIds, null, false);
	}
	
	public static Data getData(final String dataId) {
		return DataFactory.getData(WeblibsConst.KEYSPACE, dataId, null, false, true);
	}
	
	public static Data getData(final String dataId, final boolean cacheRows) {
		return DataFactory.getData(WeblibsConst.KEYSPACE, dataId, null, false, cacheRows);
	}

	public static Data getData(final String keyspace, final String dataId) {
		return DataFactory.getData(keyspace, dataId, null, false, true);
	}

	public static Data getData(final String keyspace, final String dataId, final C3Where filter, final boolean defaultIfColumnIsMissing, final boolean cacheRows) {
		Data data = new Data(dataId);
		data.setKeyspace(keyspace);
		data.setFilter(filter);
		data.setDefaultIfColumnIsMissing(defaultIfColumnIsMissing);
		data.setAllRowsSkipCache(!cacheRows);
		return data;
	}
	
	public static Data getData(final String keyspace, final List<String> dataIds, final C3Where filter, final boolean defaultIfColumnIsMissing) {
		Data data = new Data(dataIds);
		data.setKeyspace(keyspace);
		data.setFilter(filter);
		data.setDefaultIfColumnIsMissing(defaultIfColumnIsMissing);
		return data;
	}
	
	public static Results getPage(final String keyspace, final List<String> dataIds, final String state, final int size) {
		try {
			Results results = DbObject.getDatabaseProvider().getList(keyspace, 
					Data.COLUMN_FAMILY, 
					null, 
					null, 
					size, 
					state,
					new C3Where()
//						.addClause(new C3Clause(Table.COLUMN_TBID, ClauseType.EQ, dataId)));
						.addClause(new C3Clause(Table.COLUMN_TBID, ClauseType.IN, dataIds)));
			
			for (Row row : results.getRows()) {
				if (row.get(DbObject.COLUMN_DATA) instanceof ByteBuffer) {
					ByteBuffer bb = (ByteBuffer) row.get(DbObject.COLUMN_DATA);
					byte [] bytes = MapReduceUtils.getBytes(bb);
					Map<String, Object> map = ((Map<String, Object>) deserialize(bytes));
					row.getColumns().putAll(map);
				}
			}
			
			return results;
		} catch (ClassNotFoundException | IOException e) {
			LibsLogger.error(DataFactory.class, "Cannot deserialize data column", e);
			return null;
		}
	}
	
	public static Results getPage(final String keyspace, final String dataId, final String state, final int size) {
		List<String> dataIds = new ArrayList<>();
		dataIds.add(dataId);
		return getPage(keyspace, dataIds, state, size);
	}

//	public static Iterator<Row> iteratorRows(final String keyspace, final String dataId) {
//		return DataFactory.iteratorRows(keyspace, dataId, null, false);
//	}
//
//	public static Iterator<Row> iteratorRows(final String keyspace, final String dataId, final C3Where filter, final boolean defaultIfColumnIsMissing) {
//		return getData(keyspace, dataId, filter, defaultIfColumnIsMissing).iteratorRows();
//		return new Iterator<Row>() {
//			int rowIndex = 0;
//			Data dataCache = null;
//			
//			@Override
//			public void remove() {
//				throw new org.apache.commons.lang.NotImplementedException();
//			}
//			
//			@Override
//			public Row next() {
//				return dataCache.getRowsCached().get(rowIndex++);
//			}
//			
//			@Override
//			public boolean hasNext() {
//				if (dataCache == null)
//					dataCache = getData(keyspace, dataId, filter, defaultIfColumnIsMissing);
//				
//				if (rowIndex < dataCache.size())
//					return true;
//				
//				rowIndex = 0;
//				while (dataCache.nextPage()) {
//					if (rowIndex < dataCache.size())
//						return true;
//				}
//				
//				return false;
//			}
//		};
//	}

//	public static Iterable<Row> iterateRows(final String keyspace, final String dataId) {
//		return DataFactory.iterateRows(keyspace, dataId, null, false);
//	}
//
//	public static Iterable<Row> iterateRows(final String keyspace, final String dataId, final C3Where filter, final boolean defaultIfColumnIsMissing) {
//		return new Iterable<Row>() {
//			@Override
//			public Iterator<Row> iterator() {
//				return iteratorRows(keyspace, dataId, filter, defaultIfColumnIsMissing);
//			}
//		};
//	}

}
