package net.beanscode.cli.plugin;

import java.io.IOException;

import net.beanscode.cli.BeansCliCommand;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.cli.noninteractivecli.CliArg;
import net.hypki.libs5.pjf.RestMethod;
import net.hypki.libs5.pjf.components.jersey.SessionBean;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.url.Params;

public class EditorClassGet extends BeansCliCommand {
	
	@CliArg(name = "type",
			description = "Model class",
			required = false)
	private String type = null;

	public EditorClassGet(SessionBean sb) {
		super(RestMethod.GET, sb);
	}

	@Override
	public String getUrlPath() {
		return "api/plugin/editorClass";
	}

	@Override
	public void processResponse() {
		LibsLogger.debug(EditorClassGet.class, "Editor class for model class ", getType(), " is ", 
				getResponse().getAsString("editorClass"));
	}

	@Override
	public Params getRESTParams() {
		return new Params()
			.add("type", getType());
	}

	@Override
	public String getCLICommandName() {
		return "plugin editor";
	}

	@Override
	public String getCLICommandDescription() {
		return "Gets the Java class name for editor panel for a model class. This command is usefull only for BEANS web interface.";
	}
	
	public String run(String type) throws CLIException, IOException {
		super.run(new String[] {"--type", type});
		
		return getResponse().getAsString("editorClass");
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
