package net.beanscode.pojo.autoupdate;

import net.hypki.libs5.db.db.weblibs.utils.UUID;

import com.google.gson.annotations.Expose;

public class AutoUpdate {
	
	@Expose
	private int every = 0;
	
	@Expose
	private String type = null;
	
	public AutoUpdate() {
		
	}

	public int getEvery() {
		return every;
	}

	public void setEvery(int every) {
		this.every = every;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
