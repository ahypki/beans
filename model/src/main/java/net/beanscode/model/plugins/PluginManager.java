package net.beanscode.model.plugins;

import java.io.File;
import java.io.FileFilter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.beanscode.model.BeansSettings;
import net.beanscode.model.notebook.NotebookEntry;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.Watch;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.file.FileUtils;
import net.hypki.libs5.utils.file.LazyFileIterator;
import net.hypki.libs5.utils.reflection.ReflectionUtility;
import net.hypki.libs5.utils.reflection.SystemUtils;
import net.hypki.libs5.weblibs.settings.Setting;
import net.hypki.libs5.weblibs.settings.SettingFactory;
import net.hypki.libs5.weblibs.settings.SettingValue;

public class PluginManager {
	
	public static final String PLUGIN_DEFAULT_DIRECTORY = "$HOME/.beans-software/plugins";
	
	private static List<Plugin> allPlugins = null;
	
	private static List<NotebookEntry> allNotebookEntries = null;
	
	private static Map<String, String> modelToEditor = null;
	
	private static Map<String, String> fullNameCache = null;
	
	private static HashMap<String, Boolean> pluginEnabledCache = null;
	
//	static {
//		reload();
//	}
	
	public static String getFullname(String shortName) {
		return fullNameCache.get(shortName);
	}
	
	public static String getEditorClass(String modelClass) {
		return modelToEditor.get(modelClass);
	}
	
	private synchronized static void reloadPluginEditors() {
		try {
			for (Plugin plugin : PluginManager.iterateAllPlugins()) {
				if (plugin.getNotebookEntries() != null)
					for (Class<? extends NotebookEntry> ne : plugin.getNotebookEntries()) {
						try {
							LibsLogger.debug(PluginManager.class, "Adding NotebookEntry ", ne.getSimpleName(), " from Plugin ", plugin.getName());
	//						modelToEditor.put(ne, (Class<? extends NotebookEntryEditorPanel>) Class.forName(ne.newInstance().getEditorClass()));
							modelToEditor.put(ne.getSimpleName(), ne.newInstance().getEditorClass());
							
							fullNameCache.put(ne.getSimpleName(), ne.getName());
						} catch (Exception e) {
							LibsLogger.error(PluginManager.class, "Cannot add this class to Notebook editors", e);
						}
					}
			}
		} catch (Throwable e) {
			LibsLogger.error(PluginManager.class, "Cannot reload plugin editors", e);
		}
	}
	
	private synchronized static void reloadClassLoaderPlugins() {
		Class currentClass = null;
		Watch w = new Watch();
		try {
			for (Class clazz : ReflectionUtility.getClasses("net.beanscode")) {
				currentClass = clazz;
				
				if (ReflectionUtility.hasInterface(currentClass, Plugin.class))
					if (clazz.equals(Plugin.class) == false)
						addPlugin((Plugin) clazz.newInstance());
			}
			LibsLogger.debug(PluginManager.class, "Plugins reloaded from JVM in " + w);
			if (w.ms() > 3_000)
				LibsLogger.warn(PluginManager.class, "Plugins reloaded in more than 3 [s]");
		} catch (Throwable e) {
			LibsLogger.error(PluginManager.class, "Cannot load BEANS plugin " + currentClass, e);
		}
	}
	
	public static String getPluginDefaultFolder() {
		return BeansSettings.getSettingAsString(BeansSettings.SETTING_PLUGIN_FOLDER, "path", PLUGIN_DEFAULT_DIRECTORY);
	}
	
	private synchronized static void reloadThisJarPlugins() {
		String jarPath = ReflectionUtility.getJarExecutionPath(PluginManager.class);
		
		LibsLogger.debug(PluginManager.class, "This is running from jar " + jarPath);
		
		if (SystemUtils.isRunningFromJar()) {
			try {
			
				for (Class beansPlugin : ReflectionUtility.getClassesFromJARFile(jarPath, "net.beanscode")) {
					if (ReflectionUtility.hasInterface(beansPlugin, Plugin.class) && beansPlugin.isInterface() == false) {
						LibsLogger.debug(PluginManager.class, "Loading plugin from this jar ", beansPlugin);
						
						addPlugin((Plugin) beansPlugin.newInstance());
					}
				}
			} catch (ClassNotFoundException | IOException | InstantiationException | IllegalAccessException e) {
				LibsLogger.error(PluginManager.class, "Cannot load plugin from this jar", e);
			}
		}
	}

	private synchronized static void reloadJarPlugins() {
		try {
			LibsLogger.debug(PluginManager.class, "Reloading plugins from jar files...");
			final FileExt pluginDir = new FileExt(getPluginDefaultFolder());
			if (!pluginDir.exists()) {
				pluginDir.mkdirs();
			}
			LibsLogger.debug(PluginManager.class, "Reloading plugins from ", pluginDir.getAbsolutePath());
			
			FileFilter onlyJarFiles = new FileFilter() {
				@Override
				public boolean accept(File pathname) {
					return pathname.getPath().endsWith(".jar");
				}
			};
			
			for (File jar : new LazyFileIterator(pluginDir, false, true, false, onlyJarFiles)) {
				LibsLogger.debug(PluginManager.class, "Found jar file ", jar);
				
				try {
					for (Class beansPlugin : ReflectionUtility.getClassesFromJARFile(jar.getAbsolutePath(), null)) {
						if (ReflectionUtility.hasInterface(beansPlugin, Plugin.class) && beansPlugin.isInterface() == false) {
							LibsLogger.debug(PluginManager.class, "Loading plugin ", beansPlugin);
							
							addPlugin((Plugin) beansPlugin.newInstance());
						}
					}
				} catch (ClassNotFoundException | IOException | InstantiationException | IllegalAccessException e) {
					LibsLogger.error(PluginManager.class, "Cannot load plugin in the jar file " + jar.getPath(), e);
				}
			}
			
			LibsLogger.debug(PluginManager.class, "Plugins reloaded from jar files");
		} catch (Throwable t) {
			LibsLogger.error(PluginManager.class, "Cannot reload jar plugins", t);
		}
	}
	
	private synchronized static void reloadDefautPlugins() {
		try {
			// adding default Plugins
			for (Class<? extends Plugin> clazz : getDefaultPlugins()) {
				try {
					addPlugin(clazz.newInstance());
				} catch (Exception e) {
					LibsLogger.error(PluginManager.class, "Cannot load plugin " + clazz + " defined in settings file", e);
				}
			}
		} catch (Throwable t) {
			LibsLogger.error(PluginManager.class, "Cannot reload default plugins", t);
		}
	}
	
	public synchronized static void reload() {
		if (allPlugins != null) {
			LibsLogger.debug(PluginManager.class, "Plugins already reloaded, skipping reloading");
			return;
		}
		
		Watch w = new Watch();
		
		try {
			allPlugins = new ArrayList<Plugin>();
			
			modelToEditor = new HashMap<String, String>();
			fullNameCache = new HashMap<String, String>();
			
	//		fullNameCache.put(NotebookEntry.class.getSimpleName(), No)
			
			allNotebookEntries = null;
			
			// TODO remove BEANS/Plugins plugins from settings files
	//		String plugins = Settings.getString("BEANS/Plugins", null);
	//		if (StringUtilities.notEmpty(plugins)) {
	//			for (String plugin : StringUtilities.split(plugins, ',')) {
	//				try {
	//					Class clazz = Class.forName(plugin);
	//					if (clazz != null) {
	//						addPlugin(((Class<Plugin>) clazz).newInstance());
	//					}
	//				} catch (ClassNotFoundException e) {
	//					LibsLogger.error(PluginManager.class, "Cannot load plugin " + plugin + " defined in settings file");
	//				} catch (Throwable e) {
	//					LibsLogger.error(PluginManager.class, "Cannot load plugin " + plugin + " defined in settings file", e);
	//				}
	//			}
	//		}
			
			reloadClassLoaderPlugins();
			
			reloadThisJarPlugins();
			
			reloadJarPlugins();
			
			reloadDefautPlugins();
			
			reloadPluginEditors();
		
			for (Plugin plugin : iterateAllPlugins()) {
				if (plugin.getConnectorClasses() != null)
					for (Class clazz : plugin.getConnectorClasses()) {
						LibsLogger.debug(PluginManager.class, "Connector ", clazz, " loaded");
					}
			}
			LibsLogger.info(PluginManager.class, "Plugin manager reloaded in " + w);
		} catch (Throwable t) {
			LibsLogger.error(PluginManager.class, "Cannot reload connectors", t);
		}
	}
	
	public static List<Class<? extends Plugin>> getDefaultPlugins() {
		List<Class<? extends Plugin>> plugins = new ArrayList<Class<? extends Plugin>>();
		
//		plugins.add(TextPlugin.class);
//		plugins.add(PlotPlugin.class);
//		plugins.add(GnuplotPlugin.class);
//		plugins.add(PigPlugin.class);
//		plugins.add(AttachmentPlugin.class);
		
//		plugins.add(LucenePlugin.class);
		
//		plugins.add(MapDbPlugin.class);
//		plugins.add(CassandraDbPlugin.class);
		
//		plugins.add(ElasticPlugin.class);
		
//		plugins.add(PythonPlugin.class);
		
		return plugins;
	}
	
	public static boolean addPlugin(Plugin plugin) {
		for (Plugin pl : allPlugins) {
			if (pl.getClass().getName().equals(plugin.getClass().getName())) {
				LibsLogger.debug(PluginManager.class, "Plugin ", plugin.getName(), " already loaded with version ", 
						plugin.getVersion(), ", skipping...");
				return false;
			}
		}
		
		allPlugins.add(plugin);
		LibsLogger.debug(PluginManager.class, "Added plugin ", plugin.getName());
		
		return true;
	}
	
	public static boolean isPluginEnabled(Plugin plugin) {
		try {
			if (pluginEnabledCache == null)
				pluginEnabledCache = new HashMap<String, Boolean>();
			
			if (pluginEnabledCache.containsKey(plugin.getName()))
				return pluginEnabledCache.get(plugin.getName());
			
			if (plugin == null)
				return false;
			
			Setting setting = SettingFactory.getSetting(BeansSettings.SETTING_PLUGIN_LIST);
			
			if (setting == null)
				return false;
			
			SettingValue val = setting.getValue(plugin.getClass().getName());
			
			if (val != null) {
				pluginEnabledCache.put(plugin.getName(), val.getValueAsBoolean());
				return val.getValueAsBoolean();
			}
			
			pluginEnabledCache.put(plugin.getName(), false);
			return false;
		} catch (IOException e) {
			LibsLogger.error(PluginManager.class, "Cannot check if plugin " + plugin + " is enabled", e);
			return false;
		}
	}
	
	public static boolean enablePlugin(Plugin plugin) {
		if (!isPluginEnabled(plugin)) {
			try {
				Setting setting = SettingFactory.getSetting(BeansSettings.SETTING_PLUGIN_LIST);
				
				SettingValue val = setting.getValue(plugin.getClass().getName());
				
				if (val != null)
					val.setValue(Boolean.TRUE);
				else
					setting.addValue(new SettingValue(plugin.getClass().getName(), Boolean.TRUE));
				
				setting.save();

				allNotebookEntries = null;
				
				pluginEnabledCache.put(plugin.getName(), true);
				
				return true;
			} catch (ValidationException | IOException e) {
				LibsLogger.error(PluginManager.class, "Cannot enable plugin " + plugin, e);
			}
		}
		return false;
	}
	
	public static boolean disablePlugin(Plugin plugin) {
		if (isPluginEnabled(plugin)) {
			try {
				Setting setting = SettingFactory.getSetting(BeansSettings.SETTING_PLUGIN_LIST);
				
				SettingValue val = setting.getValue(plugin.getClass().getName());
				
				if (val != null)
					val.setValue(Boolean.FALSE);
				else
					setting.addValue(new SettingValue(plugin.getClass().getName(), Boolean.FALSE));
				
				setting.save();

				allNotebookEntries = null;
				
				pluginEnabledCache.put(plugin.getName(), false);
				
				return true;
			} catch (ValidationException | IOException e) {
				LibsLogger.error(PluginManager.class, "Cannot disable plugin " + plugin, e);
			}
		}
		return false;
	}
	
	public static Plugin getPlugin(String pluginName) {
		for (Plugin pl : allPlugins)
			if (pl.getClass().getName().equals(pluginName)
					|| pl.getName().equals(pluginName))
				return pl;
		return null;
	}
	
	public static Iterable<Plugin> iterateAllPlugins() {
		return new Iterable<Plugin>() {
			@Override
			public Iterator<Plugin> iterator() {
				return allPlugins.iterator();
			}
		};
	}
	
	public static Iterable<NotebookEntry> iterateAllNotebookEntries() {
		if (allNotebookEntries == null) {
			allNotebookEntries = new ArrayList<>();
			for (Plugin plugin : iterateAllPlugins()) {
				if (plugin.getNotebookEntries() != null
						&& isPluginEnabled(plugin))
					for (Class<? extends NotebookEntry> notebookEntry : plugin.getNotebookEntries()) {
						try {
							allNotebookEntries.add(notebookEntry.newInstance());
						} catch (InstantiationException | IllegalAccessException e) {
							LibsLogger.error(PluginManager.class, "Cannot create an instance of NotebookEntry " + notebookEntry.getSimpleName(), e);
						}
					}
			}
		}
		
		return new Iterable<NotebookEntry>() {
			@Override
			public Iterator<NotebookEntry> iterator() {
				return allNotebookEntries.iterator();
			}
		};
	}
	
	public static void main(String[] args) {
		for (Plugin pl : PluginManager.iterateAllPlugins()) {
			LibsLogger.info(PluginManager.class, "Plugin " + pl);
		}
	}
	
	public static String selfTestStatus(String pluginName) {
		if (new FileExt(BeansSettings.getBeansHomeFolder().getAbsolutePath(), "selftests", pluginName, "tmp").exists())
			return "running";
		else if (new FileExt(BeansSettings.getBeansHomeFolder().getAbsolutePath(),  "selftests", pluginName, "log.ok").exists())
			return "ok";
		else if (new FileExt(BeansSettings.getBeansHomeFolder().getAbsolutePath(), "selftests", pluginName, "log.error").exists())
			return "error";
		else
			return "unknown";
	}
	
	public static String selfTestLog(String pluginName) {
		FileExt f = null;
		if ((f = new FileExt(BeansSettings.getBeansHomeFolder().getAbsolutePath(), "selftests", pluginName, "tmp")).exists())
			return FileUtils.readString(f);
		else if ((f = new FileExt(BeansSettings.getBeansHomeFolder().getAbsolutePath(),  "selftests", pluginName, "log.ok")).exists())
			return FileUtils.readString(f);
		else if ((f = new FileExt(BeansSettings.getBeansHomeFolder().getAbsolutePath(), "selftests", pluginName, "log.error")).exists())
			return FileUtils.readString(f);
		else
			return "unknown";
	}
	
	private static void selfTestClear(String pluginName) throws IOException {
		final FileExt log = new FileExt(BeansSettings.getBeansHomeFolder().getAbsolutePath(), "selftests", pluginName, "tmp");
		final FileExt logError = new FileExt(BeansSettings.getBeansHomeFolder().getAbsolutePath(), "selftests", pluginName, "log.error");
		final FileExt logOK = new FileExt(BeansSettings.getBeansHomeFolder().getAbsolutePath(), "selftests", pluginName, "log.ok");

		log.mkdirs();
		
		log.deleteIfExists();
		logOK.deleteIfExists();
		logError.deleteIfExists();
	}
	
	public static boolean selfTestPlugin(UUID userId, String pluginName) throws IOException, ValidationException {
		final FileExt log = new FileExt(BeansSettings.getBeansHomeFolder().getAbsolutePath(), "selftests", pluginName, "tmp");
		final FileExt logError = new FileExt(BeansSettings.getBeansHomeFolder().getAbsolutePath(), "selftests", pluginName, "log.error");
		final FileExt logOK = new FileExt(BeansSettings.getBeansHomeFolder().getAbsolutePath(), "selftests", pluginName, "log.ok");
		
		try {
			final Plugin plugin = PluginManager.getPlugin(pluginName);	

			log.mkdirs();
			
			selfTestClear(pluginName);
			
			if (plugin == null) {
				FileUtils.appendToFile("ERROR Plugin " + pluginName + " not found", logError.getAbsolutePath());
				return false;
			}
			
			final FileWriter fw = new FileWriter(log);
			
			final boolean status = plugin.selfTest(userId, fw);
			
			fw.close();
			
			if (status) {
				FileUtils.moveFile(log, logOK);
			} else {
				FileUtils.moveFile(log, logError);
			}
			
			return status;
		} catch (Throwable t) {
			LibsLogger.error(PluginManager.class, "Cannot perform self test for plugin " + pluginName, t);
			FileUtils.appendToFile("ERROR Cannot perform self test for plugin " + pluginName, logError.getAbsolutePath());
			return false;
		}
	}

	public static void selfTestPluginInBackground(UUID userId, String pluginName) throws ValidationException, IOException {
		selfTestClear(pluginName);
		
		new SelfTestJob()
			.setUserId(userId)
			.setPluginName(pluginName)
			.save();
	}

	/**
	 * Start self tests for all plugins.
	 * 
	 * @param userId
	 * @throws ValidationException
	 * @throws IOException
	 */
	public static void selfTestPluginInBackground(UUID userId) throws ValidationException, IOException {
		for (Plugin plugin : iterateAllPlugins())
			selfTestPluginInBackground(userId, plugin.getName());
	}
}
