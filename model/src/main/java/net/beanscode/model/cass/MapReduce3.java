package net.beanscode.model.cass;

import java.util.List;

import net.hypki.libs5.db.weblibs.Settings;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.Watch;

import org.apache.cassandra.hadoop.ConfigHelper;
import org.apache.cassandra.hadoop.cql3.CqlConfigHelper;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public abstract class MapReduce3 extends Configured implements Tool {

	protected abstract String getKeyspace();

	protected abstract String getInputTable();

	protected abstract String getOutputTable();
	
	protected abstract String getOutputCql();

	protected abstract Class<? extends Mapper> getMapper();

	protected abstract Class<? extends Reducer> getReducer();
	
	protected void additionalConfiguration(Configuration conf) {
		
	}

	@Override
	public int run(String[] args) throws Exception {
		final Watch watch = new Watch();
		final String host = Settings.getString("Cassandra/host", "localhost");
		final String port = Settings.getString("Cassandra/port", "29160");
		final String rpcPort = Settings.getString("Cassandra/rpcPort", "29042");
		
		Job job = new Job(getConf(), getClass().getSimpleName());
		job.setJarByClass(getClass());
		
		additionalConfiguration(job.getConfiguration());

		job.setReducerClass(getReducer());

		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(Text.class);

		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(List.class);

//		 job.setInputFormatClass(BeansCqlInputFormat.class);
//		job.setInputFormatClass(CqlInputFormat.class);

//		job.setOutputFormatClass(BeansCqlOutputFormat.class);

		ConfigHelper.setOutputColumnFamily(job.getConfiguration(), getKeyspace(), getOutputTable());
		CqlConfigHelper.setOutputCql(job.getConfiguration(), getOutputCql());
		ConfigHelper.setOutputInitialAddress(job.getConfiguration(), host);
		ConfigHelper.setOutputPartitioner(job.getConfiguration(), "Murmur3Partitioner");

		job.setMapperClass(getMapper());
//		job.setInputFormatClass(CqlInputFormat.class);

		ConfigHelper.setInputRpcPort(job.getConfiguration(), port);
		ConfigHelper.setOutputRpcPort(job.getConfiguration(), port);
		job.getConfiguration().set("cassandra.input.thrift.port", port);
		job.getConfiguration().set("cassandra.output.thrift.port", port);
		job.getConfiguration().set("cassandra.input.native.port", rpcPort);
		job.getConfiguration().set("cassandra.output.native.port", rpcPort);

		ConfigHelper.setInputInitialAddress(job.getConfiguration(), host);
		ConfigHelper.setOutputInitialAddress(job.getConfiguration(), host);

		ConfigHelper.setInputColumnFamily(job.getConfiguration(), getKeyspace(), getInputTable());

		ConfigHelper.setInputPartitioner(job.getConfiguration(), "Murmur3Partitioner");

//		CqlConfigHelper.setInputCQLPageRowSize(job.getConfiguration(), "3");

		// it does not help
//		CqlConfigHelper.setInputMaxConnections(job.getConfiguration(), "1000");
//		CqlConfigHelper.setInputMaxSimultReqPerConnections(job.getConfiguration(), "1000");
//		CqlConfigHelper.setInputNativeReuseAddress(job.getConfiguration(), "true");
//		CqlConfigHelper.setInputNativeKeepAlive(job.getConfiguration(), "true");
//		CqlConfigHelper.setInputNativeTcpNodelay(job.getConfiguration(), "true");

		job.waitForCompletion(true);
		
		LibsLogger.debug(MapReduce3.class, "MapReduce job ", getClass().getSimpleName(), " finished in ", watch);
		return 0;
	}

	public void runMapReduce(final String[] args) throws Exception {
		ToolRunner.run(new Configuration(), this, args);
	}

	public void runMapReduce() throws Exception {
		runMapReduce(null);
	}

}
