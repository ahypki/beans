package net.beanscode.model.backup;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.pig.ExecType;

import com.google.gson.annotations.Expose;

import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.notebook.NotebookEntry;
//import net.beanscode.plugin.butcheredpig.ButcheredPigEntry;
//import net.beanscode.plugin.pig.PigScript;
//import net.beanscode.plugin.text.TextEntry;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.BigFile;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.weblibs.jobs.Job;
import net.hypki.libs5.weblibs.user.User;
import net.hypki.libs5.weblibs.user.UserFactory;

public class RestoreNotebooksJob extends Job {
	
	@Expose
	private String fileToRestore = null;
	
	@Expose
	private String userEmail = null;

	public RestoreNotebooksJob() {
		
	}
	
	@Override
	public void run() throws IOException, ValidationException {
//		String backupFile = SimpleDate.now().toStringISO() + "_" + WeblibsConst.KEYSPACE + "_pigqueries.bac";
//		
//		Setting backupPathSetting = SettingFactory.getSetting(BeansSettings.SETTING_BACKUP_PATH);
//		assertTrue(backupPathSetting != null, BeansSettings.SETTING_BACKUP_PATH, " setting does not exist");
//		
//		FileExt backupFolder = new FileExt(backupPathSetting.getValue("path").getValueAsString());
//		backupFolder.mkdirs();
//		
//		BackupManager.backupPlainPigQueries(WeblibsConst.KEYSPACE_LOWERCASE, backupFolder.getAbsolutePath() + "/" + backupFile);
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		FileExt toRestoreFile = new FileExt(getFileToRestore());
		
		if (!toRestoreFile.exists())
			return;
		
		User user = UserFactory.getUser(getUserEmail());
		
		Notebook notebook = new Notebook(user.getUserId(), "Restoring Pig queries");
		notebook.save();
		
		List<NotebookEntry> entries = new ArrayList<>();
		
		String name = "";
		StringBuilder sb = new StringBuilder();
		for (String line : new BigFile(getFileToRestore())) {
			if (line.startsWith("-- name:")) {
				// new Pig query
				if (notEmpty(sb.toString()))
					processOnePigQuery(notebook, entries, name, sb.toString());
				sb = new StringBuilder();
				
				name = line.replace("-- name:", "");
				continue;
			} else if (line.startsWith("-- create:")) {
				// do nothing
			} else if (line.startsWith("-- notebookId:")) {
				// do nothing
			} else if (line.startsWith("-- user:")) {
				// do nothing
			} else {
				sb.append(line);
				sb.append("\n");
			}
		}
		
		notebook.save();
		for (NotebookEntry ne : entries) {
			ne.save();
		}
		
		LibsLogger.info(RestoreNotebooksJob.class, "Finished");
	}

	private void processOnePigQuery(Notebook n, List<NotebookEntry> entries, 
			String name, String query) throws ValidationException, IOException {
		// creating Pig query, ButcheredPig query, and Text separator
//		PigScript pig = new PigScript(n, name);
//		pig.setPigMode(ExecType.LOCAL);
//		pig.setQuery(query);
//		n.addEntryId(pig.getId());
//		entries.add(pig);
//		
//		ButcheredPigEntry bpig = new ButcheredPigEntry(n, name);
//		bpig.setQuery(query);
//		n.addEntryId(bpig.getId());
//		entries.add(bpig);
//		
//		TextEntry tx = new TextEntry(n, "### -----------------------------------------");
//		n.addEntryId(tx.getId());
//		entries.add(tx);
	}

	public String getFileToRestore() {
		return fileToRestore;
	}

	public RestoreNotebooksJob setFileToRestore(String fileToRestore) {
		this.fileToRestore = fileToRestore;
		return this;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public RestoreNotebooksJob setUserEmail(String userEmail) {
		this.userEmail = userEmail;
		return this;
	}

}
