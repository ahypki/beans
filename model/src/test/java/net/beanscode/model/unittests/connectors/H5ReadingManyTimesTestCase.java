package net.beanscode.model.unittests.connectors;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.beanscode.model.connectors.ConnectorList;
import net.beanscode.model.connectors.H5Connector;
import net.beanscode.model.plugins.ColumnDef;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.Watch;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.string.RandomUtils;

import org.junit.Test;

public class H5ReadingManyTimesTestCase extends ConnectorTestCase {

	public H5ReadingManyTimesTestCase() {
		
	}
	
	@Test
	public void testConnector() throws IOException {
		FileExt h5File = new FileExt("test.h5");
		h5File.deleteIfExists();
		
		H5Connector conn = new H5Connector(UUID.random(), h5File)
			.setUseCache(true);
		
		writeRows(conn, 66123);

		double sum = 0.0;
		int nrOfOpenings = 100_123;
		
		LibsLogger.info(H5ReadingManyTimesTestCase.class, "Opening H5 file " + nrOfOpenings + " times...");
		for (int i = 0; i < nrOfOpenings; i++) {
			H5Connector newConn = new H5Connector(UUID.random(), h5File);
			for (Row row : newConn.iterateRows(null)) {
				sum += row.getAsDouble("c1");
				break;
			}
		}
		
		LibsLogger.info(H5ReadingManyTimesTestCase.class, "Finished with sum ", sum);
	}
}
