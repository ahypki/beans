package net.beanscode.rest.settings;

import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import net.beanscode.model.settings.ColorPalette;
import net.beanscode.rest.BeansRest;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.url.HttpResponse;
import net.hypki.libs5.utils.utils.ColorUtils;
import net.hypki.libs5.weblibs.user.Email;
import net.hypki.libs5.weblibs.user.User;

import org.quartz.SchedulerException;

@Path("api/colorpalette")
public class ColorPaletteRest extends BeansRest {

	@POST
    @Path("set")
	public void set(@Context SecurityContext sc,
			@FormParam("userId") UUID userId,
			@FormParam("colors") List<String> colorsCSS) throws IOException, ValidationException, SchedulerException {
		validateBadRequest(userId != null, "User ID not specified");
		validateUnauthorized(isAdmin(sc) || userId.equals(getUserId(sc)), "Permission denied");
		
		ColorPalette palette = ColorPalette.getColorPalette(userId);
		
		if (palette == null)
			palette = new ColorPalette();
		
		palette.getColors().clear();
		for (String color : colorsCSS) {
//			color = UrlUtilities.urlDecode(color);
			palette.getColors().add(ColorUtils.parseCssRgb(color).getRGB());
		}
		
		ColorPalette.saveColorPalette(userId, palette);
	}
	
	@GET
    @Path("get")
    @Produces(MediaType.APPLICATION_JSON)
	public Response get(@Context SecurityContext sc,
			@QueryParam("userId") UUID userId) throws IOException, ValidationException, SchedulerException {
		validateBadRequest(userId != null, "User ID not specified");
		validateUnauthorized(isAdmin(sc) || userId.equals(getUserId(sc)), "Permission denied");
		
		ColorPalette palette = ColorPalette.getColorPalette(userId);
		
		if (palette == null)
			palette = ColorPalette.defaultPalette();
		
		return Response
				.ok()
				.entity(JsonUtils.objectToStringPretty(palette))
				.build();
	}
	
	@GET
    @Path("default")
    @Produces(MediaType.APPLICATION_JSON)
	public Response getDefault(@Context SecurityContext sc) throws IOException, ValidationException, SchedulerException {
		return Response
				.ok()
				.entity(JsonUtils.objectToStringPretty(ColorPalette.defaultPalette()))
				.build();
	}
}
