package net.beanscode.model.cass.notification;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.NotImplementedException;

import net.beanscode.model.BeansSearchManager;
import net.beanscode.model.rights.Group;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.search.SearchResults;
import net.hypki.libs5.search.query.BooleanTerm;
import net.hypki.libs5.search.query.SortOrder;
import net.hypki.libs5.search.query.StringTerm;
import net.hypki.libs5.search.query.TermRequirement;
import net.hypki.libs5.search.query.WildcardTerm;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.weblibs.SearchManager;
import net.hypki.libs5.weblibs.WeblibsConst;

public class NotificationFactory {
	
	private static int PER_PAGE_1000 = 1000;
	
	public static Notification getNotification(String id) throws IOException {
		return getNotification(new UUID(id));
	}

	public static Notification getNotification(UUID id) throws IOException {
		return DbObject.getDatabaseProvider().get(WeblibsConst.KEYSPACE, 
				Notification.COLUMN_FAMILY, 
				new C3Where()
						.addClause(new C3Clause(DbObject.COLUMN_PK, ClauseType.EQ, id.getId())), 
				DbObject.COLUMN_DATA, 
				Notification.class);
	}

	public static SearchResults<Notification> searchNotifications(UUID userId, int from, int size, 
			boolean withArchived, String filter, String type) throws IOException {
		net.hypki.libs5.search.query.Query q = new net.hypki.libs5.search.query.Query();
		
		if (userId != null)
			q.addTerm(new StringTerm("userId.id", userId.getId(), TermRequirement.MUST));
		
		if (!withArchived)
			q.addTerm(new BooleanTerm("archived", false, TermRequirement.MUST));
		
		if (notEmpty(filter))
			q.addTerm(new WildcardTerm("title", filter, TermRequirement.MUST));
		
		if (notEmpty(type))
			q.addTerm(new StringTerm("notificationType", type.toLowerCase(), TermRequirement.MUST));
		
		q.setSortBy("creationMs", SortOrder.DESCENDING);

		return SearchManager.searchInstance().search(Notification.class, 
				WeblibsConst.KEYSPACE_LOWERCASE, 
				Notification.COLUMN_FAMILY.toLowerCase(), 
				q, 
				from, 
				size);
	}
	
	public static Iterable<Notification> iterateNotifications(final UUID userId, final boolean withArchived,
			final String filter) {
		return new Iterable<Notification>() {
			@Override
			public Iterator<Notification> iterator() {
				return new Iterator<Notification>() {
					private int from = 0;
					
					private int notificationIdx = 0;
					
					private List<Notification> notifications = null;
					
					@Override
					public void remove() {
						throw new NotImplementedException();
					}
					
					@Override
					public Notification next() {
						return notifications.get(notificationIdx++);
					}
					
					@Override
					public boolean hasNext() {
						try {
							if (notifications == null || notifications.size() == notificationIdx) {
								notifications = NotificationFactory
										.searchNotifications(userId, from, PER_PAGE_1000, withArchived, filter, null)
										.getObjects();
								notificationIdx = 0;
								from += notifications.size();
							}
							return notificationIdx < notifications.size();
						} catch (IOException e) {
							LibsLogger.error(BeansSearchManager.class, "Cannot get the next page with results", e);
							return false;
						}
					}
				};
			}
		};
	}
}
