package net.beanscode.model;

import net.beanscode.model.cass.TableLocation;
import net.beanscode.model.dataset.Table;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.CacheManager;

public class BeansCacheManager {

	public static void put(String location, TableLocation tableLocation) {
		CacheManager
			.cacheInstance()
			.put(TableLocation.class.getSimpleName(), "LOCATION_" + location, tableLocation);
	}
	
	public static TableLocation getTableLocation(String location) {
		return CacheManager.cacheInstance().get(TableLocation.class, TableLocation.class.getSimpleName(), "LOCATION_" + location);
	}
	
	public static void clearTableLocationCache() {
		String cacheName = TableLocation.class.getSimpleName();
		CacheManager.cacheInstance().clearCache(cacheName);
		LibsLogger.debug(BeansCacheManager.class, "Cache ", cacheName, " cleared");
	}
		
//	public static Table getTable(UUID tableId) {
//		String key = tableId.getId();
//		return CacheManager.cacheInstance().get(Table.class, Table.class.getSimpleName(), key);
//	}
	
	public static Table getTable(UUID queryId, String name) {
		String key = queryId.getId() + ":" + name;
		return CacheManager.cacheInstance().get(Table.class, Table.class.getSimpleName(), key);
	}
	
	public static void put(UUID queryId, String name, Table table) {
		String key = queryId.getId() + ":" + name;
		CacheManager.cacheInstance().put(Table.class.getSimpleName(), key, table);
	}
	
	public static void clearTableCache() {
		String cacheName = Table.class.getSimpleName();
		CacheManager.cacheInstance().clearCache(cacheName);
		LibsLogger.debug(BeansCacheManager.class, "Cache ", cacheName, " cleared");
	}
}
