package net.beanscode.model.utils;

import java.io.IOException;

import org.javatuples.Pair;
import org.markdownj.MarkdownProcessor;

import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.DatasetFactory;
import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.notebook.NotebookEntry;
import net.beanscode.model.notebook.NotebookEntryFactory;
import net.beanscode.model.notebook.NotebookFactory;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.search.SearchResults;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.string.Tokenizer;

public class MarkdownUtils {

	private static MarkdownProcessor markdownProcessor = null;
	
	static {
		LibsLogger.debug(MarkdownUtils.class, "Initializing MarkdownProcessor");
		markdownProcessor = new MarkdownProcessor();
	}
	
	public static String toHtml(final String markdownText) {
		return toHtml(markdownText, null);
	}
	
	public static String toHtml(final String markdownText, final UUID userId) {
		if (markdownText == null)
			return null;
		
		try {
			// compiling markdown
			String newText = markdownProcessor.markdown(markdownText);
			String oldTxt = newText;
	
			// compiling BEANS specific keywords
			/////////////////////////////////////////////////
			
			// (notebook ...) or (notebooks ...)
			for (String keyword : new String[] {"(notebook ", "(notebooks "}) {
				Pair<String, String> tmp = null;
				while ((tmp = getNext(keyword, newText)) != null) {
					String links = replaceNotebook(tmp.getValue0(), userId, 10);
					
					if (StringUtilities.notEmpty(tmp.getValue1()))
						newText = newText.replace(keyword + tmp.getValue0() + ")[" + tmp.getValue1() + "]", links);
					else
						newText = newText.replace(keyword + tmp.getValue0() + ")", links);
					
					// safety valve
					if (newText.equals(oldTxt))
						break;
					
					oldTxt = newText;
				}
			}
			
			// (dataset ...) or (datasets ...)
			for (String keyword : new String[] {"(dataset ", "(datasets "}) {
				Pair<String, String> tmp = null;
				while ((tmp = getNext(keyword, newText)) != null) {
					String links = replaceDataset(tmp.getValue0(), userId, 10);
					
					if (StringUtilities.notEmpty(tmp.getValue1()))
						newText = newText.replace(keyword + tmp.getValue0() + ")[" + tmp.getValue1() + "]", links);
					else
						newText = newText.replace(keyword + tmp.getValue0() + ")", links);
					
					// safety valve
					if (newText.equals(oldTxt))
						break;
					
					oldTxt = newText;
				}
				
			}
			
			// (entry ...) or (entries ...)
			for (String keyword : new String[] {"(entry ", "(entries "}) {
				Pair<String, String> tmp = null;
				while ((tmp = getNext(keyword, newText)) != null) {
					Tokenizer tokenizer = new Tokenizer(tmp.getValue0());
					String notebookQuery = "";//tokenizer.pullOut("notebooks[\\s]*=[\\s]*\"([^\\\"]+)\"");
					String entryQuery = "";//     tokenizer.pullOut("entries[\\s]*=[\\s]*\"([^\\\"]+)\"");
					if (tokenizer.trim().startsWith("notebooks"))
						notebookQuery = tokenizer
											.consumeLeft("notebooks")
											.consumeLeft("=")
											.consumeLeft("\"")
											.expectedMatchRegex("[^\\\"]+");
					tokenizer
						.consumeLeft("\"");
					if (tokenizer.trim().startsWith("entries"))
						entryQuery = tokenizer
											.consumeLeft("entries")
											.consumeLeft("=")
											.consumeLeft("\"")
											.expectedMatchRegex("[^\\\"]+");
					String links = replaceEntries(notebookQuery, entryQuery, userId, keyword.equals("(entry ") ? 1 : 10);
					
					if (StringUtilities.notEmpty(tmp.getValue1()))
						newText = newText.replace(keyword + tmp.getValue0() + ")[" + tmp.getValue1() + "]", links);
					else
						newText = newText.replace(keyword + tmp.getValue0() + ")", links);
					
					// safety valve
					if (newText.equals(oldTxt))
						break;
					
					oldTxt = newText;
				}
			}
			
			return newText;
		} catch (Throwable t) {
			LibsLogger.error(MarkdownUtils.class, "Cannot compile text  ", t);
			return markdownText;
		}
	}
	
	private static String replaceNotebook(String notebookQuery, UUID userId, int maxSize) throws IOException {
		String links = "";
		if (notebookQuery.length() == UUID.LENGTH) {
			// one item
			Notebook n = Notebook.getNotebook(notebookQuery);
			links += "<a href=\"/notebook/" + n.getId() + "\">" + n.getName() + "</a> &nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	";
		} else {
			// multiple items
			SearchResults<Notebook> notebooksFound = NotebookFactory.searchNotebooks(userId, notebookQuery, 0, maxSize);
			for (Notebook n : notebooksFound) {
				links += "<a href=\"/notebook/" + n.getId() + "\">" + n.getName() + "</a> &nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	";
			}
			
			// TODO 
//			links += compileSearch(notebookQuery, userId, contextId, linkReplacement, "All ");
		}
		return links;
	}
	
	private static String replaceDataset(String datasetQuery, UUID userId, int maxSize) throws IOException {
		String links = "";
		if (datasetQuery.length() == UUID.LENGTH) {
			// one item
			Dataset d = DatasetFactory.getDataset(datasetQuery);
			links += "<a href=\"/dataset/" + d.getId() + "\">" + d.getName() + "</a> &nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	";
		} else {
			// multiple items
			SearchResults<Dataset> datasetsFound = DatasetFactory.searchDatasets(userId, datasetQuery, 0, maxSize);
			for (Dataset d : datasetsFound) {
				links += "<a href=\"/dataset/" + d.getId() + "\">" + d.getName() + "</a> &nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	";
			}
			
			// TODO 
//			links += compileSearch(notebookQuery, userId, contextId, linkReplacement, "All ");
		}
		return links;
	}
	
	private static String replaceEntries(String notebookQuery, String entryQuery, UUID userId, int maxSize) throws IOException {
		String links = "";
		// multiple items
		SearchResults<Notebook> notebook = NotebookFactory.searchNotebooks(userId, notebookQuery, 0, 1);
		if (notebook.size() == 1) {
			SearchResults<NotebookEntry> neFound = NotebookEntryFactory.searchNotebookEntries(notebook.getObjects().get(0).getId(), userId, entryQuery, 0, maxSize);
			for (NotebookEntry ne : neFound) {
				links += "entry:" + ne.getId() + " ";// TODO 
			}
		}
		return links;
	}
	
	private static Pair<String, String> getNext(String command, String text) {
		int i = -1;
		if ((i = text.indexOf(command)) >= 0) {
			int start = i + command.length();
			int stop = text.indexOf(")", start);
			String name = null;
			if (text.charAt(stop + 1) == '[') {
				int stopBracket = text.indexOf("]", stop + 1);
				name = text.substring(stop + 2, stopBracket);
			}
			return new Pair<String, String>(text.substring(start, stop), name);
		}
		return null;
	}
}
