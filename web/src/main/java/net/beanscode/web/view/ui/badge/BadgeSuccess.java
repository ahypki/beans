package net.beanscode.web.view.ui.badge;

import static org.rendersnake.HtmlAttributesFactory.class_;

import java.io.IOException;

import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

public class BadgeSuccess extends Badge {

	public BadgeSuccess() {
		
	}
	
	public BadgeSuccess(String text) {
		super(text, Badge.SUCCESS);
	}
}
