package net.beanscode.model;

import java.io.IOException;
import java.util.Map;

import net.hypki.libs5.storage.StorageProvider;
import net.hypki.libs5.storage.local.StorageLocal;
import net.hypki.libs5.utils.LibsLogger;

public class BeansStorageManager {
	
	private static StorageProvider storageProvider = null;

	public static StorageProvider getStorageProvider() {
		if (storageProvider == null) {
			try {
				storageProvider = new StorageLocal();
				
				Map<String, Object> init = new java.util.HashMap<>();
				init.put(StorageLocal.META_STORAGE_PATH, BeansSettings.getBeansHomeFolder() + "/plugins/files/");
				storageProvider.init(init);
			} catch (Throwable e) {
				LibsLogger.error(BeansStorageManager.class, "Cannot initilize local storage", e);
				storageProvider = null;
			}
		}
		return storageProvider;
	}
}
