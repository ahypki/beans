package net.beanscode.web.view.dataset;

import static org.rendersnake.HtmlAttributesFactory.class_;

import java.io.IOException;

import javax.ws.rs.core.SecurityContext;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.pojo.dataset.Dataset;
import net.beanscode.web.beta.BetaContent;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.BetaButtonGroup;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.pjf.op.OpDialogClose;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpSet;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.url.Params;

import org.rendersnake.HtmlCanvas;

public class DatasetBetaContent extends BetaContent {

	private Dataset dataset = null;
	
	public DatasetBetaContent() {
		
	}
	
	public DatasetBetaContent(BeansComponent parent) {
		super(parent);
	}
	
	public DatasetBetaContent(BeansComponent parent, Dataset dataset) {
		super(parent);
		setDataset(dataset);
	}
	
	@Override
	protected String getAjaxPath() {
		return DatasetPage.PATH + "/" + getDataset().getId();
	}
	
	@Override
	protected void renderHeader(HtmlCanvas html) throws IOException {
		html
			.h3()
				.i(class_("fa fa-folder-open"))
				._i()
				.span()
					.content(" " + getDataset().getName())
			._h3();
	}
		
	@Override
	protected void renderContent(HtmlCanvas html) throws IOException {
		html
			.render(new DatasetPanel(this, getDataset()));
	};
	
	public OpList editDataset(SecurityContext sc, Params params) throws IOException, ValidationException {
		final String datasetId = params.getString("datasetId");
		final String name = params.getString("name");
		
		new ApiCallCommand(this)
			.setApi("api/dataset/set")
			.addParam("id", datasetId)
			.addParam("name", name)
			.run();
		
		setDataset(new ApiCallCommand(this)
						.setApi("api/dataset")
						.addParam("dataset-id", datasetId)
						.run()
						.getResponse()
						.getAsObject(Dataset.class));
		
		LibsLogger.debug(DatasetBetaContent.class, "Dataset ", getDataset().getId(), " name changed to ", name);
		
		return new OpList()
			.add(new OpSet("#page-title", name)) // TODO
			.add(new OpDialogClose());
	}
	
	public OpList createDataset(SecurityContext sc, Params params) throws IOException, ValidationException {
		final String name = params.getString("name");
		
		final Dataset dataset = new ApiCallCommand(this)
										.setApi("api/dataset/create")
										.addParam("name", name)
										.run()
										.getResponse()
										.getAsObject(Dataset.class);
		
		return new OpList()
				.add(new DatasetBetaContent(this, dataset).onShow());
	}

	public Dataset getDataset() {
		if (dataset == null) {
			try {
				dataset = new ApiCallCommand(this)
					.setApi("api/dataset")
					.addParam("dataset-id", getParams().getString("datasetId"))
					.run()
					.getResponse()
					.getAsObject(Dataset.class);
			} catch (IOException e) {
				LibsLogger.error(DatasetBetaContent.class, "Cannot find dataset in DB", e);
			}
		}
		return dataset;
	}

	public void setDataset(Dataset dataset) {
		this.dataset = dataset;
	}

	@Override
	protected BetaButtonGroup getMenu() throws IOException {
		return null;
	}

	@Override
	protected String getMenuName() throws IOException {
		return null;
	}
}
