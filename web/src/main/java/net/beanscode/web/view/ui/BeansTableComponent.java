package net.beanscode.web.view.ui;

import java.io.IOException;

import javax.ws.rs.core.SecurityContext;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.users.User;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.components.TableComponent;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpSet;
import net.hypki.libs5.utils.url.Params;

import org.rendersnake.HtmlAttributesFactory;
import org.rendersnake.HtmlCanvas;

@Deprecated
public abstract class BeansTableComponent extends TableComponent {
	
	private User user = null;
	
	public BeansTableComponent() {
		
	}
	
	public BeansTableComponent(Component parent) {
		super(parent);
	}
	
	public BeansTableComponent(SecurityContext sc, Params params) {
		super(sc, params);
	}
	
	public String getUserEmail() throws CLIException, IOException {
		return getUser() != null ? getUser().getEmail() : null;
	}

	protected User getUser() throws CLIException, IOException {
		if (user == null) {
			user = new ApiCallCommand(getSessionBean())
						.setApi("api/user/get")
						.addParam("userId", getUserId().getId())
						.run()
						.getResponse()
						.getAsObject(User.class);
		}
		return user;
	}
	
	protected OpList onTotalRowsUpdate() {
		return new OpList()
			.add(new OpSet("." + getId() + "-total-rows", "Total rows " + (totalRows() == Long.MAX_VALUE ? "unknown" : totalRows())));
	}
	
	@Override
	protected OpList onRefresh() throws IOException {
		return super.onRefresh()
				.add(onTotalRowsUpdate());
	}

	@Override
	protected String getTableHtmlClasses() {
		return "table responsive-table users-panel-table table-hover beans-table";
	}
	
	@Override
	protected void renderBeforePager(HtmlCanvas html) throws IOException {
		super.renderBeforePager(html);
		
		html
			.span(HtmlAttributesFactory.class_(getId() + "-total-rows"))
				.content("Total rows " + (totalRows() == Long.MAX_VALUE ? "unknown" : totalRows()));
	}
}
