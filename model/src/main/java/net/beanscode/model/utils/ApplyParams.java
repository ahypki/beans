package net.beanscode.model.utils;

import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;

import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.DatasetFactory;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.dataset.TableFactory;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.Meta;
import net.hypki.libs5.utils.string.RegexUtils;

public class ApplyParams {

	public static String applyParams(final String s, final Row firstRow) {
		if (firstRow == null || nullOrEmpty(s))
			return s;
		
		String sOut = s;
		try {
			// replacing values
			for (int i = 0; i < 100; i++) {
				String varName = RegexUtils.firstGroup("\\$([\\w]+)", sOut);
				
				if (varName == null)
					break;
				
				Object varValue = firstRow != null ? firstRow.get(varName) : null;
				String varValueStr = varValue != null ? String.valueOf(varValue) : "";
				
				String newStr = RegexUtils.replaceFirstGroup("(\\$[\\w]+)", sOut, varValueStr);
				
				if (newStr.equals(sOut))
					break;
				
				sOut = newStr;
				
				if (i == 99)
					LibsLogger.warn(ApplyParams.class, "There might be an error in Plot.applyParams() because there were >= 100 variables");
			}
			
			// replacing built-in functions like: DSNAME, TBNAME, DSPARAM, TBID, TBID1
			for (int i = 0; i < 100; i++) {
				String v = RegexUtils.firstGroup("DSNAME\\(([\\w\\'\\\"]+)\\)", sOut);
				
				if (v == null)
					break;
				
				Table table = TableFactory.getTable(new UUID(v));
				Dataset ds = table != null ? DatasetFactory.getDataset(table.getDatasetId()) : null;
				String replacement = ds != null ? ds.getName() : "";
				
				String newStr = RegexUtils.replaceFirstGroup("(DSNAME\\([\\w\\'\\\"]+\\))", sOut, replacement);
				
				if (newStr.equals(sOut))
					break;
				
				sOut = newStr;
				
				if (i == 99)
					LibsLogger.warn(ApplyParams.class, "There might be an error in Plot.applyParams() because there were >= 100 variables");
			}
			
			// DSID
			for (int i = 0; i < 100; i++) {
				String v = RegexUtils.firstGroup("DSID\\(([\\w\\'\\\"]+)\\)", sOut);
				
				if (v == null)
					break;
				
				Table table = TableFactory.getTable(new UUID(v));
				Dataset ds = table != null ? DatasetFactory.getDataset(table.getDatasetId()) : null;
				String replacement = ds != null ? ds.getId().getId() : "";
				
				String newStr = RegexUtils.replaceFirstGroup("(DSID\\([\\w\\'\\\"]+\\))", sOut, replacement);
				
				if (newStr.equals(sOut))
					break;
				
				sOut = newStr;
				
				if (i == 99)
					LibsLogger.warn(ApplyParams.class, "There might be an error in Plot.applyParams() because there were >= 100 variables");
			}
			
			// TBNAME
			for (int i = 0; i < 100; i++) {
				String v = RegexUtils.firstGroup("TBNAME\\(([\\w\\'\\\"]+)\\)", sOut);
				
				if (v == null)
					break;
				
				Table table = TableFactory.getTable(new UUID(v));
				String replacement = table != null ? table.getName() : "";
				
				String newStr = RegexUtils.replaceFirstGroup("(TBNAME\\([\\w\\'\\\"]+\\))", sOut, replacement);
				
				if (newStr.equals(sOut))
					break;
				
				sOut = newStr;
				
				if (i == 99)
					LibsLogger.warn(ApplyParams.class, "There might be an error in Plot.applyParams() because there were >= 100 variables");
			}
			
			// TBID
			for (int i = 0; i < 100; i++) {
				String v = RegexUtils.firstGroup("TBID[1]*\\(([\\w\\'\\\"]+)\\)", sOut);
				
				if (v == null)
					break;
				
				Table table = TableFactory.getTable(new UUID(v));
				String replacementFull = table != null ? table.getId().toString() : "";
				String replacementShort = table != null ? table.getId().toString("-") : "";
				replacementShort = replacementShort.substring(0, replacementShort.indexOf('-'));
				
				String newStr = RegexUtils.replaceFirstGroup("(TBID\\([\\w\\'\\\"]+\\))", sOut, replacementFull);
//				if (newStr.equals(sOut))
//					break;

				sOut = newStr;
				
				newStr = RegexUtils.replaceFirstGroup("(TBID1\\([\\w\\'\\\"]+\\))", sOut, replacementShort);
				if (newStr.equals(sOut))
					break;
				
				sOut = newStr;
				
				if (i == 99)
					LibsLogger.warn(ApplyParams.class, "There might be an error in Plot.applyParams() because there were >= 100 variables");
			}
			
			// DSPARAM
			for (int i = 0; i < 100; i++) {
				String v = RegexUtils.firstGroup("DSPARAM\\(([\\w\\'\\\",\\$\\s]+)\\)", sOut);
				
				if (v == null)
					break;
				
				String [] params = v.split(",");
				String tbid = params[0].trim();
				String param = params[1].trim();
				if (param.startsWith("\""))
					param = param.substring(1);
				if (param.endsWith("\""))
					param = param.substring(0, param.length() - 1);
				
				Table table = TableFactory.getTable(new UUID(tbid));
				Dataset ds = table != null ? DatasetFactory.getDataset(table.getDatasetId()) : null;
				
				if (ds == null)
					break;
				
				Meta dsMeta = ds.getMeta().get(param.trim());
				String replacement = dsMeta != null ? dsMeta.getAsString() : "";
				
				String newStr = RegexUtils.replaceFirstGroup("(DSPARAM\\([\\w\\'\\\",\\$\\s]+\\))", sOut, replacement);
				
				if (newStr.equals(sOut))
					break;
				
				sOut = newStr;
				
				if (i == 99)
					LibsLogger.warn(ApplyParams.class, "There might be an error in Plot.applyParams() because there were >= 100 variables");
			}
			
			return sOut.replaceAll("\\\\\\$", "\\$");
		} catch (Exception e) {
			LibsLogger.error(ApplyParams.class, "Error while applying params to string in Plot", e);
			return sOut;
		}
	}

}
