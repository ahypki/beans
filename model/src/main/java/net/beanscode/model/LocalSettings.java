package net.beanscode.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import com.google.gson.JsonElement;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.reflection.SystemUtils;

public class LocalSettings {

	public static final String SETTINGS_DIR = SystemUtils.getHomePath() + "/.beans-software/";
	
	private static final String SETTINGS_FILENAME = ".beans-setting.json";
	private static final String SETTINGS_FILENAME_PATH = SETTINGS_DIR + SETTINGS_FILENAME;
	
	static {
		LibsLogger.debug(LocalSettings.class, "BEANS setting dir " + SETTINGS_DIR);
		LibsLogger.debug(LocalSettings.class, "BEANS setting filename " + SETTINGS_FILENAME);
		LibsLogger.debug(LocalSettings.class, "BEANS setting filename path " + SETTINGS_FILENAME_PATH);
		
		// creating folder it does not exist
		if (new File(SETTINGS_DIR).exists() == false) {
			new File(SETTINGS_DIR).mkdirs();
			LibsLogger.debug(LocalSettings.class, "Folder " + SETTINGS_DIR + " created");
		}
	}
	
	private static JsonElement getSettingsLocal() {
		try {
			return JsonUtils.readJsonFile(SETTINGS_FILENAME_PATH);
		} catch (FileNotFoundException e) {
			LibsLogger.error(LocalSettings.class, "Cannot read json settings file", e);
			return null;
		}
	}
	
	public static void saveSettingLocal(String jsonPath, Object value) throws IOException {
		JsonElement sett = getSettingsLocal();
		JsonUtils.putSetting(sett, jsonPath, value);
		JsonUtils.saveJson(sett, SETTINGS_FILENAME_PATH, true);
	}
	
	public static Boolean getSettingLocalAsBoolean(String jsonPath) {
		return JsonUtils.readBoolean(getSettingsLocal(), jsonPath);
	}
	
	public static String getSettingLocalAsString(String jsonPath) {
		return JsonUtils.readString(getSettingsLocal(), jsonPath);
	}
	
	public static String getSettingLocalAsString(String jsonPath, String defaultValue) {
		return JsonUtils.readString(getSettingsLocal(), jsonPath, defaultValue);
	}
	
	public static JsonElement getSettingLocalAsJson(String jsonPath) {
		return JsonUtils.readJsonElement(getSettingsLocal(), jsonPath);
	}
}


