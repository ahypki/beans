package net.beanscode.web.view.notebook;

import static net.beanscode.web.BeansWebConst.MAGIC_KEY;

import java.io.IOException;

import javax.annotation.security.PermitAll;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

import net.beanscode.web.BeansPage;
import net.beanscode.web.BetaPage;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.utils.url.Params;

@Path("/" + NotebookPage.PATH)
public class NotebookPage extends BeansPage {
	
	public final static String PATH = "notebook";
	
	public NotebookPage() {
		
	}

	@GET
	@Path("{notebookId: [a-zA-Z0-9]+}")
	@PermitAll
	@Produces(MediaType.TEXT_HTML)
    public String init(@Context SecurityContext sc, 
    		@Context UriInfo uriInfo,
    		@PathParam("notebookId") String notebookId,
			@QueryParam(MAGIC_KEY) UUID mk) throws IOException {
		String tmp = new BetaPage().createPage(sc);
		tmp = tmp.replace("<!--content-->", new OpComponentClick(NotebookContent.class, 
												UUID.random().getId(), 
												"onShow", 
												new Params()
													.add("notebookId", notebookId)
													.add(MAGIC_KEY, mk))
											.toStringAdhock());
		return tmp;
    }
}
