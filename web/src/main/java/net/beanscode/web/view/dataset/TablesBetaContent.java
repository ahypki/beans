package net.beanscode.web.view.dataset;

import java.io.IOException;

import net.beanscode.web.beta.BetaContent;
import net.beanscode.web.view.ui.BetaButtonGroup;
import net.hypki.libs5.pjf.components.Component;

import org.rendersnake.HtmlCanvas;


public class TablesBetaContent extends BetaContent {

	public TablesBetaContent() {
		
	}
	
	public TablesBetaContent(Component parent) {
		super(parent);
	}
	
	@Override
	protected void renderHeader(HtmlCanvas html) throws IOException {
		html
			.h1()
				.content("Tables");
	}

	@Override
	protected void renderContent(HtmlCanvas html) throws IOException {
		html
			.render(new TablesBetaTableComponent(this));
	}

	@Override
	protected BetaButtonGroup getMenu() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String getMenuName() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}
}
