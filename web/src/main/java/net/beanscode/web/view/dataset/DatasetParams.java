package net.beanscode.web.view.dataset;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;

import java.io.IOException;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.pojo.dataset.Dataset;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.EditableTable;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.buttons.ButtonSize;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.Meta;
import net.hypki.libs5.utils.string.RegexUtils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.url.Params;
import net.hypki.libs5.utils.utils.NumberUtils;

public class DatasetParams extends EditableTable {

	private Dataset dataset = null;
	
	public DatasetParams() {
		
	}
	
	public DatasetParams(BeansComponent parent) {
		super(parent);
		setAutosave(true);
	}
	
	public DatasetParams(BeansComponent parent, Dataset dataset) {
		super(parent);
		setDataset(dataset);
		setAutosave(true);
		
		setColumnNames(new String[] {"Key", "Value", "Description"});
		
		if (dataset.getMeta().size() > 0) {
			Object[][] values = new Object[dataset.getMeta().size()][3];
			int row = 0;
			for (Meta meta : dataset.getMeta()) {
				values[row][0] = meta.getName();
				values[row++][1] = meta.getValue();
			}
			setValues(values);
		}
		
		getHiddenParams()
			.add("datasetId", dataset.getId().getId())
			.add("userId", dataset.getUserId().getId());
	}
	
	@Override
	protected String getAdditionalCssClasses() {
		return "dataset-params";
	}
	
	@Override
	protected OpList getAdditionalSaveOp() {
		return new OpList()
			.add(new OpComponentClick(this, "saveTableClick", new Params().add("datasetId", getDataset().getId().getId())));
	}
	
	@Override
	protected void saveTable(Params params, String[][] newValues) throws IOException {
//		try {
			UUID datasetId = new UUID(getParams().getString("datasetId"));
			
			Dataset ds = new ApiCallCommand(this)
								.setApi("api/dataset")
								.addParam("dataset-id", datasetId.getId())
								.run()
								.getResponse()
								.getAsObject(Dataset.class);
			
			ds.getMeta().clear();
			for (int row = 0; row < newValues.length; row++) {
				String value = String.valueOf(newValues[row][1]);
				
				if (RegexUtils.isInt(value))
					ds.getMeta().add(String.valueOf(newValues[row][0]), NumberUtils.toInt(value));
				else if (RegexUtils.isDouble(value))
					ds.getMeta().add(String.valueOf(newValues[row][0]), NumberUtils.toDouble(value));
				else
					ds.getMeta().add(String.valueOf(newValues[row][0]), value);
			}
			
//			new DatasetMetaSet(getSessionBean()).run(ds.getId(), ds.getMeta());
			ApiCallCommand api = new ApiCallCommand(this)
				.setApi("api/dataset/meta/set")
				.addParam("datasetId", ds.getId().getId());
			
			for (Meta m : ds.getMeta()) {
				api
					.addParam("metaName", m.getName())
					.addParam("metaValue", m.getValue())
					.addParam("metaDesc", m.getDescription());
			}
			
			api
				.run();
//			ds.save();
//		} catch (ValidationException e) {
//			LibsLogger.error(DatasetParams.class, "Cannot save table", e);
//			throw new IOException(e.toString());
//		}
	}
	
	@Override
	protected String getSaveClass() {
		return getClass().getName();
	}
	
	@Override
	public ButtonSize getButtonSize() {
		return ButtonSize.EXTRA_SMALL;
	}

	public Dataset getDataset() {
		if (dataset == null) {
			try {
				String datasetId = getParams().getString("hidden-datasetId");
				if (StringUtilities.nullOrEmpty(datasetId))
					datasetId = getParams().getString("datasetId");
				
				if (notEmpty(datasetId))
					this.dataset = new ApiCallCommand(this)
								.setApi("api/dataset")
								.addParam("dataset-id", datasetId)
								.run()
								.getResponse()
								.getAsObject(Dataset.class);
			} catch (IOException e) {
				LibsLogger.error(DatasetParams.class, "Cannot initialize Dataset from params", e);
			}
		}
		return dataset;
	}

	public void setDataset(Dataset dataset) {
		this.dataset = dataset;
	}
}
