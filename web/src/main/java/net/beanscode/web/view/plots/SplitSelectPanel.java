package net.beanscode.web.view.plots;

import static net.hypki.libs5.utils.string.StringUtilities.wildcardMatches;
import static org.rendersnake.HtmlAttributesFactory.onClick;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.notebooks.PlotHistogramEntry;
import net.beanscode.web.view.ui.BetaTableComponent;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.op.Op;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.StringUtilities;

import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;


public class SplitSelectPanel extends BetaTableComponent {
	
	@Expose
	private UUID plotId = null;

	@Expose
	private OpList onClick = new OpList();
	
	private PlotHistogramEntry histogramCache = null;
	
	private List<String> splitsCache = null;
	
	private List<String> splitsFilteredCache = null;
	
	private HashMap<Integer, Integer> filteredTooriginal = new HashMap<Integer, Integer>();

	public SplitSelectPanel() {
		
	}
	
	public SplitSelectPanel(PlotHistogramEntry plot) {
		setPlotId(plot.getId());
	}
	
	@Override
	protected String getSettingsPrefix() {
		return "split-table-";
	}
	
	protected PlotHistogramEntry getPlot() {
		if (histogramCache == null) {
			try {
				histogramCache = new ApiCallCommand(this)
					.setApi("api/notebook/entry/get")
					.addParam("entryId", getPlotId().getId())
					.run()
					.getResponse()
					.getAsObject(PlotHistogramEntry.class);
			} catch (CLIException | IOException e) {
				LibsLogger.error(SplitSelectPanel.class, "Cannot get histogram from DB", e);
			}
		}
		return histogramCache;
	}
	
	protected List<String> getSplits() {
		if (splitsCache == null) {
			try {
				splitsCache = (List<String>) new ApiCallCommand(this)
					.setApi("api/notebook/entry/splits")
					.addParam("entryId", getPlotId().getId())
					.run()
					.getResponse()
					.getAsList("splits", String.class);
			} catch (CLIException | IOException e) {
				LibsLogger.error(SplitSelectPanel.class, "Cannot get histogram from DB", e);
				splitsCache = new ArrayList<String>();
			}
		}
		return splitsCache;
	}
	
	protected List<String> getFilteredSplits() {
		if (splitsFilteredCache == null) {
			boolean isFiltered = StringUtilities.notEmpty(getFilter());
			
			splitsFilteredCache = new ArrayList<String>();
			
			int splitCounter = 0;
			for (String split : getSplits()) {
				if (isFiltered == false
						|| (isFiltered && wildcardMatches(getFilter(), split))) {
					splitsFilteredCache.add(split);
					filteredTooriginal.put(splitsFilteredCache.size() - 1, splitCounter);
				}
				splitCounter++;
			}
		}
		return splitsFilteredCache;
	}
	
	@Override
	protected String getFilterPlaceholder() {
		return "Filter splits with wildcards";
	}
	
	
	@Override
	protected boolean isHeaderColumnVisible() throws IOException {
		return true;
	}
	
	@Override
	protected long getMaxHits() throws IOException {
		return getFilteredSplits().size();
	}
	
	@Override
	protected int getNumberOfColumns() throws IOException {
		return 1;
	}
	
	@Override
	protected void renderCell(int row, int col, HtmlCanvas html) throws IOException {
		for (Op op : onClick) {
			op
				.addParamParam("toShow", filteredTooriginal.get(row));// getFilteredSplits().get(row));
//				.set("toShow", getFilteredSplits().get(row));
		}
		
		html
			.span()
//				.td()
					.a(onClick(getOnClick().toStringOnclick()))
						.content(getFilteredSplits().get(row))
//				._td()
//			._tr()
			;
	}

	@Override
	protected boolean isFilterVisible() {
		return true;
	}

	@Override
	protected List<Button> getMenu() {
		return null;
	}

	public UUID getPlotId() {
		return plotId;
	}

	public void setPlotId(UUID plotId) {
		this.plotId = plotId;
	}

	public OpList getOnClick() {
		return onClick;
	}

	public void setOnClick(OpList onClick) {
		this.onClick = onClick;
	}

	@Override
	protected void renderTitle(HtmlCanvas html) throws IOException {
		html
			.write("Selecting Plot split");
	}

	@Override
	protected String getColumnName(int col) throws IOException {
		return "Split ID";
	}

	@Override
	protected boolean isExapandable() throws IOException {
		return false;
	}

	@Override
	protected void renderExpandable(int row, HtmlCanvas html) throws IOException {
		
	}

	@Override
	protected String getTableCss() throws IOException {
		return null;
	}
}
