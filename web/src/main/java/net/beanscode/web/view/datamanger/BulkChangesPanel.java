package net.beanscode.web.view.datamanger;

import static net.hypki.libs5.utils.string.Base64Utils.encode;
import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.id;

import java.io.IOException;

import net.beanscode.cli.BeansCliCommand;
import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.pojo.bulkchange.BulkChangesJobs;
import net.beanscode.web.view.dataset.DatasetBetaTableComponent;
import net.beanscode.web.view.dataset.TablesBetaTableComponent;
import net.beanscode.web.view.notebook.NotebooksTableComponent;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.CardPanel;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.beanscode.web.view.ui.modals.OpModalQuestion;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.pjf.components.ComboComponent;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.buttons.ButtonGroup;
import net.hypki.libs5.pjf.components.buttons.ButtonSize;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpHide;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpSet;
import net.hypki.libs5.pjf.op.OpSetVal;
import net.hypki.libs5.pjf.op.OpShow;
import net.hypki.libs5.utils.string.Base64Utils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.url.Params;

import org.apache.commons.lang.NotImplementedException;
import org.rendersnake.HtmlCanvas;

public class BulkChangesPanel extends CardPanel {
	
	public BulkChangesPanel() {
		
	}
	
	public BulkChangesPanel(BeansComponent initializingComponent) {
		super(initializingComponent);
	}
	
	@Override
	protected void renderTitle(HtmlCanvas html) throws IOException {
		html
			.span()
				.content("Bulk change");
	}
	
	@Override
	protected void renderFooter(HtmlCanvas html) throws IOException {
		html
			.render(new ButtonGroup()
							.setButtonSize(ButtonSize.NORMAL)
							.addButton(new Button("Start bulk change")
									.add(new OpModalQuestion("Bulk change", 
											"Do you really want to start this bulk change?<br/><br/>"
													+ "WARNING: This action cannot be reverted!",
											new OpComponentClick(this, "onStart")))));
	}
	
	@Override
	protected void renderContent(HtmlCanvas html) throws IOException {
		DatasetBetaTableComponent datasets = new DatasetBetaTableComponent(this);
		
		html
//			.div(id(getId())
//					.class_("bulk-change"))
//				.h2()
//					.content("Bulk change ")
				.div(class_("bulk-change-for")
						.id(getId()))
					.b()
						.content("For the following")
						
					.input(class_("beans-bulk-current-table-id")
							.name("beans-bulk-current-table-id")
							.type("hidden")
							.value(datasets.getId()))
							
					.input(class_("beans-bulk-current-table-type")
							.name("beans-bulk-current-table-type")
							.type("hidden")
							.value("datasets"))
						
					.render(new ComboComponent("")
						.setValues("Datasets", "Tables", "Notebooks")
						.setSelectedValue("Datasets")
						.addOp("Datasets", new OpComponentClick(this, "onDatasets"))
						.addOp("Datasets", new OpComponentClick(this, 
								"onTypeChange", 
								new Params().add("beans-bulk-new-table-type", "datasets")))
						.addOp("Tables", new OpComponentClick(this, "onTables"))
						.addOp("Tables", new OpComponentClick(this, 
								"onTypeChange", 
								new Params().add("beans-bulk-new-table-type", "tables")))
						.addOp("Notebooks", new OpComponentClick(this, "onNotebooks"))
						.addOp("Notebooks", new OpComponentClick(this, 
								"onTypeChange", 
								new Params().add("beans-bulk-new-table-type", "notebooks")))
						)
						
					.div(class_("beans-bulk-select"))
						.render(datasets)
					._div()
//					.render(new GeneralSearchPanel(this)
//							.setOptionsVisible(false)
//							.setShowListOnEmptyFilters(false)
//							.addOpOnChangeType(new OpComponentClick(this, "onTypeChange"))
//							)
				._div()
				.div(class_("bulk-change-do"))
					.b()
						.content("do the following operations:")
					.br()
					.render(new ComboComponent("bulk-change-operation-datasets")
						.setAdditionalCssClass("bulk-change-operation bulk-change-operation-datasets")
						.setValues("Delete", "Clear", "Reindex datasets", "Rename", "Add meta", "Remove meta")
						.setSelectedValue("Reindex datasets")
						.addOp("Delete", new OpComponentClick(BulkChangesPanel.this, "onDeleteDatasetsCombo"))
						.addOp("Clear", new OpComponentClick(BulkChangesPanel.this, "onClearDatasetsCombo"))
						.addOp("Reindex datasets", new OpComponentClick(BulkChangesPanel.this, "onReindexDatasetsCombo"))
						.addOp("Rename", new OpComponentClick(BulkChangesPanel.this, "onRenameDatasetsCombo"))
						.addOp("Add meta", new OpComponentClick(BulkChangesPanel.this, "onAddMeta"))
						.addOp("Remove meta", new OpComponentClick(BulkChangesPanel.this, "onRemoveMeta"))
						)
					.render(new ComboComponent("bulk-change-operation-tables")
						.setAdditionalCssClass("bulk-change-operation bulk-change-operation-tables")
						.setValues("Delete", "Clear", "Reindex tables", "Add meta", "Remove meta")
						.setSelectedValue("Reindex tables")
						.setVisible(false)
						.addOp("Delete", new OpComponentClick(BulkChangesPanel.this, "onDeleteTablesCombo"))
						.addOp("Clear", new OpComponentClick(BulkChangesPanel.this, "onClearTablesCombo"))
						.addOp("Reindex tables", new OpComponentClick(BulkChangesPanel.this, "onReindexTablesCombo"))
						.addOp("Add meta", new OpComponentClick(BulkChangesPanel.this, "onAddMeta"))
						.addOp("Remove meta", new OpComponentClick(BulkChangesPanel.this, "onRemoveMeta"))
						)
					.render(new ComboComponent("bulk-change-operation-notebooks")
						.setAdditionalCssClass("bulk-change-operation bulk-change-operation-notebooks")
						.setValues("Update all fields", "Delete", "Add meta", "Remove meta", "Update auto-reload option")
						.setSelectedValue("Update all fields")
						.setVisible(false)
						.addOp("Update all fields", new OpComponentClick(BulkChangesPanel.this, "onUpdateNotebooksCombo"))
						.addOp("Delete", new OpComponentClick(BulkChangesPanel.this, "onUpdateNotebooksCombo"))
						.addOp("Add meta", new OpComponentClick(BulkChangesPanel.this, "onAddMeta"))
						.addOp("Remove meta", new OpComponentClick(BulkChangesPanel.this, "onRemoveMeta"))
						.addOp("Update auto-reload option", new OpComponentClick(BulkChangesPanel.this, "onAutoReload"))
						)
					.input(id("ds-rename")
							.name("ds-rename")
							.class_("ds-rename additional-inputs form-control")
							.add("style", "display: none;")
							.add("placeholder", "New dataset name"))
						
					.input(id("meta-add-name")
							.name("meta-add-name")
							.class_("meta-add-name meta-edit additional-inputs form-control")
							.add("style", "display: none;")
							.add("placeholder", "Meta name"))
					.span(class_("meta-add-value meta-edit additional-inputs")
							.add("style", "display: none;"))
						.content(" = ")
					.input(id("meta-add-value")
							.name("meta-add-value")
							.class_("meta-add-value meta-edit additional-inputs form-control")
							.add("style", "display: none;")
							.add("placeholder", "Meta value"))

					.render(new ComboComponent("bulk-change-autoreload-options")
						.setAdditionalCssClass("bulk-change-operation bulk-change-operation-notebooks-autoupdate")
						.setValues("Ignore", "Ignore notebook", "Update without confirmation", "Ask for confirmation before update")
						.setSelectedValue("Ask for confirmation before update")
						.setVisible(false)
						)
//					.br()
				._div()
			;
	};
	
	private OpList onDatasets() throws IOException {
		DatasetBetaTableComponent datasets = new DatasetBetaTableComponent(this);
		
		return new OpList()
			.add(new OpSet("#" + getId() + " .beans-bulk-select", datasets.toHtml()))
			.add(new OpSetVal("#" + getId() + " .beans-bulk-current-table-id", datasets.getId()))
			.add(new OpSetVal("#" + getId() + " .beans-bulk-current-table-type", "datasets"))
			;
	}
	
	private OpList onTables() throws IOException {
		TablesBetaTableComponent tables = new TablesBetaTableComponent(this);
		
		return new OpList()
			.add(new OpSet("#" + getId() + " .beans-bulk-select", tables.toHtml()))
			.add(new OpSetVal("#" + getId() + " .beans-bulk-current-table-id", tables.getId()))
			.add(new OpSetVal("#" + getId() + " .beans-bulk-current-table-type", "tables"));
	}
	
	private OpList onNotebooks() throws IOException {
		NotebooksTableComponent notebooks = new NotebooksTableComponent(this);
		
		return new OpList()
			.add(new OpSet("#" + getId() + " .beans-bulk-select", notebooks.toHtml()))
			.add(new OpSetVal("#" + getId() + " .beans-bulk-current-table-id", notebooks.getId()))
			.add(new OpSetVal("#" + getId() + " .beans-bulk-current-table-type", "notebooks"))
			;
	}
	
	private BulkChangesJobs getBulkChangesJobs() {
		final String searchType = getParams().getString("beans-bulk-current-table-type");
		
		if (searchType.equalsIgnoreCase("Datasets")) {
			String bulkCh = getParams().getString("bulk-change-operation-datasets");
			
			if (bulkCh.equalsIgnoreCase("Delete"))
				return BulkChangesJobs.DATASET_DELETE;
			else if (bulkCh.equalsIgnoreCase("Clear"))
				return BulkChangesJobs.DATASET_CLEAR;
			else if (bulkCh.equalsIgnoreCase("Reindex datasets"))
				return BulkChangesJobs.DATASET_REINDEX;
			else if (bulkCh.equalsIgnoreCase("Rename"))
				return BulkChangesJobs.DATASET_RENAME;
			else if (bulkCh.equalsIgnoreCase("Add meta"))
				return BulkChangesJobs.DATASET_META_ADD;
			else if (bulkCh.equalsIgnoreCase("Remove meta"))
				return BulkChangesJobs.DATASET_META_REMOVE;
			else
				throw new NotImplementedException();
			
		} else if (searchType.equalsIgnoreCase("Tables")) {
			String bulkCh = getParams().getString("bulk-change-operation-tables");
			
			if (bulkCh.equalsIgnoreCase("Delete"))
				return BulkChangesJobs.TABLE_DELETE;
			else if (bulkCh.equalsIgnoreCase("Clear"))
				return BulkChangesJobs.TABLE_CLEAR;
			else if (bulkCh.equalsIgnoreCase("Reindex tables"))
				return BulkChangesJobs.TABLE_REINDEX;
			else if (bulkCh.equalsIgnoreCase("Add meta"))
				return BulkChangesJobs.TABLE_META_ADD;
			else if (bulkCh.equalsIgnoreCase("Remove meta"))
				return BulkChangesJobs.TABLE_META_REMOVE;
			else
				throw new NotImplementedException();
			
		} else if (searchType.equalsIgnoreCase("Notebooks")) {
			String bulkCh = getParams().getString("bulk-change-operation-notebooks");
			
			if (bulkCh.equalsIgnoreCase("Update all fields"))
				return BulkChangesJobs.NOTEBOOK_UPDATE;
			else if (bulkCh.equalsIgnoreCase("Delete"))
				return BulkChangesJobs.NOTEBOOK_DELETE;
			else if (bulkCh.equalsIgnoreCase("Add meta"))
				return BulkChangesJobs.NOTEBOOK_META_ADD;
			else if (bulkCh.equalsIgnoreCase("Remove meta"))
				return BulkChangesJobs.NOTEBOOK_META_REMOVE;
			else if (bulkCh.equalsIgnoreCase("Update auto-reload option"))
				return BulkChangesJobs.NOTEBOOK_AUTOUPDATE;
			else
				throw new NotImplementedException();
			
		}
		
		return null;
	}
	
	private OpList onStart() throws ValidationException, IOException {
		final String searchType = getParams().getString("beans-bulk-current-table-type");
		final String searchId = getParams().getString("beans-bulk-current-table-id");
		final String search1 = getParams().getString("beans-table-filter-" + searchId);
		final String search2 = getParams().getString("beans-table-filter2-" + searchId);
		final String metaName = getParams().getString("meta-add-name", null);
		final String metaValue = getParams().getString("meta-add-value", "");
		final String dsRename = getParams().getString("ds-rename", null);
		final String autoUpdate = getParams().getString("bulk-change-autoreload-options", null);
		
		String keyValue = StringUtilities.notEmpty(metaName) ? metaName + "=" + metaValue : null;
		
		if (getBulkChangesJobs() == BulkChangesJobs.DATASET_RENAME)
			keyValue = "ds-rename=" + dsRename;
		
//		new BulkChange(getSessionBean())
//			.run(searchType, 
//					search1, 
//					search2, 
//					search1, 
//					getBulkChangesJobs().toString(),
//					keyValue);
		
		new ApiCallCommand(this)
			.setApi(BeansCliCommand.API_BULKCHANGE)
			.addParam("searchType", searchType)
			.addParam("searchDatasets", search1)
			.addParam("searchTables", search2)
			.addParam("searchNotebooks", search1)
			.addParam("job", getBulkChangesJobs().toString())
			.addParam("autoUpdate", encode(autoUpdate))
			.addParam(metaName, encode(metaValue))
			.run();
		
		return new OpList()
				.add(new OpBeansInfo("Bulk job added to the queue successfully!"));
	}
		
	private OpList onTypeChange() {
		final String searchType = getParams().getString("beans-bulk-new-table-type");
		OpList ops = new OpList()
			.add(new OpHide("#" + getId() + " .bulk-change-operation"));
		
		if (searchType.equalsIgnoreCase("Datasets")) {
			ops.add(new OpShow("#" + getId() + " .bulk-change-operation-datasets"));
		} else if (searchType.equalsIgnoreCase("Tables")) {
			ops.add(new OpShow("#" + getId() + " .bulk-change-operation-tables"));
		} else if (searchType.equalsIgnoreCase("Notebooks")) {
			ops.add(new OpShow("#" + getId() + " .bulk-change-operation-notebooks"));
		}
		
		return ops;
	}
	
	private OpList onAddMeta() {
		return new OpList()
			.add(new OpShow(".meta-edit"));
	}
	
	private OpList onRemoveMeta() {
		return new OpList()
			.add(new OpShow(".meta-add-name"))
			.add(new OpHide(".meta-add-value"));
	}
	
	private OpList onAutoReload() {
		return new OpList()
			.add(new OpHide(".meta-add-name"))
			.add(new OpHide(".meta-add-value"))
			.add(new OpShow(".bulk-change-operation-notebooks-autoupdate"));
	}
	
	private OpList onDeleteDatasetsCombo() {
		return new OpList()
			.add(new OpHide(".meta-edit"));
	}
	
	private OpList onClearDatasetsCombo() {
		return new OpList()
			.add(new OpHide(".meta-edit"));
	}

	private OpList onReindexDatasetsCombo() {
		return new OpList()
			.add(new OpHide(".meta-edit"));
	}
	
	private OpList onRenameDatasetsCombo() {
		return new OpList()
			.add(new OpHide(".meta-edit"))
			.add(new OpShow(".ds-rename"));
	}
	
	private OpList onDeleteTablesCombo() {
		return new OpList()
			.add(new OpHide(".meta-edit"));
	}
	
	private OpList onClearTablesCombo() {
		return new OpList()
			.add(new OpHide(".meta-edit"));
	}

	private OpList onReindexTablesCombo() {
		return new OpList()
			.add(new OpHide(".meta-edit"));
	}
	
	private OpList onUpdateNotebooksCombo() {
		return new OpList()
			.add(new OpHide(".meta-edit"));
	}
}
