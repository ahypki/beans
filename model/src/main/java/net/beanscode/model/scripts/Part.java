package net.beanscode.model.scripts;

import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class Part {

	@Expose
	@NotNull
	@NotEmpty
	private Language language = Language.PIG;
	
	@Expose
	@NotNull
	@NotEmpty
	private String script = null;
	
	public Part() {
		
	}
	
	public Part(Language lng, String script) {
		setLanguage(lng);
		setScript(script);
	}
	
	public Part(Language lng) {
		setLanguage(lng);
	}
	
	@Override
	public String toString() {
		return String.format("Language: %s\n%s%s", getLanguage(), getScript() != null ? getScript().substring(0, Math.min(getScript().length(), 100)) : "", getScript().length()< 100 ? "" : "...\n");
	}

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public String getScript() {
		return script;
	}

	public void setScript(String script) {
		this.script = script;
	}

	public void append(String line) {
		setScript(String.format("%s%s\n", getScript() != null ? getScript() : "", line)); 
	}
}
