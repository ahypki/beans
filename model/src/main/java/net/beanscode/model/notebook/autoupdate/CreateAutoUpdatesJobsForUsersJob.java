package net.beanscode.model.notebook.autoupdate;

import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.IOException;

import org.quartz.JobDetail;
import org.quartz.SchedulerException;

import com.google.gson.annotations.Expose;

import net.beanscode.model.BeansSettings;
import net.beanscode.model.backup.AutoBackupJob;
import net.beanscode.model.utils.JvmUtils;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.DateUtils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.url.Params;
import net.hypki.libs5.utils.utils.AssertUtils;
import net.hypki.libs5.utils.utils.NumberUtils;
import net.hypki.libs5.weblibs.ScheduleManager;
import net.hypki.libs5.weblibs.jobs.Job;
import net.hypki.libs5.weblibs.user.User;
import net.hypki.libs5.weblibs.user.UserFactory;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

public class CreateAutoUpdatesJobsForUsersJob extends Job {
	@Expose
	@NotNull
	@AssertValid
	private UUID jvm = JvmUtils.getThisJvmUUID();

	public CreateAutoUpdatesJobsForUsersJob() {
		
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		if (jvm == null 
				|| JvmUtils.isThisJvm(jvm) == false) {
			LibsLogger.debug(CreateAutoUpdatesJobsForUsersJob.class, "This ", getClass().getSimpleName(), " comes from previous"
					+ " JVM, closing this job..");
			return;
		}
		
		for (User user : UserFactory.iterateUsers()) {
			try {
				reloadScheduler(user.getUserId());
			} catch (SchedulerException e) {
				LibsLogger.error(CreateAutoUpdatesJobsForUsersJob.class, "Cannot create auto-update Quartz job for user " + user.getUserId(), e);
			}
//			ScheduleManager.scheduleRecursively(AutoUpdateScheduler.class,
////					1,
//					(int) DateUtils.ONE_DAY_MINUTES,
//					new Params().add("userId", user.getUserId().getId()));
			
			LibsLogger.debug(CreateAutoUpdatesJobsForUsersJob.class, "Created Scheduler Job for user ", user.getUserId(), " to perform auto-updates");
		}
	}
	
	public static JobDetail getReloadScheduler(UUID userId) throws IOException, SchedulerException {
		for (JobDetail jobDetail : ScheduleManager.iterateJobs()) {
			if (jobDetail.getJobClass().equals(AutoUpdateScheduler.class)) {
				final String userIdFromJob = jobDetail.getJobDataMap().getString("userId");
				
				if (userIdFromJob.equals(userId.getId())) {
					return jobDetail;
				}
			}
		}
		return null;
	}
	
	public static void reloadScheduler(UUID userId) throws IOException, SchedulerException {
		JobDetail tmp = getReloadScheduler(userId);
		
		if (tmp != null)
			ScheduleManager.stop(tmp.getKey());
		
		createScheduler(userId);
	}

	private static void createScheduler(UUID userId) throws IOException {
		String[] interval = StringUtilities.split(BeansSettings.getAutoUpdateIntervalMinutes(userId), ":");
		
		int everyMinutes = NumberUtils.toInt(interval[0], 0);
		if (interval[1].equals("Minutes")) {
			// do nothing
		} else if (interval[1].equals("Hours"))
			everyMinutes *= 60;
		else if (interval[1].equals("Days"))
			everyMinutes *= 24 * 60;
		
		assertTrue(everyMinutes > 0, "Cannot reload auto-updates job with wrong everyMinutes variable");
		
		ScheduleManager.scheduleRecursively(AutoUpdateScheduler.class,
				everyMinutes,
				new Params().add("userId", userId.getId()));
	}
}
