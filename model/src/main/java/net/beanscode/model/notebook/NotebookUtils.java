package net.beanscode.model.notebook;

import java.io.IOException;
import java.io.StringWriter;

import com.google.gson.JsonElement;

import net.beanscode.model.backup.BackupManager;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.reflection.ReflectionUtility;
import net.hypki.libs5.utils.string.Meta;

public class NotebookUtils {

//	public static String exportToSimpleScript(final Notebook notebook) {
//		StringBuilder sb = new StringBuilder();
//		
//		for (Meta meta : notebook.getMeta()) {
//			sb.append("---Meta ");
//			sb.append(meta.getName());
//			sb.append("=");
//			sb.append(meta.getAsString());
//			sb.append("\n");
//		}
//		sb.append("\n");
//		
//		for (NotebookEntry entry : notebook.iterateEntries()) {
//			
//			if (entry instanceof TextEntry) {
//				
//				sb.append("---Text\n");
//				sb.append(entry.getAs(TextEntry.class).getText().trim());
//				
//			} else if (entry instanceof PigScript) {
//				
//				PigScript pig = entry.getAs(PigScript.class);
//				sb.append("---Pig");
//				sb.append(": ");
//				sb.append(pig.getName());
//				sb.append("\n");
//				sb.append(pig.getQuery().trim());
//				
//			} else if (entry instanceof Plot) {
//				
//				sb.append("---Plot\n");
//				sb.append(entry.getAs(Plot.class).getText().trim());
//				
//			} else
//				LibsLogger.error(NotebookUtils.class, "Unimplemented ", NotebookEntry.class.getSimpleName(), " in export method");
//			
//			sb.append("\n\n\n");
//		}
//		
//		
//		return sb.toString();
//	}
	
	public static String exportToJson(final Notebook notebook) throws IOException {
		StringWriter sb = new StringWriter();
		
		BackupManager.backupNotebooks(sb, notebook.getId());
		
		return sb.toString();
	}

//	public static void importSimpleScriptToNotebook(Notebook notebook, String simpleScript) throws ValidationException, IOException {
//		if (nullOrEmpty(simpleScript, notebook)) {
//			LibsLogger.error(NotebookUtils.class, "Cannot import empty(?) script to empty(?) notebook");
//			return;
//		}
//		
//		for (String entry : simpleScript.split("---")) {
//			if (nullOrEmpty(entry))
//				continue;
//			
//			entry = entry.trim();
//			
//			NotebookEntry ne = null;
//			
//			if (entry.startsWith("Text")) {
//				
//				entry = entry.substring(entry.indexOf('\n') + 1);
//				ne = new TextEntry(notebook, entry);
//				
//			} else if (entry.startsWith("Plot")) {
//				
//				entry = entry.substring(entry.indexOf('\n') + 1);
//				ne = new Plot(notebook, entry);
//				
//			} else if (entry.startsWith("Pig")) {
//				
//				final int newLineIdx = entry.indexOf('\n');
//				String pigName = entry.substring("Pig:".length(), newLineIdx);
//				entry = entry.substring(newLineIdx + 1);
//				
//				ne = new PigScript(notebook, "pig script");
//				if (notEmpty(pigName))
//					ne.setName(pigName.trim());
//				((PigScript) ne).setQuery(entry);
//				
//			} else if (entry.startsWith("Meta")) {
//				
//				entry = entry.substring("Meta".length()).trim();// entry.indexOf('\n') + 1);
//				int eqIdx = entry.indexOf('=');
//				String key = entry.substring(0, eqIdx).trim();
//				String value = entry.substring(eqIdx + 1).trim();
//				notebook.getMeta().add(key, value);
//				
//			} else
//				
//				throw new NotImplementedException();
//			
//			if (ne != null)
//				notebook.addEntryId(ne.getId());
//			
//			notebook.save();
//			
//			if (ne != null)
//				ne.save();
//		}
//	}
	
	public static void importJsonToNotebook(Notebook notebook, String jsonScript) throws ValidationException, IOException {
		JsonElement jeScript = JsonUtils.parseJson(jsonScript);
		
		for (JsonElement je : jeScript.getAsJsonArray()) {
			try {
				String type = je.getAsJsonObject().get("type").getAsString();
				String data = je.toString();
				
				if (type.endsWith(Meta.class.getSimpleName())) {
					notebook.getMeta().add(JsonUtils.fromJson(je, Meta.class));
				} else if (type.endsWith(Notebook.class.getSimpleName())) {
					// do nothing
				} else {
					Class neClass = Class.forName(type);
					if (ReflectionUtility.isSubclassOf(neClass, NotebookEntry.class)) {
						NotebookEntry ne = (NotebookEntry) JsonUtils.fromJson(data, neClass);
						
						ne.setId(UUID.random());
						ne.setCreate(SimpleDate.now());
						ne.setUserId(notebook.getUserId());
						ne.setNotebookId(notebook.getId());
						
						notebook.addEntryId(ne.getId());
						notebook.save();
						
						ne.save();
					}
				}
			} catch (Exception e) {
				LibsLogger.error(NotebookUtils.class, "Cannot import entry " + je, e);
			}
		}
	}
}
