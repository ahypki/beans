package net.beanscode.web.view.summary;

import static org.rendersnake.HtmlAttributesFactory.href;

import java.io.IOException;

import net.beanscode.web.BeansWebConst;
import net.beanscode.web.view.help.HelpBetaContent;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.CardPanel;
import net.beanscode.web.view.ui.Link;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;

import org.rendersnake.HtmlCanvas;

public class NewsPanel extends CardPanel {

	public NewsPanel() {
		
	}
	
	public NewsPanel(BeansComponent parent) {
		super(parent);
	}
	
	@Override
	protected void renderTitle(HtmlCanvas html) throws IOException {
		html
			.span()
				.content("News");
	}
	
	@Override
	protected void renderFooter(HtmlCanvas html) throws IOException {
		
	}
	
	@Override
	protected void renderContent(HtmlCanvas html) throws IOException {
		html
			.p()
				.write("Please cite ")
				.render(new Link("Hypki 2018", "https://academic.oup.com/mnras/advance-article-abstract/doi/10.1093/mnras/sty803/4956532?redirectedFrom=fulltext"))
				.write(" if you are using BEANS for research.")
			._p()
			.p()
				.write("Go to ")
				.render(new Link("help page", new OpComponentClick(HelpBetaContent.class, "onShow")))
				.write(" if you wish to create demo data, plots and queries.")
			._p()
			.render(new Link("BEANS home page", "https://beanscode.net"))
			.span()
				.content(" &#9679; ", false)
			.render(new Link("Source code", "https://gitlab.com/ahypki/beans/"))
			.span()
				.content(" &#9679; ", false)
			.render(new Link("Report an issue", "https://gitlab.com/ahypki/beans/-/issues"))
			.span()
				.content(" &#9679; ", false)
			.render(new Link("Tutorials", "https://beanscode.net/tutorials/"))
			.span()
				.content(" &#9679; ", false)
			.render(new Link("Current version " + BeansWebConst.VERSION, 
					"https://gitlab.com/ahypki/beans/-/blob/master/CHANGELOG.md"))
			;
	}
}
