package net.beanscode.rest.jobs;

import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import mjson.Json;
import net.beanscode.model.BeansConst;
import net.beanscode.model.BeansSettings;
import net.beanscode.model.notebook.autoupdate.AutoUpdateJob;
import net.beanscode.model.rights.Rights;
import net.beanscode.model.settings.Clipboard;
import net.beanscode.rest.BeansRest;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.DateUtils;
import net.hypki.libs5.utils.url.HttpResponse;
import net.hypki.libs5.weblibs.CacheManager;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.jobs.Job;
import net.hypki.libs5.weblibs.jobs.JobFactory;
import net.hypki.libs5.weblibs.jobs.JobsManager;
import net.hypki.libs5.weblibs.jobs.JobsWorker;

@Path("api/jobs")
public class JobsRest extends BeansRest {
	
	@GET
    @Path("count")
	@RolesAllowed(Rights.ADMIN)
    @Produces(MediaType.APPLICATION_JSON)
	public Response count(@Context SecurityContext sc) throws IOException, ValidationException {
		validateUnauthorized(isAdmin(sc), "Permission denied");
		
		return Response
				.ok()
				.entity(Json
						.object()
						.set("count", DbObject.getDatabaseProvider().countRows(WeblibsConst.KEYSPACE, Job.COLUMN_FAMILY, null))
						.toString())
				.build();
	}
	
	@DELETE
    @Path("remove")
	@RolesAllowed(Rights.ADMIN)
	public void remove(@Context SecurityContext sc,
			@QueryParam("jobId") UUID jobId,
			@QueryParam("channel") String channel,
			@QueryParam("creationDate") long creationDate) throws IOException, ValidationException {
		validateUnauthorized(isAdmin(sc), "Permission denied");
		
		Job job = JobFactory.getJobData(channel, creationDate, jobId);
		
		if (job == null) {
			// in the worst case going over the list and looking for jobId
			for (Job j : JobFactory.getOldestJobs(channel, 500, DateUtils.YEAR_2008)) {
				if (j.getId().equals(jobId)) {
					job = j;
					break;
				}
			}
		}
		
		if (job != null) {
			job.remove();
			
			JobsWorker.markAsDone(job);
						
			LibsLogger.debug(JobsRest.class, "Job ", job.getId(), " removed");
		}
	}
	
	@POST
    @Path("clearall")
	@RolesAllowed(Rights.ADMIN)
	public void clearAllJobs(@Context SecurityContext sc) throws IOException, ValidationException {
		validateUnauthorized(isAdmin(sc), "Permission denied");
		
		DbObject
			.getDatabaseProvider()
			.clearTable(WeblibsConst.KEYSPACE, Job.COLUMN_FAMILY);
	}
	
	@POST
    @Path("runall")
	@RolesAllowed(Rights.ADMIN)
	public void runAllJobs(@Context SecurityContext sc) throws IOException, ValidationException {
		validateUnauthorized(isAdmin(sc), "Permission denied");
		
		JobsWorker.checkForAllNewJobs();
	}
	
	@POST
    @Path("workers/start")
	@RolesAllowed(Rights.ADMIN)
	public void workerStart(@Context SecurityContext sc,
			@FormParam("channel") String channelName) throws IOException, ValidationException {
		validateUnauthorized(isAdmin(sc), "Permission denied");
		
		JobsManager.init(channelName);
	}
	
	@DELETE
    @Path("workers/stop")
	@RolesAllowed(Rights.ADMIN)
	public void workerStart(@Context SecurityContext sc,
			@QueryParam("id") long id) throws IOException, ValidationException {
		validateUnauthorized(isAdmin(sc), "Permission denied");
		
		JobsManager.stop(id);
	}
	
	@GET
    @Path("workers/list")
	@RolesAllowed(Rights.ADMIN)
    @Produces(MediaType.APPLICATION_JSON)
	public Response workersList(@Context SecurityContext sc) throws IOException, ValidationException {
		validateUnauthorized(isAdmin(sc), "Permission denied");
		
		List<JobsWorker> workers = JobsManager.getJobsWorkersThreads();
		
		Collections.sort(workers, new Comparator<JobsWorker>() {
			@Override
			public int compare(JobsWorker o1, JobsWorker o2) {
				return ((Long)o1.getId()).compareTo(o2.getId());
			}
		});
		
		Json all = Json.array();
		
		for (JobsWorker jobsWorker : workers) {
			Json j = Json.object();
			j.set("channel", jobsWorker.getChannel());
			j.set("id", jobsWorker.getId());
			j.set("currentJobName", jobsWorker.getCurrentJobName());
			j.set("currentJobStart", jobsWorker.getCurrentJobStartTime() != null ? jobsWorker.getCurrentJobStartTime().getTimeInMillis() : 0L);
			all.add(j);
		}
		
		return Response
				.ok()
				.entity(all.toString())
				.build();
	}
	
	@GET
    @Path("list")
	@RolesAllowed(Rights.ADMIN)
    @Produces(MediaType.APPLICATION_JSON)
	public Response list(@Context SecurityContext sc,
			@QueryParam("channel") String channel,
			@QueryParam("count") int count) throws IOException, ValidationException {
		validateUnauthorized(isAdmin(sc), "Permission denied");
		
		count = count <= 0 ? 50 : count;
		count = count > 500 ? 500 : count;
		Json all = Json.array();
		
		for (Job job : JobFactory.getOldestJobs(channel, count, DateUtils.YEAR_2008)) {
			Json j = Json.object();
			j.set("channel", job.getChannel());
			j.set("time", job.getCreationMs());
			Json id = Json.object();
			id.set("id", job.getId().getId());
			j.set("id", id);
			j.set("name", job.getClazz());
			j.set("description", job.getDescription());
			all.add(j);
		}
		
		return Response
				.ok()
				.entity(all.toString())
				.build();
	}

	@GET
    @Path("channels")
	@RolesAllowed(Rights.ADMIN)
    @Produces(MediaType.APPLICATION_JSON)
	public Response get(@Context SecurityContext sc) throws IOException, ValidationException {	
		validateUnauthorized(isAdmin(sc), "Permission denied");
				
		Set<String> done = new HashSet<String>();
		mjson.Json json = Json.array();
		
		for (String channelName : BeansConst.getAllChannelsNames()) {
			if (done.contains(channelName))
				continue;
			
			json.add(channelName);
			done.add(channelName);
		}
		
		return Response
				.ok()
				.entity(json.toString())
				.build();
	}
}
