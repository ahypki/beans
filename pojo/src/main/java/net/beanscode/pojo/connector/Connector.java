package net.beanscode.pojo.connector;

import com.google.gson.annotations.Expose;

public class Connector {

	@Expose
	private String name = null;
	
	public Connector() {
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
