package net.beanscode.model.plugins;

import java.awt.List;
import java.util.ArrayList;

import net.hypki.libs5.utils.json.JsonUtils;

public class ColumnDefList extends ArrayList<ColumnDef> {

	public ColumnDefList() {
		
	}
	
	public ColumnDefList addColumnDef(ColumnDef colDef) {
		add(colDef);
		return this; 
	}
	
	public ColumnDefList cloneThis() {
		return JsonUtils.fromJson(JsonUtils.objectToString(this), ColumnDefList.class);
	}
	
	public void removeColumn(String column) {
		int i = -1;
		for (ColumnDef def : this) {
			i++;
			if (def.getName().equals(column))
				break;
		}
		if (i >= 0)
			remove(i);
	}

	public ColumnDef getColumn(String column) {
		for (ColumnDef def : this) {
			if (def.getName().equals(column))
				return def;
		}
		return null;
	}
	
	public ColumnDef getColumnWhichEndsWith(String column) {
		if (column == null)
			return null;
		
		for (ColumnDef def : this) {
			if (def.getName().endsWith(column))
				return def;
		}
		return null;
	}

	public int getColumnIndex(String column) {
		int i = -1;
		for (ColumnDef def : this) {
			i++;
			if (def.getName().equals(column))
				return i;
		}
		return -1;
	}

	public ColumnDefList addPrefix(String prefix) {
		for (ColumnDef col : this) {
			col.setName(prefix + col.getName());
		}
		return this;
	}
}
