package net.beanscode.web.view.ui.modals;

import java.io.IOException;

import net.hypki.libs5.pjf.op.OpAppend;
import net.hypki.libs5.pjf.op.OpJs;
import net.hypki.libs5.pjf.op.OpList;

public class OpModal extends OpList {
	
	private ModalWindow modal = null;
	
	public OpModal(ModalWindow modalWindow) throws IOException {
		this.modal = modalWindow;
		
		add(new OpAppend(".beans-modal-windows", modal.toHtml()));
		add(new OpJs("$('#beans-modal-" + modal.getId() + "').modal('toggle');"));
	}
}
