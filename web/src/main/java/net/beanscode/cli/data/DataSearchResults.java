package net.beanscode.cli.data;

import java.util.List;

import net.hypki.libs5.db.db.Row;

import com.google.gson.annotations.Expose;

public class DataSearchResults {

	@Expose
	private List<Row> rows = null;
	
	@Expose
	private long maxHits = 0;
	
	@Expose
	private String state = null;
	
	public DataSearchResults() {
		
	}

	public List<Row> getRows() {
		return rows;
	}

	public DataSearchResults setRows(List<Row> rows) {
		this.rows = rows;
		return this;
	}

	public long getMaxHits() {
		return maxHits;
	}

	public DataSearchResults setMaxHits(long maxHits) {
		this.maxHits = maxHits;
		return this;
	}

	public String getState() {
		return state;
	}

	public DataSearchResults setState(String state) {
		this.state = state;
		return this;
	}
}
