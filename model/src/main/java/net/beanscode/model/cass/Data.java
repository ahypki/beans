package net.beanscode.model.cass;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import net.beanscode.model.dataset.Table;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.schema.TableSchema;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.collections.CollectionUtils;
import net.hypki.libs5.utils.collections.UniqueList;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.weblibs.WeblibsConst;

import org.apache.commons.lang.NotImplementedException;

@Deprecated
public class Data implements Iterable<Row> {
	
	public static final String COLUMN_FAMILY = "data";
	
	public static final String COLUMN_ROW = "row";
	
	private static final int MAX_CACHED_ROWS_COUNT = 200000;
	
	private String keyspace = null;

	private List<String> dataIds = null;
	 
	private List<Row> rowsCached = null;
	
	private boolean allRowsSkipCache = false;
	private boolean allRowsInCache = false;
	private boolean allRowsTriedCached = false;
		
	private C3Where filter = null;
	
	private boolean defaultIfColumnIsMissing = false;
		
	public Data() {

	}
	
	public Data(String dataId) {
		getDataIds().add(dataId);
	}
	
	public Data(List<String> dataIds) {
		getDataIds().addAll(dataIds);
	}
	
	public Data(Table table) {
		getDataIds().add(table.getId().getId());
	}
	
	public int size() {
		return getRowsCached().size();
	}
	
	@Override
	public String toString() {
		return String.format("DataIds=%s filter=%s", CollectionUtils.toString(getDataIds(), ","), getFilter());
	}
	
	/**
	 * Caution: it iterates over all rows of this table (not the cached ones!)
	 * @param column
	 * @return
	 */
	public List<Object> getUniqueValues(String column) {
		if (StringUtilities.nullOrEmpty(column))
			return null;
		
		List<Object> uniqueValues = new UniqueList<>();
		for (Row row : this) {// DataFactory.iterateRows(getKeyspace(), getDataId(), getFilter(), isDefaultIfColumnIsMissing())) {
			Object o = row.get(column);
			if (o == null)
				o = row.get(column);
			uniqueValues.add(o);
		}
		return uniqueValues;
	}
		
	private List<Row> getRowsCached() {
		if (rowsCached == null)
			rowsCached = new ArrayList<>();
		return rowsCached;
	}

	public void setRowsCached(List<Row> rows) {
		this.rowsCached = rows;
	}

	public void clear() {
		getRowsCached().clear();
	}

	/**
	 * Caution: It works only for cached rows!
	 * @param columnName
	 */
	public void sort(final String columnName) {
		if (rowsCached == null || rowsCached.size() == 0)
			return;
		
		Collections.sort(rowsCached, new Comparator<Row>() {
			@Override
			public int compare(Row o1, Row o2) {
				Object v1 = o1.get(columnName);
				Object v2 = o2.get(columnName);
				if (v1 instanceof Double)
					return Double.compare((Double) v1, (Double) v2);
				else if (v1 instanceof Float)
					return Float.compare((Float) v1, (Float) v2);
				else if (v1 instanceof Long)
					return Long.compare((Long) v1, (Long) v2);
				else if (v1 instanceof Integer)
					return Integer.compare((Integer) v1, (Integer) v2);
				else
					return 0;
			}
		});
	}
	
	public TableSchema getSchema() {
		TableSchema tableSchema = new TableSchema(COLUMN_FAMILY.toLowerCase())
				.addColumn(Table.COLUMN_TBID, 	ColumnType.STRING, true)
				.addColumn(DbObject.COLUMN_PK, 	ColumnType.STRING, true)
				.addColumn(Data.COLUMN_ROW, 	ColumnType.LONG, false, true)
				.addColumn(DbObject.COLUMN_DATA,ColumnType.BLOB)
				;
		return tableSchema;
	}
	
	/**
	 * Used for bulk importing
	 * @return
	 */
	public String getDataInsertSTMT() {
		return String.format("INSERT INTO %s.%s (%s, %s, %s, %s) VALUES (?, ?, ?, ?);", WeblibsConst.KEYSPACE, COLUMN_FAMILY,
				DbObject.COLUMN_PK, Table.COLUMN_TBID, COLUMN_ROW, DbObject.COLUMN_DATA);
	}
	
	public Row getFirstRow() {
		if (this.rowsCached == null || this.rowsCached.size() == 0)
			return null;
		return this.rowsCached.get(0);
	}

	public List<String> getColumnsList() {
		Row row = getFirstRow();
		if (row == null)
			return new ArrayList<>();
		
		List<String> columnNames = new ArrayList<>();
		columnNames.addAll(row.getColumns().keySet());
		return columnNames;
	}

	public C3Where getFilter() {
		return filter;
	}

	public void setFilter(C3Where filter) {
		this.filter = filter;
	}

	public String getKeyspace() {
		return keyspace;
	}

	public Data setKeyspace(String keyspace) {
		this.keyspace = keyspace;
		return this;
	}

	public boolean isDefaultIfColumnIsMissing() {
		return defaultIfColumnIsMissing;
	}

	public void setDefaultIfColumnIsMissing(boolean defaultIfColumnIsMissing) {
		this.defaultIfColumnIsMissing = defaultIfColumnIsMissing;
	}
	
	public Iterable<Row> filter(final C3Where additionalFilter) {
		return filter(additionalFilter, false);
	}
	
	public Iterable<Row> filter(final C3Where additionalFilter, final boolean defaultIfColumnIsMissing) {
		return new Iterable<Row>() {
			@Override
			public Iterator<Row> iterator() {
				return new Iterator<Row>() {
					Iterator<Row> allRowsIter = Data.this.iterator();
					Row r = null;
					
					@Override
					public void remove() {
						throw new NotImplementedException();
					}
					
					@Override
					public Row next() {
						if (r == null)
							hasNext();
						return r;
					}
					
					@Override
					public boolean hasNext() {
						while (allRowsIter.hasNext()) {
							Row row = allRowsIter.next();
							if (additionalFilter == null || row.isFullfiled(additionalFilter, defaultIfColumnIsMissing)) {
								r = row;
								return true;
							}
						}
						return false;
					}
				};
			}
		};
	}

	@Override
	public Iterator<Row> iterator() {
		if (!allRowsSkipCache) {
			// first trying to read everything to cache
			if (allRowsTriedCached == false) {
				allRowsTriedCached = true;
				allRowsInCache = true;
				for (Row row : new MultipleDataIdIterator(getDataIds(), getFilter(), isDefaultIfColumnIsMissing())) {
					getRowsCached().add(row);
					if (getRowsCached().size() == MAX_CACHED_ROWS_COUNT) {
						LibsLogger.debug(Data.class, "Data ", CollectionUtils.toString(getDataIds(), ","), " too big for cache, using iterators");
						getRowsCached().clear();
						allRowsInCache = false;
						break;
					}
				}
			}
		}
		
		if (allRowsInCache)
			return getRowsCached().iterator();
		
		return new MultipleDataIdIterator(getDataIds(), getFilter(), isDefaultIfColumnIsMissing());
	}

	public List<String> getDataIds() {
		if (dataIds == null)
			dataIds = new ArrayList<>();
		return dataIds;
	}

	public void setDataIds(List<String> dataIds) {
		this.dataIds = dataIds;
	}

	public boolean isAllRowsSkipCache() {
		return allRowsSkipCache;
	}

	public void setAllRowsSkipCache(boolean allRowsSkipCache) {
		this.allRowsSkipCache = allRowsSkipCache;
	}
}
