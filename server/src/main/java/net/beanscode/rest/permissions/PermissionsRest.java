package net.beanscode.rest.permissions;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import net.beanscode.model.BeansDTNObject;
import net.beanscode.model.rights.Permission;
import net.beanscode.model.rights.PermissionFactory;
import net.beanscode.model.rights.PermissionRecursiveJob;
import net.beanscode.rest.BeansRest;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.url.HttpResponse;

@Path("api/permission")
public class PermissionsRest extends BeansRest {
	
	@GET
    @Path("get")
    @Produces(MediaType.APPLICATION_JSON)
	public Response getPerms(@Context SecurityContext sc,
			@QueryParam("class") String permissionClass,
			@QueryParam("id") UUID permissionId) throws IOException, ValidationException {
		Permission perm = PermissionFactory.getPermission(permissionClass, permissionId);
		
		if (perm == null) {
			perm = new Permission();
			perm.setId(permissionId);
			perm.setClazz(permissionClass);
			perm.save();
		} 

		validateNotFound(perm != null, "Permission is not found");
		
		BeansDTNObject dtn = perm.getBeansDTNObject();
		
		validateUnauthorized(isAdmin(sc) || dtn.getUserId().equals(getUserId(sc)), "Permission denied");
		
		return Response
				.ok()
				.entity(perm.getData())
				.build();
	}

	@POST
    @Path("set")
	public void setPerms(@Context SecurityContext sc,
			@FormParam("id") UUID permissionId,
			@FormParam("class") String permissionClass,
			@FormParam("readGroups") List<UUID> readGroups,
			@FormParam("writeGroups") List<UUID> writeGroups,
			@FormParam("recursive") boolean recursive) throws IOException, ValidationException {
		Permission perm = PermissionFactory.getPermission(permissionClass, permissionId);
		BeansDTNObject dtn = null;
		
		if (perm == null) {
			perm = new Permission();
			perm.setId(permissionId);
			perm.setClazz(permissionClass);
		} 
		
		dtn = perm.getBeansDTNObject();

		validateNotFound(perm != null, "Permission is not found");
		validateNotFound(dtn != null, "Dataset/Table/Notebook for this Permission is not found");
		validateUnauthorized(isAdmin(sc) || dtn.getUserId().equals(getUserId(sc)), "Permission denied");
		
		perm.clearReadPermissions();
		perm.clearWritePermissions();
		
		for (UUID groupId : readGroups) {
			perm.setReadAllowed(groupId, true);
		}
		
		for (UUID groupId : writeGroups) {
			perm.setWriteAllowed(groupId, true);
		}
		
		perm.save();
		
		if (recursive)
			new PermissionRecursiveJob(perm).save();
	}
}
