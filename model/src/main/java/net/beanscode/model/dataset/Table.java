package net.beanscode.model.dataset;

import static net.hypki.libs5.utils.string.StringUtilities.padLeft;
import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.NotImplementedException;

import com.google.gson.JsonElement;
import com.google.gson.annotations.Expose;

import net.beanscode.model.BeansDTNObject;
import net.beanscode.model.BeansSettings;
import net.beanscode.model.autostart.IncreaseFieldsCountJob;
import net.beanscode.model.cass.Data;
import net.beanscode.model.cass.DataFactory;
import net.beanscode.model.connectors.BeansConnector;
import net.beanscode.model.connectors.BeansConnectorFactory;
import net.beanscode.model.connectors.BeansConnectorStatus;
import net.beanscode.model.connectors.ConnectorList;
import net.beanscode.model.connectors.H5Connector;
import net.beanscode.model.connectors.PlainConnector;
import net.beanscode.model.plugins.ColumnDef;
import net.beanscode.model.plugins.ColumnDefList;
import net.beanscode.model.plugins.Connector;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.ResultsIter;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.schema.TableSchema;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.db.db.weblibs.Mutation;
import net.hypki.libs5.db.db.weblibs.MutationList;
import net.hypki.libs5.db.db.weblibs.MutationType;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.db.lucene.LuceneDatabaseProvider;
import net.hypki.libs5.search.SearchResults;
import net.hypki.libs5.search.query.Query;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.date.Watch;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.string.ByteUtils;
import net.hypki.libs5.utils.string.MetaList;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.weblibs.CacheManager;
import net.hypki.libs5.weblibs.SearchManager;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.mail.MailJob;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

public class Table extends BeansDTNObject<Table> {

	public static final String COLUMN_FAMILY = "beanstable";
	
	public static final String COLUMN_DSID = "dsid";
	public static final String COLUMN_TBID = "tbid";
	
	private static boolean nrOfFieldsIncreased = false;
		
	@Expose
	@NotNull
	@AssertValid
	private UUID datasetId = null;
		
	@Expose
	@NotNull
	private TableStatus tableStatus = TableStatus.IMPORTING;
	
	@Expose
	@NotNull
	@AssertValid
	private SimpleDate create = null;
	
	@Expose
	@NotNull
	private TableSearchStatus searchStatus = TableSearchStatus.NOT_INDEXED;
	
	@Expose
	@NotNull
	@NotEmpty
	private String name = null;
	
	@Expose
	@AssertValid
	private MetaList meta = null;
	
	private ColumnDefList columns = null;
	private boolean triedInitializeFromConn = false;
	
	private final String TABLE_COLUMNS_CACHE = "TABLE_COLUMNS_CACHE";
	
	public Table() {
		setId(UUID.random());
		setCreate(SimpleDate.now());
		setTableStatus(TableStatus.HEALTHY);
	}
	
	public Table(UUID userId, UUID datasetId, String name) {
		setId(UUID.random());
		setUserId(userId);
		setDatasetId(datasetId);
		setCreate(SimpleDate.now());
		setName(name);
		setTableStatus(TableStatus.HEALTHY);
	}
	
	public Table(Dataset ds, String name) {
		setId(UUID.random());
		setUserId(ds.getUserId());
		setDatasetId(ds.getId());
		setCreate(SimpleDate.now());
		setName(name);
		setTableStatus(TableStatus.HEALTHY);
	}
	
	@Override
	public String getColumnFamily() {
		return COLUMN_FAMILY;
	}
	
	@Override
	public String getKey() {
		return getId().getId();
	}
	
	@Override
	public TableSchema getTableSchema() {
		return super.getTableSchema()
				.addColumn(COLUMN_DSID, ColumnType.STRING, false, true);
	}
	
	@Override
	public void additionalValidation() throws ValidationException {
		super.additionalValidation();
		
		assertTrue(isTableNameValid(getName()), "Table name is invalid");
		
		HashSet<String> colUnique = new HashSet<>();
		if (this.columns != null)
			for (ColumnDef colDef : getColumnDefs()) {
				assertTrue(!colUnique.contains(colDef.getName()), "Column ", colDef.getName(), " is duplicated in the Table class");
				colUnique.add(colDef.getName());
			}
	}
	
	public long getAproxBytesSize() throws IOException, ValidationException {
		// TODO OPTY cache
		long bytes = 0;
		for (Connector conn : iterateConnectors()) {
			bytes += conn.getAproxBytesSize();
		}
		return bytes;
	}
	
	public Iterable<BeansConnector> iterateBeansConnectors() {
		return BeansConnectorFactory.iterateConnectors(getId());
	}
	
	public Iterable<Connector> iterateConnectors() {
		return new Iterable<Connector>() {
			@Override
			public Iterator<Connector> iterator() {
				return new Iterator<Connector>() {
					
					private Iterator<BeansConnector> beansConnIter = iterateBeansConnectors().iterator();
					
					@Override
					public Connector next() {
						BeansConnector beansConn = beansConnIter.next();
						return beansConn.getConnector();
					}
					
					@Override
					public boolean hasNext() {
						return beansConnIter.hasNext();
					}
				};
			}
		};
	}
	
	public ConnectorList getConnectorList() {
		ConnectorList connList = new ConnectorList();
		for (Connector conn : iterateConnectors()) {
			connList.addConnector(conn);
		}
		return connList;
	}
	
	public Iterable<Row> iterateRows(Query filter) {
		return getConnectorList().iterateRows(filter);
	}
	
	/**
	 * Removing non-commited BeansConnectors. This has to be called after every Pig script execution, 
	 * because there might be some Tasks with connectors which actually failed, and the data from such 
	 * connectors must be removed in order to avoid duplicated rows.
	 * 
	 * @throws ValidationException
	 * @throws IOException
	 */
	public void clearNonCommittedConnectors() throws ValidationException, IOException {
		for (BeansConnector beansConnector : iterateBeansConnectors()) {
			if (beansConnector.getStatus() == BeansConnectorStatus.NEW) {
				beansConnector.remove();
				LibsLogger.info(Table.class, "BeansConnector is not ", BeansConnectorStatus.COMMITTED, 
						" removing it", beansConnector.getConnectorLink().getConnectorInitParams());
			} else if (beansConnector.getStatus() == BeansConnectorStatus.COMMITTED) {
				// do nothing
			} else
				throw new NotImplementedException("BeansConnector status " + beansConnector.getStatus() + 
						" not implemented");
		}
		
	}
	
	public Table optimize(boolean inBackground) throws ValidationException, IOException {
		if (inBackground) {
			new TableOptimizeJob(this).save();
			return this;
		}
		
		// validate whether optimization is needed
		if (getNrOfConnectors() < 10) {
			LibsLogger.debug(Table.class, "Nr of connectors is < 10, skipping optimization");
			return this;
		}
		
		try {
			Connector out = BeansSettings.getDefaultConnector(getId(), UUID.random().getId());
			
			out.setColumnDefs(getColumnDefs());
		
			long sourceNrRows = 0;
			for (Row row : this.iterateRows(null)) {
				out.write(row);
				sourceNrRows++;
			}
			
			out.close();

			removeConnectors();
			
			BeansConnector beansConn = new BeansConnector(out, UUID.random().getId());
			beansConn.save();
			
			save();
			
			long newNrRows = 0;
			if (true) {
				for (Row row : this.iterateRows(null)) {
					newNrRows++;
				}
			}
			
			LibsLogger.debug(Table.class, "Table ", getId(), "optimized successfully (", sourceNrRows, "->", newNrRows);
		} catch (Exception e) {
			LibsLogger.error(Table.class, "Cannot optimize Table " + getId(), e);
		}
		return this;
	}
	
	public long validate(boolean inBackground, boolean sendEmail) throws IOException, ValidationException {
		if (inBackground) {
			new TableValidationJob(this)
				.save();
			return 0;
		}
		
		StringBuilder summary = new StringBuilder();
		StringBuilder problems = new StringBuilder();
		int validationProblems = 0;
		
		// checking connectors' rows
		ConnectorList validatedConnectorList = new ConnectorList();
		List<Connector> toClear = new ArrayList<>();
		
		int allConnectors = 0;
		for (Connector c : iterateConnectors()) {
			allConnectors++;
			
			try {
				c.validate();
				c.close();
				
				validatedConnectorList.addConnector(c);
			} catch (Throwable t) {
				LibsLogger.error(Table.class, "Connector " + c + " corrupted", t);
				
				toClear.add(c);
				
				validationProblems++;
				problems.append("Connector " + c + " is broken, removing it..");
				problems.append("\n");
			}
		}
		
		if (allConnectors != validatedConnectorList.getConnectorsLinks().size()) {
			LibsLogger.info(Table.class, "Connectors list validated from all ", allConnectors, " to ", 
					validatedConnectorList.getConnectorsLinks().size(), " correct ones");
			
			for (Connector connector : toClear) {
				connector.clear();
			}
		}
		
		// counting rows
		long rowsCount = 0;
		for (Row row : iterateRows(null)) {
			rowsCount++;
		}
		
		if (sendEmail) {
			summary.append("Table ID: " + getId() + "\n");
			summary.append("Table name: " + getName() + "\n");
			summary.append("Dataset name: " + getDataset().getName() + "\n");
			summary.append("Dataset id: " + getDatasetId() + "\n");
			summary.append("Rows count: " + rowsCount + "\n");
			summary.append("\n");
			
			if (validationProblems > 0) {
				summary.append("Problems:\n");
				summary.append(problems.toString());
			}

//			Mailer.sendMail(BeansSettings.getMailCredentials(), 
//					getUser().getEmail().toString(), 
//					null, 
//					null, 
//					"[BEANS] Table " + getName() + " validation report", 
//					summary.toString(),
//					false, 
//					null);
			
			new MailJob(getUser().getEmail(), 
					"[BEANS] Table " + getName() + " validation report", 
					summary.toString())
				.setMailCredentials(BeansSettings.getMailCredentials())
				.save();
		}
		
		return rowsCount;
	}
	
	public void makeReadOnly() throws ValidationException, IOException {
		// TODO bring back permissions
//		new Allowed(getUserId(), Rights.DATASET_WRITE_ENABLED + getId(), false).save();
//		new Allowed(getUserId(), Rights.DATASET_DELETE_ENABLED + getId(), false).save();
	}
	
	/**
	 * Valid name contains any characters except new lines characters.
	 * @param tableName
	 * @return
	 */
	public static boolean isTableNameValid(String tableName) {
//		return tableName.matches("^[\\w\\_\\-\\(\\)\\:\\.\\,\\s\\=]+$");
		return StringUtilities.notEmpty(tableName) 
				&& tableName.contains("\r") == false
				&& tableName.contains("\n") == false;
	}
	
	@Override
	public MutationList getSaveMutations() {
		TableFactory.clearCache(getId());
		
		return new MutationList()
			.addMutation(new Mutation(MutationType.INSERT, WeblibsConst.KEYSPACE, COLUMN_FAMILY, getKey(), DbObject.COLUMN_PK, COLUMN_DSID, getDatasetId().getId()))
			.addMutation(super.getSaveMutations())
			.addInsertMutations(new ReindexTable(this))
			.addInsertMutations(nrOfFieldsIncreased == false ? new IncreaseFieldsCountJob() : null)
			;
	}
	
	@Override
	public Table save() throws ValidationException, IOException {
		getMeta().fixD();
		
		super.save();
		
		TableFactory.updateCache(this);
		
		try {
			index();
		} catch (Exception e) {
			LibsLogger.error(Dataset.class, "Cannot index dataset ad-hoc in save() method", e);
		}
		
		nrOfFieldsIncreased = true;
		
		return this;
	}
	
	@Override
	public void index() throws IOException {
		Dataset ds = getDataset();
		
		ds.getMeta().fixD();
		getMeta().fixD();
				
		// adding Dataset data to look for Tables easier
		JsonElement thisJson = JsonUtils.toJson(this);
		thisJson.getAsJsonObject().add("dataset", JsonUtils.toJson(ds));
		
		SearchManager.searchInstance().indexDocument(WeblibsConst.KEYSPACE_LOWERCASE, 
				getColumnFamily().toLowerCase(),
				this.getCombinedKey().toLowerCase(),
				thisJson.toString());
		
		indexPermissions();
		
		CacheManager
			.cacheInstance()
			.remove("tbIdToDsName", getId().getId());
//		LibsLogger.debug(Table.class, "Table indexed in ", w.toString());
	}
	
	@Override
	public void deindex() throws IOException {
		// removing table from search index
		SearchManager.removeObject(getCombinedKey().toLowerCase(), getColumnFamily());
	}
	
	public Dataset getDataset() throws IOException {
		return DatasetFactory.getDataset(getDatasetId());
	}

	public void indexData(boolean inBackground) throws ValidationException, IOException {
		if (inBackground) {
			new TableIndexDataJob(getId()).save();
			return;
		}
		
		setSearchStatus(TableSearchStatus.INDEXING);
		save();
		
		try {
			final Watch w = new Watch();
			final Watch wOnlySearch = new Watch();
			final int perBulk = 20000;
			
			wOnlySearch.pause();
			
			long rowsIndexed = 0;
			long bytesIndexed = 0;
			String key = null;
			String val = null;
			Map<String, String> idData = new HashMap<String, String>(); 
			for (Row row : iterateRows(null)) {
				row.addColumn("tbid", getId().getId());
				
				key = DbObject.buildCombinedKey(WeblibsConst.KEYSPACE_LOWERCASE, Data.COLUMN_FAMILY.toLowerCase(), getId().getId(), row.getPk().toString());
				val = row.toJson();
				
				idData.put(key, val);
				
				bytesIndexed += key.length() * 2;
				bytesIndexed += val.length() * 2;
				
				if (idData.size() == perBulk) {
					wOnlySearch.unpause();
					SearchManager.searchInstance().indexDocument(WeblibsConst.KEYSPACE_LOWERCASE, 
							Data.COLUMN_FAMILY.toLowerCase(),
							idData);
					wOnlySearch.pause();
					
					idData.clear();
					
					LibsLogger.debug(Table.class, rowsIndexed, " rows of Data for table ", getId(), 
							" indexed in ", w.toString(), " (", wOnlySearch.toString(), " indexing), ", 
							((bytesIndexed / 1000000.0) / w.sec()), " [MB/s]");
				}
				
				rowsIndexed++;
			}
			
			if (idData.size() > 0) {
				wOnlySearch.unpause();
				SearchManager.searchInstance().indexDocument(WeblibsConst.KEYSPACE_LOWERCASE, 
						Data.COLUMN_FAMILY.toLowerCase(),
						idData);
				wOnlySearch.pause();
			}
			
			LibsLogger.debug(Table.class, "Finished! ", rowsIndexed, " rows of Data for table ", getId(), 
					" indexed in ", w.toString(), " (", wOnlySearch.toString(), " indexing), ", 
					((bytesIndexed / 1000000.0) / w.sec()), " [MB/s]");
			
			setSearchStatus(TableSearchStatus.INDEXED);
			save();
		} catch (Throwable t) {
			LibsLogger.error(Table.class, "Cannot index table", t);
			
			setSearchStatus(TableSearchStatus.NOT_INDEXED);
			save();
		}
	}

	public void move(Dataset newParentDataset) throws ValidationException, IOException {
//		ArrayList<Mutation> muts = new ArrayList<>();
		
//		muts.add(new Mutation(MutationType.REMOVE, WeblibsConst.KEYSPACE, COLUMN_FAMILY, getKey(), COLUMN_DSID, getDatasetId().getId()));
		
		setDatasetId(newParentDataset.getId());
		
//		muts.addAll(this.getSaveMutations());
//		
//		save(muts);
		save();
	}
	
	@Override
	public MutationList getRemoveMutations() {
		return super.getRemoveMutations()
				.addInsertMutations(new RemoveTableFromIndexJob(this))
//				.addInsertMutations(new RemoveDataJob(getId().getId()))
//				.addRemoveMutations(new Mutation(MutationType.REMOVE, WeblibsConst.KEYSPACE, COLUMN_FAMILY, 
//						getKey(), DbObject.COLUMN_PK, COLUMN_DSID, getDatasetId().getId()))
				.addInsertMutations(new TableConnectorsClear(this));
	}
	
	@Override
	public void remove() throws ValidationException, IOException {		
		super.remove();
		
		try {
			deindex();
		} catch (Exception e) {
			LibsLogger.error(Table.class, "Cannot adhoc remove Table from search index", e);
		}
		
		try {
			TableFactory.clearCache(getId());
		} catch (Throwable t) {
			LibsLogger.error(Table.class, "Cannot remove ad-hoc Table from cache", t);
		}
		
		LibsLogger.debug(Table.class, "Table ", getId(), " removed, with connectors ", getConnectorList());
	}

//	public UUID getUserId() {
//		return userId;
//	}
//
//	public Table setUserId(UUID userId) {
//		this.userId = userId;
//		return this;
//	}

	public UUID getDatasetId() {
		return datasetId;
	}

	public Table setDatasetId(UUID datasetId) {
		this.datasetId = datasetId;
		return this;
	}

	public TableStatus getTableStatus() {
		return tableStatus;
	}
	
	public void setTableStatus(TableStatus tableStatus) {
		this.tableStatus = tableStatus;
	}

	public String getName() {
		return name;
	}

	public Table setName(String name) {
		this.name = name;
		return this;
	}
	
	public void changeName(String newName) throws ValidationException, IOException {
		setName(newName);
		
		save();
	}

	public SimpleDate getCreate() {
		return create;
	}

	public void setCreate(SimpleDate create) {
		this.create = create;
	}
	
	public List<String> getColumnNames() {
		List<String> columnNames = new ArrayList<>();
		for (ColumnDef colDef : getColumnDefs()) {
			columnNames.add(colDef.getName());
		}
		return columnNames;
	}
	
	public boolean containColumn(String name) {
		if (this.columns != null) {
			for (ColumnDef columnDef : columns) {
				if (columnDef.getName().equals(name))
					return true;
			}
		}
		return false;
	}

	public ColumnDefList getColumnDefs() {
		if (columns == null
				|| (columns == null && triedInitializeFromConn == false)) {
			try {
				triedInitializeFromConn = true;
				
				// reading from cache
				columns = CacheManager
					.cacheInstance()
					.get(ColumnDefList.class, TABLE_COLUMNS_CACHE, "columndefs" + getId().getId());
				if (columns != null)
					return columns;
				
				columns = getConnectorList().getColumnDefs();
				
				CacheManager
					.cacheInstance()
					.put(TABLE_COLUMNS_CACHE, "columndefs" + getId().getId(), columns);
			} catch (Throwable e) {
				LibsLogger.error(Table.class, "Cannot read ColumnDefs for table " + getId(), e);
			}
		}
		return columns;
	}

//	public void setColumnDefs(ColumnDefList columns) {
//		this.columns = columns;
//	}
	
	/**
	 * Underlying data in Connector could change and thus the definitions of the columns. This method is reading the columns definitions 
	 * from the file and updating this cache.
	 */
//	public void updateColumnDefs() {
//		if (this.columns == null)
//			setColumnDefs(getConnectorList().getColumnDefs());
//	}
	
//	public void setColumnType(String columnName, ColumnType colType) {
//		getColumnDef(columnName).setType(colType);
//	}
	
	public ColumnType getColumnType(String columnName) {
		return getColumnDef(columnName).getType();
	}
	
	public ColumnDef getColumnDef(String columnName) {
		for (ColumnDef columnDef : getColumnDefs()) {
			if (columnDef.getName().equals(columnName))
				return columnDef;
		}
		return null;
	}
	
//	public Table importFile(String filepath, InputStream file) throws IOException, ValidationException {
//		return importFile(filepath, file, getConnectorList().getConnectorsLinks().get(0).getConnectorInstance());
//	}

//	public Table importFile(String filepath, InputStream file, Connector writer) throws IOException, ValidationException {
//	}
	
	public Table importFile(String file) throws IOException, ValidationException {
		return importFile(new File(file));
	}
	
	public Table importFile(File file) throws IOException, ValidationException {
//		return importFile(file, getConnectorList().getConnectorsLinks().get(0).getConnectorInstance());
		PlainConnector sourcePlainConnector = new PlainConnector(file);
		
		H5Connector writer = new H5Connector(getId(), new File(BeansSettings.getConnectorDefaultFolder().getAbsolutePath() 
				+ "/" + getId() + ".h5"));
		
//		Connector writer = getConnectorList().getConnectorsLinks().get(0).getConnectorInstance();
		
		writer.setColumnDefs(sourcePlainConnector.getColumnDefs());
		LibsLogger.debug(Table.class, "Importing file ", file, " to Connector ", writer);
		
		getMeta().add("IMPORT_PATH", file.getAbsolutePath());
//		setColumnDefs(sourcePlainConnector.getColumnDefs());
		setTableStatus(TableStatus.IMPORTING);
		save();
		
		for (Row row : sourcePlainConnector)
			writer.write(row);
		writer.close();
		
		addConnector(writer);
		
//		getConnectorList().
		setTableStatus(TableStatus.HEALTHY);
		save();
		return this;
	}
	
//	public Table importFile(File file, Connector connector) throws IOException, ValidationException {
//		return importFile(file.getAbsolutePath(), new FileInputStream(file), connector);
//	}
	
	public static Iterable<Table> iterateTables(UUID datasetId) {
		return new Iterable<Table>() {
			@Override
			public Iterator<Table> iterator() {
				return new Iterator<Table>() {
					private ResultsIter rowsIter = null;
					
					private boolean initiated = false;
					
					private Table nextTable = null;
					
					@Override
					public Table next() {
						if (!initiated)
							hasNext();
						return nextTable;
					}
					
					@Override
					public boolean hasNext() {
						if (!initiated) {
							if (DbObject.getDatabaseProvider() instanceof LuceneDatabaseProvider) {
								rowsIter = new ResultsIter(WeblibsConst.KEYSPACE, 
										COLUMN_FAMILY, 
										new String[]{DbObject.COLUMN_DATA}, 
										null
//										new C3Where()
//											.addClause(new C3Clause(COLUMN_DSID, ClauseType.EQ, datasetId.getId())));
										);
							} else {
							
								rowsIter = new ResultsIter(WeblibsConst.KEYSPACE, 
										COLUMN_FAMILY, 
										new String[]{DbObject.COLUMN_DATA}, 
										new C3Where()
											.addClause(new C3Clause(COLUMN_DSID, ClauseType.EQ, datasetId.getId())));
							}
							initiated = true;
						}
						
						while (rowsIter.hasNext()) {
							Row row = rowsIter.next();
							
							if (row.contains(DbObject.COLUMN_DATA)) {
								nextTable = JsonUtils.fromJson((String) row.getColumns().get(DbObject.COLUMN_DATA), Table.class);
								
//								if (userId == null)
//									return true;
//								else if (nextDataset.getUserId().equals(userId))
								
								if (nextTable != null && nextTable.getDatasetId().equals(datasetId))
									return true;
							}
						}
						
						return false;
					}
				};
			}
		};
	}
		
	public static Table getTable(String datasetId, String tableName) throws IOException {
		return getTable(new UUID(datasetId), tableName);
	}
	
	public static Table getTable(UUID datasetId, String tableName) throws IOException {
		SearchResults<Table> found = TableFactory.searchTables(null, datasetId, tableName, 0, 1);
		return found.size() == 1 ? found.getObjects().get(0) : null;
	}

	public boolean isTableEmpty() {
		for (Row row : iterateRows(null)) {
			if (row != null)
				return false;
		}
		return true;
//		return !getDataIter().iterator().hasNext();
//		return DbObject.getDatabaseProvider().isTableEmpty(WeblibsConst.KEYSPACE, getId().getId());
	}

	public Object parse(String columnName, byte[] value) {
		ColumnType colType = getColumnType(columnName);
		switch (colType) {
		case DOUBLE:
			return ByteUtils.toDouble(value);
			
		case LONG:
			return ByteUtils.toLong(value);
			
		case DATE:
			return new String(value);
			
		case STRING:
			return new String(value);

		default:
			throw new RuntimeException("Column type not implemented " + colType);
		}
	}

	public void clear(boolean waitForCompletion) throws ValidationException, IOException {
		setTableStatus(TableStatus.CLEARING);
		save();
			
		if (waitForCompletion)
			new ClearTableJob(this).run();
		else
			new ClearTableJob(this).save();
	}
	
	public Data getDataIter() {
		return getDataIter(true);
	}
	
	public Data getDataIter(boolean cacheRows) {
		return DataFactory.getData(getId().getId(), cacheRows);
	}

//	public Iterable<Row> iterateRows() {
//		return DataFactory.iterateRows(WeblibsConst.KEYSPACE, getId().getId());
//	}
//	
//	public Iterator<Row> iteratorRows() {
//		return DataFactory.iteratorRows(WeblibsConst.KEYSPACE, getId().getId());
//	}

	public MetaList getMeta() {
		if (meta == null)
			meta = new MetaList();
		return meta;
	}

	public void setMeta(MetaList meta) {
		this.meta = meta;
	}
	
	public Table addMeta(final String metaName, final Object metaValue) {
		getMeta().add(metaName, metaValue);
		return this;
	}
	
	public long getNrOfRows() {
				
		return -1;
	}

	public boolean areColumnsTheSame(Table table) {
		if (table.getColumnDefs().size() != getColumnDefs().size())
			return false;
		
		for (int i = 0; i < table.getColumnDefs().size(); i++) {
			ColumnDef thisColumn = getColumnDefs().get(i);
			ColumnDef toCheckColumn = table.getColumnDefs().get(i);
			if (!thisColumn.getName().equals(toCheckColumn.getName())
					|| !thisColumn.getType().equals(toCheckColumn.getType()))
				return false;
		}
		return true;
	}

	public TableSearchStatus getSearchStatus() {
		return searchStatus;
	}

	public void setSearchStatus(TableSearchStatus searchStatus) {
		this.searchStatus = searchStatus;
	}

	public boolean isInNotebook() {
		return getMeta().get("notebookId") != null;
	}
	
	public boolean isInDataset() {
		return !isInNotebook();
	}

	public Table addConnector(Connector connector) throws ValidationException, IOException {
		new BeansConnector(connector, UUID.random().getId())
			.save();
		
		return this;
	}

	public boolean isColumsDefsDefined() {
		return columns != null;
	}

	public void clearConnectors() {
		for (BeansConnector conn : BeansConnectorFactory.iterateConnectors(getId())) {
			try {
				conn.getConnector().clear();
			} catch (Exception e) {
				LibsLogger.error(Table.class, "Cannot clear " + conn, e);
			}
		}
		getConnectorList().clear();
	}
	
	public void removeConnectors() {
		for (BeansConnector conn : BeansConnectorFactory.iterateConnectors(getId())) {
			try {
				conn.getConnector().clear();
				
				conn.remove();
			} catch (Exception e) {
				LibsLogger.error(Table.class, "Cannot clear " + conn, e);
			}
		}
		getConnectorList().clear();
	}
	
	public int getNrOfConnectors() {
		int nr = 0;
		for (BeansConnector conn : BeansConnectorFactory.iterateConnectors(getId())) {
			nr++;
		}
		return nr;
	}
	
	public void streamPlain(OutputStream output) throws IOException {
		try {
        	// determining column lengths
        	HashMap<String, Integer> id2Length = new HashMap<>();
        	for (ColumnDef columnDef : getColumnDefs()) {
        		if (columnDef.getType() == ColumnType.DOUBLE)
        			id2Length.put(columnDef.getName(), 17);
        		else if (columnDef.getType() == ColumnType.LONG)
        			id2Length.put(columnDef.getName(), 23);
        		else if (columnDef.getType() == ColumnType.INTEGER)
        			id2Length.put(columnDef.getName(), 17);
        		else if (columnDef.getType() == ColumnType.STRING) {
        			for (Row row : iterateRows(null)) {
        				int length = Math.max(row.getAsString(columnDef.getName()).length(), 20);
        				id2Length.put(columnDef.getName(), length + 2);
        				break;
        			}
        		} else 
        			id2Length.put(columnDef.getName(), 20);
        	}
        	
        	Writer writer = new OutputStreamWriter(output);
        	
        	// writing header
        	int idx = 1;
        	writer.append("#");
        	for (ColumnDef columnDef : getColumnDefs()) {
        		String name = columnDef.getName();
        		name += "(" + (idx++) + ")";
        		if (name.length() > id2Length.get(columnDef.getName()))
        			id2Length.put(columnDef.getName(), name.length() + 1);
				writer.append(padLeft(name, id2Length.get(columnDef.getName())) + " ");
			}
        	writer.append("\n");
        	
        	// writing rows
        	for (Row row : iterateRows(null)) {
            	writer.append(" ");
        		for (ColumnDef columnDef : getColumnDefs()) {
					writer.append(padLeft(row.getAsString(columnDef.getName()), id2Length.get(columnDef.getName())) + " ");
				}
            	writer.append("\n");
			}
			
			writer.flush();
			writer.close();
        } catch (Throwable e) {
        	throw new IOException("Cannot stream Table's data as plain text");
//        	LibsLogger.error(Table.class, "Cannot prepare response", e);
//            throw new WebApplicationException(e);
        }
	}
}
