package net.beanscode.web.jersey;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.annotation.security.PermitAll;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import net.beanscode.web.BeansPage;
import net.hypki.libs5.pjf.OpsFactory;
import net.hypki.libs5.utils.reflection.SystemUtils;

@Path("/")
public class StaticContent {
	
	@GET
	@PermitAll
	@Path("old")
	@Produces(MediaType.TEXT_HTML)
	public String getHomePage() throws IOException {
		return SystemUtils.readFileContent("metis/Index.html");
	}

	@GET
	@PermitAll
	@Path("login")
	@Produces(MediaType.TEXT_HTML)
	public String getLoginPage() throws IOException {
		String tmp = SystemUtils.readFileContent("metis/Login.html");
		
//		tmp = tmp.replace("<div class=\"new-login-panel\"></div>", new NewLoginPanel().toHtml());
		
		return tmp;
	}
	
//	@GET
//	@PermitAll
//	@Path("beta")
//	@Produces(MediaType.TEXT_HTML)
//	public String getBetaPage() throws IOException {
//		String tmp = SystemUtils.readFileContent("adminlte/index.html");
//		
////		tmp = tmp.replace("username", get new NewLoginPanel().toHtml());
//		
//		return tmp;
//	}
	
	@GET
	@PermitAll
	@Path("confirm")
	@Produces(MediaType.TEXT_HTML)
	public String getConfirmPage(@QueryParam("userId") String user,
    		@QueryParam("resetId") String resetId) throws IOException {
		String tmp = SystemUtils.readFileContent("metis/Confirm.html");
		
		tmp = tmp.replace("userId=abc", "userId=" + user + "&resetId=" + resetId);
		
		return tmp;
	}
	
	@GET
	@PermitAll
	@Path("createAdmin")
	@Produces(MediaType.TEXT_HTML)
	public String getCreateAdmin() throws IOException {
		return SystemUtils.readFileContent("metis/CreateAdmin.html");
	}
	
	@GET
	@PermitAll
	@Path("static/assets/{file: [a-zA-Z0-9_/\\.\\-]+\\.css}")
	@Produces("text/css")
	public Response getCss(@PathParam("file") String file) throws IOException {
		CacheControl control = new CacheControl();
		control.setMaxAge(3600); // sec.
		control.setSMaxAge(3600);  // sec.

		return Response
			.ok(SystemUtils.readFileContent("metis/assets/" + file))
			.cacheControl(control)
			.build();
	}
	
	@GET
	@PermitAll
	@Path("plugins/{file: [a-zA-Z0-9_/\\.\\-]+\\.css}")
	@Produces("text/css")
	public Response getBetaCss(@PathParam("file") String file) throws IOException {
		CacheControl control = new CacheControl();
		control.setMaxAge(3600); // sec. TODO conf
		control.setSMaxAge(3600);  // sec.

		return Response
			.ok(SystemUtils.readFileContent("adminlte/plugins/" + file))
			.cacheControl(control)
			.build();
	}
	
	@GET
	@PermitAll
	@Path("dist/{file: [a-zA-Z0-9_/\\.\\-]+\\.css}")
	@Produces("text/css")
	public Response getBetaDistCss(@PathParam("file") String file) throws IOException {
		CacheControl control = new CacheControl();
		control.setMaxAge(3600); // sec. TODO conf
		control.setSMaxAge(3600);  // sec.

		return Response
			.ok(SystemUtils.readFileContent("adminlte/" + file))
			.cacheControl(control)
			.build();
	}

	@GET
	@PermitAll
	@Path("static/assets/{file: [a-zA-Z0-9_/\\.\\-]+\\.js}")
	@Produces("text/javascript")
	public String getJs(@PathParam("file") String file) throws IOException {
		return SystemUtils.readFileContent("metis/assets/" + file);
	}

	@GET
	@PermitAll
	@Path("dist/{file: [a-zA-Z0-9_/\\.\\-]+\\.js}")
	@Produces("text/javascript")
	public String getBetaJs(@PathParam("file") String file) throws IOException {
		return SystemUtils.readFileContent("adminlte/" + file);
	}

	@GET
	@PermitAll
	@Path("plugins/{file: [a-zA-Z0-9_/\\.\\-]+\\.js}")
	@Produces("text/javascript")
	public String getPluginsBetaJs(@PathParam("file") String file) throws IOException {
		return SystemUtils.readFileContent("adminlte/plugins/" + file);
	}
	
	@GET
	@PermitAll
	@Path("static/assets/{file: [a-zA-Z0-9_/\\.\\-]+\\.(png|woff|ttf)}")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response getPng(@PathParam("file") String file) throws IOException {
		return getFile(StaticContent.class, "metis/assets/" + file);
	}
	
	@GET
	@PermitAll
	@Path("dist/{file: [a-zA-Z0-9_/\\.\\-]+\\.(png|woff|woff2|ttf|jpg)}")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response getBetaPng(@PathParam("file") String file) throws IOException {
		return getFile(StaticContent.class, "adminlte/" + file);
	}
	
	@GET
	@PermitAll
	@Path("plugins/{file: [a-zA-Z0-9_/\\.\\-]+\\.(png|woff|woff2|ttf|jpg)}")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response getBetaPluginsPng(@PathParam("file") String file) throws IOException {
		return getFile(StaticContent.class, "adminlte/plugins/" + file);
	}
		
	@GET
	@PermitAll
	@Path("static/res/{file: [a-zA-Z0-9_/\\.\\-]+\\.js}")
	@Produces("text/javascript")
	public String getResJs(@PathParam("file") String file) throws IOException {
		return SystemUtils.readFileContent("res/" + file);
	}
	
	@GET
	@PermitAll
	@Path("static/res/{file: [a-zA-Z0-9_/\\.\\-]+\\.map}")
	@Produces(MediaType.TEXT_HTML)
	public String getResMap(@PathParam("file") String file) throws IOException {
		String tmp = SystemUtils.readFileContent("res/" + file);
		return tmp != null ? tmp : "";
	}
	
	@GET
	@PermitAll
	@Path("dist/{file: [a-zA-Z0-9_/\\.\\-]+\\.map}")
	@Produces(MediaType.TEXT_HTML)
	public String getResBetaMap(@PathParam("file") String file) throws IOException {
		String tmp = SystemUtils.readFileContent("dist/" + file);
		return tmp != null ? tmp : "";
	}
	
	@GET
	@PermitAll
	@Path("plugins/{file: [a-zA-Z0-9_/\\.\\-]+\\.map}")
	@Produces(MediaType.TEXT_HTML)
	public String getResBetaPluginsMap(@PathParam("file") String file) throws IOException {
		String tmp = SystemUtils.readFileContent("plugins/" + file);
		return tmp != null ? tmp : "";
	}
	
	@GET
	@PermitAll
	@Path("static/res/{file: [a-zA-Z0-9_/\\.\\-]+\\.css}")
	@Produces("text/css")
	public String getResCss(@PathParam("file") String file) throws IOException {
		return SystemUtils.readFileContent("res/" + file);
	}
	
	@GET
	@PermitAll
	@Path("static/res/{file: [a-zA-Z0-9_/\\.\\-]+\\.(png|gif)}")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response getResOctet(@PathParam("file") String file) throws IOException {
		return getFile(StaticContent.class, "res/" + file);
	}

	@GET
	@PermitAll
	@Path("static/wl/{file: [a-zA-Z0-9_/\\.\\-]+\\.js}")
	@Produces("text/javascript")
	public String getWeblibsJs(@PathParam("file") String file) throws IOException {
//		return SystemUtils.readFileContent(Resources.class, file);
		return SystemUtils.readFileContent("res/wl/" + file);
	}

	@GET
	@PermitAll
	@Path("static/wl/{file: [a-zA-Z0-9_/\\.\\-]+\\.css}")
	@Produces("text/css")
	public String getWeblibsCss(@PathParam("file") String file) throws IOException {
//		return SystemUtils.readFileContent(Resources.class, file);
		return SystemUtils.readFileContent("res/wl/" + file);
	}

	@GET
	@PermitAll
	@Path("static/wl/{file: [a-zA-Z0-9_/\\.\\-]+\\.(gif|png)}")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response getWeblibsGif(@PathParam("file") String file) throws IOException {
//		return getFile(Resources.class, file);
		return getFile(StaticContent.class, "res/wl/" + file);
	}
	
	@GET
	@PermitAll
	@Path("favicon.ico")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response getFavicon(@PathParam("file") String file) throws IOException {
//		return getFile(Resources.class, file);
		return getFile(StaticContent.class, "res/favicon.png");
	}

	@GET
	@PermitAll
	@Path("static/pjf/{file: [a-zA-Z0-9_/\\.\\-]+\\.js}")
	@Produces("text/javascript")
	public String getPjfJs(@PathParam("file") String file) throws IOException {
		return SystemUtils.readFileContent(OpsFactory.class, file);
	}
	
	@GET
	@PermitAll
	@Path("static/pjf/{file: [a-zA-Z0-9_/\\.\\-]+\\.(png|gif)}")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response getPjfOctet(@PathParam("file") String file) throws IOException {
		return getFile(OpsFactory.class, file);
	}
	
	private Response getFile(Class clazz, String file) throws IOException {
		if (file != null && !"".equals(file)) {
			StreamingOutput stream = null;
			final InputStream in = SystemUtils.getResourceAsStream(clazz != null ? clazz : BeansPage.class, file);
			stream = new StreamingOutput() {
				public void write(OutputStream out) throws IOException, WebApplicationException {
					try {
						int read = 0;
						byte[] bytes = new byte[1024];

						while ((read = in.read(bytes)) != -1) {
							out.write(bytes, 0, read);
						}
					} catch (Exception e) {
						throw new WebApplicationException(e);
					}
				}
			};
			return Response.ok(stream).header("content-disposition", "attachment; filename = " + file).build();
		}
		return Response.ok("file path null").build();
	}
}
