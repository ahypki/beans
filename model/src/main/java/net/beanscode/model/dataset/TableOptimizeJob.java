package net.beanscode.model.dataset;

import java.io.IOException;

import net.beanscode.model.connectors.BeansConnector;
import net.beanscode.model.connectors.BeansConnectorFactory;
import net.beanscode.model.connectors.ConnectorLink;
import net.beanscode.model.connectors.ConnectorList;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.weblibs.jobs.Job;

import com.google.gson.annotations.Expose;

public class TableOptimizeJob extends Job {
	
	@Expose
	private String tableJson = null;
	
	private Table table = null;

	public TableOptimizeJob() {
		
	}
	
	public TableOptimizeJob(Table table) {
		setTableJson(table.getData());
		this.table = table;
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		LibsLogger.debug(TableOptimizeJob.class, "Removing ConnectorsList");
		
		getTable().optimize(false);
	}
	
	private Table getTable() {
		if (table == null) {
			table = JsonUtils.fromJson(getTableJson(), Table.class);
		}
		return table;
	}

	private String getTableJson() {
		return tableJson;
	}

	private void setTableJson(String tableJson) {
		this.tableJson = tableJson;
	}
}
