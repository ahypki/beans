package net.beanscode.pojo;

import com.google.gson.annotations.Expose;

import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.string.MetaList;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.Max;
import net.sf.oval.constraint.Min;
import net.sf.oval.constraint.NotNull;

public class NotebookEntryProgress {
	
	@Expose
	@NotNull
	@AssertValid
	private UUID entryId = null;
	
	/**
	 * Progress, as double value, from 0.0 up to 100.0
	 */
	@Expose
	@NotNull
	@AssertValid
	@Min(value = 0.0)
	@Max(value = 100.0)
	private double progress = 0.0;
	
	@Expose
	@NotNull
	@AssertValid
	private String message = null;
	
	@Expose
	@NotNull
	@AssertValid
	private NotebookEntryStatus notebookEntryStatus = NotebookEntryStatus.NEW;
	
	@Expose
	@AssertValid
	private MetaList meta = null;
	
	// cache
	private NotebookEntry entry = null;
	
	public NotebookEntryProgress() {
		
	}
	
	public NotebookEntryProgress(NotebookEntry ne) {
		setEntryId(ne.getId());
		setEntry(ne);
	}
	
	public NotebookEntryProgress ok() {
		setNotebookEntryStatus(NotebookEntryStatus.SUCCESS);
		setProgress(100.0);
		return this;
	}
	
	public NotebookEntryProgress fail(String message) {
		setNotebookEntryStatus(NotebookEntryStatus.FAIL);
		setMessage(message);
		setProgress(100.0);
		return this;
	}
	
	public NotebookEntryProgress running(double progress, String message) {
		setNotebookEntryStatus(NotebookEntryStatus.RUNNING);
		setMessage(message);
		setProgress(progress);
		return this;
	}

	public UUID getEntryId() {
		return entryId;
	}

	public void setEntryId(UUID entryId) {
		this.entryId = entryId;
	}

	public NotebookEntryProgress setEntry(NotebookEntry entry) {
		this.entry = entry;
		return this;
	}

	public NotebookEntryStatus getNotebookEntryStatus() {
		return notebookEntryStatus;
	}

	public NotebookEntryProgress setNotebookEntryStatus(NotebookEntryStatus notebookEntryStatus) {
		this.notebookEntryStatus = notebookEntryStatus;
		return this;
	}

	public double getProgress() {
		return progress;
	}

	public void setProgress(double progress) {
		this.progress = progress;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public MetaList getMeta() {
		if (meta == null)
			meta = new MetaList();
		return meta;
	}

	public void setMeta(MetaList meta) {
		this.meta = meta;
	}
	
	public boolean isFinished() {
		return getNotebookEntryStatus() == NotebookEntryStatus.FAIL 
				|| getNotebookEntryStatus() == NotebookEntryStatus.SUCCESS;
	}
	
	public boolean isFailed() {
		return getNotebookEntryStatus() == NotebookEntryStatus.FAIL;
	}
	
	public boolean isSuccess() {
		return getNotebookEntryStatus() == NotebookEntryStatus.SUCCESS;
	}
	
	public boolean isRunning() {
		return getNotebookEntryStatus() == NotebookEntryStatus.RUNNING;
	}
}
