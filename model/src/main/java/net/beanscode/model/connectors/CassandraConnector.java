package net.beanscode.model.connectors;

import static java.lang.String.format;
import static java.lang.String.valueOf;
import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;
import static net.hypki.libs5.utils.utils.NumberUtils.toInt;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import net.beanscode.model.cass.Data;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.dataset.TableFactory;
import net.beanscode.model.plugins.ColumnDef;
import net.beanscode.model.plugins.ColumnDefList;
import net.beanscode.model.plugins.Connector;
import net.beanscode.model.plugins.ConnectorInitParams;
import net.hypki.libs5.db.cassandra.CassandraProvider;
import net.hypki.libs5.db.cassandra.MapReduceUtils;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.Results;
import net.hypki.libs5.db.db.ResultsIter;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.db.db.weblibs.Mutation;
import net.hypki.libs5.db.db.weblibs.MutationType;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.search.elastic.ElasticSearchProvider;
import net.hypki.libs5.search.query.Query;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.Watch;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.sha.MD5Checksum;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

import org.apache.commons.lang.NotImplementedException;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;

@Deprecated
public class CassandraConnector implements Connector, Serializable, Iterable<Row>, AutoCloseable {
	
//	private UUID tableId = null;
	
	private Table tableCache = null;
	
//	private DataSaver dataSaverCache = null;
//	private long dataSaverRowsSaved = 0;
	private List<Row> rowsCached = null;
	private long rowsSaved = 0;
	
	private final int CACHE_SIZE = 5000;
	
	public final static String COLUMN_FAMILY_COLUMN_DEFS = "columndefs";
	
	@Expose
	@NotNull
	@AssertValid
	private ConnectorInitParams connectorInitParams = null;
	
	private static CassandraProvider cassandraProvider = null;
	
	private boolean autoSaveThreadStarted = false;
	private boolean autoSaveCloseRequested = false;

	public CassandraConnector() {
//		startAutoSave();
	}
	
	public CassandraConnector(Table table) {
		setTableId(table.getId());
		setTableCache(table);
		
//		startAutoSave();
	}
	
	public CassandraConnector(UUID tableId) {
		setTableId(tableId);
		
//		startAutoSave();
	}
	
	private void startAutoSave() {
		if (!autoSaveThreadStarted) {
			autoSaveThreadStarted = true;
			
			new Thread() {
				@Override
				public void run() {
					while (true) {
						try {
							if (autoSaveCloseRequested) {
								saveRowsCached();
								
								autoSaveThreadStarted = false;
								break;
							}
							
							Thread.sleep(5000);
						} catch (InterruptedException | IOException e) {
							LibsLogger.error(ElasticSearchProvider.class, "Something wrong in "
									+ "CassandraConnector flush Thread", e);
						}
					}
				}
			}.start();
		}

	}
	
	@Override
	public String getName() {
		return "Cassandra";
	}
	
	@Override
	public ConnectorInitParams getInitParams() {
		if (this.connectorInitParams == null)
			this.connectorInitParams = new ConnectorInitParams();
		return this.connectorInitParams;// getName() + ":" + getTableId().getId();
	}
	
	@Override
	public Connector setInitParams(ConnectorInitParams initParams) {
//		setTableId(new UUID(initParams.substring(initParams.indexOf(':') + 1)));
		this.connectorInitParams = initParams;
		this.tableCache = null;
		return this;
	}
	
	@Override
	public void validate() throws IOException {
		// do nothing
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof CassandraConnector) {
			CassandraConnector cassConn = (CassandraConnector) obj;
			UUID thisTableId = getTableId();
			UUID inputTableId = cassConn.getTableId();
			return thisTableId != null && inputTableId != null && thisTableId.equals(inputTableId);
		}
		return super.equals(obj);
	}
	
	private static CassandraProvider getCassandraProvider() {
		if (cassandraProvider == null) {
			cassandraProvider = new CassandraProvider();
		}
		return cassandraProvider;
	}
	
	private List<Row> getRowsCached() {
		if (rowsCached == null)
			rowsCached = new ArrayList<>();
		return rowsCached;
	}
	
//	@Override
//	public List<Row> readRows(String... pk) throws IOException {
//		throw new NotImplementedException();
//	}

	@Override
	public void write(Row row) throws IOException {
//		if (dataSaverCache == null)
//			dataSaverCache = new DataSaver(getTableId().getId());
		
		startAutoSave();
				
		getRowsCached().add(row);
		
		if ((getRowsCached().size() % CACHE_SIZE) == 0) {
			saveRowsCached();
		}
	}
	
	@Override
	public String getVersion() {
		return getClass().getSimpleName() + ", ver. 1.0";
	}

	private synchronized void saveRowsCached() throws IOException {
		if (getRowsCached().size() == 0)
			return;
		
		if (rowsSaved == 0) {
			getCassandraProvider().createKeyspace(WeblibsConst.KEYSPACE);
		}
		
		try {
			List<Mutation> muts = new ArrayList<Mutation>();
			
			List<Row> rowsCachedCache = getRowsCached();
			this.rowsCached = new ArrayList<Row>();
			for (Row row : rowsCachedCache) {
				muts.add(new Mutation(MutationType.INSERT, 
						WeblibsConst.KEYSPACE, 
						Data.COLUMN_FAMILY, 
						new C3Where()
							.addClause(new C3Clause(Table.COLUMN_TBID, ClauseType.EQ, getTableId().getId()))
							.addClause(new C3Clause(DbObject.COLUMN_PK, ClauseType.EQ, String.valueOf(row.getPk())))
//							.addClause(new C3Clause(Data.COLUMN_ROW, ClauseType.EQ, rowsSaved))
		//				eq(Table.COLUMN_TBID, getTableId().getId()))
		//				.and(eq(DbObject.COLUMN_PK, row.getPk()))
		////			.and(eq(Data.COLUMN_ROW, getRowsCounter())
							, 
						DbObject.COLUMN_DATA, 
//						JsonUtils.objectToStringPretty(row)
						MapReduceUtils.toByteBuffer(JsonUtils.objectToStringPretty(row))
//						ByteBuffer.wrap(serializeToBytes(JsonUtils.objectToStringPretty(row)))
						));
				
				rowsSaved++;
			}
		
			getCassandraProvider().save(muts);
			
			LibsLogger.debug(CassandraConnector.class, "Saved ", rowsCachedCache.size(), " data rows");
			
			getRowsCached().clear();
		} catch (ValidationException e) {
			LibsLogger.error(CassandraConnector.class, "Cannot save rows to " + getClass().getSimpleName(), e);
		}
		
//		StringBuilder bulk = new StringBuilder();
//		bulk.append("BEGIN UNLOGGED BATCH\n");
//		
//		for (Row row : getRowsCached()) {
//			bulk.append(getCassandraProvider().getQueryBuilder()
//					.update(WeblibsConst.KEYSPACE, Data.COLUMN_FAMILY)
//					.with(set(DbObject.COLUMN_DATA, ByteBuffer.wrap(serializeToBytes((HashMap<String, Object>) row.getColumns()))))
//						.and(set(Table.COLUMN_TBID, getTableId().getId()))
//						.and(set(DbObject.COLUMN_PK, row.getPk()))
//						.and(set(Data.COLUMN_ROW, rowsSaved))
//					.where(eq(Table.COLUMN_TBID, getTableId().getId()))
//						.and(eq(DbObject.COLUMN_PK, row.getPk()))
////					.and(eq(Data.COLUMN_ROW, getRowsCounter()))
//					);
//			
//			rowsSaved++;
//		}
//		
//		bulk.append("APPLY BATCH;");
//		
//		getCassandraProvider().getSession().executeAsync(bulk.toString());
		
		
	}

	@Override
	public void close() throws IOException {
		saveRowsCached();
		
		autoSaveCloseRequested = true;
	}
	
	
//	@Override
//	public Results iterateRows(String state, int size) {
//		Results cass = getCassandraProvider().getList(WeblibsConst.KEYSPACE, 
//				Data.COLUMN_FAMILY, 
//				new String[] {DbObject.COLUMN_DATA},
//				null,
//				size, 
//				state, 
//				new C3Where()
//					.addClause(new C3Clause(Table.COLUMN_TBID, ClauseType.EQ, getTableId().getId())));
//		
//		Results res = new Results();
//		res.setNextPage(cass.getNextPage());
//		for (Row row : cass) {
//			res.addRow(JsonUtils.fromJson(row.getAsString(DbObject.COLUMN_DATA), Row.class));
//		}
//		return res;
//	}
	
	@Override
	public Iterator<Row> iterator() {
		return new Iterator<Row>() {
			ResultsIter resIter = new ResultsIter(getCassandraProvider(),
					WeblibsConst.KEYSPACE, 
					Data.COLUMN_FAMILY, 
					new String[] {DbObject.COLUMN_DATA}, 
					new C3Where()
						.addClause(new C3Clause(Table.COLUMN_TBID, ClauseType.EQ, getTableId().getId()))
					);
			
			@Override
			public Row next() {
				Row r = null;
				while ((r = resIter.next()) != null) {
					try {
						JsonObject jo = JsonUtils.parseJson(r.getAsString(DbObject.COLUMN_DATA)).getAsJsonObject();
						r.getColumns().clear();
						r.setPk(jo.get(DbObject.COLUMN_PK));
		//				for (JsonElement je : jo.get("columns").getAsJsonArray()) {
		//					r.addColumn(je. getAsJsonObject().get, value)
		//				}
		//				r.setColumns(jo.get("columns").getasjson);
						JsonObject columns = jo.get("columns").getAsJsonObject();
						for (ColumnDef col : getColumnDefs()) {
		//					r.addColumn(col.getName(), columns.get(col.getName()));
							JsonElement tmp = columns.get(col.getName());
							if (col.getType() == ColumnType.DOUBLE)
								r.addColumn(col.getName(), tmp != null ? tmp.getAsDouble() : 0.0);
							else if (col.getType() == ColumnType.INTEGER)
								r.addColumn(col.getName(), tmp != null ? tmp.getAsInt() : 0);
							else if (col.getType() == ColumnType.LONG)
								r.addColumn(col.getName(), tmp != null ? tmp.getAsLong() : 0L);
							else if (col.getType() == ColumnType.STRING)
								r.addColumn(col.getName(), tmp != null ? tmp.getAsString() : "");
							else
								throw new NotImplementedException();
						}
						return r;
					} catch (Exception e) {
						LibsLogger.error(CassandraConnector.class, "Cannot read row ", r.toString());
//						return r;
					}
				}
				return null;
			}
			
			@Override
			public boolean hasNext() {
				return resIter.hasNext();
			}
		};
		
	}

	@Override
	public ColumnDefList getColumnDefs() {
		try {
			String tmp = getCassandraProvider().get(WeblibsConst.KEYSPACE, 
					COLUMN_FAMILY_COLUMN_DEFS, 
					new C3Where()
						.addClause(new C3Clause(DbObject.COLUMN_PK, ClauseType.EQ, getInitParams().getTableId().getId())), 
						DbObject.COLUMN_DATA);
			if (StringUtilities.nullOrEmpty(tmp)) {
				Table table = TableFactory.getTable(getInitParams().getTableId());
				return table != null ? table.getColumnDefs() : null;
			}
			
			ColumnDefList defs = new ColumnDefList();
			for (JsonElement je : JsonUtils.parseJson(tmp).getAsJsonArray()) {
				defs.add(JsonUtils.fromJson(je, ColumnDef.class));
			}
			return defs;
		} catch (IOException e) {
			LibsLogger.error(CassandraConnector.class, "Cannot get ColumnDef from DB", e);
			return null;
		}
	}
	
	@Override
	public void clear() throws IOException {		
		getCassandraProvider()
			.remove(WeblibsConst.KEYSPACE, 
					Data.COLUMN_FAMILY,
					new C3Where()
						.addClause(new C3Clause(Table.COLUMN_TBID, ClauseType.EQ, getTableId().getId())));
	}
	
	@Override
	public void setColumnDefs(ColumnDefList defs) {
		try {
			List<Mutation> mutations = new ArrayList<Mutation>();

			mutations.add(new Mutation(MutationType.INSERT, 
					WeblibsConst.KEYSPACE, 
					COLUMN_FAMILY_COLUMN_DEFS, 
					getInitParams().getTableId().getId(), 
					DbObject.COLUMN_PK, 
					DbObject.COLUMN_DATA, 
					JsonUtils.objectToStringPretty(defs)));
			
			getCassandraProvider().save(mutations);
		} catch (IOException | ValidationException e) {
			LibsLogger.error(CassandraConnector.class, "Cannot save ColumnDef list for a table " + getInitParams().getTableId(), e);
		}
	}
	
	public void importFile(InputStream is) throws IOException {
		assertTrue(is != null, "Input stream cannot be null");
		assertTrue(getTable() != null, "Table cannot be null");
		
		Table table = getTable();
		
		LibsLogger.debug(CassandraConnector.class, format("Importing file %s to dataset %s to table %s", 
				table.getMeta().getAsString("IMPORT_PATH"), table.getDatasetId(), table.getName()));
		
//		int saverSplit = 500;
		Watch importTime = Watch.fromNow();
//		DataSaver dataSaver = new DataSaver(table.getId().getId());
		try {
			// open file and read header (if any) and first line with data
//			Iterator<String> bf = new BigFile(is).iterator();
//			Pair<String, String> headerAndFirstData = readHeaderAndFirstData(bf);
			PlainConnector sourcePlainConnector = new PlainConnector(is);
//			table.setColumnDefs(sourcePlainConnector.getColumnDefs());//parseHeader(headerAndFirstData.getValue0(), headerAndFirstData.getValue1()));
		
			// parsing header
//			final String dataSchema = BeansDbManager.getCassandraProvider().libs5DbSchemaToCQL3Schema(WeblibsConst.KEYSPACE_LOWERCASE, new Data().getSchema());
//			final String dataInsertSTMT = new Data().getDataInsertSTMT();
			
//			checking if csv file is being read
//			boolean isCSV = false;
//			if (headerAndFirstData.getValue1().contains(",")) {
//				isCSV = true;
//				LibsLogger.debug(TableUtils.class, "Treating the file as CSV, splitting by ','");
//				headerAndFirstData = new Pair<String, String>(headerAndFirstData.getValue1().replaceAll(",", " "), bf.hasNext() ? bf.next() : "");
//				table.setColumnDefs(parseHeader(headerAndFirstData.getValue0(), headerAndFirstData.getValue1()));
//			}
			
			// importing line by line (first comments lines is the header, containing JSON with columns description)
			long rowsRead = 0;
//			long bytesRead = 0;
			HashMap<String, MD5Checksum> crc = new HashMap<>();
			
			for (Row row : sourcePlainConnector) {
				
//			}
//			
//			do {
//				String line = rowsRead == 0 ? headerAndFirstData.getValue1() : bf.next();
//
//				if (line == null)
//					break;
//				
//				if (line.startsWith("#"))
//					continue;
				
//				Line l = isCSV ? new Line(line, ',') : new Line(line);
//				Row row = new Row(UUID.random().getId());
//				Map<String, Object> values = new HashMap<>();
//				
//				values.put(DbObject.COLUMN_PK, UUID.random().getId());
//				values.put(BeansConst.COLUMN_TBID, table.getId().getId());
//				values.put(Data.COLUMN_ROW, rowsRead);
				
//				Map<String, Object> data = new HashMap<>();
				for (int i = 0; i < row.size(); i++) {
					
//					dt.put(columnNames.get(i), parseValue(l.get(i), table.getColumnType(columnNames.get(i))));
					ColumnDef colDef = table.getColumnDefs().get(i);
//					ColumnType colType = table.getColumnType(columnNames.get(i));
					String colName = colDef.getName();
//					ByteBuffer bb = toByteBuffer(l.get(i), table.getColumnType(columnNames.get(i)));
//					bulkWriter.addColumn(bytes(colName), bb, timestamp);
					
					row.addColumn(colName, row.get(colDef.getName()));
//					data.put(colName, stringToObject(l.get(i), colDef.getType()));
					
					// computing CRC
					if (crc.get(colName) == null)
						crc.put(colName, new MD5Checksum());
					crc.get(colName).update(colDef.getName());
				}

//				values.put(DbObject.COLUMN_DATA, ByteBuffer.wrap(StringUtilities.serializeToBytes((HashMap<String, Object>)data)));
				
//				writer.addRow(values);
//				dataSaver.addRow(row);
//				bytesRead += line.length();
				
//				if ((++rowsRead % 500) == 0) {
//					dataSaver.save();
//					dataSaver.clear();
//					LibsLogger.debug(TableUtils.class, rowsRead, " rows saved so far...");
//				}
				
				write(row);
				
				
			}//while (bf.hasNext());
			
			close();
			
//			dataSaver.save();
//			dataSaver.clear();
			LibsLogger.debug(CassandraConnector.class, format("Last data part saved, total time= %s", importTime.toString()));
			
			if (rowsRead == 0)
				LibsLogger.warn(CassandraConnector.class, "No rows imported to the table " + table.getName());
			
			// saving CRC checksums
//			for (String colName : crc.keySet()) {
//				table.getColumnsMD5().put(colName, crc.get(colName).getChecksum());
//			}
			table.save();
			LibsLogger.debug(CassandraConnector.class, "Total " + rowsRead + " rows saved for table " + table.getName());
			
			LibsLogger.debug(CassandraConnector.class, format("File %s imported successfully in %s", 
					table.getMeta().getAsString("IMPORT_PATH"), importTime.toString()));
		} catch (Exception e) {
			LibsLogger.error(CassandraConnector.class, "Cannot import file to database", e);
			throw new IOException("Cannot import file to database", e);
		}
	}
	
	public Table getTable() {
		if (tableCache == null && getTableId() != null) {
			try {
				tableCache = TableFactory.getTable(getTableId());
			} catch (IOException e) {
				LibsLogger.error(CassandraConnector.class, "Cannot get Table object from DB", e);
			}
		}
		return tableCache;
	}
	
	private void setTableCache(Table table) {
		this.tableCache = table;
	}

	public UUID getTableId() {
		return getInitParams().getTableId();
	}

	public void setTableId(UUID tableId) {
		getInitParams().setTableId(tableId);
		setTableCache(null);
	}
	
	@Override
	public Iterable<Row> iterateRows(Query filter, int splitNr, int splitCount) {
		// TODO implement it
		if (splitNr == 0)
			return iterateRows(filter);
		else
			return new Iterable<Row>() {
				
				@Override
				public Iterator<Row> iterator() {
					return new Iterator<Row>() {
						
						@Override
						public Row next() {
							return null;
						}
						
						@Override
						public boolean hasNext() {
							return false;
						}
					};
				}
			};
	}
	
	@Override
	public Iterable<Row> iterateRows(Query filter) {
		return new Iterable<Row>() {
			
			@Override
			public Iterator<Row> iterator() {
				return new Iterator<Row>() {
					
					private Iterator<Row> allRowsIter = iterateRows(filter).iterator();
					private Row row = null;
					
					@Override
					public Row next() {
						return row;
					}
					
					@Override
					public boolean hasNext() {
						while (allRowsIter.hasNext()) {
							row = allRowsIter.next();
							
							if (row == null)
								return false;
							else if (filter.isFulfilled(row))
								return true;
						}
						
						return row != null;
					}
				};
			}
		};
	}
	
	@Override
	public Results iterateRows(Query filter, String state, int size) {
		int from = 0;
		if (notEmpty(state))
			from = toInt(state);
		
		int i = 0;
		Results res = new Results();
		for (Row row : this.iterateRows(filter)) {
			if (i >= from)
				res.addRow(row);
			
			i++;
			
			if (res.size() == size)
				break;
		}
		res.setNextPage(valueOf(i));
		return res;
	}
	
	public static void importTableCassBulk(Table table, InputStream is) throws IOException, ValidationException {
		throw new NotImplementedException("This method has to be resurected");
//		assertTrue(is != null, "Input stream cannot be null");
//		assertTrue(table != null, "Table cannot be null");
//		
//		BeansDbManager.getCassandraProvider().setSystemPropertyCassandraConfig();
//		
//		LibsLogger.debug(TableUtils.class, format("Importing file %s to dataset %s to table %s", table.getImportPath(), table.getDatasetId(), table.getName()));
//		
//		Option bulkSplitOption = OptionFactory.getOption(OptionFactory.BULK_LOAD_ROWS_SPLIT);
//		long bulkSplit = bulkSplitOption != null ? bulkSplitOption.getValueAsLong() : Long.MAX_VALUE;
//		
//		Watch importTime = Watch.fromNow();
//		File directory = null;
//		try {
//			// open file and read header (if any) and first line with data
//			Iterator<String> bf = new BigFile(is).iterator();
//			Pair<String, String> headerAndFirstData = readHeaderAndFirstData(bf);
//			table.setColumnDefs(parseHeader(headerAndFirstData.getValue0(), headerAndFirstData.getValue1()));
//			
//			// creating cassandra objects to bulk import
//			directory = new File("BulkImportTmp/" + UUID.random() + "/" + WeblibsConst.KEYSPACE_LOWERCASE + "/data");
//			directory.mkdirs();
//			FileUtil.cleanDir(directory);
//
//			// parsing header
//			final String dataSchema = BeansDbManager.getCassandraProvider().libs5DbSchemaToCQL3Schema(WeblibsConst.KEYSPACE_LOWERCASE, new Data().getSchema());
//			final String dataInsertSTMT = new Data().getDataInsertSTMT();
//			
//			// Prepare SSTable writer 
//			CQLSSTableWriter.Builder builder = CQLSSTableWriter.builder();
//			// set output directory 
//			builder.inDirectory(directory)
//			       // set target schema 
//			       .forTable(dataSchema)
//			       // set CQL statement to put data 
//			       .using(dataInsertSTMT)
//			       // set partitioner if needed 
//			       // default is Murmur3Partitioner so set if you use different one. 
////			       .withPartitioner(new Murmur3Partitioner())
////			       .withBufferSizeInMB(1)
//			       ;
//			// Prepare SSTable writer, after reading header of the file
//			CQLSSTableWriter writer = builder.build();
//			
//			// importing line by line (first comments lines is the header, containing JSON with columns description)
//			long rowsRead = 0;
//			HashMap<String, MD5Checksum> crc = new HashMap<>();
//			
//			do {
//				String line = rowsRead == 0 ? headerAndFirstData.getValue1() : bf.next();
//
//				if (line == null)
//					break;
//				
//				if (line.startsWith("#"))
//					continue;
//				
//				Line l = new Line(line);
//				Map<String, Object> values = new HashMap<>();
//				
//				values.put(DbObject.COLUMN_PK, UUID.random().getId());
//				values.put(BeansConst.COLUMN_TBID, table.getId().getId());
//				values.put(Data.COLUMN_ROW, rowsRead);
//				
//				Map<String, Object> data = new HashMap<>();
//				for (int i = 0; i < l.size(); i++) {
//					
////					dt.put(columnNames.get(i), parseValue(l.get(i), table.getColumnType(columnNames.get(i))));
//					ColumnDef colDef = table.getColumnDefs().get(i + 2);
////					ColumnType colType = table.getColumnType(columnNames.get(i));
//					String colName = colDef.getName();
////					ByteBuffer bb = toByteBuffer(l.get(i), table.getColumnType(columnNames.get(i)));
////					bulkWriter.addColumn(bytes(colName), bb, timestamp);
//					
//					data.put(colName, stringToObject(l.get(i), colDef.getType()));
//					
//					// computing CRC
//					if (crc.get(colName) == null)
//						crc.put(colName, new MD5Checksum());
//					crc.get(colName).update(l.get(i));
//				}
//
//				values.put(DbObject.COLUMN_DATA, ByteBuffer.wrap(StringUtilities.serializeToBytes((HashMap<String, Object>)data)));
//				
//				writer.addRow(values);
//				
//				if ((++rowsRead % 100000) == 0)
//					LibsLogger.debug(TableUtils.class, rowsRead + " rows saved so far...");
//					
//				if (bulkSplit > 0 && (rowsRead % bulkSplit) == 0) {
//					LibsLogger.debug(TableUtils.class, rowsRead + " rows saved so far...");
//					
//					writer.close();
//					
//					// real importing to Cass is done in a separate job
//					new ImportSSTablesJob(directory.getAbsolutePath(), true).save();
//					LibsLogger.debug(TableUtils.class, ImportSSTablesJob.class.getSimpleName() + " created");
//					
//					// creating cassandra objects to bulk import
//					directory = new File("BulkImportTmp/" + UUID.random() + "/" + WeblibsConst.KEYSPACE_LOWERCASE + "/data");
//					directory.mkdirs();
//					FileUtil.cleanDir(directory);
//					
//					builder = CQLSSTableWriter.builder();
//					// set output directory 
//					builder.inDirectory(directory)
//					       // set target schema 
//					       .forTable(dataSchema)
//					       // set CQL statement to put data 
//					       .using(dataInsertSTMT)
//					       // set partitioner if needed 
//					       // default is Murmur3Partitioner so set if you use different one. 
////					       .withPartitioner(new Murmur3Partitioner())
////					       .withBufferSizeInMB(1)
//					       ;
//					// Prepare SSTable writer, after reading header of the file
//					writer = builder.build();
//				}
//				
//				// TO-DO split imports to 1M rows or something, and for each portion of the data run separate job
//				
//			} while (bf.hasNext());
//			
//			writer.close();
//			
//			if (rowsRead == 0)
//				LibsLogger.warn(TableUtils.class, "No rows imported to the table " + table.getName());
//			
//			// saving CRC checksums
//			for (String colName : crc.keySet()) {
//				table.getColumnsMD5().put(colName, crc.get(colName).getChecksum());
//			}
//			table.save();
//			LibsLogger.debug(TableUtils.class, "Total " + rowsRead + " rows saved for table " + table.getName());
//			
//			// saving basic TableStats
//			try {
//				new TableStats(table).save();
//			} catch (Exception e) {
//				LibsLogger.error(TableUtils.class, "Cannot save table' stats", e);
//			}
//			
//			// real importing to Cass is done in a separate job
//			new ImportSSTablesJob(directory.getAbsolutePath(), true).save();
//			LibsLogger.debug(TableUtils.class, ImportSSTablesJob.class.getSimpleName() + " created");
//						
//			LibsLogger.debug(TableUtils.class, format("File %s imported successfully in %s", table.getImportPath(), importTime.toString()));
//		} catch (Exception e) {
//			throw new IOException("Cannot import file to database", e);
//		}
		
		
	}

	@Override
	public long getAproxBytesSize() throws IOException {
		// TODO Auto-generated method stub
		return 1_000_000_000;
	}
}
