package net.beanscode.model.backup;

import java.io.IOException;
import java.util.List;

import net.beanscode.model.UserUtils;
import net.beanscode.model.utils.JvmUtils;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.DateUtils;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.weblibs.jobs.Job;
import net.hypki.libs5.weblibs.user.User;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class AutoBackupJob extends Job {
	@Expose
	@NotNull
	@AssertValid
	private UUID jvm = JvmUtils.getThisJvmUUID();

	public AutoBackupJob() {
		
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		if (JvmUtils.isThisJvm(getJvm()) == false) {
			LibsLogger.debug(AutoBackupJob.class, "This ", getClass().getSimpleName(), " comes from previous"
					+ " JVM, closing this job..");
			return;
		}
		
		if (!JvmUtils.getThisJvmStartDate().addHours(1).isBefore(SimpleDate.now())) {
			LibsLogger.debug(AutoBackupJob.class, "TESTING Do not do backup if earlier than 1h after starting BEANS");
			setPostponeMs(DateUtils.ONE_HOUR_MS);
			return;
		}
		
		List<User> admins = UserUtils.getAdminUsers();
		
		if (admins.size() == 0)
			LibsLogger.error(AutoBackupJob.class, "There are no admins in BEANS!");
		else {
			new BackupJob()
				.setUserId(admins.get(0).getUserId())
				.save();
		}
		
		// postpone one day
		setPostponeMs(DateUtils.ONE_DAY_MS);
	}

	private UUID getJvm() {
		return jvm;
	}

	private void setJvm(UUID jvm) {
		this.jvm = jvm;
	}

}
