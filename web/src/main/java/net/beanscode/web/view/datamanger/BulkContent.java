package net.beanscode.web.view.datamanger;

import java.io.IOException;

import net.beanscode.web.beta.BetaContent;
import net.beanscode.web.view.ui.BetaButtonGroup;
import net.hypki.libs5.pjf.components.Component;

import org.rendersnake.HtmlCanvas;

public class BulkContent extends BetaContent {
	
	public BulkContent() {
		
	}
	
	public BulkContent(Component parent) {
		super(parent);
	}
	
	@Override
	protected void renderHeader(HtmlCanvas html) throws IOException {
		html
			.h1()
				.content("Bulk changes");
	}

	@Override
	protected void renderContent(HtmlCanvas html) throws IOException {
		html
			.render(new BulkChangesPanel(this)
					.setWidth(12));
	}

	@Override
	protected BetaButtonGroup getMenu() throws IOException {
		return null;
	}

	@Override
	protected String getMenuName() throws IOException {
		return null;
	}

}
