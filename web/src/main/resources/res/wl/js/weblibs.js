function isEmpty(v) {
	return v === undefined || v == null || v.length == 0;
}

function wlFormSerializable() {
	var arrayData, objectData;
	arrayData = this.serializeArray();
	objectData = {};

	$.each(arrayData, function() {
		var value;

		if (this.value != null) {
			value = this.value;
		} else {
			value = '';
		}

		if (objectData[this.name] != null) {
			if (!objectData[this.name].push) {
				objectData[this.name] = [ objectData[this.name] ];
			}

			objectData[this.name].push(value);
		} else {
			objectData[this.name] = value;
		}
	});

	return objectData;
}

function localstorageTest() {
	localStorage.setItem("test-1", "test-value");
	wlInfo("localStorage tes", localStorage.getItem("test-1"));
	removeItem("test-1");
	wlInfo("localStorage tes", localStorage.getItem("test-1"));
}

function wlAjax(url, params, success) {
	$.ajax({
		type : "POST", url : url, data : params, success : success
	});
}

function testRest() {
//	wlAjax("/rest/test", "dziala=okdziala", 
//		function(response) {
//			wlInfo(msg["Info"], response);
//		}
//	);
	
//	$.gritter.add({
//		"title": "gritter dziala 2",
//		"text": "message",
//		"image": '/resources/res.Resources/images/info.png',
//	});
}

function wlSearchFocusClear() {
	$("#wlSearchBox").val("").focus();
}

function wlSearchFocus(clearText) {
	if (clearText != undefined && clearText == true) {
		console.log("clearing search box");
		$("#wlSearchBox").focus().select();//val("");
	} else 
		$("#wlSearchBox").focus();
}

function wlToNumber(v) {
	return parseInt(v, 10);
}

function wlBuildQS() {
	var builder = [];
	for (var i = 0; i < arguments.length; i = i + 2) {
		builder.push(encodeURIComponent(arguments[i]));
		builder.push("=");
		builder.push(encodeURIComponent(arguments[i + 1]));
		builder.push("&");
	}
	console.log(builder.join(""));
	return builder.join("");
}

/**
 * Returns date in format YYYY-MM-DD
 */
function wlDate(ms) {
	var date = ms === undefined ? new Date() : new Date(ms);
	var month = date.getMonth() + 1;
	var day = date.getDate();
	return date.getFullYear() + "-" + (month < 10 ? "0" + month : month) + "-" + (day < 10 ? "0" + day : day);
//		var hour = date.getHour();
//		var min = date.getMinutes();
}

/**
 * Returns hour in format HH:MM
 */
function wlHour(ms) {
	var date = new Date(ms);
	var hour = date.getHours() + 1;
	var mins = date.getMinutes();
	return (hour < 10 ? "0" + hour : hour) + ":" + (mins < 10 ? "0" + mins : mins);
}

function wlIsDate(v) {
	var dateRegex = /^[\d]{4}\-[\d]{2}\-[\d]{2}$/;
	return dateRegex.test(v);
}

function wlIsHour(v) {
	var hourRegex = /^[\d]{1,2}\:[\d]{2}$/;
	return hourRegex.test(v);
}

function wlIsNumber(v) {
	var digitsRegex = /^[\d]*$/;
	return digitsRegex.test(v);
}

function wlIsRadio(jQuerySelector) {
	var selected = parseInt($(jQuerySelector + " input[type='radio']:checked").val(), 10);
	return selected;
}

function wlInputToggleEnabled(jQuerySelector) {
	var isEnabled = wlIsEnabled(jQuerySelector);
	wlSetEnabled(jQuerySelector, !isEnabled);
}

function wlSetEnabled(jQuerySelector, isEnabled) {
	if (wlIsIE8()) {
		$(jQuerySelector).prop('disabled', isEnabled);
	} else {
		if (isEnabled)
			$(jQuerySelector).removeAttr('disabled');
		else
	    	$(jQuerySelector).attr('disabled', 'disabled');
	}
}

function wlIsEnabled(jQuerySelector) {
//	if (wlIsIE8()) {
//		return !($(jQuerySelector).attr('disabled') === undefined);
//	} else
		return $(jQuerySelector).attr('disabled') === undefined;
}

function wlIsChecked(jQuerySelector) {
	if ($.browser.msie && parseInt($.browser.version, 10) === 8) {
		// IE8 shit
		// $('input.radio_id:checked').attr('checked',false);
		var ch = $(jQuerySelector + ':checked').attr('checked');
		return (ch === undefined ? false : true);
//		$(jQuerySelector + ":checked").each(function() {
//			alert("IE8 to gówno, ale checkbox jest WŁĄCZONY");
//		    return true;
//		});
//		alert("IE8 to gówno, ale checkbox jest WYŁĄCZONY");
//		return false;
	} else
		return $(jQuerySelector).is(':checked');
//	var isChecked = $(jQuerySelector + ":checked").val();
//	var b = (isChecked !== undefined ? true : false);
//	return b;
}

function wlIsIE8() {
	return ($.browser.msie && parseInt($.browser.version, 10) === 8);
}

function wlCheckboxCheck(jQuerySelector, isChecked) {
	if (isChecked)
		return $(jQuerySelector).attr('checked', isChecked ? "checked" : "");
	else
		return $(jQuerySelector).removeAttr('checked');
}

function getPageName() {
	var regexS = "http[s]*\\://[\\w\\.\\:]+/([\\w]+)";//"[\\?&]" + name + "=([^&#]*)";
	var regex = new RegExp(regexS);
	var results = regex.exec(window.location.href);
	if(results == null)
		return "";
	else
		return decodeURIComponent(results[1].replace(/\+/g, " "));
}

function getPageParam(name) {
  var regexS = "/" + name + "/([\\w]+)";//"[\\?&]" + name + "=([^&#]*)";
  var regex = new RegExp(regexS);
  var results = regex.exec(window.location.href);
  if(results == null)
    return "";
  else
    return decodeURIComponent(results[1].replace(/\+/g, " "));
}

function getParameterByName(name) {
  name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
  var regexS = "[\\?&]" + name + "=([^&#]*)";
  var regex = new RegExp(regexS);
  var results = regex.exec(window.location.href);
  if(results == null)
    return "";
  else
    return decodeURIComponent(results[1].replace(/\+/g, " "));
}

function wlLogOff() {
	eraseCookie("lc");
	window.location = "/";
}

function addTimerCallback(textArea, callback, delay) {
//	alert(textArea);
//	alert(callback);
//	alert(delay);
	var timer = null;
//	alert('begin');
	textArea.onkeyup = function() {
		if (timer) {
//			alert('przed clear');
			window.clearTimeout(timer);
//			alert('po clear');
		}
//		alert('przed set');
		timer = window.setTimeout(function() {
			timer = null;
			callback();
		}, delay);
//		alert('po set');
	};
	textArea = null;
//	alert('end');
}

function uuid(nospaces) {
	var chars = '0123456789abcdef'.split('');

	var uuid = [], rnd = Math.random, r;
	uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
	uuid[14] = '4'; // version 4

	for ( var i = 0; i < 36; i++) {
		if (!uuid[i]) {
			r = 0 | rnd() * 16;

			uuid[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r & 0xf];
		}
	}

	return nospaces === undefined ? uuid.join('') : uuid.join('').replace(/\-/g, "");
}

function editItemSelectTags() {
	var selectedTags = $("#tagsInputJQ").val();
	$('.tagInputSuggestedTagList').children().each(
	    function() {
	        // access to form element via $(this)
	    	var cl = $(this).attr("class");
	    	var v = $(this).html();
	    	if (selectedTags.match("[\s]*" + v + "[\s]*"))
	    		$(this).addClass("tagUsed");
//	    	alert(cl + " " + v);
	    }
	);
}

function showMessagesWithGritter() {
    if (document.cookie && document.cookie != '') {
        var split = document.cookie.split(';');
        for (var i = 0; i < split.length; i++) {
            var name_value = split[i].split("=");
            name_value[0] = name_value[0].replace(/^ /, '');
            if (name_value[0].substr(0, 7) === "msgInfo") {
            	wlInfo(msg["Info"], decodeURIComponent(name_value[1]).replace(/\+/g, " "));
            	eraseCookie(name_value[0]);
            } else if (name_value[0].substr(0, 8) === "msgError") {
            	wlError(msg["Error"], decodeURIComponent(name_value[1]).replace(/\+/g, " "));
            	eraseCookie(name_value[0]);
            }
        }
    }
}

function get_cookies_array() {
    var cookies = { };
    if (document.cookie && document.cookie != '') {
        var split = document.cookie.split(';');
        for (var i = 0; i < split.length; i++) {
            var name_value = split[i].split("=");
            name_value[0] = name_value[0].replace(/^ /, '');
            cookies[decodeURIComponent(name_value[0])] = decodeURIComponent(name_value[1]);
        }
    }
    return cookies;
}


function wlInfo(title, message) {
	if (message === undefined || message.length == 0)
		message = " ";
	$.gritter.add({
		title: title,
		text: message,
		image: '/resources/res.Resources/images/AddCheckbox.png'
	});
//	var parent = document.getElementById("jQMessages");
//	var v = document.createElement("li");
//	v.setAttribute("class", "feedbackPanelINFO");
//	v.innerHTML = message;
//	parent.appendChild(v);
}

function wlError(title, message) {
	if (message === undefined || message.length == 0)
		message = " ";
	$.gritter.add({
		title: title,
		text: message,
		image: '/resources/res.Resources/images/alert.png'
	});
//	var parent = document.getElementById("jQMessages");
//	var v = document.createElement("li");
//	v.setAttribute("class", "feedbackPanelERROR");
//	v.innerHTML = message;
//	parent.appendChild(v);
}

function wlFollowerAdd(userId, followerId, classLink) {
	$.ajax({
		type : "POST",
		url : "/rest/followers/add",
		data : "userId=" + userId + "&followerId=" + followerId,
		success : function(response) {
			var obj = jQuery.parseJSON(response);
			if (obj.result == "OK") {
				wlInfo(msg["Info"], obj.msg);
				$("." + classLink).hide();
			} else {
				wlError(msg["Error"], obj.msg);
			}
		}
	});
}

function wlFriendRequestAccept(userId, friendId, classLink) {
	$.ajax({
		type : "POST",
		url : "/rest/friends/req/accept",
		data : "userId=" + userId + "&friendId=" + friendId,
		success : function(response) {
			var obj = jQuery.parseJSON(response);
			if (obj.result == "OK") {
				wlInfo(msg["Info"], obj.msg);
				$("." + classLink).hide();
			} else {
				wlError(msg["Error"], obj.msg);
			}
		}
	});
}

function wlFriendRequestRemove(userId, friendId, classLink) {
	$.ajax({
		type : "POST",
		url : "/rest/friends/req/remove",
		data : "userId=" + userId + "&friendId=" + friendId,
		success : function(response) {
			var obj = jQuery.parseJSON(response);
			if (obj.result == "OK") {
				wlInfo(msg["Info"], obj.msg);
				$("." + classLink).hide();
			} else {
				wlError(msg["Error"], obj.msg);
			}
		}
	});
}

function wlFriendRequestSent(userId, friendId, classLink) {
	$.ajax({
		type : "POST",
		url : "/rest/friends/req/sent",
		data : "userId=" + userId + "&friendId=" + friendId,
		success : function(response) {
			var obj = jQuery.parseJSON(response);
			if (obj.result == "OK") {
				wlInfo(msg["Info"], obj.msg);
				$("." + classLink).hide();
			} else {
				wlError(msg["Error"], obj.msg);
			}
		}
	});
}


//function wlRemoveFolder(userId, filePath, idLinkToRemove) {
//	$.ajax({
//		type : "POST",
//		url : "/rest/folder/remove/",
//		data : "userId=" + userId + "&filePath=" + filePath,
//		success : function(response) {
//			var obj = jQuery.parseJSON(response);
//			if (obj.result == "OK") {
//				wlInfo(msg["Info"], obj.msg);
//				$("." + idLinkToRemove).parent().hide();
//			} else {
//				wlError(msg["Error"], obj.msg);
//			}
//		}
//	});
//}


function wlRemoveTemplate(userId, templateId, classLink) {
	$.ajax({
		type : "POST",
		url : "/rest/template/remove/",
		data : "userId=" + userId + "&templateId=" + templateId,
		success : function(response) {
			var obj = jQuery.parseJSON(response);
			if (obj.result == "OK") {
				wlInfo(msg["Info"], obj.msg);
				$("." + classLink).parent().hide();
			} else {
				wlError(msg["Error"], obj.msg);
			}
		}
	});
}

function wlRemoveItem(userId, itemId, classLink) {
	$.ajax({
		type : "POST",
		url : "/rest/item/remove/",
		data : "userId=" + userId + "&itemId=" + itemId,
		success : function(response) {
			var obj = jQuery.parseJSON(response);
			if (obj.result == "OK") {
				if (obj.msg != null && obj.msg != "")
					wlInfo(msg["Info"], obj.msg);
				else
					wlInfo(msg["Info"], msg["Common.OK"]);
				$("." + classLink).parent().parent().parent().replaceWith("<div class=\"wlItemRemoved\">(" + msg["Item.Removed"] + ")</div>");//hide();
//				wlRemoveChild("idList", "idLink");
			} else {
				if (obj.msg != null && obj.msg != "")
					wlError(msg["Error"], obj.msg);
				else
					wlError(msg["Error"], msg["Common.Fail"]);
			}
		}
	});
}

function wlRemoveChild(parentName, childName) {
	var parent = document.getElementById(parentName);
	var child = document.getElementById(childName);
	parent.removeChild(child);
}

function createCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name,"",-1);
}

$.fn.exchangePositionWith = function(selector) {
    var other = $(selector);
    this.after(other.clone());
    other.after(this).remove();
};

function wlRenameShiftIds() {
	var counter = 0;
	$("#addingItemSortable input[type='hidden']").each(function() {
		id = $(this).attr('id');
		if (id != null && id.match(/^Shift[\d]+/)) {
			$(this).val(counter++);
		}
	});
}

function wlShiftUp(num) {
	var pos = parseInt($('#Shift' + num).val(), 10);
	if (pos == 0) {
		wlInfo(msg["Info"], msg["Field.CannotMoveHigher"]);
		return;
	}
	
//	alert("Current pos " + pos);
	$('#Shift' + num).val(pos - 1);
			
	var old = $("#addingItemSortable li:eq(" + (pos + 3) + ")");
	var neww = $("#addingItemSortable li:eq(" + (pos + 2) + ")");
	wlInfo("->", old.attr('id') + "->" + neww.attr('id'));
//	neww.before(old);
    neww.before(old.clone());
//    alert("#addingItemSortable li:eq(" + (pos + 4) + ")");
    $("#addingItemSortable li:eq(" + (pos + 4) + ")").remove();
    
    wlRenameShiftIds();
}

function createOptions(num, divIdName, shiftVisible) {
	var element = document.createElement("div");
	element.setAttribute("class", "editItemOptions");
	
	if (shiftVisible == null)
		shiftVisible = true;
	
	var span = document.createElement("span");
	span.setAttribute("class", "editItemOptions");
	
	var v = document.createElement("input");
	v.setAttribute("type", "checkbox");
	v.setAttribute("id", "privField" + num);
	v.setAttribute("name", "privField" + num);
	span.appendChild(v);
	
	v = document.createElement("label");
	v.setAttribute("for", "privField" + num);
	v.innerHTML = msg["PrivField"];
	span.appendChild(v);
			
	span.innerHTML += " | ";
	
	var lnk = document.createElement("a");
	lnk.setAttribute("class", "verysmall zen awesome delConfirm" + num);
	lnk.innerHTML = msg["Remove"];
	span.appendChild(lnk);
	
	var numInt = parseInt(num, 10) - 1;
	var upHidden = document.createElement("input");
	upHidden.setAttribute("type", "hidden");
	upHidden.setAttribute("id", "Shift" + num);
	upHidden.setAttribute("value", numInt);
	span.appendChild(upHidden);
	
//	if (shiftVisible == true) {
//		span.innerHTML += " | ";
//		
//		var up = document.createElement("img");
//		up.setAttribute("class", "wlArrow");
//		up.setAttribute("src", "/resources/res.Resources/images/up.png");
//		up.setAttribute("onclick", "wlShiftUp('" + num + "')");
//		span.appendChild(up);
//	}
	
	var scr = document.createElement("script");
	scr.setAttribute("type", "text/javascript");
	scr.innerHTML = "$(document).ready(function() { $('a.delConfirm" + num + "').click(function() { removeEvent('" + divIdName + 
		"'); return false; }); $('a.delConfirm" + num + "').confirm(); });";
	span.appendChild(scr);
	
	element.appendChild(span);
	return element;
}

function createFieldnameLabel(num, labelName) {
	var field = document.createElement("label");
	field.setAttribute("id", "label" + num);
	field.setAttribute("class", "editable");
	field.setAttribute("title", msg["DblClickToEdit"]);
	field.innerHTML = labelName + " " + num;
	
	var hidden = document.createElement("input");
	hidden.setAttribute("type", "hidden");
	hidden.setAttribute("value", labelName + " " + num);
	hidden.setAttribute("id", "label" + num);
	hidden.setAttribute("name", "label" + num);
	
	var span = document.createElement("span");
	span.appendChild(field);
	span.appendChild(hidden);
	return span;
}

function wlUploadOther(id) {
	var divIdName = "my" + id + "Div";
	var myDiv = document.getElementById(divIdName);
	wlRemoveChild(divIdName, "fileData" + id);
	wlRemoveChild(divIdName, "aFileData" + id);
	
	var v = document.createElement("input");
	v.setAttribute("type", "file");
	v.setAttribute("name", "fileData" + id);
	myDiv.appendChild(v);
}

function addFileEvent(idContainer)
{
	var ni = document.getElementById(idContainer);
	var numi = document.getElementById("fieldCounter");
	var num = (document.getElementById("fieldCounter").value -1)+ 2;
	numi.value = num;
	var divIdName = "my" + num + "Div";
	var newdiv = document.createElement("li");
	newdiv.setAttribute("id", divIdName);
	
	newdiv.appendChild(createFieldnameLabel(num, msg["NewItem.DefaultFileLabel"]));
	newdiv.appendChild(createOptions(num, divIdName));
	newdiv.appendChild(document.createElement("br"));
	
	var v = document.createElement("input");
	v.setAttribute("type", "file");
	v.setAttribute("name", "fileData" + num);
//	v.setAttribute("class", "wlInput");
//	v.setAttribute("width", "60");
	newdiv.appendChild(v);
	
	var v2 = document.createElement("input");
	v2.setAttribute("type", "hidden");
	v2.setAttribute("name", "file" + num);
	newdiv.appendChild(v2);

	ni.appendChild(newdiv);
	setClickable(document.getElementById("label" + num), num);
}

function addStarsEvent(idContainer)
{
	var ni = document.getElementById(idContainer);
	var numi = document.getElementById("fieldCounter");
	var num = (document.getElementById("fieldCounter").value -1)+ 2;
	numi.value = num;
	var divIdName = "my" + num + "Div";
	var newdiv = document.createElement("li");
	newdiv.setAttribute("id", divIdName);
	newdiv.setAttribute("class", "starPanel");
	
	newdiv.appendChild(createFieldnameLabel(num, msg["NewItem.DefaultStarsLabel"]));
	newdiv.appendChild(createOptions(num, divIdName));
	newdiv.appendChild(document.createElement("br"));
	
	for (i = 1; i <= 5; i++) {
		var element = document.createElement("input");
		element.setAttribute("type", "radio");
		element.setAttribute("value", i);
		element.setAttribute("name", "starsRating" + num);
		element.setAttribute("class", "star");
		newdiv.appendChild(element);
	}
	
	var v = document.createElement("input");
	v.setAttribute("type", "hidden");
	v.setAttribute("id", "hiddenStars" + num);
	v.setAttribute("name", "hiddenStars" + num);
	v.setAttribute("value", "0");
	newdiv.appendChild(v);
	
	var scr = document.createElement("script");
	scr.setAttribute("type", "text/javascript");
	scr.innerHTML = "$(function(){$('input.star').rating();});";
	newdiv.appendChild(scr);
	
	ni.appendChild(newdiv);
	
	setClickable(document.getElementById("label" + num), num);
}

function addTextareaEvent(idContainer)
{
	var ni = document.getElementById(idContainer);
	var numi = document.getElementById("fieldCounter");
	var num = (document.getElementById("fieldCounter").value -1)+ 2;
	numi.value = num;
	var divIdName = "my" + num + "Div";
	var newdiv = document.createElement("li");
	newdiv.setAttribute("id", divIdName);
	newdiv.appendChild(createFieldnameLabel(num, msg["NewItem.DefaultLabel"]));
	newdiv.appendChild(createOptions(num, divIdName, false));
	newdiv.appendChild(document.createElement("br"));
	
	var v = document.createElement("textarea");
	v.setAttribute("name", "text" + num);
	v.setAttribute("class", "ckEditor");
	newdiv.appendChild(v);
		
	ni.appendChild(newdiv);
	setClickable(document.getElementById("label" + num), num);
	
	var scr = document.createElement("script");
	scr.setAttribute("type", "text/javascript");
	scr.innerHTML = "$(function() { CKEDITOR.replace( 'text" + num + "', { toolbar : 'Full', language : 'pl', resize_enabled : false, filebrowserBrowseUrl : '/files/' } ); });";
//	newdiv.appendChild(scr);
	var js = document.getElementById("addingItemSortableJS");
	js.appendChild(scr);
}

//function addImageEvent(idContainer, labelName, dblClickTooltip, removeLabel, privFieldLabel)
//{
//	var ni = document.getElementById(idContainer);
//	var numi = document.getElementById("fieldCounter");
//	var num = (document.getElementById("fieldCounter").value -1)+ 2;
//	numi.value = num;
//	var divIdName = "my" + num + "Div";
//	var newdiv = document.createElement("li");
//	newdiv.setAttribute("id", divIdName);
//	newdiv.innerHTML = " <label id=\"label" + num + "\" class=\"editable\" title=\"" + dblClickTooltip + "\">" + labelName + " " + num + "</label> ";
//	newdiv.innerHTML += "(<input type=\"checkbox\" id=\"privField" + num + "\" name=\"privField" + num + "\">" + privFieldLabel + "</input> ";
//	newdiv.innerHTML += ", &nbsp;&nbsp; <a href=\"#\" class=\"verysmall red awesome delConfirm" + num + "\">" + removeLabel + "</a> )";
//	newdiv.innerHTML += "<script> $(document).ready(function() { $('a.delConfirm" + num + "').click(function() { removeEvent('" + divIdName + "'); return false; }); $('a.delConfirm" + num + "').confirm(); }); </script> ";	
//	newdiv.innerHTML += "<input class=\"extra_large\" type='file' name='field" + num + "'>";
//	newdiv.innerHTML += "<input type=\"hidden\" value=\"" + labelName + " " + num + "\" id=\"label" + num + "\" name=\"label" + num + "\"/>";
//	ni.appendChild(newdiv);
//	setClickable(document.getElementById("label" + num), num);
//}

function addShortTextEvent(idContainer) {
	addInputEvent(idContainer, msg["NewItem.DefaultShortTextLabel"], "field", "");
}

function addWwwEvent(idContainer) {
	addInputEvent(idContainer, msg["NewItem.DefaultWwwLabel"], "www", "{url:true}");
}

function addEmailEvent(idContainer) {
	addInputEvent(idContainer, msg["NewItem.DefaultEmailLabel"], "email", "{email:true}");
}

function addInputEvent(idContainer, defaultLabel, fieldname, additionalClasses)
{
	var ni = document.getElementById(idContainer);
	var numi = document.getElementById("fieldCounter");
	var num = (document.getElementById("fieldCounter").value -1)+ 2;
	numi.value = num;
	var divIdName = "my" + num + "Div";
	var newdiv = document.createElement("li");
	newdiv.setAttribute("id", divIdName);
	
	newdiv.appendChild(createFieldnameLabel(num, defaultLabel));
	newdiv.appendChild(createOptions(num, divIdName));
	newdiv.appendChild(document.createElement("br"));
	
	var v = document.createElement("input");
	v.setAttribute("type", "text");
	v.setAttribute("name", fieldname + num);
//	if (additionalConstraints) {
		v.setAttribute("class", "wlInput " + additionalClasses);
//	} else {
//		v.setAttribute("class", "wlInput");
//	}
	newdiv.appendChild(v);

	ni.appendChild(newdiv);
	setClickable(document.getElementById("label" + num), num);
}

function addNumberEvent(idContainer)
{
	var ni = document.getElementById(idContainer);
	var numi = document.getElementById("fieldCounter");
	var num = (document.getElementById("fieldCounter").value -1)+ 2;
	numi.value = num;
	var divIdName = "my" + num + "Div";
	var newdiv = document.createElement("li");
	newdiv.setAttribute("id", divIdName);

	newdiv.appendChild(createFieldnameLabel(num, msg["NewItem.DefaultNumberLabel"]));
	newdiv.appendChild(createOptions(num, divIdName));
	newdiv.appendChild(document.createElement("br"));

	var v = document.createElement("input");
	v.setAttribute("type", "text");
	v.setAttribute("name", "number" + num);
	v.setAttribute("class", "wlInput {digits: true, messages:{digits: '" + msg["NewItem.OnlyDigits"] + "'}}");
	newdiv.appendChild(v);

	ni.appendChild(newdiv);
	setClickable(document.getElementById("label" + num), num);
}

function addDateEvent(idContainer)
{
	var ni = document.getElementById(idContainer);
	var numi = document.getElementById("fieldCounter");
	var num = (document.getElementById("fieldCounter").value -1)+ 2;
	numi.value = num;
	var divIdName = "my" + num + "Div";
	var newdiv = document.createElement("li");
	newdiv.setAttribute("id", divIdName);

	newdiv.appendChild(createFieldnameLabel(num, msg["NewItem.DefaultDateLabel"]));
	newdiv.appendChild(createOptions(num, divIdName));
	newdiv.appendChild(document.createElement("br"));
	
	var v = document.createElement("input");
	v.setAttribute("type", "text");
	v.setAttribute("id", "datepicker" + num);
	v.setAttribute("name", "datepicker" + num);
	v.setAttribute("autocomplete", "off");
	v.setAttribute("class", "{date: true, messages: {date: '" + msg["NewItem.DateFormatInvalid"] + "'}}");
	newdiv.appendChild(v);
	
	v = document.createElement("script");
	v.setAttribute("type", "text/javascript");
	v.innerHTML = "$(function() { $(\"#datepicker" + num + "\").datepicker({ numberOfMonths: 1, dateFormat: 'yy-mm-dd'}); }); ";
	newdiv.appendChild(v);

	ni.appendChild(newdiv);
	setClickable(document.getElementById("label" + num), num);
}

function addCheckboxEvent(idContainer)
{
	var ni = document.getElementById(idContainer);
	var numi = document.getElementById("fieldCounter");
	var num = (document.getElementById("fieldCounter").value -1)+ 2;
	numi.value = num;
	var divIdName = "my" + num + "Div";
	var newdiv = document.createElement("li");
	newdiv.setAttribute("id", divIdName);

	var v = createFieldnameLabel(num, msg["NewItem.DefaultCheckboxLabel"]);
	newdiv.appendChild(v);
//	newdiv.appendChild(document.createElement("br"));
	
	v = document.createElement("input");
	v.setAttribute("type", "checkbox");
	v.setAttribute("id", "checkbox" + num);
	v.setAttribute("name", "checkbox" + num);
	newdiv.appendChild(v);
	
	newdiv.appendChild(createOptions(num, divIdName));
	
	v = document.createElement("input");
	v.setAttribute("type", "hidden");
	v.setAttribute("id", "hiddenCheckbox" + num);
	v.setAttribute("name", "hiddenCheckbox" + num);
	v.setAttribute("value", "0");
	newdiv.appendChild(v);

	ni.appendChild(newdiv);
	setClickable(document.getElementById("label" + num), num);
	
	// connecting label with checkbox
	var l = document.getElementById("label" + num);
	l.setAttribute("for", "checkbox" + num);
//	alert(v);
}

function removeEvent(divNum)
{
//	alert(divNum);
	var d = document.getElementById("addingItemSortable");
//	alert(d);
	var olddiv = document.getElementById(divNum);
//	alert(olddiv.id);
	d.removeChild(olddiv);
}

// probably obsolete function
function removeEvent2(divNum)
{
//	alert(divNum);
	var d = document.getElementById("addingItemSortable");
//	alert(d);
	var exFields = document.getElementById("existingFields");
//	alert(exFields.id);
	var olddiv2 = document.getElementById(divNum);
//	alert(olddiv2.id);
	exFields.removeChild(olddiv2);
}

// disables ENTER key when changing field's name
function disableEnterKey(e)
{
     var key;

     if(window.event)
          key = window.event.keyCode;     //IE
     else
          key = e.which;     //firefox

     if(key == 13)
          return false;
     else
          return true;
}
