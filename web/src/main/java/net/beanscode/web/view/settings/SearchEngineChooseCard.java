package net.beanscode.web.view.settings;

import java.io.IOException;

import org.rendersnake.HtmlCanvas;

import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.CardPanel;


public class SearchEngineChooseCard extends CardPanel {

	public SearchEngineChooseCard() {
		
	}
	
	public SearchEngineChooseCard(BeansComponent parent) {
		super(parent);
	}

	@Override
	protected void renderTitle(HtmlCanvas html) throws IOException {
		html
			.span()
				.content("Change search engine");
	}

	@Override
	protected void renderContent(HtmlCanvas html) throws IOException {
		html
			.render(new SearchIndexChoosePanel(this));
	}

	@Override
	protected void renderFooter(HtmlCanvas html) throws IOException {
		
	}
	

}
