package net.beanscode.model.backup;

import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.stream.JsonReader;

import kotlin.NotImplementedError;
import net.beanscode.model.BeansConst;
import net.beanscode.model.BeansSettings;
import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.notebook.NotebookEntry;
import net.beanscode.model.notebook.NotebookEntryFactory;
import net.beanscode.model.notebook.NotebookFactory;
import net.hypki.libs5.db.db.BackupUtils;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.collections.ArrayUtils;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.date.Watch;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.file.FileUtils;
import net.hypki.libs5.utils.file.LazyFileIterator;
import net.hypki.libs5.utils.json.GsonStreamReader;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.utils.AssertUtils;
import net.hypki.libs5.weblibs.settings.Setting;
import net.hypki.libs5.weblibs.settings.SettingFactory;

public class BackupManager {
	
	public static Iterable<FileExt> iterateBackupFiles() throws IOException {
		Setting backupPath = SettingFactory.getSetting(BeansSettings.SETTING_BACKUP_PATH);
		
		return new LazyFileIterator(new FileExt(backupPath.getValue("path").getValueAsString()), false, true, false, null);
	}
	
	public static void restoreKeyspace(final String keyspace, final RestoreJob restoreJob) throws IOException {
		Watch w = new Watch();
		
		final Setting backupPath = SettingFactory.getSetting(BeansSettings.SETTING_BACKUP_PATH);
		final String inputFile = new FileExt(backupPath.getValue("path").getValueAsString()).getAbsolutePath() 
				+ "/" + restoreJob.getRestoreFile();
		
		LibsLogger.debug(BackupUtils.class, "Restoring backup for keyspace ", keyspace, " from the file ", inputFile);
		
		try {
			if (restoreJob.isRestoreNotebooks()) {
				restoreNotebooks(inputFile, keyspace, restoreJob);
//				restoreNotebookEntry(inputFile, keyspace, restoreJob);
			} else
				throw new NotImplementedError("Only restoring Notebooks is implemented for now");
			
			LibsLogger.debug(BackupUtils.class, "Backup for a keyspace ", keyspace, " restored from the file ",
					inputFile, " in ", w.toString());
		} catch (Throwable t) {
			LibsLogger.error(BackupUtils.class, "Cannot restore backup " + inputFile, t);
			throw new IOException("Cannot restore backup " + inputFile, t);
		}
	}
	
	private static void restoreNotebooks(final String inputFile, 
			final String keyspace,
			final RestoreJob restoreJob) throws FileNotFoundException, IOException, ValidationException {
		Map<UUID, UUID> notebookOldToNew = new HashMap<UUID, UUID>();
	
		// restoring notebooks
		try (JsonReader reader = new JsonReader(new FileReader(inputFile))) {

			// beginning of backup (JsonObject)
            reader.beginObject();

            while (reader.hasNext()) {

            	// table name (key)
            	final String currentTableName = reader.nextName();
            	            	
            	// table rows (JsonArray)
            	reader.beginArray();
            	
            	while (reader.hasNext()) {
            		// reading rows (JsonObject) from JsonArray
            		Row r = JsonUtils.getGson().fromJson(reader, Row.class);
            		
            		if (!currentTableName.equalsIgnoreCase(Notebook.COLUMN_FAMILY))
            			continue;
            		
            		Notebook n = JsonUtils.fromJson(r.getAsString(DbObject.COLUMN_DATA), Notebook.class);
            		if (restoreJob.getRestoreStrategy() == RestoreStrategy.CLONE_OBJECTS) {
            			UUID newId = UUID.random();
            			notebookOldToNew.put(n.getId(), newId);
            			n.setName((restoreJob.isWithPrefixDefined() ? restoreJob.getWithPrefix() : "") 
            					+ n.getName() 
            					+ (restoreJob.isWithSuffixDefined() ? restoreJob.getWithSuffix() : ""));
            			n.setId(newId);
            			n.getEntries().clear();
            		} else if (restoreJob.getRestoreStrategy() == RestoreStrategy.CREATE_OBJECTS_IF_NOT_EXIST) {
            			Notebook nFromDb = Notebook.getNotebook(n.getId());
            			if (nFromDb != null)
            				continue;
            		} else if (restoreJob.getRestoreStrategy() == RestoreStrategy.OVERRIDE_OBJECTS) {
            			// do nothing
            		} else
            			throw new NotImplementedError("Restore strategy " + restoreJob.getRestoreStrategy() + " not implemented");
            		
            		n.setUserId(restoreJob.getUserId());            		
            		n.save();
            	}
            	
            	// end of table rows
            	reader.endArray();
            }

            reader.endObject();
		}
		
		// restoring notebooks' entries
		Notebook defaultNotebook = null;
		try (JsonReader reader = new JsonReader(new FileReader(inputFile))) {

			// beginning of backup (JsonObject)
            reader.beginObject();

            while (reader.hasNext()) {

            	// table name (key)
            	final String currentTableName = reader.nextName();
            	            	
            	// table rows (JsonArray)
            	reader.beginArray();
            	
            	while (reader.hasNext()) {
            		// reading rows (JsonObject) from JsonArray
            		Row r = JsonUtils.getGson().fromJson(reader, Row.class);
            		
            		if (!currentTableName.equalsIgnoreCase(NotebookEntry.COLUMN_FAMILY))
            			continue;
            		
            		NotebookEntry ne = JsonUtils.fromJson(r.getAsString(DbObject.COLUMN_DATA), NotebookEntry.class);
            		
            		if (restoreJob.getRestoreStrategy() == RestoreStrategy.CLONE_OBJECTS) {
            			ne.setNotebookId(notebookOldToNew.get(ne.getNotebookId()));
            		} else if (restoreJob.getRestoreStrategy() == RestoreStrategy.CREATE_OBJECTS_IF_NOT_EXIST) {
            			NotebookEntry neFromDb = NotebookEntryFactory.getNotebookEntry(ne.getId());
            			if (neFromDb != null)
            				continue;
            		} else if (restoreJob.getRestoreStrategy() == RestoreStrategy.OVERRIDE_OBJECTS) {
            			// do nothing
            		} else
            			throw new NotImplementedError("Restore strategy " + restoreJob.getRestoreStrategy() + " not implemented");
            		
            		if (nullOrEmpty(ne.getNotebookId())) {
            			LibsLogger.error(BackupManager.class, "Notebook ID not set for the entry: " + ne.getData());
            			if (defaultNotebook == null) {
            				defaultNotebook = new Notebook(restoreJob.getUserId(), 
            						(restoreJob.isWithPrefixDefined() ? restoreJob.getWithPrefix() : "") 
                					+ "DEFAULT NOTEBOOK FOR ORPHANED ENTRIES" 
                					+ (restoreJob.isWithSuffixDefined() ? restoreJob.getWithSuffix() : ""));
            				defaultNotebook.save();
            			}
            			ne.setNotebookId(defaultNotebook.getId());
            		} 
            		
            		ne.setUserId(restoreJob.getUserId());
            		ne.save();
            	}
            	
            	// end of table rows
            	reader.endArray();
            }

            reader.endObject();
		}
		
		LibsLogger.info(BackupManager.class, "Restoring notebooks finished");
	}
	
	@Deprecated
	public static void backupNotebooks(final Writer writer, final UUID ... notebookIds) throws IOException {
		List<UUID> ids = (List<UUID>) ArrayUtils.toList(notebookIds);
		backupNotebooks(writer, ids);
	}
	
	@Deprecated
	public static void backupNotebooks(final Writer writer, final List<UUID> notebookIds) throws IOException {
		writer.append("[\n");
		boolean first = true;
		for (UUID id : notebookIds) {
			final Notebook notebook = Notebook.getNotebook(id);

			if (!first) {
				writer.append(",\n");
				first = false;
			}
			
			JsonElement jo = JsonUtils.toJson(notebook);
			jo.getAsJsonObject().addProperty("type", Notebook.class.getName());
			writer.append(jo.toString());
			
			for (NotebookEntry entry : notebook.iterateEntries()) {
				writer.append(",\n");
				jo = JsonUtils.toJson(entry);
				jo.getAsJsonObject().addProperty("type", entry.getClass().getName());
				writer.append(jo.toString());
			}
		}
		writer.append("]");
		
		writer.close();
	}
	
	@Deprecated
	public static void backupNotebooks(final UUID userId) throws IOException {
		new FileExt("backups").mkdirs();
		
		final String filename = "backups/Notebooks-" + SimpleDate.now().toStringISO() + ".bac";
		FileWriter fw = new FileWriter(filename);
		
		fw.append("[\n");
		boolean first = true;
		for (Notebook notebook : NotebookFactory.iterateNotebooks(userId)) {
			if (!notebook.getUserId().equals(userId))
				continue;

			if (!first) {
				fw.append(",\n");
				first = false;
			}
			
			JsonElement jo = JsonUtils.toJson(notebook);
			jo.getAsJsonObject().addProperty("type", Notebook.class.getName());
			fw.append(jo.toString());
			
			for (NotebookEntry entry : notebook.iterateEntries()) {
				try {
					if (entry != null) {
						fw.append(",\n");
						jo = JsonUtils.toJson(entry);
						jo.getAsJsonObject().addProperty("type", entry.getClass().getName());
						fw.append(jo.toString());
					}
				} catch (Exception e) {
					LibsLogger.error(BackupManager.class, "Cannot backup Entry " + entry, e);
				}
			}
		}
		fw.append("]\n");
		
		fw.close();
	}

	@Deprecated
	public static void restoreNotebooks(UUID userId, File file) throws IOException {
		AssertUtils.assertTrue(file.exists(), "File with backup ", file.getAbsolutePath(), " does not exist, restoring Notebooks failed");
		
		JsonElement je = JsonUtils.parseJson(FileUtils.readString(file));
		JsonArray ja = je.getAsJsonArray();
		
		for (JsonElement j : ja) {
			
			if (j.isJsonObject() && j.getAsJsonObject().get("type") != null) {
				String type = j.getAsJsonObject().get("type").getAsString();
				try {
					if (StringUtilities.notEmpty(type)) {
						Class<? extends NotebookEntry> clazz = (Class<? extends NotebookEntry>) Class.forName(type);
						NotebookEntry entry = JsonUtils.fromJson(j, clazz);
						entry.setUserId(userId);
						entry.save();
						continue;
					}
				} catch (Exception e) {
					LibsLogger.debug(BackupManager.class, "Cannot restore Plot", e);
				}
			}
			
			try {
				Notebook notebook = JsonUtils.fromJson(j, Notebook.class);
				if (notebook != null) {
					notebook.setUserId(userId);
					notebook.save();
					continue;
				}
			} catch (Exception e) {
				LibsLogger.debug(BackupManager.class, "It is not a Notebook");
			}
			
			if (j.isJsonNull())
				continue;
			
			throw new IOException("Don't know how to parse " + j);
		}
	}

	public static void restoreBackup(String file) throws IOException, ValidationException {
		// restoring notebooks
		GsonStreamReader reader = new GsonStreamReader(file);
		reader.beginArray();
		
		while (reader.hasNext()) {
			JsonElement je = reader. next();
			
			if (je.isJsonObject()) {
				if (je.getAsJsonObject().get("name").getAsString().equals("beansdev3___notebook")) {
					try {
						Notebook notebook = JsonUtils.fromJson(je.getAsJsonObject().get("value").getAsString(), Notebook.class);
						notebook.save();
					} catch (Exception e) {
						LibsLogger.error(BeansConst.class, "Cannot save notebook from dump", e);
					}
				}
			}
		}
		
		reader.endArray();
		reader.close();
		
		// read notebook entries
		reader = new GsonStreamReader(file);
		reader.beginArray();
		
		// restoring notebooks
		while (reader.hasNext()) {
			JsonElement je = reader. next();
			
			if (je.isJsonObject()) {
				if (je.getAsJsonObject().get("name").getAsString().equals("beansdev3___notebookentry")) {
					try {
						JsonElement je2 = JsonUtils.parseJson(je.getAsJsonObject().get("value").getAsString());
						if (je2.isJsonObject() && je2.getAsJsonObject().get("type") != null) {
							String type = je2.getAsJsonObject().get("type").getAsString();
							NotebookEntry entry = (NotebookEntry) JsonUtils.fromJson(je2, Class.forName(type));
							entry.save();
						}
					} catch (Exception e) {
						LibsLogger.error(BeansConst.class, "Cannot save notebookentry from dump", e);
					}
				}
			}
		}
		
		reader.endArray();
		reader.close();
	}
}
