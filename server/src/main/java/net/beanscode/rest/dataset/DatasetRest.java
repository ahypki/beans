package net.beanscode.rest.dataset;

import static net.hypki.libs5.db.db.weblibs.ValidationUtils.validateTrue;
import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.Response.ResponseBuilder;

import mjson.Json;
import net.beanscode.model.BeansConst;
import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.DatasetFactory;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.rights.Rights;
import net.beanscode.model.utils.MarkdownUtils;
import net.beanscode.rest.BeansRest;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.search.SearchResults;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.json.JsonWrapper;
import net.hypki.libs5.utils.string.Meta;
import net.hypki.libs5.utils.string.RegexUtils;
import net.hypki.libs5.utils.url.HttpResponse;
import net.hypki.libs5.utils.utils.NumberUtils;
import net.hypki.libs5.weblibs.SearchManager;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.user.Allowed;

import com.google.gson.JsonObject;

@Path("api/dataset")
public class DatasetRest extends BeansRest {
	
	@POST
	@Path("meta/add")
	public void addMeta(@Context SecurityContext sc, 
			@FormParam("datasetId") UUID datasetId,
			@FormParam("metaName") String metaName,
			@FormParam("metaValue") String metaValue,
			@FormParam("metaDesc") String metaDesc) throws IOException, ValidationException {
		Dataset ds = DatasetFactory.getDataset(datasetId);
		
		validateNotFound(ds != null, "Dataset is not found");
		validateUnauthorized(isAdmin(sc) || ds.isWriteAllowed(getUserId(sc)), "Permission denied");

		ds
			.addMeta(metaName, metaValue, metaDesc)
			.save();
	}
	
	@DELETE
	@Path("meta")
	public void deleteMeta(@Context SecurityContext sc, 
			@QueryParam("datasetId") UUID datasetId,
			@QueryParam("metaName") List<String> metaNames) throws IOException, ValidationException {
		Dataset ds = DatasetFactory.getDataset(datasetId);
		
		validateNotFound(ds != null, "Dataset is not found");
		validateUnauthorized(isAdmin(sc) || ds.isWriteAllowed(getUserId(sc)), "Permission denied");

		for (String toRemove : metaNames) {
			ds.getMeta().remove(toRemove);
		}
		
		ds.save();
	}
	
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getDatasetById(@Context SecurityContext sc, 
			@QueryParam("dataset-id") String datasetId) throws IOException, ValidationException {
		Dataset ds = DatasetFactory.getDataset(datasetId);
		
		validateNotFound(ds != null, "Dataset is not found");
		validateUnauthorized(isAdmin(sc) || ds.isReadAllowed(getUserId(sc)), "Permission denied");
				
		return toResponse(ds);
	}
	
	@GET
	@Path("query")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getDatasetsByQuery(@Context SecurityContext sc, 
			@QueryParam("query") String query,
			@QueryParam("from") int from,
			@QueryParam("size") int size
			) throws IOException, ValidationException {
		validateUnauthorized(getUserId(sc) != null, "Permission denied");
		
		size = size == 0 ? 10 : size;
		
		SearchResults<Dataset> datasets = DatasetFactory.searchDatasets(getUserId(sc), query, from, size);
		
		return Response
				.ok()
				.entity(Json
						.object()
						.set("maxHits", datasets.maxHits())
						.set("datasets", datasets.getObjectsAsJson())
						.toString())
				.build();
	}
	
	@POST
	@Path("create")
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(@Context SecurityContext sc, 
			@FormParam("name") String name,
			MultivaluedMap<String, String> meta) throws IOException, ValidationException {
		validateUnauthorized(getUserId(sc) != null, "Permission denied");
		
		meta.remove("name");
		meta.remove("id");
		
		Dataset ds = new Dataset(getSessionBean(sc).getUserId(), 
				BeansConst.DEFAULT_DATASET_NAME + " (" + SimpleDate.now().toStringHuman() + ")");

		// meta params
		if (meta != null && meta.size() > 0)	
			ds.addMeta(meta);
		
		// name
		if (notEmpty(name))
			ds.setName(name);

		ds.save();

		LibsLogger.debug(DatasetRest.class, "New dataset '", ds.getName(), "' created with id " + ds.getId());
		
		return toResponse(ds);
	}
	
	@POST
	@Path("set")
	public void editDataset(@Context SecurityContext sc,
			@FormParam("id") String id,
			@FormParam("name") String newName,
			@FormParam("description") String newDescription,
			MultivaluedMap<String, String> meta) throws IOException, ValidationException {
		Dataset ds = DatasetFactory.getDataset(id);
		
		validateNotFound(ds != null, "Dataset is not found");
		validateUnauthorized(isAdmin(sc) || ds.isWriteAllowed(getUserId(sc)), "Permission denied");
		
		meta.remove("id");
		meta.remove("name");
		meta.remove("description");

		// meta params
		if (meta != null && meta.size() > 0)
			ds.addMeta(meta);

		if (notEmpty(newName))
			ds.setName(newName);
		
		if (notEmpty(newDescription))
			ds.setDescription(newDescription);

		ds.save();

		LibsLogger.debug(DatasetRest.class, "Dataset '", ds.getName(), "' updated with id " + ds.getId());
	}

	@DELETE
	@Path("delete")
	public void deleteDataset(@Context SecurityContext sc,
			@QueryParam("datasetId") List<UUID> datasetId) throws IOException, ValidationException {
		// validation 
		for (UUID dsId : datasetId) {
			Dataset ds = DatasetFactory.getDataset(dsId);
			
			if (ds != null)
				validateUnauthorized(isAdmin(sc) || ds.isWriteAllowed(getUserId(sc)), "Permission denied");
		}
		
		// actual removal
		for (UUID dsId : datasetId) {
			Dataset ds = DatasetFactory.getDataset(dsId);
			
			if (ds != null)
				ds.remove();
			else {
				// at least removing from index
				SearchManager.removeObject(DbObject.buildCombinedKey(WeblibsConst.KEYSPACE_LOWERCASE, 
						Dataset.COLUMN_FAMILY.toLowerCase(), 
						dsId.getId(), 
						DbObject.COLUMN_DATA), 
						Dataset.COLUMN_FAMILY.toLowerCase());
			}
		}
	}
	
	@DELETE
	@Path("clear")
	public void clearDataset(@Context SecurityContext sc,
			@QueryParam("id") List<UUID> datasetId) throws IOException, ValidationException {
		// validation 
		for (UUID dsId : datasetId) {
			Dataset ds = DatasetFactory.getDataset(dsId);
			
			if (ds != null)
				validateUnauthorized(isAdmin(sc) || ds.isWriteAllowed(getUserId(sc)), "Permission denied");
		}
		
		// actual removal
		for (UUID dsId : datasetId) {
			Dataset ds = DatasetFactory.getDataset(dsId);
			
			if (ds != null)
				ds.clear();
		}
	}
	
	@GET
	@Path("isReadAllowed")
	@Produces(MediaType.APPLICATION_JSON)
	public Response isReadAllowed(@Context SecurityContext sc,
			@QueryParam("userId") UUID userId, 
			@QueryParam("tableId") UUID datasetId) throws IOException {
		Dataset ds = DatasetFactory.getDataset(datasetId);
		
		validateNotFound(ds != null, "Dataset " + datasetId + " not found");

		return Response
				.ok()
				.entity(Json
						.object()
						.set("readAllowed", ds.isWriteAllowed(userId))
						.toString())
				.build();
	}
	
	@GET
	@Path("isWriteAllowed")
	@Produces(MediaType.APPLICATION_JSON)
	public Response isWriteAllowed(@Context SecurityContext sc,
			@QueryParam("userId") UUID userId, 
			@QueryParam("datasetId") UUID datasetId) throws IOException {
		Dataset ds = DatasetFactory.getDataset(datasetId);
		
		validateNotFound(ds != null, "Dataset " + datasetId + " not found");
		
		return Response
				.ok()
				.entity(Json
						.object()
						.set("writeAllowed", ds.isWriteAllowed(userId))
						.toString())
				.build();
	}
	
	@GET
	@Path("isDeleteAllowed")
	@Produces(MediaType.APPLICATION_JSON)
	public Response isDeleteAllowed(@Context SecurityContext sc,
			@QueryParam("userId") UUID userId, 
			@QueryParam("datasetId") UUID datasetId) throws IOException {
		Dataset ds = DatasetFactory.getDataset(datasetId);
		
		validateNotFound(ds != null, "Dataset " + datasetId + " not found");
		
		return Response
				.ok()
				.entity(Json
						.object()
						.set("deleteAllowed", ds.isWriteAllowed(userId))
						.toString())
				.build();
	}
	
	private Response toResponse(Dataset ds) {		
		Json dsJson = Json
				.object()
				.set("id", Json.object().set("id", ds.getId().getId()))
				.set("userId", Json.object().set("id", ds.getUserId().getId()))
				.set("name", ds.getName())
				.set("description", ds.getDescription())
				.set("descriptionHtml", MarkdownUtils.toHtml(ds.getDescription()))
				.set("creationMs", ds.getCreationMs());
				;
		
		Json dsMeta = Json.object();
		for (Meta meta : ds.getMeta()) {			
			dsMeta.set(meta.getName(), Json
					.object()
					.set("name", meta.getName())
					.set("value", meta.getValue() != null ? meta.getValue().toString() : null)
					.set("description", meta.getDescription()));
		}
		
		dsJson
			.set("meta", Json.object().set("meta", dsMeta));
		
		return Response
				.ok()
				.entity(dsJson.toString())
				.build();
	}
}
