package net.beanscode.model.notebook;

import java.io.IOException;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.jobs.Job;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class NotebookIndexJob extends Job {
	
	@Expose
	@NotNull
	@NotEmpty
	private UUID userId = null;
	
	@Expose
	@NotNull
	@NotEmpty
	private UUID notebookId = null;
	
	@Expose
	@NotNull
	@NotEmpty
	private boolean isReindexEntries = false;
	
	public NotebookIndexJob() {
		
	}
	
	public NotebookIndexJob(UUID userId, UUID notebookId) {
		setUserId(userId);
		setNotebookId(notebookId);
	}
	
	public NotebookIndexJob(UUID userId, UUID notebookId, boolean isReindexEntries) {
		setUserId(userId);
		setNotebookId(notebookId);
		setReindexEntries(isReindexEntries);
	}

	@Override
	public void run() throws IOException, ValidationException {
		Notebook notebook = Notebook.getNotebook(getNotebookId());
		
		if (notebook == null) {
			LibsLogger.error(NotebookIndexJob.class, String.format("Cannot find notebook %s for user %s. Consuming job...", getNotebookId().getId(), getUserId().getId()));
			return;
		}
		
		notebook.index();
		LibsLogger.debug(NotebookIndexJob.class, "Notebook " + getNotebookId() + " indexed");
	}

	public UUID getNotebookId() {
		return notebookId;
	}

	public void setNotebookId(UUID notebookId) {
		this.notebookId = notebookId;
	}

	public UUID getUserId() {
		return userId;
	}

	public void setUserId(UUID userId) {
		this.userId = userId;
	}

	public boolean isReindexEntries() {
		return isReindexEntries;
	}

	public NotebookIndexJob setReindexEntries(boolean isReindexEntries) {
		this.isReindexEntries = isReindexEntries;
		return this;
	}
}
