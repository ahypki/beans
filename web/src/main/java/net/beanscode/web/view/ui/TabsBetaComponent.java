package net.beanscode.web.view.ui;

import static org.rendersnake.HtmlAttributesFactory.class_;

import java.io.IOException;

import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.op.OpList;

import org.rendersnake.HtmlCanvas;
import org.rendersnake.Renderable;

public abstract class TabsBetaComponent extends BeansComponent {
	
//	private String id = "tabs-" + UUID.random().getId();
	
	public TabsBetaComponent() {
		
	}

	public TabsBetaComponent(Component parent) {
		super(parent);
	}
	
	public abstract int getTabsCount();
	public abstract String getTabName(int tabIndex);
	public abstract Renderable getTabHtml(int tabIndex) throws IOException;
	
	protected String getAdditionalTabHtmlClasses() {
		return "";
	}
	
	protected OpList getAdditionalOpListOnTabClick(int tabIndex) throws IOException {
		return null;
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.div(class_("card card-primary card-outline card-outline-tabs"))
			.div(class_("card-header p-0 border-bottom-0 beans-tabs-component " + getAdditionalTabHtmlClasses())
					.id(getId()))
				.ul(class_("nav nav-tabs")
						.id("tabs-" + getId())
						.role("tablist"))
					.render(new Renderable() {
						@Override
						public void renderOn(HtmlCanvas html) throws IOException {
							for (int i = 0; i < getTabsCount(); i++) {
								html
									.li(class_("nav-item"))
										.a(class_("nav-link " + (i == 0 ? "active" : ""))
												.id("tab-" + getId() + "-" + i)
												.data("toggle", "pill")
												.href("#panel-" + getId() + "-" + i)
												.role("tab")
												.add("aria-controls", "#panel-" + getId() + "-" + i)
												.add("aria-selected", i == 0 ? "true" : "false")
												.onClick(new OpList()
		//											.add(new OpHide("#" + getId() + " .tab-panel"))
		//											.add(new OpRemoveClass("#" + getId() + " li.tab", "active"))
		//											.add(new OpAddClass("#" + getId() + " #tab-" + getId() + "-" + i, "active"))
		//											.add(new OpShow("#" + getId() + " #panel-" + getId() + "-" + i))
													.add(getAdditionalOpListOnTabClick(i))
													.toStringOnclick()
												))
											.content(getTabName(i), false)
									._li();
							}
						}
					})
				._ul()
			._div()
			.div(class_("card-body"))
				.div(class_("tab-content")
						.id("tabs-" + getId() + "-tabContent"))
					.render(new Renderable() {
						@Override
						public void renderOn(HtmlCanvas html) throws IOException {
							for (int i = 0; i < getTabsCount(); i++) {
								html
									.div(class_("tab-pane " + (i == 0 ? " show active " : ""))
											.id("panel-" + getId() + "-" + i)
											.role("tabpanel")
											.add("aria-labelledby", "#panel-" + getId() + "-" + i + "-tab")
//											.add("style", i > 0 ? "display: none;" : "")
											)
										.render(getTabHtml(i))
									._div();
							}
						}
					})
				._div()
			._div()
			._div()
			;
	}
}
