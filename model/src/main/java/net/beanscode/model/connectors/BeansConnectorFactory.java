package net.beanscode.model.connectors;

import java.io.IOException;
import java.util.Iterator;

import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.ResultsIter;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.weblibs.WeblibsConst;

public class BeansConnectorFactory {

	public static BeansConnector getBeansConnector(UUID tableId, String connectorId) throws IOException {
		return DbObject.getDatabaseProvider().get(WeblibsConst.KEYSPACE, 
				BeansConnector.COLUMN_FAMILY, 
				new C3Where()
						.addClause(new C3Clause(DbObject.COLUMN_PK, ClauseType.EQ, 
								tableId + "-" + connectorId)), 
				DbObject.COLUMN_DATA, 
				BeansConnector.class);
	}

	public static Iterable<BeansConnector> iterateConnectors(UUID tableId) {
		return new Iterable<BeansConnector>() {
			@Override
			public Iterator<BeansConnector> iterator() {
				return new Iterator<BeansConnector>() {
					private ResultsIter rowsIter = null;
					
					private boolean initiated = false;
					
					private BeansConnector nextConnector = null;
					
					@Override
					public BeansConnector next() {
						if (!initiated)
							hasNext();
						return nextConnector;
					}
					
					@Override
					public boolean hasNext() {
						if (!initiated) {
							rowsIter = new ResultsIter(WeblibsConst.KEYSPACE, 
									BeansConnector.COLUMN_FAMILY, 
									new String[]{DbObject.COLUMN_DATA}, 
									new C3Where()
										.addClause(new C3Clause(BeansConnector.COLUMN_TABLEID, ClauseType.EQ, tableId.getId())));
							initiated = true;
						}
						
						while (rowsIter.hasNext()) {
							Row row = rowsIter.next();
							
							if (row.contains(DbObject.COLUMN_DATA)) {
								nextConnector = JsonUtils.fromJson((String) row.getColumns().get(DbObject.COLUMN_DATA), 
										BeansConnector.class);
								return true;
							}
						}
						
						return false;
					}
				};
			}
		};
	}
}
