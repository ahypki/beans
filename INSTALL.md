Installation notes for `BEANS` software.

## Table of contents

[[_TOC_]]



## Installation

Installation has been tested on Linux (Ubuntu 22.04), but it should work in any other operating system supporting Java.


## Requirements

- git
- Java
- Apache Maven



#### Install Java (version 8)

- `BEANS` will be updated to Java 21 version in the near future. For now please use `Java version 8`
- go to [Java SE 8 Archive Downloads](https://www.oracle.com/java/technologies/javase/javase8-archive-downloads.html) and download `jdk-8u202-linux-x64.tar.gz` file
- unpack the file `jdk-8u202-linux-x64.tar.gz` to some folder. In this installation procedure we will extract `Java 8` to the folder `$HOME/programs/jdk8`
- add downloaded Java binaries to the `PATH` variable with:

	`export PATH=\$HOME/programs/jdk8/bin:$PATH`

- test if Java was successfully installed:

	`java -version`

You should see something like this:

```bash
java version "1.8.0_202"
Java(TM) SE Runtime Environment (build 1.8.0_202-b08)
Java HotSpot(TM) 64-Bit Server VM (build 25.202-b08, mixed mode)
```

#### Install `libs5` libraries

Go to [Gitlab libs5 project](https://gitlab.com/ahypki/libs5) and install it.

#### Install BEANS

1. Download the code:

```bash
git clone https://gitlab.com/beans-code/beans.git
```

2. First, install additional libraries

For _HDF5_ files:

```bash
cd ./beans/bin/
chmod +x jhdf5-install
./jhdf5-install
```

And _Apache Tez parser_ library:

```bash
cd ./beans/bin/
chmod +x tez-history-parser-install
./tez-history-parser-install
```

3. Install `Jawk` library

```bash
git clone https://github.com/hoijui/Jawk.git
cd Jawk
mvn -q install -Dmaven.test.skip=true
```

Remark 1: If the compilations fails because of the error "Source option 6 is no longer supported. Use 7 or later." edit `pom.xml` file and change:

```xml
<java.old.version>6</java.old.version>
```

to:

```xml
<java.old.version>7</java.old.version>
```

and compile again.

Remark 2: If the above fix does not work, then install Jawk from [BEANS Jawk](https://gitlab.com/beans-code/Jawk) which has already updated `pom.xml` file.
    
4. Install `BEANS`

Install `BEANS`, skip the tests, with the command:

```bash
cd ./beans/
mvn clean install -Dmaven.test.skip=true
```

#### Install `BEANS` plugins 

1. Install _beans-plugin-text_ (https://gitlab.com/beans-code/beans-plugin-text)

```bash
git clone https://gitlab.com/beans-code/beans-plugin-text.git
cd ./beans-plugin-text/
mvn clean install
```

#### Install `BEANS bulk`

`BEANS bulk` combines `BEANS` and all `BEANS plugins` together into one `jar` file for simplicity:

```bash
git clone https://gitlab.com/beans-code/beans-bulk.git
cd ./beans-bulk/
mvn clean install
```

## Start BEANS

Go to `beans-bulk` and type:

```bash
java -jar target/beans-bulk-1.0.0.jar
```

## Other installation notes

### Apache Cassandra 

`Apache Cassandra` is needed only for multinode, multi TB setup

1. Download Apache Cassandra

Code is tested with Apache Cassandra version 2.2.4. It should work with any 2.x version, but the version 2.2.4 is recommended for now. 

https://archive.apache.org/dist/cassandra/2.2.4/

Unpack the folder and change in _./conf/cassandra.yaml_ values:

	* native_transport_port to 29042
	* storage_port to 27000
	* ssl_storage_port to 27001
	* rpc_port to 29160

3. Start Apache Cassandra

	`./bin/cassandra -f`

Option -f means foreground.
