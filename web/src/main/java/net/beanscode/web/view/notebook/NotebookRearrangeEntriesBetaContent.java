package net.beanscode.web.view.notebook;

import java.io.IOException;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.notebooks.Notebook;
import net.beanscode.web.beta.BetaContent;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.BetaButtonGroup;
import net.hypki.libs5.db.db.weblibs.utils.UUID;

import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

public class NotebookRearrangeEntriesBetaContent extends BetaContent {
	
	@Expose
	private UUID notebookId = null;
	
	private Notebook notebook = null;

	public NotebookRearrangeEntriesBetaContent() {
		
	}
	
	public NotebookRearrangeEntriesBetaContent(BeansComponent parent, Notebook notebook) {
		super(parent);
		setNotebookId(notebook.getId());
		this.notebook = notebook;
	}
		
	@Override
	protected void renderHeader(HtmlCanvas html) throws IOException {
		html
			.h1()
				.content("Rearranging entries for Notebook " + getNotebook().getName());
	}
	
	@Override
	protected BetaButtonGroup getMenu() throws IOException {
		return null;
	}
	
	@Override
	protected String getMenuName() throws IOException {
		return null;
	}
	
	@Override
	protected void renderContent(HtmlCanvas html) throws IOException {
		html
			.render(new NotebookRearrangeEntriesPanel(this, getNotebookId()));
	}
	
	private Notebook getNotebook() throws IOException {
		if (notebook == null) {
			notebook = new ApiCallCommand(this)
						.setApi("api/notebook/get")
						.addParam("notebookId", getNotebookId().getId())
						.run()
						.getResponse()
						.getAsObject(Notebook.class);
		}
		return notebook;
	}

	private UUID getNotebookId() {
		if (notebookId == null)
			notebookId = new UUID(getParams().getString("notebookId"));
		return notebookId;
	}

	private void setNotebookId(UUID notebookId) {
		this.notebookId = notebookId;
	}
}
