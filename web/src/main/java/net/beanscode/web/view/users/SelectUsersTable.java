package net.beanscode.web.view.users;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.string.StringUtilities.split;
import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.type;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.users.Group;
import net.beanscode.cli.users.User;
import net.beanscode.cli.users.UserList;
import net.beanscode.cli.users.UsersSearchResult;
import net.beanscode.web.view.ui.BetaTableComponent;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpAddClass;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpRemoveClass;
import net.hypki.libs5.pjf.op.OpSetVal;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.collections.ArrayUtils;
import net.hypki.libs5.utils.url.Params;

import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

public class SelectUsersTable extends BetaTableComponent {
	
//	@Expose
	private List<UUID> selectedUsers = null;
	
//	@Expose
	private UsersSearchResult users = null;
	
	@Expose 
	private UUID groupId = null;
	
	private Group group = null;

	public SelectUsersTable() {
		
	}
	
	public SelectUsersTable(Component parent, UUID groupId) {
		super(parent);
		setGroupId(groupId);
		
		for (UUID uuid : getGroup().iterateUserIds())
			getSelectedUsers().add(uuid);
	}
	
	@Override
	protected String getSettingsPrefix() {
		return "selectusers-table-";
	}
	
	@Override
	protected long getMaxHits() throws IOException {
		return (int) getUsers().getMaxHits();
	}
	
	@Override
	protected int getNumberOfColumns() throws IOException {
		return 2;
	}
	
	@Override
	protected String getColumnName(int col) throws IOException {
		if (col == 0)
			return "Selected";
		else
			return "User";
	}
	
	@Override
	protected void renderCell(int row, int col, HtmlCanvas html) throws IOException {
		final User user = getUsers().getUsers().get(row % getPerPage());
		final boolean selected = getSelectedUsers().contains(user.getId());
		
		if (user.getId().equals(getUserId()))
			return;
		
		if (col == 0)
			html
				.span(class_(user.getId().getId() + (selected ? " selected" : "")))
//					.td()
					.input(type("checkbox")
						.checked(selected ? "checked" : null)
						.onChange(new OpComponentClick(this, 
								"onToggle", 
								new Params().add("userIdToggle", user.getId().getId())).toStringOnclick()))
//					._td()
				._span();
		else
//				.td(onClick(selected ? new OpUncheck("." + user.getId().getId() + " input[type=\"checkbox\"]").toStringOnclick()
//						: new OpCheck("." + user.getId().getId() + " input[type=\"checkbox\"]").toStringOnclick()))
			html
				.span()
					.content(user.getEmail())
//			._tr()
			;
	}

	@Override
	protected boolean isFilterVisible() {
		return true;
	}

	@Override
	protected List<Button> getMenu() {
		return null;
//		return new ButtonGroup()
//			.addButton(new InputButton("filter")
//					.add(new OpComponentTimer(this, "onRefresh", 300)))
//			.getButtons();
	}
	
	@Override
	protected void renderTitle(HtmlCanvas html) throws IOException {
		html
			.span()
				.content("Selecting users")
				
				.input(type("hidden")
						.name("selectedUserIds")
						.id("selectedUserIds")
						.value(ArrayUtils.toString(getSelectedUsers(), ";")))
				.input(type("hidden")
						.name("groupId")
						.id("groupId")
						.value(getGroupId().getId()))
			;
	}
	
	private OpList onToggle() {
		final UUID userId = new UUID(getParams().getString("userIdToggle"));
		
		boolean selected = false;
		if (getSelectedUsers().contains(userId))
			getSelectedUsers().remove(userId);
		else {
			getSelectedUsers().add(userId);
			selected = true;
		}
		
		return new OpList()
//			.add(new OpBeansInfo(ArrayUtils.toString(getSelectedUsers(), ";")))
			.add(new OpSetVal("#" + getId() + " #selectedUserIds", ArrayUtils.toString(getSelectedUsers(), ";")))
			.add(selected ? new OpAddClass("." + userId, "selected") : new OpRemoveClass("." + userId, "selected"));
	}

	private UsersSearchResult getUsers() {
		if (users == null) {
			try {
				users = new UserList(getSessionBean()).run(getParams().getString("filter"), 
						getPage() * getPerPage(), getPerPage());
			} catch (CLIException | IOException e) {
				LibsLogger.error(SelectUsersTable.class, "Cannot search for users", e);
				users = new UsersSearchResult();
			}
		}
		return users;
	}

	private void setUsers(UsersSearchResult users) {
		this.users = users;
	}

	private List<UUID> getSelectedUsers() {
		if (selectedUsers == null) {
			selectedUsers = new ArrayList<UUID>();
			
			if (notEmpty(getParams().getString("selectedUserIds"))) {
				for (String uuid : split(getParams().getString("selectedUserIds"), ";")) {
					getSelectedUsers().add(new UUID(uuid));
				}
			} 
		}
		return selectedUsers;
	}

	private void setSelectedUsers(List<UUID> seletectUsers) {
		this.selectedUsers = seletectUsers;
	}

	public UUID getGroupId() {
		return groupId;
	}

	public void setGroupId(UUID groupId) {
		this.groupId = groupId;
	}

	public Group getGroup() {
		if (group == null)
			try {
				group = new ApiCallCommand(this)
					.setApi("api/group/get")
					.addParam("groupId", getGroupId().getId())
					.run()
					.getResponse()
					.getAsObject(Group.class);
			} catch (CLIException | IOException e) {
				LibsLogger.error(SelectUsersTable.class, "Cannot get Group from API", e);
			}
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	@Override
	protected boolean isHeaderColumnVisible() throws IOException {
		return true;
	}

	@Override
	protected boolean isExapandable() throws IOException {
		return false;
	}

	@Override
	protected void renderExpandable(int row, HtmlCanvas html)
			throws IOException {
		
	}

	@Override
	protected String getTableCss() throws IOException {
		return null;
	}
}
