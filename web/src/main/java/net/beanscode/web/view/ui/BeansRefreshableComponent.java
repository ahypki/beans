package net.beanscode.web.view.ui;

import static org.rendersnake.HtmlAttributesFactory.id;

import java.io.IOException;

import org.rendersnake.HtmlAttributesFactory;
import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

import net.hypki.libs5.pjf.components.ops.OpComponentTimer;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpReplace;
import net.hypki.libs5.pjf.op.OpSet;

public abstract class BeansRefreshableComponent extends BeansComponent {
	
	@Expose
	private int timerMs = 1500;
	
	protected abstract HtmlCanvas renderContent(HtmlCanvas html) throws IOException ;
	protected abstract boolean isRefreshNeeded();

	public BeansRefreshableComponent(BeansComponent parent) {
		super(parent);
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.div(id(getId()));
		
		renderContent(html);
		
		html
			._div()
			.write(new OpComponentTimer(this, "onRefresh", getTimerMs()).toStringAdhock(), false)
			;
	}
	
	private OpList onRefresh() throws IOException {
		return new OpList()
				.add(new OpSet("#" + getId(), renderContent(new HtmlCanvas()).toHtml()))
				.add(isRefreshNeeded(), new OpComponentTimer(this, "onRefresh", getTimerMs()));
	}

	protected int getTimerMs() {
		return timerMs;
	}

	protected void setTimerMs(int timerMs) {
		this.timerMs = timerMs;
	}
}
