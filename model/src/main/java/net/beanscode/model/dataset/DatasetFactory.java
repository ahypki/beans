package net.beanscode.model.dataset;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.NotImplementedException;

import net.beanscode.model.BeansSearchManager;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.ResultsIter;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.search.SearchResults;
import net.hypki.libs5.search.query.Query;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.weblibs.CacheManager;
import net.hypki.libs5.weblibs.SearchManager;
import net.hypki.libs5.weblibs.WeblibsConst;

public class DatasetFactory {

	public static void clearCache(UUID datasetId) {
//		CacheManager.cacheInstance().put(Table.COLUMN_FAMILY, table.getId().getId(), table.getData());
		CacheManager.cacheInstance().remove(Dataset.COLUMN_FAMILY, datasetId.getId());
	}

	public static Dataset getDataset(String datasetId) throws IOException {
		return DatasetFactory.getDataset(new UUID(datasetId));
	}

	public static Dataset getDataset(UUID datasetId) throws IOException {
		Dataset ds = DatasetFactory.getFromCache(datasetId);
		
		if (ds != null)
			return ds;
		
		ds = DbObject.getDatabaseProvider().get(WeblibsConst.KEYSPACE, 
				Dataset.COLUMN_FAMILY, 
				new C3Where()
						.addClause(new C3Clause(DbObject.COLUMN_PK, ClauseType.EQ, datasetId.getId())), 
				DbObject.COLUMN_DATA, 
				Dataset.class);
		
		if (ds != null)
			updateCache(ds);
		
		return ds;
	}

	private static Dataset getFromCache(UUID datasetId) {
		return CacheManager
			.cacheInstance()
			.get(Dataset.class, Dataset.COLUMN_FAMILY, datasetId.getId());
	}

	public static void updateCache(Dataset dataset) {
		if (dataset == null)
			return;
		
		CacheManager
			.cacheInstance()
			.put(Dataset.COLUMN_FAMILY, dataset.getId().getId(), dataset);
	}

	public static Iterable<Dataset> iterateDatasets() {
		return iterateDatasets((UUID) null);
	}
	
	public static Iterable<Dataset> iterateDatasets(final UUID userId) {
		return new Iterable<Dataset>() {
			@Override
			public Iterator<Dataset> iterator() {
				return new Iterator<Dataset>() {
					private ResultsIter rowsIter = null;
					
					private boolean initiated = false;
					
					private Dataset nextDataset = null;
					
					@Override
					public Dataset next() {
						if (!initiated)
							hasNext();
						return nextDataset;
					}
					
					@Override
					public boolean hasNext() {
						if (!initiated) {
							rowsIter = new ResultsIter(WeblibsConst.KEYSPACE, Dataset.COLUMN_FAMILY, new String[]{DbObject.COLUMN_DATA}, null);
							initiated = true;
						}
						
						while (rowsIter.hasNext()) {
							Row row = rowsIter.next();
							
							if (row.contains(DbObject.COLUMN_DATA)) {
								nextDataset = JsonUtils.fromJson((String) row.getColumns().get(DbObject.COLUMN_DATA), Dataset.class);
								
								if (userId == null)
									return true;
								else if (nextDataset.getUserId().equals(userId))
									return true;
							}
						}
						
						return false;
					}
				};
			}
		};
	}

	public static SearchResults<Dataset> searchDatasets(Query q, int from, int size) throws IOException {
		return SearchManager.searchInstance().search(Dataset.class, 
				WeblibsConst.KEYSPACE_LOWERCASE, 
				Dataset.COLUMN_FAMILY.toLowerCase(), 
				q, 
				from, 
				size);
	}

	public static SearchResults<Dataset> searchDatasets(UUID userId, final String queryToParse, int from, int size) throws IOException {
		return searchDatasets(BeansSearchManager.parseDatasetsQuery(queryToParse, userId, null), from, size);
	}

	public static Iterable<Dataset> iterateDatasets(final UUID userId, final String query) {
		return DatasetFactory.iterateDatasets(BeansSearchManager.parseDatasetsQuery(query, userId, null));
	}

	public static Iterable<Dataset> iterateDatasets(final Query q) {
		return new Iterable<Dataset>() {
			@Override
			public Iterator<Dataset> iterator() {
				return new Iterator<Dataset>() {
					private int from = 0;
					
					private int tablesIdx = 0;
					
					private List<Dataset> tables = null;
					
					@Override
					public void remove() {
						throw new NotImplementedException();
					}
					
					@Override
					public Dataset next() {
						return tables.get(tablesIdx++);
					}
					
					@Override
					public boolean hasNext() {
						try {
							if (tables == null || tables.size() == tablesIdx) {
								tables = searchDatasets(q, from, BeansSearchManager.PER_PAGE).getObjects();
								tablesIdx = 0;
								from += tables.size();
							}
							return tablesIdx < tables.size();
						} catch (IOException e) {
							LibsLogger.error(BeansSearchManager.class, "Cannot get the next page with results", e);
							return false;
						}
					}
				};
			}
		};
	}

}
