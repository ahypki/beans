package net.beanscode.model.bulk;

import static net.hypki.libs5.utils.utils.NumberUtils.autoParse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.beanscode.model.cass.notification.Notification;
import net.beanscode.model.cass.notification.NotificationType;
import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.DatasetFactory;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.dataset.TableFactory;
import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.notebook.NotebookEntry;
import net.beanscode.model.notebook.NotebookFactory;
import net.beanscode.model.notebook.autoupdate.AutoUpdateOption;
import net.beanscode.model.notebook.autoupdate.AutoUpdateSettings;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.Base64Utils;
import net.hypki.libs5.utils.url.Params;
import net.hypki.libs5.weblibs.jobs.Job;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import org.apache.commons.lang.NotImplementedException;

import com.google.gson.annotations.Expose;

public class BulkChangeJob extends Job {
	
	@Expose
	private UUID userId = null;
	
	@Expose
	private String datasetQuery = null;
	
	@Expose
	private String tableQuery = null;
	
	@Expose
	private String notebookQuery = null;
	
	@Expose
	@NotNull
	@NotEmpty
	private List<BulkChangesJobs> jobs = null;
	
	@Expose
	private Params params = null;

	public BulkChangeJob() {
		
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		final UUID notId = UUID.random();
		
		try {
			new Notification()
				.setId(notId)
//			.setDescription("Bulk change")
				.setNotificationType(NotificationType.INFO)
				.setTitle("Bulk change")
				.setUserId(getUserId())
				.save();
			
			for (BulkChangesJobs bulkChangesJob : getJobs()) {
				if (bulkChangesJob == BulkChangesJobs.DATASET_CLEAR) {
					
					for (Dataset ds : DatasetFactory.iterateDatasets(getUserId(), getDatasetQuery()))
						if (ds.isWriteAllowed(getUserId()))
							ds.clear();
					
				} else if (bulkChangesJob == BulkChangesJobs.DATASET_DELETE) {
					
					for (Dataset ds : DatasetFactory.iterateDatasets(getUserId(), getDatasetQuery()))
						if (ds.isWriteAllowed(getUserId()))
							ds.remove();
					
				} else if (bulkChangesJob == BulkChangesJobs.DATASET_REINDEX) {
					
					for (Dataset ds : DatasetFactory.iterateDatasets(getUserId(), getDatasetQuery()))
						if (ds.isWriteAllowed(getUserId()))
							ds.index();
					
				} else if (bulkChangesJob == BulkChangesJobs.DATASET_RENAME) {
					
					for (Dataset ds : DatasetFactory.iterateDatasets(getUserId(), getDatasetQuery()))
						if (ds.isWriteAllowed(getUserId())) {
							try {
								ds.rename(getParams().getString("ds-rename"));
							} catch (Exception e) {
								LibsLogger.error(BulkChangeJob.class, "Cannot change Dataset name for " + ds.getName(), e);
							}
						}
					
				} else if (bulkChangesJob == BulkChangesJobs.DATASET_META_ADD) {
					
					for (Dataset ds : DatasetFactory.iterateDatasets(getUserId(), getDatasetQuery()))
						if (ds.isWriteAllowed(getUserId())) {
							for (String key : getParams().keys()) {
								ds.getMeta().add(key, autoParse(getParams().get(key)));
							}
							ds.save();
						}
					
				} else if (bulkChangesJob == BulkChangesJobs.DATASET_META_REMOVE) {
					
					for (Dataset ds : DatasetFactory.iterateDatasets(getUserId(), getDatasetQuery()))
						if (ds.isWriteAllowed(getUserId())) {
							for (String key : getParams().keys()) {
								ds.getMeta().remove(key);
							}
							ds.save();
						}
					
				} else if (bulkChangesJob == BulkChangesJobs.TABLE_CLEAR) {
					
					for (Table tb : TableFactory.iterateTables(getUserId(), getDatasetQuery(), getTableQuery()))
						if (tb.isWriteAllowed(getUserId()))
							tb.clear(false);
					
				} else if (bulkChangesJob == BulkChangesJobs.TABLE_DELETE) {
					
					for (Table tb : TableFactory.iterateTables(getUserId(), getDatasetQuery(), getTableQuery()))
						if (tb.isWriteAllowed(getUserId()))
							tb.remove();
					
				} else if (bulkChangesJob == BulkChangesJobs.TABLE_REINDEX) {
					
					for (Table tb : TableFactory.iterateTables(getUserId(), getDatasetQuery(), getTableQuery()))
						if (tb.isWriteAllowed(getUserId()))
							tb.indexData(true);
					
				} else if (bulkChangesJob == BulkChangesJobs.TABLE_META_ADD) {
					
					for (Table tb : TableFactory.iterateTables(getUserId(), getDatasetQuery(), getTableQuery()))
						if (tb.isWriteAllowed(getUserId())) {
							for (String key : getParams().keys()) {
								tb.getMeta().add(key, autoParse(getParams().get(key)));
							}
							tb.save();
						}
					
				} else if (bulkChangesJob == BulkChangesJobs.TABLE_META_REMOVE) {
					
					for (Table tb : TableFactory.iterateTables(getUserId(), getDatasetQuery(), getTableQuery()))
						if (tb.isWriteAllowed(getUserId())) {
							for (String key : getParams().keys()) {
								tb.getMeta().remove(key);
							}
							tb.save();
						}
					
				} else if (bulkChangesJob == BulkChangesJobs.NOTEBOOK_UPDATE) {
					
					for (Notebook nt : NotebookFactory.iterateNotebooks(getUserId(), getNotebookQuery()))
						if (nt.isWriteAllowed(getUserId()))
							nt.update();
					
				} else if (bulkChangesJob == BulkChangesJobs.NOTEBOOK_DELETE) {
					
					for (Notebook nt : NotebookFactory.iterateNotebooks(getUserId(), getNotebookQuery()))
						if (nt.isWriteAllowed(getUserId()))
							nt.remove();
					
				} else if (bulkChangesJob == BulkChangesJobs.NOTEBOOK_META_ADD) {
					
					for (Notebook nt : NotebookFactory.iterateNotebooks(getUserId(), getNotebookQuery()))
						if (nt.isWriteAllowed(getUserId())) {
							for (String key : getParams().keys()) {
								nt.getMeta().add(key, autoParse(getParams().get(key)));
							}
							nt.save();
						}
					
				} else if (bulkChangesJob == BulkChangesJobs.NOTEBOOK_META_REMOVE) {
					
					for (Notebook nt : NotebookFactory.iterateNotebooks(getUserId(), getNotebookQuery()))
						if (nt.isWriteAllowed(getUserId())) {
							for (String key : getParams().keys()) {
								nt.getMeta().remove(key);
							}
							nt.save();
						}
					
				} else if (bulkChangesJob == BulkChangesJobs.NOTEBOOK_AUTOUPDATE) {
					
					if (getParams().contains("autoUpdate")) {
						AutoUpdateOption option = AutoUpdateOption.parse(getParams().getString("autoUpdate"));
						
						for (Notebook nt : NotebookFactory.iterateNotebooks(getUserId(), getNotebookQuery()))
							if (nt.isWriteAllowed(getUserId())) {
								for (NotebookEntry ne : nt.iterateEntries()) {
									ne
										.setAutoUpdateOptions(new AutoUpdateSettings(option))
										.save();
								}
							}
					}
					
				} else
					
					LibsLogger.error(BulkChangeJob.class, "Bulk job " + bulkChangesJob + " not implemented in class " + getClass().getSimpleName());
			}
			
			new Notification()
				.setId(notId)
//		.setDescription("Bulk change")
				.setNotificationType(NotificationType.OK)
				.setTitle("Bulk change")
				.setUserId(getUserId())
				.save();
			
			LibsLogger.info(BulkChangeJob.class, "Bulk change finished");
		} catch (Exception e) {
			LibsLogger.error(BulkChangeJob.class, "Bulk change failed, consuming job", e);
		}
	}
	
	@Override
	public void validate() throws ValidationException {
		
	}
	
	public String getDatasetQuery() {
		return datasetQuery;
	}

	public BulkChangeJob setDatasetQuery(String datasetQuery) {
		this.datasetQuery = datasetQuery;
		return this;
	}

	public String getTableQuery() {
		return tableQuery;
	}

	public BulkChangeJob setTableQuery(String tableQuery) {
		this.tableQuery = tableQuery;
		return this;
	}

	public String getNotebookQuery() {
		return notebookQuery;
	}

	public BulkChangeJob setNotebookQuery(String notebookQuery) {
		this.notebookQuery = notebookQuery;
		return this;
	}
	
	public BulkChangeJob addJob(BulkChangesJobs job) {
		if (job != null)
			getJobs().add(job);
		
		return this;
	}

	public List<BulkChangesJobs> getJobs() {
		if (jobs == null)
			jobs = new ArrayList<>();
		return jobs;
	}

	public void setJobs(List<BulkChangesJobs> jobs) {
		this.jobs = jobs;
	}

	public UUID getUserId() {
		return userId;
	}

	public BulkChangeJob setUserId(UUID userId) {
		this.userId = userId;
		return this;
	}

	public Params getParams() {
		if (params == null)
			params = new Params();
		return params;
	}

	public void setParams(Params params) {
		this.params = params;
	}
}
