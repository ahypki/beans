package net.beanscode.web.view.dataset;

import static net.hypki.libs5.utils.string.RandomUtils.nextInt;
import static org.rendersnake.HtmlAttributesFactory.class_;

import java.io.IOException;

import javax.ws.rs.core.SecurityContext;

import org.rendersnake.HtmlAttributesFactory;
import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

import net.beanscode.cli.BeansCLISettings;
import net.beanscode.cli.datasets.Table;
import net.beanscode.cli.datasets.TableGateway;
import net.beanscode.cli.notebooks.Notebook;
import net.beanscode.cli.notebooks.NotebookGateway;
import net.beanscode.cli.notebooks.NotebookSearchResults;
import net.beanscode.web.view.notebook.NotebooksTableComponent;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.Link;
import net.beanscode.web.view.ui.ListComponent;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.op.Op;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.search.SearchResults;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.RandomUtils;
import net.hypki.libs5.utils.url.Params;

public class TablesListComponent extends ListComponent {
	
	@Expose
	private OpList onClick = null;
	
	@Expose
	private UUID datasetId = null;
	
	private SearchResults<Table> tablesSearchResults = null;

	public TablesListComponent() {
		
	}
	
	public TablesListComponent(BeansComponent parent) {
		super(parent);
	}
	
	public TablesListComponent(SecurityContext securityContext, Params params) {
		super(securityContext, params);
	}
	
	protected SearchResults<Table> getTablesSearchResults() {
		if (tablesSearchResults == null) {
			try {
				tablesSearchResults = TableGateway.getTables(this.getSessionBean(), getDatasetId().getId(), 
						getFilter(), getPage(), getPerPage());
			} catch (CLIException | IOException e) {
				LibsLogger.error(NotebooksTableComponent.class, "Cannot search for tables", e);
				tablesSearchResults = new SearchResults<>();
			}
		}
		return tablesSearchResults;
	}
	
	@Override
	protected String getSettingsPrefix() {
		return "tables-list-";
	}
	
	@Override
	protected String getFilterTooltip() {
		return "Filter tables...";
	}
	
	@Override
	protected long getMaxHits() throws IOException {
		return getTablesSearchResults().maxHits();
	}
	
	@Override
	protected void renderItem(int idx, HtmlCanvas html) throws IOException {
		Table n = getTablesSearchResults().getObjects().get(idx % getPerPage());
		
		html
//			.span(class_("").add("style", "background-color: rgb(" + nextInt(255) + "," + nextInt(255) + "," + nextInt(255) + ")"))
//				.content(new Link(n.getName(), getOnClick().addParamParam("notebookId", n.getId().getId())).toHtml(), false);
			.render(new Link(n.getName(), getOnClick().addParamParam("tableId", n.getId().getId())));
	}

	public OpList getOnClick() {
		if (onClick == null)
			onClick = new OpList();
		return onClick;
	}

	public TablesListComponent setOnClick(OpList onClick) {
		this.onClick = onClick;
		return this;
	}
	
	public TablesListComponent setOnClick(Op onClick) {
		getOnClick().add(onClick);
		return this;
	}

	public UUID getDatasetId() {
		return datasetId;
	}

	public TablesListComponent setDatasetId(UUID datasetId) {
		this.datasetId = datasetId;
		return this;
	}
}
