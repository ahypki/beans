package net.beanscode.cli.unittests;

import java.io.IOException;

import net.beanscode.cli.BeansCLISettings;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.pjf.components.jersey.SessionBean;
import net.hypki.libs5.utils.tests.LibsTestCase;

/**
 * Unit test for simple App.
 */
public class BeansTestCase extends LibsTestCase {

	public BeansTestCase() {
		System.setProperty("weblibsSettings", "settings-beans-dev.json");
	}
	
	protected SessionBean getSessionBean() throws CLIException, IOException {
		return BeansCLISettings.getSessionBeans();
	}
}
