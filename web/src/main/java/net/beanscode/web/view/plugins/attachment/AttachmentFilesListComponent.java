package net.beanscode.web.view.plugins.attachment;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static org.rendersnake.HtmlAttributesFactory.data;
import static org.rendersnake.HtmlAttributesFactory.src;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

import net.beanscode.cli.BeansCliCommand;
import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.notebooks.AttachmentEntry;
import net.beanscode.cli.notebooks.NotebookEntryGateway;
import net.beanscode.web.BeansWebConst;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.Link;
import net.beanscode.web.view.ui.ListComponent;
import net.beanscode.web.view.ui.SpinnerLabel;
import net.beanscode.web.view.ui.modals.OpModalQuestion;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpSet;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.url.Params;

public class AttachmentFilesListComponent extends ListComponent {
	
	@Expose
	private UUID entryId = null;
	
	@Expose
	private boolean deleteEnabled = true;

	private AttachmentEntry attachementEntry = null;
	
	private List<String> filteredFiles = null;
	
	public AttachmentFilesListComponent() {
		
	}
	
	public AttachmentFilesListComponent(BeansComponent parent, AttachmentEntry entry) {
		super(parent);
		setAttachementEntry(entry);
	}
	
	@Override
	public String getHtmlClass() {
		return super.getHtmlClass() + " attachment-list";
	}

	@Override
	protected long getMaxHits() throws IOException {
		return getFilteredFiles().size();
	}
	
	@Override
	protected boolean isFilterVisible() {
		try {
			return notEmpty(getFilter()) || getMaxHits() > getPerPage();
		} catch (IOException e) {
			LibsLogger.error(AttachmentFilesListComponent.class, "Cannot check for max hints", e);
			return true;
		}
	}

	@Override
	protected void renderItem(int idx, HtmlCanvas html) throws IOException {
		String f = getFilteredFiles().get(getPage() * getPerPage() + idx);
		html
			.render(new Link(f, new OpComponentClick(this, "onClickFile", new Params().add("idx", idx)))
					.setHtmlClass(getAttachementEntry().isRemoved(f) ? "striked" : ""))
			.if_(isDeleteEnabled())
				.render(new Link(null, 
						BeansWebConst.ICON_REMOVE, 
						new OpComponentClick(this, "onRemoveFileAsk", new Params().add("idx", idx))))
			._if();
	}
	
	private OpList onRemoveFileAsk() throws IOException {
		int idx = getParams().getInteger("idx", 0);
		String f = getFilteredFiles().get(getPage() * getPerPage() + idx);
		
		return new OpList()
				.add(new OpModalQuestion("Removing attachment", 
						"Do you want to remove <b>" + f + "</b>?", 
						new OpComponentClick(this, "onRemoveFile", new Params().add("idx", idx))));
	}
	
	private OpList onRemoveFile() throws IOException {
		int idx = getParams().getInteger("idx", 0);
		String f = getFilteredFiles().get(getPage() * getPerPage() + idx);
		
		new ApiCallCommand(this)
			.setApi(BeansCliCommand.API_NOTEBOOK_ENTRY_META_ADD)
			.addParam("entryId", getAttachementEntry().getId().getId())
			.addParam("metaName", "removed-" + UUID.random().getId())
			.addParam("metaValue", f)
			.run();
		
		return new OpList()
				.add(new OpSet("#" + getId() + " .items", new SpinnerLabel("Refreshing...").toHtml()))
				.add(new OpComponentClick(this, "onRefresh"));
	}
	
	private OpList onClickFile() throws IOException {
		int idx = getParams().getInteger("idx", 0);
		String f = getFilteredFiles().get(getPage() * getPerPage() + idx);
		
		if (f.toLowerCase().endsWith("png")
				|| f.toLowerCase().endsWith("jpg")
				|| f.toLowerCase().endsWith("jpeg")) {
			HtmlCanvas img = new HtmlCanvas();
			img
				.img(src("/api/notebook/entry/attachment/" 
						+ "?entryId=" + getAttachementEntry().getId() 
						+ "&fileName=" + f
						+ "&t=" + System.currentTimeMillis()));
			return new OpList()
				.add(new OpSet("#" + getId() + " .footer", img.toHtml()));
		} else if (f.toLowerCase().endsWith("pdf")) {
			HtmlCanvas html = new HtmlCanvas();
			html
				.object(data("/api/notebook/entry/attachment/pdf" 
						+ "?entryId=" + getAttachementEntry().getId() 
						+ "&fileName=" + f
						+ "&t=" + System.currentTimeMillis())
						.type("application/pdf")
						.class_("")
						.width("1800px")
						.height("900px"))
				._object();
			return new OpList()
					.add(new OpSet("#" + getId() + " .footer", html.toHtml()));
		} else {
			HtmlCanvas download = new HtmlCanvas();
			download
				.render(new Link("Download " + f, 
						"/api/notebook/entry/attachment/?entryId=" + getAttachementEntry().getId() + "&fileName=" + f));
//			"/view/notebook/entry/download/" 
//					+ "?entryId=" + getAttachementEntry().getId() 
//					+ "&metaName=" + meta.getName()
//					+ "&t=" + System.currentTimeMillis();
			return new OpList()
					.add(new OpSet("#" + getId() + " .footer", download.toHtml()));
		}
//		return new OpList();
	}

	@Override
	protected String getSettingsPrefix() {
		return "attachment-files-" + getId();
	}

	@Override
	protected String getFilterTooltip() {
		return "Filter attachments";
	}

	public AttachmentEntry getAttachementEntry() {
		if (attachementEntry == null) {
			try {
				attachementEntry = (AttachmentEntry) NotebookEntryGateway.getNotebookEntry(getSessionBean(), getEntryId());
			} catch (CLIException | IOException e) {
				LibsLogger.error(AttachmentFilesListComponent.class, "Cannot get entryId", e);
			}
		}
		return attachementEntry;
	}

	public void setAttachementEntry(AttachmentEntry attachementEntry) {
		this.attachementEntry = attachementEntry;
		setEntryId(attachementEntry.getId());
	}

	public boolean isDeleteEnabled() {
		return deleteEnabled;
	}

	public AttachmentFilesListComponent setDeleteEnabled(boolean deleteEnabled) {
		this.deleteEnabled = deleteEnabled;
		return this;
	}

	public UUID getEntryId() {
		return entryId;
	}

	public void setEntryId(UUID entryId) {
		this.entryId = entryId;
	}

	public List<String> getFilteredFiles() {
		if (filteredFiles == null) {
			filteredFiles = new ArrayList<>();
			for (String filename : getAttachementEntry().getAttachmentFiles()) {
				if (notEmpty(getFilter())) {
					if (filename.contains(getFilter()))
						filteredFiles.add(filename);
				} else
					filteredFiles.add(filename);
			}
		}
		return filteredFiles;
	}

	public void setFilteredFiles(List<String> filteredFiles) {
		this.filteredFiles = filteredFiles;
	}
	
	
}
