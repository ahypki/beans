package net.beanscode.web.view.ui;

import static net.beanscode.cli.BeansCliCommand.API_SETTING_GETSETTING;
import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static org.rendersnake.HtmlAttributesFactory.add;
import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.data;
import static org.rendersnake.HtmlAttributesFactory.id;
import static org.rendersnake.HtmlAttributesFactory.name;

import java.io.IOException;
import java.util.List;

import org.rendersnake.HtmlCanvas;
import org.rendersnake.Renderable;

import com.google.gson.annotations.Expose;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.settings.SettingGateway;
import net.beanscode.web.view.settings.Setting;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.components.ops.OpComponentTimer;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpSet;
import net.hypki.libs5.pjf.op.OpSetVal;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.url.Params;

// TODO change name later
public abstract class BetaTableComponent extends BeansComponent {
	
	@Expose
	private int page = 0;

	@Expose
	private int perPage = 5;
	
	@Expose
	private OpList onRefreshAdditional = null;
	
	private String filterCache = null;
	
	private String filter2Cache = null;
	
	protected abstract void renderTitle(HtmlCanvas html) throws IOException;
	
	protected abstract boolean isFilterVisible() throws IOException;
	
	protected abstract boolean isHeaderColumnVisible() throws IOException;
	
	protected abstract int getNumberOfColumns() throws IOException;
	protected abstract String getColumnName(int col) throws IOException;
	
	protected abstract boolean isExapandable() throws IOException;
	protected abstract void renderExpandable(int row, HtmlCanvas html) throws IOException;
	
	protected abstract long getMaxHits() throws IOException;
	
	protected abstract void renderCell(int row, int col, HtmlCanvas html) throws IOException;
	
	protected abstract List<Button> getMenu() throws IOException;
	
	protected abstract String getTableCss() throws IOException;
	
	protected abstract String getSettingsPrefix();
	
	private boolean postponeReload = false;

	public BetaTableComponent() {
		
	}
	
	public BetaTableComponent(Component parent) {
		super(parent);
	}
	
	protected boolean isHeaderVisible() {
		return true;
	}
	
	protected boolean isFooterVisible() {
		return true;
	}
	
	protected boolean isFilter2Visible() {
		return false;
	}
	
	protected String getFilterPlaceholder() {
		return "Search";
	}
	
	protected String getFilter2Placeholder() {
		return "Search 2";
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.div(id(getId())
					.class_("card card-primary"))
										
//				.if_(isHeaderVisible())
					.div(class_("card-header"))
						.h3(class_("card-title"))
							.render(new Renderable() {
								@Override
								public void renderOn(HtmlCanvas html) throws IOException {							
									renderTitle(html);
								}
							})
						._h3() // card-title
						.div(class_("card-tools beans-card-tools-50"))
						;
		
		// filter
		html
			.div(class_("input-group input-group-sm"));
		if (isFilterVisible()) {
			html
					.input(name("beans-table-per-page")
							.class_("beans-table-per-page")
							.type("hidden")
							.value("" + getPerPage()))
					.input(name("beans-table-last-filter-" + getId())
							.class_("beans-table-last-filter-" + getId())
							.type("hidden")
							.value(""))
					.input(name("beans-table-filter-" + getId())
							.type("text")
							.class_("form-control float-right beans-table-filter")
							.add("placeholder", getFilterPlaceholder())
							.value(getFilter())
//							.onChange(new OpComponentClick(this, "onRefresh").toStringOnclick())
							.onKeyup(new OpList()
								.add(new OpSet("#" + getId() + " .beans-table-rows", 
										new SpinnerLabel("Refreshing...").toHtml()))
								.add(new OpComponentTimer(this, "onRefresh", 300))
								.toStringOnclick())
							)
					.if_(isFilter2Visible())
						.input(name("beans-table-filter2-" + getId())
								.type("text")
								.class_("form-control float-right beans-table-filter2")
								.add("placeholder", getFilter2Placeholder())
								.value(getFilter2())
//								.onChange(new OpComponentClick(this, "onRefresh").toStringOnclick())
								.onKeyup(new OpList()
									.add(new OpSet("#" + getId() + " .beans-table-rows", 
											new SpinnerLabel("Refreshing...").toHtml()))
									.add(new OpComponentTimer(this, "onRefresh", 300))
									.toStringOnclick())
								)
					._if()
					.div(class_("input-group-append"))
						.button(class_("btn btn-default")
								.type("button")
								.onClick(new OpComponentClick(this, "onRefresh").toStringOnclick()))
							.i(class_("fas fa-search"))
							._i()
						._button()
					._div()
					;
		}
		
		// render additional menu if needed
		if (getMenu() != null) {
			html.div(class_("input-group-append"));
			for (Button button : getMenu()) {
				html
					.button(class_("btn btn-default " + (button.getHtmlClass() != null ? button.getHtmlClass() : ""))
							.type("button")
							.onClick(button.getOpList().toStringOnclick())
							.title(notEmpty(button.getTooltip()) ? button.getTooltip() : ""))
							;
				
				if (StringUtilities.notEmpty(button.getAwesomeicon())){
					html
						.i(class_(button.getAwesomeicon()))
						._i()
						._button();
				} else
					html
						.content(button.getName());
				
//					.render(button
//							.setReplacedHtmlClass("btn btn-default"));
			}
			html._div();
		}
		
		html
			._div() // input-group
			;
		
		html
					._div() // card-tools
				._div() // card-header
//			._if()
			;

		// rendering table
		html
			.div(class_("card-body p-0"))
				.table(class_("table " + (getTableCss() != null ? getTableCss() : "")))
				;
		
		// rendering header columns
		if (isHeaderColumnVisible()) {
			html
				.thead()
					.tr()
					;
			
			for (int i = 0; i < getNumberOfColumns(); i++) {
				html
					.th(class_("beans-col-" + i))
//						.div()
							.span()
								.content(getColumnName(i))
//						._div()
					._th()
					;
			}
			
			html
					._tr()
				._thead()
				;
		}
		
		// rendering rows
		html
				.tbody(class_("beans-table-rows"));
		renderRows(html);
		html
				._tbody()
				._table()
			._div();
		
		// rendering footer
		html
			.if_(isFooterVisible())
				.div(class_("card-footer clearfix row"))
					.div(class_("col-sm-6"))
						.ul(class_("pagination pagination-sm m-0 beans-pagination"))
							.render(new Renderable() {
								@Override
								public void renderOn(HtmlCanvas html) throws IOException {								
									renderPagination(html);
								}
							})
						._ul()
					._div()
					.div(class_("col-sm-6"))
						.ul(class_("pagination pagination-sm m-0 float-right beans-table-perpage"))
							.render(new Renderable() {
								@Override
								public void renderOn(HtmlCanvas html) throws IOException {
									renderPerPage(html);									
								}
							})
						._ul()
					._div()
				._div()
			._if()
			;
		
			
		html
			._div() // card
			;
	}

	private HtmlCanvas renderOnePageButton(HtmlCanvas html, int currentPage) throws IOException {
		if (currentPage == getPage()) {
			return html
					.li(class_("page-item")
							.add("title", "Page " + (currentPage + 1)))
						.a(class_("page-link beans-link-inactive"))
							.content("" + (currentPage + 1))
					._li();
		} else {
			return html
					.li(class_("page-item")
							.add("title", "Page " + (currentPage + 1)))
						.a(class_("page-link")
//								.href("#")
								.onClick(new OpList()
									.add(new OpSet("#" + getId() + " .beans-table-rows", 
											new SpinnerLabel("Refreshing...").toHtml()))	
									.add(new OpComponentClick(this, 
										"onRefresh", 
										new Params()
											.add("page", currentPage)))
									.toStringOnclick()))
							.content("" + (currentPage + 1))
					._li();
		}
	}
	
	private HtmlCanvas renderPerPage(HtmlCanvas html) throws IOException {
		for (Integer perPage : new Integer[] {5, 10, 20}) {
			html
				.li(class_("page-item")
						.add("title", "" + perPage + " per page"))
					.a(class_("page-link " + (getPerPage() == perPage ? "beans-link-inactive" : ""))
							.href("#")
							.onClick(new OpList()
									.add(new OpComponentClick(this, 
											"onPerPageChange", 
											new Params()
												.add("perPage", perPage)))
									.toStringOnclick()))
						.content("" + perPage)
				._li()
				;
		}
		
		return html;
	}
	
	private OpList onPerPageChange() throws IOException {
		int perPageNew = getParams().getInteger("perPage", 5);
		
		setPage(0);
		setPerPage(perPageNew);
		
		return new OpList()
			.add(new OpSetVal("#" + getId() + " .beans-table-per-page", perPageNew))
			.add(new OpComponentClick(this, "onRefresh"));
//			.add(new OpSet("#" + getId() + " .beans-table-rows", renderRows(new HtmlCanvas()).toHtml()))
//			.add(new OpSet("#" + getId() + " .beans-table-perpage", renderPerPage(new HtmlCanvas()).toHtml()));
	}

	private HtmlCanvas renderPagination(HtmlCanvas html) throws IOException {
		int maxPage = (int) getMaxHits() / getPerPage();
		
		int startPage = getPage() - 2;
		int stopPage = getPage() + 2;
		
		if (startPage < 0) {
			stopPage = stopPage + Math.abs(startPage);
			
			startPage = 0;
		}
		
		if (stopPage > maxPage) {
			startPage = startPage + (maxPage - stopPage);
			if (startPage < 0)
				startPage = 0;
		
			stopPage = maxPage;
		}
		
		int currentPage = 0;
		renderOnePageButton(html, currentPage);
		
		for (currentPage = Math.max(currentPage + 1, startPage); currentPage <= stopPage; currentPage++) {
			renderOnePageButton(html, currentPage);
		}
		
		if (maxPage >= currentPage)
			renderOnePageButton(html, maxPage);
		
		return html;
	}
	
	protected String getRowCss(int row) {
		return "";
	}
	
	private HtmlCanvas renderRows(HtmlCanvas html) throws IOException {
		for (int i = 0; 
				i < getPerPage()
					&& (getPage() * getPerPage() + i) < getMaxHits(); 
				i++) {
			
			if (isExapandable()) {
				html
					.tr(data("widget", "expandable-table")
							.add("aria-expanded", "false")
							.class_(getRowCss(i)));
			} else {
				html
					.tr(class_(getRowCss(i)));
			}
			
			for (int col = 0; col < getNumberOfColumns(); col++) {
				html
					.td(class_("beans-col-" + col))
						;
				
				renderCell(i, col, html);
				
				html
					._td()
					;
			}
			
			if (isExapandable()) {
				html
					.tr(class_("expandable-body d-none"))
						.td(class_("").add("colspan", getNumberOfColumns()))
							.div(add("style", "display: none;", false))
						;
				renderExpandable(i, html);
				html
							._div()
						._td()
					._tr();
			}
			
			html
				._tr();
		}
		
		return html;
	}
	
	protected String getFilter() {
		if (filterCache == null)
			filterCache = getParams().getString("beans-table-filter-" + getId(), null);
		if (filterCache == null) {
			try {
				Setting s = SettingGateway.getUserSetting(getSessionBean(), getSettingsPrefix() + "-filter");
				filterCache = s != null ? s.getValueAsString("filter") : null;
			} catch (CLIException | IOException e) {
				LibsLogger.error(BetaTableComponent.class, "Cannot get setting", e);
			}
		}
		return filterCache;
	}
	
	protected String getFilterNotNull() {
		String tmp = getFilter();
		return tmp != null ? tmp : "";
	}
	
	protected String getLastFilterNotNull() {
		return getParams().getString("beans-table-last-filter-" + getId(), "");
	}
	
	protected String getFilter2() {
		if (filter2Cache == null)
			filter2Cache = getParams().getString("beans-table-filter2-" + getId(), null);
		if (filter2Cache == null) {
			try {
				Setting s = SettingGateway.getUserSetting(getSessionBean(), getSettingsPrefix() + "-filter2");
				filter2Cache = s != null ? s.getValueAsString("filter") : null;
			} catch (CLIException | IOException e) {
				LibsLogger.error(BetaTableComponent.class, "Cannot get setting", e);
			}
		}
		return filter2Cache;
	}
	
	private OpList onRefresh() throws IOException {
		int page = getParams().getInteger("page", -1);

		if (getFilterNotNull().equalsIgnoreCase(getLastFilterNotNull())) {
			if (this.page == page)
				return null;
		}
		
		// saving filter
		this.filterCache = getParams().getString("beans-table-filter-" + getId(), null);
		SettingGateway.setUserSetting(getSessionBean(), 
				getSettingsPrefix() + "-filter", 
				"filter", 
				this.filterCache);
		
		String filter2 = getParams().getString("beans-table-filter2-" + getId(), null);
		SettingGateway.setUserSetting(getSessionBean(), 
				getSettingsPrefix() + "-filter2", 
				"filter", 
				filter2);
		
		if (page > -1)
			setPage(page);
		
		return new OpList()
			.add(new OpSet("#" + getId() + " .beans-table-rows", renderRows(new HtmlCanvas()).toHtml()))
			.add(new OpSet("#" + getId() + " .beans-pagination", renderPagination(new HtmlCanvas()).toHtml()))
			.add(new OpSet("#" + getId() + " .beans-table-perpage", renderPerPage(new HtmlCanvas()).toHtml()))
			.add(new OpSetVal("#" + getId() + " .beans-table-last-filter-" + getId(), getFilterNotNull()))
			.add(getOnRefreshAdditional())
//			.add(new OpBeansInfo("refreshed"))
			;
	}
	
	public int getPage() {
		return page;
	}
	
	public void setPage(int page) {
		this.page = page;
	}

	protected int getPerPage() {
		// params have precedance 
		if (getParams().getInteger("beans-table-per-page", -1) > 0)
			perPage = getParams().getInteger("beans-table-per-page", perPage);
		return perPage;
	}

	public BetaTableComponent setPerPage(int perPage) {
		this.perPage = perPage;
		return this;
	}

	public OpList getOnRefreshAdditional() {
		return onRefreshAdditional;
	}

	public void setOnRefreshAdditional(OpList onRefreshAdditional) {
		this.onRefreshAdditional = onRefreshAdditional;
	}

	public boolean isPostponeReload() {
		return postponeReload;
	}

	public BetaTableComponent setPostponeReload(boolean postponeReload) {
		this.postponeReload = postponeReload;
		return this;
	}
}