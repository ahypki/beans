package net.beanscode.model.unittests.rights;

import java.io.IOException;

import net.beanscode.model.BeansSearchManager;
import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.dataset.TableFactory;
import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.rights.Group;
import net.beanscode.model.tests.BeansTestCase;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.search.SearchResults;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.user.User;

import org.junit.Test;

public class PermissionUnitTest extends BeansTestCase {

	@Test
	public void testReadWritePermissions() throws IOException, ValidationException {
		BeansSearchManager.getSearchManager().clearIndexType(WeblibsConst.KEYSPACE_LOWERCASE, Dataset.COLUMN_FAMILY.toLowerCase());
		BeansSearchManager.getSearchManager().clearIndexType(WeblibsConst.KEYSPACE_LOWERCASE, Table.COLUMN_FAMILY.toLowerCase());
		BeansSearchManager.getSearchManager().clearIndexType(WeblibsConst.KEYSPACE_LOWERCASE, Notebook.COLUMN_FAMILY.toLowerCase());
		
		final User u1 = createTestUser("u1");
		final User u2 = createTestUser("u2");
		
		final Dataset ds1 = createDataset(u1, "d1");
		final Table t1 = createTable(ds1, "t1");
		
		runAllJobs(1000);
		
		// u1 should have one table found
		SearchResults<Table> tables = TableFactory.searchTables(u1.getUserId(), "", "", 0, 10);
		assertTrue(tables.maxHits() == 1, "Expected 1 tables to find");
		
		// u2 should have 0 tables found
		tables = TableFactory.searchTables(u2.getUserId(), "", "", 0, 10);
		assertTrue(tables.maxHits() == 0, "Expected 0 tables to find");
		
		// u1 shares t1 with u2
		Group g1 = new Group();
		g1.setName("G1  u1->u2");
		g1.setOwner(u1.getUserId());
		g1.addUser(u2);
		g1.save();
		addToRemove(g1);
		
		t1.setReadPermission(g1, true);
		
		runAllJobs(1000);
		
		// u2 NOW should have 1 table found
		tables = TableFactory.searchTables(u2.getUserId(), "", "", 0, 10);
		assertTrue(tables.maxHits() == 1, "Expected 1 tables to find");
	}
}
