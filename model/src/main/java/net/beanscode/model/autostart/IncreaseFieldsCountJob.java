package net.beanscode.model.autostart;

import java.io.IOException;

import net.beanscode.model.BeansSearchManager;
import net.beanscode.model.BeansSettings;
import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.notebook.NotebookEntry;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.search.elastic.ElasticSearchProvider;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.jobs.Job;
import net.hypki.libs5.weblibs.settings.Setting;
import net.hypki.libs5.weblibs.settings.SettingFactory;
import net.hypki.libs5.weblibs.settings.SettingValue;


public class IncreaseFieldsCountJob extends Job {

	public IncreaseFieldsCountJob() {
		
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		if (!(BeansSearchManager.getSearchManager() instanceof ElasticSearchProvider)) {
			LibsLogger.debug(IncreaseFieldsCountJob.class, "Search engine is not Elastic, exiting");
			return;
		}
		
		Setting setting = SettingFactory.getSetting(BeansSettings.SETTING_NR_FIELDS_INCREASED);
		
		if (setting == null) {
			setting = new Setting()
				.setId(BeansSettings.SETTING_NR_FIELDS_INCREASED)
				.setName(BeansSettings.SETTING_NR_FIELDS_INCREASED)
				.setSystem(true)
				.setUserId(null)
				.save();
		}
		
		// increasing number of fields for BEANS tables, datasets and notebook entries
		for (String table : new String[] {Table.COLUMN_FAMILY, 
										Dataset.COLUMN_FAMILY,
										Notebook.COLUMN_FAMILY,
										NotebookEntry.COLUMN_FAMILY}) {
			
			if (setting.getValue(table) == null
					|| setting.getValue(table).getValueAsBoolean() == false) {
				boolean success = ((ElasticSearchProvider) BeansSearchManager.getSearchManager())
					.increaseFieldsCount(WeblibsConst.KEYSPACE_LOWERCASE, table);
				
				if (success)
					setting
						.addValue(new SettingValue(table, true))
						.save();
			}
		}
	}
}
