package net.beanscode.web.view.settings;

import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.id;
import static org.rendersnake.HtmlAttributesFactory.type;

import java.io.IOException;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.pojo.autoupdate.AutoUpdate;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.beanscode.web.view.ui.modals.OpModalQuestion;
import net.beanscode.web.view.ui.remarks.InfoRemark;
import net.beanscode.web.view.ui.remarks.WarningRemark;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.pjf.components.ComboComponent;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.buttons.ButtonGroup;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.components.ops.OpComponentTimer;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpSet;
import net.hypki.libs5.utils.api.APICallType;
import net.hypki.libs5.utils.date.SimpleDate;

import org.quartz.SchedulerException;
import org.rendersnake.HtmlCanvas;

public class AutoUpdatesPanel extends BeansComponent {
	
	public AutoUpdatesPanel() {
		
	}
	
	public AutoUpdatesPanel(BeansComponent parent) {
		super(parent);
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		AutoUpdate autoUpdate = new ApiCallCommand(this)
			.setApi(ApiCallCommand.API_AUTOUPDATE_SETTING)
			.addParam("userId", getUserEmail())
			.run()
			.getResponse()
			.getAsObject(AutoUpdate.class);
		
		SimpleDate nextFireAlarm = new ApiCallCommand(this)
			.setApi("api/autoupdate/next")
			.addParam("userId", getUserEmail())
			.run()
			.getResponse()
			.getAsDate("nextAlarm");
		
		html
			.div(id(getId()))
				.span()
					.content("Start auto-updates to check whether external data has changed every ")
				.input(type("text")
						.name("every")
						.value("" + autoUpdate.getEvery()))
				.render(new ComboComponent("everyType")
						.setName("every-type")
						.setValues("Minutes", "Hours", "Days")
						.setSelectedValue(autoUpdate.getType()))
				.render(new ButtonGroup()
						.addButton(new Button("Save")
								.add(new OpComponentClick(this, "onSave"))))
//				.br()
//				.span(class_("next-auto-update"))
					.render(new net.beanscode.web.view.ui.remarks.InfoRemark("Next Auto-update: ", "Scheduled at " + (nextFireAlarm != null ? nextFireAlarm.toStringHuman() : "---")))
//				._span()
//				._span()
//				.span()
					.render(new WarningRemark("Remark: ", 
							"Do not set this to low value if you have a lot of data, plots and queries because this may slow down your computer."))
//				._span()
				.render(new ButtonGroup()
						.addButton(new Button("Start auto-update now!")
								.add(new OpModalQuestion("Auto-update", 
										"Do you want to start auto-update on all notebooks?", 
										new OpComponentClick(this, "onAutoUpdateStart", null)))))
			._div();
		
	}
	
	private OpList onAutoUpdateStart() throws IOException, ValidationException {
		new ApiCallCommand(this)
			.setApi(ApiCallCommand.API_AUTOUPDATE_START)
			.addParam("userId", getUserEmail())
			.run();
		
		return new OpList()
			.add(new OpBeansInfo("Auto-update job has been started successfully"));
	}
	
	private OpList onReloadNextAlarm() throws IOException, SchedulerException {
		SimpleDate nextFireAlarm = new ApiCallCommand(this)
			.setApi("api/autoupdate/next")
			.addParam("userId", getUserEmail())
			.run()
			.getResponse()
			.getAsDate("nextAlarm");
		
		return new OpList()
				.add(new OpSet("#" + getId() + " .next-auto-update", 
						new InfoRemark("Next Auto-update: ", "Scheduled at " + (nextFireAlarm != null ? nextFireAlarm.toStringHuman() : "---")).toString()));
	}
	
	private OpList onSave() throws IOException, SchedulerException {
		String everyType = getParams().getString("every-type");
		int every = getParams().getInteger("every", 0);
		
//		new AutoUpdateSettingSet(getSessionBean()).run(getUserEmail(), every, everyType);
		new ApiCallCommand(getSessionBean())
			.setApi("api/autoupdate/setting")
			.setApiCallType(APICallType.POST)
			.addParam("userId", getUserId().getId())
			.addParam("every", every)
			.addParam("type", everyType)
			.run()
			;
				
		return new OpList()
				.add(new OpBeansInfo("Setting saved"))
				.add(new OpComponentTimer(this, "onReloadNextAlarm", 500));
	}
}
