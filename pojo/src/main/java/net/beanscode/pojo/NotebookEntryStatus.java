package net.beanscode.pojo;

public enum NotebookEntryStatus {
	NEW,
	RUNNING,
	SUCCESS,
	FAIL
}
