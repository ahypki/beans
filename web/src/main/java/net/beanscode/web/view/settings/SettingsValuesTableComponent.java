package net.beanscode.web.view.settings;

import static org.rendersnake.HtmlAttributesFactory.for_;
import static org.rendersnake.HtmlAttributesFactory.name;
import static org.rendersnake.HtmlAttributesFactory.type;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.beanscode.web.view.ui.BetaTableComponent;
import net.hypki.libs5.pjf.components.buttons.Button;

import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

public class SettingsValuesTableComponent extends BetaTableComponent {
	
	@Expose
	private List<SettingValue> values = null;

	public SettingsValuesTableComponent() {
		
	}
	
	public SettingsValuesTableComponent(List<SettingValue> values) {
		setValues(values);
	}
	
	@Override
	protected String getSettingsPrefix() {
		return "settingsvalues-table-";
	}

	public List<SettingValue> getValues() {
		if (values == null)
			values = new ArrayList<SettingValue>();
		return values;
	}

	public void setValues(List<SettingValue> values) {
		this.values = values;
	}
	
	@Override
	protected long getMaxHits() throws IOException {
		return getValues().size();
	}
	
	@Override
	protected int getNumberOfColumns() throws IOException {
		return 3;
	}
	
	@Override
	protected boolean isHeaderVisible() {
		return false;
	}
	
	@Override
	protected boolean isFooterVisible() {
		return false;
	}
	
	@Override
	protected void renderCell(int row, int column, HtmlCanvas html) throws IOException {
		SettingValue val = getValues().get(row);
		String name = "newVal-" + val.getName();
		
		if (column == 0) {
			
			if (val.getType() == SettingType.BOOLEAN) {
				html
					.label(for_(name))
						.content(val.getName());
			} else {
				html
					.span()
						.content(val.getName());
			}
			
		} else if (column == 1) {
			
			if (val.getType() == SettingType.BOOLEAN) {
				html
					.td()
						.input(type("checkbox")
							.id(name)
							.name(name)
							.checked(val.getValueAsBoolean() ? "checked" : null))
					._td();
			} else if (val.getType() == SettingType.PASS) {
				html
					.td()
						.input(type("password")
								.name(name)
								.value(val.getValueAsString()))
					._td();
			} else if (val.getType() == SettingType.MULTILINE) {
				html
					.td()
						.textarea(name(name))
							.content(val.getValueAsString())
					._td();
			} else {
				html
					.td()
						.input(type("text")
								.name(name)
								.value(val.getValueAsString()))
					._td();
			}
			
		} else {
			
			html
				.span()
					.content(" (" 
							+ (val.isDescriptionEmpty() ? "" : val.getDescription() + ", ") 
							+ "default: " + val.getDef() + ")")
			;
				
		}
			
	}


	@Override
	protected boolean isFilterVisible() {
		return false;
	}

	@Override
	protected List<Button> getMenu() {
		return null;
	}
	
	@Override
	protected String getColumnName(int col) throws IOException {
		return "";
	}

	@Override
	protected void renderTitle(HtmlCanvas html) throws IOException {
		
	}

	@Override
	protected boolean isHeaderColumnVisible() throws IOException {
		return false;
	}

	@Override
	protected boolean isExapandable() throws IOException {
		return false;
	}

	@Override
	protected void renderExpandable(int row, HtmlCanvas html)
			throws IOException {
		
	}

	@Override
	protected String getTableCss() throws IOException {
		return null;
	}
}
