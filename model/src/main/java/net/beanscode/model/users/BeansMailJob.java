package net.beanscode.model.users;

import java.io.IOException;

import net.beanscode.model.BeansSettings;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.weblibs.mail.MailJob;
import net.hypki.libs5.weblibs.user.Email;

public class BeansMailJob extends MailJob {

	public BeansMailJob() {
		
	}
	
	public BeansMailJob(String to, String title, String body) {
		super(to, title, body);
	}
	
	public BeansMailJob(Email to, String title, String body) {
		super(to, title, body);
	}
	
	@Override
	public Object save() throws ValidationException, IOException {
		setMailCredentials(BeansSettings.getMailCredentials());
		
		return super.save();
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		setMailCredentials(BeansSettings.getMailCredentials());
		
		super.run();
	}
}
