package net.beanscode.web.view.users;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static org.rendersnake.HtmlAttributesFactory.class_;

import java.io.IOException;
import java.util.List;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.users.Group;
import net.beanscode.cli.users.GroupSearchResults;
import net.beanscode.cli.users.User;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.BetaButtonGroup;
import net.beanscode.web.view.ui.BetaTableComponent;
import net.beanscode.web.view.ui.modals.InputType;
import net.beanscode.web.view.ui.modals.ModalWindow;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.beanscode.web.view.ui.modals.OpModal;
import net.beanscode.web.view.ui.modals.OpModalQuestion;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.buttons.ButtonGroup;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.components.ops.OpComponentTimer;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpRemove;
import net.hypki.libs5.pjf.op.OpSet;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.url.Params;

import org.rendersnake.HtmlCanvas;

public class GroupsBetaTableComponent extends BetaTableComponent {
	
	private GroupSearchResults groups = null;

	public GroupsBetaTableComponent() {
		
	}
	
	public GroupsBetaTableComponent(BeansComponent parent) {
		super(parent);
	}
	
	@Override
	protected String getSettingsPrefix() {
		return "groups-table-";
	}
	
	private GroupSearchResults getGroups() {
		if (groups == null) {
			try {
				groups = new ApiCallCommand(this)
						.setApi("api/group/query")
						.addParam("query", getFilter())
						.addParam("from", getPage() * getPerPage())
						.addParam("size", getPerPage())
						.run()
						.getResponse()
						.getAsObject(GroupSearchResults.class);
			} catch (CLIException | IOException e) {
				LibsLogger.error(GroupsBetaTableComponent.class, "Cannot search for groups", e);
				groups = new GroupSearchResults();
			}
		}
		return groups;
	}
	
	@Override
	protected long getMaxHits() throws IOException {
		return getGroups().getMaxHits();
	}
	
	@Override
	protected int getNumberOfColumns() throws IOException {
		return 4;
	}
	
	@Override
	protected String getColumnName(int column) throws IOException {
		if (column == 0)
			return "Name";
		else if (column == 1)
			return "Description";
		else if (column == 2)
			return "Users";
		else
			return "";
	}
	
	@Override
	protected boolean isHeaderColumnVisible() throws IOException {
		return true;
	}
	
	@Override
	protected boolean isExapandable() throws IOException {
		return false;
	}
	
	@Override
	protected void renderExpandable(int row, HtmlCanvas html) throws IOException {
		
	}
	
	@Override
	protected void renderTitle(HtmlCanvas html) throws IOException {
		html
			.write("Groups");
	}
	
	@Override
	protected String getRowCss(int row) {
		final Group gr = getGroups().getGroups().get(row % getPerPage());
		return super.getRowCss(row) + " group-" + gr.getId().getId();
	}
	
	@Override
	protected void renderCell(int row, int column, HtmlCanvas html) throws IOException {
		final Group gr = getGroups().getGroups().get(row % getPerPage());
		
		if (column == 0)
			html
				.span(class_("beans-group-name-" + gr.getId()))
					.content(gr.getName());
		else if (column == 1)
			html
				.span(class_("beans-group-desc-" + gr.getId()))
					.content(gr.getDescription());
		else if (column == 2) {
			html
				.span(class_("beans-group-users-" + gr.getId()));
			boolean first = true;
			for (User user : gr.iterateUsers(getSessionBean())) {
				if (!first)
					html.write(", ", false);
				
				if (user != null)
					html
						.write(user.getEmail());
				first = false;
			}
			html
				._span();
		} else {
			html
				.render(new BetaButtonGroup()
						.add(new Button("Edit")
								.add(new OpComponentClick(this, 
										"onEditGroup", 
										new Params().add("id", gr.getId().getId()))))
						.add(new Button("Remove")
							.add(new OpModalQuestion("Removing group", 
									"Are you sure you want to remove the Group <b>" + gr.getName() + "</b>?",
									new OpComponentClick(this,
											"onRemoveGroup",
											new Params().add("id", gr.getId().getId())))))
						.add(new Button("Add/Remove users")
							.add(new OpComponentClick(this, "onAddRemoveUsers", new Params().add("id", gr.getId().getId())))));
		}
	}

	@Override
	protected boolean isFilterVisible() {
		return true;
	}

	@Override
	protected List<Button> getMenu() {
		return new ButtonGroup()
			.addButton(new Button("New group")
				.add(new OpComponentClick(this, "onEditGroup")))
//			.addButton(new Button()
//				.setGlyphicon("refresh")
//				.add(new OpComponentClick(this, "onRefresh")))
			.getButtons();
	}
	
	private OpList onAddRemoveUsers() throws IOException {
		ModalWindow modal = new ModalWindow(this);
		modal.setTitle("Select/deselect users");
		modal
			.getContent()
			.render(new SelectUsersTable(this, new UUID(getParams().getString("id"))));
		modal.add(new Button("Save")
				.add(new OpComponentClick(this.getClass(), 
						modal.getId(), 
						"onAddRemoveUsersSave",
						new Params().add("componentToRefresh", getId())))
				.add(modal.onClose()));
		modal.add(new Button("Cancel")
				.add(modal.onClose()));
		
		return new OpList()
			.add(new OpModal(modal));
	}
	
	private OpList onAddRemoveUsersSave() throws CLIException, IOException {
		final String groupId = getParams().getString("groupId");
		final String componentToRefresh = getParams().getString("componentToRefresh");
		
//		new GroupSet(getSessionBean()).run(groupId, null, null, 
//				StringUtilities.split(getParams().getString("selectedUserIds"), ";", null));
		new ApiCallCommand(this)
			.setApi("api/group/set")
			.addParam("id", groupId)
			.addParam("userIds", StringUtilities.split(getParams().getString("selectedUserIds"), ";", null))
			.run();
		
		return new OpList()
			.add(new OpComponentTimer(this.getClass(), new UUID(componentToRefresh), "onRefresh", 1200))
//			.add(new OpSet("#" + componentToRefresh + " .beans-group-name-" + id, name))
//			.add(new OpReplace("#" + groupId, prepareRow(new HtmlCanvas(), grTmp).toHtml()))
//			.add(new OpDialogClose())
			;
	}
	
	private OpList onEditGroup() throws IOException {
		String groupId = getParams().getString("id");
		
		Group gr = null;
		if (notEmpty(groupId)) {
			gr = new ApiCallCommand(this)
				.setApi("api/group/get")
				.addParam("groupId", groupId)
				.run()
				.getResponse()
				.getAsObject(Group.class);
		} else {
			gr = new Group();
			gr.setName("New name");
			groupId = gr.getId().getId();
		}
		
		ModalWindow modal = new ModalWindow(this);
		modal.setTitle("Editing group");
		modal.add("Group ID", InputType.HIDDEN, "groupId", groupId);
		modal.add("Name", InputType.STRING, "groupName", gr.getNameNotNull());
		modal.add("Description", InputType.TEXT, "groupDesc", gr.getDescriptionNotNull());
		modal.add(new Button("Save")
				.add(new OpComponentClick(this.getClass(), 
						modal.getId(), 
						"onGroupSave",
						new Params().add("componentToRefresh", getId())))
				.add(modal.onClose()));
		modal.add(new Button("Cancel")
				.add(modal.onClose()));
		
		return new OpList()
			.add(new OpModal(modal))
//			.add(new OpDialogOpen(this, "onGroupSave", "Editing group", new EditGroupPanel(this, gr).toHtml()))
			;
	}
	
	private OpList onRemoveGroup() throws IOException {
		final String groupId = getParams().getString("id");
		
		new ApiCallCommand(this)
			.setApi("api/group/delete")
			.addParam("groupId", groupId)
			.run();
		
		return new OpList()
			.add(new OpRemove(".group-" + groupId))
			.add(new OpBeansInfo("Group removed"));
	}
	
	private OpList onGroupSave() throws CLIException, IOException {
		String id = getParams().getString("groupId");
		final String name = getParams().getString("groupName");
		final String description = getParams().getString("groupDesc");
		final boolean isNew = !notEmpty(id);
		final String compId = getParams().getString("componentToRefresh");
		
		id = notEmpty(id) ? id : UUID.random().getId();
		
//		new GroupSet(getSessionBean()).run(id, name, description, null);
		new ApiCallCommand(this)
			.setApi("api/group/set")
			.addParam("id", id)
			.addParam("name", name)
			.addParam("description", description)
			.run();
		
//		Group grTmp = new GroupGet(getSessionBean()).run(id);
//		String grHtml = prepareRow(new HtmlCanvas(), grTmp).toHtml();
		
		return new OpList()
			.add(new OpSet("#" + compId + " .beans-group-name-" + id, name))
			.add(new OpSet("#" + compId + " .beans-group-desc-" + id, description))
//			.add(new OpComponentTimer(this.getClass(), new UUID(compId), "onRefresh", 1000))
			.add(new OpBeansInfo("Group data saved"))
//			.add(!isNew, new OpReplace("#" + id, grHtml))
//			.add(isNew, new OpAppend("#" + getId() + " .table-data table", grHtml))
//			.add(new OpComponentTimer(this, "onRefresh", 500))
//			.add(new OpDialogClose())
			;
	}

	@Override
	protected String getTableCss() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}
}
