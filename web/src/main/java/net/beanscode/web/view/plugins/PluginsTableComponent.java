package net.beanscode.web.view.plugins;

import static net.beanscode.cli.BeansCliCommand.API_PLUGIN_DISABLE;
import static net.beanscode.cli.BeansCliCommand.API_PLUGIN_ENABLE;
import static net.beanscode.cli.BeansCliCommand.API_PLUGIN_GET;
import static net.beanscode.cli.BeansCliCommand.API_PLUGIN_IS_ENABLED;
import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.string.StringUtilities.replaceNonAlphanumeric;
import static org.rendersnake.HtmlAttributesFactory.class_;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.rendersnake.HtmlCanvas;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.plugin.PluginPOJO;
import net.beanscode.web.view.ui.BetaButtonGroup;
import net.beanscode.web.view.ui.BetaTableComponent;
import net.beanscode.web.view.ui.Link;
import net.beanscode.web.view.ui.badge.BadgeError;
import net.beanscode.web.view.ui.badge.BadgeSuccess;
import net.beanscode.web.view.ui.badge.BadgeWarning;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.beanscode.web.view.ui.modals.OpModalQuestion;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.labels.Label;
import net.hypki.libs5.pjf.components.labels.MessageType;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpReplace;
import net.hypki.libs5.pjf.op.OpSet;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.url.Params;

public class PluginsTableComponent extends BetaTableComponent {

	private List<PluginPOJO> allPlugins = null;
	
	private List<PluginPOJO> filteredPlugins = null;
	
	private HashMap<String, Boolean> pluginEnabledCache = new HashMap<String, Boolean>();
	
	public PluginsTableComponent() {
		
	}
	
	public PluginsTableComponent(Component parent) {
		super(parent);
	}
	
	@Override
	protected String getSettingsPrefix() {
		return "plugins-table-";
	}
	
	private List<PluginPOJO> getPlugins() {
		if (this.filteredPlugins == null)
			getAllPlugins();
		return this.filteredPlugins;
	}

	private List<PluginPOJO> getAllPlugins() {
		if (allPlugins == null) {
			try {
				allPlugins = new ArrayList<PluginPOJO>();// new PluginGetList(getSessionBean()).run(getUserEmail());
				
				ApiCallCommand call = new ApiCallCommand(this)
					.setApi("api/plugin/list")
					.run();
				
				allPlugins.addAll(call.getResponse().getAsList("/", PluginPOJO.class));
				
				filteredPlugins = new ArrayList<PluginPOJO>();
				
				if (notEmpty(getFilter())) {
					for (PluginPOJO pluginPOJO : allPlugins) {
						if (pluginPOJO.getName().toLowerCase().contains(getFilter().toLowerCase()))
							filteredPlugins.add(pluginPOJO);
					}
				} else
					filteredPlugins.addAll(allPlugins);
			} catch (CLIException | IOException e) {
				LibsLogger.error(PluginsTableComponent.class, "Cannot get Plugin list", e);
			}
		}
		return allPlugins;
	}

	private void setPlugins(List<PluginPOJO> plugins) {
		this.allPlugins = plugins;
	}

	@Override
	protected void renderTitle(HtmlCanvas html) throws IOException {
		html
			.write("List of all available BEANS plugins");
	}

	@Override
	protected boolean isFilterVisible() throws IOException {
		return true;
	}

	@Override
	protected boolean isHeaderColumnVisible() throws IOException {
		return true;
	}

	@Override
	protected int getNumberOfColumns() throws IOException {
		return 6;
	}

	@Override
	protected String getColumnName(int col) throws IOException {
		if (col == 0)
			return "Name";
		else if (col == 1)
			return "Version";
		else if (col == 2)
			return "Description";
		else if (col == 3)
			return "Status";
		else if (col == 4)
			return "Self test";
		else
			return "Options";
	}

	@Override
	protected boolean isExapandable() throws IOException {
		return false;
	}

	@Override
	protected void renderExpandable(int row, HtmlCanvas html)
			throws IOException {
		
	}

	@Override
	protected long getMaxHits() throws IOException {
		return getPlugins().size();
	}

	private String onClickPluginOpen(UUID userId, PluginPOJO plugin) throws IOException {
		return new HtmlCanvas()
			.render(new Link(plugin.getName(), new OpComponentClick(this, "onOpenPlugin", new Params()
					.add("plugin", plugin.getName())
					.add("userId", userId))))
			.toHtml();
	}

	public OpList onOpenPlugin(Params params) throws IOException {
		final UUID userId = new UUID(params.getString("userId"));
		final String pluginName = params.getString("plugin");
		
		LibsLogger.debug(PluginsTableComponent.class, "Opening panel ", pluginName);
		
		PluginPOJO plugin = new ApiCallCommand(this)
								.setApi(API_PLUGIN_GET)
								.addParam("userId", getUserEmail())
								.addParam("pluginName", pluginName)
								.run()
								.getResponse()
								.getAsObject(PluginPOJO.class);
		
		return new OpList()
				.add(new OpSet("#page-title", plugin.getName()))
				.add(new OpSet(".beans-content-body", new PluginComponent(this, plugin).toHtml()));
	}
	
	@Override
	protected void renderCell(int row, int col, HtmlCanvas html)
			throws IOException {
		PluginPOJO plugin = getPlugins().get(getPage() * getPerPage() + row);
		Boolean pluginEnabled = pluginEnabledCache.containsKey(plugin.getName())
				? pluginEnabledCache.get(plugin.getName()) : null;
		if (pluginEnabled == null) {
			pluginEnabled = new ApiCallCommand(this)
								.setApi(API_PLUGIN_IS_ENABLED)
								.addParam("userId", getUserEmail())
								.addParam("pluginName", plugin.getName())
								.run()
								.getResponse()
								.getAsBoolean("enabled");
			pluginEnabledCache.put(plugin.getName(), pluginEnabled);
		}
		
		ApiCallCommand selftTest = new ApiCallCommand(this)
			.setApi(ApiCallCommand.API_PLUGIN_SELFTEST_GET)
			.addParam("userId", getUserEmail())
			.addParam("pluginName", plugin.getName())
			.run();
		
		if (col == 0)
			html
				.span()
					.content(pluginEnabled ? onClickPluginOpen(getUserId(), plugin) : plugin.getName(), false);
		else if (col == 1)
			html.span().content(plugin.getVersion());
		else if (col == 2)
			html.span().content(plugin.getDescription());
		else if (col == 3)
			html
				.span(class_(pluginEnabled ? "badge badge-success" : "badge badge-secondary"))
					.content(pluginEnabled ? new Label(MessageType.SUCCESS, "Enabled").toString() : new Label(MessageType.DEFAULT, "Disabled").toString(), false);
		else if (col == 4) {
			html
				.span(class_("plugin-" + StringUtilities.replaceNonAlphanumeric(plugin.getName(), "")))
					.render(new SelfTestStatusComponent(this, plugin.getName()))
				._span();
		} else
			html
				.render(new BetaButtonGroup()
						.add(new Button()
								.setAwesomeicon("fa fa-toggle-off")
								.setTooltip("Click to enable plugin")
								.setVisible(!pluginEnabled)
								.add(new OpModalQuestion("Enabling plugin", 
										"Do you want to <b>enable " + plugin.getName() + "</b> plugin?", 
										new OpComponentClick(this, 
												"onEnablePlugin",
												new Params()
													.add("plugin", plugin.getName())))))
						.add(new Button()
								.setAwesomeicon("fa fa-toggle-on")
								.setTooltip("Click to disable plugin")
								.setVisible(pluginEnabled)
								.add(new OpModalQuestion("Enabling plugin", 
										"Do you want to <b>disable " + plugin.getName() + "</b> plugin?", 
										new OpComponentClick(this, 
												"onDisablePlugin",
												new Params()
													.add("plugin", plugin.getName())))))
						.add(new Button()
								.setAwesomeicon("fa fa-medkit")
								.setTooltip("Click to start self test")
								.setVisible(pluginEnabled)
								.add(new OpComponentClick(this, 
										"onSelfTestStart",
										new Params()
											.add("plugin", plugin.getName()))))
						);
	}
	
	public OpList onSelfTestStart() throws CLIException, IOException {
//		final UUID userId = new UUID(getParams().getString("userId"));
		final String pluginName = getParams().getString("plugin");
//		
//		LibsLogger.debug(PluginsTableComponent.class, "Starting selft test");
//		
		new ApiCallCommand(this)
			.setApi(ApiCallCommand.API_PLUGIN_SELFTEST_START)
			.addParam("pluginName", pluginName)
			.run();
		
		return new OpList()
				.add(new OpBeansInfo("Self test started. Expect mail notification."))
//				.add(new OpComponentClick(this, "onSelfTestRefresh", getParams()))
				.add(new OpSet(".plugin-" + replaceNonAlphanumeric(pluginName, ""), 
						new SelfTestStatusComponent(this, pluginName).toHtml()))
				;
	}
	
//	public OpList onSelfTestRefresh() throws CLIException, IOException {
//		final String pluginName = getParams().getString("plugin");
//		
//		ApiCallCommand selftTest = new ApiCallCommand(this)
//				.setApi(ApiCallCommand.API_PLUGIN_SELFTEST_GET)
//				.addParam("userId", getUserEmail())
//				.addParam("pluginName", pluginName)
//				.run();
//
//		OpList ops = new OpList();
//		HtmlCanvas html = new HtmlCanvas();
//		if (selftTest.getResponse().getAsString("status", "unknown").equals("ok")) {
//			html
//				.render(new BadgeSuccess(selftTest.getResponse().getAsString("status", "unknown")));
//		} else if (selftTest.getResponse().getAsString("status", "unknown").equals("error")) {
//			html
//				.render(new BadgeError(selftTest.getResponse().getAsString("status", "unknown")));
//		} else {
//			html
//				.render(new BadgeWarning(selftTest.getResponse().getAsString("status", "unknown")));
//			ops
//				.add(new OpComponentClick(this, "onSelfTestRefresh", getParams()));
//		}
//		
//		return ops;
//	}

	public OpList onEnablePlugin(Params params) throws IOException {
		final UUID userId = new UUID(params.getString("userId"));
		final String pluginName = params.getString("plugin");
		
		LibsLogger.debug(PluginsTableComponent.class, "Enabling panel ", pluginName);
		
		new ApiCallCommand(this)
			.setApi(API_PLUGIN_ENABLE)
			.addParam("userId", getUserEmail())
			.addParam("pluginName", pluginName)
			.run();
//		Plugin plugin = PluginManager.getPlugin(userId, pluginName);
		
		PluginPOJO plugin = new ApiCallCommand(this)
								.setApi(API_PLUGIN_GET)
								.addParam("userId", getUserEmail())
								.addParam("pluginName", pluginName)
								.run()
								.getResponse()
								.getAsObject(PluginPOJO.class);
		
//		PluginManager.enablePlugin(userId, plugin);
		
		final String css = plugin.getName().replaceAll("[^\\w]", "_");
		
		return new OpList()
				.add(new OpComponentClick(this, "onRefresh"))
//				.add(new OpSet("#plugin-table ." + css + " .status", new Label(MessageType.SUCCESS, "Enabled").toString()))
//				.add(new OpSet("#plugin-table ." + css + " .name", onclickPluginOpen(userId, plugin)))
//				.add(new OpEnable("#plugin-table ." + css + " .disableButton"))
//				.add(new OpDisable("#plugin-table ." + css + " .enableButton"))
				;
	}
	
	public OpList onDisablePlugin(Params params) throws IOException {
		final UUID userId = new UUID(params.getString("userId"));
		final String pluginName = params.getString("plugin");
		
		LibsLogger.debug(PluginsTableComponent.class, "Disabling panel ", pluginName);
		
		new ApiCallCommand(this)
			.setApi(API_PLUGIN_DISABLE)
			.addParam("userId", getUserEmail())
			.addParam("pluginName", pluginName)
			.run();
		
		final String css = pluginName.replaceAll("[^\\w]", "_");
		
		return new OpList()
				.add(new OpComponentClick(this, "onRefresh"))
		;
	}

	
	private OpList onReloadPlugins() throws CLIException, IOException {
		new ApiCallCommand(this)
			.setApi("api/plugin/reload")
			.run();
		
//		HtmlCanvas html = new HtmlCanvas();
//		renderOn(html);
		
		return new OpList()
			.add(new OpBeansInfo("Plugins reloaded"))
			.add(new OpComponentClick(this, "onRefresh"));	
	}
	
	private OpList onSelfTestAllPlugins() throws CLIException, IOException {
		new ApiCallCommand(this)
			.setApi("api/plugin/selftest/all")
			.run();
		
//		HtmlCanvas html = new HtmlCanvas();
//		renderOn(html);
		
		return new OpList()
			.add(new OpBeansInfo("Self test started for all plugins"))
			.add(new OpComponentClick(this, "onRefresh"));	
	}
	
	@Override
	protected List<Button> getMenu() throws IOException {
		return new BetaButtonGroup()
			.add(new Button("Reload plugins")
									.add(new OpComponentClick(this, 
											"onReloadPlugins",
											new Params()))
				)
			.add(new Button("Self test all plugins")
					.add(new OpComponentClick(this, 
							"onSelfTestAllPlugins",
							new Params())))
			.getButtons();
	}

	@Override
	protected String getTableCss() throws IOException {
		return null;
	}
}
