package net.beanscode.model.notebook;

import java.io.IOException;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.jobs.Job;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class RemoveEntryFromNotebookJob extends Job {
	
	@Expose
	@NotNull
	@NotEmpty
	@AssertValid
	private UUID notebookId = null;
		
	@Expose
	@NotNull
	@NotEmpty
	@AssertValid
	private UUID entryToRemove = null;

	public RemoveEntryFromNotebookJob() {
		
	}
	
	public RemoveEntryFromNotebookJob(NotebookEntry notebookEntry) {
		setNotebookId(notebookEntry.getNotebookId());
		setEntryToRemove(notebookEntry.getId());
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		Notebook notebook = Notebook.getNotebook(getNotebookId());
		
		if (notebook == null) {
			LibsLogger.debug(RemoveEntryFromNotebookJob.class, "Cannot remove entry from non-exiting notebook ", getNotebookId());
			return;
		}
		
		notebook.removeEntry(getEntryToRemove());
		notebook.save();
	}

	public UUID getNotebookId() {
		return notebookId;
	}

	public void setNotebookId(UUID notebookId) {
		this.notebookId = notebookId;
	}

	public UUID getEntryToRemove() {
		return entryToRemove;
	}

	public void setEntryToRemove(UUID entryToRemove) {
		this.entryToRemove = entryToRemove;
	}
}
