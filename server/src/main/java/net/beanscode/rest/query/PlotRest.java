package net.beanscode.rest.query;

import javax.ws.rs.Path;

import net.beanscode.rest.BeansRest;

@Path("api/plot")
public class PlotRest extends BeansRest {
	
//	@POST
//	@Path("logscale")
//	public void plotLogscale(@Context SecurityContext sc,
//			@FormParam("plotId") UUID plotId,
//			@FormParam("logscaleOn") boolean logscaleOn) throws IOException, ValidationException {
//		Plot plot = (Plot) NotebookEntryFactory.getNotebookEntry(plotId);
//		
//		validateUnauthorized(isAdmin(sc) || plot.getNotebook().isWriteAllowed(getUserId(sc)), "Permission denied");
//		
//		if (logscaleOn)
//			plot.setLogscaleOn();
//		else
//			plot.setLogscaleOff();
//		
//		plot.save();
//	}
	
//	@POST
//	@Path("save/histogram")
//	public void staticPlotHistogram(@Context SecurityContext sc,
//			@FormParam("plotId") UUID plotId,
//			@FormParam("plotData") String plotData) throws IOException, ValidationException {
//		PlotHistogramEntry incomingPlot = JsonUtils.fromJson(plotData, PlotHistogramEntry.class);
//		PlotHistogramEntry plot = (PlotHistogramEntry) NotebookEntryFactory.getNotebookEntry(plotId);
//		
//		final Notebook note = plot.getNotebook();
//		
//		validateNotFound(note != null, "Notebook is not found");
//		validateUnauthorized(isAdmin(sc) || note.isWriteAllowed(getUserId(sc)), "Permission denied");
//		
////		plot.setUserId(getSessionBean(sc).getUserId());
//		
//		plot.setTitle(incomingPlot.getTitle());
//		
//		plot.setDsQuery(incomingPlot.getDsQuery());
//		plot.setTbQuery(incomingPlot.getTbQuery());
//				
//		plot.setX(incomingPlot.getX());
//		plot.setY(incomingPlot.getY());
//		plot.setXLabel(incomingPlot.getXLabel());
//		plot.setYLabel(incomingPlot.getYLabel());
//		
//		plot.setBinWidth(incomingPlot.getBinWidth());
//		
//		plot.setColorBy(incomingPlot.getColorBy());
//		plot.setSplitBy(incomingPlot.getSplitBy());
//		
//		plot.getPlotStatus().setStarted(true); // TODO move it to model
//		
//		plot.save();
//		
//		new PlotPrepareJob(plot.getId()).save();
//	}
	
//	@POST
//	@Path("save/parallel")
//	public void staticPlotParallel(@Context SecurityContext sc,
//			@FormParam("plotId") UUID plotId,
//			@FormParam("plotData") String plotData) throws IOException, ValidationException {
//		PlotParallelCoordinatesEntry incomingPlot = JsonUtils.fromJson(plotData, PlotParallelCoordinatesEntry.class);
//		PlotParallelCoordinatesEntry plot = (PlotParallelCoordinatesEntry) NotebookEntryFactory.getNotebookEntry(plotId);
//		
//		final Notebook note = plot.getNotebook();
//		
//		validateNotFound(note != null, "Notebook is not found");
//		validateUnauthorized(isAdmin(sc) || note.isWriteAllowed(getUserId(sc)), "Permission denied");
//		
////		plot.setUserId(getSessionBean(sc).getUserId());
//		
//		plot.setTitle(incomingPlot.getTitle());
//		
//		plot.setDsQuery(incomingPlot.getDsQuery());
//		plot.setTbQuery(incomingPlot.getTbQuery());
//				
//		plot.setColumns(incomingPlot.getColumns());
//		plot.setLegends(incomingPlot.getLegends());
//		
//		plot.setMinValue(incomingPlot.getMinValue());
//		plot.setMaxValue(incomingPlot.getMaxValue());
//		
//		plot.getPlotStatus().setStarted(true); // TODO move it to model
//		
//		plot.save();
//		
//		new PlotPrepareJob(plot.getId()).save();
//	}
	
//	@POST
//	@Path("save/points")
//	public void staticPlotPoints(@Context SecurityContext sc,
//			@FormParam("plotId") UUID plotId,
//			@FormParam("plotData") String plotData) throws IOException, ValidationException {
//		Plot incomingPlot = JsonUtils.fromJson(plotData, Plot.class);
//		Plot plot = (Plot) NotebookEntryFactory.getNotebookEntry(plotId);
//		
//		final Notebook note = plot.getNotebook();
//		
//		validateNotFound(note != null, "Notebook is not found");
//		validateUnauthorized(isAdmin(sc) || note.isWriteAllowed(getUserId(sc)), "Permission denied");
//		
////		plot.setUserId(getSessionBean(sc).getUserId());
//		
//		plot.setTitle(incomingPlot.getTitle());
//		
//		plot.setDsQuery(incomingPlot.getDsQuery());
//		plot.setTbQuery(incomingPlot.getTbQuery());
//		
//		plot.setSortBy(incomingPlot.getSortBy());
//		plot.setColorBy(incomingPlot.getColorBy());
//		plot.setSplitBy(incomingPlot.getSplitBy());
//		
//		plot.setLabels(incomingPlot.getLabels());
//		
////		List<String> types = getParams().getList("plot-type");
////		List<String> columns = getParams().getList("plot-columns");
////		List<String> titles = getParams().getList("plot-series-title");
////		
////		getPlot().getSeries().clear();
////		int i = 0;
////		for (String type : types) {
////			List<String> oneColumns = allGroups("([^:\\\"]+)", columns.get(i));
////
////			PlotSeries cols = new PlotSeries();
////			cols.setPlotType(PlotType.parse(type, PlotType.POINTS));
////			
////			for (String col : oneColumns) {
////				if (notEmpty(col))
////					cols.addColumnExpr(col.trim());
////			}
////			
////			if (notEmpty(titles.get(i)))
////				cols.setLegend(titles.get(i));
////			getPlot().getSeries().add(cols);
////			
////			i++;
////		}
//		plot.setSeries(incomingPlot.getSeries());
//		
//		plot.getPlotStatus().setStarted(true); // TODO move it to model
//		plot.save();
//		
//		new PlotPrepareJob(plot.getId()).save();
//		
////		return new HttpResponse()
////			set
////			.toString();
//	}
	
	
	
	
	
	
	
//	@GET
//	@Path("data/parallel")
//	@Produces(MediaType.APPLICATION_JSON)
//	public StreamingOutput dataParallel(@Context SecurityContext sc,
//			@QueryParam("queryId") String queryId) throws IOException, ValidationException {
//		
//		LibsLogger.debug(PlotRest.class, "Data for queryId ", queryId);
//		
//		assertTrue(queryId != null, "queryId or plotIndex not specified");
//		
//		// retrieving plot data
//		final PlotParallelCoordinatesEntry plot = PlotFactory.getPlotParallelCoordinates(new UUID(queryId));
//		final ConnectorList plotData = plot.getDataIter();
//		final Notebook note = plot.getNotebook();
//		
//		validateNotFound(note != null, "Notebook is not found");
//		validateUnauthorized(isAdmin(sc) || note.isReadAllowed(getUserId(sc)), "Permission denied");
//				
//		return ;
//	}
	
//	@GET
//	@Path("data")
//	@Produces(MediaType.APPLICATION_JSON)
//	public StreamingOutput data(@Context SecurityContext sc,
//			@QueryParam("queryId") String queryId,
//			@QueryParam("plotNr") final int plotNr,
//			@QueryParam("type") final String type
//			) throws IOException, ValidationException {
//		LibsLogger.debug(PlotRest.class, "Data for queryId ", queryId, " plotNr ", plotNr);
//		
//		assertTrue(queryId != null, "queryId or plotIndex not specified");
//		
//		// retrieving plot data
//		final DataType dataType = notEmpty(type) ? DataType.valueOf(type.toUpperCase()) : DataType.JSON;
//		final Plot plot = PlotFactory.getPlot(new UUID(queryId));
//		final ConnectorList plotData = plot.getDataIter();
//		final ColorPalette palette = ColorPalette.getColorPalette(getSessionBean(sc).getUserId(), ColorPalette.defaultPalette());
//		final PlotType plotType = plot.getSeries().size() > 0 ? plot.getSeries().get(0).getPlotType() : null;
//		final Notebook note = plot.getNotebook();
//		
//		validateNotFound(note != null, "Notebook is not found");
//		validateUnauthorized(isAdmin(sc) || note.isReadAllowed(getUserId(sc)), "Permission denied");
//				
////		if (plotType == PlotType.PARALLEL_COORDINATES) {
////			return ;
////		} else if (plotType == PlotType.SCATTERPLOT_MATRIX) {
////			return new StreamingOutput() {
////		        public void write(OutputStream output) throws IOException, WebApplicationException {
////		            try {
////						Writer writer = new OutputStreamWriter(output);
////
////						// columns names
////						for (int i = 0; i < plot.getSeries().size(); i++) {
////							if (i > 0)
////								writer.write(",");
////							writer.write(String.valueOf(plot.getSeries().get(i)));
////						}
////						writer.write("\n");
////						
////						for (Row row : plotData) {
////							boolean firstColumn = true;
////							for (String colName : plot.getAllColumns()) {
////								if (!firstColumn) {
////									writer.write(",");
////									firstColumn = false;
////								}
////								writer.write(row.get(colName).toString());
////							}
////							writer.write("\n");
////						}
////												
////						writer.flush();
////						writer.close();
////		            } catch (Exception e) {
////		            	LibsLogger.error(PlotRest.class, "Cannot prepare response", e);
////		                throw new WebApplicationException(e);
////		            }
////		        }
////		    };
////		} else 
//		
//		
////		return null;
//	}

//	@GET
//	@Path(REFRESH)
//	@Produces(MediaType.APPLICATION_JSON)
//	public String refresh(@Context SecurityContext sc,
//			@QueryParam("queryId") String queryId,
//			@QueryParam("plotNr") int plotNr,
//			@QueryParam("previous") boolean previous,
//			@QueryParam("next") boolean next) throws IOException, ValidationException {
//		
//		assertTrue(queryId != null, "queryId not specified");
//		
//		final String panelId = queryId;
//		final PigScript query = PigScript.getQuery(queryId);
//		
//		assertTrue(query != null, "cannot find query with id " + queryId);
//		
//		if (next)
//			plotNr = plotNr + 1;
//		
//		if (previous)
//			plotNr = plotNr - 1;
//		
//		if (plotNr == 1)
//			addToResponse(new OpDisable("#" + panelId + " #button-previous"));
//		else
//			addToResponse(new OpEnable("#" + panelId + " #button-previous"));
//		
//		addToResponse(new OpSetVal("#" + panelId + " .plotNr", plotNr));
//		
//		final Plot plot = Plot.getPlot(new UUID(queryId));
//				
//		if (plot.getNrPoints() > 10000) {
//			
//			addToResponse(new OpSet("#figure-" + query.getId(), 
//				new HtmlCanvas().img(src("/" + IMAGE_URL + "?" + UrlUtilities.toQueryString("queryId", queryId, "plotNr", plotNr, "type", 
//						"png", "dummy", UUID.random().getId()))).toHtml()));
////				addToResponse(new OpsFactory.opAjax(STATIC_URL, "queryId", queryId, "plotNr", plotNr, "type", "png", "plotName", plot.getPlotName()));
//			addToResponse(new OpRemove(".figure-" + panelId + "-title"));
//				
//		} else {
//			
//			// set title
//			if (notEmpty(plot.getTitle()))
//				addToResponse(new OpSet(".figure-" + panelId + "-title", plot.getTitle()));
//			
//			addToResponse(new OpRemove("#figure-" + query.getId() + " img"));
//			
//			if (plot.getPlotType() == PlotType.LINES)
//		
//	//			addToResponse(new OpJs(JqPlotFactory.createJqPlot(plot, plotNr)));
//				addToResponse(new OpJs(WebPlotsFactory.createLines(plot, plotNr)));
//			
//			else if (plot.getPlotType() == PlotType.POINTS)
//			
//				addToResponse(new OpJs(WebPlotsFactory.createPoints(plot, plotNr)));
//			
//			else if (plot.getPlotType() == PlotType.BOXES)
//				
//				addToResponse(new OpJs(WebPlotsFactory.createBoxes(plot, plotNr)));
//			
//			else if (plot.getPlotType() == PlotType.PARALLEL_COORDINATES)
//			
//				addToResponse(new OpJs(WebPlotsFactory.createParallelCoordinates(plot, plotNr)));
//			
//			else
//				throw new NotImplementedException();
//			
//		}
//		
//		return toString();
//	}
}
