package net.beanscode.model.notebook;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.weblibs.jobs.Job;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class RemoveNotebookEntriesJob extends Job {
	
	@Expose
	@NotNull
	@NotEmpty
	@AssertValid
	private UUID notebookId = null;
	
	@Expose
	@NotNull
	@NotEmpty
	@AssertValid
	private UUID userId = null;
	
	@Expose
	@NotNull
	@NotEmpty
	private List<UUID> entriesToRemove = null;

	public RemoveNotebookEntriesJob() {
		
	}
	
	public RemoveNotebookEntriesJob(Notebook notebook, List<UUID> entriesIds) {
		setUserId(notebook.getUserId());
		setNotebookId(notebook.getId());
		getEntriesToRemove().addAll(entriesIds);
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		for (UUID uuid : getEntriesToRemove()) {			
			NotebookEntry ne = NotebookEntryFactory.getNotebookEntry(uuid);
			
			if (ne != null) {
				ne.remove();
			}
		}
	}

	public UUID getNotebookId() {
		return notebookId;
	}

	public void setNotebookId(UUID notebookId) {
		this.notebookId = notebookId;
	}

	public UUID getUserId() {
		return userId;
	}

	public void setUserId(UUID userId) {
		this.userId = userId;
	}

	public List<UUID> getEntriesToRemove() {
		if (entriesToRemove == null)
			entriesToRemove = new ArrayList<UUID>();
		return entriesToRemove;
	}

	public void setEntriesToRemove(List<UUID> entriesToRemove) {
		this.entriesToRemove = entriesToRemove;
	}
}
