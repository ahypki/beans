package net.beanscode.web.view.settings.jobspanel;

import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.web.beta.BetaContent;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.BetaButtonGroup;
import net.beanscode.web.view.ui.TabsBetaComponent;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.url.Params;

import org.rendersnake.HtmlCanvas;
import org.rendersnake.Renderable;

import com.google.gson.JsonElement;
import com.google.gson.annotations.Expose;

public class JobsBetaContent extends BetaContent {
	
	@Expose
	private List<String> channels = null;
	
	public JobsBetaContent() {
		
	}
	
	public JobsBetaContent(BeansComponent parent) {
		super(parent);
	}
		
	@Override
	protected void renderHeader(HtmlCanvas html) throws IOException {
		html
			.h1()
				.content("Jobs");
	}
	
	protected void renderBreadcrumb(HtmlCanvas html) throws IOException {
		
	};
	
	@Override
	protected void renderContent(HtmlCanvas html) throws IOException {
		assertTrue(isAdmin(), "You have no rights to see this content");
		
		html
			.h3()
				.content("Running/queded jobs")
			.p()
				.content("The list of all background top 50 jobs (not only the Queries from Notebooks). It gives an idea what is the load for the server.")
			.render(new TabsBetaComponent(this) {
				
				@Override
				public int getTabsCount() {
					return getChannels().size();
				}
				
				@Override
				public String getTabName(int tabIndex) {
					return getChannels().get(tabIndex);
				}
				
				@Override
				public Renderable getTabHtml(int tabIndex) throws IOException {
					return new JobsTable(JobsBetaContent.this, getChannels().get(tabIndex));
				}
				
				@Override
				protected OpList getAdditionalOpListOnTabClick(int tabIndex) {
					String channel = getChannels().get(tabIndex);
					return new OpList()
						.add(new OpComponentClick(JobsTable.class, channel, "refresh", new Params().add("channel", channel)));
				}
			})
			
			.render(new JobsWorkersPanel(this))
			;
	}

	private List<String> getChannels() {
		if (channels == null) {
			try {
				channels = new ApiCallCommand(this)
					.setApi("api/jobs/channels")
					.run()
					.getResponse()
					.getAsList("", String.class);
			} catch (IOException e) {
				LibsLogger.error(JobsBetaContent.class, "Cannot get channels list", e);
			}
		}
		return channels;
	}

	private void setChannels(List<String> channels) {
		this.channels = channels;
	}

	@Override
	protected BetaButtonGroup getMenu() throws IOException {
		return null;
	}

	@Override
	protected String getMenuName() throws IOException {
		return null;
	}
}
