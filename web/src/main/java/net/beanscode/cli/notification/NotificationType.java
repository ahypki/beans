package net.beanscode.cli.notification;

public enum NotificationType {
	INFO, 
	QUESTION,
	OK,
	WARN,
	ERROR
}
