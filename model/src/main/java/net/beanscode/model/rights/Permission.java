package net.beanscode.model.rights;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang.NotImplementedException;

import net.beanscode.model.BeansDTNObject;
import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.DatasetFactory;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.dataset.TableFactory;
import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.notebook.NotebookFactory;
import net.hypki.libs5.db.db.weblibs.MutationList;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.weblibs.db.WeblibsObject;
import net.hypki.libs5.weblibs.search.IndexJob;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class Permission extends WeblibsObject<Permission> {
	
	public static final String COLUMN_FAMILY = "permission";

	/**
	 * Notebook, Dataset or Table ID
	 */
	@Expose
	@NotNull
	@AssertValid
	private UUID id = null;
	
	@Expose
	@NotNull
	@AssertValid
	private String clazz = null;
	
	@Expose
//	@NotNull
	@AssertValid
	private Map<String, Boolean> groupRead = null;
	
	@Expose
//	@NotNull
	@AssertValid
	private Map<String, Boolean> groupWrite = null;
	
	private BeansDTNObject beansDTN = null;

	public Permission() {
		
	}
	
	public Permission(BeansDTNObject obj) {
		setId(obj.getId());
		setClazz(obj.getClass().getName());
	}
	
	public Permission(Notebook notebook) {
		setId(notebook.getId());
		setClazz(Notebook.class.getName());
	}
	
	public Permission(Dataset dataset) {
		setId(dataset.getId());
		setClazz(Dataset.class.getName());
	}
	
	public Permission(Table table) {
		setId(table.getId());
		setClazz(Table.class.getName());
	}

	@Override
	public String getColumnFamily() {
		return COLUMN_FAMILY;
	}

	@Override
	public String getKey() {
		return getClazz() + getId().getId();
	}
	
	@Override
	public MutationList getSaveMutations() {
		return super.getSaveMutations()
				.addInsertMutations(new IndexJob(getClazz(), getId()));
	}
	
	public void setReadAllowed(UUID groupId, boolean readAllowed) {
		if (!readAllowed)
			getReadPermissions().remove(groupId);
		else
			getReadPermissions().put(groupId.getId(), readAllowed);
	}
	
	public void setWriteAllowed(UUID groupId, boolean writeAllowed) {
		if (writeAllowed)
			setReadAllowed(groupId, true);

		if (!writeAllowed)
			getWritePermissions().remove(groupId);
		else
			getWritePermissions().put(groupId.getId(), writeAllowed);
	}
	
	public boolean isReadAllowed(UUID groupId) {
		Boolean perm = getReadPermissions().get(groupId);
		return perm != null ? perm : false;
	}
	
	public BeansDTNObject getBeansDTNObject() throws IOException {
		if (beansDTN != null)
			return beansDTN;
		
		if (isDataset()) {
			beansDTN = DatasetFactory.getDataset(getId());
		} else if (isTable()) {
			beansDTN = TableFactory.getTable(getId());
		} else if (isNotebook()) {
			beansDTN = Notebook.getNotebook(getId());
		} else
			throw new NotImplementedException();
		
		return beansDTN;
	}
	
	public boolean isReadAllowedForUser(UUID userId) {
		for (Group group : iterateReadAllowedGroups()) {
			if (group.containsUser(userId))
				return true;
		}
		return false;
	}
	
	public boolean isWriteAllowedForUser(UUID userId) {
		for (Group group : iterateWriteAllowedGroups()) {
			if (group.containsUser(userId))
				return true;
		}
		return false;
	}
	
	public boolean isWriteAllowed(UUID groupId) {
		Boolean perm = getWritePermissions().get(groupId);
		return perm != null ? perm : false;
	}
	
	public boolean isDataset() {
		return getClazz().equals(Dataset.class.getName());
	}
	
	public boolean isTable() {
		return getClazz().equals(Table.class.getName());
	}
	
	public boolean isNotebook() {
		return getClazz().equals(Notebook.class.getName());
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	private Map<String, Boolean> getReadPermissions() {
		if (groupRead == null)
			groupRead = new HashMap<>();
		return groupRead;
	}
	
	private Map<String, Boolean> getWritePermissions() {
		if (groupWrite == null)
			groupWrite = new HashMap<>();
		return groupWrite;
	}

	public String getClazz() {
		return clazz;
	}

	public void setClazz(String permissionClass) {
		if (permissionClass.endsWith("Dataset")) {
			this.clazz = Dataset.class.getName();
		} else if (permissionClass.endsWith("Table")) {
			this.clazz = Table.class.getName();
		} else if (permissionClass.endsWith("Notebook")) {
			this.clazz = Notebook.class.getName();
		} else
			throw new net.hypki.libs5.utils.utils.ValidationException("Unknown class " + permissionClass);
	}

	public Iterable<UUID> getReadAllowedGroups() {
		return new Iterable<UUID>() {
			
			@Override
			public Iterator<UUID> iterator() {
				return new Iterator<UUID>() {
					private Iterator<String> keyIter = getReadPermissions().keySet().iterator();
					
					@Override
					public UUID next() {
						return new UUID(keyIter.next());
					}
					
					@Override
					public boolean hasNext() {
						return keyIter.hasNext();
					}
				};
			}
		};
	}
	
	public Iterable<Group> iterateReadAllowedGroups() {
		return new Iterable<Group>() {
			
			@Override
			public Iterator<Group> iterator() {
				return new Iterator<Group>() {
					private Iterator<String> keyIter = getReadPermissions().keySet().iterator();
					
					@Override
					public Group next() {
						try {
							return GroupFactory.getGroup(new UUID(keyIter.next()));
						} catch (IOException e) {
							LibsLogger.error(Permission.class, "Cannot get Group from DB", e);
							return null;
						}
					}
					
					@Override
					public boolean hasNext() {
						return keyIter.hasNext();
					}
				};
			}
		};
	}
	
	public Iterable<UUID> getWriteAllowedGroups() {
		return new Iterable<UUID>() {
			
			@Override
			public Iterator<UUID> iterator() {
				return new Iterator<UUID>() {
					private Iterator<String> keyIter = getWritePermissions().keySet().iterator();
					
					@Override
					public UUID next() {
						return new UUID(keyIter.next());
					}
					
					@Override
					public boolean hasNext() {
						return keyIter.hasNext();
					}
				};
			}
		};
	}
	
	public Iterable<Group> iterateWriteAllowedGroups() {
		return new Iterable<Group>() {
			
			@Override
			public Iterator<Group> iterator() {
				return new Iterator<Group>() {
					private Iterator<String> keyIter = getWritePermissions().keySet().iterator();
					
					@Override
					public Group next() {
						try {
							return GroupFactory.getGroup(new UUID(keyIter.next()));
						} catch (IOException e) {
							LibsLogger.error(Permission.class, "Cannot get Group from DB", e);
							return null;
						}
					}
					
					@Override
					public boolean hasNext() {
						return keyIter.hasNext();
					}
				};
			}
		};
	}

	public void clearReadPermissions() {
		groupRead = null;
	}
	
	public void clearWritePermissions() {
		groupWrite = null;
	}
	
	public Table getAsTable() throws IOException {
		return (Table) this.getBeansDTNObject();
	}

	public Dataset getAsDataset() throws IOException {
		return (Dataset) this.getBeansDTNObject();
	}
	
	public Notebook getAsNotebook() throws IOException {
		return (Notebook) this.getBeansDTNObject();
	}
	
	private static Map<String, Boolean> clone(Map<String, Boolean> groups) {
		return JsonUtils.fromJson(JsonUtils.toJson(groups), Map.class);
	}
	
	public int getGroupReadSize() {
		return groupRead != null ? groupRead.size() : 0;
	}
	
	public int getGroupWriteSize() {
		return groupWrite != null ? groupWrite.size() : 0;
	}
	
	public void copyPermissions(Permission from) {
		this.groupRead = from != null ? clone(from.groupRead) : null;
		this.groupWrite = from != null ? clone(from.groupWrite) : null;
	}

	public void setRightsRecursively() throws IOException, ValidationException {
		if (this.isDataset()) {
			for (Table table : this.getAsDataset().iterateTables()) {
				Permission tablePerm = PermissionFactory.getPermission(Table.class.getName(), table.getId());
				
				if (tablePerm == null)
					tablePerm = new Permission(table);
				
				tablePerm.groupRead = clone(this.groupRead);
				tablePerm.groupWrite = clone(this.groupWrite);
				tablePerm.save();
			}
		} else if (this.isNotebook()) {
			
			Dataset ds = DatasetFactory.getDataset(this.getAsNotebook().getId());
			if (ds != null) {
				for (Table table : ds.iterateTables()) {
					Permission tablePerm = PermissionFactory.getPermission(Table.class.getName(), table.getId());
					
					if (tablePerm == null)
						tablePerm = new Permission(table);
					
					tablePerm.groupRead = clone(this.groupRead);
					tablePerm.groupWrite = clone(this.groupWrite);
					tablePerm.save();
				}
				
				ds.getPermission().copyPermissions(this);
				ds.save();
			}
			
			Notebook n = this.getAsNotebook();
			n.getPermission().copyPermissions(this);
			n.save();
			
			if (ds != null) {
				for (Table table : ds.iterateTables()) {
					table.index();
				}
				
				ds.index();
			}
			n.index();
		} else if (this.isTable()) {
			Table table = getAsTable();
			Dataset ds = table.getDataset();
			
			Notebook nt = Notebook.getNotebook(ds.getId());
		
			table.getPermission().copyPermissions(nt != null ? nt.getPermission() : ds.getPermission());
			table.getPermission().save();
			
//			table.getPermission().setReadAllowed(groupId, readAllowed); setReadPermission(ds.getPermission().getReadPermissions(), true);
		} else
			LibsLogger.error(Permission.class, "Not implemented case for " + this);
	}
}
