package net.beanscode.rest.notebook;

import java.io.IOException;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.quartz.SchedulerException;

import mjson.Json;
import net.beanscode.model.BeansSettings;
import net.beanscode.model.notebook.autoupdate.AutoUpdateFactory;
import net.beanscode.model.notebook.autoupdate.AutoUpdateJob;
import net.beanscode.model.notebook.autoupdate.AutoUpdateOption;
import net.beanscode.rest.BeansRest;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.string.StringUtilities;

@Path("api/autoupdate")
public class AutoUpdateRest extends BeansRest {
	
	@GET
    @Path("entry/default")
    @Produces(MediaType.APPLICATION_JSON)
	public Response getDefault(@Context SecurityContext sc, 
			@QueryParam("userId") UUID userId) throws IOException, ValidationException {
		validateBadRequest(userId != null, "User ID not specified");
		validateUnauthorized(isAdmin(sc) || userId.equals(getUserId(sc)), "Permission denied");
		
		AutoUpdateOption option = BeansSettings.getAutoUpdateEntryDefaultOption(userId);
		
		return Response
				.ok()
				.entity(Json
						.object()
						.set("option", option.name())
						.toString())
				.build();
	}
	
	@POST
    @Path("entry/default")
	public void setDefault(@Context SecurityContext sc, 
			@FormParam("userId") UUID userId,
			@FormParam("option") String option) throws IOException, ValidationException {
		validateBadRequest(userId != null, "User ID not specified");
		validateUnauthorized(isAdmin(sc) || userId.equals(getUserId(sc)), "Permission denied");
		
		BeansSettings.setAutoUpdateEntryDefaultOption(userId, AutoUpdateOption.valueOf(option));
	}

	@GET
    @Path("setting")
    @Produces(MediaType.APPLICATION_JSON)
	public Response get(@Context SecurityContext sc, 
			@QueryParam("userId") UUID userId) throws IOException, ValidationException {
		validateBadRequest(userId != null, "User ID not specified");
		validateUnauthorized(isAdmin(sc) || userId.equals(getUserId(sc)), "Permission denied");
		
		String[] every = StringUtilities.split(BeansSettings.getAutoUpdateIntervalMinutes(userId), ":");
		
		mjson.Json json = Json.object();
		
		return Response
				.ok()
				.entity(Json
						.object()
						.set("every", every[0])
						.set("type", every[1])
						.toString())
				.build();
	}
	
	@POST
    @Path("setting")
	public void set(@Context SecurityContext sc,
			@FormParam("userId") UUID userId,
			@FormParam("every") int every,
			@FormParam("type") String type) throws IOException, ValidationException, SchedulerException {
		validateBadRequest(userId != null, "User ID not specified");
		validateUnauthorized(isAdmin(sc) || userId.equals(getUserId(sc)), "Permission denied");
		
		BeansSettings.setAutoUpdateIntervalMinutes(userId, every, type);
	}
	
	@GET
    @Path("next")
    @Produces(MediaType.APPLICATION_JSON)
	public Response nextAlarm(@Context SecurityContext sc,
			@QueryParam("userId") UUID userId) throws IOException, ValidationException {
		validateBadRequest(userId != null, "User ID not specified");
		validateUnauthorized(isAdmin(sc) || userId.equals(getUserId(sc)), "Permission denied");
		
		SimpleDate nextFireAlarm = AutoUpdateFactory.getNextFireAlarmForAutoUpdates(userId);
		
		return Response
				.ok()
				.entity(Json
						.object()
						.set("nextAlarm", nextFireAlarm != null ? nextFireAlarm.getTimeInMillis() : "")
						.toString())
				.build();
	}
	
	@POST
    @Path("start")
	public void start(@Context SecurityContext sc,
			@FormParam("userId") UUID userId) throws IOException, ValidationException {
		validateBadRequest(userId != null, "User ID not specified");
		validateUnauthorized(isAdmin(sc) || userId.equals(getUserId(sc)), "Permission denied");
		
		new AutoUpdateJob(userId)
			.save();
	}
}
