package net.beanscode.model.dataset;

import java.io.IOException;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.weblibs.jobs.Job;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class RemoveDatasetDependencies extends Job {
	
	@Expose
	@NotNull
	@NotEmpty
	private String dataset = null;
	
	public RemoveDatasetDependencies() {
		
	}

	public RemoveDatasetDependencies(Dataset dataset) {
		setDataset(dataset);
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		final Dataset ds = getDataset();
		
		for (Table tab : Table.iterateTables(ds.getId())) {
			if (tab != null && tab.getDatasetId().equals(ds.getId())) {
				LibsLogger.debug(RemoveDatasetDependencies.class, "Removing table ", tab.getName(), " from dataset ", ds.getName());
				tab.remove();
			}
		}
	}

	public Dataset getDataset() {
		return JsonUtils.fromJson(dataset, Dataset.class);
	}

	public void setDataset(Dataset dataset) {
		this.dataset = dataset.getData();
	}
}
