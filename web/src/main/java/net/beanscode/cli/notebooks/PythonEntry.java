package net.beanscode.cli.notebooks;

import net.beanscode.pojo.NotebookEntry;
import net.hypki.libs5.plot.gnuplot.GnuplotWrapper;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class PythonEntry extends NotebookEntry {

	private static final String META_CODE 		= "code";
	private static final String META_COMPILEDCODE 	= "compiledcode";

	public PythonEntry() {
		
	}
	
	public PythonEntry(Notebook notebook) {
		setNotebookId(notebook.getId());
		setUserId(notebook.getUserId());
	}

	public String getPythonCode() {
		return getMetaAsString(META_CODE, "");
	}
	
	public PythonEntry setPythonCode(String code) {
		setMeta(META_CODE, code);
		return this;
	}
	
	public String getPythonCompiledCode() {
		return getMetaAsString(META_COMPILEDCODE, "");
	}
	
	public PythonEntry setPythonCompiledCode(String code) {
		setMeta(META_COMPILEDCODE, code);
		return this;
	}
}
