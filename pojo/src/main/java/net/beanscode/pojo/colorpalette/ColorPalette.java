package net.beanscode.pojo.colorpalette;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

public class ColorPalette {

	@Expose
	private List<Integer> colors = null;
	
	public ColorPalette() {
		
	}

	public List<Integer> getColors() {
		if (colors == null)
			colors = new ArrayList<Integer>();
		return colors;
	}

	public ColorPalette setColors(List<Integer> colors) {
		this.colors = colors;
		return this;
	}
}
