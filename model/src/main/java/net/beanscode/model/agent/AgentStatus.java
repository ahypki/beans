package net.beanscode.model.agent;

public enum AgentStatus {
	UP,
	DOWN
}
