package net.beanscode.web.view.notebook;

import static net.beanscode.web.BeansWebConst.MAGIC_KEY;
import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;
import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.id;
import static org.rendersnake.HtmlAttributesFactory.type;

import java.io.IOException;

import javax.ws.rs.core.SecurityContext;

import org.rendersnake.HtmlAttributesFactory;
import org.rendersnake.HtmlCanvas;

import com.google.gson.JsonElement;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.notebooks.Notebook;
import net.beanscode.cli.notebooks.NotebookEntryGateway;
import net.beanscode.pojo.NotebookEntry;
import net.beanscode.web.view.dataset.MetaTableComponent;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.BetaButtonGroup;
import net.beanscode.web.view.ui.Link;
import net.beanscode.web.view.ui.modals.InputType;
import net.beanscode.web.view.ui.modals.ModalWindow;
import net.beanscode.web.view.ui.modals.OpBeansError;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.beanscode.web.view.ui.modals.OpModal;
import net.beanscode.web.view.ui.modals.OpModalQuestion;
import net.beanscode.web.view.users.PermissionPanel;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.buttons.InputButton;
import net.hypki.libs5.pjf.components.labels.DangerLabel;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.components.ops.OpComponentTimer;
import net.hypki.libs5.pjf.op.OpAppend;
import net.hypki.libs5.pjf.op.OpDialogClose;
import net.hypki.libs5.pjf.op.OpHide;
import net.hypki.libs5.pjf.op.OpJs;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpRemove;
import net.hypki.libs5.pjf.op.OpReplace;
import net.hypki.libs5.pjf.op.OpSet;
import net.hypki.libs5.pjf.op.OpShow;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.api.APICallType;
import net.hypki.libs5.utils.string.RandomUtils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.url.HttpResponse;
import net.hypki.libs5.utils.url.Params;
import net.hypki.libs5.utils.utils.AssertUtils;

public class NotebookPanel extends BeansComponent {
	
	private Notebook notebook = null;

	public NotebookPanel() {
		
	}
	
	public NotebookPanel(Component parent, Notebook notebook) {
		super(parent);
		setNotebook(notebook);
	}
	
	//	public OpList addPlot() throws ValidationException, IOException {
	//		Notebook notebook = getNotebook();
	//		
	//		Plot plot = new Plot(notebook);
	//		plot.setUserId(getSessionBean(sc).getUserId());
	//		plot.save();
	//		
	//		notebook.getEntries().add(plot.getId());
	//		notebook.save();
	//		
	//		PlotPanel plotPanel = new PlotPanel();
	//		plotPanel.setNotebookEntry(plot);
	//		
	//		return new OpList()
	//			.add(new OpAppend("#main #notebook-entries", plotPanel.toHtml()));
	//	}
		
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		if (getNotebook() == null) {
			return;
		}
		
		BetaButtonGroup menu = new BetaButtonGroup();
		menu
			.add(new Button()
					.setAwesomeicon("fa fa-edit")
					.addButton(new Button("Add entry")
							.add(new OpComponentClick(this, "onInsert", new Params().add("notebookId", getNotebook().getId().getId()))))
					.addButton(new Button().setSeparator(true))
					
					.addButton(new Button("Edit title", new OpComponentClick(this, "onEditTitle", new Params()
							.add("notebookId", getNotebook().getId())))
									.setAwesomeicon("edit"))

					.addButton(new Button("Edit description", new OpComponentClick(this, "onEditDescription", new Params()
							.add("notebookId", getNotebook().getId())))
									.setAwesomeicon("edit"))
					
					.addButton(new Button("Edit parameters", new OpComponentClick(this, "onParams", new Params()
																											.add("notebookId", getNotebook().getId())))
									.setAwesomeicon("edit"))
					
					.addButton(new Button().setSeparator(true))
					
					.addButton(new Button("Rearrange entries")
									.add(new OpComponentClick(NotebookRearrangeEntriesBetaContent.class,
											UUID.random(), 
											"onShow", 
											new Params().add("notebookId", getNotebook().getId())))
									.setAwesomeicon("edit"))
					
					.addButton(new Button().setSeparator(true))
					
					.addButton(new Button("Permissions", new OpComponentClick(this, "onOpenPermissions", new Params()
																											.add("notebookId", getNotebook().getId())))
									.setAwesomeicon("edit"))
					
					.addButton(new Button().setSeparator(true))
					
		//			.addButton(new Button("Refresh all plots")
		//				.setAwesomeicon("refresh")
		//				.add(new OpComponentClick(this, "onRefresh", 
		////							OpsFactory.opConfirmation("Reload", "Do you really want to reload the whole Notebook?"),
		//								new Params()
		//									.add("notebookId", getNotebook().getId()) 
		//									.add("userId", getNotebook().getUserId()))))
					
					.addButton(new Button("Reload all entries")
							.setAwesomeicon("refresh")
							.add(new OpModalQuestion("Reload", 
									"Do you really want to reload the whole Notebook? This may take a while.", 
									new OpComponentClick(this, 
											"onReload", 
											new Params()
												.add("notebookId", getNotebook().getId()) 
												.add("userId", getNotebook().getUserId())))))
					
					.addButton(new Button().setSeparator(true))
					
					.addButton(new Button("Properties", new OpComponentClick(this, 
															"onProperties",
															new Params()
																.add("notebookId", getNotebook().getId()) 
																.add("userId", getNotebook().getUserId())))
									.setAwesomeicon("gears"))
					
					.addButton(new Button().setSeparator(true))
					
					.addButton(new Button("Clear notebook")
									.setAwesomeicon("eraser")
									.add(new OpModalQuestion("Removing ALL entries in the notebook", 
											"Are you sure you want to remove all entries from the notebook <b>" + 
													getNotebook().getName() + "</b>?", 
											new OpComponentClick(this, 
													"clearNotebook",
													new Params()
														.add("notebookId", getNotebook().getId()) 
														.add("userId", getNotebook().getUserId())))))
					
		    		.addButton(new Button("Remove notebook")
		    						.setAwesomeicon("trash")
		    						.add(new OpModalQuestion("Removing notebook", 
		    								"Do you really want to delete the notebook: <b>" + getNotebook().getName() + "</b>? "
		    										+ "(That operation cannot be reverted)", 
		    								new OpComponentClick(this, 
		    										"removeNotebook",
		    										new Params().add("notebookId", getNotebook().getId())))
		    								)
								)
					)
				.add(new InputButton()
					.setName("notebook-filter")
					.setTooltip("Filter entries...")
					.add(new OpComponentTimer(this, "onFilter", 300, new Params().add("notebookId", getNotebook().getId()))))
			;
		
		
		getOpList()
			.add(new OpAppend(".notebook-title", menu.toHtml()));
		
		html
			.div(id(getId()))
//				.render(new NotebookBtnGroup(getNotebook(), ButtonSize.NORMAL, this.getId()))
//				.input(id("notebook-filter")
//						.name("notebook-filter")
//						.class_("notebook-filter")
//						.type("text")
//						.add("placeholder", "Filter entries...")
//						.onKeyup(new OpComponentTimer(this, "onFilter", 300, new Params().add("notebookId", getNotebook().getId())).toStringOnclick()))
				.div(id("notebook-entries"));
		
		final int loadedMax = 20;
		int loaded = 0;
		int timer = 0;
		UUID lastEntryId = null;
		for (UUID entryID : getNotebook().getEntries()) {
			NotebookEntry ne = null;
			try {
				ne = NotebookEntryGateway.getNotebookEntry(new ApiCallCommand(this)
															.setApi("api/notebook/entry/get")
															.addParam("entryId", entryID.getId())
															.run()
															.getResponse()
															.getAsJson()
															.getAsJsonObject());
				
				if (ne == null) {
					LibsLogger.error(NotebookPanel.class, "Cannot find in DB ", NotebookEntry.class.getSimpleName(), entryID);
					continue;
				}
				
				Component editor = NotebookEntryEditorFactory.getEditorPanel(this, ne.getClass());
				
				if (editor == null) {
					html
						.div()
							.render(new DangerLabel("Cannot render entry " + entryID + ": Cannot find the editor class"))
						._div();
					LibsLogger.error(NotebookPanel.class, "Cannot create editor class for entry ", ne.getClass(), 
							", is the proper plugin enabled?");
					continue;
				}
				
				NotebookEntryEditorPanel notebookEditor = (NotebookEntryEditorPanel) editor;
				notebookEditor.setNotebookEntry(ne);
				
				HtmlCanvas html2 = new HtmlCanvas();
				html2.render(notebookEditor);
				
				getOpList()
					.add(notebookEditor.getInitOpList());
				
				if (++loaded < loadedMax) {
					getOpList()
						.add(new OpComponentTimer(notebookEditor, 
									"onRefresh", 
									timer,
									new Params()
										.add("entryId", ne.getId())
										.add(MAGIC_KEY, getParams().getString(MAGIC_KEY))));
				} else if (loaded == loadedMax) {
					html2
						.span(id("notebook-load-more")
								.onClick(new OpComponentClick(this, 
									"onLoadMore",
									new Params()
										.add("notebookId", getNotebook().getId().getId())
										.add("lastEntryId", ne.getId().getId()))
							.toStringOnclick()))
						._span();
					lastEntryId = ne.getId();
				}
				
				html
					.write(html2.toHtml(), false);
				
				timer += 300;
			} catch (Exception e) {
				LibsLogger.error(NotebookPanel.class, "Cannot render an Entry ", e);
				
				if (ne != null)
					html
						.div()
							.render(new DangerLabel("Cannot render entry " + (ne != null ? ne.getName() : "") 
								+ " (" + ne.getType() + ", " + entryID.toString(" - ") + "): " + e.getMessage()))
						._div();
				else
					html
						.div()
							.render(new DangerLabel("Cannot render entry " + entryID + ": " + e.getMessage()))
						._div();
			}
		}
		
		// loading all entries on reaching the end
		html
			.span(id("notebook-load-more")
					.onClick(new OpComponentClick(this, 
						"onLoadMore",
						new Params()
							.add("notebookId", getNotebook().getId().getId())
							.add("lastEntryId", lastEntryId)
							.add("toend", true)
							.add(MAGIC_KEY, getParams().getString(MAGIC_KEY))
							)
				.toStringOnclick()))
			._span();
				
		html
				._div() // #notebook-entries
				
				.div(class_("new-entry"))
					.render(new Link("Add new entry", 
							new OpComponentClick(this, 
									"onNewEntry",
									new Params()
										.add("notebookId", getNotebook().getId().getId()))))
				._div()
				
			._div() // #id
			;
		
		// destroy existing D3 instances, in order to create the new ones from the current notebook
		getOpList().add(new OpJs("destroyD3();"));
	}
	
	private OpList onNewEntry() throws IOException {
		HtmlCanvas html = new HtmlCanvas();
		html
			.h2()
				.content("Add new entry")
				.render(new AddNewEntryPanel(this, "onAddEntry", getNotebook().getId()));
		
		return new OpList()
				.add(new OpSet("#" + getId() + " .new-entry", html.toHtml()));
	}
	

	public OpList onEditDescription() throws IOException, ValidationException {
		ModalWindow modal = new ModalWindow(this);
		modal.setTitle("Editing description");
		modal.add("Description", 
				InputType.TEXT, 
				"notebook-desc", 
				getNotebook().getDescriptionNotNull());
		modal.add(new Button("Save")
				.add(new OpComponentClick(this.getClass(), 
						modal.getId(), 
						"onDescSave",
						new Params()
							.add("notebookId", getNotebook().getId().getId())))
				.add(modal.onClose()));
		modal.add(new Button("Cancel")
				.add(modal.onClose()));
		return new OpList()
			.add(new OpModal(modal));
	}
	

	
	
	public OpList onDescSave() throws IOException, ValidationException {
		String newDesc = getParams().getString("notebook-desc");
		
		new ApiCallCommand(this)
			.setApi("api/notebook/description")
			.addParam("notebookId", getNotebook().getId())
			.addParam("description", newDesc)
			.run();
		
		return new OpList()
			.add(new OpBeansInfo("Description saved"));
	}
	
	private OpList onProperties() throws IOException {
		ModalWindow modal = new ModalWindow(this);
		modal
			.setTitle("Notebook properties")
			.add(new Button("Close")
					.add(modal.onClose()))
			.getContent()
				.render(new NotebookProperties(getNotebook()));
				
		return new OpList()
				.add(new OpModal(modal));
	}

	private OpList onOpenPermissions() throws IOException {
		ModalWindow modal = new ModalWindow(this);
		modal
			.setTitle("Permissions")
			.add(new Button("Close")
					.add(modal.onClose()))
			.getContent()
				.render(new PermissionPanel(this, getNotebook().getId(), "Notebook"))
				.render(new ShareByLink(this, getNotebook().getId()));
		
		return new OpList()
			.add(new OpModal(modal))
//			.add(new net.hypki.libs5.pjf.components.ops.OpDialogOpen(
//					"Permissions", 
//					new PermissionPanel(this, getNotebook().getId(), "Notebook").toHtml()
//					)
//					.setOnCancelBtnCaption("Close"))
			;
	}
	
	
	
	private OpList onAddEntry() throws IOException, ValidationException {
		final String destinationNotebookId = getParams().getString("destinationNotebookId");
		final String destinationAfterEntryId = getParams().getString("destinationAfterEntryId");
		
		final String sourceNotebookId = getParams().getString("sourceNotebookId");
		String sourceNotebookEntryClass = getParams().getString("sourceNotebookEntryClass");
		final String sourceEntryId = getParams().getString("sourceEntryId");
			
		if (sourceNotebookId != null 
				&& nullOrEmpty(sourceNotebookEntryClass)
				&& nullOrEmpty(sourceEntryId)) {
			
			// clone whole notebook
			OpList opList = new OpList();
			HttpResponse resp = new ApiCallCommand(getSessionBean())
				.setApi("api/notebook/add")
				.addParam("--notebookId", destinationNotebookId)
				.addParam("--selectedNotebookId", sourceNotebookId)
				.run()
				.getResponse();
			
			if (resp.getAsJson() != null) {
				for (JsonElement je : resp.getAsJson().getAsJsonObject().get("entries").getAsJsonArray()) {
					NotebookEntry ne = NotebookEntryGateway.getNotebookEntry(new ApiCallCommand(getSessionBean())
											.setApi("api/notebook/entry/get")
											.addParam("entryId", je.getAsString())
											.run()
											.getResponse()
											.getAsJson()
											.getAsJsonObject());
	//				
	//				getNotebook().getEntries().add(ne.getId());
	//				
					NotebookEntryEditorPanel nePanel = NotebookEntryEditorFactory.getEditorPanel(this, ne.getClass());
					nePanel.setNotebookEntry(ne);
					nePanel.setParams(getParams());
					nePanel.setSessionBean(getSecurityContext());
	
					opList.add(new OpAppend("#notebook-entries", nePanel.toHtml()));
				}
			}
			
			return opList
					.add(new OpBeansInfo("Entries added successfully"))
					.add(new OpDialogClose());
			
		} else if (notEmpty(sourceNotebookEntryClass)) {
			
			try {
				// adding new entry (not a clone)
				HttpResponse resp = new ApiCallCommand(getSessionBean())
					.setApi("api/notebook/entry/add")
					.addParam("--notebookId", destinationNotebookId)
					.addParam("--notebookEntryClass", sourceNotebookEntryClass)
//					.addParam("--refEntryId", destinationAfterEntryId)
//					.addParam("--sourceEntryId", sourceEntryId)
					.run()
					.getResponse();
				
				if (nullOrEmpty(sourceNotebookEntryClass)) {
					NotebookEntry sourceEntry = NotebookEntryGateway.getNotebookEntry(new ApiCallCommand(this.getSessionBean())
													.setApi("api/notebook/entry/get")
													.addParam("entryId", sourceEntryId)
													.run()
													.getResponse()
													.getAsJson()
													.getAsJsonObject());
					sourceNotebookEntryClass = sourceEntry.getClass().getName();
				}
				
				NotebookEntry ne = (NotebookEntry) resp.getAsObject(Class.forName(sourceNotebookEntryClass));
				
				getNotebook().getEntries().add(ne.getId());
				
				NotebookEntryEditorPanel nePanel = NotebookEntryEditorFactory.getEditorPanel(this, ne.getClass());
				nePanel.setNotebookEntry(ne);
				nePanel.setParams(getParams());
				nePanel.setSessionBean(getSecurityContext());
				
//				ModalWindow modal = new ModalWindow(this);
//				modal
//					.add(new Button("Close")
//							.add(modal.onClose()))
//					.getContent()
//						.render(nePanel);
				
				return new OpList()
					.add(new OpAppend("#notebook-entries", nePanel.toHtml()))
					.add(new OpBeansInfo("Entry " + ne.getTypeShort()  + " added"))
//					.add(new OpModal(modal))
//					.add(new OpDialogClose())
					;
			} catch (CLIException | ClassNotFoundException e) {
				LibsLogger.error(NotebookPanel.class, "Cannot create instance of the Entry", e);
				
				return new OpList()
					.add(new OpBeansError("Cannot add/clone the entry"));
			}
			
		} else if (notEmpty(sourceEntryId)) {
			
			try {
				// cloning an existing entry
				HttpResponse resp = new ApiCallCommand(getSessionBean())
					.setApi("api/notebook/entry/clone/entry")
					.addParam("--notebookId", destinationNotebookId)
					.addParam("--sourceEntryId", sourceEntryId)
//					.addParam("--refEntryId", destinationAfterEntryId)
//					.addParam("--sourceEntryId", sourceEntryId)
					.run()
					.getResponse();
				
				if (nullOrEmpty(sourceNotebookEntryClass)) {
					NotebookEntry sourceEntry = NotebookEntryGateway.getNotebookEntry(new ApiCallCommand(getSessionBean())
													.setApi("api/notebook/entry/get")
													.addParam("entryId", sourceEntryId)
													.run()
													.getResponse()
													.getAsJson()
													.getAsJsonObject());
					sourceNotebookEntryClass = sourceEntry.getClass().getName();
				}
								
				NotebookEntry ne = (NotebookEntry) resp.getAsObject(Class.forName(sourceNotebookEntryClass));
				
				getNotebook().getEntries().add(ne.getId());
				
				NotebookEntryEditorPanel nePanel = NotebookEntryEditorFactory.getEditorPanel(this, ne.getClass());
				nePanel.setNotebookEntry(ne);
				nePanel.setParams(getParams());
				nePanel.setSessionBean(getSecurityContext());

//				ModalWindow modal = new ModalWindow(this);
//				modal
//					.add(new Button("Close")
//							.add(modal.onClose()))
//					.getContent()
//						.render(nePanel);
				
				return new OpList()
					.add(new OpAppend("#notebook-entries", nePanel.toHtml()))
					.add(new OpBeansInfo("Entry " + ne.getTypeShort()  + " added"))
//					.add(new OpModal(modal))
//					.add(new OpDialogClose())
					;
			} catch (CLIException | ClassNotFoundException e) {
				LibsLogger.error(NotebookPanel.class, "Cannot create instance of the Entry", e);
				
				return new OpList()
					.add(new OpBeansError("Cannot add/clone the entry"));
			}
			
		} else {
			
			LibsLogger.error(NotebookPanel.class, "Unknown case for adding/cloning a new entry");
			
			return new OpList()
				.add(new OpBeansError("Cannot add/clone the entry"));
						
		}
	}

	public OpList importNotebook(SecurityContext sc, Params params) throws IOException, ValidationException {
//		final Notebook notebook = Notebook.getNotebook(params.getString("notebookId"));
		
		HtmlCanvas html = new HtmlCanvas()
			.b()
				.content("Notebook's entries to import:")
			.br()
			.input(type("hidden")
					.name("notebookId")
					.value(params.getString("notebookId")))
			.textarea(HtmlAttributesFactory.add("style", "width: 100%; height: 100%;", false)
					.name("beans-import")
//					.onClick(new OpList()
//							.add(new OpComponentClick(this, "importNotebook2", params))
//							.toStringOnclick())
						)
				.content("");
		
		return new OpList()
//			.add(new OpDialogOpen(null, "Simple notebook export", html.toHtml()))
			.add(new net.hypki.libs5.pjf.components.ops.OpDialogOpen(this, "importNotebook2", "Importing entries into Notebook", html.toHtml()));
	}
	
	public OpList addTable(SecurityContext sc, Params params) throws IOException, ValidationException {
		final String notebookId = params.getString("notebookId");
		
		AssertUtils.assertTrue(false, "not implemented");
//		Notebook notebook = Notebook.getNotebook(notebookId);
//				
//		TableEntry newTable = new TableEntry(getSessionBean(sc).getUserId(), new UUID(notebookId), "New table", "");
//		newTable.save();
//		
//		notebook.getEntries().add(newTable.getId());
//		notebook.save();
		
		return new OpList()
			.add(new OpAppend("#main #notebook-entries", new TablePanel(null /*newTable*/).toHtml()));
	}
	
	public OpList clearNotebook(SecurityContext sc, Params params) throws IOException, ValidationException {
//		List<UUID> toRemove = new ArrayList<>();
//		toRemove.add(new UUID(params.getString("notebookId")))
		
		new ApiCallCommand(getSessionBean(sc))
			.setApi("api/notebook/clear")
			.addParam("notebookId", params.getString("notebookId"))
			.run();
		
		return new OpList()
			.add(new OpRemove("#notebook-entries .notebook-entry"))
			.add(new OpBeansInfo("Notebook cleared"))
//			.add(new OpComponentClick(NotebookContent.class, UUID.random(), "showContent", 
//					new Params()
//						.add("userId", getUserId())
//						.add("notebookId", params.getString("notebookId"))))
						;
	}
	
	public OpList removeNotebook(SecurityContext sc, Params params) throws IOException, ValidationException {
		final String notebookId = params.getString("notebookId");
		
		new ApiCallCommand(getSessionBean(sc))
			.setApi("api/notebook/remove")
			.addParam("notebookId", notebookId)
			.run();
		
		return new OpList()
			.add(new OpBeansInfo("Notebook removed"))
//			.add(new OpComponentClick(AllNotebooksContent.class, 
//					UUID.random(), 
//					"showContent",
//					new Params().add("ignore", notebookId)))
					;
	}
	

	private OpList onRearrange() throws CLIException, IOException {
		return new OpList()
			.add(new net.hypki.libs5.pjf.components.ops.OpDialogOpen(this, 
					"onRearrangeSave", 
					"Rearranging entries for Notebook " + getNotebook().getName(), 
					new NotebookRearrangeEntriesPanel(this, getNotebook().getId()).toHtml(), 
					640, 
					640));
	}
	
	private OpList onFilter() throws IOException {
		final String filter = getParams().getString("notebook-filter", null);
		
		final boolean isEMpty = StringUtilities.nullOrEmpty(filter);
		if (isEMpty)
			return new OpList()
					.add(new OpShow(".notebook-entry"));
		
		OpList ops = new OpList()
				.add(new OpHide(".notebook-entry"));
		
		// TODO implement it
//		for (NotebookEntry ne : new EntrySearch(getSessionBean()).run(getUserId().getId(), getNotebook().getId(), filter, 0, 100).getNotebookEntries()) {
//			ops.add(new OpShow("#" + ne.getId().getId()));
//		}
		
		ops.add(new OpJs("onScrollShow();"));
		
		return ops;
	}
	

	public OpList onParams(SecurityContext sc, Params params) throws IOException {
//		final Notebook notebook = new ApiCallCommand(getSessionBean(sc))
//									.setApi("api/notebook/get")
//									.addParam("notebookId", params.getString("notebookId"))
//									.run()
//									.getResponse()
//									.getAsObject(Notebook.class);
		
		ModalWindow modal = new ModalWindow(this);
		modal
			.setTitle("Notebook's parameters")
			.add(new Button("Close")
					.add(modal.onClose()))
			.getContent()
				.render(new MetaTableComponent(this, getNotebook())
						.setPerPage(5));
		
		return new OpList()
			.add(new OpModal(modal))
//			.add(new OpDialogOpen(null, 
//						"Notebook parameters", 
////						new NotebookParams(this, notebook).toHtml()
//						new MetaTableComponent(this, notebook)
//							.setPerPage(5)
//							.toHtml()
//						)
//					.setOnCancelBtnCaption("Close"))
			;
	}
	
	private OpList onInsert() throws IOException, ValidationException {
		ModalWindow modal = new ModalWindow(this);
		modal
			.setTitle("Adding new entry")
			.add(new Button("Close")
					.add(modal.onClose()))
			.getContent()
				.render(new AddNewEntryPanel(this, "onAddEntry", getNotebook().getId()));
		
		return new OpList()
				.add(new OpModal(modal))
//				.add(new net.hypki.libs5.pjf.components.ops.OpDialogOpen(
//	//						this, 
//	//						null, 
//							"Adding new entry", 
//							new AddNewEntryPanel(this, "onAddEntry", getNotebook().getId()).toHtml(),
//							640, 640
//							)
////						.setOnOKBtnCaption("aa")
//						.setOnCancelBtnCaption("Close")
//						)
;
	}
	
	private OpList onRefresh() throws IOException, ValidationException {
		OpList op = new OpList();
		
		for (NotebookEntry entry : getNotebook().iterateEntries(getSessionBean())) {
			op
				.add(new OpComponentClick(NotebookEntryEditorFactory.getEditorPanel(this, entry.getClass()).getClass(),
						entry.getId().getId(),
						"onRefresh",
						new Params().add("entryId", entry.getId().getId())));
		}
		
		return op;
		
//		return new OpList()
//			.add(new OpComponentClick(NotebookContent.class, UUID.random(), "showContent", 
//				new Params()
//					.add("userId", getNotebook().getUserId())
//					.add("notebookId", getNotebook().getId())));
	}
	
	private OpList onReload() throws IOException, ValidationException {
		OpList op = new OpList();
		
		new ApiCallCommand(getSessionBean())
			.setApi("api/notebook/reload")
			.addParam("notebookId", getNotebook().getId().getId())
			.run();
		
		op
			.add(new OpComponentClick(NotebookContent.class, 
					"onShow", 
					new Params()
						.add("notebookId", getParams().getString("notebookId"))));
		
//		for (NotebookEntry entry : getNotebook().iterateEntries(getSessionBean())) {
//			if (entry instanceof Plot) {
//				op.add(new OpComponentClick(PlotPanel.class, 
//						entry.getId(), 
//						"onRefresh", 
//						new Params().add("plotId",  entry.getId().getId())));
//			}
//		}
		
		return op.add(new OpBeansInfo("All entries are scheduled to reload, be patient."));
	}
	

	public OpList onEditTitle() throws IOException, ValidationException {
		ModalWindow modal = new ModalWindow(this);
		modal.setTitle("Editing title");
		modal.add("Title", InputType.STRING, "notebook-name", getNotebook().getName());
		modal.add(new Button("Save")
				.add(new OpComponentClick(this.getClass(), 
						modal.getId(), 
						"onTitleSave",
						new Params()
							.add("notebookId", getNotebook().getId().getId())))
				.add(modal.onClose()));
		modal.add(new Button("Cancel")
				.add(modal.onClose()));
		return new OpList()
			.add(new OpModal(modal));
	}
	public OpList onTitleSave() throws IOException, ValidationException {
		String newName = getParams().getString("notebook-name");
		
		new ApiCallCommand(this)
			.setApi("api/notebook/edit")
			.addParam("notebookId", getNotebook().getId().getId())
			.addParam("title", newName)
			.run();
		
		return new OpList()
			.add(new OpSet(".notebook-title-" + getNotebook().getId() + " .name", newName))
			.add(new OpBeansInfo("Title changed"));
	}
	
//	public OpList onEditTitle(SecurityContext sc, Params params) throws ValidationException, IOException {
//		final String notebookId = params.getString("notebookId");
//		final String name = params.getString("name");
//		
//		new ApiCallCommand(this)
//			.setApi("api/notebook/edit")
//			.addParam("notebookId", notebookId)
//			.addParam("title", name)
//			.run();
//		
//		LibsLogger.debug(NotebookPanel.class, "Notebook ", notebookId, " name changed to ", name);
//		
//		return new OpList()
//			.add(new OpSet("#notebook-title-" + notebookId, " " + name))
//			.add(new OpDialogClose());
//	}
	
	public OpList removeEntry(SecurityContext sc, Params params) throws ValidationException, IOException {
		final String notebookId = params.getString("notebookId");
		final String entryId = params.getString("entryId");
		
		OpList opList = new OpList();
		
		if (notEmpty(entryId)) {
			new ApiCallCommand(this)
				.setApi("api/notebook/entry/remove")
				.setApiCallType(APICallType.DELETE)
				.addParam("entryId", entryId)
				.run();
			
			LibsLogger.debug(NotebookPanel.class, "Entry ", entryId, " removed from notebook ", notebookId);
			
			opList.add(new OpRemove("#" + entryId));
		} else {
			Notebook n = new ApiCallCommand(getSessionBean(sc))
							.setApi("api/notebook/get")
							.addParam("notebookId", notebookId)
							.run()
							.getResponse()
							.getAsObject(Notebook.class);
			
			for (UUID entryUuid : n.getEntries()) {
				opList.add(new OpRemove("#" + entryUuid));
			}
			
			new ApiCallCommand(getSessionBean(sc))
				.setApi("api/notebook/clear")
				.addParam("notebookId", notebookId)
				.run();
		}
		
		return opList 
			.add(new OpDialogClose());
	}
	
//	public OpList addPlot() throws ValidationException, IOException {
//		Notebook notebook = getNotebook();
//		
//		Plot plot = new Plot(notebook);
//		plot.setUserId(getSessionBean(sc).getUserId());
//		plot.save();
//		
//		notebook.getEntries().add(plot.getId());
//		notebook.save();
//		
//		PlotPanel plotPanel = new PlotPanel();
//		plotPanel.setNotebookEntry(plot);
//		
//		return new OpList()
//			.add(new OpAppend("#main #notebook-entries", plotPanel.toHtml()));
//	}
	
	private OpList onLoadMore() throws IOException {
		String lastEntryId = getParams().getString("lastEntryId");
		boolean toEnd = getParams().getBoolean("toend");
		
		OpList ops = new OpList();
		int loaded = -1;
		for (UUID entryID : getNotebook().getEntries()) {
			if (entryID.equals(lastEntryId)) {
				loaded = 0;
//				continue;
			}
			
			if (loaded >= 0 && (loaded < 5 || toEnd)) {
				loaded++; 
				
				// preparing notebook editor
				NotebookEntry ne = null;
				NotebookEntryEditorPanel notebookEditor = null;
				try {
					ne = NotebookEntryGateway.getNotebookEntry(new ApiCallCommand(this)
								.setApi("api/notebook/entry/get")
								.addParam("entryId", entryID.getId())
								.run()
								.getResponse()
								.getAsJson()
								.getAsJsonObject());
					
					if (ne == null) {
						LibsLogger.error(NotebookPanel.class, "Cannot find in DB ", NotebookEntry.class.getSimpleName(), entryID);
						continue;
					}
					
					Component editor = NotebookEntryEditorFactory.getEditorPanel(this, ne.getClass());
					
					if (editor == null) {
						LibsLogger.error(NotebookPanel.class, "Cannot create editor class for entry ", ne.getClass());
						continue;
					}
					
					notebookEditor = (NotebookEntryEditorPanel) editor;
					notebookEditor.setNotebookEntry(ne);
				} catch (Exception e) {
					LibsLogger.error(NotebookPanel.class, "Cannot render an Entry ", e);
				}
				
				HtmlCanvas loadMore = null;
				if (loaded == 5 && toEnd == false) {
					loadMore = new HtmlCanvas();
					loadMore
						.span(id("notebook-load-more")
								.onClick(new OpComponentClick(this, 
									"onLoadMore",
									new Params()
										.add("notebookId", getNotebook().getId().getId())
										.add("lastEntryId", ne.getId().getId()))
							.toStringOnclick()))
						._span();
				}
				
				ops
					.add(new OpReplace("#" + entryID, 
							notebookEditor.toHtml() + (loadMore != null ? loadMore.toHtml() : "")))
					.add(notebookEditor != null, new OpComponentTimer(notebookEditor, 
							"onRefresh", 
							RandomUtils.nextInt(1000),
							new Params()
								.add("entryId", ne.getId())));
			}
		}
		
		return ops;
	}

	public Notebook getNotebook() {
		if (notebook == null) {
			try {
				notebook = new ApiCallCommand(this)
							.setApi("api/notebook/get")
							.addParam("notebookId", getParams().getString("notebookId"))
							.run()
							.getResponse()
							.getAsObject(Notebook.class);
			} catch (IOException e) {
				LibsLogger.error(NotebookPanel.class, "Cannot initalize Notebook from DB", e);
			}
		}
		return notebook;
	}

	public void setNotebook(Notebook notebook) {
		this.notebook = notebook;
	}

	
}
