package net.beanscode.model.users;

import java.io.IOException;

import net.beanscode.model.cass.notification.Notification;
import net.beanscode.model.cass.notification.NotificationType;
import net.beanscode.model.rights.Rights;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.weblibs.jobs.Job;
import net.hypki.libs5.weblibs.user.User;
import net.hypki.libs5.weblibs.user.UserFactory;

public class IsAdminPasswordChangedJob extends Job {
	
	private static final UUID notficiationId = new UUID("fbad38d051122eeacc540fe372d362c1");

	public IsAdminPasswordChangedJob() {
		
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		User user = UserFactory.getUser("admin");
		
		if (user != null) {
			if (user.isPasswordCorrect("admin")) {
				// notify every admin
				for (User admin : UserFactory.iterateUsers()) {
					if (User.isAllowed(admin, Rights.ADMIN)) {
						new Notification()
							.setId(notficiationId)
							.setDescription("Change the default password for 'admin' user to avoid hacking")
							.setNotificationType(NotificationType.WARN)
							.setTitle("Admin password")
							.setUserId(admin.getUserId())
							.save();
					}
				}
			}
		}
	}
}
