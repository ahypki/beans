package net.beanscode.web;

import java.io.IOException;

import javax.ws.rs.core.SecurityContext;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.users.User;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.jersey.SessionBean;
import net.hypki.libs5.pjf.op.Op;
import net.hypki.libs5.pjf.op.OpList;

public class BeansPage {

	public static final String RIGHT_ADMIN = "ADMIN";
	
	private User user = null;
	
	private OpList opList = null;
	
	private Boolean isAdmin = null;
	
	public BeansPage() {
		
	}
	
	@Override
	public String toString() {
		return getOpList().toString();
	}
	
	public OpList getOpList() {
		if (opList == null)
			opList = new OpList();
		return opList;
	}
	
	protected void addToResponse(Op op) {
		getOpList().add(op);
	}
	
	protected void addToResponse(OpList opList) {
		getOpList().add(opList);
	}
	
	public SessionBean getSessionBean(final SecurityContext sc) {
		return (SessionBean) sc.getUserPrincipal();
	}
	
	protected UUID getUserId(final SecurityContext sc) {
		return getSessionBean(sc).getUserId();
	}
	
	protected User getLoggedUser(final SecurityContext sc) throws IOException {
		if (user == null)
			user = new ApiCallCommand(getSessionBean(sc))
						.setApi("api/user/get")
						.addParam("userId", getUserId(sc).getId())
						.run()
						.getResponse()
						.getAsObject(User.class);
		return user;
	}
	
	public boolean isAdmin(final SecurityContext sc) throws CLIException, IOException {
		if (isAdmin != null)
			return isAdmin;
		
		if (getSessionBean(sc) == null)
			return false;
		
		isAdmin = new ApiCallCommand(getSessionBean(sc))
					.setApi("api/user/allowed")
					.addParam("user-email", getLoggedUser(sc).getEmail())
					.addParam("right", RIGHT_ADMIN)
					.run()
					.getResponse()
					.getAsBoolean("allowed");
		return isAdmin;
	}

}
