package net.beanscode.web.view.notebook;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;
import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.id;
import static org.rendersnake.HtmlAttributesFactory.type;

import java.io.IOException;

import javax.ws.rs.core.SecurityContext;

import org.rendersnake.HtmlCanvas;
import org.rendersnake.Renderable;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.notebooks.NotebookEntryGateway;
import net.beanscode.cli.notification.Notification;
import net.beanscode.pojo.NotebookEntry;
import net.beanscode.pojo.NotebookEntryProgress;
import net.beanscode.web.BeansWebConst;
import net.beanscode.web.view.plots.AutoUpdateSettingsPanel;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.BetaButtonGroup;
import net.beanscode.web.view.ui.modals.ModalWindow;
import net.beanscode.web.view.ui.modals.OpBeansError;
import net.beanscode.web.view.ui.modals.OpModal;
import net.beanscode.web.view.ui.modals.OpModalQuestion;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpAddClass;
import net.hypki.libs5.pjf.op.OpAfter;
import net.hypki.libs5.pjf.op.OpAppend;
import net.hypki.libs5.pjf.op.OpBefore;
import net.hypki.libs5.pjf.op.OpDialogClose;
import net.hypki.libs5.pjf.op.OpHide;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpMoveDown;
import net.hypki.libs5.pjf.op.OpMoveUp;
import net.hypki.libs5.pjf.op.OpRemove;
import net.hypki.libs5.pjf.op.OpRemoveClass;
import net.hypki.libs5.pjf.op.OpReplace;
import net.hypki.libs5.pjf.op.OpSet;
import net.hypki.libs5.pjf.op.OpSetAttr;
import net.hypki.libs5.pjf.op.OpSetVal;
import net.hypki.libs5.pjf.op.OpShow;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.api.APICallType;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.url.HttpResponse;
import net.hypki.libs5.utils.url.Params;

public abstract class NotebookEntryEditorPanel extends BeansComponent {
	
	private OpList initOpList = null;
	
	private NotebookEntry notebookEntry = null;
	
	private NotebookEntryProgress notebookEntryProgressCache = null;
	
	private Notification notification = null;
	
	public abstract Renderable getView();
	protected abstract Renderable getEditor();
	protected abstract Button getMenu() throws IOException;
	
	protected abstract OpList opOnPlay(SecurityContext sc, Params params) throws IOException;
	protected abstract OpList opOnEdit(SecurityContext sc, Params params) throws IOException;
	protected abstract OpList opOnStop(SecurityContext sc, Params params) throws IOException;
	protected abstract OpList opOnRemove(SecurityContext sc, Params params) throws IOException;
	protected abstract OpList onRefresh(SecurityContext sc, Params params) throws IOException;
	
	public NotebookEntryEditorPanel() {
		
	}

	public NotebookEntryEditorPanel(Component parent) {
		super(parent);
	}
	
	@Override
	public String getId() {
		if (getNotebookEntry() != null
				&& getNotebookEntry().getId() != null)
			return getNotebookEntry().getId().getId();
		return super.getId();
	}
	
	protected OpList onBeforePlay() {
		return null;
	}
	
	protected OpList onBeforeCancel() {
		return null;
	}
	
	private OpList divOpListDeactivate() {
		return new OpList()
			.add(new OpRemoveClass("#" + getId(), "selected"))
			.add(new OpHide("#" + getId() + " .beans-btn-group"))
			.add(new OpSetAttr("#" + getId(), "onclick", divOnClick().toStringOnclick()))
			;
	}
	
	private OpList divOnClick() {
		return new OpList()
			.add(new OpAddClass("#" + getId(), "selected"))
			.add(new OpShow("#" + getId() + " .beans-btn-group"))
			.add(new OpSetAttr("#" + getId(), "onclick", ""));
	}
	
	public OpList editClick(SecurityContext sc, Params params) {
		try {
			LibsLogger.debug(NotebookEntryEditorPanel.class, "Edit with params ", params);
						
			return new OpList()
				.add(opOnEdit(sc, params))
				.add(new OpSet("#" + getId() + " .content", new HtmlCanvas().render(getEditor()).toHtml()));
		} catch (IOException e) {
			LibsLogger.error(NotebookEntryEditorPanel.class, "Cannot prepare editor", e);
			return null;
		}
	}
	
	public OpList playClick(SecurityContext sc, Params params) throws IOException {
		try {
			LibsLogger.debug(NotebookEntryEditorPanel.class, "View with params ", params);
			
			return new OpList()
				.add(opOnPlay(sc, params))
				.add(divOpListDeactivate())
//				.add(new OpSet("#" + getId() + " .status", renderStatus(new HtmlCanvas(), null).toHtml()))
				.add(new OpSet("#" + getId() + " .content", new HtmlCanvas().render(getView()).toHtml()))
				.add(new OpReplace(".entry-progress-" + getEntryId(), 
						new EntryProgressPanel(this, getNotebookEntryProgress()).toHtml()))
				;
		} catch (Exception e) {
			LibsLogger.error(NotebookEntryEditorPanel.class, "Cannot prepare editor", e);
			return new OpList()
				.add(new OpBeansError(e.getMessage()));
		}
		
	}
	
	public OpList stopClick(SecurityContext sc, Params params) throws IOException {
		try {
			LibsLogger.debug(NotebookEntryEditorPanel.class, "Stop with params ", params);
			
//			boolean refresh = params.get("plot-type") != null;
						
			return new OpList()
				.add(opOnStop(sc, params))
				.add(divOpListDeactivate())
				.add(/*refresh, */new OpSet("#" + getId() + " .content", new HtmlCanvas().render(getView()).toHtml()));
		} catch (Exception e) {
			LibsLogger.error(NotebookEntryEditorPanel.class, "Cannot prepare editor", e);
			return null;
		}	
	}
	
	public OpList removeClick() {
		try {
			return new OpList()
				.add(opOnRemove(getSecurityContext(), getParams()))
				.add(new OpRemove("#" + getParams().getString("entryId")));
		} catch (IOException e) {
			LibsLogger.error(NotebookEntryEditorPanel.class, "Cannot prepare editor", e);
			return null;
		}
	}
	
	public OpList moveUpClick(SecurityContext sc, Params params) throws IOException, ValidationException {
		LibsLogger.debug(NotebookEntryEditorPanel.class, "Move up with params ", params);
		
		UUID entryId = new UUID(params.getString("entryId"));
		
		ApiCallCommand apiCall = (ApiCallCommand) new ApiCallCommand(getSessionBean())
			.setApi("api/notebook/entry/moveUp")
			.addParam("entryId", entryId)
			.run();
		
		return new OpList()
			.add(new OpMoveUp(".notebook-entry-" + entryId, ".notebook-entry"))
			;
	}
	
	public OpList moveDownClick(SecurityContext sc, Params params) throws IOException, ValidationException {
		LibsLogger.debug(NotebookEntryEditorPanel.class, "Move down with params ", params);
		
		UUID entryId = new UUID(params.getString("entryId"));
		
		ApiCallCommand apiCall = (ApiCallCommand) new ApiCallCommand(getSessionBean())
			.setApi("api/notebook/entry/moveDown")
			.addParam("entryId", entryId)
			.run();
				
		return new OpList()
			.add(new OpMoveDown(".notebook-entry-" + entryId, ".notebook-entry"))
			;
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		final Button menu = getMenu();
		final BetaButtonGroup buttonGroup = new BetaButtonGroup();
		
		buttonGroup
//			.setButtonSize(ButtonSize.NORMAL)
			.setVisible(false)
			.add(new Button()
				.setAwesomeicon("fa fa-edit")
				.setHtmlClass("edit")
				.setTooltip("Edit item")
				.setOpList(new OpList()
							.add(new OpHide("#" + getId() + " .edit"))
							.add(new OpShow("#" + getId() + " .play"))
							.add(new OpComponentClick(this, "editClick", new Params()
																			.add("notebookId", getNotebookEntry().getNotebookId().getId())
																			.add("entryId", getNotebookEntry().getId().getId())))
						))
			.add(new Button()
				.setAwesomeicon("fa fa-play")
				.setHtmlClass("play")
				.setTooltip("Run item")
				.setVisible(false)
				.setStopPropagation(true)
				.setOpList(new OpList()
							.add(onBeforePlay())
							.add(new OpShow("#" + getId() + " .edit"))
							.add(new OpHide("#" + getId() + " .play"))
							.add(new OpComponentClick(this, "playClick", new Params()
																			.add("notebookId", getNotebookEntry().getNotebookId().getId())
																			.add("entryId", getNotebookEntry().getId().getId())))
							.add(new OpHide("#" + getId() + " .beans-btn-group"))
							.add(new OpSetAttr("#" + getId(), "onclick", divOnClick().toStringOnclick()))
						))
			.add(new Button()
//				.setGlyphicon("stop")
//				.setGlyphicon("hand-left")
				.setAwesomeicon("fa fa-times")
				.setTooltip("Cancel editing")
				.setStopPropagation(true)
				.setOpList(new OpList()
							.add(onBeforeCancel())
							.add(new OpShow("#" + getId() + " .edit"))
							.add(new OpHide("#" + getId() + " .play"))
							.add(divOpListDeactivate())
							.add(new OpComponentClick(this, "stopClick", new Params()
																			.add("notebookId", getNotebookEntry().getNotebookId().getId())
																			.add("entryId", getNotebookEntry().getId().getId())))
						))
			.add(new Button()
				.setAwesomeicon("fa fa-trash")
				.setTooltip("Remove item")
				.setOpList(new OpList()
							.add(new OpModalQuestion("Removing query",
									"Do you really want to remove the following notebook entry?"
									+ "<br/>"
									+ "<b>" + getNotebookEntry().getName() + "</b>"
									+ " (ID = " + getNotebookEntry().getId() + ")",
									new OpComponentClick(this, 
											"removeClick",
											new Params()
												.add("userId", getNotebookEntry().getUserId())
												.add("notebookId", getNotebookEntry().getNotebookId().getId())
												.add("entryId", getNotebookEntry().getId().getId())
										)))))
			.add(new Button()
				.setAwesomeicon("fa fa-arrow-up")
				.setTooltip("Move up")
				.setOpList(new OpList()
							.add(new OpComponentClick(this, "moveUpClick", new Params()
																				.add("notebookId", getNotebookEntry().getNotebookId().getId())
																				.add("entryId", getNotebookEntry().getId().getId())))
							.add(new OpMoveUp("#" + buttonGroup.getId(), ".notebook-entry"))
							))
			.add(new Button()
				.setAwesomeicon("fa fa-arrow-down")
				.setTooltip("Move down")
				.setOpList(new OpList()
							.add(new OpComponentClick(this, "moveDownClick", new Params()
																					.add("notebookId", getNotebookEntry().getNotebookId().getId())
																					.add("entryId", getNotebookEntry().getId().getId())))
							.add(new OpMoveDown("#" + buttonGroup.getId(), ".notebook-entry"))
							))
			.add(new Button()
					.setAwesomeicon("fa fa-plus")
					.setTooltip("Insert new entry")
					.addButton(new Button("New entry before this entry")
							.add(new OpComponentClick(this, "onInsertEntry", new Params()
									.add("entryId", getNotebookEntry().getId().getId())
									.add("notebookId", getNotebookEntry().getNotebookId().getId())
									.add("position", "before")
									.add("refEntryId", getNotebookEntry().getId().getId())))
									)
					.addButton(new Button("New entry after this entry")
							.add(new OpComponentClick(this, "onInsertEntry", new Params()
									.add("entryId", getNotebookEntry().getId().getId())
									.add("notebookId", getNotebookEntry().getNotebookId().getId())
									.add("position", "after")
									.add("refEntryId", getNotebookEntry().getId().getId())))
									)
					.addButton(new Button()
							.setSeparator(true))
//					)
//			.add(new Button()
//					.setAwesomeicon("fa fa-copy")
//					.setTooltip("Clone this entry")
					.addButton(new Button("Clone entry before this entry")
							.add(new OpComponentClick(this, "onCloneEntry", new Params()
									.add("entryId", getNotebookEntry().getId().getId())
									.add("notebookId", getNotebookEntry().getNotebookId().getId())
									.add("position", "before")
									.add("refEntryId", getNotebookEntry().getId().getId())
									.add("sourceNotebookEntryClass", getNotebookEntry().getClass().getName())
									)))
					.addButton(new Button("Clone entry after this entry")
							.add(new OpComponentClick(this, "onCloneEntry", new Params()
									.add("entryId", getNotebookEntry().getId().getId())
									.add("notebookId", getNotebookEntry().getNotebookId().getId())
									.add("position", "after")
									.add("refEntryId", getNotebookEntry().getId().getId())
									.add("sourceNotebookEntryClass", getNotebookEntry().getClass().getName())
									)))
					)
			.add(new Button()
					.setAwesomeicon("fa fa-cog")
					.setTooltip("Other options")
					.addButton(new Button("Auto-update settings")
							.setAwesomeicon("fa fa-cogs")
							.setTooltip("Auto-update settings")
							.setOpList(new OpList()
										.add(new OpComponentClick(this, "onAutoUpdateSettings", new Params()
																								.add("notebookId", getNotebookEntry().getNotebookId().getId())
																								.add("entryId", getNotebookEntry().getId().getId())))
										))
					.addButton(new Button("Move entry")
							.setAwesomeicon("fa fa-plane")
							.setTooltip("Move this entry to another Notebook")
							.add(new OpComponentClick(this, "onMove", new Params()
								.add("entryId", getNotebookEntry().getId().getId()))))
					.addButton(new Button("History of changes")
							.setAwesomeicon("fa fa-history")
							.setTooltip("Examine history of changes")
							.add(new OpComponentClick(this, "onHistoryOpen", new Params()
								.add("entryId", getNotebookEntry().getId().getId()))))
					.addButton(new Button("Logs")
							.setAwesomeicon("fa fa-comment")
							.setTooltip("Show log messages")
							.add(new OpComponentClick(this, "onLog")))
					)
			
			;
		
		if (menu != null && menu.getButtons().size() > 0) {
			for (Button button : menu.getButtons()) {
				buttonGroup.add(button);
			}
		} else if (menu != null) {
			buttonGroup.add(menu);
		}
		
		html
			.div(class_("notebook-entry notebook-entry-" + getNotebookEntry().getId().getId())
					.id(getId())
					.onClick(divOnClick().toStringOnclick()))
				.span(id(getId() + "-scroll-onclick")
						.onClick(new OpComponentClick(this, 
								"onRefresh", 
								new Params()
									.add("entryId", getNotebookEntry().getId())
									.add(BeansWebConst.MAGIC_KEY, getParams().get(BeansWebConst.MAGIC_KEY))
									).toStringOnclick()))
				._span()
				.div(class_("menu"))
					.render(buttonGroup)
				._div();
				
//				renderStatus(html, null);
		
		html
			.render(new EntryProgressPanel(this, getNotebookEntryProgress()));
		
		html
				.div(class_("content"))
					.render(getView())
				._div()
			._div();
	}
	
//	protected OpList onReloadStatus(String message) throws IOException {
//		return new OpList()
//			.add(new OpReplace("#" + getId() + " .status", renderStatus(new HtmlCanvas(), message).toHtml()));
//	}
	
	private OpList onHistoryOpen() throws CLIException, IOException {
		ModalWindow modal = new ModalWindow(this);
		modal
			.add(new Button("Close")
					.add(modal.onClose()))
			.getContent()
				.render(new NotebookEntryHistoryPanel(this, getEntryId()));
		
		return new OpList()
				.add(new OpModal(modal));
	}
	
	private OpList onLog() throws CLIException, IOException {
//		HtmlCanvas html = new HtmlCanvas();
//		
//		html
//			.textarea(HtmlAttributesFactory.readonly("readonly"))
//				.content(new EntryLogGet(getSessionBean()).run(getId()));
		
		return new OpList()
			.add(new OpSet("#" + getId() + " .content", new LogPanel(this, new UUID(getId())).toHtml()));
	}
	
	private OpList onMoveSave() throws IOException, ValidationException {
		ApiCallCommand apiCall = (ApiCallCommand) new ApiCallCommand(getSessionBean())
			.setApi("api/notebook/entry/move")
			.addParam("notebookEntryId", getNotebookEntry().getId())
			.addParam("notebookId", getParams().getString("selectedNotebookId"))
			.run();
		
		return new OpList()
			.add(new OpRemove("#" + getId()));
	}
	
	private OpList onMoveSelected() throws IOException, ValidationException {
//		ApiCallCommand apiCall = (ApiCallCommand) new ApiCallCommand(getSessionBean())
//			.setApi("api/notebook/entry/move")
//			.addParam("notebookEntryId", getNotebookEntry().getId())
//			.addParam("notebookId", getParams().getString("notebookId"))
//			.run();
		
		return new OpList()
			.add(new OpSet(".selected-notebook", getParams().getString("notebookName", " ")))
			.add(new OpSetVal(".selectedNotebookId", getParams().getString("notebookId", " ")));
	}
	
	private OpList onMove() throws IOException, ValidationException {
		ModalWindow modal = new ModalWindow(this);

		NotebooksTableComponent notebooksPanel = (NotebooksTableComponent) new NotebooksTableComponent(this)
			.setCompact(true)
			.setOpOnClick(new OpList()
					.add(new OpComponentClick(this, "onMoveSelected")))
			.setPerPage(4);
		
		modal
			.setTitle("Move entry to another Notebook")
			.setContent(notebooksPanel)
			.add(new Button("Move")
					.add(new OpComponentClick(this.getClass(),
							modal.getId(),
							"onMoveSave",
							new Params()
								.add("entryId", getNotebookEntry().getId().getId())))
					.add(modal.onClose()))
			.add(new Button("Cancel")
					.add(modal.onClose()));
		
		modal
			.getContent()
				.span()
					.content("Destination notebook: ")
				.input(type("hidden")
						.name("selectedNotebookId")
						.class_("selectedNotebookId"))
				.b(class_("selected-notebook"))
					.content("");
		
		// TODO fix this to work with beta view
//		notebooksPanel
//			.getOnNotebookClick()
//				.add(new OpComponentClick(this, 
//						"onMoveSelected",
//						OpsFactory.opConfirmation("Moving entry", "Are you sure to move this entry?"),
//						new Params()
//							.add("entryId", getNotebookEntry().getId().getId())));
		
		return new OpList()
			.add(new OpModal(modal))
			.add(new OpComponentClick(notebooksPanel, "onRefresh"));
	}
	
	private OpList onInsertEntry() throws IOException, ValidationException {
		final String position = getParams().getString("position");
		final String refEntryId = getParams().getString("refEntryId");
		
		return new OpList()
			.add(new OpModal(new ModalWindow(this, 
					"Adding new entry", 
					new AddNewEntryPanel(this, 
							"onAddEntry", 
							getNotebookEntry().getNotebookId(), 
							position, 
							refEntryId != null ? new UUID(refEntryId) : null),
					(OpList) null
//					new OpList()
//						.add(new OpComponentClick(this, 
//								"onAddEntry", 
//								new Params()
//									.add("refEntryId", refEntryId)
//									.add("position", position)))
									)));
	}
	
	private OpList onCloneEntry() throws IOException, ValidationException {
		try {
			final String positionStr = getParams().getString("position");
			final String refEntryId = getParams().getString("refEntryId");
			final String entryId = getParams().getString("entryId");
			final String notebookId = getParams().getString("notebookId");
			final String sourceNotebookEntryClass = getParams().getString("sourceNotebookEntryClass");

			int position = 0;
			if (positionStr != null) {
				if (positionStr.equalsIgnoreCase("before"))
					position = -1;
				else if (positionStr.equalsIgnoreCase("after"))
					position = 1;
				else
					LibsLogger.error(NotebookEntryEditorPanel.class, "Unknown case position= " + position);
			}
			
			// cloning an existing entry
			HttpResponse resp = new ApiCallCommand(getSessionBean())
				.setApi("api/notebook/entry/clone/entry")
				.addParam("--notebookId", notebookId)
				.addParam("--sourceEntryId", entryId)
				.addParam("--refEntryId", refEntryId)
				.addParam("--position", position)
				.run()
				.getResponse();
			
			NotebookEntry ne = (NotebookEntry) resp.getAsObject(Class.forName(sourceNotebookEntryClass));
			
			NotebookEntryEditorPanel nePanel = NotebookEntryEditorFactory.getEditorPanel(this, ne.getClass());
			nePanel.setNotebookEntry(null);
			nePanel.setParams(new Params()
					.add("entryId", ne.getId().getId()));
			nePanel.setSessionBean(getSecurityContext());
			
			return new OpList()
				.add(nullOrEmpty(positionStr), new OpAppend("#notebook-entries", nePanel.toHtml()))
				.add(notEmpty(positionStr) && positionStr.equals("after"), 
						new OpAfter("#notebook-entries #" + entryId, nePanel.toHtml()))
				.add(notEmpty(positionStr) && positionStr.equals("before"), 
						new OpBefore("#notebook-entries #" + entryId, nePanel.toHtml()))
				.add(new OpDialogClose())
				;
		} catch (CLIException | ClassNotFoundException e) {
			LibsLogger.error(NotebookEntryEditorPanel.class, "Cannot clone the entry", e);
			return new OpList()
				.add(new OpBeansError("Cannot clone this entry"));
		}
	}
	
	private OpList onAddEntry() throws IOException, ValidationException {
		final String destinationNotebookId = getParams().getString("destinationNotebookId");
		final String refEntryId = getParams().getString("refEntryId");
//		final String destinationAfterEntryId = getParams().getString("destinationAfterEntryId");
		final String sourceNotebookEntryClass = getParams().getString("sourceNotebookEntryClass");
		final String positionStr = getParams().getString("position");
		final String sourceEntryId = getParams().getString("refEntryId");
		
		try {
			int position = 0;
			if (positionStr != null) {
				if (positionStr.equalsIgnoreCase("before"))
					position = -1;
				else if (positionStr.equalsIgnoreCase("after"))
					position = 1;
				else
					LibsLogger.error(NotebookEntryEditorPanel.class, "Unknown case position= " + position);
			}
			
			HttpResponse resp = new ApiCallCommand(getSessionBean())
				.setApi("api/notebook/entry/add")
				.addParam("--notebookId", destinationNotebookId)
				.addParam("--notebookEntryClass", sourceNotebookEntryClass)
				.addParam("--refEntryId", refEntryId)
				.addParam("--position", position)
				.run()
				.getResponse();
			
			NotebookEntry ne = (NotebookEntry) resp.getAsObject(Class.forName(sourceNotebookEntryClass));
					
			NotebookEntryEditorPanel nePanel = NotebookEntryEditorFactory.getEditorPanel(this, ne.getClass());
			nePanel.setNotebookEntry(ne);
			nePanel.setParams(getParams());
			nePanel.setSessionBean(getSecurityContext());
			
			return new OpList()
				.add(nullOrEmpty(positionStr), new OpAppend("#notebook-entries", nePanel.toHtml()))
				.add(notEmpty(positionStr) && positionStr.equals("after"), 
						new OpAfter("#notebook-entries #" + sourceEntryId, nePanel.toHtml()))
				.add(notEmpty(positionStr) && positionStr.equals("before"), 
						new OpBefore("#notebook-entries #" + sourceEntryId, nePanel.toHtml()))
				.add(new OpDialogClose())
				;
		} catch (CLIException | ClassNotFoundException e) {
			LibsLogger.error(NotebookEntryEditorPanel.class, "Cannot add new entry", e);
			return new OpList()
				.add(new OpBeansError("Cannot add new entry"));
		}
	}
	
	private OpList onAutoUpdateSettings() throws IOException {
		ModalWindow modal = new ModalWindow(this);
		
		modal
			.setTitle("Auto-update settings")
			.setContent(new AutoUpdateSettingsPanel(this, getNotebookEntry()))
			.add(new Button("Save")
					.add(new OpComponentClick(this, "onAutoUpdateSave"))
					.add(modal.onClose()))
			.add(new Button("Cancel")
					.add(modal.onClose()));
		
		return new OpList()
			.add(new OpModal(modal));
	}
	
	private OpList onAutoUpdateSave() throws ValidationException, IOException {
		final String value = getParams().getString("auto-update-setting");
		
		new ApiCallCommand(this)
			.setApi("api/notebook/entry/autoUpdate")
			.addParam("entryId", getNotebookEntry().getId())
			.addParam("autoUpdateOption", value)
			.run();
//		
//		if (value.equalsIgnoreCase(AutoUpdateOption.NO_CONFIRMATION_NEEDED.name()))
//			getNotebookEntry().getAutoUpdateOptions().setOption(AutoUpdateOption.NO_CONFIRMATION_NEEDED);
//		else if (value.equalsIgnoreCase(AutoUpdateOption.IGNORE.name()))
//			getNotebookEntry().getAutoUpdateOptions().setOption(AutoUpdateOption.IGNORE);
//		else if (value.equalsIgnoreCase(AutoUpdateOption.CONFIRMATION_NEEDED.name()))
//			getNotebookEntry().getAutoUpdateOptions().setOption(AutoUpdateOption.CONFIRMATION_NEEDED);
//		else
//			throw new ValidationException("Unknown option for auto-updates: " + value);
//		
//		getNotebookEntry().save();
		
		return new OpList()
				.add(new OpDialogClose());
	}
	
	protected String getEntryId() {
		String entryId = getParams().getString("entryId");
		if (entryId == null)
			entryId = this.notebookEntry != null ? this.notebookEntry.getId().getId() : null;
		return entryId;
	}

	public NotebookEntry getNotebookEntry() {
		if (notebookEntry == null) {
			String entryId = getParams().getString("entryId");
			if (StringUtilities.notEmpty(entryId)) {
				try {
					this.notebookEntry = NotebookEntryGateway.getNotebookEntry(new ApiCallCommand(this)
											.setApi("api/notebook/entry/get")
											.addParam("entryId", entryId)
											.run()
											.getResponse()
											.getAsJson()
											.getAsJsonObject());
				} catch (IOException e) {
					LibsLogger.error(NotebookEntryEditorPanel.class, "Canot initialize NotebookEntry with ID " + entryId, e);
				}
			}
		}
		return notebookEntry;
	}
	
//	public Notification getNotificationEntry() {
//		if (notification == null) {
//			try {
//				notification = new ApiCallCommand(this)
//									.setApi("api/notification/get")
//									.addParam("id", getNotebookEntry().getId().getId())
//									.run()
//									.getResponse()
//									.getAsObject(Notification.class);
//			} catch (CLIException | IOException e) {
//				LibsLogger.error(NotebookEntryEditorPanel.class, "Cannot get Notification for Enrty " + getId(), e);
//			}
//		}
//		return notification;
//	}

	public void setNotebookEntry(NotebookEntry notebookEntry) {
		this.notebookEntry = notebookEntry;
	}

	public OpList getInitOpList() {
		if (initOpList == null)
			initOpList = new OpList();
		return initOpList;
	}

	public void setInitOpList(OpList initOpList) {
		this.initOpList = initOpList;
	}

	public boolean isInitOpListDefined() {
		return initOpList != null && initOpList.size() > 0;
	}
	
	// TODO name getProgress()
	public NotebookEntryProgress getNotebookEntryProgress() {
		if (notebookEntryProgressCache == null) {
			try {
				notebookEntryProgressCache = new ApiCallCommand(this)
						.setApi("api/notebook/entry/progress")
						.setApiCallType(APICallType.GET)
						.addParam("entryId", getEntryId())
						.run()
						.getResponse()
						.getAsObject(NotebookEntryProgress.class);
			} catch (Exception e) {
				LibsLogger.error(NotebookEntryEditorPanel.class, "Cannot get progress from DB", e);
			}
		}
		return notebookEntryProgressCache;
	}
	
//	protected void init(UUID entryId) throws IOException {
//		setNotebookEntry(new EntryGet().run(entryId.getId()));
//	}
}
