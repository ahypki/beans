package net.beanscode.web.view.ui;

import static org.rendersnake.HtmlAttributesFactory.class_;

import java.io.IOException;

import net.hypki.libs5.pjf.components.Component;

import org.rendersnake.HtmlCanvas;
import org.rendersnake.Renderable;

import com.google.gson.annotations.Expose;

public abstract class CardPanel extends BeansComponent {
	
	@Expose
	private int width = 6;
	
	protected abstract void renderTitle(HtmlCanvas html) throws IOException;
	protected abstract void renderContent(HtmlCanvas html) throws IOException;
	protected abstract void renderFooter(HtmlCanvas html) throws IOException;

	public CardPanel() {
		
	}
	
	public CardPanel(Component parent) {
		super(parent);
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.section(class_("col-sm-" + getWidth())
					.id(getId()))
				.div(class_("card card-primary "))
					.div(class_("card-header"))
						.render(new Renderable() {
							@Override
							public void renderOn(HtmlCanvas html) throws IOException {
								renderTitle(html);
							}
						})
					._div()
					.div(class_("card-body"))
						.render(new Renderable() {
							@Override
							public void renderOn(HtmlCanvas html) throws IOException {
								renderContent(html);
							}
						})
					._div()
					.div(class_("card-footer"))
						.render(new Renderable() {
							@Override
							public void renderOn(HtmlCanvas html) throws IOException {
								renderFooter(html);
							}
						})
					._div()
				._div()
			._section();
	}
	
	public int getWidth() {
		return width;
	}
	
	public CardPanel setWidth(int width) {
		this.width = width;
		return this;
	}
}
