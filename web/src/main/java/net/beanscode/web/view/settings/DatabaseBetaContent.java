package net.beanscode.web.view.settings;

import java.io.IOException;

import net.beanscode.web.beta.BetaContent;
import net.beanscode.web.view.ui.BetaButtonGroup;
import net.hypki.libs5.pjf.components.Component;

import org.rendersnake.HtmlCanvas;

public class DatabaseBetaContent extends BetaContent {

	public DatabaseBetaContent() {
		
	}
	
	public DatabaseBetaContent(Component parent) {
		super(parent);
	}
		
	@Override
	protected void renderHeader(HtmlCanvas html) throws IOException {
		html
			.h1()
				.content("Database");
	}
	
	protected void renderBreadcrumb(HtmlCanvas html) throws IOException {
		
	};
	
	@Override
	protected void renderContent(HtmlCanvas html) throws IOException {
		html
			.render(new DatabaseChoosePanel(this))
			;
	}

	@Override
	protected BetaButtonGroup getMenu() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String getMenuName() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}
		
	
}
