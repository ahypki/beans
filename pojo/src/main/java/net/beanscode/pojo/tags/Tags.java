package net.beanscode.pojo.tags;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

public class Tags {
	@Expose
	private List<String> tags = null;
	
	@Expose
	private String tagsjoined = null;
	
	public Tags() {
		
	}

	public List<String> getTags() {
		if (tags == null)
			tags = new ArrayList<>();
		return tags;
	}
	
	public void clear() {
		getTags().clear();
	}
	
	public boolean addTags(String tags) {
		boolean added = false;
		if (tags != null)
			for (String tag1 : tags.split(",")) {
				for (String tag2 : tag1.split("[\\s]+")) {
					if (notEmpty(tag2)) {
						if (addTag(tag2.trim()))
							added = true;
					}
				}
			}
		return added;
	}
	
	public boolean addTag(String tag) {
		if (!getTags().contains(tag)) {
			getTags().add(tag);
			reloadTagsJoined();
			return true;
		}
		return false;
	}
	
	public boolean removeTag(String tag) {
		if (getTags().contains(tag)) {
			getTags().remove(tag);
			reloadTagsJoined();
			return true;
		}
		return false;
	}

	private void reloadTagsJoined() {
		StringBuilder sb = new StringBuilder();
		sb.append("_");
		for (String tag : getTags()) {
			sb.append(tag);
			sb.append("_");
		}
		this.tagsjoined = sb.toString();
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}
	
	public String getTagsJoined(String separator) {
		StringBuilder sb = new StringBuilder();
		for (String tag : getTags()) {
			sb.append(tag);
			sb.append(separator);
		}
		return sb.toString();
	}

	public String getTagsJoined() {
		return tagsjoined;
	}

	public void setTagsJoined(String tagsJoined) {
		this.tagsjoined = tagsJoined;
	}
}
