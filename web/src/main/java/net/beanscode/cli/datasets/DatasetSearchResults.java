package net.beanscode.cli.datasets;

import java.util.List;

import net.beanscode.pojo.dataset.Dataset;

import com.google.gson.annotations.Expose;

public class DatasetSearchResults {

	@Expose
	private List<Dataset> datasets = null;
	
	@Expose
	private int maxHits = 0;
	
	public DatasetSearchResults() {
		
	}

	public List<Dataset> getDatasets() {
		return datasets;
	}

	public DatasetSearchResults setDatasets(List<Dataset> datasets) {
		this.datasets = datasets;
		return this;
	}

	public int getMaxHits() {
		return maxHits;
	}

	public DatasetSearchResults setMaxHits(int maxHits) {
		this.maxHits = maxHits;
		return this;
	}
	
	public DatasetSearchResults setMaxHits(long maxHits) {
		this.maxHits = (int) maxHits;
		return this;
	}
}
