package net.beanscode.web.view.settings;

import static org.rendersnake.HtmlAttributesFactory.id;

import java.io.IOException;

import net.beanscode.cli.BeansCliCommand;
import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.pojo.autoupdate.AutoUpdateOption;
import net.beanscode.web.view.plots.AutoUpdateSettingsPanel;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.buttons.ButtonGroup;
import net.hypki.libs5.pjf.components.buttons.ButtonSize;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpList;

import org.rendersnake.HtmlCanvas;

public class DefaultEntryAutoUpdatePanel extends BeansComponent {

	public DefaultEntryAutoUpdatePanel() {
		
	}
	
	public DefaultEntryAutoUpdatePanel(Component parent) {
		super(parent);
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.div(id(getId()))
					.render(new AutoUpdateSettingsPanel(this, 
							new ApiCallCommand(this)
								.setApi(BeansCliCommand.API_AUTOUPDATE_ENTRY_DEFUALT)
								.addParam("userId", getUserId().getId())
								.run()
								.getResponse()
								.getAsObject(AutoUpdateOption.class)))
					
					.render(new ButtonGroup()
							.setButtonSize(ButtonSize.NORMAL)
							.addButton(new Button("Save")
								.add(new OpComponentClick(this, "onSave"))))
			._div()
			;
	}
	
	private OpList onSave() throws CLIException, IOException {
		final String value = getParams().getString("auto-update-setting");
		
		new ApiCallCommand(this)
			.setApi(BeansCliCommand.API_AUTOUPDATE_ENTRY_DEFUALT)
			.addParam("userId", getUserId().getId())
			.addParam("option", AutoUpdateOption.valueOf(value))
			.run();
		
		return new OpList()
			.add(new OpBeansInfo("Default option for entries auto-update saved successfully"));
	}
}
