package net.beanscode.web.view.notebook;

import java.io.IOException;

import net.beanscode.web.beta.BetaContent;
import net.beanscode.web.view.ui.BetaButtonGroup;
import net.hypki.libs5.pjf.components.Component;

import org.rendersnake.HtmlCanvas;

public class NotebooksBetaContent extends BetaContent {

	public NotebooksBetaContent() {
		
	}
	
	public NotebooksBetaContent(Component parent) {
		super(parent);
	}
	
	@Override
	protected void renderHeader(HtmlCanvas html) throws IOException {
		html
			.h1()
				.content("Notebooks");
	}

	@Override
	protected void renderContent(HtmlCanvas html) throws IOException {
		html
			.render(new NotebooksTableComponent(this));
	}

	@Override
	protected BetaButtonGroup getMenu() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String getMenuName() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
