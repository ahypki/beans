package net.beanscode.web.view.ui;

import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.id;
import static org.rendersnake.HtmlAttributesFactory.type;

import java.io.IOException;

import javax.ws.rs.core.SecurityContext;

import org.rendersnake.HtmlAttributesFactory;
import org.rendersnake.HtmlCanvas;
import org.rendersnake.Renderable;

import com.google.gson.annotations.Expose;

import net.beanscode.cli.settings.SettingGateway;
import net.beanscode.web.view.settings.Setting;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.pjf.components.jersey.SessionBean;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.components.ops.OpComponentTimer;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpReplace;
import net.hypki.libs5.pjf.op.OpSet;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.url.Params;

public abstract class ListComponent extends BeansComponent {
	
	@Expose
	private int page = 0;

	@Expose
	private int perPage = 10;
	
	@Expose
	private String htmlClass = "";
	
	protected abstract long getMaxHits() throws IOException;
	protected abstract void renderItem(int idx, HtmlCanvas html) throws IOException;
	protected abstract String getSettingsPrefix();
	protected abstract String getFilterTooltip();

	private String filterCache = null;

	public ListComponent() {
		
	}
	
	public ListComponent(BeansComponent parent) {
		super(parent);
	}
	
	public ListComponent(SecurityContext securityContext, Params params) {
		super(securityContext, params);
	}
	
	protected boolean isFilterVisible() {
		return true;
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.div(id(getId()).class_("beans-list " + getHtmlClass()))
				.div(class_("row"))
					.if_(isFilterVisible())
						.input(type("text")
								.class_("filter")
								.name("filter")
								.add("placeholder", getFilterTooltip())
								.onKeyup(new OpList()
									.add(new OpSet("#" + getId() + " .items", 
											new SpinnerLabel("Refreshing...").toHtml()))
									.add(new OpComponentTimer(this, "onRefresh", 300))
									.toStringOnclick()))
					._if()
					.if_(getMaxHits() > getPerPage())
						.render(new Renderable() {
							@Override
							public void renderOn(HtmlCanvas html) throws IOException {
								renderPagination(html);
							}
						})
					._if()
				._div()
				
				.write(renderItems(new HtmlCanvas()).toHtml(), false)
				
				.div(class_("row footer"))
					
				._div()
			._div()
			;
	}
	
	private HtmlCanvas renderItems(HtmlCanvas html) throws IOException {
		html
			.div(class_("items"));
		for (int i = 0; 
				i < getPerPage()
					&& (getPage() * getPerPage() + i) < getMaxHits(); 
				i++) {
			renderItem(i, html);
		}
		html
			._div();
		return html;
	}
	
	private HtmlCanvas renderPagination(HtmlCanvas html) throws IOException {
		int maxPage = (int) getMaxHits() / getPerPage();
		
		int startPage = getPage() - 2;
		int stopPage = getPage() + 2;
		
		if (startPage < 0) {
			stopPage = stopPage + Math.abs(startPage);
			
			startPage = 0;
		}
		
		if (stopPage > maxPage) {
			startPage = startPage + (maxPage - stopPage);
			if (startPage < 0)
				startPage = 0;
		
			stopPage = maxPage;
		}
		
		int currentPage = 0;
		html
			.div(class_("pages"));
		renderOnePageButton(html, currentPage);
		
		for (currentPage = Math.max(currentPage + 1, startPage); currentPage <= stopPage; currentPage++) {
			renderOnePageButton(html, currentPage);
		}
		
		if (maxPage >= currentPage)
			renderOnePageButton(html, maxPage);
		html
			._div();
		
		return html;
	}
	
	private HtmlCanvas renderOnePageButton(HtmlCanvas html, int currentPage) throws IOException {
		if (currentPage == getPage()) {
			return html
					.li(class_("page-item")
							.add("title", "Page " + (currentPage + 1)))
						.a(class_("page-link beans-link-inactive"))
							.content("" + (currentPage + 1))
					._li();
		} else {
			return html
					.li(class_("page-item")
							.add("title", "Page " + (currentPage + 1)))
						.a(class_("page-link")
//								.href("#")
								.onClick(new OpList()
									.add(new OpSet("#" + getId() + " .items", 
											new SpinnerLabel("Refreshing...").toHtml()))	
									.add(new OpComponentClick(this, 
										"onRefresh", 
										new Params()
											.add("page", currentPage)))
									.toStringOnclick()))
							.content("" + (currentPage + 1))
					._li();
		}
	}
	
	private OpList onRefresh() throws IOException {
		setPage(getParams().getInteger("page", 0));
		
		return new OpList()
				.add(new OpReplace("#" + getId() + " .pages", renderPagination(new HtmlCanvas()).toHtml()))
				.add(new OpReplace("#" + getId() + " .items", renderItems(new HtmlCanvas()).toHtml()));
	}
	
	protected String getFilter() {
		if (filterCache == null)
			filterCache = getParams().getString("filter", null);
		if (filterCache == null) {
			try { 
				Setting s = SettingGateway.getUserSetting(getSessionBean(), getSettingsPrefix() + "-filter");
				filterCache = s != null ? s.getValueAsString("filter") : null;
			} catch (CLIException | IOException e) {
				LibsLogger.error(BetaTableComponent.class, "Cannot get setting", e);
			}
		}
		return filterCache;
	}
	
	public int getPage() {
		return page;
	}
	
	public ListComponent setPage(int page) {
		this.page = page;
		return this;
	}
	
	protected int getPerPage() {
		// params have precedance 
		if (getParams().getInteger("beans-table-per-page", -1) > 0)
			perPage = getParams().getInteger("beans-table-per-page", perPage);
		return perPage;
	}

	public ListComponent setPerPage(int perPage) {
		this.perPage = perPage;
		return this;
	}
	
	public String getHtmlClass() {
		return htmlClass;
	}
	
	public ListComponent setHtmlClass(String htmlClass) {
		this.htmlClass = htmlClass;
		return this;
	}
}
