package net.beanscode.model.notebook;

import com.google.gson.annotations.Expose;

import net.hypki.libs5.db.db.weblibs.MutationList;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.json.JsonUtils;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

public class NotebookEntryHistory extends NotebookEntry {

	public static final String COLUMN_FAMILY = "notebookentryhistory";
	
	@Expose
	@NotNull
	@AssertValid
	private UUID sourceEntryId = null;
	
	public NotebookEntryHistory() {
		setId(UUID.random());
	}
	
	public static NotebookEntryHistory create(NotebookEntry ne) {
		NotebookEntryHistory neh = JsonUtils.fromJson(ne.getData(), NotebookEntryHistory.class);
		neh.setSourceEntryId(ne.getId());
		neh.setId(UUID.random());
		return neh;
		
	}
	
	@Override
	public String getColumnFamily() {
		return COLUMN_FAMILY;
	}
	
	@Override
	public MutationList getSaveMutations() {
		return new MutationList()
				.addInsertMutations(new IndexNotebookEntryHistoryJob(getId()))
				.addMutation(insertMutation(this));
	}

	public UUID getSourceEntryId() {
		return sourceEntryId;
	}

	public void setSourceEntryId(UUID sourceEntryId) {
		this.sourceEntryId = sourceEntryId;
	}
}
