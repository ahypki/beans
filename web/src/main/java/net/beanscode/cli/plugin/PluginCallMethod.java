package net.beanscode.cli.plugin;

import java.io.IOException;

import net.beanscode.cli.BeansCliCommand;
import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.users.User;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.cli.noninteractivecli.CliArg;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.RestMethod;
import net.hypki.libs5.pjf.components.jersey.SessionBean;
import net.hypki.libs5.pjf.op.Op;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.url.Params;

import com.google.gson.JsonElement;

public class PluginCallMethod extends BeansCliCommand {
	
	@CliArg(name = "plugin-name",
			description = "Plugin name to enable",
			required = true)
	private String pluginName = null;
	
	@CliArg(name = "method-name",
			description = "",
			required = true)
	private String methodName = null;
	
	@CliArg(name = "user",
			description = "User for which Plugin will be enabled",
			required = true)
	private String email = null;
	
	@CliArg(name = "params",
			description = "",
			required = true)
	private String params = null;

	public PluginCallMethod(SessionBean sb) {
		super(RestMethod.POST, sb);
	}

	@Override
	public String getUrlPath() {
		return "api/plugin/call";
	}

	@Override
	public void processResponse() {
		LibsLogger.debug(PluginCallMethod.class, "Plugin ", getPluginName(), " method ", getRestMethodType(), " called for user ", getEmail());
	}

	@Override
	public Params getRESTParams() {
		return new Params()
			.add("userId", getUserIdFromEmail().getId())
			.add("pluginName", getPluginName())
			.add("methodName", getRestMethodType())
			.add("params", getParams())
			;
	}

	@Override
	public String getCLICommandName() {
		return "plugin call";
	}

	@Override
	public String getCLICommandDescription() {
		return "Calls a method from a given plugin for a given user";
	}
	
	public OpList run(String userEmail, String pluginName, String methodName, String params) throws CLIException, IOException {
		super.run(new String[] {"--user", userEmail, "--plugin-name", pluginName, 
				"--method-name", methodName, "--params", params});
		
		OpList op = new OpList();
		if (getResponse().getAsJson() != null)
			for (JsonElement je : getResponse().getAsJson().getAsJsonArray()) {
				op.add(JsonUtils.fromJson(je, Op.class));
			}
		return op;
	}

	public String getPluginName() {
		return pluginName;
	}

	public void setPluginName(String pluginName) {
		this.pluginName = pluginName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public UUID getUserIdFromEmail() {
		try {
			return new ApiCallCommand(getSessionBean())
						.setApi("api/user/get")
						.addParam("userEmail", getEmail())
						.run()
						.getResponse()
						.getAsObject(User.class)
						.getId();
		} catch (CLIException | IOException e) {
			LibsLogger.error(PluginCallMethod.class, "Cannot get user ID based on email", e);
			return null;
		}
	}

	protected String getRestMethodType() {
		return methodName;
	}

	protected void setRestMethodName(String methodName) {
		this.methodName = methodName;
	}

	public String getParams() {
		return params;
	}

	public void setParams(String params) {
		this.params = params;
	}
}
