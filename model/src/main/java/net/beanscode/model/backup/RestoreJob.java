package net.beanscode.model.backup;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;

import java.io.IOException;

import kotlin.NotImplementedError;
import net.beanscode.model.cass.notification.Notification;
import net.beanscode.model.cass.notification.NotificationType;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.jobs.Job;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class RestoreJob extends Job {
	
	@Expose
	private boolean restoreUsersGroups = true;
	
	@Expose
	private boolean restoreDatasetsTables = true;
	
	@Expose
	private boolean restoreNotebooks = true;
	
	@Expose
	@NotNull
	private String restoreFile = null;
	
	@Expose
	@NotNull
	@AssertValid
	private UUID userId = null;
	
	@Expose
	@NotNull
	private RestoreStrategy restoreStrategy = RestoreStrategy.CREATE_OBJECTS_IF_NOT_EXIST;
	
	@Expose
//	@NotNull
	private String withPrefix = null;
	
	@Expose
//	@NotNull
	private String withSuffix = null;

	public RestoreJob() {
		
	}
	
	public RestoreJob(UUID userId, String restoreFile) {
		setUserId(userId);
		setRestoreFile(restoreFile);
	}
	
	@Override
	public void run() throws IOException, ValidationException {
//		if (isRestoreNotebooks())
//			BackupManager.restoreNotebooks(getUserId(), new File("backups/" + getRestoreFile()));
		final Notification notification = new Notification()
			.setUserId(getUserId())
			.setTitle("Restore")
			;
		
		try {
			notification
				.setDescription("Restoring objects...")
//				.setLocked(true)
				.setNotificationType(NotificationType.INFO)
				.save();
		
//			Setting backupPath = SettingFactory.getSetting(BeansSettings.SETTING_BACKUP_PATH);
			
//			BackupUtils.restoreKeyspace(WeblibsConst.KEYSPACE, null, 
//					backupPath.getValue("path").getValueAsString() + "/" + getRestoreFile());
			
//			if (true)
//				throw new NotImplementedError("bum");
			BackupManager.restoreKeyspace(WeblibsConst.KEYSPACE, this);

			notification
				.setDescription("Restore done successfully")
				.setNotificationType(NotificationType.OK)
				.save();
		} catch (Exception e) {
			LibsLogger.error(RestoreJob.class, "Cannot restore database. Consuming job..", e);
			
			notification
				.setDescription("Error while doing restore")
				.setNotificationType(NotificationType.ERROR)
				.save();
		}
	}

//	public boolean isRestoreNotebooks() {
//		return restoreNotebooks;
//	}
//
//	public RestoreJob setRestoreNotebooks(boolean restoreNotebooks) {
//		this.restoreNotebooks = restoreNotebooks;
//		return this;
//	}

	public UUID getUserId() {
		return userId;
	}

	public RestoreJob setUserId(UUID userId) {
		this.userId = userId;
		return this;
	}

	public String getRestoreFile() {
		return restoreFile;
	}

	public RestoreJob setRestoreFile(String backupFile) {
		this.restoreFile = backupFile;
		return this;
	}

	public boolean isRestoreUsersGroups() {
		return restoreUsersGroups;
	}

	public RestoreJob setRestoreUsersGroups(boolean restoreUsersGroups) {
		this.restoreUsersGroups = restoreUsersGroups;
		return this;
	}

	public boolean isRestoreDatasetsTables() {
		return restoreDatasetsTables;
	}

	public RestoreJob setRestoreDatasetsTables(boolean restoreDatasetsTables) {
		this.restoreDatasetsTables = restoreDatasetsTables;
		return this;
	}

	public boolean isRestoreNotebooks() {
		return restoreNotebooks;
	}

	public RestoreJob setRestoreNotebooks(boolean restoreNotebooks) {
		this.restoreNotebooks = restoreNotebooks;
		return this;
	}

	public RestoreStrategy getRestoreStrategy() {
		return restoreStrategy;
	}

	public RestoreJob setRestoreStrategy(RestoreStrategy restoreStrategy) {
		this.restoreStrategy = restoreStrategy;
		return this;
	}

	public String getWithPrefix() {
		return withPrefix;
	}
	
	public boolean isWithPrefixDefined() {
		return notEmpty(this.withPrefix);
	}
	
	public boolean isWithSuffixDefined() {
		return notEmpty(this.withSuffix);
	}

	public RestoreJob setWithPrefix(String withPrefix) {
		this.withPrefix = notEmpty(withPrefix) ? withPrefix.trim() : null;
		return this;
	}

	public String getWithSuffix() {
		return withSuffix;
	}

	public RestoreJob setWithSuffix(String withSuffix) {
		this.withSuffix = notEmpty(withSuffix) ? withSuffix.trim() : null;
		return this;
	}
}
