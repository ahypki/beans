package net.beanscode.model.dataset;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.beanscode.model.BeansChannels;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.db.db.weblibs.utils.UUIDPair;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.jobs.Job;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class ClearTableJob extends Job {
	
	private static final UUID BULK_JOB_ID = UUID.random();
	
	@Expose
	@NotNull
	@NotEmpty
	@AssertValid
	private List<UUIDPair> datasetTableIdPair = null;

	public ClearTableJob() {
		setChannel(BeansChannels.CHANNEL_LONG_NORMAL);
	}

	public ClearTableJob(Table table) {
		getDatasetTableIdPair().add(new UUIDPair(table.getDatasetId(), table.getId()));
		setChannel(BeansChannels.CHANNEL_LONG_NORMAL);
	}

	@Override
	public void run() throws IOException, ValidationException {
		for (UUIDPair uuidPair : datasetTableIdPair) {
			try {
				DbObject.getDatabaseProvider().removeTable(WeblibsConst.KEYSPACE, uuidPair.getValue1().getId());
			} catch (Exception e) {
				LibsLogger.error(ClearTableJob.class, "Cannot drop CF " + uuidPair.getValue1() + " for user " + uuidPair.getValue0(), e);
			}
		}
	}

	public List<UUIDPair> getDatasetTableIdPair() {
		if (datasetTableIdPair == null)
			datasetTableIdPair = new ArrayList<>();
		return datasetTableIdPair;
	}

	public void setDatasetTableIdPair(List<UUIDPair> datasetTableIdPair) {
		this.datasetTableIdPair = datasetTableIdPair;
	}
}
