package net.beanscode.web.view.settings;

import static org.rendersnake.HtmlAttributesFactory.class_;

import java.io.IOException;

import net.beanscode.web.beta.BetaContent;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.BetaButtonGroup;

import org.rendersnake.HtmlAttributesFactory;
import org.rendersnake.HtmlCanvas;

public class AccountSettingsBetaContent extends BetaContent {
	
	public AccountSettingsBetaContent() {
		
	}
	
	public AccountSettingsBetaContent(BeansComponent parent) {
		super(parent);
	}
	
	@Override
	protected void renderHeader(HtmlCanvas html) throws IOException {
		html
			.h1()
				.content("Settings: " + getUserEmail());
	}
		
	@Override
	protected void renderContent(HtmlCanvas html) throws IOException {				
		html
			.div(class_("row"))
				.render(new DefaultEntryAutoUpdateCard(this))
				
				.render(new AutoUpdatesCard(this))
			._div();
		
		html
			.h2()
				.content("Other settings")
			.render(new SettingsTableComponent(this)
					.setWithSystemSettings(false));
	}

	@Override
	protected BetaButtonGroup getMenu() throws IOException {
		return null;
	}

	@Override
	protected String getMenuName() throws IOException {
		return null;
	}
}
