package net.beanscode.model.demo;

import static java.lang.String.format;
import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.beanscode.model.BeansConst;
import net.beanscode.model.notebook.Notebook;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.args.ArgsUtils;
import net.hypki.libs5.weblibs.SearchManager;
import net.hypki.libs5.weblibs.jobs.JobsManager;
import net.hypki.libs5.weblibs.user.User;

public class DemoNotebooks {

	public static void main(String[] args) throws ValidationException, IOException {
		BeansConst.init(new String[] {"--settings", "beans-dev.json"});

		final String userEmail = ArgsUtils.getString(args, "user");
		assertTrue(notEmpty(userEmail), "Arg --user is not specified");
		
		final User user = SearchManager.searchUsers(userEmail, 0, 1).getObjects().get(0);
		assertTrue(user != null, format("User %s not found", userEmail));
		
		for (Notebook notebook : demoNotebooks(user)) {
			notebook.save();
		};
		
		JobsManager.runAllJobs(BeansConst.getAllChannelsNames());
		
		LibsLogger.debug(DemoNotebooks.class, "Finished!");
	}
	
	public static List<Notebook> demoNotebooks(final User user) throws IOException, ValidationException {
		List<Notebook> notebooks = new ArrayList<>();
		
		// Harris queries
		Notebook notebook = new Notebook(user.getUserId(), "Demo Harris catalogue queries");
		notebook.setDescription("This set of plots and queries presents the features of BEANS software using Harris Catalogue - a catalogue "
				+ "containing the global properties of almost all star clusters present in the Milky Way.");
		notebooks.add(notebook);
		
		// MOCCA queries
		notebook = new Notebook(user.getUserId(),  "Demo MOCCA queries");
		notebooks.add(notebook);
		
		return notebooks;
	}
}
