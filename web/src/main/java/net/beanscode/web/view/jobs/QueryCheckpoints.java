package net.beanscode.web.view.jobs;

import java.io.IOException;

import net.beanscode.web.view.ui.BeansComponent;
import net.hypki.libs5.pjf.components.ops.OpComponentTimer;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.utils.LibsLogger;

import org.rendersnake.HtmlCanvas;


public class QueryCheckpoints extends BeansComponent {
	
	public static final int REFRESH_INTERVAL = 3000;
	
	public QueryCheckpoints() {
		
	}
	
	public QueryCheckpoints(BeansComponent parent) {
		super(parent);
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		renderJobsPanel(html);
		
		getOpList()
			.add(new OpComponentTimer(this, "refresh", REFRESH_INTERVAL));
	}
	
	private OpList refresh() throws IOException {
		LibsLogger.debug(QueryCheckpoints.class, "Refreshing jobs statuses...");
		
		return new OpList();
//			.add(new OpSet("#jobs-dropdown #info dl", renderJobsPanel(new HtmlCanvas()).toHtml()));
	}
	
	private HtmlCanvas renderJobsPanel(HtmlCanvas panel) throws IOException {
//		List<QueryStatusSorted> checkpoints = QueryStatusSorted.searchLastUpdated(getSessionBean().getUserId(), System.currentTimeMillis(), 15);
//				
//		panel
//			.div(class_("query-checkpoints"))
//			.h4()
//				.content("Statuses of the last 15 queries");
//		
//		if (checkpoints != null) {
//			for (QueryStatusSorted queryCheckpoint : checkpoints) {
//				
//				if (queryCheckpoint.getStatus() == QueryStatusType.SUBMITTED) {
//					panel
//						.dt(class_("label label-default"))
//							.content("Submitted")
//						.dd()
//							.span(class_("query-name"))
//								.content(queryCheckpoint.getQueryName())
//						._dd();
//				} else if (queryCheckpoint.getStatus() == QueryStatusType.RUNNING) {
//					panel
//						.dt(class_("label label-info"))
//							.content("Running " + queryCheckpoint.getProgress() + "%")
//						.dd()
//							.span(class_("query-name"))
//								.content(queryCheckpoint.getQueryName())
//						._dd();
//				} else if (queryCheckpoint.getStatus() == QueryStatusType.FAILED) {
//					panel
//						.dt(class_("label label-danger"))
//							.content("Failed")
//						.dd()
//							.span(class_("query-name"))
//								.content(queryCheckpoint.getQueryName())
//							.write(StringUtilities.substringMax(queryCheckpoint.getMessage(), 0, 100))
//						._dd();
//				} else if (queryCheckpoint.getStatus() == QueryStatusType.DONE) {
//					panel
//						.dt(class_("label label-success"))
//							.content("Done")
//						.dd()
//							.span(class_("query-name"))
//								.content(queryCheckpoint.getQueryName())
//						._dd();
//				} else {
//					LibsLogger.error(QueryCheckpoints.class, "Unimplemented case for " + queryCheckpoint.getStatus());
//				}
//			}
//		}
//		
//		panel
//			._div();
//		
		return panel;
	}
	
//	@GET
//	@Path("list")
//	@RolesAllowed(Rights.ADMIN)
//	@Produces(MediaType.APPLICATION_JSON)
//	public String list(@Context SecurityContext sc) throws IOException, ValidationException {
//		SessionBean session = (SessionBean) sc.getUserPrincipal();
//		if (session == null || session.getUserId() == null)
//			return null;
//		
//		
//		
//		
//	}
}
