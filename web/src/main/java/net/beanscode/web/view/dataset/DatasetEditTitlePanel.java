package net.beanscode.web.view.dataset;

import static org.rendersnake.HtmlAttributesFactory.type;

import java.io.IOException;

import net.beanscode.pojo.dataset.Dataset;
import net.hypki.libs5.pjf.components.Component;

import org.rendersnake.HtmlCanvas;

public class DatasetEditTitlePanel extends Component {
	
	private Dataset dataset = null;
	
	public DatasetEditTitlePanel() {
		
	}
	
	public DatasetEditTitlePanel(Dataset dataset) {
		setDataset(dataset);
	}

	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.form()
				.span()
					.content("Dataset's title:")
				.br()
				.input(type("hidden")
						.name("userId")
						.value(getDataset().getUserId().getId()))
				.input(type("hidden")
						.name("datasetId")
						.value(getDataset().getId().getId()))
				.input(type("text")
						.name("name")
						.value(getDataset().getName())
						.style("width: 100%;"))
			._form()
			;
	}

	public Dataset getDataset() {
		return dataset;
	}

	public void setDataset(Dataset dataset) {
		this.dataset = dataset;
	}
}
