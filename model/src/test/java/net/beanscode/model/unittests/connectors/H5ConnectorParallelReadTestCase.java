package net.beanscode.model.unittests.connectors;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.beanscode.model.connectors.H5Connector;
import net.beanscode.model.plugins.ColumnDef;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.Watch;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.string.RandomUtils;

import org.junit.Test;

public class H5ConnectorParallelReadTestCase extends ConnectorTestCase {

	public H5ConnectorParallelReadTestCase() {
		
	}
	
	@Test
	public void testConnector() throws IOException {
		FileExt h5File = new FileExt("test.h5");
		h5File.deleteIfExists();
		
		H5Connector conn = new H5Connector(UUID.random(), h5File)
			.setUseCache(true);
		
		writeRows(conn, 66123);
		
		Row r1 = null;
		Row r2 = null;
		int readRows = 10123;
		int readRows2 = 10123;
		for (Row row : conn.iterateRows(null)) {
			r1 = row;
			if (--readRows == 0) {
				LibsLogger.info(H5ConnectorParallelReadTestCase.class, "Read " + readRows + " rows, opening "
						+ "another iterator");
				
				for (Row row2 : conn.iterateRows(null)) {
					r2 = row2;
					
					if (--readRows2 == 0) {
						LibsLogger.info(H5ConnectorParallelReadTestCase.class, "Read " + readRows + " rows, opening "
								+ "another iterator");
						
//						assertTrue(r1.equals(r2));
					}
				}
			}
		}
		
		LibsLogger.info(H5ConnectorParallelReadTestCase.class, "Finished");
	}
}
