package net.beanscode.web.view.dataset;

import static org.rendersnake.HtmlAttributesFactory.class_;

import java.io.IOException;
import java.util.List;

import net.beanscode.cli.BeansCLISettings;
import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.datasets.DatasetSearchResults;
import net.beanscode.pojo.dataset.Dataset;
import net.beanscode.web.view.ui.BetaTableComponent;
import net.beanscode.web.view.ui.SpinnerLabel;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpSet;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.url.Params;

import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

// TODO DatasetS
public class DatasetBetaTableComponent extends BetaTableComponent {
	
	@Expose
	private boolean compact = false;

	private DatasetSearchResults datasetSearchResults = null;
	
	public DatasetBetaTableComponent() {
		
	}
	
	public DatasetBetaTableComponent(Component parent) {
		super(parent);
	}
	
	@Override
	protected String getSettingsPrefix() {
		return "datasets-table-";
	}

	@Override
	protected void renderTitle(HtmlCanvas html) throws IOException {
		html
			.span()
				.content("Datasets");
	}

	@Override
	protected boolean isFilterVisible() throws IOException {
		return true;
	}

	@Override
	protected boolean isHeaderColumnVisible() throws IOException {
		return true;
	}

	@Override
	protected int getNumberOfColumns() throws IOException {
		if (isCompact())
			return 2;
		else
			return 4;
	}

	@Override
	protected String getColumnName(int col) throws IOException {
		if (col == 0)
			return "Name";
		else if (!isCompact() && col == 1)
			return "ID";
		else if ((!isCompact() && col == 2) || (isCompact() && col == 1))
			return "Created";
		else if (col == 3)
			return "Options";
		else
			return "";
	}

	@Override
	protected long getMaxHits() throws IOException {
		return getDatasetSearchResults().getMaxHits();
	}

	@Override
	protected void renderCell(int row, int col, HtmlCanvas html)
			throws IOException {
		Dataset ds = getDatasetSearchResults().getDatasets().get(row % getPerPage());
		
		if (col == 0)
			html
				.a(class_("beans-link")
						.onClick(new OpList()
							.add(new OpSet(".beans-content-body", new SpinnerLabel("Loading dataset...").toHtml()))
							.add(new OpComponentClick(DatasetBetaContent.class,
								UUID.random(),
								"onShow",
								new Params()
									.add("datasetId", ds.getId().getId())))							
							.toStringOnclick()))
					.content(ds.getName());
		else if (!isCompact() && col == 1)
			html
				.write(ds.getId().toString(" - "));
		else if ((!isCompact() && col == 2) || (isCompact() && col == 1))
			html
				.write(ds.getCreationDate().toStringHuman());
		else if (col == 3) {
			
		} else {
			
		}
	}


	private DatasetSearchResults getDatasetSearchResults() {
		if (datasetSearchResults == null) {
			try {
				datasetSearchResults = new ApiCallCommand(this)
											.setApi("api/dataset/query")
											.addParam("query", getFilter())
											.addParam("from", getPage() * getPerPage())
											.addParam("size", getPerPage())
											.run()
											.getResponse()
											.getAsObject(DatasetSearchResults.class);
				
				BeansCLISettings.set("datasetSearch", getFilter());
			} catch (CLIException | IOException e) {
				LibsLogger.error(DatasetBetaTableComponent.class, "Cannot search for Datasets", e);
				datasetSearchResults = new DatasetSearchResults();
			}
		}
		return datasetSearchResults;
	}
	
	@Override
	protected List<Button> getMenu() throws IOException {
		return null;
	}

	public boolean isCompact() {
		return compact;
	}

	public DatasetBetaTableComponent setCompact(boolean compact) {
		this.compact = compact;
		return this;
	}

	@Override
	protected boolean isExapandable() throws IOException {
		return false;
	}

	@Override
	protected void renderExpandable(int row, HtmlCanvas html) throws IOException {
		
	}

	@Override
	protected String getTableCss() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}
}
