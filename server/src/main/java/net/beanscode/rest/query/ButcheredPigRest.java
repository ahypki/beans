package net.beanscode.rest.query;

import static java.lang.String.format;
import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;

import java.io.IOException;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;

import net.beanscode.cli.notebooks.ButcheredPigEntry;
import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.notebook.NotebookEntry;
import net.beanscode.model.notebook.NotebookEntryFactory;
import net.beanscode.rest.BeansRest;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.op.OpSet;
import net.hypki.libs5.utils.LibsLogger;

@Path("api/butcheredpig")
public class ButcheredPigRest extends BeansRest {
	
	
	@POST
	@Path("edit")
	public void edit(@Context SecurityContext sc,
			@FormParam("queryId") String queryId,
			@FormParam("name") String newName,
			@FormParam("pigMode") String pigMode,
			@FormParam("query") String query,
			@FormParam("notes") String notes) throws IOException, ValidationException {
		
		NotebookEntry q = NotebookEntryFactory.getNotebookEntry(new UUID(queryId));
		
		validateNotFound(q != null, format("Cannot find query %s", queryId));
		
		final Notebook note = q.getNotebook();
		
		validateNotFound(note != null, "Notebook is not found");
		validateUnauthorized(isAdmin(sc) || note.isWriteAllowed(getUserId(sc)), "Permission denied");
		
		if (notEmpty(newName))
			q.setName(newName);
		if (notEmpty(query))
			q.setMeta(ButcheredPigEntry.META_QUERY, query);
		q.save();
		
		LibsLogger.debug(getClass(), format("Name for the query %s updated to %s", queryId, newName));
	}

	@POST
	@Path("run")
	public void run(@Context SecurityContext sc,
			@FormParam("queryId") String queryId,
			@FormParam("query") String query) throws IOException, ValidationException {
		NotebookEntry q = NotebookEntryFactory.getNotebookEntry(new UUID(queryId));
		
		validateNotFound(q != null, format("Cannot find query %s", queryId));
		
		final Notebook note = q.getNotebook();
		
		validateNotFound(note != null, "Notebook is not found");
		validateUnauthorized(isAdmin(sc) || note.isWriteAllowed(getUserId(sc)), "Permission denied");
		
		q.reload();
//		((ButcheredPigEntry) q).runQuery();
		
		try {
			// update last edit
			note.save();
		} catch (Throwable t) {
			LibsLogger.error(QueryRest.class, "Cannot update last edit for Notebook " + note, t);
		}
		
		addToResponse(new OpSet("#" + q.getId() + " .status .label", "SUBMITTED"));
		
//		return toString();
	}
}
