package net.beanscode.model.dataset;

import java.io.IOException;

import net.beanscode.model.BeansChannels;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.weblibs.jobs.Job;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class RemoveTableFromIndexJob extends Job {


	@Expose
	@NotNull
	@NotEmpty
	private String table = null;
	
	public RemoveTableFromIndexJob() {
		super(BeansChannels.CHANNEL_INDEX);
	}
	
	public RemoveTableFromIndexJob(Table table) {
		super(BeansChannels.CHANNEL_INDEX);
		setTable(table.getData());
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		final Table tab = getTable();
		
		if (tab != null)
			tab.deindex();
	}

	private Table getTable() {
		try {
			return JsonUtils.fromJson(table, Table.class);
		} catch (Exception e) {
			LibsLogger.error(RemoveTableFromIndexJob.class, "Cannot deserialize Table", e);
			return null;
		}
	}

	private void setTable(String table) {
		this.table = table;
	}
	
}
