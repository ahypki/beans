package net.beanscode.web.view.users;

import static org.rendersnake.HtmlAttributesFactory.id;

import java.io.IOException;

import net.beanscode.cli.users.Group;
import net.beanscode.web.view.ui.BeansComponent;
import net.hypki.libs5.pjf.components.Component;

import org.rendersnake.HtmlCanvas;

public class EditGroupPanel extends BeansComponent {
	
	private Group group = null;
	
	public EditGroupPanel() {
		
	}

	public EditGroupPanel(Component parent) {
		super(parent);
	}
	
	public EditGroupPanel(Component parent, Group group) {
		super(parent);
		this.group = group;
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.input(id(group != null ? group.getId().getId() : "")
				.name("id")
				.value(group != null ? group.getId().getId() : "")
				.type("hidden"))
			.label()
				.content("Name")
			.input(id("name")
					.name("name")
					.value(group != null ? group.getNameNotNull() : "")
					.class_("panel edit"))
			.label()
				.content("Description")
			.textarea(id("description")
					.name("description")
					.class_("panel edit"))
				.content(group != null ? group.getDescriptionNotNull() : "")
//			._textarea()
			;
	}
}
