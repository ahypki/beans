package net.beanscode.model.dataset;

public enum TableSearchStatus {
	NOT_INDEXED,
	INDEXING,
	INDEXED;

	public String toString(boolean humanReadable) {
		if (humanReadable == false)
			return super.toString();
		
		if (this.ordinal() == 0)
			return "Not indexed yet";
		else if (this.ordinal() == 1)
			return "Indexing in progress...";
		else
			return "Fully indexed";
	}
}
