package net.beanscode.rest;


import java.net.URI;

import javax.ws.rs.core.UriBuilder;

//import org.apache.wicket.protocol.http.WebApplication;
import org.eclipse.jetty.server.Server;
import org.glassfish.jersey.jetty.JettyHttpContainerFactory;
import org.glassfish.jersey.server.ResourceConfig;
//import javax.servlet.DispatcherType;
//import org.eclipse.jetty.webapp.WebAppContext;

import net.beanscode.cli.BeansCliCommand;
import net.beanscode.model.BeansConst;
import net.beanscode.model.agent.BeansAgent;
import net.beanscode.rest.jersey.JerseyApplication;
import net.hypki.libs5.db.weblibs.Settings;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.args.ArgsUtils;
import net.hypki.libs5.utils.collections.ArrayUtils;
import net.hypki.libs5.weblibs.jobs.JobsManager;


public class BeansServerMain {
	
	public static void main(String[] args) throws Exception {
		// default settings
		if (args != null && !ArgsUtils.exists(args, "settings"))
			args = (String []) ArrayUtils.concat(args, new String[] {"settings", "beans-dev.json"});
		
		// default logger
		if (ArgsUtils.exists(args, "verbose")) {
			System.setProperty(LibsLogger.LOG4J_SYSTEM_PROPERTY_NAME, "log4j-debug.properties");
			LibsLogger.debug(BeansServerMain.class, "Log4j set to ", "log4j-debug.properties");
		} else {
			System.setProperty("log4j", "log4j.properties");
			LibsLogger.debug(BeansServerMain.class, "Log4j set to ", "log4j.properties");
		}
		
		LibsLogger.debug(BeansServerMain.class, "Starting server...");
		
		BeansConst.init(args);
		
		// initializing hazelcast
		BeansConst.initHazelcast();
		
		// getting port number for jetty
		int port = ArgsUtils.getInt(args, "port", 25000);
//		if (System.getProperty("port") != null)
//			port = Integer.parseInt(System.getProperty("beansJettyPort"));
//		else
//			port = Settings.getInt("Jetty/port", 25000);
		
		if (ArgsUtils.exists(args, "agent")) {
			BeansAgent.main(args);
			System.exit(0);
		}
		
		BeansCliCommand.setPort(port);
		
		ResourceConfig rc = new JerseyApplication();
		URI baseUri = UriBuilder.fromUri("http://localhost/").port(port).build();
		Server server = JettyHttpContainerFactory.createServer(baseUri, rc);		
		server.setStopTimeout(2 * 3600 * 1000 /*2 hours*/);

//		Server server = new Server(port);
//		ServletContextHandler ctx = new ServletContextHandler(ServletContextHandler.NO_SESSIONS);
//		ctx.setContextPath("/");
//		server.setHandler(ctx);
//		ServletHolder serHol = ctx.addServlet(ServletContainer.class, "/rest/*");
//		serHol.setInitOrder(1);
//		serHol.setInitParameter("jersey.config.server.provider.packages", "com.zetcode.res");

		
		new Thread(){ 
			@Override
			public void run() {
				JobsManager.init(BeansConst.getAllChannelsNames());
			}
		}.start();
		
		try {
			server.start();
			
//			LibsLogger.info(BeansServerMain.class, "");
			LibsLogger.info(BeansServerMain.class, "BEANS started at http://localhost:", port);
//			LibsLogger.info(BeansServerMain.class, "");
			
			server.join();
		} finally {
			server.destroy();
		}
	}
}
