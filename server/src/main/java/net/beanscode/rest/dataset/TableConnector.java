package net.beanscode.rest.dataset;

import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import mjson.Json;
import net.beanscode.model.connectors.BeansConnector;
import net.beanscode.model.connectors.ConnectorLink;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.dataset.TableFactory;
import net.beanscode.model.plugins.Connector;
import net.beanscode.rest.BeansRest;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;

@Path("api/table/connector")
public class TableConnector extends BeansRest {
	
	@GET
	@Path("list")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getConnectorList(@Context SecurityContext sc,
			@QueryParam("tableId") UUID tableId) throws IOException, ValidationException {
		final Table table = TableFactory.getTable(tableId);
		
		validateNotFound(table != null, "Dataset is not found");
		validateUnauthorized(isAdmin(sc) || table.isReadAllowed(getUserId(sc)), "Permission denied");
		
		Json connectors = Json.array();
		
		for (ConnectorLink conn : table.getConnectorList().getConnectorsLinks()) {
			connectors.add(Json
					.object()
					.set("name", conn.getConnectorInstance().getName())
					.set("init", conn.getConnectorInstance().getInitParams().toString()));
		}
		
		return Response
				.ok()
				.entity(connectors.toString())
				.build();
	}
	
	@POST
	@Path("add")
	public void addConnector(@Context SecurityContext sc, 
			@FormParam("tableId") List<UUID> tableIds,
			@FormParam("connectorClass") List<String> connectorClasses,
			MultivaluedMap<String, String> meta) throws IOException, ValidationException {
		
		try {
			// validation
			validateBadRequest(connectorClasses.size() == tableIds.size(), "Number of connector classes is different than number od tables IDs, cannot add connectors");

			Iterator<String> connClassesIter = connectorClasses.iterator();
			for (UUID tableId : tableIds) {
				String connectorClass = connClassesIter.next();
				if (connectorClass.startsWith("["))
					connectorClass = connectorClass.replace("[", "").replace("]", "");
				
				final Table table = TableFactory.getTable(tableId);
				
				validateNotFound(table != null, "Dataset is not found");
				validateUnauthorized(isAdmin(sc) || table.isWriteAllowed(getUserId(sc)), "Permission denied");
				
				Connector conn = (Connector) Class.forName(connectorClass).newInstance();// ConnectorFactory.getConnector(connectorClass);
				conn.getInitParams().setTableId(tableId);
				
				for (Entry<String, List<String>> m : meta.entrySet()) {
					if (m.getKey().equals("datasetId")
							|| m.getKey().equals("tableId")
							|| m.getKey().equals("connectorClass"))
						continue;
					
					assertTrue(m.getValue().size() <= 1, "Curently only one value is allowed for every 'key' in 'meta' param");
					
					conn.getInitParams().getMeta().add(m.getKey(), m.getValue().get(0));
				}
				
				new BeansConnector(conn, UUID.random().getId())
					.save();	
			}
		} catch (Throwable e) {
			LibsLogger.error(TableRest.class, "Cannot add connector to tables", e);
		}
	}
}
