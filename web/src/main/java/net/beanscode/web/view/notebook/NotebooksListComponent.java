package net.beanscode.web.view.notebook;

import static net.hypki.libs5.utils.string.RandomUtils.nextInt;
import static org.rendersnake.HtmlAttributesFactory.class_;

import java.io.IOException;

import javax.ws.rs.core.SecurityContext;

import org.rendersnake.HtmlAttributesFactory;
import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

import net.beanscode.cli.BeansCLISettings;
import net.beanscode.cli.notebooks.Notebook;
import net.beanscode.cli.notebooks.NotebookGateway;
import net.beanscode.cli.notebooks.NotebookSearchResults;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.Link;
import net.beanscode.web.view.ui.ListComponent;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.Op;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.RandomUtils;
import net.hypki.libs5.utils.url.Params;

public class NotebooksListComponent extends ListComponent {
	
	@Expose
	private OpList onClick = null;
	
	private NotebookSearchResults notebookSearchResults = null;

	public NotebooksListComponent() {
		
	}
	
	public NotebooksListComponent(BeansComponent parent) {
		super(parent);
	}
	
	public NotebooksListComponent(SecurityContext securityContext, Params params) {
		super(securityContext, params);
	}
	
	protected NotebookSearchResults getNotebookSearchResults() {
		if (notebookSearchResults == null) {
			try {
				notebookSearchResults = NotebookGateway.searchNotebooks(this.getSessionBean(), getFilter(), getPage(), getPerPage());
			} catch (CLIException | IOException e) {
				LibsLogger.error(NotebooksTableComponent.class, "Cannot search for Notebooks", e);
				notebookSearchResults = new NotebookSearchResults();
			}
		}
		return notebookSearchResults;
	}
	
	@Override
	protected String getSettingsPrefix() {
		return "notebooks-list-";
	}
	
	@Override
	protected String getFilterTooltip() {
		return "Filter notebooks...";
	}
	
	@Override
	protected long getMaxHits() throws IOException {
		return getNotebookSearchResults().getMaxHits();
	}
	
	@Override
	protected void renderItem(int idx, HtmlCanvas html) throws IOException {
		Notebook n = getNotebookSearchResults().getNotebooks().get(idx % getPerPage());
		
		html
//			.span(class_("").add("style", "background-color: rgb(" + nextInt(255) + "," + nextInt(255) + "," + nextInt(255) + ")"))
//				.content(new Link(n.getName(), getOnClick().addParamParam("notebookId", n.getId().getId())).toHtml(), false);
			.render(new Link(n.getName(), 
					getOnClick()
						.addParamParam("notebookId", n.getId().getId()))
					.addHtmlClass("name"))
			.render(new Link(null, 
					"link",
					new OpComponentClick(NotebookContent.class, 
							"onShow",
							new Params().add("notebookId", n.getId().getId())))
					.addHtmlClass("link"));
	}

	public OpList getOnClick() {
		if (onClick == null)
			onClick = new OpList();
		return onClick;
	}

	public NotebooksListComponent setOnClick(OpList onClick) {
		this.onClick = onClick;
		return this;
	}
}
