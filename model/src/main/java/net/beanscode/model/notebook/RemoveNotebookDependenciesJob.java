package net.beanscode.model.notebook;

import java.io.IOException;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.weblibs.SearchManager;
import net.hypki.libs5.weblibs.jobs.Job;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class RemoveNotebookDependenciesJob extends Job {
	
	@Expose
	@NotNull
	@NotEmpty
	@AssertValid
	private String notebookJson = null;

	public RemoveNotebookDependenciesJob() {
		
	}
	
	public RemoveNotebookDependenciesJob(Notebook notebook) {
		setNotebookJson(notebook.getData());
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		final Notebook notebook = getNotebook();
		
		SearchManager.removeObject(notebook.getCombinedKey(), Notebook.COLUMN_FAMILY);
		LibsLogger.debug(RemoveNotebookDependenciesJob.class, String.format("Removed %s from index %s", getCombinedKey(), Notebook.COLUMN_FAMILY));
		
		for (NotebookEntry ne : notebook.iterateEntries()) {
			if (ne != null)
				ne.remove();
		}
	}

	public Notebook getNotebook() {
		return JsonUtils.fromJson(getNotebookJson(), Notebook.class);
	}

	public String getNotebookJson() {
		return notebookJson;
	}

	public void setNotebookJson(String notebookJson) {
		this.notebookJson = notebookJson;
	}
}
