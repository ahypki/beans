package net.beanscode.web.view.settings;

import java.io.IOException;

import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpSet;
import net.hypki.libs5.utils.url.Params;

import org.rendersnake.HtmlCanvas;

public class InternalsContent extends Component {

	public InternalsContent() {
		
	}
	
	public OpList showContent(Params params) throws IOException {
		HtmlCanvas content = new HtmlCanvas()
			.h3()
				.content("Stress testing")
			
			.render(new StressTestingPanel())
			;
		
		return new OpList()
			.add(new OpSet("#page-title", "BEANS internals"))
			.add(new OpSet("#main", content.toHtml()))
			;
	}
}
