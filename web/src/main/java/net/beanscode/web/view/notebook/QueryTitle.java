package net.beanscode.web.view.notebook;

import static net.hypki.libs5.pjf.OpsFactory.opsCall;
import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.name;

import java.io.IOException;

import net.beanscode.pojo.NotebookEntry;
import net.hypki.libs5.pjf.op.OpDialogOpen;
import net.hypki.libs5.utils.LibsLogger;

import org.rendersnake.HtmlCanvas;
import org.rendersnake.Renderable;

public class QueryTitle implements Renderable {
	
	private NotebookEntry entry = null;
	
	public QueryTitle() {
		
	}

	public QueryTitle(NotebookEntry entry) {
		this.entry = entry;
	}
	
	public HtmlCanvas onHtmlCanvas() {
		try {
			return new HtmlCanvas()
				.render(this);
		} catch (IOException e) {
			LibsLogger.error(QueryTitle.class, "Cannot create html canvas", e);
			return null;
		}
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.span(class_("title")
					.onDblclick(opsCall(new OpDialogOpen("api/query/edit", "Editing entry name", queryNameEdit().toHtml()))))
				.content(entry.getName());
	}
	
	public HtmlCanvas queryNameEdit() throws IOException {
		return new HtmlCanvas()
					.form(class_("query-form dialog-form"))
						.label()
							.content("New query name")
						.br()
						.input(name("name")
								.type("text")
								.value(entry.getName()))
						.input(name("notebookId")
								.type("hidden")
								.value(entry.getNotebookId().getId()))
						.input(name("queryId")
								.type("hidden")
								.value(entry.getId().getId()))
					._form()
					;
	}
}
