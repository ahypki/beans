package net.beanscode.model.connectors;

import static net.hypki.libs5.db.db.weblibs.MutationType.INSERT;
import static net.hypki.libs5.db.db.weblibs.MutationType.REMOVE;

import java.io.IOException;
import java.util.Iterator;

import com.google.gson.annotations.Expose;

import net.beanscode.model.BeansObject;
import net.beanscode.model.plugins.Connector;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.schema.TableSchema;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.db.db.weblibs.Mutation;
import net.hypki.libs5.db.db.weblibs.MutationList;
import net.hypki.libs5.db.db.weblibs.MutationType;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.search.query.Query;
import net.hypki.libs5.weblibs.SearchManager;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

public class BeansConnector extends BeansObject<BeansConnector> implements Iterable<Row> {
	
	public static final String COLUMN_FAMILY = "connector";
	
	public static final String COLUMN_TABLEID = "tableid";
	
	@Expose
	@NotNull
	@AssertValid
	private String id = null;
	
	@Expose
	@NotNull
	@AssertValid
	private UUID tableId = null;
	
	@Expose
//	@NotNull
	@AssertValid
	private ConnectorLink connectorLink = null;
	
	@Expose
	@NotNull
	@AssertValid
	private BeansConnectorStatus status = null;
	
	private Connector connector = null;

	public BeansConnector() {
		
	}
	
	public BeansConnector(Connector connector, String connectorId) {
		setId(connectorId);
		setStatus(BeansConnectorStatus.NEW);
		setTableId(connector.getInitParams().getTableId());
		setConnector(connector);
	}
	
	public BeansConnector(ConnectorLink connector, String connectorId) {
		setId(connectorId);
		setStatus(BeansConnectorStatus.NEW);
		setTableId(connector.getConnectorInitParams().getTableId());
		setConnectorLink(connector);
	}
	
	@Override
	public String getColumnFamily() {
		return COLUMN_FAMILY;
	}
	
//	@Override
//	public TableSchema getTableSchema() {
//		return new TableSchema(getColumnFamily())
//				.addColumn(COLUMN_TABLEID, 	ColumnType.STRING, 	true, true)
//				.addColumn(COLUMN_PK, 		ColumnType.STRING, 	true, true)
//				.addColumn(COLUMN_DATA, 	ColumnType.STRING)
//				;
//	}
	
	@Override
	public TableSchema getTableSchema() {
		return super.getTableSchema()
				.addColumn(COLUMN_TABLEID, ColumnType.STRING, true, true);
	}
	
	@Override
	public String getKey() {
		return getTableId() + "-" + getId();
	}
	
	@Override
	public void index() throws IOException {
		SearchManager.index(this);
	}
	
	@Override
	public void deindex() throws IOException {
		SearchManager.removeObject(getCombinedKey().toLowerCase(), getColumnFamily());
	}
	
	@Override
	public MutationList getSaveMutations() {
		MutationList muts = new MutationList();
		
		muts
			.addMutation(new Mutation(INSERT, 
					WeblibsConst.KEYSPACE, 
					COLUMN_FAMILY, 
					new C3Where()
						.addClause(new C3Clause(COLUMN_TABLEID, ClauseType.EQ, getTableId().getId()))
						.addClause(new C3Clause(COLUMN_PK, ClauseType.EQ, getKey())),
					DbObject.COLUMN_DATA, 
					getData()
					));
		
		return muts;
	}
	
	@Override
	public MutationList getRemoveMutations() {
		return new MutationList()
				.addMutation(new Mutation(REMOVE, 
						WeblibsConst.KEYSPACE, 
						COLUMN_FAMILY, 
						new C3Where()
							.addClause(new C3Clause(COLUMN_TABLEID, ClauseType.EQ, getTableId().getId()))
							.addClause(new C3Clause(COLUMN_PK, ClauseType.EQ, getKey())),
						DbObject.COLUMN_DATA, 
						getData()
						));
	}
	
	@Override
	public Iterator<Row> iterator() {
		return getConnector().iterateRows(null).iterator();
	}
	
	public Iterator<Row> iterator(Query filter) {
		return getConnector().iterateRows(filter).iterator();
	}

	public UUID getTableId() {
		return tableId;
	}

	public BeansConnector setTableId(UUID tableId) {
		this.tableId = tableId;
		return this;
	}

	public String getId() {
		return id;
	}

	public BeansConnector setId(String id) {
		this.id = id;
		return this;
	}

	public ConnectorLink getConnectorLink() {
		return connectorLink;
	}

	public BeansConnector setConnectorLink(ConnectorLink connectorLink) {
		this.connectorLink = connectorLink;
		return this;
	}
	
	public BeansConnector setConnector(Connector connector) {
		this.connectorLink = new ConnectorLink(connector);
		return this;
	}
	
	public Connector getConnector() {
		if (connector == null
				&& getConnectorLink() != null) {
			connector = getConnectorLink().getConnectorInstance();
		}
		return connector;
	}

	public BeansConnectorStatus getStatus() {
		return status;
	}

	public BeansConnector setStatus(BeansConnectorStatus status) {
		this.status = status;
		return this;
	}
}
