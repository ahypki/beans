package net.beanscode.model.notebook;

// TODO make a class from it, with properties like from PlotStatus
public enum NotebookEntryStatus {
	NEW,
	RUNNING,
	SUCCESS,
	FAIL
}
