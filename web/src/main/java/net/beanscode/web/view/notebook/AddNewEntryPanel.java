package net.beanscode.web.view.notebook;

import static org.rendersnake.HtmlAttributesFactory.id;
import static org.rendersnake.HtmlAttributesFactory.type;

import java.io.IOException;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.notebooks.Notebook;
import net.beanscode.web.view.search.BeansExplorer;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.Link;
import net.beanscode.web.view.ui.TabsBetaComponent;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.beanscode.web.view.ui.modals.OpModalQuestion;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpDialogClose;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpReplace;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.url.Params;

import org.rendersnake.HtmlCanvas;
import org.rendersnake.Renderable;

import com.google.gson.annotations.Expose;

public class AddNewEntryPanel extends BeansComponent {
	
	@Expose
	private String parentId = null;
	
	@Expose
	private String parentClass = null;
	
	@Expose
	private String parentMethodTocall = null;
	
	@Expose
	private UUID notebookId = null;
	
	@Expose
	private String position = null;
	
	@Expose
	private UUID refEntryId = null;
	
	private Notebook notebook = null;
	
	public AddNewEntryPanel() {
		
	}
	
	public AddNewEntryPanel(Component parent, String parentMethodToCall, UUID notebookId) {
		super(parent);
		setParentId(parent.getId());
		setParentClass(parent.getClass().getName());
		setParentMethodTocall(parentMethodToCall);
		setNotebookId(notebookId);
	}
	
	public AddNewEntryPanel(Component parent, String parentMethodToCall, UUID notebookId, String position, UUID refEntryId) {
		super(parent);
		setParentId(parent.getId());
		setParentClass(parent.getClass().getName());
		setParentMethodTocall(parentMethodToCall);
		setNotebookId(notebookId);
		setPosition(position);
		setRefEntryId(refEntryId);
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.div(id(getId()))
				.input(type("hidden")
						.name("notebookEntryClass")
						.class_("notebookEntryClass"))
				.input(type("hidden")
						.name("notebookId")
						.value(getNotebookId().getId()))
				.input(type("hidden")
						.name("refEntryId")
						.value(getRefEntryId() != null ? getRefEntryId().getId() : ""))
				.input(type("hidden")
						.name("position")
						.value(getPosition() != null ? getPosition() : ""))
				;
		
		html
				.render(new TabsBetaComponent() {
					
					@Override
					public int getTabsCount() {
						return 2;
					}
					
					@Override
					public String getTabName(int tabIndex) {
						if (tabIndex == 0)
							return "New entry";
						else if (tabIndex == 1)
							return "Clone existing entry";
						else if (tabIndex == 2)
							return "Clone whole notebook";
						else
							return "";
					}
					
					@Override
					public Renderable getTabHtml(int tabIndex) throws IOException {
						if (tabIndex == 0) {
							return new NewEntryPanel(AddNewEntryPanel.this,
									new OpList()
										.add(new OpComponentClick(getParentClass(), 
												getParentId(), 
												getParentMethodTocall(), 
												getParams()
													.add("notebookId", getNotebookId().getId())
													.add("destinationNotebookId", getNotebookId().getId())
													.add(getRefEntryId() != null, "refEntryId", getRefEntryId())
													.add(getPosition() != null, "position", getPosition())
													))
//										.add(new OpBeansInfo("Entry added"))
										)
								.setPerPage(30);
						} else if (tabIndex == 1) {
							return new BeansExplorer(AddNewEntryPanel.this)
									.setDatasetsVisible(false)
									.addAfterEntry(new Link(null, 
											"fa fa-clone",
											new OpComponentClick(AddNewEntryPanel.this.getParentClass(), 
													AddNewEntryPanel.this.getParentId(), 
													AddNewEntryPanel.this.getParentMethodTocall(),
													getParams()
														.add("notebookId", AddNewEntryPanel.this.getNotebookId().getId())
														.add("destinationNotebookId", AddNewEntryPanel.this.getNotebookId().getId())
														.add(AddNewEntryPanel.this.getRefEntryId() != null, "refEntryId", AddNewEntryPanel.this.getRefEntryId())
														.add(AddNewEntryPanel.this.getPosition() != null, "position", AddNewEntryPanel.this.getPosition())
													)));
						} else if (tabIndex == 2) {
//							return new NotebooksTableComponent(NotebookEntriesPanel.this);//new QuickSearchPanel(NotebookEntriesPanel.this);
							return new Renderable() {
								
								@Override
								public void renderOn(HtmlCanvas html) throws IOException {
									html
										.render(new CloneNotebooksTableComponent(AddNewEntryPanel.this,
												getParentId(),
												getNotebookId().getId()));
//										.div(id("notebooks-clone-panel"));
								}
							};
						} else
							return null;
					}
				})
			._div();
	}
	
	private Notebook getNotebook() throws IOException {
		if (notebook == null) {
			notebook = new ApiCallCommand(this)
							.setApi("api/notebook/get")
							.addParam("notebookId", getNotebookId().getId())
							.run()
							.getResponse()
							.getAsObject(Notebook.class);
		}
		return notebook;
	}
	
	private OpList onNotebookConfirm() throws IOException, ClassNotFoundException {
		return new OpList()
			.add(new OpModalQuestion("Clone notebook", 
					"Do you want to clone all entries from the notebook <b>" + 
						getNotebook().getName() + "</b>?", 
					new OpBeansInfo("Notebok cloned")));
	}
	
	private OpList onNotebooksLoad() throws IOException, ClassNotFoundException {
		OpList onNotebookClick = new OpList();
		onNotebookClick
			.add(new OpModalQuestion("Cloning notebook", 
					"Add all entries from this notebook?", 
					new OpComponentClick((Class<? extends BeansComponent>) Class.forName(getParentClass()), 
						getParentId(),
						getParentMethodTocall(),
						new Params()
							.add("destinationNotebookId", getNotebookId().getId())
						)))
//			.add(new OpBeansInfo("Entries added successfully"))
			;
		
		return new OpList()
			.add(new OpReplace("#notebooks-clone-panel", new NotebooksTableComponent(AddNewEntryPanel.this
//					,
//					onNotebookClick
					)
				.toHtml()));
	}
		
	public OpList onSelect() {
		try {
			return new OpList()
					.add(new OpDialogClose())
					.add(new OpComponentClick((Class<? extends Component>) Class.forName(getParentClass()), 
							getParentId(), 
							getParentMethodTocall(), 
							getParams()
								.add("notebookId", getNotebookId().getId())));
		} catch (ClassNotFoundException e) {
			LibsLogger.error(AddNewEntryPanel.class, "Cannot create new entry", e);
			return null;
		}
	}

	private String getParentId() {
		return parentId;
	}

	private void setParentId(String parentId) {
		this.parentId = parentId;
	}

	private String getParentMethodTocall() {
		return parentMethodTocall;
	}

	private void setParentMethodTocall(String parentMethodTocall) {
		this.parentMethodTocall = parentMethodTocall;
	}

	private String getParentClass() {
		return parentClass;
	}

	private void setParentClass(String parentClass) {
		this.parentClass = parentClass;
	}

	private UUID getNotebookId() {
		return notebookId;
	}

	private void setNotebookId(UUID notebookId) {
		this.notebookId = notebookId;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}
	
	public boolean isRefEntryIdDefined() {
		return refEntryId != null && UUID.isValid(refEntryId.getId());
	}

	public UUID getRefEntryId() {
		return refEntryId;
	}

	public void setRefEntryId(UUID refEntryId) {
		this.refEntryId = refEntryId;
	}
}
