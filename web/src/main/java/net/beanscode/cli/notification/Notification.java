package net.beanscode.cli.notification;

import java.util.ArrayList;
import java.util.List;

import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.string.MetaList;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.Min;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class Notification {
	
	public static final String COLUMN_FAMILY = "notification";
	
	@Expose
	@NotNull
	@AssertValid
	private UUID id = null;
	
	@Expose
	@NotNull
	@AssertValid
	private UUID userId = null;
	
	@Expose
	@NotNull
	@NotEmpty
	private String title = null;
	
	@Expose
	@AssertValid
	private MetaList meta = null;
	
	@Expose
	private String description = null;
	
	@Expose
	@Min(value = 1)
	private long creationMs = 0;
	
	@Expose
	private boolean read = false;
	
	@Expose
	private boolean locked = false;
	
	@Expose
	private boolean archived = false;
	
	@Expose
	@NotNull
	@NotEmpty
	private NotificationType notificationType = NotificationType.INFO;
	
	@Expose
//	@NotNull
//	@NotEmpty
	private List<Question> questions = null;

	public Notification() {
		setId(UUID.random());
		setCreationMs(System.currentTimeMillis());
	}
		
	public boolean isQuestionDefined() {
		return questions != null && questions.size() > 0;
	}
	
	public UUID getId() {
		return id;
	}

	public Notification setId(UUID id) {
		this.id = id;
		return this;
	}

	public UUID getUserId() {
		return userId;
	}

	public Notification setUserId(UUID userId) {
		this.userId = userId;
		return this;
	}

	public String getTitle() {
		return title;
	}

	public Notification setTitle(String title) {
		this.title = title;
		return this;
	}

	public MetaList getMeta() {
		if (meta == null)
			meta = new MetaList();
		return meta;
	}

	public Notification setMeta(MetaList meta) {
		this.meta = meta;
		return this;
	}

	public String getDescription() {
		return description;
	}
	
	public String getDescriptionNotNull() {
		return description != null ? description : "";
	}

	public Notification setDescription(String description) {
		this.description = description;
		return this;
	}

	public long getCreationMs() {
		return creationMs;
	}
	
	public SimpleDate getCreationDate() {
		return new SimpleDate(creationMs);
	}

	public Notification setCreationMs(long creationMs) {
		this.creationMs = creationMs;
		return this;
	}

	public NotificationType getNotificationType() {
		return notificationType;
	}

	public Notification setNotificationType(NotificationType notificationType) {
		this.notificationType = notificationType;
		return this;
	}

	public Notification addMeta(String name, Object value) {
		getMeta().add(name, value);
		return this;
	}

	public boolean isRead() {
		return read;
	}

	public Notification setRead(boolean read) {
		this.read = read;
		return this;
	}

	public boolean isArchived() {
		return archived;
	}

	public Notification setArchived(boolean archived) {
		this.archived = archived;
		return this;
	}

	public boolean isLocked() {
		return locked;
	}

	public Notification setLocked(boolean locked) {
		this.locked = locked;
		return this;
	}

	public List<Question> getQuestions() {
		if (questions == null)
			questions = new ArrayList<Question>();
		return questions;
	}

	public Notification setQuestions(List<Question> questions) {
		this.questions = questions;
		return this;
	}
	
	public Notification addQuestion(Question q) {
		getQuestions().add(q);
		return this;
	}

	public Question getQuestion(String questionId) {
		for (Question q : getQuestions()) {
			if (q.getQuestionId().equals(questionId))
				return q;
		}
		return null;
	}

	public boolean isDone() {
		return getNotificationType() == NotificationType.OK || getNotificationType() == NotificationType.ERROR;
	}
	
}
