package net.beanscode.web.view.ui;

import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.for_;
import static org.rendersnake.HtmlAttributesFactory.id;
import static org.rendersnake.HtmlAttributesFactory.name;

import java.io.IOException;

import org.rendersnake.HtmlAttributes;
import org.rendersnake.HtmlAttributesFactory;
import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.datasets.Table;
import net.beanscode.web.BeansAPI;
import net.beanscode.web.view.plots.PlotPanel;
import net.hypki.libs5.pjf.components.ops.OpComponentTimer;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpSet;

public class DatasetTableSelector extends BeansComponent {
	
	@Expose
	private String dsQuery = null;
	
	@Expose
	private String tbQuery = null;
	
	@Expose
	private String dsQueryInputName = null;
	
	@Expose
	private String tbQueryInputName = null;

	public DatasetTableSelector() {
		
	}
	
	public DatasetTableSelector(BeansComponent parent, String initialDsQuery, String initialTbQuery,
			String dsQueryInputName, String tbQueryInputName) {
		super(parent);
		setDsQuery(initialDsQuery);
		setTbQuery(initialTbQuery);
		setDsQueryInputName(dsQueryInputName);
		setTbQueryInputName(tbQueryInputName);
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.div(id(getId())
					.class_("dataset-table-selector"))
				.div(class_("row"))
					.div(class_("col-sm-6"))
						.label(for_(getDsQueryInputName()))
							.content("Data from datasets ")
						.input(name(getDsQueryInputName())
								.type("text")
								.class_(getDsQueryInputName() + " form-control")
								.add("placeholder", "datasets filter...")
								.value(getDsQuery())
								.onKeyup(new OpComponentTimer(DatasetTableSelector.this, "onRefreshInputTables", 300).toStringOnclick()))
					._div()
					.div(class_("col-sm-6"))
						.label(for_(getTbQueryInputName()))
							.content("from Tables")
						.input(name(getTbQueryInputName())
								.type("text")
								.class_(getTbQueryInputName() + " form-control")
								.add("placeholder", "tables filter")
								.value(getTbQuery())
								.onKeyup(new OpComponentTimer(DatasetTableSelector.this, "onRefreshInputTables", 300).toStringOnclick()))
					._div()
				._div()
				.div(class_("input-tables"))
				._div()
			._div();
	}
	
	private OpList onRefreshInputTables() throws IOException {
		ApiCallCommand api = new ApiCallCommand(this)
			.setApi(BeansAPI.API_TABLE_SEARCH)
			.addParam("datasetQuery", getParams().getString(getDsQueryInputName()))
			.addParam("tableQuery", getParams().getString(getTbQueryInputName()))
			.addParam("size", "" + 5)
			.run();
		
		long maxHits = api.getResponse().getAsLong("maxHits");
		StringBuilder sb = new StringBuilder();
		sb.append("Will read from " + "<a class=\"beans-link\">" + maxHits + "</a>" + "tables: ");
		for (Table t : api.getResponse().getAsList("tables", Table.class))
			sb.append("<br/><a class=\"beans-link\">" + 
						t.getName() + " (" + t.getDataset(this.getSessionBean()).getName() + ")</a>");
		if (maxHits > 5)
			sb.append("<a class=\"beans-link\">...</a>");
		
		return new OpList()
				.add(new OpSet("#" + getId() + " .input-tables", sb.toString()));
	}

	private String getDsQuery() {
		return dsQuery != null ? dsQuery : "";
	}

	private void setDsQuery(String dsQuery) {
		this.dsQuery = dsQuery;
	}

	private String getTbQuery() {
		return tbQuery != null ? tbQuery : "";
	}

	private void setTbQuery(String tbQuery) {
		this.tbQuery = tbQuery;
	}

	private String getDsQueryInputName() {
		return dsQueryInputName;
	}

	private void setDsQueryInputName(String dsQueryInputName) {
		this.dsQueryInputName = dsQueryInputName;
	}

	private String getTbQueryInputName() {
		return tbQueryInputName;
	}

	private void setTbQueryInputName(String tbQueryInputName) {
		this.tbQueryInputName = tbQueryInputName;
	}
}
