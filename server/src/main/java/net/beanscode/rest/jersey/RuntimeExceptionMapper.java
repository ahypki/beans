package net.beanscode.rest.jersey;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import net.hypki.libs5.pjf.op.OpError;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.utils.LibsLogger;

@Provider
public class RuntimeExceptionMapper implements ExceptionMapper<RuntimeException> {

	public Response toResponse(RuntimeException exception) {
		LibsLogger.error(RuntimeExceptionMapper.class, "Runtime exception", exception);
		
		return Response
				.status(Response.Status.NOT_ACCEPTABLE)
//				.entity("{ \"message\": \"" + exception.getMessage() + "\" }")
				
//				.entity(new HttpResponse()
//							.setCode(Response.Status.NOT_ACCEPTABLE.getStatusCode())
//							.setMessage(exception.getMessage()))
				
				.entity(new OpList()
								.add(new OpError(exception.toString())).toString())
				
				.build();
	}

}
