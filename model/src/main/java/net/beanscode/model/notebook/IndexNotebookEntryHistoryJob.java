package net.beanscode.model.notebook;

import java.io.IOException;

import com.google.gson.annotations.Expose;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.weblibs.jobs.Job;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

public class IndexNotebookEntryHistoryJob extends Job {
	
	@Expose
	@NotNull
	@AssertValid
	private UUID notebookEntryHistoryId = null;

	public IndexNotebookEntryHistoryJob() {
		
	}
	
	public IndexNotebookEntryHistoryJob(UUID notebookEntryId) {
		setNotebookEntryHistoryId(notebookEntryId);
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		NotebookEntryHistory ne = NotebookEntryHistoryFactory.getNotebookEntryHistory(getNotebookEntryHistoryId());
		
		if (ne != null)
			ne.index();
	}

	public UUID getNotebookEntryHistoryId() {
		return notebookEntryHistoryId;
	}

	public void setNotebookEntryHistoryId(UUID notebookEntryHistoryId) {
		this.notebookEntryHistoryId = notebookEntryHistoryId;
	}
}
