package net.beanscode.pojo.dataset;

import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.string.MetaList;

import com.google.gson.annotations.Expose;

public class Dataset {

	@Expose
	private UUID id = null;
	
	@Expose
	private UUID userId = null;
	
	@Expose
	private String name = null;
	
	@Expose
	private MetaList meta = null;
	
	@Expose
	private String description = null;

	@Expose
	private String descriptionHtml = null;
	
	@Expose
	private long creationMs = 0;
	
	public Dataset() {
		

	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public UUID getUserId() {
		return userId;
	}
	
//	public User getUser(SessionBean sb) throws CLIException, IOException {
//		return new UserGet(sb).run(getUserId().getId(), null);
//	}

	public void setUserId(UUID userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public MetaList getMeta() {
		if (meta == null)
			meta = new MetaList();
		return meta;
	}

	public void setMeta(MetaList meta) {
		this.meta = meta;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getCreationMs() {
		return creationMs;
	}

	public void setCreationMs(long creationMs) {
		this.creationMs = creationMs;
	}

	public SimpleDate getCreationDate() {
		return new SimpleDate(getCreationMs());
	}

	public String getDescriptionHtml() {
		return descriptionHtml;
	}

	public void setDescriptionHtml(String descriptionHtml) {
		this.descriptionHtml = descriptionHtml;
	}
}
