package net.beanscode.model.plugins;

import java.lang.reflect.Type;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.Meta;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

@Deprecated
public class MetaDeser implements JsonDeserializer<Meta> {

	@Override
	public Meta deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		JsonObject jo = json.getAsJsonObject();
		Meta meta = new Meta();
		
		if (jo.has("description"))
			meta.setDescription(jo.get("description") != null && !jo.get("description").isJsonNull() ? 
					jo.get("description").getAsString() : null);
		meta.setName(jo.get("name").getAsString());
		if (jo.get("value") != null
				&& jo.get("value").getAsJsonPrimitive().isString()) {
			meta.setValue(jo.get("value").getAsString());
		} else
			meta.setValue(jo.get("value"));
		
		return meta;
	}

}
