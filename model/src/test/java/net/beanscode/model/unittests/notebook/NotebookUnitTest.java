package net.beanscode.model.unittests.notebook;

import java.io.IOException;
import java.util.List;

import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.notebook.NotebookFactory;
import net.beanscode.model.tests.BeansTestCase;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.weblibs.SearchManager;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.unittest.WeblibsTestCase;
import net.hypki.libs5.weblibs.user.User;

import org.junit.Test;

public class NotebookUnitTest extends BeansTestCase {
	

	@Test
	public void testCreate() throws ValidationException, IOException {
		SearchManager.searchInstance().clearIndex(WeblibsConst.KEYSPACE_LOWERCASE);
		
		
		final User user = testUser();
		final String name = "notebook test name";
		
		Notebook n = new Notebook(user.getUserId(), name);
		n.save();
		
		runAllJobs(1500);
		
		addToRemove(n);
		
		Notebook nFromDb = Notebook.getNotebook(n.getId());
		assertTrue(n.getName().equals(nFromDb.getName()));
		
		List<Notebook> notebooks = NotebookFactory.searchNotebooks(user.getUserId(), "*", 0, 20).getObjects();
		assertTrue(notebooks.size() == 1, "Found " + notebooks.size() + " notebooks");
	}
	
	@Test
	public void testSearchByTag() throws ValidationException, IOException {
		SearchManager.searchInstance().clearIndex(WeblibsConst.KEYSPACE_LOWERCASE);
		
		
		final User user = testUser();
		final String name = "notebook test name";
		
		Notebook n = new Notebook(user.getUserId(), name);
		n.getTags().addTags("seven eight nine");
//		n.setTagsjoined(",seven,eight,nine,");
		n.save();
		
		runAllJobs(2500);
		
		addToRemove(n);
		
		Notebook nFromDb = Notebook.getNotebook(n.getId());
		assertTrue(n.getName().equals(nFromDb.getName()));
		
		List<Notebook> notebooks = NotebookFactory.searchNotebooks(user.getUserId(), "+eight", 0, 20).getObjects();
		assertTrue(notebooks.size() == 1, "Found " + notebooks.size() + " notebooks");
	}
}