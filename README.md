BEANS is a web-based software for interactive distributed data analysis with a clear interface for querying, filtering, aggregating, and plotting data from an arbitrary number of datasets and tables.

## Table of contents

[[_TOC_]]

## Quick start

If you trust me ([http://hypki.net](http://hypki.net)) just download `BEANS` release file:

	wget https://hypki.net/content/images/beans-bulk-1.0.0.jar

and start it:

	java -jar beans-bulk-1.0.0.jar
