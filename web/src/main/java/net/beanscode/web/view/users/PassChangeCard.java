package net.beanscode.web.view.users;

import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;
import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.name;

import java.io.IOException;

import javax.ws.rs.core.SecurityContext;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.BetaButtonGroup;
import net.beanscode.web.view.ui.CardPanel;
import net.beanscode.web.view.ui.modals.OpBeansError;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.utils.url.Params;

import org.rendersnake.HtmlCanvas;

public class PassChangeCard extends CardPanel {

	public PassChangeCard() {
		
	}
	
	public PassChangeCard(BeansComponent parent) {
		super(parent);
	}

	@Override
	protected void renderTitle(HtmlCanvas html) throws IOException {
		html
			.span()
				.content("Changing password");
	}

	@Override
	protected void renderContent(HtmlCanvas html) throws IOException {
		html
//			.div(id(getId()).class_("options-page"))
//				.h3()
//					.content("Password change")
//				.div()
	//				.form(id("form-new-pass"))
						.dl(class_("row"))
							.dt(class_("col-sm-4"))
								.content("Current password")
							.dd(class_("col-sm-8"))
								.input(name("oldPass")
										.type("password"))
							._dd()
							.dt(class_("col-sm-4"))
								.content("New password")
							.dd(class_("col-sm-8"))
								.input(name("pass1")
									.type("password"))
							._dd()
							.dt(class_("col-sm-4"))
								.content("New password (again)")
							.dd(class_("col-sm-8"))
								.input(name("pass2")
									.type("password"))
							._dd()
						._dl()
						;
	//				._form()
//				._div()
//			._div()
//			.toHtml()
//			;
	}
	
	public OpList onUpdate(SecurityContext sc, Params params) throws IOException, ValidationException {
		final String oldPass = params.getString("oldPass");
		final String pass1 = params.getString("pass1");
		final String pass2 = params.getString("pass2");
		
		if (nullOrEmpty(oldPass)) {
			return new OpList()
				.add(new OpBeansError("Old password is empty"));
		}
		if (nullOrEmpty(pass1)) {
			return new OpList()
				.add(new OpBeansError("New password is empty"));
		}
		if (nullOrEmpty(pass2)) {
			return new OpList()
				.add(new OpBeansError("New password (again) is empty"));
		}
		
		new ApiCallCommand(this)
			.setApi("api/user/update")
			.addParam("user-email", getUserEmail())
			.addParam("old-pass", oldPass)
			.addParam("new-pass1", pass1)
			.addParam("new-pass2", pass2)
			.run();
		
		return new OpList()
//			.add(new OpDialogClose())
			.add(new OpBeansInfo("Password saved"));
	}

	@Override
	protected void renderFooter(HtmlCanvas html) throws IOException {
		html
			.render(new BetaButtonGroup()
				.add(new Button("Save password")
						.add(new OpComponentClick(this, "onUpdate"))));
	}
}
