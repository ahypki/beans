package net.beanscode.cli.notebooks;

import net.beanscode.pojo.NotebookEntry;

import com.google.gson.annotations.Expose;

public class TableEntry extends NotebookEntry {

	@Expose
	private String query = null;
	
	public TableEntry() {
		
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}
}
