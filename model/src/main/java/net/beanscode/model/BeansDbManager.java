package net.beanscode.model;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;

import java.io.IOException;

import net.hypki.libs5.db.cassandra.CassandraProvider;
import net.hypki.libs5.db.db.DatabaseProvider;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.lucene.LuceneDatabaseProvider;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.reflection.ReflectionUtility;
import net.hypki.libs5.utils.string.MetaList;
import net.hypki.libs5.utils.string.StringUtilities;

import com.google.gson.JsonElement;

public class BeansDbManager {
	
	private static DatabaseProvider mainDatabaseProvider = null;
	private static DatabaseProvider tableDataDatabaseProvider = null;
	
	public static boolean isCassandraProvider() {
		return DbObject.getDatabaseProvider() instanceof CassandraProvider;
	}

	public static CassandraProvider getCassandraProvider() {
		return (CassandraProvider) DbObject.getDatabaseProvider();
	}
	
	public static DatabaseProvider getMainDatabaseProvider() {
		if (mainDatabaseProvider == null) {
			try {
				// searching for database class
				// searching for database init params
				String dbProvider 		= LocalSettings.getSettingLocalAsString("MainDatabaseProvider/class");
				JsonElement initParams 	= LocalSettings.getSettingLocalAsJson("MainDatabaseProvider/initParams");
				MetaList initMeta = new MetaList();

				if (nullOrEmpty(dbProvider))
					dbProvider = "Lucene";
				
				if (initParams != null)
					initMeta = JsonUtils.fromJson(initParams, MetaList.class);
					
				Class mainDatabaseClass = ReflectionUtility.findClass(dbProvider);
				
				if (mainDatabaseClass == null && dbProvider.toLowerCase().contains("Cassandra".toLowerCase()))
					mainDatabaseClass = CassandraProvider.class;
				else if (mainDatabaseClass == null && dbProvider.toLowerCase().contains("Lucene".toLowerCase())) {
					mainDatabaseClass = LuceneDatabaseProvider.class;
					
					initMeta.add(LuceneDatabaseProvider.INIT_DIRECTORY, 
							new FileExt(BeansSettings.getBeansHomeFolder(), "db").getAbsolutePath());
				} else if (mainDatabaseClass == null) {
					// default DB provider: Lucene
					mainDatabaseClass = LuceneDatabaseProvider.class;
					
					initMeta.add(LuceneDatabaseProvider.INIT_DIRECTORY, 
							new FileExt(BeansSettings.getBeansHomeFolder(), "db").getAbsolutePath());
				}
				
				LibsLogger.debug(BeansDbManager.class, "Main database selected ", mainDatabaseClass);
				
				if (mainDatabaseClass != null) {
					// creating main database instance
					mainDatabaseProvider = (DatabaseProvider) mainDatabaseClass.newInstance();
					
					mainDatabaseProvider.init(initMeta);
				}
				
				saveMainDatabaseProvider();
				
				LibsLogger.debug(BeansDbManager.class, "MainDatabaseProvider ", mainDatabaseProvider.getClass().getSimpleName());
			} catch (InstantiationException | IllegalAccessException e) {
				LibsLogger.error(BeansDbManager.class, "Cannot create DatabaseProvider instance", e);
			} catch (IOException e) {
				LibsLogger.error(BeansDbManager.class, "Cannot save MainDatabaseProvider to local settings", e);
			}
		}
		return mainDatabaseProvider;
	}
	
	public static DatabaseProvider getTableDataDatabaseProvider() {
		if (tableDataDatabaseProvider == null) {
			try {
				String tabProvider = LocalSettings.getSettingLocalAsString("TableDataDatabaseProvider");
				LibsLogger.debug(BeansDbManager.class, "TableDataDatabaseProvider ", tabProvider);
//			mainDatabaseProvider = new CassandraProvider(); 
//			mainDatabaseProvider = new MapDbManager();
				if (notEmpty(tabProvider))
					tableDataDatabaseProvider = (DatabaseProvider) Class.forName(tabProvider).newInstance();
				else
					tableDataDatabaseProvider = new LuceneDatabaseProvider();
			} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
				LibsLogger.error(BeansDbManager.class, "Cannot create DatabaseProvider instance", e);
			}
		}
//			 = new MapDbManager();
//			tableDataDatabaseProvider = new CassandraProvider();
		return tableDataDatabaseProvider;
	}
	
	private static void saveMainDatabaseProvider() throws IOException {
		LocalSettings.saveSettingLocal("MainDatabaseProvider/class", mainDatabaseProvider.getClass().getName());
		LocalSettings.saveSettingLocal("MainDatabaseProvider/initParams", JsonUtils.toJson(mainDatabaseProvider.getInitParams()));
	}

	public static void setMainDatabaseProvider(String clazz) {
		try {
			if (mainDatabaseProvider != null)
				mainDatabaseProvider.close();
			
			Class dbClass = Class.forName(clazz);
			
			mainDatabaseProvider = (DatabaseProvider) dbClass.newInstance();
			
			saveMainDatabaseProvider();
			
			BeansConst.initDb();
		} catch (Throwable e) {
			LibsLogger.error(BeansDbManager.class, "Cannot find class " + clazz + " to set it as a "
					+ "main database engine", e);
		}
	}
	
	public static void setTableDataDatabaseProvider(String clazz) {
		try {
			Class tableDbClass = Class.forName(clazz);
			
			tableDataDatabaseProvider = (DatabaseProvider) tableDbClass.newInstance();
			
			LocalSettings.saveSettingLocal("TableDataDatabaseProvider", mainDatabaseProvider.getClass().getName());
		} catch (Throwable e) {
			LibsLogger.error(BeansDbManager.class, "Cannot find class " + clazz + " to set it as a "
					+ "table data database engine", e);
		}
	}
}
