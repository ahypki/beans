package net.beanscode.web.view.settings.jobspanel;

import static org.rendersnake.HtmlAttributesFactory.id;

import java.io.IOException;

import net.beanscode.web.view.ui.BeansComponent;
import net.hypki.libs5.pjf.op.OpList;

import org.rendersnake.HtmlCanvas;

public class JobsWorkersPanel extends BeansComponent {
	
	public JobsWorkersPanel() {
		
	}

	public JobsWorkersPanel(BeansComponent parent) {
		super(parent);
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.div(id(getId()))
				.h3()
					.content("Running jobs workers")
				.render(new RunningJobWorkersPanel(this))
//				.div(class_("jobs-table"))
//					.content("Refreshing list of workers...")
//				._div()
			._div()
//			.write(new OpComponentClick(this, "onRefresh").toStringAdhock(), false)
			
			.div()
				.h3()
					.content("Jobs Workers channels")
				.render(new JobsChannelsTable(this))
			._div()
			;
	}
	
	@Override
	public OpList getOpList() {
		return super.getOpList();
	}
	
	
	
	
}
