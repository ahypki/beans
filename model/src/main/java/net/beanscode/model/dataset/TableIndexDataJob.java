package net.beanscode.model.dataset;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import net.beanscode.model.BeansChannels;
import net.beanscode.model.cass.Data;
import net.beanscode.model.plugins.Connector;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.Watch;
import net.hypki.libs5.weblibs.SearchManager;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.jobs.Job;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class TableIndexDataJob extends Job {
	
	@Expose
	@NotNull
	@AssertValid
	private UUID tableId = null;
	
	@Expose
	@NotNull
	private boolean replace = false;

	public TableIndexDataJob() {
		super(BeansChannels.CHANNEL_LONG_HIDDEN);
	}
	
	public TableIndexDataJob(UUID tableId) {
		super(BeansChannels.CHANNEL_LONG_HIDDEN);
		setTableId(tableId);
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		Table table = TableFactory.getTable(getTableId());
		
		if (table == null) {
			LibsLogger.warn(TableIndexDataJob.class, "Cannot find table in DB, skipping job...");
			return;
		}
		
//		if (table.getSearchStatus() != null && table.getSearchStatus() == TableSearchStatus.INDEXED) {
//			if (isReplace() == false) {
//				LibsLogger.warn(TableIndexDataJob.class, "Table ", table.getId(), " already indexed, skipping job...");
//				return;
//			}
//		}
		
		table.indexData(false);
	}

	public UUID getTableId() {
		return tableId;
	}

	public void setTableId(UUID tableId) {
		this.tableId = tableId;
	}

	public boolean isReplace() {
		return replace;
	}

	public TableIndexDataJob setReplace(boolean replace) {
		this.replace = replace;
		return this;
	}
}
