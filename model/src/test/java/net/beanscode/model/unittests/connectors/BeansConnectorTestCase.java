package net.beanscode.model.unittests.connectors;

import java.io.IOException;
import java.util.List;

import net.beanscode.model.connectors.BeansConnector;
import net.beanscode.model.connectors.BeansConnectorFactory;
import net.beanscode.model.connectors.BeansConnectorStatus;
import net.beanscode.model.connectors.H5Connector;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.weblibs.WeblibsConst;

import org.junit.Test;

public class BeansConnectorTestCase extends ConnectorTestCase {

	@Test
	public void testCreateGet() throws IOException, ValidationException {
		final UUID tableId = UUID.random();
		final FileExt one = new FileExt("one.h5");
		final FileExt two = new FileExt("two.h5");
		
		DbObject.getDatabaseProvider().removeTable(WeblibsConst.KEYSPACE_LOWERCASE, BeansConnector.COLUMN_FAMILY);
		DbObject.getDatabaseProvider().createTable(WeblibsConst.KEYSPACE_LOWERCASE, new BeansConnector().getTableSchema());
		
		one.deleteIfExists();
		two.deleteIfExists();
		
		H5Connector connOne = new H5Connector(tableId, one);
		writeRows(connOne, 20);
		
		H5Connector connTwo = new H5Connector(tableId, two);
		writeRows(connTwo, 20);
		
		// saving one BEANS connector to DB
		BeansConnector beansConnOne = new BeansConnector()
			.setTableId(tableId)
			.setId("one")
			.setConnector(connOne)
			.setStatus(BeansConnectorStatus.COMMITTED)
			.save();
		BeansConnector beansConnTwo = new BeansConnector()
			.setTableId(tableId)
			.setId("two")
			.setConnector(connTwo)
			.setStatus(BeansConnectorStatus.COMMITTED)
			.save();
		
		// getting one BEANS connector from DB
		BeansConnector beansConnFromDb = BeansConnectorFactory.getBeansConnector(tableId, "one");
		assertTrue(beansConnFromDb != null, "BEANS connector not found");
		assertTrue(beansConnFromDb.getKey().equals(beansConnOne.getKey()), "BEANS connector keys do not match");
		
		beansConnFromDb = BeansConnectorFactory.getBeansConnector(tableId, "two");
		
		assertTrue(beansConnFromDb != null, "BEANS connector not found");
		assertTrue(beansConnFromDb.getKey().equals(beansConnTwo.getKey()), "BEANS connector keys do not match");
		
		for (Row row : beansConnFromDb) {
			LibsLogger.info("row: " + row); 
		}
		
		int rowsCount = 0;
		for (BeansConnector connFromDb : BeansConnectorFactory.iterateConnectors(tableId)) {
			for (Row row : connFromDb) {				
				rowsCount++;
			}
		}
		
		assertTrue(rowsCount == 40, "Rows count " + rowsCount + " do not match to 20");
	}
}
