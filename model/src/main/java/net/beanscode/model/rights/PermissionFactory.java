package net.beanscode.model.rights;

import java.io.IOException;

import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.notebook.Notebook;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.weblibs.WeblibsConst;

public class PermissionFactory {
	public static Permission getPermission(String clazz, String permissionId) throws IOException {
		return PermissionFactory.getPermission(clazz, new UUID(permissionId));
	}

	public static Permission getPermission(String clazz, UUID permissionId) throws IOException {
		if (clazz.equals("Dataset"))
			clazz = Dataset.class.getName();
		else if (clazz.equals("Table"))
			clazz = Table.class.getName();
		else if (clazz.equals("Notebook"))
			clazz = Notebook.class.getName();
		
		return DbObject.getDatabaseProvider().get(WeblibsConst.KEYSPACE, 
				Permission.COLUMN_FAMILY, 
				new C3Where()
						.addClause(new C3Clause(DbObject.COLUMN_PK, ClauseType.EQ, clazz + permissionId.getId())), 
				DbObject.COLUMN_DATA, 
				Permission.class);
	}
}
