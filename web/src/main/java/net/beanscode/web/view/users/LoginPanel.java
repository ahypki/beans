package net.beanscode.web.view.users;

import java.io.IOException;

import net.beanscode.cli.BeansCLISettings;
import net.beanscode.cli.users.UserLogin;
import net.beanscode.web.BeansWebConst;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.modals.OpBeansError;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.components.ops.OpComponentTimer;
import net.hypki.libs5.pjf.op.OpCookieCreate;
import net.hypki.libs5.pjf.op.OpJs;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpRedirect;
import net.hypki.libs5.utils.LibsLogger;

import org.rendersnake.HtmlCanvas;

// TODO remove
public class LoginPanel extends BeansComponent {
	
	public LoginPanel() {
		
	}

	public LoginPanel(BeansComponent parent) {
		super(parent);
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		
	}
	
	public OpList login() throws CLIException, IOException {
		try {
			final String email = getParams().getString("email");
			
			UserLogin u = (UserLogin) new UserLogin(getSessionBean()).run(email, getParams().getString("pass"));
			
			LibsLogger.debug(LoginPanel.class, "Login cookie ", u.getResponse().getBody());
			
//			BeansCLISettings.set("userEmail", email);
			
			
			if (u.getResponse().isOK()
					&& (u.getResponse().getAsLong("code") == 200 || u.getResponse().getAsLong("code") == 0)
					)
				return new OpList()
					.add(new OpCookieCreate(BeansWebConst.COOKIE_LOGIN, 
							u.getResponse().getAsString(BeansWebConst.COOKIE_LOGIN), 
							BeansWebConst.COOKIE_VALID_DAYS_DEFAULT))
					.add(new OpCookieCreate(BeansWebConst.COOKIE_USERID, 
							u.getResponse().getAsString(BeansWebConst.COOKIE_USERID), 
							BeansWebConst.COOKIE_VALID_DAYS_DEFAULT))
//					.add(new OpRedirect("/", (Object[]) null))
					.add(new OpRedirect("/"))
//					.add(new OpJs("window.location = '/';", 1000))
//					.add(new OpComponentTimer(this, "onRedirect", 1000))
//					.add(new OpRedirect("http://localhost:25001/"))
//					.add(new OpRedirect("http://localhost:25001/", 2000))
//					.add(new OpJs("window.location = 'http://localhost:25001/';", 1000))
//					.add(new OpJs("window.location = '/';", 5000))
					;
			else
				return new OpList()
					.add(new OpBeansError("Cannot login user"));
		} catch (Exception e) {
			LibsLogger.error(LoginPanel.class, "Cannot login user", e);
			return new OpList()
				.add(new OpBeansError(e.getMessage()));
		}
	}
	
	public OpList onRedirect() {
		return new OpList()
//			.add(new OpRedirect("/", (Object[]) null))
			.add(new OpJs("window.location = '/';"))
			;
	}
}
