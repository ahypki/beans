package net.beanscode.model.init;

import java.io.IOException;

import net.beanscode.model.BeansSettings;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.jobs.Job;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class ReloadSettingsJob extends Job {
	
	@Expose
	@NotNull
	@AssertValid
	private boolean force = false;

	public ReloadSettingsJob() {
		
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		try {
			BeansSettings.reloadSettings(isForce());
		} catch (Throwable t) {
			LibsLogger.error(ReloadSettingsJob.class, "Cannot reload settings in Job class", t);
		}
	}

	public boolean isForce() {
		return force;
	}

	public void setForce(boolean force) {
		this.force = force;
	}
}
