package net.beanscode.cli.jobs;

import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.date.SimpleDate;

import com.google.gson.annotations.Expose;

public class Job {

	@Expose
	private String channel = null;
	
	@Expose
	private long time = 0;
	
	@Expose
	private UUID id = null;
	
	@Expose
	private String name = null;
	
	@Expose
	private String description = null;
	
	public Job() {
		
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public SimpleDate getCreationDate() {
		return new SimpleDate(getTime());
	}
	
	public String getDescriptionNotNull() {
		return description != null ? description : "";
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
