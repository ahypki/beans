package net.beanscode.model.plugins;

import java.io.FileWriter;
import java.io.IOException;

import com.google.gson.annotations.Expose;

import net.beanscode.model.BeansSettings;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.file.FileUtils;
import net.hypki.libs5.weblibs.jobs.Job;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

public class SelfTestJob extends Job {
	
	@Expose
	@NotNull
	@NotEmpty
	private UUID userId = null;
	
	@Expose
	@NotNull
	@NotEmpty
	private String pluginName = null;

	public SelfTestJob() {
		
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		LibsLogger.debug(SelfTestJob.class, "Starting self tests for plugins");
		
		try {
			PluginManager.selfTestPlugin(userId, pluginName);
		} catch (Throwable e) {
			LibsLogger.error(SelfTestJob.class, "Self test job failed, consuming job...", e);
		}
		
		LibsLogger.debug(SelfTestJob.class, "Finished self tests for plugins");
	}

	public UUID getUserId() {
		return userId;
	}

	public SelfTestJob setUserId(UUID userId) {
		this.userId = userId;
		return this;
	}

	public String getPluginName() {
		return pluginName;
	}

	public SelfTestJob setPluginName(String pluginName) {
		this.pluginName = pluginName;
		return this;
	}
}
