package net.beanscode.rest.user;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.apache.hadoop.security.Groups;

import mjson.Json;
import net.beanscode.model.BeansSearchManager;
import net.beanscode.model.rights.Group;
import net.beanscode.model.rights.GroupFactory;
import net.beanscode.rest.BeansRest;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.search.SearchResults;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.url.HttpResponse;

@Path("api/group")
public class GroupRest extends BeansRest {
	
	@DELETE
    @Path("delete")
	public void delete(@Context SecurityContext sc,
			@QueryParam("groupId") String groupId) throws IOException, ValidationException {
		Group group = GroupFactory.getGroup(groupId);
		
		validateNotFound(group != null, "Group not found");
		validateUnauthorized(isAdmin(sc) || group.getOwner().equals(getUserId(sc)), "Permission denied");
		
		group.remove();
	}
	
	@GET
    @Path("get")
    @Produces(MediaType.APPLICATION_JSON)
	public Response get(@Context SecurityContext sc,
			@QueryParam("groupId") String groupId) throws IOException, ValidationException {
		Group group = GroupFactory.getGroup(groupId);
		
		validateNotFound(group != null, "Group not found");
		validateUnauthorized(isAdmin(sc) || group.getOwner().equals(getUserId(sc)), "Permission denied");
		
		return Response
				.ok()
				.entity(JsonUtils.objectToString(group))
				.build();
	}
	
	@POST
    @Path("set")
	public void set(@Context SecurityContext sc,
			@FormParam("id") UUID groupId,
			@FormParam("name") String groupName,
			@FormParam("description") String groupDesc,
			@FormParam("userIds") List<UUID> groupUserIds) throws IOException, ValidationException {
		Group group = GroupFactory.getGroup(groupId);
		
		if (group == null) {
			group = new Group();
			if (groupId != null && groupId.isValid())
				group.setId(groupId);
			group.setOwner(getUserId(sc));
		}
		
		validateNotFound(group != null, "Group not found");
		validateUnauthorized(isAdmin(sc) || group.getOwner().equals(getUserId(sc)), "Permission denied");
		
		if (notEmpty(groupName))
			group.setName(groupName);
		
		if (notEmpty(groupDesc))
			group.setDescription(groupDesc);

		group.clearUsers();
		if (groupUserIds != null && groupUserIds.size() > 0) {
			group.addUser(groupUserIds);
		}
		group.save();
		
		LibsLogger.debug(GroupRest.class, "Group ", group.getName(), " saved successfully");
	}

	@GET
	@Path("query")
	@Produces(MediaType.APPLICATION_JSON)
	public Response search(@Context SecurityContext sc, 
			@QueryParam("query") String query,
			@QueryParam("from") int from,
			@QueryParam("size") int size) throws IOException, ValidationException {
		
		try {
			query = StringUtilities.nullOrEmpty(query) ? null : query.trim();
			
			SearchResults<Group> groups = BeansSearchManager.searchGroups(getUserId(sc), query, from, size);
					
			return Response
					.ok()
					.entity(Json
							.object()
							.set("groups", groups.getObjectsAsJson())
							.set("maxHits", groups.maxHits())
							.toString())
					.build();
		} catch (Exception e) {
			LibsLogger.error(GroupRest.class, "Cannot search for groups", e);
			return Response
				.ok()
				.entity(Json
						.object()
						.set("groups", "")
						.set("maxHits", 0)
						.toString())
				.build();
		}
	}
}
