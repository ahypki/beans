package net.beanscode.model.cass.notification;

public enum NotificationType {
	INFO, 
	QUESTION,
	OK,
	WARN,
	ERROR
}
