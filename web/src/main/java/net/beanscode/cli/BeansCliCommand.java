package net.beanscode.cli;

import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.users.User;
import net.beanscode.web.BeansWebConst;
import net.hypki.libs5.cli.noninteractivecli.CLICommand;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.cli.noninteractivecli.HttpCLICommand;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.RestMethod;
import net.hypki.libs5.pjf.components.jersey.SessionBean;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.collections.ArrayUtils;
import net.hypki.libs5.utils.collections.CollectionUtils;
import net.hypki.libs5.utils.json.JsonUtils;

import com.google.gson.JsonElement;

public abstract class BeansCliCommand<T> extends HttpCLICommand {
	
	public static final String API_AUTOUPDATE_START 		= "api/autoupdate/start";
	public static final String API_AUTOUPDATE_SETTING 		= "api/autoupdate/setting";
	public static final String API_AUTOUPDATE_ENTRY_DEFUALT = "api/autoupdate/entry/default";
	
	public static final String API_NOTEBOOK_META_SET 			= "api/notebook/meta/set";
	public static final String API_NOTEBOOK_QUERY 				= "api/notebook/query";
	public static final String API_NOTEBOOK_SHARE_MAGICKEY		= "api/notebook/share/magickey";
	public static final String API_NOTEBOOK_ENTRY 				= "api/notebook/entry";
	public static final String API_NOTEBOOK_ENTRY_META_ADD		= "api/notebook/entry/meta/add";
	
	public static final String API_NOTEBOOKEBTRY_START			= "api/notebook/entry/start";
	public static final String API_NOTEBOOKENTRY_QUERY			= "api/notebook/entry/query";
	public static final String API_NOTEBOOKENTRY_UPLOAD			= "api/notebook/entry/upload";
	
	public static final String API_NOTIFICATION_GET 			= "api/notification/get";
	public static final String API_NOTIFICATION_ANSWER 			= "api/notification/answer";
	public static final String API_NOTIFICATION_READ 			= "api/notification/read";
	public static final String API_NOTIFICATION_ARCHIVE			= "api/notification/archive";
	
	public static final String API_DATASET_CLEAR 				= "api/dataset/clear";
	
	public static final String API_TABLE_DATA_ROWS 				= "api/table/data/rows";
	public static final String API_TABLE_DATA_SEARCH			= "api/table/data/search";
	
	public static final String API_SETTING_CONNECTOR_DEFAULT 	= "api/setting/connector/default";
	public static final String API_SETTING_CONNECTOR_LIST 		= "api/setting/connector/list";
	public static final String API_SETTING_MAIL_TEST 			= "api/setting/mail/test";
	public static final String API_SETTING_GETSETTING 			= "api/setting/getSetting";
	
	public static final String API_BULKCHANGE 					= "api/bulkchange";
	
	public static final String API_PLOT_DATA 					= "api/plot/data";
	public static final String API_PLOT_DATA_PARALLEL			= "api/notebook/entry/data";//"api/plot/data/parallel";
	public static final String API_PLOT_DATA_HISTOGRAM			= "api/notebook/entry/data";
	public static final String API_PLOT_IMAGE					= "api/notebook/entry/image";
	
//	public static final String API_TEXT_SAVE					= "api/text/save";
	
//	public static final String API_PIG_SAVE						= "api/query/edit";
//	public static final String API_PIG_RUN						= "api/query/run";
	
	public static final String API_FILE_UPLOAD					= "api/file/upload";
	
	public static final String API_PERMISSION_SET				= "api/permission/set";
	public static final String API_PERMISSION_GET				= "api/permission/get";
	
	public static final String API_PLUGIN_GET					= "api/plugin/get";
	public static final String API_PLUGIN_DISABLE				= "api/plugin/disable";
	public static final String API_PLUGIN_ENABLE				= "api/plugin/enable";
	public static final String API_PLUGIN_IS_ENABLED			= "api/plugin/isEnabled";
	public static final String API_PLUGIN_SELFTEST_START		= "api/plugin/selftest/start";
	public static final String API_PLUGIN_SELFTEST_GET			= "api/plugin/selftest";
	
	private String userLC = null;
	
	private UUID userId = null;
	
	private User user = null;
	
	private SessionBean sessionBean = null;
	
	private Object body = null;
	
	private boolean loginRequired = true;
	
	private static String host = "http://localhost";
	
	private static int port = 25000;
	
	public BeansCliCommand(final RestMethod RESTmethodName, String userLC, UUID userId) {
		super(RESTmethodName.toString());
		
		setUserLC(userLC);
		setUserId(userId);
	}
	
	public BeansCliCommand(final RestMethod RESTmethodName, SessionBean sessionBean) {
		super(RESTmethodName != null ? RESTmethodName.toString() : null);
		
		if (sessionBean != null) {
			setSessionBean(sessionBean);
			setUserLC(sessionBean.getSessionId());
			setUserId(sessionBean.getUserId());
		}
	}
	
	@Override
	public String getLoginCookieName() {
		return BeansWebConst.COOKIE_LOGIN;
	}

	@Override
	public String getBody() {
		if (body == null)
			return null;
		
		return body instanceof String ? (String) body : JsonUtils.objectToString(body);
	}
	
	public <T extends BeansCliCommand> T run() throws CLIException, IOException {
		return (T) super.run(getArgs());
	}

	public BeansCliCommand setBody(Object body) {
		this.body = body;
		return this;
	}
	
	public BeansCliCommand<T> setLoginRequired(boolean loginRequired) {
		this.loginRequired = loginRequired;
		return this;
	}
			
	@Override
	public boolean isLoginRequired() {
		return loginRequired;
	}
	
	public static void setPort(int port) {
		BeansCliCommand.port = port;
	}
	
	@Override
	public String getHost() {
		return host;
	}
	
	@Override
	public int getPort() {
		return port;
	}
	
	@Override
	protected CLICommand doCommand() throws CLIException, IOException {
		super.doCommand();
		
		if (getResponse() != null && getResponse().isFailed())
			throw new net.hypki.libs5.utils.utils.ValidationException(getResponse().getMessage());
		
		return this;
	}
	
	@Override
	public CLICommand run(String[] args) throws CLIException, IOException {
		LibsLogger.debug(BeansCliCommand.class, "Host ", getHost(),
				" port ", getPort(),
				" userLC ", getUserLC(),
				" userId ", getUserId(),
				" loginRequired ", isLoginRequired(),
				" args ", CollectionUtils.toString(args, " "));
		return super.run(args);
	}
	
	@Override
	public void logout() {
		try {
			setUserLC(null);
			setUserId(null);
			
			BeansCLISettings.set("userLC", "");
			BeansCLISettings.set("userId", "");
			BeansCLISettings.save();
		} catch (IOException e) {
			LibsLogger.error(BeansCliCommand.class, "Cannot log out the user", e);
		}
	}
	
//	public void login(final String beansUsername, final String beansPass) {
//		try {
//			Params params = new Params()
//					.add("email", beansUsername)
//					.add("pass", beansPass);
//			
//			HttpResponse resp = UrlUtilities.post(getHost() + ":" + getPort() + "/api/user/login", null, params);
//
//			setUserLC(resp.getAsString("userLC"));
//			setUserId(new UUID(resp.getAsString("userId")));
//		} catch (Exception e) {
//			LibsLogger.error(BeansCLICommand.class, "Cannot login user " + beansUsername, e);
//		}
//	}
	
	public void setUserId(UUID userId) {
		this.userId = userId;
	}
	
	public BeansCliCommand<T> readCredentialsFromSettins() {
		String tmp = BeansCLISettings.get("userId", null);
		userId = tmp != null ? new UUID(tmp) : null;
		
		userLC = BeansCLISettings.get("userLC", null);
		
		return this;
	}

	protected UUID getUserId() {
		return userId;
	}
	
	@Override
	public String getUserLC() {
		return userLC;
	}

	public void setUserLC(String userLC) {
		this.userLC = userLC;
	}
	
	protected User getUser() throws CLIException, IOException {
		if (user == null) {
			user = new ApiCallCommand(getSessionBean())
						.setApi("api/user/get")
						.addParam("userId", getUserId().getId())
						.run()
						.getResponse()
						.getAsObject(User.class);
		}
		return user;
	}

//	protected String[] paramsJoin(String[] input, String ... toJoin) {
//		ArrayList<String> params = new ArrayList<String>();
//		
//		if (input != null)
//			for (String i : input)
//				if (i != null)
//					params.add(i);
//		
//		for (String toJoinOne : toJoin) {
//			params.add(toJoinOne);
//		}
//		
//		return ArrayUtils.toArray(params);
//	}
//	
//	protected String[] paramsJoin(String[] input, String toJoin) {
//		if (input == null)
//			input = new String[] {};
//		return (String[]) ArrayUtils.concat(input, new String[]{toJoin});
//	}
//	
//	protected String[] paramsJoin(String[] input, List toJoin, String prefix) {
//		ArrayList<String> params = new ArrayList<String>();
//		
//		if (input != null)
//			for (String i : input)
//				if (i != null)
//					params.add(i);
//			
//		if (toJoin != null)
//			for (Object toJoinOne : toJoin) {
//				params.add(prefix);
//				params.add(toJoinOne.toString());
//			}
//		
//		return ArrayUtils.toArray(params);
//	}
	
	public <T> List<T> toList(JsonElement json, Class<T> type) {
		List<T> list = new ArrayList<T>();
		for (JsonElement je : getResponse().getAsJson().getAsJsonArray()) {
			list.add(JsonUtils.fromJson(je, type));
		}
		return list;
	}

	protected SessionBean getSessionBean() {
		return sessionBean;
	}

	protected void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}
	
	protected UUID getUserId(String emailOrId) {
		if (nullOrEmpty(emailOrId))
			return null;
		
		if (emailOrId.contains("@")) {
			try {
				return getUser().getId();
			} catch (CLIException | IOException e) {
				LibsLogger.error(BeansCliCommand.class, "Cannot get User from DB", e);
				return null;
			}
		}
		
		return new UUID(emailOrId);
	}
}
