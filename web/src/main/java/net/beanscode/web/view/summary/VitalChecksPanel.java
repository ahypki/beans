package net.beanscode.web.view.summary;

import static org.rendersnake.HtmlAttributesFactory.class_;

import java.io.IOException;

import net.beanscode.web.view.ui.BeansComponent;

import org.rendersnake.HtmlCanvas;

public class VitalChecksPanel extends BeansComponent {
	
	public VitalChecksPanel() {
		
	}

	public VitalChecksPanel(BeansComponent parent) {
		super(parent);
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		
		html
			.div(class_("row"))
				.div(class_("col-sm-6"))
					.render(new ServerStatusCardPanel(this)
							.setWidth(12))
				._div()
				
				.div(class_("col-sm-6"))
					.render(new EmailSummaryCardPanel(this)
							.setWidth(12))
				._div()
			._div()
			
			
			.div(class_("row"))
				.div(class_("col-sm-6"))
					.render(new DatabaseSummaryCardPanel(this)
							.setWidth(12))
				._div()
				
				.div(class_("col-sm-6"))
				._div()
			._div()
			;
	}
	
	
	
	
}
