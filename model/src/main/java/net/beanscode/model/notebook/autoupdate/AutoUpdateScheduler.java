package net.beanscode.model.notebook.autoupdate;

import java.io.IOException;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.google.gson.annotations.Expose;

import net.beanscode.model.utils.JvmUtils;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

public class AutoUpdateScheduler extends net.hypki.libs5.weblibs.jobs.Job implements Job {
	@Expose
	@NotNull
	@AssertValid
	private UUID jvm = JvmUtils.getThisJvmUUID();

	public AutoUpdateScheduler() {
		
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		
	}
	
	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		if (jvm == null 
				|| JvmUtils.isThisJvm(jvm) == false) {
			LibsLogger.debug(AutoUpdateScheduler.class, "This ", getClass().getSimpleName(), " comes from previous"
					+ " JVM, closing this job..");
			return;
		}
		
		final String userId = arg0.getJobDetail().getJobDataMap().getString("userId");
		
		try {
			new AutoUpdateJob(new UUID(userId)).save();
			
			LibsLogger.debug(AutoUpdateScheduler.class, "Auto-update job created for the user ", userId);
		} catch (ValidationException | IOException e) {
			LibsLogger.error(AutoUpdateScheduler.class, "Cannot start auto-updates for user " + userId + ". Consuming job..", e);
		}
	}
}
