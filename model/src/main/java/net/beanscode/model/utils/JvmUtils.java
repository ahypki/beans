package net.beanscode.model.utils;

import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.date.SimpleDate;

public class JvmUtils {

	private static final UUID thisJVM = UUID.random();
	private static final SimpleDate thisJVMDate = SimpleDate.now();
	
	public static UUID getThisJvmUUID() {
		return thisJVM.copy();
	}
	
	public static SimpleDate getThisJvmStartDate() {
		return thisJVMDate.clone();
	}
	
	public static boolean isThisJvm(UUID jvmUUID) {
		return jvmUUID != null && thisJVM.equals(jvmUUID);
	}
}
