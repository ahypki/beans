package net.beanscode.web.view.settings;

import java.io.IOException;

import org.rendersnake.HtmlCanvas;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.BetaButtonGroup;
import net.beanscode.web.view.ui.CardPanel;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.beanscode.web.view.ui.modals.OpModalQuestion;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpInfo;
import net.hypki.libs5.pjf.op.OpList;

public class SearchEngineManagementCard extends CardPanel {

	public SearchEngineManagementCard() {
		
	}
	
	public SearchEngineManagementCard(BeansComponent parent) {
		super(parent);
	}

	@Override
	protected void renderTitle(HtmlCanvas html) throws IOException {
		html
			.span()
				.content("Management");
	}

	@Override
	protected void renderContent(HtmlCanvas html) throws IOException {
		html
			.render(new BetaButtonGroup()
					.add(new Button("Clear the whole search index")
							.add(new OpModalQuestion("Clear search index", 
									"Do you want to CLEAR the whole search index? It is a safe operation because you can always rebuild search index, but "
											+ "it can take some time.", 
									new OpComponentClick(this, 
										"onClearIndex", 
										null))))
					.add(new Button("Rebuild the whole search index")
							.add(new OpModalQuestion("Rebuild search index", 
									"Do you want to rebuild the whole search index? It is a safe operation but "
											+ "it may take some time if you have millions of datasets/notebooks/tables.", 
									new OpComponentClick(this, 
										"onRebuildIndex", 
										null)))))
				;
	}

	@Override
	protected void renderFooter(HtmlCanvas html) throws IOException {
		// TODO Auto-generated method stub
		
	}
	
	private OpList onRebuildIndex() throws CLIException, IOException {
		new ApiCallCommand(this)
			.setApi("api/admin/search/reindex")
			.run();
	
		return new OpList()
			.add(new OpBeansInfo("Search index is scheduled for rebuilding"));
	}
	
	private OpList onClearIndex() throws CLIException, IOException {
		new ApiCallCommand(this)
			.setApi("api/admin/search/clear")
			.run();
	
		return new OpList()
			.add(new OpBeansInfo("Search index is scheduled for rebuilding"));
	}
}
