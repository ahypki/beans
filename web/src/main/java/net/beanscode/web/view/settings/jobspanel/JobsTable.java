package net.beanscode.web.view.settings.jobspanel;

import static net.hypki.libs5.pjf.OpsFactory.opsCall;
import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;
import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.id;
import static org.rendersnake.HtmlAttributesFactory.name;
import static org.rendersnake.HtmlAttributesFactory.type;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.core.SecurityContext;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.jobs.Job;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.beanscode.web.view.ui.modals.OpModalQuestion;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.buttons.ButtonGroup;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpRemove;
import net.hypki.libs5.pjf.op.OpReplace;
import net.hypki.libs5.pjf.op.OpSet;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.url.Params;
import net.hypki.libs5.utils.utils.NumberUtils;

import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

public class JobsTable extends BeansComponent {
	
	@Expose
	private String channelName = null;
		
	private List<Job> jobs = null;
	
	public JobsTable() {
		
	}

	public JobsTable(Component parent, String channelName) {
		super(parent);
		setChannelName(channelName);
		setId(channelName);
	}

	public OpList refresh() throws IOException, ValidationException {
		if (nullOrEmpty(getChannelName()))
			setChannelName(getParams().getString("channel"));
		
		long count = new ApiCallCommand(this)
			.setApi("api/jobs/count")
			.run()
			.getResponse()
			.getAsLong("count");
		
		return new OpList()
			.add(new OpReplace("#" + getId() + " #jobs-table", renderJobsTable(new HtmlCanvas()).toHtml()))
			.add(new OpSet(".total-jobs-" + getChannelName(), 
					"50 top jobs from total " + count + " jobs"))
			.add(new OpBeansInfo("Jobs table refreshed"));
	}
	
	private HtmlCanvas renderJobsTable(HtmlCanvas html) throws IOException {
		html
			.table(id("jobs-table").
					class_("table responsive-table jobs-table table-hover"))
				.thead()
					.th()
						.input(type("checkbox")
								.onClick("$('#" + getId() + " input:checkbox').not(this).prop('checked', this.checked);"))
					._th()
					.th()
						.content("Name")
					.th()
						.content("Description")
					.th()
						.content("ID")
					.th()
						.content("Channel")
					.th()
						.content("Creation date")
					.th()
						.content("Postpone date")
					.th()
						.content("Options")
				._thead();
		
		for (Job job : getJobs()) {
			try {
				row(html, job);
			} catch (Exception e) {
				LibsLogger.error(JobsTable.class, "Cannot render one row for Job "+ job.getId(), e);
			}
		}
		
		html
			._table();
		return html;
	}

	public OpList delete(SecurityContext sc, Params params) throws IOException, ValidationException {
		final String channel = params.getString("channel");
		final String jobId = params.getString("jobId");
		final long createMs = params.getLong("createMs", 0);
		
		new ApiCallCommand(getSessionBean(sc))
			.setApi("api/jobs/remove")
			.addParam("jobId", new UUID(jobId))
			.addParam("channel", channel)
			.addParam("creationDate", createMs)
			.run();
//		Job job = Job.getJobData(channel, createMs, new UUID(jobId));
		
//		if (job != null) {
//			job.remove();
			LibsLogger.debug(JobsTable.class, "Job ", jobId, " removed");
//		}
		
		return new OpList()
			.add(new OpRemove("#tr-" + jobId));
	}
	
	public OpList deleteJobs(SecurityContext sc, Params params) throws IOException, ValidationException {
		OpList resp = new OpList();
		
		for (Object id : params.getList("jobCombinedId")) {
			String parts[] = id.toString().split(":");
			UUID jobId = new UUID(parts[2]);
			
			new ApiCallCommand(getSessionBean(sc))
				.setApi("api/jobs/remove")
				.addParam("jobId", jobId)
				.addParam("channel", parts[0])
				.addParam("creationDate", NumberUtils.toLong(parts[1]))
				.run();

			resp.add(new OpRemove("#tr-" + jobId));
		}

		return resp;
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.div(id(getId()))
				.render(new ButtonGroup()
						.addButton(new Button("Remove selected")
								.add(new OpModalQuestion("Removing Job", 
										"Do you really want to remove all the selected jobs?", 
										new OpComponentClick(this, 
												"deleteJobs",
												null))))
						.addButton(new Button("Refresh")
								.add(new OpComponentClick(this, "refresh")))
						)
				.div(class_("total-jobs-" + getChannelName()))
					.content("")
				.table(id("jobs-table"))
				._table()
			._div();
//				.content("50 top jobs from total " + new JobsCount(getSessionBean()).run() + " jobs");
		
//		renderJobsTable(html);
	}

	private HtmlCanvas row(HtmlCanvas html, Job job) throws IOException {
		return html
				.tr(id("tr-" + job.getId()))
					.td()
						.input(name("jobCombinedId")
								.type("checkbox")
								.value(job.getChannel() + ":" + job.getTime() + ":" + job.getId().getId()))
					._td()
					.td()
						.content(job.getName() + "<br/> " + job.getId(), false)
					.td()
						.content(job.getDescription(), false)
					.td()
						.content(job.getId().toString("-"))
					.td()
						.content(job.getChannel())
					.td()
						.content(job.getCreationDate().toStringHuman())
					.td()
						.content("")
//						.content(job.isPostponed() ? job.getPostponeDate().toStringHuman() : "")
					.td()
						.div(class_("btn-group"))
							.button(class_("btn btn-default btn-xs")
										.onClick(opsCall(
												new OpModalQuestion("Removing Job", 
														"Do you really want to remove <b>" + job.getName() + "</b> job?", 
														new OpComponentClick(this, 
																"delete",
																new Params()
																	.add("channel", job.getChannel())
																	.add("jobId", job.getId().getId())
																	.add("createMs", job.getTime()))))))
								.content("Remove Job")
						._div()
					._td()
				._tr();
	}

	public List<Job> getJobs() {
		if (jobs == null) {
			try {
//				jobs = new JobsList(getSessionBean()).run(getChannelName(), 50);
				jobs = new ApiCallCommand(getSessionBean())
							.setApi("api/jobs/list")
							.addParam("count", 50)
							.addParam("channel", getChannelName())
							.run()
							.getResponse()
							.getAsList(null, Job.class);
			} catch (IOException e) {
				LibsLogger.error(JobsTable.class, "Cannot get jobs list from DB", e);
			}
		}
		return jobs;
	}

	public void setJobs(List<Job> jobs) {
		this.jobs = jobs;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}
}
