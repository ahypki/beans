package net.beanscode.model.tests;

import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;
import groovy.swing.factory.TableFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.junit.AfterClass;
import org.junit.Test;

import net.beanscode.model.BeansChannels;
import net.beanscode.model.BeansConst;
import net.beanscode.model.BeansSettings;
import net.beanscode.model.backup.AutoBackupJob;
import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.es.DataTable;
import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.notebook.autoupdate.CreateAutoUpdatesJobsForUsersJob;
import net.beanscode.model.users.CreateAdminAccountJob;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.reflection.SystemUtils;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.jobs.JobsManager;
import net.hypki.libs5.weblibs.unittest.WeblibsTestCase;
import net.hypki.libs5.weblibs.user.User;

public abstract class BeansTestCase extends WeblibsTestCase {
		
	public BeansTestCase() {
//		// default libs5-db
//		System.setProperty(DatabaseProvider.class.getSimpleName(), CassandraUtils.class.getName());
//		
//		// cassandra home
//		CassandraUtils.setCassandraHome(SystemUtils.getHomePath() + "/programy/apache-cassandra/");
//		
//		CassandraUtils.setSystemPropertyCassandraConfig();
		
		try {
//			BeansSettings.putSetting(BeansSettings.SETTING_DATABASE_VERSION, BeansConst.VERSION);
			
//			--verbose --db mapdb --settings beans-dev.json
			BeansConst.init(new String[] {"--settings", "beans-ut.json", "--db", "mapdb", "--verboseX"});
		} catch (Exception e) {
			throw new RuntimeException("Cannot initialize BEANS for testing", e);
		}
		
		getChannelNames().add(WeblibsConst.CHANNEL_NORMAL);
		getChannelNames().add(BeansChannels.CHANNEL_LONG_NORMAL);
		
	}
	
//	@Override
//	public void before() throws IOException, ValidationException {
//		super.before();
//		
//		BeansSettings.putSetting(BeansSettings.SETTING_DATABASE_VERSION, BeansConst.VERSION);
//	}
	

	@AfterClass
	public static void afterUnitTest() throws IOException, ValidationException {
		JobsManager.stopJobs(CreateAutoUpdatesJobsForUsersJob.class);
		JobsManager.stopJobs(CreateAdminAccountJob.class);
		JobsManager.stopJobs(AutoBackupJob.class);
	}
	
	protected Dataset createDataset(User user, String datasetName) throws ValidationException, IOException {
		Dataset dataset = new Dataset(user.getUserId(), datasetName);
		dataset.save();
		addToRemove(dataset);
		return dataset;
	}
	
	protected Notebook createNotebook(User user, String notebookName) throws ValidationException, IOException {
		Notebook n = new Notebook(user.getUserId(), notebookName);
		n.save();
		addToRemove(n);
		return n;
	}
	
	protected Table createTable(Dataset dataset, String tableName) throws ValidationException, IOException {
		Table table = new Table(dataset, tableName);
		table.save();
		addToRemove(table);
		return table;
	}
	
	protected void createTestTableWithData(Dataset dataset, String tableName) throws Exception {		
		final Table table = createTable(dataset, tableName);
		table.importFile(SystemUtils.getResourceURL("testdata/test-line-simple-header").getFile());
		table.save();
		
		runAllJobs(1000);
		
		Table tabDb = net.beanscode.model.dataset.TableFactory.getTable(table.getId());
		assertTrue(tabDb != null);
		assertTrue(tabDb.getColumnDefs().size() == 3);
		assertTrue(tabDb.getColumnType("x") == ColumnType.LONG);
		assertTrue(tabDb.getColumnType("y") == ColumnType.DOUBLE);
		assertTrue(nullOrEmpty(tabDb.getColumnDef("x").getDescription()));
	}
		
//	protected void generateSquareData(Plot plot, int nrTables) {
//		
//	}
	
	/**
	 * Helper function, checks if plot contains given points (x, y values only)
	 * @return
	 * @throws IOException 
	 */
	protected boolean plotContains(UUID tableId, String plotName, int ... points) throws IOException {
		HashMap<Integer, Boolean> ok = new HashMap<>();
		for (int i : points) {
			ok.put(i, false);
		}
		
		int from = 0;
		final int size = 500;
		List<DataTable> pointsES = DataTable.getPlotRows(tableId, plotName, from, size);
		while (pointsES.size() > 0) {
			for (DataTable dt : pointsES) {
				int toCheck = (int) dt.getParams().get("x");
				
				if (ok.containsKey(toCheck) == false)
					return false;
				
				ok.put(toCheck, true);
			}
			
			from = from + size;
			pointsES = DataTable.getPlotRows(tableId, plotName, from, size);
		}
		
		// check if all points were found
		for (Entry<Integer, Boolean> o : ok.entrySet()) {
			if (o.getValue() == false)
				return false;
		}
		
		return true;
	}
}
