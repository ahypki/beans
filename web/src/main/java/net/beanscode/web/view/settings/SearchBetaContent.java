package net.beanscode.web.view.settings;

import static org.rendersnake.HtmlAttributesFactory.class_;

import java.io.IOException;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.web.beta.BetaContent;
import net.beanscode.web.view.ui.BetaButtonGroup;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.beanscode.web.view.ui.modals.OpModalQuestion;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.utils.url.Params;

import org.rendersnake.HtmlCanvas;

public class SearchBetaContent extends BetaContent {

	public SearchBetaContent() {
		
	}
	
	public SearchBetaContent(Component parent) {
		super(parent);
	}
		
	@Override
	protected void renderHeader(HtmlCanvas html) throws IOException {
		html
			.h1()
				.content("Search index");
	}
	
	@Override
	protected void renderContent(HtmlCanvas html) throws IOException {
		html
			.div(class_("row"))
//				.div(class_("col-sm-12"))
					.render(new SearchEngineStatusCard(this))
					
					.render(new SearchEngineManagementCard(this))
//				._div()

//				.div(class_("col-sm-12"))
					.render(new SearchEngineChooseCard(this))
			
//				._div()
			._div()
			;
	}
	

	public OpList onRebuildIndex(Params params) throws IOException {
		new ApiCallCommand(this)
			.setApi("api/admin/search/reindex")
			.run();
		
		return new OpList()
			.add(new OpBeansInfo("Search index is scheduled for rebuilding"));
	}

	@Override
	protected BetaButtonGroup getMenu() throws IOException {
		return null;
	}

	@Override
	protected String getMenuName() throws IOException {
		return null;
	}
	
	
}
