package net.beanscode.model.dataset;

import java.io.IOException;

import net.beanscode.model.connectors.BeansConnector;
import net.beanscode.model.connectors.BeansConnectorFactory;
import net.beanscode.model.connectors.ConnectorLink;
import net.beanscode.model.connectors.ConnectorList;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.weblibs.jobs.Job;

import com.google.gson.annotations.Expose;

public class TableConnectorsClear extends Job {
	
	@Expose
	private String tableJson = null;
	
	private Table table = null;

	public TableConnectorsClear() {
		
	}
	
	public TableConnectorsClear(Table table) {
		setTableJson(table.getData());
		this.table = table;
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		LibsLogger.debug(TableConnectorsClear.class, "Removing ConnectorsList");
		
		getTable().clearConnectors();
	}
	
	private Table getTable() {
		if (table == null) {
			table = JsonUtils.fromJson(getTableJson(), Table.class);
		}
		return table;
	}

	private String getTableJson() {
		return tableJson;
	}

	private void setTableJson(String tableJson) {
		this.tableJson = tableJson;
	}
}
