package net.beanscode.web.view.settings.jobspanel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.jobs.JobsWorker;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.BetaButtonGroup;
import net.beanscode.web.view.ui.BetaTableComponent;
import net.beanscode.web.view.ui.modals.OpBeansError;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.beanscode.web.view.ui.modals.OpModalQuestion;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.url.Params;

import org.rendersnake.HtmlCanvas;

public class RunningJobWorkersPanel extends BetaTableComponent {
	
//	@Expose
	private List<JobsWorker> workers = null;

	public RunningJobWorkersPanel() {
		
	}
	
	public RunningJobWorkersPanel(BeansComponent parent) {
		super(parent);
	}
	
	@Override
	protected String getSettingsPrefix() {
		return "runningjobs-table-";
	}
	
	private List<JobsWorker> getJobsWorkers() {
		if (workers == null) {
			try {
//				workers = new JobsWorkersList(getSessionBean()).run();
				workers = new ApiCallCommand(this)
							.setApi("api/jobs/workers/list")
							.run()
							.getResponse()
							.getAsList("", JobsWorker.class);
			} catch (CLIException | IOException e) {
				LibsLogger.error(RunningJobWorkersPanel.class, "Cannot get list of Jobs workers", e);
			}
		}
		return workers;
	}
	
	@Override
	protected long getMaxHits() throws IOException {
		return getJobsWorkers().size();
	}
	
	@Override
	protected int getNumberOfColumns() throws IOException {
		return 5;
	}

	@Override
	protected void renderCell(int row, int column, HtmlCanvas html) throws IOException {
		final JobsWorker worker = getJobsWorkers().get(getPage() * getPerPage() + row);
		final SimpleDate startedAt = worker.getCurrentJobStart();
		
		if (column == 0)
			html
				.span()
					.content(worker.getChannel());
		else if (column == 1)
			html
				.span()
					.content("" + worker.getId());
		else if (column == 2)
			html
				.span()
					.content(worker.getCurrentJobName());
		else if (column == 3)
			html
				.span()
					.content(startedAt != null ? startedAt.toStringHuman() : "");
		else if (column == 4)
			html
				.render(new BetaButtonGroup()
								.add(new Button("Stop")
										.add(new OpModalQuestion("Stopping Jobs Worker", 
												"Would you like to stop Jobs Worker", 
												new OpComponentClick(this, 
														"onStop", 
														new Params().add("id", worker.getId()))))));
	}

	@Override
	protected boolean isFilterVisible() {
		return true;
	}

	@Override
	protected List<Button> getMenu() throws IOException {
		List<Button> buttons = new ArrayList<Button>();
		
//		buttons.add(new Button()
//			.setGlyphicon("refresh")
//			.add(new OpComponentClick(this, "onRefresh")));
		
		buttons.add(new Button("Stop all")
//			.setGlyphicon("refresh")
			.setTooltip("Stop all jobs")
			.add(new OpModalQuestion("Stopping all workers", 
					"Do you want to stop all workers?", 
					new OpComponentClick(this, 
							"onStopAll",
							null))));
		
		buttons.add(new Button("Run all jobs")
				.setTooltip("Run all jobs without using Jobs queuing system")
				.add(new OpModalQuestion("Running all jobs", 
						"Are you sure you want to start all jobs without using Jobs queuing system?", 
						new OpComponentClick(this, 
								"onRunAll",
								new Params()))));
		
		buttons.add(new Button("Remove all jobs")
			.setTooltip("Remove all jobs")
			.setHtmlClass("bg-gradient-danger")
			.add(new OpModalQuestion("Clearing all jobs", 
					"Do you want to clear all Jobs from the queue? It is irreversible operation, use it "
							+ "carefully because this can make your database inconsistent!", 
					new OpComponentClick(this, 
							"onClearJobs",
							new Params()))));
		
		return buttons;
	}
	
	private OpList onClearJobs() throws ValidationException, IOException {
		new ApiCallCommand(getSessionBean())
			.setApi("api/jobs/clearall")
			.run();
		
		return new OpList()
			.add(new OpBeansInfo("All jobs cleared from the queue"));
	}
	
	private OpList onRunAll() throws ValidationException, IOException {
		new ApiCallCommand(getSessionBean())
			.setApi("api/jobs/runall")
			.run();
		
		return new OpList()
			.add(new OpBeansInfo("A job to run all jobs started"));
	}
	

//	private OpList onRefresh() throws IOException {
//		HtmlCanvas html = new HtmlCanvas();
//		
//		html.table();
//		for (JobsWorker worker : JobsManager.iterateJobsThreads()) {
//			
//			
//			html
//				._tr();
//		}
//		html._table();
//		
//		return new OpList()
//			.add(new OpSet("#" + getId() + " .jobs-table", html.toHtml()));
//	}
	
	@Override
	protected String getColumnName(int column) throws IOException {
		if (column == 0)
			return "Channel";
		else if (column == 1)
			return "ID";
		else if (column == 2)
			return "Current Job's name";
		else if (column == 3)
			return "Started at";
		else if (column == 4)
			return "Options";
		else
			return "";
	}
	
	private OpList onStopAll() {
		OpList ops = new OpList();
		
		for (JobsWorker jobsWorker : getJobsWorkers()) {
			try {
				new ApiCallCommand(this)
					.setApi("api/jobs/workers/stop")
					.addParam("id", jobsWorker.getId())
					.run();
			} catch (CLIException | IOException e) {
				LibsLogger.error(RunningJobWorkersPanel.class, "Cannot stop " + jobsWorker.getId(), e);
				ops.add(new OpBeansError("Cannot stop worker " + jobsWorker.getId()));
			}
		}
		
		if (ops.size() == 0)
			ops.add(new OpBeansInfo("All workers stopped"));
		
		ops.add(new OpComponentClick(this, "onRefresh"));
		
		return ops;
	}
	
	private OpList onStop() throws CLIException, IOException {
		final long id = getParams().getLong("id", 0);
		
		if (id <= 0) {
			LibsLogger.error(JobsWorkersPanel.class, "Id of a Thread is wrong");
			return null;
		}
		
		new ApiCallCommand(this)
			.setApi("api/jobs/workers/stop")
			.addParam("id", id)
			.run();
		
		return new OpList()
			.add(new OpBeansInfo("Thread was interrupted successfully"))
			.add(new OpComponentClick(this, "onRefresh"));
	}

	@Override
	protected void renderTitle(HtmlCanvas html) throws IOException {
		html
			.write("Running jobs workers");
	}

	@Override
	protected boolean isHeaderColumnVisible() throws IOException {
		return true;
	}

	@Override
	protected boolean isExapandable() throws IOException {
		return false;
	}

	@Override
	protected void renderExpandable(int row, HtmlCanvas html) throws IOException {

	}

	@Override
	protected String getTableCss() throws IOException {
		return null;
	}
}
