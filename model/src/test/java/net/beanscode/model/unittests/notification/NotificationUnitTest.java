package net.beanscode.model.unittests.notification;

import java.io.IOException;

import org.junit.Test;

import net.beanscode.model.cass.notification.Notification;
import net.beanscode.model.cass.notification.NotificationFactory;
import net.beanscode.model.tests.BeansTestCase;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.weblibs.user.User;

public class NotificationUnitTest extends BeansTestCase {

	@Test
	public void testCreate() throws ValidationException, IOException {
		User user = createTestUser("note1");
		
		Notification n = new Notification()
				.setDescription("desc")
				.setTitle("title1")
				.setUserId(user.getUserId())
				.save();
		
		addToRemove(n);
		
		Notification nFromDb = NotificationFactory.getNotification(n.getId());
		assertTrue(nFromDb != null, "Cannot get Notification from DB");
		assertTrue(nFromDb.getDescription().equalsIgnoreCase("desc"), "Notification saved wrong description into the DB");
		
		removeAndCheck();
	}
}
