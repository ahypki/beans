package net.beanscode.pojo.tables;

import java.io.Serializable;

import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class ConnectorLink implements Serializable {

	@Expose
	@NotNull
	@NotEmpty
	private String connectorClass = null;
	
	@Expose
	private ConnectorInitParams connectorInitParams = null;
		
	public ConnectorLink() {
		
	}
	
	public ConnectorLink(Class connectorClass, ConnectorInitParams initParams) {
		setConnectorClass(connectorClass.getName());
		setConnectorInitParams(initParams);
	}
	
	public ConnectorLink(String connectorClass, ConnectorInitParams initParams) {
		setConnectorClass(connectorClass);
		setConnectorInitParams(initParams);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ConnectorLink) {
			ConnectorLink cl = (ConnectorLink) obj;
			return cl.getConnectorClass().equals(this.getConnectorClass())
					&& cl.getConnectorInitParams().equals(getConnectorInitParams());
		}
		return false;
	}
	
	@Override
	public String toString() {
		return getConnectorClass() + ":" + getConnectorInitParams();
	}

	public String getConnectorClass() {
		return connectorClass;
	}

	public ConnectorLink setConnectorClass(String connectorClass) {
		this.connectorClass = connectorClass;
		return this;
	}

	public ConnectorInitParams getConnectorInitParams() {
		return connectorInitParams;
	}

	public ConnectorLink setConnectorInitParams(ConnectorInitParams connectorInitParams) {
		this.connectorInitParams = connectorInitParams;
		return this;
	}
}
