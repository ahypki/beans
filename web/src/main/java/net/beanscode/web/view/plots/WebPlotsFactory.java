package net.beanscode.web.view.plots;

import static java.lang.String.format;

import java.util.List;

import net.beanscode.cli.notebooks.Plot;
import net.beanscode.cli.notebooks.PlotHistogramEntry;
import net.beanscode.cli.notebooks.PlotParallelCoordinatesEntry;

import com.google.gson.JsonArray;
import com.google.gson.JsonPrimitive;

public class WebPlotsFactory {
	
	private static final String DATA_URL = "view/data";
	
	public static String createParallelCoordinates(PlotParallelCoordinatesEntry plot) {
		JsonArray columnsArr = new JsonArray();
		for (String column : plot.getColumnsAsList()) {
			columnsArr.add(new JsonPrimitive(column));
		}
		return format("beansD3Parallel('%s', %s, '%s', 'queryId=%s&plotNr=%d', '%s');", 
				plot.getId().getId(), 
				columnsArr.toString().replaceAll("\"", "'"),
				"view/data/parallel", 
				plot.getId().getId(), 0,
				plot.getTitle());
	}

	public static String createPoints(Plot plot, int plotNr, List<String> columns) {
		JsonArray columnsArr = new JsonArray();
		for (String column : columns) {
			columnsArr.add(new JsonPrimitive(column));
		}
		return format("beansD3Points('figure-%s', %s, '%s?queryId=%s&plotNr=%d');", 
				plot.getId(), 
				columnsArr.toString().replaceAll("\"", "'"),
//				columnsArr,
				DATA_URL, plot.getId(), plotNr);
//		rest/query/data?queryId={0}&plotName={1}".format(queryId, currentPlotName))"
	}
	
	public static String createLines(Plot plot, int plotNr, List<String> columns) {
		JsonArray columnsArr = new JsonArray();
		for (String column : columns) {
			columnsArr.add(new JsonPrimitive(column));
		}
		return format("beansD3Lines('%s', %s, '%s', 'queryId=%s&plotNr=%d', '%s');", 
				plot.getId().getId(), 
				columnsArr.toString().replaceAll("\"", "'"),
				DATA_URL, 
				plot.getId().getId(), plotNr,
				plot.getName());
	}
	
	public static String createBoxes(PlotHistogramEntry plot, int plotNr) {
		JsonArray columnsArr = new JsonArray();
		columnsArr.add(new JsonPrimitive(plot.getX()));
		columnsArr.add(new JsonPrimitive(plot.getY()));
		return format("beansD3Boxes('%s', %s, '%s', 'queryId=%s&plotNr=%d', '%s', '%s', '%s');", 
				plot.getId().getId(), 
				columnsArr.toString().replaceAll("\"", "'"),
				"view/data/histogram", 
				plot.getId().getId(), 
				plotNr,
				plot.getTitle(),
				plot.getXLabelNotNull(),
				plot.getYLabelNotNull()
				);
	}

	public static String createLinesPlot(Plot plot, int plotNr) {
//		String queryId = plot.getQueryId().getId();
//		return format("d3Plots['%s'] = c3.generate({ data: { xs: { data1: 'x1', data2: 'x2'}, url: '%s?queryId=%s&plotNr=%d&type=csv', mimeType: 'json'}, bindto : '#figure-%s'});",
//				queryId,
//				PlotRefresh.DATA_URL, queryId, plotNr,
//				queryId);
		
		// TEST it works
//		String queryId = plot.getQueryId().getId();
//		return format("d3Plots['%s'] = c3.generate({data: { xs: {'data1': 'x1','data2': 'x2',},columns: [['x1', 10, 30, 45, 50, 70, 100],['x2', 30, 50, 75, 100, 120],['data1', 30, 200, 100, 400, 150, 250],['data2', 20, 180, 240, 100, 190]]}, bindto : '#figure-%s'});",
//				queryId,
//				queryId);
		
		String queryId = plot.getId().getId();
		return format("d3Plots['%s'] = c3.generate({data: { xs: {'data1': 'x1','data2': 'x2',}, url: '%s?queryId=%s&plotNr=%d', mimeType: 'json'}, bindto : '#figure-%s'});",
				queryId,
				DATA_URL, queryId, plotNr,
				queryId);
		
//		String queryId = plot.getQueryId().getId();
//		return format("d3Plots['%s'] = c3.generate({data: { xs: {data1: 'x1',data2: 'x2',}, url: '%s?queryId=%s&plotNr=%d', mimeType: 'json'}, bindto : '#figure-%s'});",
//				queryId,
//				PlotRefresh.DATA_URL, queryId, plotNr,
//				queryId);
	}
}
