package net.beanscode.web.view.ui;

import static net.hypki.libs5.pjf.OpsFactory.opsCall;
import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.id;
import static org.rendersnake.HtmlAttributesFactory.value;

import java.io.IOException;

import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.op.OpHide;
import net.hypki.libs5.pjf.op.OpJs;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpRemoveReadonly;
import net.hypki.libs5.pjf.op.OpSetReadonly;
import net.hypki.libs5.pjf.op.OpShow;

import org.rendersnake.HtmlCanvas;

public class EditableInput extends Component {
	
	private String name = null;
	
	private String text = null;

	public EditableInput() {
		
	}
	
	public EditableInput(String name, String text) {
		setName(name);
		setText(text);
	}
	
	protected OpList getAdditionalSaveOp() {
		return null;
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.span(id(getId())
					.class_("editable-input"))
				.input(value(getText())
					.name(getName())
					.readonly("true")
					.add("style", "background: transparent; border: none; border-bottom: 1px dotted; border-color: light-gray; width: 100%;")
					.onDblclick(opsCall(
							new OpJs("$('#" + getId() + " input').data('prev-value', $('#" + getId() + " input').val())"),
							new OpRemoveReadonly("#" + getId() + " input"),
							new OpShow("#" + getId() + " .save"),
							new OpShow("#" + getId() + " .cancel")))
					)
				.a(class_("save")
					.style("display: none;")
					.onClick(opsCall(
							new OpSetReadonly("#" + getId() + " input"),
							new OpHide("#" + getId() + " .save"),
							new OpHide("#" + getId() + " .cancel"),
							getAdditionalSaveOp()
						)))
					.content(" save ")
				.a(class_("cancel")
					.style("display: none;")
					.onClick(opsCall(
								new OpJs("$('#" + getId() + " input').val($('#" + getId() + " input').data('prev-value'))"),
								new OpSetReadonly("#" + getId() + " input"),
								new OpHide("#" + getId() + " .save"),
								new OpHide("#" + getId() + " .cancel")
						)))
					.content(" cancel ")
			._span()
				;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
