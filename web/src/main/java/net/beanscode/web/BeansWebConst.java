package net.beanscode.web;


public class BeansWebConst {
	public static final String VERSION = "1.0.2";
	
	public static final int DEFAULT_DIALOG_WIDTH = 640;
	public static final int DEFAULT_DIALOG_HEIGHT = 480;
	
	public static final String DEFAULT_NOTEBOOK_NAME = "New notebook";
	public static final String DEFAULT_DATASET_NAME = "New dataset";
	public static final String DEFAULT_TABLE_NAME = "New table";
	
	public static final int PLOT_AUTOREFRESH_INTERVAL_MS = 2000;
	
	public static final int PIG_AUTOREFRESH_INTERVAL_MS = 5000;
	
	public static final String COOKIE_LOGIN = "userLC";
	public static final String COOKIE_USERID = "userId";
	
	public static final int COOKIE_VALID_DAYS_DEFAULT = 120;
	
	public static final String ICON_REMOVE = "fa fa-trash";
	
	public static final String MAGIC_KEY = "magickey";
}
