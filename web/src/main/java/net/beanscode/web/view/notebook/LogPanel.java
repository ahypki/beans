package net.beanscode.web.view.notebook;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static org.rendersnake.HtmlAttributesFactory.id;
import static org.rendersnake.HtmlAttributesFactory.name;

import java.io.IOException;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.web.view.ui.BeansComponent;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.ops.OpComponentTimer;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpSet;

import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

public class LogPanel extends BeansComponent {
	
	@Expose
	private UUID entryId = null;

	public LogPanel(BeansComponent parent) {
		super(parent);
	}
	
	public LogPanel(BeansComponent parent, UUID entryId) {
		super(parent);
		setEntryId(entryId);
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		String logs = new ApiCallCommand(this)
			.setApi("api/notebook/entry/log")
			.addParam("entryId", getEntryId().getId())
			.run()
			.getResponse()
			.getAsString("log");
		
		html
			.div(id(getId())
					.class_("log-panel"))
				.input(name("log-filter")
						.class_("log-filter form-control")
						.add("placeholder", "Regex log filter")
						.onKeyup(new OpComponentTimer(this, "onRefresh", 300).toStringOnclick())
						.onChange(new OpComponentTimer(this, "onRefresh", 300).toStringOnclick())
						)
				.textarea(id("log-content")
						.class_(" form-control")
						.readonly("readonly"))
					.content(logs)
//					.content(new EntryLogGet(getSessionBean()).run(getEntryId().getId()))
			._div();
	}
	
	private OpList onRefresh() throws CLIException, IOException {
		String filter = getParams().getString("log-filter");
		
		if (notEmpty(filter) && filter.matches("[\\w]+"))
			filter = ".*" + filter + ".*";
		
		String newContent = new ApiCallCommand(this)
			.setApi("api/notebook/entry/log")
			.addParam("entryId", getEntryId().getId())
			.addParam("filter", filter)
			.run()
			.getResponse()
			.getAsString("log");
		
		return new OpList()
			.add(new OpSet("#" + getId() + " #log-content", newContent));
	}

	public UUID getEntryId() {
		return entryId;
	}

	public void setEntryId(UUID entryId) {
		this.entryId = entryId;
	}
}
