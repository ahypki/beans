package net.beanscode.web.view.backup;

import static net.hypki.libs5.utils.string.Base64Utils.decode;
import static net.hypki.libs5.utils.string.Base64Utils.encode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.BetaTableComponent;
import net.beanscode.web.view.ui.modals.ModalWindow;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.beanscode.web.view.ui.modals.OpModal;
import net.beanscode.web.view.ui.modals.OpModalQuestion;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.buttons.ButtonGroup;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.Base64Utils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.url.Params;

import org.rendersnake.HtmlCanvas;

public class BackupPanel extends BetaTableComponent {
	
//	@Expose
	private List<String> backupFiles = null;

	public BackupPanel() {
		
	}
	
	public BackupPanel(BeansComponent parent) {
		super(parent);
	}
	
	@Override
	protected String getSettingsPrefix() {
		return "backups-table-";
	}
	
	@Override
	protected int getPerPage() {
		return 10;
	}
	
	@Override
	protected long getMaxHits() throws IOException {
		return getBackupFiles() != null ? getBackupFiles().size() : 0;
	}
	
	@Override
	protected boolean isHeaderColumnVisible() throws IOException {
		return true;
	}
	
	@Override
	protected String getColumnName(int column) {
		if (column == 0)
			return "Filename";
		else if (column == 1)
			return "Options";
		else
			return "";
	}
	
	@Override
	protected int getNumberOfColumns() throws IOException {
		return 2;
	}
	
	@Override
	protected boolean isExapandable() throws IOException {
		return false;
	}
	
	@Override
	protected void renderCell(int row, int column, HtmlCanvas html) throws IOException {
		String file = getBackupFiles().get(row);
		
		if (column == 0)
			html
				.write(file);
		else if (column == 1)
			html
				.render(new ButtonGroup()
							.addButton(new Button("Restore")
									.add(new OpComponentClick(this, "onRestoreDialog",
											new Params()
												.add("restoreFile", Base64Utils.encode(file))
												)
											
//											OpsFactory.opConfirmation("Restoring backup", 
//													"Are you sure you want to restore the backup " + 
//															file+ "?"), 
//											new Params()
//												.add("restoreFile", Base64Utils.encode(file)))))
									)));
	}

	@Override
	protected boolean isFilterVisible() {
		return true;
	}

	@Override
	protected List<Button> getMenu() throws IOException {
		return new ButtonGroup()
//			.addButton(new Button("Refresh"))
			.addButton(new Button("Backup")
					.setGlyphicon("hdd")
					.add(new OpModalQuestion("Backup", 
							"Do you want to start backup?", 
							new OpComponentClick(this, "onBackup", null)))
					)
			.getButtons()
			;
	}
	
	@Override
	protected void renderExpandable(int row, HtmlCanvas html) throws IOException {
		
	}
	
	@Override
	protected void renderTitle(HtmlCanvas html) throws IOException {
		html
			.write("Backups");
	}
	
	private OpList onBackup() throws ValidationException, IOException {
		ApiCallCommand api = new ApiCallCommand(this)
			.setApi("api/admin/backups/backup")
			.addParam("userId", getUserId())
			.run();
		
//		new BackupDatabase(getSessionBean()).run(getUserEmail());
//		new BackupJob()
//			.setUserId(getUserId())
//			.save();
		
		return new OpList()
				.add(new OpBeansInfo("Backup added to scheduler successfully"));
	}
	
	private OpList onRestoreDialog() throws IOException {
//		String file64 = getParams().getString("restoreFile");
		String file = getParams().getString("restoreFile");
		file = decode(file);
		
		ModalWindow modal = new ModalWindow(this);
		modal.setTitle("Restoring database");
		modal
			.getContent()
			.render(new RestoreDialogPanel(file));
		modal
			.add(new Button("Restore")
					.add(new OpComponentClick(this.getClass(), 
							modal.getId(),
							"onRestoreQuestion",
							new Params())));
		modal
			.add(new Button("Cancel")
					.add(modal.onClose()));
		
		return new OpList()
			.add(new OpModal(modal));
	}
	
	private OpList onRestore() throws ValidationException, IOException {
		String file = getParams().getString("restoreFileX");
		file = decode(file);
		final boolean restoreUsers = getParams().getBoolean("usersX");
		final boolean restoreDatasets = getParams().getBoolean("datasetsX");
		final boolean restoreNotebooks = getParams().getBoolean("notebooksX");
		final int restoreStrategy = getParams().getInteger("selected-option-idX", -1);
		final String withPrefix = getParams().getString("restore-prefix");
		final String withSuffix = getParams().getString("restore-suffix");
		
		new ApiCallCommand(getSessionBean())
			.setApi("api/admin/backups/restore")
			.addParam("userId", getUserId())
			.addParam("file", file)
			.addParam("restoreUsers", restoreUsers)
			.addParam("restoreDatasets", restoreDatasets)
			.addParam("restoreNotebooks", restoreNotebooks)
			.addParam("restoreStrategy", restoreStrategy)
			.addParam("withPrefix", withPrefix)
			.addParam("withSuffix", withSuffix)
			.run();
		
		return new OpList()
			.add(new OpBeansInfo("Restoring backup " + file + " added to the scheduler, be patient"));
	}
	
	private OpList onRestoreQuestion() throws ValidationException, IOException {
		return new OpList()
			.add(new OpModalQuestion("Restoring database", 
					"Do you really want to restore the database?", 
					new OpComponentClick(this, 
							"onRestore", 
							new Params()
								.add("usersX", getParams().getBoolean("users"))
								.add("datasetsX", getParams().getBoolean("datasets"))
								.add("notebooksX", getParams().getBoolean("notebooks"))
								.add("selected-option-idX", getParams().getInteger("selected-option-id", -1))
								.add("restoreFileX", encode(getParams().getString("restoreFile"))))));
	}

	private List<String> getBackupFiles() {
		if (backupFiles == null) {
			try {
				ApiCallCommand call = new ApiCallCommand(this)
					.setApi("api/admin/backups/list")
					.addParam("filter", getFilter())
					.run();
				
				List<String> files = call.getResponse().getAsList("files", String.class);
				
				Collections.sort(files, new Comparator<String>() {
					@Override
					public int compare(String o1, String o2) {
						return o2.compareTo(o1);
					}
				});
				
				// applying filer
				if (isFilterVisible() && StringUtilities.notEmpty(getFilter())) {
					backupFiles = new ArrayList<String>();
					String[] filterArr = StringUtilities.split(getFilter());
					for (String file : files) {
						boolean add = true;
						for (String filterPart : filterArr) {
							if (!file.contains(filterPart)) {
								add = false;
								break;
							}
						}
						if (add)
							backupFiles.add(file);
					}
				} else
					backupFiles = files;
			} catch (CLIException | IOException e) {
				LibsLogger.error(BackupPanel.class, "Cannot get list of backup files", e);
			}
		}
		return backupFiles;
	}

	private void setBackupFiles(List<String> backupFiles) {
		this.backupFiles = backupFiles;
	}

	@Override
	protected String getTableCss() throws IOException {
		return null;
	}
}
