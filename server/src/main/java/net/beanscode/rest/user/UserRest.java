package net.beanscode.rest.user;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;
import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

import mjson.Json;
import net.beanscode.model.BeansConst;
import net.beanscode.model.BeansSettings;
import net.beanscode.model.rights.Rights;
import net.beanscode.model.users.CreateAdminAccountJob;
import net.beanscode.rest.BeansRest;
import net.hypki.libs5.db.db.Results;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.db.weblibs.Settings;
import net.hypki.libs5.pjf.op.OpCookieCreate;
import net.hypki.libs5.pjf.op.OpDialogClose;
import net.hypki.libs5.pjf.op.OpError;
import net.hypki.libs5.pjf.op.OpErrorCookie;
import net.hypki.libs5.pjf.op.OpInfo;
import net.hypki.libs5.pjf.op.OpInfoCookie;
import net.hypki.libs5.pjf.op.OpRedirect;
import net.hypki.libs5.search.SearchResults;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.DateUtils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.url.HttpResponse;
import net.hypki.libs5.weblibs.SearchManager;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.mail.MailCredentials;
import net.hypki.libs5.weblibs.mail.Mailer;
import net.hypki.libs5.weblibs.settings.SettingFactory;
import net.hypki.libs5.weblibs.user.Allowed;
import net.hypki.libs5.weblibs.user.CookieUUID;
import net.hypki.libs5.weblibs.user.Email;
import net.hypki.libs5.weblibs.user.ResetPassJob;
import net.hypki.libs5.weblibs.user.ResetPassUUID;
import net.hypki.libs5.weblibs.user.User;
import net.hypki.libs5.weblibs.user.UserFactory;
import net.hypki.libs5.weblibs.user.UserStatus;

@Path("api/user")
public class UserRest extends BeansRest {
	
	@POST
    @Path("status")
	@RolesAllowed(Rights.ADMIN)
	public void status(@Context SecurityContext sc,
			@FormParam("userId") UUID userId, 
			@FormParam("newStatus") String newStatusStr) throws IOException, ValidationException {
		validateUnauthorized(isAdmin(sc), "Permission denied");
		
		final UserStatus newStatus = UserStatus.valueOf(newStatusStr);
		final User user = UserFactory.getUser(userId);
		
		assertTrue(user != null, "User ID not specified");
		assertTrue(user.getStatus() != newStatus, "User " + user.getEmail() + " already has status " + newStatus + ". No changes made.");
		assertTrue(user.getUserId().equals(getSessionBean(sc).getUserId()) == false, "You can not change your own status");
		
		user.setStatus(newStatus);
		user.save();
			
		LibsLogger.debug(getClass(), String.format("User %s new status %s saved", user.getEmail(), user.getStatus()));
	}
	
	@GET
    @Path("get")
    @Produces(MediaType.APPLICATION_JSON)
	public Response get(@Context SecurityContext sc,
			@QueryParam("userId") String userId,
			@QueryParam("userEmail") String userEmail) throws IOException, ValidationException {		
		User user = null;
		
		if (notEmpty(userId))
			user = UserFactory.getUser(new UUID(userId));
		else
			user = User.getUser(new Email(userEmail));
		
		assertTrue(user != null, "User not found");
		
		return Response
				.ok()
				.entity(Json
						.object()
						.set("id", Json.object().set("id", user.getUserId().getId()))
						.set("email", user.getEmail().getEmail())
						.set("creationDate", user.getCreationDate())
						.set("status", user.getStatus().toString())
						.toString())
				.build();
	}
	
	@GET
    @Path("search")
    @Produces(MediaType.APPLICATION_JSON)
	public Response search(@Context SecurityContext sc,
			@QueryParam("search-term") String searchTerm,
			@QueryParam("from") int from, 
			@QueryParam("size") int size) throws IOException, ValidationException {
		
		validateUnauthorized(getUserId(sc) != null, "Permission denied");
		
		SearchResults<User> usersTmp = SearchManager.searchUsers(searchTerm, from, size);
		
		mjson.Json usersArray = Json.array();
		for (User user : usersTmp) {
			mjson.Json oneUSer = Json.object();
			mjson.Json id = Json.object();
			id.set("id", user.getUserId().getId());
			oneUSer.set("id", id);
			oneUSer.set("email", user.getEmail().getEmail());
			oneUSer.set("creationDate", user.getCreationDate());
			oneUSer.set("status", user.getStatus().toString());
			usersArray.add(oneUSer);
		}
		
		mjson.Json ret = Json.object();
		ret.set("maxHits", usersTmp.maxHits());
		ret.set("users", usersArray);
		
		return Response
				.ok()
				.entity(ret.toString())
				.build();
	}
	
	@GET
    @Path("allowed")
    @Produces(MediaType.APPLICATION_JSON)
	public Response allowed(@Context SecurityContext sc,
			@QueryParam("user-email") String userEmail,
			@QueryParam("userId") UUID userId,
			@QueryParam("right") String right) throws IOException, ValidationException {
		User userToCheck = userId != null ? UserFactory.getUser(userId) : User.getUser(new Email(userEmail));
		
		validateNotFound(userToCheck != null, "User not found");
		
		return Response
				.ok()
				.entity(Json
						.object()
						.set("allowed", Allowed.isAllowed(userToCheck.getUserId(), right))
						.toString())
				.build();
	}
	
	@POST
    @Path("create")
	@RolesAllowed(Rights.ADMIN)
    @Produces(MediaType.APPLICATION_JSON)
	public Response create(@Context SecurityContext sc,
			@FormParam("email") String email, 
			@FormParam("pass") String pass) throws IOException, ValidationException {
		validateUnauthorized(isAdmin(sc), "Permission denied");
				
		final User userUniquity = User.getUser(new Email(email));
		
//		assertTrue(isAdmin(), "Insuficient rights");
		validateBadRequest(userUniquity == null, "User with this email already exists");
//		assertTrue(notEmpty(pass), "Password can not be empty");
//		assertTrue(notEmpty(pass2), "Password 2 can not be empty");
//		assertTrue(pass1.equals(pass2), "Passwords do not match");
		
		try {
			User newUser = new User();
			newUser.setEmail(email);
			newUser.setLogin(email);
			if (nullOrEmpty(pass))
				newUser.setRandomPassword();
			else
				newUser.setPlainPasswordForHashing(pass);
			newUser.setStatus(UserStatus.NEW);
			newUser.setUserUUID(UUID.random());
			newUser.setAlreadyStatusNormal(true);
			newUser.save();
			
//			return newUser.getUserId().getId();
//			mjson.Json json = Json.object();
//			json.set("id", newUser.getUserId().getId());
//			json.set("email", newUser.getEmail().getEmail());
//			json.set("creationDate", newUser.getCreationDate());
//			json.set("status", newUser.getStatus().toString());
			
//			return json.asString();
			
			return Response
					.ok()
					.entity(Json
							.object()
							.set("id", Json.object().set("id", newUser.getUserId().getId()))
							.set("email", newUser.getEmail().getEmail())
							.set("creationDate", newUser.getCreationDate())
							.set("status", newUser.getStatus().toString())
							.toString())
					.build();
		} catch (Exception e) {
			LibsLogger.error(UserRest.class, String.format("Cannot change status for user %s for the status %s", userUniquity.getEmail(), userUniquity.getStatus()));
//			return "FAIL";
			return Response
					.noContent() // TODO error?
					.build();
		}
		
//		return toString();
	}
		
	@POST
    @Path("login")
	@PermitAll
    @Produces(MediaType.APPLICATION_JSON)
	public Response login(@FormParam("email") String userEmail, 
			@FormParam("pass") String userPass,
			@Context UriInfo url) throws IOException, ValidationException {		
		final User user = User.getUser(new Email(userEmail));
		
		validateNotFound(user != null, "User not found");
		validateUnauthorized(user.isPasswordCorrect(userPass), "Login credentialns invalid");
		
		try {
			// creating login cookie
			CookieUUID cookieUUID = new CookieUUID();
			cookieUUID.setUserUUID(user.getUserId());
			cookieUUID.setUserName(user.getEmail().getEmail());
			cookieUUID.setUserEmail(user.getEmail().getEmail());
//				cookieUUID.setUserName(cookieUUID.getUserEmail());
			cookieUUID.setUuid(UUID.random());
			cookieUUID.setClientDomain(url.getRequestUri().getHost());
			cookieUUID.setLastLogin(System.currentTimeMillis());
			cookieUUID.setRememberMe(false);
			cookieUUID.setExpirationDate(System.currentTimeMillis() + BeansConst.COOKIE_VALID_DAYS_DEFAULT * DateUtils.ONE_DAY_MS);
			
			cookieUUID.save();
			return Response
					.ok()
					.entity(Json
							.object()
							.set("userLC", cookieUUID.getUuid().getId())
							.set("userId", user.getUserId().getId())
							.toString())
					.build();
		} catch (ValidationException e) {
			LibsLogger.error("Cannot create login cookie", e);
			return Response
					.status(Response.Status.UNAUTHORIZED.getStatusCode())
					.build();
		}
	}
	
	@POST
	@Path("recover")
	@Produces(MediaType.APPLICATION_JSON)
	public Response recover(@FormParam("email") String userEmail) throws IOException, ValidationException {
//		validateUnauthorized(isAdmin(sc), "Permission denied");
		
		assertTrue(notEmpty(userEmail), "Email cannot be empty");

		final User user = User.getUser(new Email(userEmail));
		assertTrue(user != null, "User not found. Are you sure you typed valid email address?");
		
		MailCredentials mc = BeansSettings.getMailCredentials();
		assertTrue(mc != null, "Cannot reset password because administrator did not set up email account yet.");
//		assertTrue(mc.isValid(), "Cannot reset password because administrator did not set up email account yet.");
		
		new ResetPassUUID(user.getUserId(), mc)
			.setMailSubject("[BEANS] Link to reset the password")
			.setDomain(BeansSettings.getDomain())
			.save();
		
//		addToResponse(new OpInfo("Mail sent", "Mail with instructions how to change the password was sent to the given email"));
		
		return Response
				.ok()
				.entity(Json
						.object()
						.set("status", "OK")
						.toString())
				.build();
	}
	
	@GET
	@Path("confirmResetPass")
	@Produces(MediaType.APPLICATION_JSON)
	public Response confirmResetPass(@QueryParam("userId") UUID userId,
			@QueryParam("resetId") UUID resetId) throws IOException, ValidationException {
		try {
			assertTrue(!nullOrEmpty(userId), "User ID cannot be empty");
//			assertTrue(!nullOrEmpty(resetId), "Reset ID ID cannot be empty");
			
			ResetPassUUID reset = ResetPassUUID.getResetPassId(userId);
			
			if (reset != null) {
				new ResetPassJob(userId).save();
				addToResponse(new OpInfoCookie("New password will be send by email"));
			} else {
				addToResponse(new OpErrorCookie("There was a problem creating a new password, try again later"));
			}
			
			return Response.seeOther(new URI("/login?resetSent=true")).build();
		} catch (URISyntaxException e) {
			throw new IOException("Cannot reset password and redirect", e);
		}
	}
	
	@POST
	@Path("reset")
	@Produces(MediaType.APPLICATION_JSON)
	public void reset(@Context SecurityContext sc, 
			@FormParam("userId") String userId) throws IOException, ValidationException {
		validateNotFound(userId != null, "User ID is not specified");
		
		final User user = UserFactory.getUser(new UUID(userId));
		final String host = SettingFactory.getSettingValueString("BEANS_HOST", "host", "localhost:25000");
		final String pass = user.setRandomPassword();
		final String mailTitle = "[BEANS] New password";
		final String mailBody = "Password for the BEANS web page has been reset.\n\n"
				+ "url:       " 		+ host 				+ "\n"
				+ "username:  " 	+ user.getEmail()	+ "\n"
				+ "password:  " 	+ pass 				+ "\n";
		
		validateNotFound(user != null, "User with this email already exists");
		validateBadRequest(notEmpty(pass), "New password cannot be empty");
				
		try {			
			user.setPlainPasswordForHashing(pass);
			user.save();
			
			Mailer.sendMail(BeansSettings.getMailCredentials(), 
					user.getEmail().toString(), 
					null, 
					null,
					mailTitle, 
					mailBody, 
					false, 
					null);
		} catch (IOException | ValidationException e) {
			throw e;
		} catch (Exception e) {
			throw new IOException(String.format("Cannot reset pass for user %s", user.getEmail()), e);
		}
	}
	
	@POST
	@Path("update")
	public void update(@Context SecurityContext sc,
			@FormParam("user-email") String userEmail,
			@FormParam("old-pass") String oldPass,
			@FormParam("new-pass1") String pass1,
			@FormParam("new-pass2") String pass2) throws IOException, ValidationException {
		validateBadRequest(notEmpty(userEmail), "User email cannot be empty");
		
		final User userToChange = User.getUser(new Email(userEmail));
		final boolean isAdmin = isAdmin(sc);
		
		validateUnauthorized(getUserId(sc) != null, "User is not loged in");
		validateNotFound(userToChange != null, "User not found");
		validateUnauthorized(isAdmin || userToChange.isPasswordCorrect(oldPass), "Old password is incorrect");
		validateBadRequest(notEmpty(pass1), "Password 1 can not be empty");
		validateBadRequest(notEmpty(pass2), "Password 2 can not be empty");
		validateBadRequest(pass1.equals(pass2), "Passwords do not match");
		
		userToChange.setPlainPasswordForHashing(pass1);
		userToChange.save();
    }
}
