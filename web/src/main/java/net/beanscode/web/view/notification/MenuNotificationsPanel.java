package net.beanscode.web.view.notification;

import static org.rendersnake.HtmlAttributesFactory.class_;

import java.io.IOException;
import java.util.List;

import org.rendersnake.HtmlAttributesFactory;
import org.rendersnake.HtmlCanvas;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.notification.Notification;
import net.beanscode.cli.notification.NotificationSearchResults;
import net.beanscode.cli.notification.NotificationType;
import net.beanscode.web.view.ui.BeansComponent;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpAddClass;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpReplace;
import net.hypki.libs5.pjf.op.OpSet;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.url.Params;

public class MenuNotificationsPanel extends BeansComponent {
	public MenuNotificationsPanel() {
		
	}
	
	public MenuNotificationsPanel(Component parent) {
		super(parent);
	}
	
	public OpList onRefresh() throws IOException {
		NotificationSearchResults all = getNotifications(null);
		NotificationSearchResults error = getNotifications(NotificationType.ERROR.toString());
		NotificationSearchResults warn = getNotifications(NotificationType.WARN.toString());
		NotificationSearchResults question = getNotifications(NotificationType.QUESTION.toString());
		NotificationSearchResults info = getNotifications(NotificationType.INFO.toString());
		NotificationSearchResults ok = getNotifications(NotificationType.OK.toString());
		
//		HtmlCanvas html = new HtmlCanvas();
//		
//		renderOn(html);
//		
		return new OpList()
			.add(new OpSet(".beans-notifications-all", "" + all.getMaxHits()))
			.add(new OpSet(".beans-notifications-error", "" + error.getMaxHits() + " errors"))
			.add(new OpSet(".beans-notifications-warn", "" + warn.getMaxHits() + " warnings"))
			.add(new OpSet(".beans-notifications-question", "" + question.getMaxHits() + " questions"))
			.add(new OpSet(".beans-notifications-info", "" + info.getMaxHits() + " information"))
			.add(new OpSet(".beans-notifications-ok", "" + ok.getMaxHits() + " successful"))
//			.add(new OpReplace("#" + getId(), html.toHtml()))
////			.add(new OpSet(".beans-menu-notifications", html.toHtml()));
			;
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		NotificationSearchResults all = getNotifications(null);
		NotificationSearchResults err = getNotifications(NotificationType.ERROR.toString());
		NotificationSearchResults warn = getNotifications(NotificationType.WARN.toString());
		NotificationSearchResults question = getNotifications(NotificationType.QUESTION.toString());
		NotificationSearchResults info = getNotifications(NotificationType.INFO.toString());
		NotificationSearchResults ok = getNotifications(NotificationType.OK.toString());
		
		html
			.a(class_("nav-link")
					.id(getId())
					.data("toggle", "dropdown")
					.href("#")
//					.onClick(new OpComponentClick(this, "onRefresh").toStringOnclick())
					)
				.i(class_("far fa-bell"))
				._i()
				.span(class_("badge badge-warning navbar-badge beans-notifications-all"))
					.content("" + all.getMaxHits())
			._a()
			
			.div(class_("dropdown-menu dropdown-menu-lg dropdown-menu-right"))
//				.a(class_("dropdown-item")
//						.href("#")
//						.onClick(new OpComponentClick(this, "onRefresh").toStringOnclick()))
//					.i(class_("fas fa-sync mr-2"))
//					._i()
//					.write("" + all.getMaxHits() + " Notifications")
//				._a()
				.span(class_("dropdown-item dropdown-header beans-notifications-refresh")
						.onClick(new OpComponentClick(this, "onRefresh").toStringOnclick()))
					.i(class_("fas fa-sync mr-2"))
					._i()
					.write(" Refresh ")
				._span()
				
				.div(class_("dropdown-divider"))
				._div()
				.a(class_("dropdown-item")
						.href("#")
						.onClick(new OpComponentClick(NotificationsBetaContent.class,
								UUID.random(),
								"onShow",
								new Params().add("type", NotificationType.ERROR.toString())).toStringOnclick()))
					.i(class_("fas fa-bug mr-2"))
					._i()
					.span(class_("beans-notifications-error"))
						.content(" " + err.getMaxHits() + " errors")
				._a()
				
				.div(class_("dropdown-divider"))
				._div()
				.a(class_("dropdown-item")
						.href("#")
						.onClick(new OpComponentClick(NotificationsBetaContent.class,
								UUID.random(),
								"onShow",
								new Params().add("type", NotificationType.WARN.toString())).toStringOnclick()))
					.i(class_("fas fa-exclamation-circle mr-2"))
					._i()
					.span(class_("beans-notifications-warn"))
						.content(" " + warn.getMaxHits() + " warnings")
				._a()
				
				.div(class_("dropdown-divider"))
				._div()
				.a(class_("dropdown-item")
						.href("#")
						.onClick(new OpComponentClick(NotificationsBetaContent.class,
								UUID.random(),
								"onShow",
								new Params().add("type", NotificationType.QUESTION.toString())).toStringOnclick()))
					.i(class_("fas fa-question-circle mr-2"))
					._i()
					.span(class_("beans-notifications-question"))
						.content(" " + question.getMaxHits() + " questions")
				._a()
				
				.div(class_("dropdown-divider"))
				._div()
				.a(class_("dropdown-item")
						.href("#")
						.onClick(new OpComponentClick(NotificationsBetaContent.class,
								UUID.random(),
								"onShow",
								new Params().add("type", NotificationType.INFO.toString())).toStringOnclick()))
					.i(class_("fas fa-info-circle mr-2"))
					._i()
					.span(class_("beans-notifications-info"))
						.content(" " + info.getMaxHits() + " information")
				._a()
				
				.div(class_("dropdown-divider"))
				._div()
				.a(class_("dropdown-item")
						.href("#")
						.onClick(new OpComponentClick(NotificationsBetaContent.class,
								UUID.random(),
								"onShow",
								new Params().add("type", NotificationType.OK.toString())).toStringOnclick()))
					.i(class_("fas fa-check-circle mr-2"))
					._i()
					.span(class_("beans-notifications-ok"))
						.content(" " + ok.getMaxHits() + " successful")
				._a()
					
				.div(class_("dropdown-divider"))
				._div()
				.a(class_("dropdown-item dropdown-footer")
						.href("#")
						.onClick(new OpComponentClick(NotificationsBetaContent.class, "onShow").toStringOnclick()))
					.content("See All Notifications")
			._div() // dropdown-menu dropdown-menu-lg dropdown-menu-right
			;
	}
	
	private NotificationSearchResults getNotifications(String type) {
		NotificationSearchResults notifications = null;
//		if (this.notifications == null) {
			try {
				ApiCallCommand call = (ApiCallCommand) new ApiCallCommand(getSessionBean())
					.setApi("api/notification/search")
					.addParam("from", 0)
					.addParam("size", 1)
					.addParam("type", type)
					.run();
				
				notifications = new NotificationSearchResults();
				notifications.setNotification((List<Notification>) call.getResponse().getAsList("notifications", Notification.class));
				notifications.setMaxHits(call.getResponse().getAsLong("maxHits"));
				
			} catch (CLIException | IOException e) {
				LibsLogger.error(NotificationsBeansTableComponent.class, "Cannot load notifications", e);
				notifications = new NotificationSearchResults();
			}
//		}
		return notifications;
	}

}
