package net.beanscode.web.view.ui.modals;

import net.hypki.libs5.pjf.op.OpJs;

public class OpBeansInfo extends OpJs {

	public OpBeansInfo(String info) {
		super("toastr.info('" + info + "');");
	}
}
