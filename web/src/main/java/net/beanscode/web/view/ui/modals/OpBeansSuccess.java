package net.beanscode.web.view.ui.modals;

import net.hypki.libs5.pjf.op.OpJs;

public class OpBeansSuccess extends OpJs {

	public OpBeansSuccess(String success) {
		super("toastr.success('" + success + "');");
	}
}
