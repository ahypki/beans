package net.beanscode.model.users;

import java.io.IOException;

import net.beanscode.model.rights.Rights;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.jobs.Job;
import net.hypki.libs5.weblibs.user.Allowed;
import net.hypki.libs5.weblibs.user.Email;
import net.hypki.libs5.weblibs.user.User;

public class CreateAdminAccountJob extends Job {

	public CreateAdminAccountJob() {
		
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		User admin = User.getUser(new Email("admin"));
		
		if (admin == null) {
			admin = new User();
			admin.setAlreadyStatusNormal(true);
			admin.setEmail("admin");
			admin.setLogin("admin");
			admin.setPlainPasswordForHashing("admin");
			admin.setUserUUID(UUID.random());
			
			admin.save();
		}
			
		LibsLogger.debug(CreateAdminAccountJob.class, "Admin account created");
		
		Allowed allowed = new Allowed();
		allowed.setRight(Rights.ADMIN);
		allowed.setAllowed(true);
		allowed.setUserUUID(admin.getUserId());
		allowed.save();
		
		LibsLogger.debug(CreateAdminAccountJob.class, "Admin account granted ", Rights.ADMIN, " right");
	}
}
