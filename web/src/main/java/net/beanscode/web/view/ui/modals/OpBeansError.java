package net.beanscode.web.view.ui.modals;

import net.hypki.libs5.pjf.op.OpJs;

public class OpBeansError extends OpJs {

	public OpBeansError(String error) {
		super("toastr.error('" + error + "');");
	}
}
