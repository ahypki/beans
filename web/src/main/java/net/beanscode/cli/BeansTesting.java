package net.beanscode.cli;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.datasets.Table;
import net.beanscode.cli.datasets.TableGateway;
import net.beanscode.web.view.ui.BeansComponent;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.file.LazyFileIterator;
import net.hypki.libs5.utils.string.RegexUtils;
import net.hypki.libs5.utils.utils.NumberUtils;

public class BeansTesting {

	public static void main(String[] args) throws CLIException, IOException {
		
		LazyFileIterator files = new LazyFileIterator(new File("./Dragon/"), 
				false, 
				true, 
				false, 
				new FileFilter() {
					@Override
					public boolean accept(File pathname) {
						return pathname.getName().matches("snap.*h5part");
					}
				});
		String dsId = "7e8e5392edf3e62e578c3b65a5b56da5";
		for (File file : files) {
			String time = RegexUtils.firstGroup("snap\\.40\\_([\\d]+)\\.h5part", file.getName());
			double timeDbl = NumberUtils.toDouble(time);
			
			for (int i = 1; i <= 8; i++) {
				// single stars
				Table tbl = TableGateway.createTable(null, new Table()
													.setDatasetId(new UUID(dsId))
													.setName("snapshot " + time + " timenb " + (timeDbl + (i * 0.125)) + " step " + (i - 1) + " single stars")
													.addMeta("time", time)
													.addMeta("timenb", (timeDbl + (i * 0.125)))
													.addMeta("step", (i - 1))
													.addMeta("single", "1"));
				
				new ApiCallCommand((BeansComponent) null)
					.setApi("api/table/connector/add")
					.addParam("table-id", tbl.getId())
					.addParam("connector", "net.beanscode.plugin.mocca.NbodySingleConnector")
					.addParam("meta", "path=" + file.getAbsolutePath())
					.addParam("meta", "step=" + (i - 1))
					.run();
				
				
				// binaries
				tbl = TableGateway.createTable(null, new Table()
													.setDatasetId(new UUID(dsId))
													.setName("snapshot " + time + " timenb " + (timeDbl + (i * 0.125)) + " step " + (i - 1) + " binaries")
													.addMeta("time", time)
													.addMeta("timenb", (timeDbl + (i * 0.125)))
													.addMeta("step", (i - 1))
													.addMeta("binaries", "1"));
				
				new ApiCallCommand((BeansComponent) null)
					.setApi("api/table/connector/add")
					.addParam("table-id", tbl.getId())
					.addParam("connector", "net.beanscode.plugin.mocca.NbodyBinariesConnector")
					.addParam("meta", "path=" + file.getAbsolutePath())
					.addParam("meta", "step=" + (i - 1))
					.run();
				

				// mergers
				tbl = TableGateway.createTable(null, new Table()
														.setDatasetId(new UUID(dsId))
														.setName("snapshot " + time + " timenb " + (timeDbl + (i * 0.125)) + " step " + (i - 1) + " mergers")
														.addMeta("time", time)
														.addMeta("timenb", (timeDbl + (i * 0.125)))
														.addMeta("step", (i - 1))
														.addMeta("mergers", "1"));
								
				new ApiCallCommand((BeansComponent) null)
					.setApi("api/table/connector/add")
					.addParam("table-id", tbl.getId())
					.addParam("connector", "net.beanscode.plugin.mocca.NbodyMergersConnector")
					.addParam("meta", "path=" + file.getAbsolutePath())
					.addParam("meta", "step=" + (i - 1))
					.run();
			}
		}
	}
}
