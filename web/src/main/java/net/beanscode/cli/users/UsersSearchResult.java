package net.beanscode.cli.users;

import java.util.List;

import com.google.gson.annotations.Expose;

public class UsersSearchResult {

	@Expose
	private long maxHits = 0;
	
	@Expose
	private List<User> users = null;
	
	public UsersSearchResult() {
		
	}

	public long getMaxHits() {
		return maxHits;
	}

	public void setMaxHits(long maxHits) {
		this.maxHits = maxHits;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}
}
