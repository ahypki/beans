package net.beanscode.web.view.awk;

import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;
import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.name;

import java.io.IOException;

import javax.ws.rs.core.SecurityContext;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.notebooks.AwkEntry;
import net.beanscode.cli.notification.Notification;
import net.beanscode.web.view.notebook.NotebookEntryEditorPanel;
import net.beanscode.web.view.notification.NotificationLabel;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.DatasetTableSelector;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.labels.WarningLabel;
import net.hypki.libs5.pjf.components.ops.OpComponentTimer;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpReplace;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.api.APICallType;
import net.hypki.libs5.utils.url.Params;

import org.rendersnake.HtmlCanvas;
import org.rendersnake.Renderable;

public class AwkPanel extends NotebookEntryEditorPanel {
	
	public AwkPanel() {
		
	}
	
	public AwkPanel(BeansComponent parent) {
		super(parent);
	}
	
	private AwkEntry getAwkEntry() {
		return (AwkEntry) getNotebookEntry();
	}

	@Override
	public Renderable getView() {
		return new Renderable() {
			@Override
			public void renderOn(HtmlCanvas html) throws IOException {
				html
					.div()
						.span()
							.content(nullOrEmpty(getAwkEntry().getName()) ? "AWK script" : getAwkEntry().getName())
					._div()
					;
			}
		};
	}

	@Override
	protected Renderable getEditor() {
		return new Renderable() {
			@Override
			public void renderOn(HtmlCanvas html) throws IOException {
				html
					.div(
//							id(getId())
//							.
							class_("awk-panel"))
						.span()
							.content("Title")
						.input(name("name")
								.type("text")
								.add("placeholder", "Name of the AWK script")
								.class_("query-name form-control")
								.value(getAwkEntry().getName()))
//						.br()
						.render(new DatasetTableSelector(AwkPanel.this, getAwkEntry().getDsQuery(), getAwkEntry().getTbQuery(), 
								"dsQuery", "tbQuery"))
						.span()
							.content("Dataset filter")
//						.input(name("dsQuery")
//								.type("text")
//								.add("placeholder", "Dataset filter...")
//								.class_("form-control")
//								.value(getAwkEntry().getDsQuery()))
//						.span()
//							.content("Table filter")
//						.input(name("tbQuery")
//								.type("text")
//								.add("placeholder", "Table filter...")
//								.class_("form-control")
//								.value(getAwkEntry().getTbQuery()))
//						.br()
						.span()
							.content("AWK script:")
						.textarea(name("awkScript")
								.class_("form-control"))
							.content(getAwkEntry().getScript())
//						._textarea()
						.span()
							.content("Output table name")
						.input(name("outTable")
								.type("text")
								.add("placeholder", "Table name")
								.class_("form-control")
								.value(getAwkEntry().getOutTable()))
					._div()
					;
			}
		};
	}

	@Override
	protected Button getMenu() throws IOException {
		return null;
	}

	@Override
	protected OpList opOnPlay(javax.ws.rs.core.SecurityContext sc, Params params) throws IOException {
		new ApiCallCommand(this)
			.setApi("api/notebook/entry/")
			.addParam("entryId", getAwkEntry().getId().getId())
			.addParam("name", getParams().getString("name"))
			.addParam("META_SCRIPT", getParams().getString("awkScript"))
			.addParam("META_DS_QUERY", getParams().getString("dsQuery"))
			.addParam("META_TB_QUERY", getParams().getString("tbQuery"))
			.addParam("META_OUT_TABLE", getParams().getString("outTable"))
			.run();
		
		new ApiCallCommand(this)
			.setApi("api/notebook/entry/start")
			.addParam("entryId", getAwkEntry().getId().getId())
			.run();
					
		return new OpList()
			.add(new OpBeansInfo("AWK script scheduled to run"))
			.add(new OpComponentTimer(this, "onRefresh", 100, new Params().add("entryId", getAwkEntry().getId().getId())));
	}
	
	@Override
	protected OpList onRefresh(SecurityContext sc, Params params) throws IOException {
		setNotebookEntry(null);
		
		final Notification n = new ApiCallCommand(AwkPanel.this)
			.setApi(ApiCallCommand.API_NOTIFICATION_GET)
			.addParam("id", getAwkEntry().getId().getId())
			.run()
			.getResponse()
			.getAsObject(Notification.class);
				
		OpList ops = new OpList();
			
		if (n != null)
			ops
				.add(n != null, new OpReplace("#" + getId() + " .label-" + getAwkEntry().getId().getId(), 
						new NotificationLabel(n)
//							.setAdditionalCssClass("label-" + getAwkEntry().getId().getId())
							.toHtml()
					))
				.add(getAwkEntry().isRunning(), 
						new OpComponentTimer(this, "onRefresh", 3000, new Params().add("entryId", getAwkEntry().getId())));
		else
			ops
			.add(n != null, new OpReplace("#" + getId() + " .label-" + getAwkEntry().getId().getId(), 
					new WarningLabel("AWK script: " + getAwkEntry().getName())
						.setAdditionalCssClass("label-" + getAwkEntry().getId().getId())
						.toHtml()
				));
		
		return ops;
	}

	@Override
	protected OpList opOnEdit(javax.ws.rs.core.SecurityContext sc, Params params) throws IOException {
		return new OpList();
	}

	@Override
	protected OpList opOnStop(javax.ws.rs.core.SecurityContext sc, Params params) throws IOException {
		return new OpList();
	}

	@Override
	protected OpList opOnRemove(javax.ws.rs.core.SecurityContext sc, Params params) throws IOException {
		try {
			new ApiCallCommand(this)
				.setApi("api/notebook/entry/remove")
				.setApiCallType(APICallType.DELETE)
				.addParam("entryId", params.getString("entryId"))
				.run();
		} catch (IOException e) {
			LibsLogger.error(AwkPanel.class, "Cannot remove Notebook Entry", e);
		}
		
		return new OpList();
	}

}
