package net.beanscode.model.cass;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

//import net.beanscode.plugin.plot.PlotSeries;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.math.MathUtils;
import net.hypki.libs5.weblibs.WeblibsConst;

public class DataUtils {
	
	public static Object compute(final String expr, final List<String> params, final Row row) {
		if (!expr.matches("[\\w]+")) {
			String tmp = expr;
			if (params != null)
				for (String param : params) {
					tmp = tmp.replace(param, String.valueOf(row.get(param.substring(1))));
				}
			return MathUtils.eval(tmp);
		} else
			return row.get(expr);
	}

//	public static void saveToFile(final File outputFile, final Data data, final PlotSeries columns, 
//			final C3Where filters, final boolean defaultIfColumnIsMissing) throws IOException {
//		FileWriter fw = new FileWriter(outputFile, false);
//		
//		// write header in the form: # column1  column2....
//		fw.append("# ");
//		for (String col : columns.iterateColumnExpr()) {
//			fw.append(col);
//			fw.append(" \t ");
//		}
//		fw.append("\n");
//		
//		// write values
//		for (Row row : data) {
//			if (filters == null || filters.size() == 0 || row.isFullfiled(filters, defaultIfColumnIsMissing)) {
//				for (String columnExpr : columns.iterateColumnExpr()) {
//					fw.append(compute(columnExpr, columns.getParams(), row).toString());
//					fw.append(" ");
//				}
//				fw.append("\n");
//			}
//		}
//		fw.close();
//	}

	public static boolean isPlotEmpty(String tableName) throws IOException {
		return DbObject.getDatabaseProvider().isTableEmpty(WeblibsConst.KEYSPACE, tableName);
	}

	public static void removeData(final String keyspace, final String dataId) throws IOException, ValidationException {
		new RemoveDataJob(dataId).save();
//		DbObject.getDatabaseProvider().remove(keyspace, 
//				Data.COLUMN_FAMILY, 
//				new C3Where()
//						.addClause(new C3Clause(Table.COLUMN_TBID, ClauseType.EQ, dataId)));
	}
	
	
	
//	public static Iterable<Row> filter(final Iterator<Row> rows, final C3Where filters, final boolean defaultIfColumnIsMissing) {
//		return new Iterable<Row>() {
//			@Override
//			public Iterator<Row> iterator() {
//				return new Iterator<Row>() {
//					Row rNext = null;
//					
//					@Override
//					public void remove() {
//						throw new NotImplementedException();
//					}
//					
//					@Override
//					public Row next() {
//						if (rNext == null)
//							hasNext();
//						return rNext;
//					}
//					
//					@Override
//					public boolean hasNext() {
//						if (filters != null && filters.size() > 0) {
//							while (rows.hasNext()) {
//								Row r = rows.next();
//								if (r.isFullfiled(filters, defaultIfColumnIsMissing)) {
//									rNext = r;
//									return true;
//								}
//							}
//							return false;
//						} else {
//							while (rows.hasNext()) {
//								rNext = rows.next();
//								return true;
//							}
//							return false;
//						}
//					}
//				};
//			}
//		};
//	}
}
