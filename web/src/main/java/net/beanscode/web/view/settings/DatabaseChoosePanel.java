package net.beanscode.web.view.settings;

import java.io.IOException;
import java.util.List;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.plugin.DbProviderPOJO;
import net.beanscode.cli.plugin.PluginDbGetList;
import net.beanscode.cli.plugin.PluginGetList;
import net.beanscode.cli.plugin.PluginPOJO;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.modals.OpModalQuestion;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.buttons.ButtonGroup;
import net.hypki.libs5.pjf.components.labels.WarningRemark;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpInfoCookie;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpRedirect;
import net.hypki.libs5.utils.url.Params;

import org.rendersnake.HtmlAttributesFactory;
import org.rendersnake.HtmlCanvas;

public class DatabaseChoosePanel extends BeansComponent {

	public DatabaseChoosePanel() {
		
	}
	
	public DatabaseChoosePanel(BeansComponent parent) {
		super(parent);
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
//		final HttpResponse status = new Status(getSessionBean()).run();
		ApiCallCommand apiCall = (ApiCallCommand) new ApiCallCommand(getSessionBean())
			.setApi("api/admin/status")
			.run();
		
		html
			.h3()
				.content("Current database driver")
			.p()
				.content(apiCall.getResponse().getAsJson().getAsJsonObject().get("DatabaseProvider").getAsString())
			
			.h3()
				.content("Change database provider")
			.write(new WarningRemark("Remember", "It is highly recommended (before changing the database "
					+ "driver) to perform the full backup of the current database. In this way one can "
					+ "restore the data (e.g. notebooks) once new database driver will be selected.")
				.toString(), false)
			.write(new WarningRemark("Remember", "After changing the database driver "
						+ "rebuild the whole search index!")
				.toString(), false);
		
		List<PluginPOJO> plugins = new PluginGetList(getSessionBean()).run((String) null);
		
		for (DbProviderPOJO plugin : new PluginDbGetList(getSessionBean()).getDbProviders()) {
			html
				.div(HtmlAttributesFactory.style("margin: 30px;"))
					.h4()
						.content(plugin.getPlugin())
					.p()
						.content(getPluginPOJO(plugins, plugin.getPlugin()).getDescription())
//					.span()
//						.content(plugin.getClazz())
//					.br()
					.render(new ButtonGroup()
							.addButton(new Button("Select " + plugin.getPlugin())
									.add(new OpModalQuestion("Changing database driver", 
											"Do you want to change the database driver?", 
											new OpComponentClick(this, 
												"onSelect",
												new Params()
													.add("driver", plugin.getClazz()))))))
				._div();
		}
	}
	
	private OpList onSelect() throws CLIException, IOException {
		String newDriver = getParams().getString("driver");
		
		new ApiCallCommand(getSessionBean())
			.setApi("api/admin/database/provider")
			.addParam("--clazz", newDriver)
			.run();
		
		return new OpList()
			.add(new OpInfoCookie("Database driver changed"))
			.add(new OpRedirect("/login"));
	}
	
	private PluginPOJO getPluginPOJO(List<PluginPOJO> plugins, String name) {
		for (PluginPOJO pluginPOJO : plugins) {
			if (pluginPOJO.getName().equals(name))
				return pluginPOJO;
		}
		return null;
	}
	
}
