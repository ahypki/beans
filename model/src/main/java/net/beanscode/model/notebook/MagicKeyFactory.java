package net.beanscode.model.notebook;

import java.io.IOException;

import net.beanscode.model.BeansObject;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.weblibs.CacheManager;
import net.hypki.libs5.weblibs.WeblibsConst;

public class MagicKeyFactory {

	public static MagicKey getMagicKey(UUID beansObjectId) throws IOException {
		MagicKey mkCached = CacheManager
				.cacheInstance()
				.get(MagicKey.class, MagicKey.class.getSimpleName(), beansObjectId.getId());
		if (mkCached != null)
			return mkCached;
			
		MagicKey mk = (MagicKey) DbObject.getDatabaseProvider()
				.get(WeblibsConst.KEYSPACE, 
						MagicKey.COLUMN_FAMILY, 
						new C3Where()
							.addClause(new C3Clause(DbObject.COLUMN_PK, ClauseType.EQ, beansObjectId.getId())),
						DbObject.COLUMN_DATA, 
						MagicKey.class);
		
		if (mk != null)
			updateCache(mk);
		
		return mk;
	}
	
	public static void updateCache(MagicKey mk) {
		CacheManager
			.cacheInstance()
			.put(MagicKey.class.getSimpleName(), 
					mk.getBeansObjectId().getId(), 
					mk);
	}
}
