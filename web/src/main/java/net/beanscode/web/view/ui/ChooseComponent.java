package net.beanscode.web.view.ui;

import static org.rendersnake.HtmlAttributesFactory.class_;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.op.Op;
import net.hypki.libs5.pjf.op.OpAddClass;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpRemoveClass;

import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

public class ChooseComponent extends BeansComponent {
	
	@Expose
	private List<String> options = null;
	
	@Expose
	private String selectedOption = null;
	
	private OpList opsOnChange = null;
	
	public ChooseComponent() {
		
	}

	public ChooseComponent(Component parent) {
		super(parent);
	}
	
	public ChooseComponent(Component parent, String ... options) {
		super(parent);
		for (String opt : options) {
			getOptions().add(opt);
		}
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.div(class_("choose-component")
					.id(getId()));
		
		for (String option : getOptions()) {
			html
				.span(class_("choose-option " + " choose-" + option + " " + (getSelectedOption().equals(option) ? "choose-selected" : ""))
						.onClick(new OpList()
									.add(new OpRemoveClass("#" + getId() + " .choose-option", "choose-selected"))
									.add(new OpAddClass("#" + getId() + " .choose-" + option, "choose-selected"))
									.add(getOpsOnChange()
											.addParamParam("choose-selected", option))
									.toStringOnclick()))
					.content(option);
		}
		
		html
			._div();
	}

	public List<String> getOptions() {
		if (options == null)
			options = new ArrayList<>();
		return options;
	}

	public ChooseComponent setOptions(List<String> options) {
		this.options = options;
		return this;
	}

	public String getSelectedOption() {
		if (selectedOption == null)
			selectedOption = getOptions().get(0);
		return selectedOption;
	}

	public ChooseComponent setSelectedOption(String selectedOption) {
		this.selectedOption = selectedOption;
		return this;
	}

	public OpList getOpsOnChange() {
		if (opsOnChange == null)
			opsOnChange = new OpList();
		return opsOnChange;
	}

	public ChooseComponent setOpsOnChange(OpList opsOnChange) {
		this.opsOnChange = opsOnChange;
		return this;
	}
	
	public ChooseComponent addOpsOnChange(OpList opsOnChange) {
		getOpsOnChange().add(opsOnChange);
		return this;
	}
	
	public ChooseComponent addOpsOnChange(Op opOnChange) {
		getOpsOnChange().add(opOnChange);
		return this;
	}
}
