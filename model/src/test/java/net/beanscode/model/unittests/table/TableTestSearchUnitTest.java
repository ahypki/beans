package net.beanscode.model.unittests.table;

import java.io.IOException;

import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.DatasetFactory;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.dataset.TableFactory;
import net.beanscode.model.tests.BeansTestCase;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.search.SearchResults;
import net.hypki.libs5.weblibs.user.User;

import org.junit.Test;

public class TableTestSearchUnitTest extends BeansTestCase {

	@Test
	public void testSearch() throws ValidationException, IOException {
		User user = createTestUser("testuser");
		Dataset ds = new Dataset(user.getUserId(), "Demo lines")
			.addMeta("n", 40000)
			.addMeta("fracb", 0.25)
			.save();
		
		addToRemove(user);
		addToRemove(ds);
		
		addToRemove(new Table(ds, "Line square 1 25")
			.addMeta("from", 1)
			.addMeta("to", 25)
			.save());
		addToRemove(new Table(ds, "Line 1 10")
			.addMeta("from", 1)
			.addMeta("to", 10)
			.save());
		addToRemove(new Table(ds, "Line 20 30")
			.addMeta("from", 20)
			.addMeta("to", 30)
			.save());
		addToRemove(new Table(ds, "Line 5 25")
			.addMeta("from", 5)
			.addMeta("to", 25)
			.save());
		addToRemove(new Table(ds, "Bar plot points")
			.save());
		addToRemove(new Table(ds, "Parallel plot points")
			.save());
		
		runAllJobs();
		
		takeANap(2000);
		
		SearchResults<Table> res = TableFactory
				.searchTables(user.getUserId(), 
						"n == 40000", 
						"from == 1", 
						0, 
						100);
		assertTrue(res.size() == 2, "Expected to find 2 tables, but found " + res.size());
		
		res = TableFactory
				.searchTables(user.getUserId(), 
						"demo lines", 
						"to > 25", 
						0, 
						100);
		assertTrue(res.size() == 1, "Expected to find 1 tables, but found " + res.size());
	}
	
	@Test
	public void testSearch2() throws ValidationException, IOException {
		User user = createTestUser("testuser");
		Dataset ds = new Dataset(user.getUserId(), "Demo lines")
			.addMeta("n", 40000)
			.addMeta("fracb", 0.25)
			.save();
		
		addToRemove(user);
		addToRemove(ds);
		
		runAllJobs(2000);
		
		// demo lines
		SearchResults<Dataset> res = DatasetFactory
			.searchDatasets(user.getUserId(), 
					"demo", 
					0, 
					10);
		assertTrue(res.size() == 1, "Expected to find 1 Dataset, but found " + res.size());
		
		// demo_lines
		ds
			.setName("demo_lines");
		runAllJobs(1500);
		res = DatasetFactory
				.searchDatasets(user.getUserId(), 
						"demo", 
						0, 
						10);
		assertTrue(res.size() == 1, "Expected to find 1 Dataset, but found " + res.size());
		
		// demo_lines
		res = DatasetFactory
				.searchDatasets(user.getUserId(), 
						"demo lines", 
						0, 
						10);
		assertTrue(res.size() == 1, "Expected to find 1 Dataset, but found " + res.size());
		
		// demo_lines
		res = DatasetFactory
				.searchDatasets(user.getUserId(), 
						"demo_lines", 
						0, 
						10);
		assertTrue(res.size() == 1, "Expected to find 1 Dataset, but found " + res.size());
	}
}
