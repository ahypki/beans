package net.beanscode.cli.notebooks;

import static net.hypki.libs5.utils.string.StringUtilities.join;
import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;
import static net.hypki.libs5.utils.string.StringUtilities.split;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonElement;

import net.beanscode.pojo.NotebookEntry;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.collections.ArrayUtils;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.reflection.SystemUtils;
import net.hypki.libs5.utils.string.Meta;
import net.hypki.libs5.utils.string.StringUtilities;

public class PlotParallelCoordinatesEntry extends NotebookEntry {
	
	public static final String PNG = "PNG";
	public static final String PDF = "PDF";
	public static final String PS = "PS";
	public static final String ASCII = "ASCII";
	
	private static final String META_DS_QUERY 		= "META_DS_QUERY";
	private static final String META_TB_QUERY 		= "META_TB_QUERY";
	private static final String META_PLOT_STATUS	= "META_PLOT_STATUS";
	private static final String META_READ_TABLES	= "META_READ_TABLES";
	private static final String META_TITLE 			= "META_TITLE";
	private static final String META_COLUMNS 		= "META_COLUMNS";
	private static final String META_LEGENDS		= "META_LEGENDS";
	private static final String META_MIN_VALUE		= "META_MIN_VALUE";
	private static final String META_MAX_VALUE		= "META_MAX_VALUE";
	
	private PlotStatus plotStatus = null;
	
	private List<String> legends = null;
	
	public PlotParallelCoordinatesEntry() {
		
	}
	
	public PlotParallelCoordinatesEntry(Notebook notebook) {
		setNotebookId(notebook.getId());
		setUserId(notebook.getUserId());
	}
	
	@Override
	public String toString() {
		return getTitle();
	}
			
	public static String getDefaultText() {
		return SystemUtils.readFileContent(PlotParallelCoordinatesEntry.class, "PlotDefaultText");
	}

	public String getTitle() {
		return getMetaAsString(META_TITLE, null);
	}

	public void setTitle(String title) {
		setMeta(META_TITLE, title);
	}
		
	public boolean containsColumn(String colName) {
		return getColumnsAsList().contains(colName);
	}

	private List<UUID> getReadTables() {
		List<UUID> readTables = null;
		String tbStr = getMetaAsString(META_READ_TABLES, null);
		if (tbStr == null) {
			readTables = new ArrayList<>();
		} else {
			readTables = JsonUtils.readList(tbStr, UUID.class);
		}
		return readTables;
	}

	private void setReadTables(List<UUID> readTables) {
		setMeta(META_READ_TABLES, JsonUtils.objectToStringPretty(readTables));
	}
	
	public PlotStatus getPlotStatus() {
		if (plotStatus == null) {
			String tmp = getMetaAsString(META_PLOT_STATUS, null);
			if (tmp == null) {
				plotStatus = new PlotStatus();
				setPlotStatus(plotStatus);
			} else { 
				plotStatus = JsonUtils.fromJson(tmp, PlotStatus.class);
			}
		}
		return plotStatus;
	}

	private void setPlotStatus(PlotStatus plotStatus) {
		setMeta(META_PLOT_STATUS, JsonUtils.objectToStringPretty(plotStatus));
		this.plotStatus = plotStatus;
	}

	public String getDsQuery() {
		return getMetaAsString(META_DS_QUERY, null);
	}

	public void setDsQuery(String dsQuery) throws IOException {
		setMeta(META_DS_QUERY, dsQuery);
	}

	public String getTbQuery() {
		return getMetaAsString(META_TB_QUERY, null);
	}

	public void setTbQuery(String tbQuery) {
		setMeta(META_TB_QUERY, tbQuery);
	}
	
	public boolean getMetaAsBoolean(String metaName, boolean defaultValue) {
		Meta m = getMeta().get(metaName);
		if (m != null)
			return m.getAsBoolean();
		return defaultValue;
	}
	
	public PlotParallelCoordinatesEntry setMeta(String name, boolean value) {
		getMeta().add(name, value);
		return this;
	}

	public String getColumns() {
		return getMetaAsString(META_COLUMNS, null);
	}
	

	public PlotParallelCoordinatesEntry setColumns(List<String> columns) {
		setColumns(join(columns.toArray(), ':'));
		return this;
	}

	public PlotParallelCoordinatesEntry setColumns(String columns) {
		setMeta(META_COLUMNS, columns);
		return this;
	}

	public List<String> getColumnsAsList() {
		if (nullOrEmpty(getColumns()))
			return new ArrayList<String>();
		
		List<String> columns = new ArrayList<>();
		for (JsonElement c : JsonUtils.parseJson(getColumns()).getAsJsonArray())
			columns.add(c.getAsString());
		
		return columns;
	}

	public List<String> getLegends() {
		if (this.legends == null) {
			String tbStr = getMetaAsString(META_LEGENDS, null);
			if (tbStr == null) {
				legends = new ArrayList<>();
			} else {
				legends = JsonUtils.readList(tbStr, String.class);
			}
		}
		return legends;
	}

	public void setLegends(List<String> legends) {
		this.legends = legends;
		setMeta(META_LEGENDS, JsonUtils.objectToStringPretty(legends));
	}
	
	public Object getMaxValue(int i) {
		return i < getMaxValue().size() ? getMaxValue().get(i) : null;
	}

	public Object getMinValue(int i) {
		return i < getMinValue().size() ? getMinValue().get(i) : null;
	}
	
	public List<Object> getMinValue() {
		List<Object> minValue = null;
		String tbStr = getMetaAsString(META_MIN_VALUE, null);
		if (nullOrEmpty(tbStr)) {
			minValue = new ArrayList<>();
		} else {
			minValue = JsonUtils.readList(tbStr, Object.class);
		}
		return minValue;
	}

	public void setMinValue(List<Object> minValue) {
		setMeta(META_MIN_VALUE, JsonUtils.objectToStringPretty(minValue));
	}

	public List<Object> getMaxValue() {
		List<Object> maxValue = null;
		String tbStr = getMetaAsString(META_MAX_VALUE, null);
		if (nullOrEmpty(tbStr)) {
			maxValue = new ArrayList<>();
		} else {
			maxValue = JsonUtils.readList(tbStr, Object.class);
		}
		return maxValue;
	}

	public void setMaxValue(List<Object> maxValue) {
		setMeta(META_MAX_VALUE, JsonUtils.objectToStringPretty(maxValue));
	}
}
