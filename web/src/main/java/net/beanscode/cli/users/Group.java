package net.beanscode.cli.users;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.beanscode.cli.api.ApiCallCommand;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.jersey.SessionBean;
import net.hypki.libs5.utils.LibsLogger;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class Group {

	@Expose
	@NotNull
	@AssertValid
	private UUID id = null;
	
	@Expose
	@NotNull
	@AssertValid
	private UUID owner = null;
	
	@Expose
	@NotNull
	@NotEmpty
	private String name = null;
	
	@Expose
	@NotNull
	@NotEmpty
	private String description = null;
	
	@Expose
	@NotNull
	@NotEmpty
	private List<UUID> userIds = null;
	
	public Group() {
		setId(UUID.random());
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	private List<UUID> getUserIds() {
		if (userIds == null)
			userIds = new ArrayList<>();
		return userIds;
	}

	private void setUserIds(List<UUID> userIds) {
		this.userIds = userIds;
	}
	
	public Iterable<UUID> iterateUserIds() {
		return getUserIds();
	}
	
	public Iterable<User> iterateUsers(final SessionBean sessionBean) {
		return new Iterable<User>() {
			@Override
			public Iterator<User> iterator() {
				return new Iterator<User>() {
					private Iterator<UUID> userIdIter = getUserIds().iterator();
					
					@Override
					public User next() {
						UUID userId = userIdIter.next();
						try {
							return new ApiCallCommand(sessionBean)
										.setApi("api/user/get")
										.addParam("userId", userId.getId())
										.run()
										.getResponse()
										.getAsObject(User.class);
						} catch (Exception e) {
							LibsLogger.error(Group.class, "Cannot get User for ID " + userId, e);
							return null;
						}
					}
					
					@Override
					public boolean hasNext() {
						return userIdIter.hasNext();
					}
				};
			}
		};
	}

	public UUID getOwner() {
		return owner;
	}

	public void setOwner(UUID owner) {
		this.owner = owner;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getNameNotNull() {
		return name == null ? "" : name;
	}
	
	public String getDescriptionNotNull() {
		return description == null ? "" : description;
	}
}
