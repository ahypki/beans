package net.beanscode.cli.notebooks;

import java.io.IOException;

import net.beanscode.pojo.NotebookEntry;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;

public class AwkEntry extends NotebookEntry {
	
	private static final String META_SCRIPT 		= "META_SCRIPT";
	private static final String META_DS_QUERY 		= "META_DS_QUERY";
	private static final String META_TB_QUERY 		= "META_TB_QUERY";
	private static final String META_OUT_TABLE 		= "META_OUT_TABLE";
	private static final String META_RUNNING 		= "META_RUNNING";
	private static final String META_OUTPUT_TABLEID = "META_OUTPUT_TABLEID";
		
	public AwkEntry() {
		
	}
	
	public String getScript() {
		return getMetaAsString(META_SCRIPT, null);
	}

	public AwkEntry setScript(String script) {
		setMeta(META_SCRIPT, script);
		return this;
	}

	public String getDsQuery() {
		return getMetaAsString(META_DS_QUERY, null);
	}

	public AwkEntry setDsQuery(String dsQuery) {
		setMeta(META_DS_QUERY, dsQuery);
		return this;
	}

	public String getTbQuery() {
		return getMetaAsString(META_TB_QUERY, null);
	}

	public AwkEntry setTbQuery(String tbQuery) {
		setMeta(META_TB_QUERY, tbQuery);
		return this;
	}
	
	public void setRunning(boolean running) {
		setMeta(META_RUNNING, running);
	}

	public boolean isRunning() {
		return getMetaAsBoolean(META_RUNNING, false);
	}
	
	public String getOutTable() {
		return getMetaAsString(META_OUT_TABLE, null);
	}

	public AwkEntry setOutTable(String outTable) {
		setMeta(META_OUT_TABLE, outTable);
		return this;
	}
	
	public UUID getOutputTableId() {
		return getMetaAsUUID(META_OUTPUT_TABLEID, UUID.random());
	}

	public AwkEntry setOutputTableId(UUID outputTableId) {
		setMeta(META_OUTPUT_TABLEID, outputTableId != null ? outputTableId.getId() : null);
		return this;
	}

}
