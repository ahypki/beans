package net.beanscode.web.view.settings;

import java.io.IOException;

import net.beanscode.web.view.ui.BeansComponent;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpSet;
import net.hypki.libs5.utils.url.Params;

import org.rendersnake.HtmlCanvas;

public class DatabaseContent extends BeansComponent {

	public DatabaseContent() {
		
	}
	
	public OpList showContent(Params params) throws IOException {
		HtmlCanvas content = new HtmlCanvas()
		
			.render(new DatabaseChoosePanel(this))
			
			;
		
		return new OpList()
			.add(new OpSet("#page-title", "Database settings"))
			.add(new OpSet("#main", content.toHtml()))
			;
	}
		
	
}
