package net.beanscode.web.view.notebook;

import static java.lang.String.format;
import static net.hypki.libs5.pjf.OpsFactory.opsCall;
import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.data;
import static org.rendersnake.HtmlAttributesFactory.href;
import static org.rendersnake.HtmlAttributesFactory.id;
import static org.rendersnake.HtmlAttributesFactory.name;
import static org.rendersnake.HtmlAttributesFactory.type;

import java.io.IOException;

import net.beanscode.cli.notebooks.TableEntry;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.op.OpAjaxJs;
import net.hypki.libs5.pjf.op.OpJs;
import net.hypki.libs5.pjf.op.OpResize;

import org.rendersnake.HtmlCanvas;

public class TablePanel extends Component {
	
	private TableEntry tableEntry = null;
	
	public TablePanel() {
		
	}

	public TablePanel(TableEntry tableEntry) {
		setTableEntry(tableEntry);
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
		.div(class_("query-panel item tableEntry-panel")
				.add("style", "width: 470px; height: 300px;")
				.id(getId()))
		    .div(class_("box dark"))
				.input(type("hidden")
						.id("notebookId")
						.name("notebookId")
						.value(getTableEntry().getNotebookId().getId()))
				.input(type("hidden")
						.id("entryId")
						.name("entryId")
						.value(getTableEntry().getId().getId()))
		        .header()
		        	.div(class_("toolbar toolbar-left"))
		                .ul(class_("nav"))
		                	.li()
		                		.div(class_("btn-group"))
		                		// default size: width: 470px; height: 300px;
		                        .a(class_("btn btn-default btn-xs minimize-box").add("data-toggle", "collapse").href("#div-" + getId())
		                        		.onClick(opsCall(new OpJs("if ($('#div-" + getId() + "').attr('style').contains('height: auto;')) {" +
		                        				"$('#" + getId() + "').width('470'); $('#" + getId() + "').height('300');" +
                        						"$('#div-" + getId() + "').attr('style', '');" +
                        						"} else {" +
                        						"$('#" + getId() + "').width(''); $('#" + getId() + "').height('');" +
                        						"}" //+
//                        						"$('#main').masonry();"
                        						, 500))))
		                            .i(class_("fa fa-chevron-up"))
					                ._i()
		                        ._a()
//		                    ._li()
//		                    .li()
		                        .a(class_("btn btn-default btn-xs minimize-box")
		                        		.onClick(opsCall(new OpResize("#" + getId(), 150, 100)
//			                        		opResize("#" + panelId, 70, 50),
//			                        		opResize("#" + panelId + " div.figure", 70, 50),
//			                        		opJs("$('#main').masonry('reloadItems'); $('#main').masonry(); d3Plots['" + query.getId() + "'].resize();", 500))))
//			                        		opJs("d3Plots['" + query.getId() + "'].resize();", 500)
			                        		)))
		                            .i(class_("fa fa-plus"))
		                            ._i()
		                        ._a()
//		                    ._li()
//		                    .li()
		                        .a(class_("btn btn-default btn-xs minimize-box")
		                        		.onClick(opsCall(new OpResize("#" + getId(), -150, -100)
//			                        		opResize("#" + panelId, -70, -50),
//			                        		opResize("#" + panelId + " div.figure", -70, -50),
//			                        		opJs("$('#main').masonry('reloadItems'); $('#main').masonry(); d3Plots['" + query.getId() + "'].resize();", 500))))
//			                        		opJs("d3Plots['" + query.getId() + "'].resize();", 500)
			                        		)))
		                            .i(class_("fa fa-minus"))
		                            ._i()
		                        ._a()
		                        ._div()
		                    ._li()
		                ._ul()
		            ._div()
//		            .div(class_("icons"))
//		                
//		            ._div()
		            .h5()
		            	.render(new QueryTitle(getTableEntry()))
	            	._h5()
//		                .content(getQuery().getName())
		            .div(class_("toolbar"))
		                .ul(class_("nav"))
//		                	.render(getMenu())
		                	.li(class_("active"))
		                        .a(href("#pane1" + getId()).data("toggle", "tab"))
		                            .i(class_("fa fa-edit"))
					                ._i()
		                        ._a()
		                    ._li()
		        			.li(class_(""))
			        			.a(href("#pane2" + getId()).data("toggle", "tab"))
			        				.i(class_("glyphicon glyphicon-picture"))
					                ._i()
				                ._a()
			        		._li()
			        		.li(class_(""))
			        			.a(href("#pane3" + getId()).data("toggle", "tab"))
			        				.i(class_("glyphicon glyphicon-info-sign"))
					                ._i()
				                ._a()
			        		._li()
		                    .li(class_("dropdown"))
		                        .a(data("toggle", "dropdown"))
		                            .i(class_("fa fa-cog"))
					                ._i()
		                        ._a()
		                        .ul(class_("dropdown-menu"))
//		                        	.li(class_(""))
//					        			.a(
//					        				.i(class_("fa fa-info"))
//							                ._i()
//							                .span()
//							                	.content(" Info")
//						                ._a()
//					        		._li()
					        		.li(class_(""))
					        			.a()
					        				.i(class_("glyphicon glyphicon-remove"))
							                ._i()
							                .span()
							                	.content(" Remove")
						                ._a()
					        		._li()
		                        ._ul()
		                    ._li()
		                ._ul()
		            ._div()
		        ._header()
	        ._div()
//	        .div(class_("accordion-body collapse in body"))
//	        	.content("Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher retro keffiyeh dreamcatcher synth. Cosby sweater eu banh mi, qui irure terry richardson ex squid. Aliquip placeat salvia cillum iphone. Seitan aliquip quis cardigan american apparel, butcher voluptate nisi qui.")
	        .div(class_("tabbable accordion-body collapse in body")
					.id("div-" + getId()))
//		        .ul(class_("nav nav-tabs"))
//		        		.li(class_("active"))
//		        			.a(href("#pane1" + id).data("toggle", "tab"))
//		        				.content("Query")
//		        		._li()
//	        			.li(class_(""))
//		        			.a(href("#pane2" + id).data("toggle", "tab"))
//		        				.content("Plot")
//		        		._li()
//		        ._ul()
		        .div(class_("tab-content"))
		        	.div(id("pane1" + getId()).class_("tab-pane active query-tab"))
		        		.div(class_("btn-group"))
							.button(class_("btn btn-default")
										.onClick(opsCall(new OpAjaxJs("api/query/run", format("$('#%s :input').serialize()", getId())))))
								.content("Run!")
							.button(class_("btn btn-default")
										.onClick(opsCall(new OpAjaxJs("api/query/edit", format("$('#%s :input').serialize()", getId())))))
								.content("Save only")
							.button(class_("btn btn-default")
										.onClick(opsCall(new OpAjaxJs("api/query/status", format("$('#%s :input').serialize()", getId())))))
								.i(class_("fa fa-refresh"))
								._i()
							._button()
						._div()
						.div(class_("info-right"))
//							.button(class_("btn btn-default")
//									.onClick(""))
//								.i(class_("glyphicon glyphicon-refresh"))
//								._i()
//							._button()
							.span(class_("label label-default")
									.id("query-status"))
								.content("")
						._div()
						.textarea(name("query"))
							.content(getTableEntry().getQuery())
		        	._div()
		        	.div(id("pane2" + getId()).class_("tab-pane plot-tab"))
		        		.div(class_("btn-group"))
							.button(class_("btn btn-default")
										.onClick(opsCall(new OpAjaxJs("api/plot/refresh", format("$('#%s :input').serialize()", getId())))))
								.content("Refresh")
							.button(class_("btn btn-default")
										.id("button-previous")
										.onClick(opsCall(new OpAjaxJs("api/plot/refresh", format("'previous=true&' + ($('#%s :input').serialize())", getId())))))
								.i(class_("glyphicon glyphicon-backward"))
								._i()
							._button()
							.button(class_("btn btn-default")
										.id("button-next")
										.onClick(opsCall(new OpAjaxJs("api/plot/refresh", format("'next=true&' + ($('#%s :input').serialize())", getId())))))
								.i(class_("glyphicon glyphicon-forward"))
								._i()
							._button()
							.button(class_("btn btn-default")
										.id("button-refresh")
										.onClick(opsCall(new OpJs(""))))
								.i(class_("glyphicon glyphicon-refresh"))
								._i()
							._button()
						._div()
						.div(class_("figure")
								.id("figure-")
//								.add("style", "height: 200px;")
								)
						._div()
		        	._div()
		        	.div(id("pane3" + getId()).class_("tab-pane info-tab"))
		        		.dl(class_("dl-horizontal"))
		        			.dt()
		        				.content("Owner")
	        				.dd()
	        					.content("")
        					.dt()
		        				.content("Creation date")
	        				.dd()
	        					.content(getTableEntry().getCreate().toStringHuman())
		        		._dl()
		        	._div()
		        ._div()
	        ._div()
        ._div()
        ;
	}

	public TableEntry getTableEntry() {
		return tableEntry;
	}

	public void setTableEntry(TableEntry tableEntry) {
		this.tableEntry = tableEntry;
	}
}
