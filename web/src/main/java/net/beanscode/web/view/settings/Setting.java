package net.beanscode.web.view.settings;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.users.User;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.jersey.SessionBean;
import net.hypki.libs5.utils.date.SimpleDate;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class Setting {
	
	@Expose
	@NotNull
//	@AssertValid
	private String id = null;
	
	@Expose
//	@NotNull
	@AssertValid
	private UUID userId = null;
	
	@Expose
	@NotNull
	@NotEmpty
	private String name = null;
	
	@Expose
	private String description = null;
	
	@Expose
	@AssertValid
	private List<SettingValue> values = null;
	
	@Expose
	private SimpleDate lastEdit = null;
	
	@Expose
	private boolean system = true;
	
	private User userCache = null;
	
	public Setting() {
		
	}

	public String getId() {
		return id;
	}

	public Setting setId(String id) {
		this.id = id;
		return this;
	}

	public UUID getUserId() {
		return userId;
	}

	public Setting setUserId(UUID userId) {
		this.userId = userId;
		return this;
	}

	public String getName() {
		return name;
	}

	public Setting setName(String name) {
		this.name = name;
		return this;
	}

	public String getDescription() {
		return description;
	}

	public Setting setDescription(String description) {
		this.description = description;
		return this;
	}

	public List<SettingValue> getValues() {
		if (values == null)
			values = new ArrayList<SettingValue>();
		return values;
	}

	private void setValues(List<SettingValue> values) {
		this.values = values;
	}

	public SimpleDate getLastEdit() {
		return lastEdit;
	}

	public void setLastEdit(SimpleDate lastEdit) {
		this.lastEdit = lastEdit;
	}
	
	public SettingValue getValue(String name) {
		for (SettingValue val : getValues()) {
			if (val.getName().equals(name))
				return val;
		}
		return null;
	}
	
	public String getValueAsString(String name) {
		SettingValue sv = getValue(name);
		return sv != null ? sv.getValueAsString() : null;
	}

	public boolean isSystem() {
		return system;
	}

	public void setSystem(boolean system) {
		this.system = system;
	}

	public User getUser(SessionBean sb) throws CLIException, IOException {
		if (userCache == null && getUserId() != null) {
			userCache = new ApiCallCommand(sb)
							.setApi("api/user/get")
							.addParam("userId", getUserId().getId())
							.run()
							.getResponse()
							.getAsObject(User.class);			
		}
		return userCache;
	}
}
