package net.beanscode.model.notebook.autoupdate;

import com.google.gson.annotations.Expose;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.sf.oval.constraint.NotNull;

public class AutoUpdateSettings {

	@Expose
	@NotNull
	private AutoUpdateOption option = null;
	
	public AutoUpdateSettings() {
		setOption(AutoUpdateOption.NO_CONFIRMATION_NEEDED);
	}
	
	public AutoUpdateSettings(AutoUpdateOption option) {
		setOption(option);
	}

	public AutoUpdateOption getOption() {
		return option;
	}

	public AutoUpdateSettings setOption(AutoUpdateOption option) {
		this.option = option;
		return this;
	}

	public void setOption(String autoUpdateOption) throws ValidationException {
		if (autoUpdateOption.equalsIgnoreCase(AutoUpdateOption.NO_CONFIRMATION_NEEDED.name()))
			setOption(AutoUpdateOption.NO_CONFIRMATION_NEEDED);
		else if (autoUpdateOption.equalsIgnoreCase(AutoUpdateOption.IGNORE.name()))
			setOption(AutoUpdateOption.IGNORE);
		else if (autoUpdateOption.equalsIgnoreCase(AutoUpdateOption.CONFIRMATION_NEEDED.name()))
			setOption(AutoUpdateOption.CONFIRMATION_NEEDED);
		else
			throw new ValidationException("Unknown option for auto-updates: " + autoUpdateOption);
	}
}
