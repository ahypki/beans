package net.beanscode.web.view.notebook;

import static net.beanscode.cli.BeansCLIConst.MAGIC_KEY;
import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;
import static org.rendersnake.HtmlAttributesFactory.class_;

import java.io.IOException;

import javax.ws.rs.core.SecurityContext;

import net.beanscode.cli.BeansCLIConst;
import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.notebooks.Notebook;
import net.beanscode.cli.notebooks.NotebookEntryGateway;
import net.beanscode.pojo.NotebookEntry;
import net.beanscode.web.BeansWebConst;
import net.beanscode.web.beta.BetaContent;
import net.beanscode.web.beta.DashboardContent;
import net.beanscode.web.view.dataset.MetaTableComponent;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.BetaButtonGroup;
import net.beanscode.web.view.ui.Link;
import net.beanscode.web.view.ui.OpComponentDialogOpen;
import net.beanscode.web.view.ui.SpinnerLabel;
import net.beanscode.web.view.ui.modals.InputType;
import net.beanscode.web.view.ui.modals.ModalWindow;
import net.beanscode.web.view.ui.modals.OpBeansError;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.beanscode.web.view.ui.modals.OpModal;
import net.beanscode.web.view.ui.modals.OpModalQuestion;
import net.beanscode.web.view.ui.remarks.ErrorRemark;
import net.beanscode.web.view.users.PermissionBetaTableComponent;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.buttons.ButtonSize;
import net.hypki.libs5.pjf.components.labels.Label;
import net.hypki.libs5.pjf.components.labels.MessageType;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.Op;
import net.hypki.libs5.pjf.op.OpAppend;
import net.hypki.libs5.pjf.op.OpJs;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpRemove;
import net.hypki.libs5.pjf.op.OpSet;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.url.Params;

import org.rendersnake.HtmlCanvas;

import com.google.gson.JsonElement;

public class NotebookContent extends BetaContent {
	
	private Notebook notebook = null;
	
	public NotebookContent() {
		
	}
		
	public NotebookContent(BeansComponent parent) {
		super(parent);
	}
	
	public NotebookContent(BeansComponent parent, Notebook notebook) {
		super(parent);
		setNotebook(notebook);
		
		if (notebook != null)
			getParams().add("notebookId", notebook.getId().getId());
	}
	
	@Override
	protected String getAjaxPath() {
		return NotebookPage.PATH + "/" + getNotebook().getId();
	}
		
	@Override
	protected void renderHeader(HtmlCanvas html) throws IOException {
		if (getNotebook() == null) {
			html
				.render(new ErrorRemark("Error", "Cannot find notebook, or insufficient rihts"));
			return;
		}
		
		html
			.h4(class_("notebook-title notebook-title-" + getNotebook().getId()))
				.i(class_("fas fa-book"))
				._i()
				.span()
					.content(" ")
				.span(class_("name"))
					.content(getNotebook().getName())
			._h4()
				;
		
//			.i(class_("fa fa-book"))
//			.span(id("notebook-title-" + getNotebook().getId())
//					.onDblclick(opsCall(opEditName(getNotebook(), getId()))))
//				.content(" " + getNotebook().getName())
//			.toHtml();
	}
	
	@Override
	protected void renderContent(HtmlCanvas html) throws IOException {
		NotebookPanel np = new NotebookPanel(this, getNotebook());
		
		html
			.render(np)
			.write(np.getOpList().toStringAdhock(), false)
			// TODO 
//			.write(new OpJs("processScroll()").toStringAdhock(), false)
//			.write(new OpJs("observer.observe(document.querySelector(\"#notebook-entries\"))").toStringAdhock(), false)
			;
	}
	

	public OpList create(SecurityContext sc, Params params) throws IOException, ValidationException {
		String name = params.getString("name");
		
		if (nullOrEmpty(name))
			name = BeansWebConst.DEFAULT_NOTEBOOK_NAME;
		
		Notebook notebook = new ApiCallCommand(this)
								.setApi("api/notebook/create")
								.addParam("name", name)
								.run()
								.getResponse()
								.getAsObject(Notebook.class);
		
//		Notebook notebook = JsonUtils.fromJson(new NotebookRest().create(sc, name), Notebook.class);
		
		return new OpList()
			.add(new OpBeansInfo("Notebook created"))
			.add(new NotebookContent(this, notebook)
//				.setParams(new Params()
//						.add("notebookId", notebook.getId().getId()))
				.onShow());
	}
	
	
	public Notebook getNotebook() {
		if (notebook == null) {
			final String notebookId = getParams().getString("notebookId");
			if (notebookId != null)
				try {
					notebook = new ApiCallCommand(this)
									.setApi("api/notebook/get")
									.addParam("notebookId", notebookId)
									.addParam(MAGIC_KEY, getParams().get(MAGIC_KEY))
									.run()
									.getResponse()
									.getAsObject(Notebook.class);
				} catch (IOException e) {
					LibsLogger.error(NotebookContent.class, "Cannot get notebook from DB", e);
				}
		}
		return notebook;
	}

	public void setNotebook(Notebook notebook) {
		this.notebook = notebook;
	}
	
	@Override
	protected BetaButtonGroup getMenu() throws IOException {
		return null;
	}
	
	public OpList onRearrange() throws IOException {
		return new OpList()
			.add(new NotebookRearrangeEntriesBetaContent(this, getNotebook()).onShow());
	}
	
	
	public OpList onPermissionsOpen() throws IOException, ValidationException {
		ModalWindow modal = new ModalWindow(this);
		modal.setTitle("Permissions");
		modal
			.getContent()
				.render(new PermissionBetaTableComponent(this, getNotebook().getId(), getNotebook().getClass().getSimpleName()));
		modal.add(new Button("Close")
				.add(modal.onClose()));
		return new OpList()
			.add(new OpModal(modal));
	}
	
	private OpList onReload() throws IOException, ValidationException {
		OpList op = new OpList();
		
		new ApiCallCommand(this)
			.setApi("api/notebook/reload")
			.addParam("notebookId", getNotebook().getId().getId())
			.run();
		
		op
			.add(new OpComponentClick(NotebookContent.class, 
					UUID.random(),
					"onShow",
					new Params().add("notebookId", getNotebook().getId())));
		
//		for (NotebookEntry entry : getNotebook().iterateEntries(getSessionBean())) {
//			if (entry instanceof Plot) {				
//				op.add(new OpComponentClick(PlotPanel.class, 
//						entry.getId(), 
//						"onRefresh", 
//						new Params().add("plotId",  entry.getId().getId())));
//			}
//		}
		
		return op.add(new OpBeansInfo("All entries are scheduled to reload, be patient."));
	}
	
	
	
	public OpList onEditMeta() throws IOException, ValidationException {
		ModalWindow modal = new ModalWindow(this);
		modal.setTitle("Editing meta parameters");
		modal
			.getContent()
				.render(new MetaTableComponent(modal, getNotebook()));
//		modal.add(new Button("Save")
//				.add(new OpComponentClick(this.getClass(), 
//						modal.getId(), 
//						"onMetaSave",
//						new Params()
//							.add("notebookId", getNotebook().getId().getId())))
//				.add(modal.onClose()));
		modal.add(new Button("Close")
				.add(modal.onClose()));
		return new OpList()
			.add(new OpModal(modal));
	}
	
	public OpList onInsert() throws IOException, ValidationException {
		ModalWindow modal = new ModalWindow(this);
		modal.setTitle("Inserting new entry");
		modal
			.getContent()
			.render(new AddNewEntryPanel(this, "onAddEntry", getNotebook().getId()));
		modal.add(new Button("Close")
				.add(modal.onClose()));
		return new OpList()
			.add(new OpModal(modal));
	}
	
	private OpList onAddEntry() throws IOException, ValidationException {
		final String destinationNotebookId = getParams().getString("destinationNotebookId");
		final String destinationAfterEntryId = getParams().getString("destinationAfterEntryId");
		
		final String sourceNotebookId = getParams().getString("sourceNotebookId");
		String sourceNotebookEntryClass = getParams().getString("sourceNotebookEntryClass");
		final String sourceEntryId = getParams().getString("sourceEntryId");
			
		if (sourceNotebookId != null 
				&& nullOrEmpty(sourceNotebookEntryClass)
				&& nullOrEmpty(sourceEntryId)) {
			
			// clone whole notebook
			OpList opList = new OpList();
			net.hypki.libs5.utils.url.HttpResponse resp = new ApiCallCommand(getSessionBean())
				.setApi("api/notebook/add")
				.addParam("--notebookId", destinationNotebookId)
				.addParam("--selectedNotebookId", sourceNotebookId)
				.run()
				.getResponse();
			
			if (resp.getAsJson() != null) {
				for (JsonElement je : resp.getAsJson().getAsJsonObject().get("entries").getAsJsonArray()) {
					NotebookEntry ne = NotebookEntryGateway.getNotebookEntry(new ApiCallCommand(this)
										.setApi("api/notebook/entry/get")
										.addParam("entryId", je.getAsString())
										.run()
										.getResponse()
										.getAsJson()
										.getAsJsonObject());
	//				
	//				getNotebook().getEntries().add(ne.getId());
	//				
					NotebookEntryEditorPanel nePanel = NotebookEntryEditorFactory.getEditorPanel(this, ne.getClass());
					nePanel.setNotebookEntry(ne);
					nePanel.setParams(getParams());
					nePanel.setSessionBean(getSecurityContext());
	
					opList.add(new OpAppend("#notebook-entries", nePanel.toHtml()));
				}
			}
			
			return opList
					.add(new OpBeansInfo("Entries added successfully"))
//					.add(new OpDialogClose())
					;
			
		} else if (notEmpty(sourceNotebookEntryClass)) {
			
			try {
				// adding new entry (not a clone)
				net.hypki.libs5.utils.url.HttpResponse resp = new ApiCallCommand(getSessionBean())
					.setApi("api/notebook/entry/add")
					.addParam("--notebookId", destinationNotebookId)
					.addParam("--notebookEntryClass", sourceNotebookEntryClass)
//					.addParam("--refEntryId", destinationAfterEntryId)
//					.addParam("--sourceEntryId", sourceEntryId)
					.run()
					.getResponse();
				
				if (nullOrEmpty(sourceNotebookEntryClass)) {
					NotebookEntry sourceEntry = NotebookEntryGateway.getNotebookEntry(new ApiCallCommand(this)
													.setApi("api/notebook/entry/get")
													.addParam("entryId", sourceEntryId)
													.run()
													.getResponse()
													.getAsJson()
													.getAsJsonObject());
					sourceNotebookEntryClass = sourceEntry.getClass().getName();
				}
				
				NotebookEntry ne = (NotebookEntry) resp.getAsObject(Class.forName(sourceNotebookEntryClass));
				
				getNotebook().getEntries().add(ne.getId());
				
				NotebookEntryEditorPanel nePanel = NotebookEntryEditorFactory.getEditorPanel(this, ne.getClass());
				nePanel.setNotebookEntry(ne);
				nePanel.setParams(getParams());
				nePanel.setSessionBean(getSecurityContext());
				
				return new OpList()
					.add(new OpAppend("#notebook-entries", nePanel.toHtml()))
					.add(new OpBeansInfo("Entry added successfully"))
//					.add(new OpDialogClose())
					;
			} catch (CLIException | ClassNotFoundException e) {
				LibsLogger.error(NotebookContent.class, "Cannot create instance of the Entry", e);
				
				return new OpList()
					.add(new OpBeansError("Cannot add/clone the entry"));
			}
			
		} else if (notEmpty(sourceEntryId)) {
			
			try {
				// cloning an existing entry
				net.hypki.libs5.utils.url.HttpResponse resp = new ApiCallCommand(getSessionBean())
					.setApi("api/notebook/entry/clone/entry")
					.addParam("--notebookId", destinationNotebookId)
					.addParam("--sourceEntryId", sourceEntryId)
//					.addParam("--refEntryId", destinationAfterEntryId)
//					.addParam("--sourceEntryId", sourceEntryId)
					.run()
					.getResponse();
				
				if (nullOrEmpty(sourceNotebookEntryClass)) {
					NotebookEntry sourceEntry = NotebookEntryGateway.getNotebookEntry(new ApiCallCommand(this)
													.setApi("api/notebook/entry/get")
													.addParam("entryId", sourceEntryId)
													.run()
													.getResponse()
													.getAsJson()
													.getAsJsonObject());
					sourceNotebookEntryClass = sourceEntry.getClass().getName();
				}
								
				NotebookEntry ne = (NotebookEntry) resp.getAsObject(Class.forName(sourceNotebookEntryClass));
				
				getNotebook().getEntries().add(ne.getId());
				
				NotebookEntryEditorPanel nePanel = NotebookEntryEditorFactory.getEditorPanel(this, ne.getClass());
				nePanel.setNotebookEntry(ne);
				nePanel.setParams(getParams());
				nePanel.setSessionBean(getSecurityContext());
				
				return new OpList()
					.add(new OpAppend("#notebook-entries", nePanel.toHtml()))
					.add(new OpBeansInfo("Entry added successfully"))
//					.add(new OpDialogClose())
					;
			} catch (CLIException | ClassNotFoundException e) {
				LibsLogger.error(NotebookContent.class, "Cannot create instance of the Entry", e);
				
				return new OpList()
					.add(new OpBeansError("Cannot add/clone the entry"));
			}
			
		} else {
			
			LibsLogger.error(NotebookContent.class, "Unknown case for adding/cloning a new entry");
			
			return new OpList()
				.add(new OpBeansError("Cannot add/clone the entry"));
						
		}
	}
	
	
	public OpList onClearNotebook() throws IOException, ValidationException {
		new ApiCallCommand(getSessionBean())
			.setApi("api/notebook/clear")
			.addParam("notebookId", getParams().getString("notebookId"))
			.run();

		return new OpList()
			.add(new OpRemove("#notebook-entries .notebook-entry"))
			.add(new OpBeansInfo("Notebook cleared"))
//			.add(new OpComponentClick(NotebookContent.class, UUID.random(), "showContent", 
//					new Params()
//						.add("userId", getUserId())
//						.add("notebookId", getParams().getString("notebookId"))))
			;
	}
	
	public OpList onRemoveNotebook() throws IOException, ValidationException {
		new ApiCallCommand(getSessionBean())
			.setApi("api/notebook/remove")
			.addParam("notebookId", getParams().getString("notebookId"))
			.run();

		return new OpList()
			.add(new OpRemove("#notebook-entries .notebook-entry"))
			.add(new DashboardContent(this).onShow())
			.add(new OpBeansInfo("Notebook removed"))
//			.add(new OpComponentClick(NotebookContent.class, UUID.random(), "showContent", 
//					new Params()
//						.add("userId", getUserId())
//						.add("notebookId", getParams().getString("notebookId"))))
			;
	}

	@Override
	protected String getMenuName() throws IOException {
		return "NOTEBOOK";
	}
}
