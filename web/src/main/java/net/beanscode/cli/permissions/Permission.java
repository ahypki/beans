package net.beanscode.cli.permissions;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import net.hypki.libs5.db.db.weblibs.utils.UUID;

import com.google.gson.annotations.Expose;

public class Permission {

	/**
	 * Notebook, Dataset or Table ID
	 */
	@Expose
	private UUID id = null;
	
	@Expose
	private String clazz = null;
	
	@Expose
	private Map<String, Boolean> groupRead = null;
	
	@Expose
	private Map<String, Boolean> groupWrite = null;

	public Permission() {
		
	}
		
	public boolean isReadAllowed(UUID groupId) {
		Boolean perm = getReadPermissions().get(groupId.getId());
		return perm != null ? perm : false;
	}
	
	public boolean isWriteAllowed(UUID groupId) {
		Boolean perm = getWritePermissions().get(groupId);
		return perm != null ? perm : false;
	}
	
	public boolean isDataset() {
		return getClazz().endsWith("Dataset");
	}
	
	public boolean isTable() {
		return getClazz().endsWith("Table");
	}
	
	public boolean isNotebook() {
		return getClazz().endsWith("Notebook");
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	private Map<String, Boolean> getReadPermissions() {
		if (groupRead == null)
			groupRead = new HashMap<>();
		return groupRead;
	}
	
	private Map<String, Boolean> getWritePermissions() {
		if (groupWrite == null)
			groupWrite = new HashMap<>();
		return groupWrite;
	}

	public String getClazz() {
		return clazz;
	}

	public void setClazz(String clazz) {
		this.clazz = clazz;
	}

	public Iterable<UUID> getReadAllowedGroups() {
		return new Iterable<UUID>() {
			
			@Override
			public Iterator<UUID> iterator() {
				return new Iterator<UUID>() {
					private Iterator<String> keyIter = getReadPermissions().keySet().iterator();
					
					@Override
					public UUID next() {
						return new UUID(keyIter.next());
					}
					
					@Override
					public boolean hasNext() {
						return keyIter.hasNext();
					}
				};
			}
		};
	}
	
	public Iterable<UUID> getWriteAllowedGroups() {
		return new Iterable<UUID>() {
			
			@Override
			public Iterator<UUID> iterator() {
				return new Iterator<UUID>() {
					private Iterator<String> keyIter = getWritePermissions().keySet().iterator();
					
					@Override
					public UUID next() {
						return new UUID(keyIter.next());
					}
					
					@Override
					public boolean hasNext() {
						return keyIter.hasNext();
					}
				};
			}
		};
	}

	public void clearReadPermissions() {
		groupRead = null;
	}
	
	public void clearWritePermissions() {
		groupWrite = null;
	}
}
