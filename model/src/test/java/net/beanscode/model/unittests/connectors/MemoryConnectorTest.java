package net.beanscode.model.unittests.connectors;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.junit.Test;

import com.mchange.v1.db.sql.ConnectionUtils;

import net.beanscode.model.connectors.ConnectorList;
import net.beanscode.model.plugins.ColumnDef;
import net.beanscode.model.plugins.ColumnDefList;
import net.beanscode.model.plugins.ConnectorUtils;
import net.beanscode.model.plugins.MemoryConnector;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.string.RandomUtils;

public class MemoryConnectorTest {

	@Test
	public void testSort() throws IOException {
		MemoryConnector conn = new MemoryConnector(new ColumnDefList()
														.addColumnDef(new ColumnDef("a", null, ColumnType.DOUBLE))
														.addColumnDef(new ColumnDef("b", null, ColumnType.DOUBLE)));
		
		conn.write(new Row(UUID.random()).addColumn("a", 2.0).addColumn("b", 5.0));
		conn.write(new Row(UUID.random()).addColumn("a", 2.0).addColumn("b", 4.0));
		conn.write(new Row(UUID.random()).addColumn("a", 2.0).addColumn("b", 3.0));
		conn.write(new Row(UUID.random()).addColumn("a", 2.0).addColumn("b", 2.0));
		conn.write(new Row(UUID.random()).addColumn("a", 2.0).addColumn("b", 1.0));
		conn.write(new Row(UUID.random()).addColumn("a", 1.0).addColumn("b", 5.0));
		conn.write(new Row(UUID.random()).addColumn("a", 1.0).addColumn("b", 4.0));
		conn.write(new Row(UUID.random()).addColumn("a", 1.0).addColumn("b", 3.0));
		conn.write(new Row(UUID.random()).addColumn("a", 1.0).addColumn("b", 2.0));
		conn.write(new Row(UUID.random()).addColumn("a", 1.0).addColumn("b", 1.0));
		
		System.out.println("NOT sorted");
		for (Row r : conn.iterateRows(null))
			System.out.println(r);
		
		conn.sort(Arrays.asList("a", "b"));
		
		System.out.println("SORTED");
		for (Row r : conn.iterateRows(null))
			System.out.println(r);
	}
	
	@Test
	public void testSort2() throws IOException {
		ConnectorList connList = new ConnectorList();
		
		for (int i = 0; i < 10; i++) {
			MemoryConnector conn = new MemoryConnector(new ColumnDefList()
														.addColumnDef(new ColumnDef("a", null, ColumnType.INTEGER))
														.addColumnDef(new ColumnDef("b", null, ColumnType.INTEGER)));
			
			for (int k = 0; k < 1000; k++) {				
				conn.write(new Row(UUID.random())
						.addColumn("a", RandomUtils.nextInt(10))
						.addColumn("b", RandomUtils.nextInt(10)));
			}
			
			conn.sort(Arrays.asList("a", "b"));
			connList.addConnector(conn);
		}
		
		MemoryConnector out = new MemoryConnector(new ColumnDefList()
				.addColumnDef(new ColumnDef("a", null, ColumnType.INTEGER))
				.addColumnDef(new ColumnDef("b", null, ColumnType.INTEGER)));
		
		ConnectorUtils.combineSortedConnectors(connList, out, Arrays.asList("a", "b"));
		
		System.out.println("SORTED");
		for (Row r : out.iterateRows(null))
			System.out.println(r);
	}
}
