package net.beanscode.cli.datasets;

public enum TableStatus {
	IMPORTING,
	CLEARING,
	HEALTHY,
	EMPTY
}
