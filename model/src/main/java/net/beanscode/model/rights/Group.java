package net.beanscode.model.rights;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.beanscode.model.BeansObject;
import net.beanscode.model.dataset.Dataset;
import net.hypki.libs5.db.db.weblibs.MutationList;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.weblibs.SearchManager;
import net.hypki.libs5.weblibs.db.Indexable;
import net.hypki.libs5.weblibs.db.WeblibsObject;
import net.hypki.libs5.weblibs.search.IndexJob;
import net.hypki.libs5.weblibs.user.User;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class Group extends BeansObject<Group> implements Indexable {
	
	public static final String COLUMN_FAMILY = "group";

	@Expose
	@NotNull
	@AssertValid
	private UUID id = null;
	
	@Expose
	@NotNull
	@AssertValid
	private UUID owner = null;
	
	@Expose
	@NotNull
	@NotEmpty
	private String name = null;
	
	@Expose
//	@NotNull
//	@NotEmpty
	private String description = null;
	
	@Expose
//	@NotNull
//	@NotEmpty
	private List<UUID> userIds = null;
	
	public Group() {
		setId(UUID.random());
	}

	@Override
	public String getColumnFamily() {
		return COLUMN_FAMILY;
	}

	@Override
	public String getKey() {
		return getId().getId();
	}
	
	@Override
	public void index() throws IOException {
		SearchManager.index(this);
	}
	
	// TODO usuwanie poprzednich rzeczy z grup zwłaszcza linków do DTN
	
	@Override
	public void deindex() throws IOException {
		SearchManager.removeObject(getCombinedKey(), getColumnFamily());
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public boolean addUser(UUID userId) {
		if (!getUserIds().contains(userId))
			return getUserIds().add(userId);
		return false;
	}
	
	public boolean addUser(User user) {
		if (!getUserIds().contains(user.getUserId()))
			return getUserIds().add(user.getUserId());
		return false;
	}

	private List<UUID> getUserIds() {
		if (userIds == null)
			userIds = new ArrayList<>();
		return userIds;
	}

	private void setUserIds(List<UUID> userIds) {
		this.userIds = userIds;
	}

	public UUID getOwner() {
		return owner;
	}

	public void setOwner(UUID owner) {
		this.owner = owner;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void clearUsers() {
		getUserIds().clear();
	}

	public void addUser(List<UUID> groupUserIds) {
		for (UUID id : groupUserIds) {
			if (id.isValid())
				addUser(id);
		}
	}

	public boolean containsUser(UUID userId) {
		return getUserIds().contains(userId);
	}
	
	public Iterable<UUID> getUsers() {
		return getUserIds();
	}
}
