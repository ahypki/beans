package net.beanscode.model.agent;

import static net.hypki.libs5.utils.args.ArgsUtils.exists;
import static net.hypki.libs5.utils.args.ArgsUtils.getStrings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.hypki.libs5.db.cassandra.CassandraUtils;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.db.weblibs.Settings;
import net.hypki.libs5.distributed.Lock;
import net.hypki.libs5.distributed.Member;
import net.hypki.libs5.distributed.hazelcast.DistributedHazelcast;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.args.ArgsUtils;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.string.RegexUtils;
import net.hypki.libs5.utils.string.StringUtilities;

public class BeansAgent {
	
	private static final String VERSION = "0.0.1";

	public static final String CURRENT_COMMAND_MAP_NAME = "BEANS_AGENT_CMDS";
	
	public static final String CMD_START_CASSANDRA = "START_CASSANDRA";
	public static final String CMD_STOP_CASSANDRA = "STOP_CASSANDRA";
	public static final String CMD_CHECK_CASSANDRA = "CHECK_CASSANDRA";
	public static final String CMD_SET_CASSANDRA = "SET_CASSANDRA_CONF";
	public static final String CMD_STOP_AGENT = "STOP_AGENT";
	public static final String CMD_STOP_DOUBLE_AGENT = "STOP_DOUBLE_AGENT";
	
	public static final int 	AGENT_DOWN_MS = 5000;
	public static final int 	AGENT_DEAD_MS = 60000;
	
	private BeansAgent() {
		
	}

	public static void main(String[] args) {
		LibsLogger.debug(Agent.class, "Starting BEANS Agent version ", VERSION);
		
		if (exists(args, "help") || exists(args, "h")) {
			
			printHelp();
			
		} else if (exists(args, "slave")) {
			
			new Agent().run(args);
			
		} else if (exists(args, "status")) {
			
			new Agent()
				.setAllowToStartCassandra(false)
				.run(args);
			
		} else if (exists(args, "cass-start")) {
			
			saveCmd(getStrings(args, "cass-start"), CMD_START_CASSANDRA);
			
		} else if (exists(args, "cass-stop")) {
			
			saveCmd(getStrings(args, "cass-stop"), CMD_STOP_CASSANDRA);
			
		} else if (exists(args, "cass-check")) {
			
			saveCmd(getStrings(args, "cass-check"), CMD_CHECK_CASSANDRA);

		} else if (exists(args, "cass-set")) {
			
			saveCmd(getStrings(args, "cass-set"), CMD_SET_CASSANDRA + ArgsUtils.getString(args, "cass-key"));
			
		} else if (exists(args, "stop")) {
			
			saveCmd(getStrings(args, "stop"), CMD_STOP_AGENT);
			
		} else if (exists(args, "stop-double")) {
			
			stopDoubleAgents();
			
		} else {
			
			LibsLogger.error(BeansAgent.class, "Unrecognized command for BEANS agent, exiting");
			
		}
	}
	
	private static void printHelp() {
		LibsLogger.debug(BeansAgent.class, "BEANS Agent help:");
		LibsLogger.debug(BeansAgent.class, "");
		LibsLogger.debug(BeansAgent.class, "\t--help | --h - prints help");
		LibsLogger.debug(BeansAgent.class, "\t--slave - starts the Agent as a slave");
		LibsLogger.debug(BeansAgent.class, "\t--cass-start - starts Cassandra on all agents");
		LibsLogger.debug(BeansAgent.class, "\t--cass-stop - starts Cassandra on all agents");
		LibsLogger.debug(BeansAgent.class, "\t--stop-double - stops double agents");
	}
	
	private static void stopDoubleAgents() {
		List<String> ipOccupied = new ArrayList<>();
		for (Agent agent : Agent.iterateAgents()) {
			if (ipOccupied.contains(agent.getIP()))
				if (!agent.isCassandraHomeDefined()) {
					saveCmd(Arrays.asList(agent.getAgentId()), CMD_STOP_AGENT);
				}
			ipOccupied.add(agent.getIP());
		}
	}

	private static void saveCmd(List<String> agentIds, String cmd) {
		for (Agent agent : Agent.iterateAgents()) {
			boolean gogogo = false;
			if (agentIds == null || agentIds.isEmpty())
				gogogo = true;
			else {
				for (String id : agentIds) {
					if (agent.getAgentId().contains(id)) {
						gogogo = true;
					}
				}
			}
			
			if (gogogo) {
				setCurrentCommand(agent.getAgentId(), cmd);
			}
		} 
	}
	
	public static String getCurrentCommand(String agentId) {
		final Object cmd = DistributedHazelcast.getInstance().getMap(BeansAgent.CURRENT_COMMAND_MAP_NAME).get(agentId);
		return cmd != null ? (String) cmd : "";
	}
	
	private static void setCurrentCommand(String agentId, String cmd) {
		if (cmd == null) {
			DistributedHazelcast.getInstance().getMap(CURRENT_COMMAND_MAP_NAME).remove(agentId);			
		} else {
			DistributedHazelcast.getInstance().getMap(CURRENT_COMMAND_MAP_NAME).put(agentId, cmd);
		}
		LibsLogger.debug(BeansAgent.class, "Agent ", agentId, " new command is: ", cmd);
	}
	
	public static List<String> getCassandraHomes() {
		List<String> paths = new ArrayList<>();
		String pathsSettings = Settings.getString("BEANS/Agent/paths");
		for (String byCol : StringUtilities.split(pathsSettings, ',')) {
			for (String bySemi : StringUtilities.split(byCol, ';')) {
				if (StringUtilities.notEmpty(bySemi))
					paths.add(bySemi);
			}
		}
		return paths;
	}

	
	
	
}
