package net.beanscode.web.view.dataset;

import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.name;
import static org.rendersnake.HtmlAttributesFactory.type;

import java.io.IOException;

import net.beanscode.pojo.dataset.Dataset;
import net.hypki.libs5.pjf.components.Component;

import org.rendersnake.HtmlCanvas;

public class DatasetEditDescPanel extends Component {
	
	private Dataset dataset = null;
	
	public DatasetEditDescPanel() {
		
	}
	
	public DatasetEditDescPanel(Dataset dataset) {
		setDataset(dataset);
	}

	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.form(class_("notebook-edit-description"))
				.span()
					.content("Dataset description:")
				.br()
				.input(type("hidden")
						.name("userId")
						.value(getDataset().getUserId().getId()))
				.input(type("hidden")
						.name("datasetId")
						.value(getDataset().getId().getId()))
				.textarea(name("description"))
					.content(getDataset().getDescription())
//				._textarea()
			._form()
			;
	}

	public Dataset getDataset() {
		return dataset;
	}

	public void setDataset(Dataset dataset) {
		this.dataset = dataset;
	}
}
