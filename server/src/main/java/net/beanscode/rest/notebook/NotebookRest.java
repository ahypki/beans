package net.beanscode.rest.notebook;

import static net.beanscode.rest.BeansServevrConst.MAGIC_KEY;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import mjson.Json;
import net.beanscode.model.notebook.MagicKey;
import net.beanscode.model.notebook.MagicKeyFactory;
import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.notebook.NotebookFactory;
import net.beanscode.model.notebook.NotebookUtils;
import net.beanscode.rest.BeansRest;
import net.beanscode.rest.BeansServevrConst;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.search.SearchResults;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.Meta;
import net.hypki.libs5.weblibs.user.UserFactory;

@Path("api/notebook")
public class NotebookRest extends BeansRest {
	
	@POST
	@Path("order")
	public void order(@Context SecurityContext sc, 
			@FormParam("notebookId") UUID notebookId,
			@FormParam("entryId") List<UUID> entryId) throws IOException, ValidationException {
		Notebook notebook = Notebook.getNotebook(notebookId);
		
		validateNotFound(notebook != null, "Notebook is not found");
		validateUnauthorized(isAdmin(sc) || notebook.isWriteAllowed(getUserId(sc)), "Permission denied");
		
		notebook.order(entryId);
		
		notebook.save();
	}
	
	@POST
	@Path("clone")
	public Response clone(@Context SecurityContext sc, 
			@FormParam("notebookId") UUID notebookId) throws IOException {
		Notebook notebook = Notebook.getNotebook(notebookId);
		
		validateNotFound(notebook != null, "Notebook is not found");
		validateUnauthorized(isAdmin(sc) || notebook.isReadAllowed(getUserId(sc)), "Permission denied");
		
		Notebook clonedNotebook = notebook.copyAsNew(getUserId(sc));
		
		return Response
				.ok()
				.entity(Json
						.object()
						.set("notebookId", clonedNotebook.getId().getId())
						.toString())
				.build();
	}
	
	@POST
	@Path("meta/add")
	public void metaAdd(@Context SecurityContext sc, 
			@FormParam("notebookId") UUID notebookId,
			@FormParam("metaName") String metaName,
			@FormParam("metaValue") String metaValue,
			@FormParam("metaDesc") String metaDesc) throws IOException, ValidationException {
		Notebook notebook = Notebook.getNotebook(notebookId);
		
		validateNotFound(notebook != null, "Notebook is not found");
		validateUnauthorized(isAdmin(sc) || notebook.isWriteAllowed(getUserId(sc)), "Permission denied");

		notebook.getMeta().add(metaName, metaValue, metaDesc);
		notebook.save();
	}
	
	@DELETE
	@Path("meta/remove")
	public void metaRemove(@Context SecurityContext sc, 
			@QueryParam("notebookId") UUID notebookId,
			@QueryParam("metaName") List<String> metaNames) throws IOException, ValidationException {
		Notebook notebook = Notebook.getNotebook(notebookId);
		
		validateNotFound(notebook != null, "Notebook is not found");
		validateUnauthorized(isAdmin(sc) || notebook.isWriteAllowed(getUserId(sc)), "Permission denied");

		for (String toRemove : metaNames) {
			notebook.getMeta().remove(toRemove);
		}
		notebook.save();
		LibsLogger.debug(NotebookRest.class, "Meta paramas removed from notebook ", notebookId);
	}
	
	@POST
	@Path("reload")
	public void reload(@Context SecurityContext sc,
			@FormParam("notebookId") UUID notebookId) throws IOException, ValidationException {
		Notebook notebook = Notebook.getNotebook(notebookId);
		
		validateNotFound(notebook != null, "Notebook is not found");
		validateUnauthorized(isAdmin(sc) || notebook.isWriteAllowed(getUserId(sc)), "Permission denied");
		
		notebook.reload();
	}
	
	@POST
	@Path("import")
//	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public void imporT(@Context SecurityContext sc,
//			@FormDataParam("file") InputStream uploadedInputStream,
//			@FormDataParam("file") FormDataContentDisposition fileDetail,
			@FormParam("file") String toImport,
			@FormParam("notebookId") UUID notebookId) throws IOException, ValidationException {
		Notebook notebook = Notebook.getNotebook(notebookId);
		
		validateNotFound(notebook != null, "Notebook is not found");
		validateUnauthorized(isAdmin(sc) || notebook.isWriteAllowed(getUserId(sc)), "Permission denied");
		
//		String toImport = StringUtilities.convertStreamToString(uploadedInputStream);
		
		NotebookUtils.importJsonToNotebook(notebook, toImport);
	}
	
	@GET
	@Path("export")
	@Produces(MediaType.APPLICATION_JSON)
	public Response export(@Context SecurityContext sc, 
			@QueryParam("notebookId") String notebookId) throws IOException, ValidationException {
		Notebook notebook = Notebook.getNotebook(notebookId);
		
		validateNotFound(notebook != null, "Notebook is not found");
		validateUnauthorized(isAdmin(sc) || notebook.isReadAllowed(getUserId(sc)), "Permission denied");
		
		return Response
				.ok()
				.entity(Json
						.object()
						.set("export", NotebookUtils.exportToJson(notebook))
						.toString())
				.build();
	}
	
	@GET
	@Path("get")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@Context SecurityContext sc, 
			@QueryParam("notebookId") String notebookId,
			@QueryParam(MAGIC_KEY) UUID mk) throws IOException, ValidationException {
		Notebook notebook = Notebook.getNotebook(notebookId);
			
		validateNotFound(notebook != null, "Notebook is not found");
		validateUnauthorized(isAdmin(sc) 
				|| notebook.isReadAllowed(getUserId(sc))
				|| notebook.isReadAllowedByShareLink(notebook.getId()), "Permission denied");
		
		return toResponse(notebook);
	}
	
	@GET
	@Path("share/magickey")
	@Produces(MediaType.APPLICATION_JSON)
	public Response shareMagicKeyGet(@Context SecurityContext sc, 
			@QueryParam("notebookId") UUID notebookId) throws IOException, ValidationException {
		MagicKey mk = MagicKeyFactory.getMagicKey(notebookId);
		
		if (mk == null)
			return Response
					.ok()
					.entity(Json
							.object())
					.build();
			
		validateNotFound(mk != null, "MagicKey not found for notebook " + notebookId);
		
		return Response
				.ok()
				.entity(Json
						.object()
						.set("id", Json.object().set("id", mk.getId().getId()))
						.set("beansObjectId", Json.object().set("id", mk.getBeansObjectId().getId()))
						.toString())
				.build();
	}
	
	@POST
	@Path("share/magickey")
	@Produces(MediaType.APPLICATION_JSON)
	public Response shareMagicKeypost(@Context SecurityContext sc, 
			@FormParam("notebookId") UUID notebookId) throws IOException, ValidationException {
		MagicKey mk = new MagicKey(notebookId)
				.save();

		return Response
				.ok()
				.entity(Json
						.object()
						.set("id", Json.object().set("id", mk.getId().getId()))
						.set("beansObjectId", Json.object().set("id", mk.getBeansObjectId().getId())))
				.build();
	}
	
	@GET
	@Path("query")
	@Produces(MediaType.APPLICATION_JSON)
	public Response search(@Context SecurityContext sc, 
			@QueryParam("query") String query,
			@QueryParam("from") int from,
			@QueryParam("size") int size) throws IOException, ValidationException {
		
		if (size <= 0)
			size = 20;
		
		SearchResults<Notebook> notebooks = NotebookFactory
											.searchNotebooks(getUserId(sc), 
													query, 
													from, 
													size);
		
		Json notebooksJson = notebooks.getObjectsAsJson();
		
		for (Json oneNotebookJson : notebooksJson.asJsonList()) {
			String userEmail = UserFactory.getUser(new UUID(oneNotebookJson.asJsonMap().get("userId").asJsonMap().get("id").asString())).getEmail().getEmail();
			oneNotebookJson
				.asJsonMap()
				.put("userEmail", Json.make(userEmail));
		}
		
		return Response
				.ok()
				.entity(Json
						.object()
						.set("notebooks", notebooksJson)
						.set("maxHits", notebooks.maxHits())
						.toString())
				.build();
	}
	
	@GET
	@Path("query")
	@Produces(MediaType.TEXT_PLAIN)
	public Response searchPlain(@Context SecurityContext sc, 
			@QueryParam("query") String query,
			@QueryParam("from") int from,
			@QueryParam("size") int size) throws IOException, ValidationException {
		validateUnauthorized(isAdmin(sc) || getUserId(sc) != null, "Permission denied");
		
		if (size <= 0)
			size = 20;
		
		SearchResults<Notebook> notebooks = NotebookFactory
											.searchNotebooks(getUserId(sc), 
													query, 
													from, 
													size);
		
		Json notebooksJson = notebooks.getObjectsAsJson();
		
		for (Json oneNotebookJson : notebooksJson.asJsonList()) {
			String userEmail = UserFactory.getUser(new UUID(oneNotebookJson.asJsonMap().get("userId").asJsonMap().get("id").asString())).getEmail().getEmail();
			oneNotebookJson
				.asJsonMap()
				.put("userEmail", Json.make(userEmail));
		}
		
		return Response
				.ok()
				.entity(Json
						.object()
						.set("notebooks", notebooks.getObjectsAsJson())
						.set("maxHits", notebooks.maxHits())
						.toString())
				.build();
	}
	
	@DELETE
	@Path("remove")
	public void remove(@Context SecurityContext sc, 
			@QueryParam("notebookId") List<UUID> notebookId) throws IOException, ValidationException {
		
		// validation 
		for (UUID noteId : notebookId) {
			Notebook notebook = Notebook.getNotebook(noteId);
			
			validateNotFound(notebook != null, "Notebook is not found");
			validateUnauthorized(isAdmin(sc) || notebook.isWriteAllowed(getUserId(sc)), "Permission denied");
		}
		
		// removal 
		for (UUID noteId : notebookId) {
			Notebook notebook = Notebook.getNotebook(noteId);
			
			if (notebook != null) {
				notebook.remove();
				LibsLogger.debug(NotebookRest.class, "Notebook ", notebook.getName(), " removed");
			}
		}
		
	}
	
	@DELETE
	@Path("clear")
	public void clear(@Context SecurityContext sc, 
			@QueryParam("notebookId") List<UUID> notebookId) throws IOException, ValidationException {
		
		// validation 
		for (UUID noteId : notebookId) {
			Notebook notebook = Notebook.getNotebook(noteId);
			
			validateNotFound(notebook != null, "Notebook is not found");
			validateUnauthorized(isAdmin(sc) || notebook.isWriteAllowed(getUserId(sc)), "Permission denied");
		}		

		// removal 
		for (UUID noteId : notebookId) {
			Notebook notebook = Notebook.getNotebook(noteId);
			
			if (notebook != null) {
				notebook.clearEntries();
				notebook.save();
				LibsLogger.debug(NotebookRest.class, "Notebook ", notebook.getName(), " cleared");
			}
		}
		
	}
	
	@POST
	@Path("create")
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(@Context SecurityContext sc, 
			@FormParam("name") String name) throws IOException, ValidationException {
		
		validateUnauthorized(getUserId(sc) != null, "Permission denied");
		
		Notebook note = new Notebook(getUserId(sc), name);

		note.save();
		LibsLogger.debug(NotebookRest.class, "New notebook created with id " + note.getId());
			
		return toResponse(note);
	}
	
	@POST
	@Path("edit")
	public void title(@Context SecurityContext sc, 
			@FormParam("notebookId") UUID notebookId,
			@FormParam("title") String title) throws IOException, ValidationException {
		Notebook notebook = Notebook.getNotebook(notebookId);
		
		validateNotFound(notebook != null, "Notebook is not found");
		validateUnauthorized(isAdmin(sc) || notebook.isWriteAllowed(getUserId(sc)), "Permission denied");
		
		notebook.setName(title);
		notebook.setReindexEntries(true);
		notebook.save();

		LibsLogger.debug(NotebookRest.class, "New notebook title set to " + title);
	}
	
	@POST
	@Path("tags")
	public void tags(@Context SecurityContext sc, 
			@FormParam("notebookId") UUID notebookId,
			@FormParam("tags") String tags) throws IOException, ValidationException {
		Notebook notebook = Notebook.getNotebook(notebookId);
		
		validateNotFound(notebook != null, "Notebook is not found");
		validateUnauthorized(isAdmin(sc) || notebook.isWriteAllowed(getUserId(sc)), "Permission denied");
		
		notebook.getTags().clear();
		notebook.getTags().addTags(tags);
		notebook.setReindexEntries(true);
		notebook.save();

		LibsLogger.debug(NotebookRest.class, "New notebook tags set to " + tags);
	}
	
	@POST
	@Path("description")
	public void description(@Context SecurityContext sc, 
			@FormParam("notebookId") UUID notebookId,
			@FormParam("description") String description) throws IOException, ValidationException {
		Notebook notebook = Notebook.getNotebook(notebookId);
		
		validateNotFound(notebook != null, "Notebook is not found");
		validateUnauthorized(isAdmin(sc) || notebook.isWriteAllowed(getUserId(sc)), "Permission denied");
		
		notebook.setDescription(description);
		notebook.setReindexEntries(false);
		notebook.save();

		LibsLogger.debug(NotebookRest.class, "Notebook description updated");
	}
	
	@POST
	@Path("add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addAll(@Context SecurityContext sc, 
			@FormParam("notebookId") UUID notebookId,
			@FormParam("selectedNotebookId") UUID selectedNotebookId) throws IOException, ValidationException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		final Notebook notebook = Notebook.getNotebook(notebookId);
		final Notebook notebookToClone = Notebook.getNotebook(selectedNotebookId);
		
		validateNotFound(notebook != null, "Notebook is not found");
		validateNotFound(notebookToClone != null, "Notebook to clone is not found");
		validateUnauthorized(isAdmin(sc) || notebook.isWriteAllowed(getUserId(sc)), "Permission denied");
		validateUnauthorized(isAdmin(sc) || notebookToClone.isReadAllowed(getUserId(sc)), "Permission denied");
		
		List<UUID> newEntriesIDs = notebook.addAllEntries(notebookToClone);
		
		Json entries = Json.array();
		for (UUID uuid : newEntriesIDs) {
			entries.add(uuid.getId());
		}
		
		return Response
				.ok()
				.entity(Json
						.object()
						.set("entries", entries)
						.toString())
				.build();
	}
	
	private Response toResponse(Notebook n) throws IOException {
		Json notebook = Json
				.object()
				.set("id", Json.object().set("id", n.getId().getId()))
				.set("userId", Json.object().set("id", n.getUserId().getId()))
				.set("userEmail", n.getOwner().getEmail().getEmail())
				.set("name", n.getName())
				.set("description", n.getDescription())
	//			.set("descriptionHtml", MarkdownUtils.toHtml(ds.getDescription()))
				.set("creationMs", n.getCreationMs());
		
		Json entriesIdsArr = Json.array();
		for (UUID entryId : n.getEntries()) {
			entriesIdsArr.add(Json.object().set("id", entryId.getId()));
		}
		notebook.set("entries", entriesIdsArr);
		
		Json metaObj = Json.object();
		for (Meta meta : n.getMeta()) {
			Json oneMeta = Json
					.object()
					.set("name", meta.getName())
					.set("value", meta.getValue() != null ? meta.getValue().toString() : null)
					.set("description", meta.getDescription());
			metaObj.set(meta.getName(), oneMeta);
		}
		notebook.set("meta", Json.object().set("meta", metaObj));
		
		return Response
				.ok()
				.entity(notebook.toString())
				.build();
	}
}
