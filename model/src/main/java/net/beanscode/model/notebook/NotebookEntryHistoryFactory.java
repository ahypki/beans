package net.beanscode.model.notebook;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.NotImplementedException;

import com.google.gson.JsonElement;

import net.beanscode.model.BeansSearchManager;
import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.DatasetFactory;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.ResultsIter;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.search.SearchResults;
import net.hypki.libs5.search.query.Bracket;
import net.hypki.libs5.search.query.Query;
import net.hypki.libs5.search.query.SortOrder;
import net.hypki.libs5.search.query.StringTerm;
import net.hypki.libs5.search.query.Term;
import net.hypki.libs5.search.query.TermRequirement;
import net.hypki.libs5.search.query.WildcardTerm;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.weblibs.CacheManager;
import net.hypki.libs5.weblibs.SearchManager;
import net.hypki.libs5.weblibs.WeblibsConst;

public class NotebookEntryHistoryFactory {
	
	public static NotebookEntryHistory getNotebookEntryHistory(UUID entryHistoryId) throws IOException {
		return DbObject.getDatabaseProvider().get(WeblibsConst.KEYSPACE, 
				NotebookEntryHistory.COLUMN_FAMILY, 
				new C3Where()
						.addClause(new C3Clause(DbObject.COLUMN_PK, ClauseType.EQ, entryHistoryId.getId())), 
				DbObject.COLUMN_DATA, 
				NotebookEntryHistory.class);
	}

	// TODO opty
	public static Iterable<NotebookEntryHistory> iterateNotebookEntryHistory(final UUID entryId) {
		return new Iterable<NotebookEntryHistory>() {
			@Override
			public Iterator<NotebookEntryHistory> iterator() {
				return new Iterator<NotebookEntryHistory>() {
					private ResultsIter rowsIter = null;
					
					private boolean initiated = false;
					
					private NotebookEntryHistory nextNotebookEntry = null;
					
					@Override
					public NotebookEntryHistory next() {
						if (!initiated)
							hasNext();
						return nextNotebookEntry;
					}
					
					@Override
					public boolean hasNext() {
						if (!initiated) {
							rowsIter = new ResultsIter(WeblibsConst.KEYSPACE, 
									NotebookEntryHistory.COLUMN_FAMILY, 
									new String[]{DbObject.COLUMN_DATA}, null);
							initiated = true;
						}
						
						while (rowsIter.hasNext()) {
							Row row = rowsIter.next();
							
							if (row != null && row.contains(DbObject.COLUMN_DATA)) {
								nextNotebookEntry = JsonUtils.fromJson((String) row.getColumns().get(DbObject.COLUMN_DATA), NotebookEntryHistory.class);
								
								if (nextNotebookEntry.getSourceEntryId().equals(entryId))
									return true;
							}
						}
						
						return false;
					}
				};
			}
		};
	}
	
	public static SearchResults<NotebookEntryHistory> searchNotebookEntryHistory(UUID entryId, int from, int size) throws IOException {
		Query esQuery = new Query();
		
		esQuery.addTerm(new StringTerm("sourceEntryId.id", entryId.getId(), TermRequirement.MUST));
				
//		esQuery.setSortBy("editMs", SortOrder.DESCENDING);
		
		return SearchManager.searchInstance().search(NotebookEntryHistory.class, 
				WeblibsConst.KEYSPACE_LOWERCASE, 
				NotebookEntryHistory.COLUMN_FAMILY.toLowerCase(), 
				esQuery, 
				from, 
				size);
	}
	
	public static Iterable<NotebookEntryHistory> iterateNotebookEntryHistorySearch(final UUID entryId) {
		return new Iterable<NotebookEntryHistory>() {
			@Override
			public Iterator<NotebookEntryHistory> iterator() {
				return new Iterator<NotebookEntryHistory>() {
					private int from = 0;
					
					private int nehIdx = 0;
					
					private List<NotebookEntryHistory> neh = null;
					
					@Override
					public void remove() {
						throw new NotImplementedException();
					}
					
					@Override
					public NotebookEntryHistory next() {
						return neh.get(nehIdx++);
					}
					
					@Override
					public boolean hasNext() {
						try {
							if (neh == null || neh.size() == nehIdx) {
								neh = searchNotebookEntryHistory(entryId, from, BeansSearchManager.PER_PAGE).getObjects();
								nehIdx = 0;
								from += neh.size();
							}
							return nehIdx < neh.size();
						} catch (IOException e) {
							LibsLogger.error(BeansSearchManager.class, "Cannot get the next page with results", e);
							return false;
						}
					}
				};
			}
		};
	}
}
