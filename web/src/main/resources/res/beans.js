function destroyD3() {
//	debug("[destroyD3] Destroying D3 plots");
//	for (var i = 0; i < d3Plots.length; i++) {
//		debug("[destroyD3] Removing D3 ", d3Plots[i]);
//		d3Plots[i].destroy();
//	}
}

var ajaxDataRenderer = function(url, plot, options) {
	var ret = [[[0,0]]];
	pjfShowProgress();
	$.ajax({
		url : url, dataType : "json",
		success : function(resp) {
			if (resp.result == "FAIL") {
				error("server ajax response failed: ", resp);
			} else {
				ret = resp;
			}
			pjfHideProgress();
		},
		error : function(resp) {
			error("Ajax call failed, no connection probably: ", resp);
			pjfHideProgress();
//			pjfError("Error", res.msg);
		}
	});
	return ret;
};

function beansJqplot(chartId, data, options) {
	$("#" + chartId).empty();
//	$("#" + chartId).resizable();
//	  $("#" + chartId).bind('resize', function(event, ui) {
//	      // pass in resetAxes: true option to get rid of old ticks and axis properties
//	      // which should be recomputed based on new plot size.
//	      plot1.replot( { resetAxes: true } );
//	  });
	$("#" + chartId).jqplot(data, options);
//			{title: title, dataRenderer: ajaxDataRenderer, highlighter: {show: true,sizeAdjust: 7.5},cursor : {show : true,zoom : true,showTooltip : false},legend: {show: true}}

}

function sinAndCos() {
	  var sin = [],sin2 = [],
	      cos = [];

	  //Data is represented as an array of {x,y} pairs.
	  for (var i = 0; i < 100; i++) {
	    sin.push({x: i, y: Math.sin(i/10)});
	    sin2.push({x: i, y: Math.sin(i/10) *0.25 + 0.5});
	    cos.push({x: i, y: .5 * Math.cos(i/10)});
	  }

	  //Line chart data should be sent as an array of series objects.
	  return [
	    {
	      values: sin,      //values - represents the array of {x,y} data points
	      key: 'Sine Wave', //key  - the name of the series.
	      color: '#ff7f0e'  //color - optional: choose your own line color.
	    },
	    {
	      values: cos,
	      key: 'Cosine Wave',
	      color: '#2ca02c'
	    },
	    {
	      values: sin2,
	      key: 'Another sine wave',
	      color: '#7777ff',
	      area: true      //area - set to true if you want this line to turn into a filled area chart.
	    }
	  ];
}

function demoData(colors) {
	  var lines = [];

	  // Data is represented as an array of {x,y} pairs.
	  for (var k = 0; k < 20; k++) {
		  var line = [];
		  for (var i = 0; i <= 20; i++) {
			  line.push({x: i, y: i + (k * i)});
		  }
		  if (colors != null) {
			  lines.push({
				  key: 'Line ' + (k + 1),
				  values: line,
				  color: colors[k]
			  });
		  } else {
			  lines.push({
				  key: 'Line ' + (k + 1),
				  values: line
			  });
		  }
	  }
	  
	  return lines;
}

function ajaxNvd3Data(dataUrl, params, chartId) {
	debug("[ajaxNvd3Data] Ajax nvd3 calling url ", dataUrl, " with params ", params);
	var ret = [];
	$.ajax({
		async : false, 
		cache : false, 
		url : "/" + dataUrl, 
		type: "GET", 
		data : params,
		dataType : "json",
		success : function(resp) {
			title = resp.title;
			if (!isEmpty(title))
				$(chartId + " .chart-title").html(title);
			ret = resp.data;
		},
		error : function(resp) {
			error("Ajax call failed, no connection probably: ", resp);
		}
	});
//	debug("[ajaxNvd3Data] Returning ret= ", ret);
	return ret;
}

function beansDemoPlot(chartId, colors) {
	d3.selectAll("#" + chartId + " svg").remove();
	$("#" + chartId).append("<svg>");
	nv.addGraph(function() {
		chart = nv.models.lineChart()
		        .margin({left: 100})  //Adjust chart margins to give the x-axis some breathing room.
		        .useInteractiveGuideline(true)  //We want nice looking tooltips and a guideline!
//		        .transitionDuration(350)  //how fast do you want the lines to transition?
		        .showLegend(true)       //Show the legend, allowing users to turn on/off line series.
		        .showYAxis(true)        //Show the y-axis
		        .showXAxis(true)        //Show the x-axis
		;
		
		var myData = demoData(colors);
		
	    d3
	    	.select("#" + chartId + " svg")
	    	.datum(myData)
	    	.call(chart);
	    
	    // Update the chart when window resizes.
	    nv.utils.windowResize(function() { chart.update() });
	    return chart;
	});
	debug("Demo plot reloaded with colors ", colors);
}

function beansD3Points(chartId, columns, dataUrl) {
	d3.selectAll("#" + chartId + " svg").remove();
	
	$("#" + chartId).append("<svg>");
	nv.addGraph(function() {
		chart = nv.models.scatterChart()
//		        .showDistX(true)
//		        .showDistY(true)
//		        .useVoronoi(false)
		        .color(d3.scale.category10().range())
		        .duration(300)
		;
		
		//Axis settings
		chart.xAxis.tickFormat(d3.format('.02f'));
		chart.yAxis.tickFormat(d3.format('.02f'));
		chart.tooltip.gravity('s');
	
	    d3
	    	.select("#" + chartId + " svg")
	    	.datum(ajaxNvd3Data(dataUrl))
//	    	.datum(sinAndCos())
	    	.call(chart);
	    nv.utils.windowResize(chart.update);
	    
//	    d3.rebind('clipVoronoi');
//		chart.clipVoronoi(false);
		
	    return chart;
	});
}

function beansD3Lines(plotId, columns, dataUrl, params, plotTitle) {
	var d3ChartId = "#ch" + plotId;
	debug("[beansD3Lines] Lines plot on " + d3ChartId + " with columns " + columns + ", calling url ", dataUrl, " with params ", params);
	$(d3ChartId).append("<svg>");
	nv.addGraph(function() {
		chart = nv.models.lineChart()
		        .margin({left: 100})  //Adjust chart margins to give the x-axis some breathing room.
//		        .useInteractiveGuideline(true)  //We want nice looking tooltips and a guideline!
//		        .transitionDuration(350)  //how fast do you want the lines to transition?
		        .showLegend(true)       //Show the legend, allowing users to turn on/off line series.
		        .showYAxis(true)        //Show the y-axis
		        .showXAxis(true)        //Show the x-axis
		;
		
		//Axis settings
		chart.xAxis.tickFormat(d3.format('.02f'));
		chart.yAxis.tickFormat(d3.format('.02f'));
		chart.interactiveLayer.tooltip.gravity('s');
		
	    d3
	    	.select(d3ChartId + " svg")
	    	.datum(ajaxNvd3Data(dataUrl, params, d3ChartId))
//	    	.datum(myData)
	    	.call(chart);
	    nv.utils.windowResize(chart.update);
	    return chart;
	});
}


function setSplitName(data, chartId) {
	try {
		var splitName = data[0].splitName;
		var splitValue = data[0].splitValue;
		if (splitValue != "##NO_SPLIT_BY") {
			var titleId = "." + chartId + "-title";
			$(titleId).html($(titleId).html() + " [" + splitName + "=" + splitValue + "]");
		}
	} catch (ex) {
		error("Cannot set split name", ex);
	}
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function beansD3Parallel(plotId, columns, dataUrl, params, plotTitle) {
	var d3ChartId = "#ch" + plotId;
	debug("[beansD3Parallel] Parallel plot on " + d3ChartId + " with columns " + columns + ", calling url ", dataUrl, 
			" with params ", params, ", title ", plotTitle);
//	$(d3ChartId).append("<svg>");
	
	var colors = d3.scale.category20b();
	var colorgen = d3.scale.ordinal()
	    .range(["#a6cee3","#1f78b4","#b2df8a","#33a02c",
	            "#fb9a99","#e31a1c","#fdbf6f","#ff7f00",
	            "#cab2d6","#6a3d9a","#ffff99","#b15928"]);
	  var color = function(d) { return colors(getRandomInt(0, 500)); };
	  
	  var dataFunc = ajaxNvd3Data(dataUrl, params, d3ChartId);
	
	  var parcoords = d3.parcoords()(d3ChartId)
	    .data(dataFunc)
	    .hideAxis(["name"])
	    .color(color)
//	    .color(function(d) { return blue_to_brown(getRandomInt(0, 5000)); })  // quantitative color scale
	    .alpha(0.25)
	    .composite("darken")
//	    .margin({ top: 24, left: 150, bottom: 12, right: 0 })
	    .mode("queue")
	    .render()
	    .brushMode("1D-axes");  // enable brushing
	
	  parcoords.svg.selectAll("text")
	    .style("font", "10px sans-serif");
	  

	  // create data table, row hover highlighting
	  var data = dataFunc;
	  var grid = d3.divgrid();
	  d3.select("#grid" + plotId)
	    .datum(data.slice(0,100))
	    .call(grid)
	    .selectAll(".row")
	    .on({
	      "mouseover": function(d) { parcoords.highlight([d]) },
	      "mouseout": parcoords.unhighlight
	    });

	  // update data table on brush event
	  parcoords.on("brush", function(d) {
	    d3.select("#grid" + plotId)
	      .datum(d.slice(0,1000))
	      .call(grid)
	      .selectAll(".row")
	      .on({
	        "mouseover": function(d) { parcoords.highlight([d]) },
	        "mouseout": parcoords.unhighlight
	      });
	  });

		if (!isEmpty(plotTitle)) {
	    	var $svg = $(d3ChartId + " svg");
	    	$svg.parent().prepend('<div class="chart-title">' + plotTitle + '</div>');
	    	debug("Added title");
	    }
			
	  debug("grid created 2");
//	var chart;
//	nv.addGraph(function() {
//	    chart = nv.models.parallelCoordinates()
//	    	.dimensionNames(columns)
////	    	.lineTension(0.85)
//	    	;
//	    
//	    if (!isEmpty(plotTitle)) {
//	    	var $svg = $(d3ChartId + " svg");
//	    	$svg.parent().prepend('<div class="chart-title">' + plotTitle + '</div>');
//	    }
//	    
//	    var dataFunc = ajaxNvd3Data(dataUrl, params, d3ChartId);
//	    
//	    d3
//	    	.select(d3ChartId + " svg")
//	    	.datum(dataFunc)
//	    	.call(chart);
//	    
//	    nv.utils.windowResize(chart.update);
//	    return chart;
//	});
}


function beansD3Boxes(plotId, columns, dataUrl, params, plotTitle, xLabel, yLabel) {
	var d3ChartId = "#ch" + plotId;
	debug("[beansD3Boxes] Boxes plot on " + d3ChartId + " with columns " + columns + ", calling url ", dataUrl, " with params ", params);
//	$(d3ChartId).select("<svg>").remove();
	d3.selectAll($("[id^=nvtooltip-]")).remove();
	$(d3ChartId).append("<svg>");
//	var chart;
	nv.addGraph({
        generate: function() {		
		    var chart = nv.models.multiBarChart()
//		        .stacked(true)
		        ;
		    
		    chart.xAxis
		        .tickFormat(d3.format('.5f'));

		    chart.yAxis
		        .tickFormat(d3.format('.5f'));
		
		    chart.dispatch.on('renderEnd', function(){
		        debug('[beansD3Boxes] Render Complete');
		    });
		    
		    if (!isEmpty(plotTitle)) {
		    	var $svg = $(d3ChartId + " svg");
		    	$svg.parent().prepend('<div class="chart-title">' + plotTitle + '</div>');
		    	$svg.parent().append('<div class="chart-ylabel" style="transform: rotate(-90deg); transform-origin: left top 0; float: left;">' + yLabel + '</div><div class="chart-xlabel">' + xLabel + '</div>');
		    }
		    
		    debug("[beansD3Boxes] before ajaxNvd3Data")
		    var dataFunc = ajaxNvd3Data(dataUrl, params, d3ChartId);
		
		    var svg = d3.select(d3ChartId + " svg").datum(dataFunc);
		    
		    svg.transition().duration(0).call(chart);

		    nv.utils.windowResize(chart.update);
		    return chart;
        },
        callback: function(graph) {
//            nv.utils.windowResize(function() {
//                var width = nv.utils.windowSize().width;
//                var height = nv.utils.windowSize().height;
//                graph.width(width).height(height);
//
//                d3.select(d3ChartId + " svg")
//                    .attr('width', width)
//                    .attr('height', height)
//                    .transition().duration(0)
//                    .call(graph);
//
//            });
        }
	});
}

function toggleMenu(ele) {
	var $this = $(ele);
	$this.parent('li').toggleClass('active').children('ul').collapse('toggle'); 
	$this.parent('li').siblings().removeClass('active').children('ul.in').collapse('hide');
}

function callPlugin(pluginClass, pluginMethod, paramsJS) {
	params = "";
	if (!isEmpty(paramsJS)) {
		eval(b64_to_utf8(paramsJS)).split("&").forEach(function(entry) {
//			debug(entry);
			parts = entry.split("=");
		    params = (isEmpty(params) ? "" : params + "&") + parts[0] + "=" + encodeURIComponent(parts[1]);
		  }, this);
	}
	info("calling plugin " + pluginClass + ", method " + pluginMethod + " with params " + params);
	pjfOps([{'type' : 'post', 
		'url' : 'view/component/click', 
		'params' : 'componentClass=net.beanscode.cli.web.view.plugins.PluginComponent&method=runPluginMethod&pluginClass=' + pluginClass + '&pluginMethod=' + pluginMethod + '&' + params}]);
}

var scrollTimer, lastScrollFireTime = 0;

function processScroll() {
	var h = window.innerHeight;
	$( ".plot-view" ).each(function( index ) {
//		  console.log( index + ", ID= " + $( this ).attr('id'));
	  var e = document.getElementById($( this ).attr('id'));
	  console.log("processScroll fired: e.offsetTop " + e.offsetTop + ", from top: " + (h + $(window).scrollTop()));
	  // check if element is visible
	  if (e.offsetTop < (h + $(window).scrollTop()) && e.offsetTop > $(window).scrollTop()) {
		  // check if onvisible was already called
		  if (!($( this ).data('onvisiblefired'))) {
			  $( this ).data('onvisiblefired', 'true');
			  
			  console.log("HTML element " + $( this ).attr('id') + " visible, current " + $(window).scrollTop());
			  console.log("calling " + $( this ).data('onvisible'));
			  eval($( this ).data('onvisible'));
		  }
		  
		  
//		  console.log("calling: " + b64_to_utf8(funcToCall));
	  }
	});
}

function onScrollShow() {
    var minScrollTime = 500;
    var now = new Date().getTime();
    
    if (!scrollTimer) {
        if (now - lastScrollFireTime > (3 * minScrollTime)) {
            processScroll();   // fire immediately on first scroll
            lastScrollFireTime = now;
        }
        scrollTimer = setTimeout(function() {
            scrollTimer = null;
            lastScrollFireTime = new Date().getTime();
            processScroll();
        }, minScrollTime);
    }	
}
