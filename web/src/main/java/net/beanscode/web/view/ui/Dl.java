package net.beanscode.web.view.ui;

import static org.rendersnake.HtmlAttributesFactory.class_;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.rendersnake.HtmlCanvas;

public class Dl extends BeansComponent {
	
	private List<String> titles = null;
	
	private List<String> values = null;
	
	private int titleWidth = 3;
	
	private int valueWidth = 9;

	public Dl() {
		
	}
	
	public Dl(BeansComponent parent) {
		super(parent);
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.dl(class_("row"));
		
		int i = 0;
		for (String t : getTitles()) {
			html
				.dt(class_("col-sm-" + getTitleWidth()))
					.content(t)
				.dd(class_("col-sm-" + getValueWidth()))
					.content(getValues().get(i++));
		}
		
		html
			._dl();
	}
	
	public Dl add(String title, String value) {
		getTitles().add(title);
		getValues().add(value);
		return this;
	}

	public int getTitleWidth() {
		return titleWidth;
	}

	public void setTitleWidth(int titleWidth) {
		this.titleWidth = titleWidth;
	}

	public int getValueWidth() {
		return valueWidth;
	}

	public void setValueWidth(int valueWidth) {
		this.valueWidth = valueWidth;
	}

	private List<String> getTitles() {
		if (titles == null)
			titles = new ArrayList<String>();
		return titles;
	}

	private void setTitles(List<String> titles) {
		this.titles = titles;
	}

	private List<String> getValues() {
		if (values == null)
			values = new ArrayList<String>();
		return values;
	}

	private void setValues(List<String> values) {
		this.values = values;
	}
}
