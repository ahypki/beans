package net.beanscode.web.view.dataset;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.datasets.Table;
import net.beanscode.cli.datasets.TableGateway;
import net.beanscode.cli.notebooks.Notebook;
import net.beanscode.pojo.dataset.Dataset;
import net.beanscode.web.view.ui.BetaButtonGroup;
import net.beanscode.web.view.ui.BetaTableComponent;
import net.beanscode.web.view.ui.modals.InputType;
import net.beanscode.web.view.ui.modals.ModalWindow;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.beanscode.web.view.ui.modals.OpModal;
import net.beanscode.web.view.ui.modals.OpModalQuestion;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.Meta;
import net.hypki.libs5.utils.string.MetaList;
import net.hypki.libs5.utils.url.Params;

import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

public class MetaTableComponent extends BetaTableComponent {
	
	@Expose
	private UUID datasetId = null;
	
	@Expose
	private UUID tableId = null;
	
	@Expose
	private UUID notebookId = null;
	
//	@Expose
//	@NotNull
//	@AssertValid
	private MetaList metaList = null;
	
//	@Expose
//	@NotNull
//	@AssertValid
//	private MetaList metaListFiltered = null;
	
//	@Expose
//	private String filterCached = null;
	
	private Dataset dataset = null;
	
	private Table table = null;
	
	@Expose
	private boolean isEditEnabled = true;
	
	@Expose
	private boolean isDescriptionVisible = true;

	public MetaTableComponent() {
		
	}
	
	public MetaTableComponent(Component parent, Dataset ds) {
		super(parent);
		if (ds != null) {
			setDatasetId(ds.getId());
			setMetaList(ds.getMeta());
		}
	}
	
	public MetaTableComponent(Component parent, Table tb) {
		super(parent);
		setTableId(tb.getId());
		setMetaList(tb.getMeta());
	}
	
	public MetaTableComponent(Component parent, Notebook nt) {
		super(parent);
		setNotebookId(nt.getId());
		setMetaList(nt.getMeta());
	}
	
	@Override
	protected String getSettingsPrefix() {
		return "meta-table-";
	}
	
	@Override
	protected boolean isExapandable() throws IOException {
		return false;
	}
	
	@Override
	protected void renderExpandable(int row, HtmlCanvas html)
			throws IOException {
		
	}
	
	@Override
	protected List<Button> getMenu() throws IOException {
		List<Button> buttons = new ArrayList<>();
		buttons.add(new Button("Add")
				.setVisible(isEditEnabled())
				.add(new OpComponentClick(this, 
						"onEdit", 
						new Params()
							.add("notebookId", getNotebookId() != null ? getNotebookId() : null)
							.add("datasetId", getDatasetId() != null ? getDatasetId() : null)
							.add("tableId", getTableId() != null ? getTableId() : null)
							)));
		return buttons;
	}
	
	private OpList onEdit() throws IOException {
		ModalWindow modal = new ModalWindow(this);
		
		modal
			.setTitle("Editing meta parameter")
			.add(new Button("Save")
					.add(new OpComponentClick(this.getClass(), modal.getId(), "onEditSave"))
					.add(modal.onClose()))
			.add(new Button("Cancel")
					.add(modal.onClose()))
			.add("parentId", InputType.HIDDEN, "parentId", getId());
		if (getNotebookId() != null)
			modal.add("ID", InputType.HIDDEN, "notebookId", getNotebookId());
		if (getDatasetId() != null)
			modal.add("ID", InputType.HIDDEN, "datasetId", getDatasetId());
		if (getTableId() != null)
			modal.add("ID", InputType.HIDDEN, "tableId", getTableId());
		modal
			.add("Name", InputType.STRING, "meta-name", getParams().getString("meta-name", ""))
			.add("Value", InputType.STRING, "meta-value", getParams().getString("meta-value", ""))
			.add("Description", InputType.TEXT, "meta-description", getParams().getString("meta-description", ""))
			;
		
		return new OpList()
			.add(new OpModal(modal));
//			.add(new OpDialogOpen(this, 
//					"onEditSave", 
//					"New meta parameter", 
//					getEditDialog(getParams().getString("meta-name"),
//							getParams().getString("meta-value"), 
//							getParams().getString("meta-description")).toHtml()));
	}
	
//	private BeansDialogPanel getEditDialog(String name, String value, String description) {
//		return new BeansDialogPanel(this, "New meta parameter")
//			.addEntry("Name", notEmpty(name) ? name :  "", false)
//			.addEntry("Value", notEmpty(value) ? value : "", false)
//			.addEntry("Description", notEmpty(description) ? description : "", true);
//	}
	
	private OpList onEditSave() throws IOException, ValidationException {
		final String metaName = getParams().getString("meta-name");
		final String metaValue = getParams().getString("meta-value");
		final String metaDesc = getParams().getString("meta-description");
		final String parenId = getParams().getString("parentId");
		
		Params params = new Params();
		if (getDatasetId() != null) {
//			new DatasetMetaAdd(getSessionBean()).run(getDataset().getId(), metaName, metaValue, metaDesc);
			new ApiCallCommand(this)
				.setApi("api/dataset/meta/add")
				.addParam("datasetId", getDataset().getId())
				.addParam("metaName", metaName)
				.addParam("metaValue", metaValue)
				.addParam("metaDesc", metaDesc)
				.run();
			params.add("datasetId", getDatasetId().getId());
		} else if (getTableId() != null) {
			new ApiCallCommand(this)
				.setApi("api/table/meta/add")
				.addParam("tableId", getTableId().getId())
				.addParam("metaName", metaName)
				.addParam("metaDesc", metaDesc)
				.addParam("metaValue", metaValue)
				.run();
			params.add("tableId", getTableId().getId());
//			getTable().getMeta().add(metaName, metaValue, metaDesc);
//			getTable().save();
		} else  {
			new ApiCallCommand(this)
				.setApi("api/notebook/meta/add")
				.addParam("notebookId", getNotebookId().getId())
				.addParam("metaName", metaName)
				.addParam("metaDesc", metaDesc)
				.addParam("metaValue", metaValue)
				.run();
			params.add("notebookId", getNotebookId().getId());
		}
		
		return new OpList()
//			.add(new OpDialogClose())
//			.add(new OpComponentClick(this, "onRefresh"))
			.add(new OpBeansInfo("Meta parameter saved"))
			.add(new OpComponentClick(this.getClass(), parenId, "onRefresh", params))
			;
	}
	
	@Override
	protected boolean isFilterVisible() {
		return true;
	}
	
//	@Override
//	protected String getTableHtmlClasses() {
//		return super.getTableHtmlClasses() + " meta-table";
//	}

	protected MetaList getMetaList() {
		if (this.metaList != null)
			return this.metaList;
		
		try {
			if (isDatasetMetalist()) {
				Dataset ds = new ApiCallCommand(this)
									.setApi("api/dataset")
									.addParam("dataset-id", getDatasetId().getId())
									.run()
									.getResponse()
									.getAsObject(Dataset.class);
				this.metaList = ds.getMeta();
			} else if (isTableMetalist()) {
				Table tb = TableGateway.getTable(getSessionBean(), getTableId().getId());
				this.metaList = tb.getMeta();
			} else {
				if (getNotebookId() != null) {
					Notebook nb = new ApiCallCommand(this)
									.setApi("api/notebook/get")
									.addParam("notebookId", getNotebookId().getId())
									.run()
									.getResponse()
									.getAsObject(Notebook.class);
					this.metaList = nb.getMeta();
				} else
					this.metaList = null;
			}
			
			if (isFilterVisible() && notEmpty(getFilter())) {
				this.metaList = metaList.filterIn(getFilter());
			}
		} catch (IOException e) {
			LibsLogger.error(MetaTableComponent.class, "Cannot initialize or filter MetaList", e);
		}
		
		return metaList;
	}
	
//	protected MetaList getMetaList(String filter) {
//		if (!equalsContent(filterCached, filter)) {
//			filterCached = filter.trim().toLowerCase();
//			
//			getMetaListFiltered().clear();
//			
//			for (Meta meta : getMetaList()) {
//				if (meta.isNameSpecified() && meta.getName().toLowerCase().contains(filterCached))
//					getMetaListFiltered().add(meta);
//				else if (meta.isDescriptionSpecified() && meta.getDescription().toLowerCase().contains(filterCached))
//					getMetaListFiltered().add(meta);
//			}
//		} else if (nullOrEmpty(filter)) {
//			filterCached = null;
//			setMetaListFiltered(getMetaList());
//		}
//		return getMetaListFiltered();
//	}

//	protected void setMetaList(MetaList metaList) {
//		this.metaList = metaList;
//	}
	
	protected long getMaxHits() throws IOException {
		return getMetaList() != null ? getMetaList().size() : 0;
	}
	
	@Override
	protected String getColumnName(int column) throws IOException {
		if (column == 0)
			return "Key";
		else if (column == 1)
			return "Value";
		else if (column == 2)
			return "Description";
		else
			return "Options";
	}
	
	@Override
	protected boolean isHeaderColumnVisible() throws IOException {
		return true;
	}
	
	@Override
	protected void renderTitle(HtmlCanvas html) throws IOException {
		html
			.write("Meta parameters");
	}
	
	@Override
	protected void renderCell(int row, int column, HtmlCanvas html)
			throws IOException {
		Meta meta = getMetaList().get(getPage() * getPerPage() + row % getPerPage());
		
		if (meta == null)
			return;
		
		if (column == 0)
			html
				.span()
					.content(meta.getName());
		else if (column == 1)
			html
				.span()
					.content(String.valueOf(meta.getValue()));
		else if (column == 2)
			html
				.span()
					.content(String.valueOf(meta.getDescription() != null ? meta.getDescription() : ""));
		else {
			if (isEditEnabled())
				html
					.render(new BetaButtonGroup()
							.add(new Button()
										.setAwesomeicon("fa fa-trash")
										.add(new OpComponentClick(this, 
												"onRemoveQuestion", 
												new Params()
													.add("meta-name", meta.getName()))))
							.add(new Button()
										.setAwesomeicon("fa fa-edit")
										.add(new OpComponentClick(this, 
												"onEdit",
												new Params()
													.add("meta-name", meta.getName())
													.add("meta-value", meta.getValue())
													.add("meta-description", meta.getDescription())
													)))
							);
		}
	}
	
	private OpList onRemoveQuestion() throws ValidationException, IOException {
		final String metaName = getParams().getString("meta-name");

		return new OpList()
			.add(new OpModalQuestion("Removing meta parameter", 
					"Are you sure you want to remove meta parameter: " + metaName + "?",
					new OpComponentClick(this, 
							"onRemove",
							new Params()
								.add("meta-name", metaName))));
	}
	
	private OpList onRemove() throws ValidationException, IOException {
		final String metaName = getParams().getString("meta-name");		
		
		int row = getMetaList().getKeys().indexOf(metaName);
		
		getMetaList().remove(metaName);
		
		if (isDatasetMetalist()) {
			new ApiCallCommand(this)
				.setApi("api/dataset/meta")
				.addParam("datasetId", getDatasetId())
				.addParam("metaName", metaName)
				.run();
		} else if (isTableMetalist()) {
			new ApiCallCommand(this)
				.setApi("api/table/meta/remove")
				.addParam("tableId", getTableId().getId())
				.addParam("metaName", metaName)
				.run();
		} else {
			new ApiCallCommand(this)
				.setApi("api/notebook/meta/remove")
				.addParam("notebookId", getNotebookId().getId())
				.addParam("metaName", metaName)
				.run();
		}
		
		return new OpList()
//			.add(new OpRemove("#" + getId() + " .tr-" + row))
			.add(new OpComponentClick(this, "onRefresh"))
			.add(new OpBeansInfo("Meta parameter " + metaName + " removed"))
			;
	}
	
	private Dataset getDataset() throws IOException {
		if (dataset == null) {
			this.dataset = new ApiCallCommand(this)
								.setApi("api/dataset")
								.addParam("dataset-id", getDatasetId().getId())
								.run()
								.getResponse()
								.getAsObject(Dataset.class);
		}
		return dataset;
	}
	
	private Table getTable() throws IOException {
		if (table == null) {
			this.table = TableGateway.getTable(getSessionBean(), getTableId().getId());
		}
		return table;
	}
	
	@Override
	protected int getNumberOfColumns() throws IOException {
		return 2 + (isDescriptionVisible() ? 1 : 0) + (isEditEnabled() ? 1 : 0);
	}

//	private MetaList getMetaListFiltered() {
//		if (metaListFiltered == null)
//			metaListFiltered = new MetaList();
//		return metaListFiltered;
//	}
//
//	private void setMetaListFiltered(MetaList metaListFiltered) {
//		this.metaListFiltered = metaListFiltered;
//	}
	
	private void setMetaList(MetaList metaList) {
		this.metaList = metaList;
	}

	public UUID getDatasetId() {
		if (datasetId == null)
			datasetId = getParams().getString("datasetId") != null ? new UUID(getParams().getString("datasetId")) : null;
		return datasetId;
	}

	public void setDatasetId(UUID datasetId) {
		this.datasetId = datasetId;
	}

	public UUID getTableId() {
		if (tableId == null)
			tableId = getParams().getString("tableId") != null ? new UUID(getParams().getString("tableId")) : null;
		return tableId;
	}

	public void setTableId(UUID tableId) {
		this.tableId = tableId;
	}
	
	public boolean isDatasetMetalist() {
		return getDatasetId() != null;
	}
	
	public boolean isTableMetalist() {
		return getTableId() != null;
	}
	
	public boolean isNotebookMetalist() {
		return getNotebookId() != null;
	}

	public UUID getNotebookId() {
		if (notebookId == null)
			notebookId = getParams().getString("notebookId") != null ? new UUID(getParams().getString("notebookId")) : null;
		return notebookId;
	}
	
	public UUID getEntryId() {
		if (isNotebookMetalist())
			return getNotebookId();
		else if (isDatasetMetalist())
			return getDatasetId();
		else if (isTableMetalist())
			return getTableId();
		else
			return null;
	}

	public void setNotebookId(UUID notebookId) {
		this.notebookId = notebookId;
	}

	public boolean isEditEnabled() {
		return isEditEnabled;
	}

	public MetaTableComponent setEditEnabled(boolean isEditEnabled) {
		this.isEditEnabled = isEditEnabled;
		return this;
	}

	public boolean isDescriptionVisible() {
		return isDescriptionVisible;
	}

	public MetaTableComponent setDescriptionVisible(boolean isDescriptionVisible) {
		this.isDescriptionVisible = isDescriptionVisible;
		return this;
	}

	@Override
	protected String getTableCss() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}
}
