package net.beanscode.rest.jersey;

import javax.ws.rs.core.Response;

public class ValidationRestException extends RuntimeException {
	
	private Response.Status code = Response.Status.BAD_REQUEST;
	
	public ValidationRestException() {
		super("REST call failed with unknown error");
	}
	
	public ValidationRestException(Response.Status code, String message) {
		super(message);
		
		setCode(code);
	}

	public Response.Status getCode() {
		return code;
	}

	public void setCode(Response.Status code) {
		this.code = code;
	}
}
