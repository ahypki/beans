package net.beanscode.web.view.ui.remarks;

import static org.rendersnake.HtmlAttributesFactory.class_;

import java.io.IOException;

import org.rendersnake.HtmlAttributesFactory;
import org.rendersnake.HtmlCanvas;

import net.hypki.libs5.pjf.components.Component;

public class WarningRemark extends Component {
	
	private String title = null;
	
	private String message = null;

	public WarningRemark() {
		
	}
	
	public WarningRemark(String title, String message) {
		setTitle(title);
		setMessage(message);
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.div(class_("alert alert-warning"))
				.h5()
					.i(class_("icon fas fa-exclamation-triangle"))
					._i()
					.write(getTitle())
				._h5()
				.write(getMessage())
			._div();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
