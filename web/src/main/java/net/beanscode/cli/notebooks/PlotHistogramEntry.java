package net.beanscode.cli.notebooks;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.beanscode.pojo.NotebookEntry;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.reflection.SystemUtils;
import net.hypki.libs5.utils.string.Meta;
import net.hypki.libs5.utils.string.StringUtilities;

public class PlotHistogramEntry extends NotebookEntry {
	
	public static final String PNG = "PNG";
	public static final String PDF = "PDF";
	public static final String PS = "PS";
	public static final String ASCII = "ASCII";
	
	private static final String META_DS_QUERY 		= "META_DS_QUERY";
	private static final String META_TB_QUERY 		= "META_TB_QUERY";
	private static final String META_PLOT_STATUS	= "META_PLOT_STATUS";
	private static final String META_READ_TABLES	= "META_READ_TABLES";
	private static final String META_TITLE			= "META_TITLE";
	private static final String META_XLABEL			= "META_XLABEL";
	private static final String META_YLABEL			= "META_YLABEL";
	private static final String META_SPLITBY		= "META_SPLITBY";
	private static final String META_COLORBY		= "META_COLORBY";
	private static final String META_X_COLUMN		= "META_X_COLUMN";
	private static final String META_Y_COLUMN		= "META_Y_COLUMN";
	private static final String META_BINWIDTH		= "META_BINWIDTH";
	
	private PlotStatus plotStatus = null;
		
	public PlotHistogramEntry() {
		
	}
	
	public PlotHistogramEntry(Notebook notebook) {
		setNotebookId(notebook.getId());
		setUserId(notebook.getUserId());
	}
	
	@Override
	public String toString() {
		return getTitle();
	}
			
	public static String getDefaultText() {
		return SystemUtils.readFileContent(PlotHistogramEntry.class, "PlotDefaultText");
	}

	public String getTitle() {
		return getMetaAsString(META_TITLE, null);
	}

	public void setTitle(String title) {
		setMeta(META_TITLE, title);
	}

	private List<UUID> getReadTables() {
		List<UUID> readTables = null;
		String tbStr = getMetaAsString(META_READ_TABLES, null);
		if (tbStr == null) {
			readTables = new ArrayList<>();
		} else {
			readTables = JsonUtils.readList(tbStr, UUID.class);
		}
		return readTables;
	}

	public void setReadTables(List<UUID> readTables) {
		setMeta(META_READ_TABLES, JsonUtils.objectToStringPretty(readTables));
	}
	
	public PlotStatus getPlotStatus() {
		if (plotStatus == null) {
			String tmp = getMetaAsString(META_PLOT_STATUS, null);
			if (tmp == null) {
				plotStatus = new PlotStatus();
				setPlotStatus(plotStatus);
			} else { 
				plotStatus = JsonUtils.fromJson(tmp, PlotStatus.class);
			}
		}
		return plotStatus;
	}

	private void setPlotStatus(PlotStatus plotStatus) {
		this.plotStatus = plotStatus;
		setMeta(META_PLOT_STATUS, JsonUtils.objectToStringPretty(plotStatus));
	}

	public String getDsQuery() {
		return getMetaAsString(META_DS_QUERY, null);
	}

	public void setDsQuery(String dsQuery) throws IOException {
		setMeta(META_DS_QUERY, dsQuery);
	}

	public String getTbQuery() {
		return getMetaAsString(META_TB_QUERY, null);
	}

	public void setTbQuery(String tbQuery) {
		setMeta(META_TB_QUERY, tbQuery);
	}
	
	public boolean getMetaAsBoolean(String metaName, boolean defaultValue) {
		Meta m = getMeta().get(metaName);
		if (m != null)
			return m.getAsBoolean();
		return defaultValue;
	}
	
	public PlotHistogramEntry setMeta(String name, boolean value) {
		getMeta().add(name, value);
		return this;
	}

	public double getBinWidth() {
		return getMetaAsDouble(META_BINWIDTH, 0.0);
	}

	public void setBinWidth(double binWidth) {
		setMeta(META_BINWIDTH, binWidth);
	}

	public String getXLabel() {
		return getMetaAsString(META_XLABEL, null);
	}

	public void setXLabel(String xLabel) {
		setMeta(META_XLABEL, xLabel);
	}

	public String getYLabel() {
		return getMetaAsString(META_YLABEL, null);
	}

	public void setYLabel(String yLabel) {
		setMeta(META_YLABEL, yLabel);
	}
	
	public String getXLabelNotNull() {
		return getXLabel() != null ? getXLabel() : "";
	}
	
	public String getYLabelNotNull() {
		return getYLabel() != null ? getYLabel() : "";
	}

	public String getSplitBy() {
		return getMetaAsString(META_SPLITBY, null);
	}

	public void setSplitBy(String splitBy) {
		setMeta(META_SPLITBY, splitBy);
	}

	public String getColorBy() {
		return getMetaAsString(META_COLORBY, null);
	}

	public void setColorBy(String colorBy) {
		setMeta(META_COLORBY, colorBy);
	}

	public String getX() {
		return getMetaAsString(META_X_COLUMN, null);
	}

	public PlotHistogramEntry setX(String x) {
		setMeta(META_X_COLUMN, x);
		return this;
	}

	public String getY() {
		return getMetaAsString(META_Y_COLUMN, null);
	}

	public void setY(String y) {
		setMeta(META_Y_COLUMN, y);
	}

	public String getXNotNull() {
		return getX() == null ? "" : getX();
	}

	public String getYNotNull() {
		return getY() == null ? "" : getY();
	}
	
	public String getXYColumns() {
		if (getX() != null && getY() != null)
			return getXNotNull() + ":" + getYNotNull();
		else if (getX() != null)
			return getXNotNull();
		else
			return "";
	}

	public boolean isSplitBySet() {
		return !StringUtilities.nullOrEmpty(getSplitBy());
	}
	
}
