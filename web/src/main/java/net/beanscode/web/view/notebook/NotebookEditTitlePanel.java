package net.beanscode.web.view.notebook;

import static org.rendersnake.HtmlAttributesFactory.type;

import java.io.IOException;

import net.beanscode.cli.notebooks.Notebook;
import net.hypki.libs5.pjf.components.Component;

import org.rendersnake.HtmlCanvas;

public class NotebookEditTitlePanel extends Component {
	
	private Notebook notebook = null;
	
	public NotebookEditTitlePanel() {
		
	}
	
	public NotebookEditTitlePanel(Notebook notebook) {
		setNotebook(notebook);
	}

	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
//			.form()
				.span()
					.content("Notebook title:")
				.br()
				.input(type("hidden")
						.name("userId")
						.value(getNotebook().getUserId().getId()))
				.input(type("hidden")
						.name("notebookId")
						.value(getNotebook().getId().getId()))
				.input(type("text")
						.name("name")
						.value(getNotebook().getName())
						.style("width: 100%;"))
//			._form()
			;
	}

	public Notebook getNotebook() {
		return notebook;
	}

	public void setNotebook(Notebook notebook) {
		this.notebook = notebook;
	}
}
