package net.beanscode.model.plugins;

import static java.lang.String.format;
import net.hypki.libs5.utils.string.Base64Utils;
import net.hypki.libs5.utils.string.StringUtilities;


public class CallPlugin {
	
	private Class<? extends Plugin> pluginClass = null;
	
	private String methodToCall = null;
	
	private String jQToSerialize = null;

	public CallPlugin(final Plugin plugin, final String methodToCall) {
		setPluginClass(plugin.getClass());
		setMethodToCall(methodToCall);
	}
	
	public CallPlugin(final Plugin plugin, final String methodToCall, final String jQToSerialize) {
		setPluginClass(plugin.getClass());
		setMethodToCall(methodToCall);
		setjQToSerialize(jQToSerialize);
	}
	
	@Override
	public String toString() {
		return String.format("callPlugin('%s', '%s', '%s')", getPluginClass().getName(), getMethodToCall(),
				StringUtilities.notEmpty(getjQToSerialize()) ? Base64Utils.encode(getjQToSerialize()) : "");
	}

	public Class<? extends Plugin> getPluginClass() {
		return pluginClass;
	}

	public void setPluginClass(Class<? extends Plugin> pluginClass) {
		this.pluginClass = pluginClass;
	}

	public String getMethodToCall() {
		return methodToCall;
	}

	public void setMethodToCall(String methodToCall) {
		this.methodToCall = methodToCall;
	}

	public String getjQToSerialize() {
		return jQToSerialize;
	}

	public void setjQToSerialize(String jQToSerialize) {
		this.jQToSerialize = jQToSerialize;
	}
}
