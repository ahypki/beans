package net.beanscode.rest.jersey;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.pjf.op.OpError;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.url.HttpResponse;

@Provider
public class ValidationRestExceptionMapper implements ExceptionMapper<ValidationRestException> {

	public Response toResponse(ValidationRestException exception) {
		LibsLogger.error(ValidationExceptionMapper.class, "Validation REST exception", exception);
		
		return Response
				.status(exception.getCode())
				.entity(new HttpResponse()
							.setCode(exception.getCode().getStatusCode())
							.setMessage(exception.getMessage()))
				.build();
	}

}
