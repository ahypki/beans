package net.beanscode.model.notebook;

import java.io.IOException;

import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.weblibs.WeblibsConst;

public class NotebookEntryProgressFactory {

	public static NotebookEntryProgress getNotebookEntryProgress(UUID entryId) throws IOException {
		if (entryId == null)
			return null;
		
		// TODO cache 
		return DbObject.getDatabaseProvider().get(WeblibsConst.KEYSPACE, 
				NotebookEntryProgress.COLUMN_FAMILY, 
				new C3Where()
						.addClause(new C3Clause(DbObject.COLUMN_PK, ClauseType.EQ, entryId.getId())), 
				DbObject.COLUMN_DATA, 
				NotebookEntryProgress.class);
	}
}
