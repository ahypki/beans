package net.beanscode.web.view.notebook;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;

import java.io.IOException;
import java.util.List;

import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

import net.beanscode.cli.notebooks.NotebookEntryGateway;
import net.beanscode.cli.notebooks.Plot;
import net.beanscode.pojo.NotebookEntry;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.BetaTableComponent;
import net.beanscode.web.view.ui.Link;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.buttons.ButtonGroup;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.search.SearchResults;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.url.Params;

public class CloneEntryPanel extends BetaTableComponent {
		
	@Expose
	private OpList onAdd = null;
	
	private SearchResults<NotebookEntry> entries = null;
	
	@Expose
	private boolean cloneBtnVisible = true;
	
	public CloneEntryPanel() {
		
	}

	public CloneEntryPanel(BeansComponent parent, OpList onAdd) {
		super(parent);
		setOnAdd(onAdd);
	}
	
	@Override
	protected String getSettingsPrefix() {
		return "cloneentries-table-";
	}
	
	@Override
	protected String getFilter2Placeholder() {
		return "Filter entries";
	}
	
	@Override
	protected String getFilterPlaceholder() {
		return "Filter notebooks";
	}
	
	private SearchResults<NotebookEntry> getEntries() {
		if (entries == null) {
			try {
//				entries = new EntrySearch(getSessionBean())
//					.run(getUserEmail(), 
//							null, 
//							getFilter(), 
//							getPage() * getPerPage(), 
//							getPerPage());
				entries = NotebookEntryGateway.searchEntries(this.getSessionBean(), getFilter(), 
						getFilter2(), getPage(), getPerPage());
			} catch (Throwable e) {
				LibsLogger.error(CloneEntryPanel.class, "Cannot search for entries", e);
				entries = new SearchResults<NotebookEntry>();
			}
		}
		return entries;
	}
	
	@Override
	protected long getMaxHits() throws IOException {
		return getEntries().maxHits();
	}
	
	@Override
	protected List<Button> getMenu() throws IOException {
		return null;
	}
	
	@Override
	protected int getNumberOfColumns() throws IOException {
		return 4;
	}
	
	@Override
	protected String getColumnName(int col) throws IOException {
		if (col == 0)
			return "Notebook";
		else if (col == 1)
			return "Entry";
		else if (col == 2)
			return "Type";
		else 
			return "Options";
	}
	
	@Override
	protected boolean isExapandable() throws IOException {
		return false;
	}
	
	@Override
	protected boolean isFilterVisible() throws IOException {
		return true;
	}
	
	@Override
	protected boolean isHeaderColumnVisible() throws IOException {
		return true;
	}
	
	@Override
	protected void renderTitle(HtmlCanvas html) throws IOException {
		html
			.write("Clone entry");
	}
	
	@Override
	protected void renderExpandable(int row, HtmlCanvas html) throws IOException {
		
	}
	
	@Override
	protected boolean isFilter2Visible() {
		return true;
	}
	
	@Override
	protected void renderCell(int row, int col, HtmlCanvas html) throws IOException {
		NotebookEntry ne = row % getPerPage() < getEntries().getObjects().size() ?
				getEntries().getObjects().get(row % getPerPage())
				: null;
		
		if (ne == null)
			html
				.write("");
		else if (col == 0)
			html
				.render(new Link(ne.getNotebookName(), 
						new OpComponentClick(NotebookContent.class, 
							UUID.random(),
							"onShow",
							new Params().add("notebookId", ne.getNotebookId().getId()))));
		else if (col == 1) {
			String name = notEmpty(ne.getName()) ? ne.getName() : "";
			
			if (ne instanceof Plot)
				name = ((Plot) ne).getName();
			
			html
				.write(name)
				.write(" (" + ne.getId().toString() + ")");
		} else if (col == 2) {
			html
				.write(StringUtilities.substringLast(ne.getType(), '.'));
		} else {
			html
				.render(new ButtonGroup()
								.addButton(isCloneBtnVisible(), new Button("Clone")
											.setTooltip("Clones the entry and adds it to the current notebook")
											.add(getOnAdd() != null ? getOnAdd()
													.addParamParam("sourceNotebookId", ne.getNotebookId().getId())
													.addParamParam("sourceEntryId", ne.getId().getId()) 
													: null)));
		}
	}

	private OpList getOnAdd() {
		return onAdd;
	}

	private void setOnAdd(OpList onAdd) {
		this.onAdd = onAdd;
	}

	@Override
	protected String getTableCss() throws IOException {
		return null;
	}

	public boolean isCloneBtnVisible() {
		return cloneBtnVisible;
	}

	public CloneEntryPanel setCloneBtnVisible(boolean cloneBtnVisible) {
		this.cloneBtnVisible = cloneBtnVisible;
		return this;
	}


}
