package net.beanscode.model.connectors;

public enum BeansConnectorStatus {
	NEW,
	COMMITTED,
	FAILED
}
