package net.beanscode.model.notebook;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;

import java.io.IOException;
import java.util.Iterator;

import net.beanscode.model.BeansCacheManager;
import net.beanscode.model.cass.TableLocation;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.ResultsIter;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.search.SearchResults;
import net.hypki.libs5.search.query.Bracket;
import net.hypki.libs5.search.query.StringTerm;
import net.hypki.libs5.search.query.TermRequirement;
import net.hypki.libs5.search.query.WildcardTerm;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.weblibs.CacheManager;
import net.hypki.libs5.weblibs.SearchManager;
import net.hypki.libs5.weblibs.WeblibsConst;

import com.google.gson.JsonElement;

public class NotebookEntryFactory {

	public static Iterable<NotebookEntry> iterateNotebookEntry() {
		return iterateNotebookEntry(null);
	}
	
	public static Iterable<NotebookEntry> iterateNotebookEntry(final UUID userId) {
		return new Iterable<NotebookEntry>() {
			@Override
			public Iterator<NotebookEntry> iterator() {
				return new Iterator<NotebookEntry>() {
					private ResultsIter rowsIter = null;
					
					private boolean initiated = false;
					
					private NotebookEntry nextNotebookEntry = null;
					
					@Override
					public NotebookEntry next() {
						if (!initiated)
							hasNext();
						return nextNotebookEntry;
					}
					
					@Override
					public boolean hasNext() {
						if (!initiated) {
							rowsIter = new ResultsIter(WeblibsConst.KEYSPACE, 
									NotebookEntry.COLUMN_FAMILY, 
									new String[]{DbObject.COLUMN_DATA}, null);
							initiated = true;
						}
						
						while (rowsIter.hasNext()) {
							Row row = rowsIter.next();
							
							if (row != null && row.contains(DbObject.COLUMN_DATA)) {
								nextNotebookEntry = JsonUtils.fromJson((String) row.getColumns().get(DbObject.COLUMN_DATA), NotebookEntry.class);
								
								if (userId == null)
									return true;
								else if (nextNotebookEntry.getUserId().equals(userId))
									return true;
							}
						}
						
						return false;
					}
				};
			}
		};
	}
	
	public static SearchResults<NotebookEntry> searchNotebookEntries(UUID notebookId, UUID userId, 
			final String keywordsEntries, int from, int size) throws IOException {
		from = Math.max(0, from);
		size = Math.min(50, size);
		
		net.hypki.libs5.search.query.Query q = new net.hypki.libs5.search.query.Query();
		
		if (userId != null)
			q.addTerm(new StringTerm("userId.id", userId.getId(), TermRequirement.SHOULD));
		
		if (keywordsEntries != null)
			for (String term : keywordsEntries.toLowerCase().split("[^\\w]+")) {
				q.addTerm(new Bracket()
					.addTerm(new WildcardTerm("name", term, TermRequirement.SHOULD))
					.addTerm(new WildcardTerm("title", term, TermRequirement.SHOULD))
					.addTerm(new WildcardTerm("text", term, TermRequirement.SHOULD))
					.addTerm(new WildcardTerm("query", term, TermRequirement.SHOULD))
					.addTerm(new WildcardTerm("id.id", term, TermRequirement.SHOULD)))
				;
			}
		
		if (notebookId != null)
			q.addTerm(new StringTerm("notebookId.id", notebookId.getId(), TermRequirement.SHOULD));
		
		return SearchManager.searchInstance().search(NotebookEntry.class, 
				WeblibsConst.KEYSPACE_LOWERCASE, 
				NotebookEntry.COLUMN_FAMILY.toLowerCase(), 
				q, 
				from, 
				size);
	}
	
	public static SearchResults<NotebookEntry> searchNotebookEntries(String keywordsNotebook, UUID userId, 
			final String keywordsEntries, int from, int size) throws IOException {
		from = Math.max(0, from);
		size = Math.min(50, size);
		
		net.hypki.libs5.search.query.Query q = new net.hypki.libs5.search.query.Query();
		
		if (userId != null)
			q.addTerm(new StringTerm("userId.id", userId.getId(), TermRequirement.SHOULD));
		
		if (keywordsEntries != null)
			for (String term : keywordsEntries.toLowerCase().split("[^\\w]+")) {
				q.addTerm(new Bracket()
					.addTerm(new WildcardTerm("name", term, TermRequirement.SHOULD))
					.addTerm(new WildcardTerm("title", term, TermRequirement.SHOULD))
					.addTerm(new WildcardTerm("text", term, TermRequirement.SHOULD))
					.addTerm(new WildcardTerm("query", term, TermRequirement.SHOULD))
					.addTerm(new WildcardTerm("id.id", term, TermRequirement.SHOULD)))
				;
			}
		
		if (keywordsNotebook != null) {
			for (String term : keywordsNotebook.toLowerCase().split("[^\\w]+")) {
				q.addTerm(new Bracket()
					.addTerm(new WildcardTerm("notebookId.id", term, TermRequirement.SHOULD))
					.addTerm(new WildcardTerm("name", term, TermRequirement.SHOULD))
					.addTerm(new WildcardTerm("description", term, TermRequirement.SHOULD)))
				;
			}
		}
		
		return SearchManager.searchInstance().search(NotebookEntry.class, 
				WeblibsConst.KEYSPACE_LOWERCASE, 
				NotebookEntry.COLUMN_FAMILY.toLowerCase(), 
				q, 
				from, 
				size);
	}

	public static NotebookEntry getNotebookEntry(String entryId) throws IOException {
		return NotebookEntryFactory.getNotebookEntry(new UUID(entryId));
	}

	public static NotebookEntry getNotebookEntry(UUID entryId) throws IOException {
		NotebookEntry neCached = CacheManager
			.cacheInstance()
			.get(NotebookEntry.class, NotebookEntry.class.getSimpleName(), entryId.getId());
		if (neCached != null) {
//			LibsLogger.info(NotebookEntryFactory.class, "" + neCached.getMetaAsString("script").substring(neCached.getMetaAsString("script").indexOf("autoscale"), neCached.getMetaAsString("script").indexOf("autoscale")+20));
			return neCached;
		}
		
		String type = null;
		Object d = null;
		try {
			d = DbObject.getDatabaseProvider()
						.get(WeblibsConst.KEYSPACE, 
								NotebookEntry.COLUMN_FAMILY, 
								new C3Where()
									.addClause(new C3Clause(DbObject.COLUMN_PK, ClauseType.EQ, entryId.getId())),
								DbObject.COLUMN_DATA);
			
			if (d == null) {
				LibsLogger.info(NotebookEntryFactory.class, "NotebookEntry not found in DB, trying search index");
				
				// TODO DEBUG trying to get NotebookEntry from search index
				SearchResults<NotebookEntry> res = searchNotebookEntries((UUID) null, null, entryId.getId(), 0, 1);
				
				LibsLogger.info(NotebookEntryFactory.class, "NotebookEntry search results " + res + ", maxHits " + res.maxHits());
				
				if (res.maxHits() > 0)
					return res.getObjects().get(0);
				
				return null; // NotebookEntry could be removed in the meantime
			}
			
			JsonElement data = JsonUtils.parseJson((String) d);
					
			type = data.getAsJsonObject().get("type").getAsString();
			
			
			// TODO DEBUG fixing some Plot objects
			if (data.getAsJsonObject() != null
					&& data.getAsJsonObject().get("plotStatus") != null
					&& data.getAsJsonObject().get("plotStatus").getAsJsonObject() != null
					&& data.getAsJsonObject().get("plotStatus").getAsJsonObject().get("started") != null
					&& data.getAsJsonObject().get("plotStatus").getAsJsonObject().get("started").isJsonObject()) {
				LibsLogger.info(NotebookEntryFactory.class, "Fixing Plot.plotStatus.started");
				
				data.getAsJsonObject().get("plotStatus").getAsJsonObject().remove("started");
				data.getAsJsonObject().get("plotStatus").getAsJsonObject().addProperty("started", false);
				
				data.getAsJsonObject().get("plotStatus").getAsJsonObject().remove("finished");
				data.getAsJsonObject().get("plotStatus").getAsJsonObject().addProperty("finished", false);
				
				data.getAsJsonObject().get("plotStatus").getAsJsonObject().remove("errorMsg");
				data.getAsJsonObject().remove("tables");
			}
			
//				return (NotebookEntry) JsonUtils.fromJson(data, Class.forName(type));
			NotebookEntry ne = NotebookEntrySerializer.getInstance().deserialize(data, null, null);
			
			// TODO DEBUG remove this later when db will be upgraded
			if (nullOrEmpty(ne.getName())
					&& notEmpty(ne.getMetaAsString("META_TITLE"))) {
				ne.setName(ne.getMetaAsString("META_TITLE"));
			}
			
			updateCache(ne);
			
			return ne;
		} catch (Exception e) {
			LibsLogger.error(NotebookEntry.class, "Cannot cast to class " + type, e);
			return null;
		}
	}

	public static void updateCache(NotebookEntry notebookEntry) {
		CacheManager
			.cacheInstance()
			.put(NotebookEntry.class.getSimpleName(), 
					notebookEntry.getId().getId(), 
					notebookEntry);
	}
}
