package net.beanscode.web.view.plots;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static org.rendersnake.HtmlAttributesFactory.add;
import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.for_;
import static org.rendersnake.HtmlAttributesFactory.id;
import static org.rendersnake.HtmlAttributesFactory.name;
import static org.rendersnake.HtmlAttributesFactory.type;

import java.io.IOException;

import javax.ws.rs.core.SecurityContext;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.notebooks.PlotHistogramEntry;
import net.beanscode.pojo.NotebookEntry;
import net.beanscode.web.BeansWebConst;
import net.beanscode.web.view.notebook.NotebookEntryEditorPanel;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.modals.ModalWindow;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.beanscode.web.view.ui.modals.OpModal;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.buttons.InputButton;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.components.ops.OpComponentTimer;
import net.hypki.libs5.pjf.op.OpAfter;
import net.hypki.libs5.pjf.op.OpDialogClose;
import net.hypki.libs5.pjf.op.OpDisable;
import net.hypki.libs5.pjf.op.OpEnable;
import net.hypki.libs5.pjf.op.OpHide;
import net.hypki.libs5.pjf.op.OpJs;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpRemove;
import net.hypki.libs5.pjf.op.OpSet;
import net.hypki.libs5.pjf.op.OpSetAttr;
import net.hypki.libs5.pjf.op.OpSetVal;
import net.hypki.libs5.pjf.op.OpShow;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.api.APICallType;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.url.Params;

import org.rendersnake.HtmlCanvas;
import org.rendersnake.Renderable;

public class PlotHistogramPanel extends NotebookEntryEditorPanel {
	
	public PlotHistogramPanel() {
		
	}
	
	public PlotHistogramPanel(BeansComponent parent) {
		super(parent);
	}

	@Override
	public Renderable getView() {
		return new Renderable() {
			@Override
			public void renderOn(HtmlCanvas html) throws IOException {
				html
//					.div()
						.div(class_("plot-view")
								.id("plot-view-" + getId())
								.style("width: 95%;")
//								.data("onvisible", new OpComponentClick(PlotHistogramPanel.this, "onRefresh", new Params().add("plotId", getPlot().getId())).toStringOnclick())
								)
							.write("Plot '" + getPlot().getTitle() + "' is not yet ready")
//							.write(new OpComponentTimer(PlotHistogramPanel.this, 
//										"onRefresh", 
//										RandomUtils.nextInt(1000, 3000),
//										new Params()
//											.add("plotId", getPlot().getId()))
//								.toStringAdhock(), false)
						._div()
						.div(id("plot-legend-" + getId())
								.class_("dygraph-legend-beans"))
						._div()
						.div(id("plot-logbuttons-" + getId())
								.class_("dygraph-legend-beans"))
						._div()
//					._div()
					
					
					;
			}
		};
	}

	@Override
	protected Renderable getEditor() {
		return new Renderable() {
			@Override
			public void renderOn(HtmlCanvas html) throws IOException {
				html
					.div(class_("plot-query plot-histogram form-group"))
						.label(for_("plot-title"))
							.content("Title")
						.input(name("plot-title")
								.type("text")
								.id("plot-title")
								.class_("title form-control")
								.value(notEmpty(getPlot().getTitle()) ? getPlot().getTitle() : "Plot title")
								.add("placeholder", "Title of the plot"))
						.div(class_("row"))
							.div(class_("col-sm-6"))
								.label(for_("plot-ds-query"))
									.content("Read data from Datasets ")
								.input(name("plot-ds-query")
										.type("text")
										.class_("query form-control")
										.id("plot-ds-query")
										.add("placeholder", "datasets filter...")
										.value(getPlot().getDsQuery()))
							._div()
							.div(class_("col-sm-6"))
								.label(for_("plot-tb-query"))
									.content("from Tables")
								.input(name("plot-tb-query")
										.type("text")
										.class_("query form-control")
										.id("plot-tb-query")
										.add("placeholder", "tables filter")
										.value(getPlot().getTbQuery()))
							._div()
						._div()
								
						.div(class_("row"))
							.div(class_("col-sm-6"))
								.label(for_("plot-column"))
										.content("Plot column")
								.input(name("plot-column")
										.type("text")
										.class_("query form-control")
										.id("plot-column")
										.add("placeholder", "column (or math expression) to plot")
										.value(getPlot().getXNotNull()))
							._div()
							.div(class_("col-sm-6"))
								.label(for_("plot-weblibs"))
										.content("with weight")
								.input(name("plot-weight")
										.type("text")
										.class_("query form-control")
										.id("plot-weight")
										.add("placeholder", "weight (or math expression, default 1.0)")
										.value(getPlot().getYNotNull()))
							._div()
						._div()
								
						.div(class_("row"))
							.div(class_("col-sm-12"))
								.label(for_("name"))
										.content("X label")
								.input(name("plot-xlabel")
										.type("text")
										.class_("query form-control")
										.add("placeholder", "Label for X axis")
										.value(getPlot().getXLabelNotNull()))
							._div()
						._div()
									
						.div(class_("row"))
							.div(class_("col-sm-12"))
								.label(class_("name"))
										.content("Y label")
								.input(name("plot-ylabel")
										.type("text")
										.class_("query form-control")
										.add("placeholder", "Label for Y axis")
										.value(getPlot().getYLabelNotNull()))
							._div()
						._div()
								
						.div(class_("row check"))
							.div(class_("col-sm-3 custom-control custom-checkbox"))
								.input(type("checkbox")
										.class_("custom-control-input")
//										.type("text")
										.id("plot-width-on")
										.name("plot-width-on")
										.checked(getPlot().getBinWidth() != 0.0 ? "checked" : null)
										.onClick(new OpComponentClick(PlotHistogramPanel.this, 
												"onTogglePlotWidth",
												new Params().add("plotId", getPlot().getId().getId()))
											.toStringOnclick()))
								.label(for_("plot-width-on")
										.class_("custom-control-label"))
									.content("split by width")
							._div()
							.div(class_("col-sm-9"))
								.input(name("plot-width")
										.type("text")
										.id("plot-width")
										.class_("plot-width form-control")
										.add("placeholder", "Bin width (leave empty to do this automatically for 10 bins)")
										.value(String.valueOf(getPlot().getBinWidth()))
										.disabled(getPlot().getBinWidth() != 0.0 ? null : "disabled"))
							._div()
						._div()
								
						.div(class_("row check"))
							.div(class_("col-sm-3 custom-control custom-checkbox"))
								.input(type("checkbox")
										.class_("custom-control-input")
//										.type("text")
										.id("plot-colorby-on")
										.name("plot-colorby-on")
										.checked(notEmpty(getPlot().getColorBy()) ? "checked" : null)
										.onClick(new OpComponentClick(PlotHistogramPanel.this, 
												"onTogglePlotColorBy",
												new Params().add("plotId", getPlot().getId().getId()))
											.toStringOnclick()))
								.label(for_("plot-colorby-on")
										.class_("custom-control-label"))
									.content("color by column")
							._div()
							.div(class_("col-sm-9"))
								.input(name("plot-colorby")
										.type("text")
										.id("plot-colorby")
										.class_("plot-colorby form-control")
										.add("placeholder", "Column name")
										.value(notEmpty(getPlot().getColorBy()) ? getPlot().getColorBy() : "")
										.disabled(notEmpty(getPlot().getColorBy()) ? null : "disabled"))
							._div()
						._div()
								
						.div(class_("row check"))
							.div(class_("col-sm-3 custom-control custom-checkbox"))
								.input(type("checkbox")
										.class_("custom-control-input")
	//									.type("text")
										.id("plot-splitby-on")
										.name("plot-splitby-on")
	//									.class_("form-control")
										.checked(notEmpty(getPlot().getSplitBy()) ? "checked" : null)
										.onClick(new OpComponentClick(PlotHistogramPanel.this, 
												"onTogglePlotSplitBy",
												new Params().add("plotId", getPlot().getId().getId()))
											.toStringOnclick()))
								.label(for_("plot-splitby-on")
										.class_("custom-control-label"))
									.content("split by column")
							._div()
							.div(class_("col-sm-9"))
								.input(name("plot-splitby")
										.type("text")
										.id("plot-splitby")
										.class_("plot-splitby form-control ")
										.add("placeholder", "Column name")
										.value(notEmpty(getPlot().getSplitBy()) ? getPlot().getSplitBy() : "")
										.disabled(notEmpty(getPlot().getSplitBy()) ? null : "disabled"))
							._div()
						._div()
					._div()
					;
			}
		};
	}
	
	public OpList changePlot(Params params) throws IOException {
//		final String plotId = params.getString("plotId");
		
//		setNotebookEntry(NotebookEntry.getNotebookEntry(new UUID(plotId)));
		
		int plotNr = params.getInteger("plotNr", 0);
		int readPlotNrFromInput = params.getInteger("readPlotNrFromInput", 0);
		
		if (readPlotNrFromInput > 0)
			plotNr = params.getInteger("plotNrInput", 0);
		
		boolean noMorePlots = false;
		if (plotNr >= getPlot().getPlotStatus().getMaxPlots()) {
			// check if the plotNr actually exists
			LibsLogger.debug(PlotPanel.class, "PlotNr ", plotNr, " does not exist, subtracting by 1");
			plotNr = getPlot().getPlotStatus().getMaxPlots() - 1;
			noMorePlots = true;
		}
		
		final int previous = plotNr > 0 ? plotNr - 1 : 0;
		final int next = plotNr >= 0 ? plotNr + 1 : 1;
		
		boolean prevBtnEnable = true;
		boolean nextBtnEnable = true;
		
		if (!getPlot().isSplitBySet())
			prevBtnEnable = false;
		if (!getPlot().isSplitBySet() || noMorePlots)
			nextBtnEnable = false;
		
		return new OpList()
			.add(new OpSetAttr("#" + getId() + " .previous", "onclick", 
					new OpComponentClick(this, "changePlot", 
						new Params()
							.add("plotId", getPlot().getId())
							.add("plotNr", previous)
						).toStringOnclick()
						))
			.add(new OpSetAttr("#" + getId() + " .next", "onclick", 
					new OpComponentClick(this, "changePlot", 
						new Params()
							.add("plotId", getPlot().getId())
							.add("plotNr", next)
						).toStringOnclick()
						))
			// enable/disable next/previous buttons
			.add(prevBtnEnable ? new OpEnable("#" + getId() + " .previous") : new OpDisable("#" + getId() + " .previous"))
			.add(nextBtnEnable ? new OpEnable("#" + getId() + " .next")		: new OpDisable("#" + getId() + " .next"))
			.add(noMorePlots ? new OpBeansInfo("There are no more plots in this figure") : null)
			.add(new OpComponentClick(this, "onRefresh", new Params().add("plotId", getPlot().getId()).add("plotNr", plotNr).add("dummy", UUID.random().getId())))
			;
	}
	
	private OpList onTogglePlotSplitBy() {
		boolean splitByOn = getParams().getBoolean("plot-splitby-on");
		
		return new OpList()
			.add(splitByOn, new OpEnable("#" + getId() + " .plot-splitby"))
			.add(!splitByOn, new OpDisable("#" + getId() + " .plot-splitby"));
	}
	
	private OpList onTogglePlotColorBy() {
		boolean colorByOn = getParams().getBoolean("plot-colorby-on");
		
		return new OpList()
			.add(colorByOn, new OpEnable("#" + getId() + " .plot-colorby"))
			.add(!colorByOn, new OpDisable("#" + getId() + " .plot-colorby"));
	}
	
	private OpList onTogglePlotWidth() {
		boolean widthOn = getParams().getBoolean("plot-width-on");
		
		return new OpList()
			.add(widthOn, new OpEnable("#" + getId() + " .plot-width"))
			.add(!widthOn, new OpDisable("#" + getId() + " .plot-width"));
	}

	@Override
	protected Button getMenu() throws IOException {
		return new Button()
				.addButton(new Button()
					.setHtmlClass("refresh")
					.setAwesomeicon("fa fa-sync-alt")
					.setTooltip("Refresh")
					.add(new OpComponentClick(this, 
							"onRefresh", 
							new Params()
								.add("plotId", getPlot().getId())))
					)
//				.addButton(new Button(" ")
//					.setEnabled(false)
//						.setSeparator(true)
//						.setSeparatorVertical(true)
//						)
				.addButton(new Button()
					.setHtmlClass("previous")
					.setAwesomeicon("fa fa-backward")
					.setTooltip("Previous plot")
					.add(new OpComponentClick(this, 
							"changePlot", 
							new Params()
								.add("plotId", getPlot().getId())
								.add("plotNr", 0)))
					)
				.addButton(new InputButton("plotNrInput")
					.add(new OpComponentClick(this, 
							"changePlot", 
							new Params()
								.add("plotId", getPlot().getId())
								.add("readPlotNrFromInput", 1)))
					)
				.addButton(new Button()
					.setHtmlClass("next")
					.setAwesomeicon("fa fa-forward")
					.setTooltip("Next plot")
					.add(new OpComponentClick(this, 
							"changePlot", 
							new Params()
								.add("plotId", getPlot().getId())
								.add("plotNr", 1)))
					)
				.addButton(new Button()
					.setHtmlClass("splits-select")
					.setAwesomeicon("fa fa-list-ul")
					.setTooltip("List")
					.add(new OpComponentClick(this, 
							"onSplitSelect", 
							new Params()
								.add("plotId", getPlot().getId())))
					)
			;
	}
	
	private OpList onSplitSelect() throws IOException {
		SplitSelectPanel splitsPanel = (SplitSelectPanel) new SplitSelectPanel(getPlot());
		
		splitsPanel
			.getOnClick()
				.add(new OpComponentClick(this, "onShowSplit", getParams()))
				.add(new OpDialogClose());
		
		ModalWindow modal = new ModalWindow(splitsPanel)
			.setContent(splitsPanel)
			.setTitle("Selecting plot");
		
		return new OpList()
			.add(new OpModal(modal))
//			.add(new OpDialogOpen("Select split to show", 
//						splitsPanel.toHtml())
//					.setOnOKBtnCaption("Close"))
			;
	}
	
	private OpList onShowSplit() {
		String toShow = getParams().getString("toShow");
		
		return new OpList()
			.add(new OpSetVal("#" + getId() + " .plotNrInput", toShow))
			.add(new OpComponentClick(this, 
					"onRefresh", 
					getParams()
						.add("plotNr", toShow)));
	}
	
	@Override
	public OpList getInitOpList() {
		return super.getInitOpList()
//				.add(new OpComponentTimer(this, "onRefresh", RandomUtils.nextInt(1, 2000), new Params().add("plotId", getPlot().getId())))
				;
	}
	
	@Override
	protected OpList onRefresh(SecurityContext sc, Params params) throws IOException {
		int plotNr = params.getInteger("plotNr", 0);
		OpList ops = new OpList();

		if (getPlot().getPlotStatus().isFinished() == false) {
			return ops
					.add(new OpSet("#" + getId() + " .plot-view", new HtmlCanvas()
																		.img(add("src", "/static/res/ajax-loader-1.gif", false))
																		.write(" ")
																		.span()
																			.content("Plot '" + getPlot().getTitle() + "' is being prepared...")
																		.toHtml()))
					.add(new OpComponentTimer(this, "onRefresh", BeansWebConst.PLOT_AUTOREFRESH_INTERVAL_MS, new Params().add("plotId", getPlot().getId()).add("plotNr", plotNr)))
					;
		}
					
		HtmlCanvas html = new HtmlCanvas();
		html
			.div(class_("chart parcoords")
					.id("ch" + getPlot().getId().getId())
					.add("style", "height:440px;"))
			._div();
		
		ops
			.add(new OpSetVal("#" + getId() + " .plotNrInput", plotNr));
		
		String idShort = "ch" + getPlot().getId().getId();
		return ops
			.add(new OpJs("d3.selectAll(\"#" + idShort + " svg\").remove();"))
			.add(new OpRemove("#" + idShort + " svg"))
			.add(new OpRemove("#" + idShort + " "))
			.add(new OpAfter("#" + getId() + " .plot-view", html.toHtml()))
			.add(new OpRemove("#" + idShort + " .chart-title"))
			.add(new OpSet("#" + getId() + " .plot-view", ""))
			.add(new OpJs(WebPlotsFactory.createBoxes(getPlot(), plotNr)))
			.add(new OpShow("#" + getId() + " .plotNrInput"))
			.add(new OpShow("#" + getId() + " .splits-select"))
			;
			
	}
		
	@Override
	public NotebookEntry getNotebookEntry() {
		if (super.getNotebookEntry() == null) {
			final String plotId = getParams().getString("plotId");
			try {
				setNotebookEntry(new ApiCallCommand(getSessionBean())
										.setApi("api/notebook/entry/get")
										.addParam("entryId", plotId)
										.run()
										.getResponse()
										.getAsObject(PlotHistogramEntry.class));
			} catch (IOException e) {
				LibsLogger.error(PlotHistogramPanel.class, "Cannot get Notebook entry from DB", e);
			}
		}
		return super.getNotebookEntry();
	}

	@Override
	protected OpList opOnPlay(SecurityContext sc, Params params) throws IOException {
		try {
			new ApiCallCommand(getSessionBean())
				.setApi("api/notebook/entry")
				.setApiCallType(APICallType.POST)
				.addParam("entryId", getPlot().getId())
				.addParam("name", getParams().getString("plot-title"))
				.addParam("META_DS_QUERY", getParams().getString("plot-ds-query"))
				.addParam("META_TB_QUERY", getParams().getString("plot-tb-query"))
				.addParam("META_X_COLUMN", getParams().getString("plot-column"))
				.addParam("META_Y_COLUMN", getParams().getString("plot-weight"))
				.addParam("META_XLABEL", getParams().getString("plot-ylabel"))
				.addParam("META_YLABEL", getParams().getString("plot-title"))
				.addParam("META_BINWIDTH", getParams().getBoolean("plot-width-on") ? 
						getParams().getDouble("plot-width", 0.0) : 0.0)
				.addParam("META_COLORBY", getParams().getBoolean("plot-colorby-on") ? 
						getParams().getString("plot-colorby") : null)
				.addParam("META_SPLITBY", getParams().getBoolean("plot-splitby-on") ? 
						getParams().getString("plot-splitby") : null)
				.run();
			
			new ApiCallCommand(getSessionBean())
				.setApi("api/notebook/entry/start")
				.addParam("entryId", getPlot().getId())
				.run();
		} catch (IOException e) {
			LibsLogger.error(PlotHistogramPanel.class, "Cannot save notebook entry", e);
		}
		
		return new OpList()
			.add(new OpComponentClick(this, "onRefresh", new Params().add("plotId", getPlot().getId())))
			.add(new OpShow("#" + getId() + " .refresh"))
			.add(new OpShow("#" + getId() + " .previous"))
			.add(new OpShow("#" + getId() + " .next"))
			.add(new OpShow("#" + getId() + " .plotNrInput"))
			.add(new OpShow("#" + getId() + " .splits-select"))
			;
	}
	
	@Override
	protected OpList opOnEdit(SecurityContext sc, Params params) throws IOException {
		return new OpList()
			.add(new OpHide("#" + getId() + " .refresh"))
			.add(new OpHide("#" + getId() + " .previous"))
			.add(new OpHide("#" + getId() + " .next"))
			.add(new OpHide("#" + getId() + " .plotNrInput"))
			.add(new OpHide("#" + getId() + " .splits-select"))
			;
	}

	@Override
	protected OpList opOnStop(SecurityContext sc, Params params) throws IOException {
		boolean refresh = params.get("plot-type") != null;
		return new OpList()
			.add(refresh, new OpComponentClick(this, "onRefresh", new Params().add("plotId", getPlot().getId())))
			.add(new OpShow("#" + getId() + " .refresh"))
			.add(new OpShow("#" + getId() + " .previous"))
			.add(new OpShow("#" + getId() + " .next"))
			.add(new OpShow("#" + getId() + " .plotNrInput"))
			.add(new OpShow("#" + getId() + " .splits-select"))
			;
	}

	@Override
	protected OpList opOnRemove(SecurityContext sc, Params params) throws IOException {
		try {
			new ApiCallCommand(this)
				.setApi("api/notebook/entry/remove")
				.setApiCallType(APICallType.DELETE)
				.addParam("entryId", params.getString("entryId"))
				.run();
		} catch (IOException e) {
			LibsLogger.error(PlotHistogramPanel.class, "Cannot remove Notebook Entry");
		}
		
		return new OpList();
	}
	
	public PlotHistogramEntry getPlot() {
		return (PlotHistogramEntry) getNotebookEntry();
	}
}
