package net.beanscode.model.scripts;

import java.util.ArrayList;
import java.util.List;

import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class Script {

	@Expose
	@NotNull
	@NotEmpty
	private List<Part> parts = null;
	
	public Script() {
		
	}

	public List<Part> getParts() {
		if (parts == null)
			parts = new ArrayList<>();
		return parts;
	}

	private void setParts(List<Part> scriptParts) {
		this.parts = scriptParts;
	}
}
