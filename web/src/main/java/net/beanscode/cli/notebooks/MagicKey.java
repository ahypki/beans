package net.beanscode.cli.notebooks;

import com.google.gson.annotations.Expose;

import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

public class MagicKey {

	@Expose
	@NotNull
	@NotEmpty
	private UUID id = null;
	
	@Expose
	@NotNull
	@NotEmpty
	private UUID beansObjectId = null;
	
	public MagicKey() {
		
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public UUID getBeansObjectId() {
		return beansObjectId;
	}

	public void setBeansObjectId(UUID beansObjectId) {
		this.beansObjectId = beansObjectId;
	}
}
