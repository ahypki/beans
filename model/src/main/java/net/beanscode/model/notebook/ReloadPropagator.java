package net.beanscode.model.notebook;

public enum ReloadPropagator {
	BLOCK,
	NONBLOCK
}
