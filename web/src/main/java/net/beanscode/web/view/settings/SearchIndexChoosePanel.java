package net.beanscode.web.view.settings;

import java.io.IOException;
import java.util.List;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.plugin.PluginGetList;
import net.beanscode.cli.plugin.PluginPOJO;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.beanscode.web.view.ui.modals.OpModalQuestion;
import net.beanscode.web.view.ui.remarks.ErrorRemark;
import net.beanscode.web.view.ui.remarks.InfoRemark;
import net.beanscode.web.view.ui.remarks.WarningRemark;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.buttons.ButtonGroup;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.utils.url.Params;

import org.rendersnake.HtmlAttributesFactory;
import org.rendersnake.HtmlCanvas;

import com.google.gson.JsonElement;

public class SearchIndexChoosePanel extends BeansComponent {

	public SearchIndexChoosePanel() {
		
	}
	
	public SearchIndexChoosePanel(BeansComponent parent) {
		super(parent);
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		
		List<PluginPOJO> plugins = new PluginGetList(getSessionBean()).run((String) null);
		
		ApiCallCommand cmd = (ApiCallCommand) new ApiCallCommand(getSessionBean())
			.setApi("api/plugin/search/list")
			.run();
		
		for (JsonElement je : cmd.getResponse().getAsJson().getAsJsonArray()) {
			String plugin = je.getAsJsonObject().get("plugin").getAsString();
			String clazz = je.getAsJsonObject().get("clazz").getAsString();
			
			html
				.div(HtmlAttributesFactory.style("margin: 30px;"))
					.h4()
						.content(plugin)
					.p()
						.content(getPluginPOJO(plugins, plugin).getDescription())
//					.span()
//						.content(plugin.getClazz())
//					.br()
					.render(new ButtonGroup()
							.addButton(new Button("Select " + plugin)
									.add(new OpModalQuestion("Changing database driver", 
											"Do you want to change the database driver?", 
											new OpComponentClick(this, 
													"onSelect",
													new Params().add("driver", clazz))))))
				._div();
		}

		html
			.render(new WarningRemark("Remember", "Please remember about reloading the search index after "
					+ "changing the driver."));
	}
	
	private OpList onSelect() throws CLIException, IOException {
		String newDriver = getParams().getString("driver");
		
		new ApiCallCommand(getSessionBean())
			.setApi("api/admin/search/provider")
			.addParam("--clazz", newDriver)
			.run();
		
		return new OpList()
			.add(new OpBeansInfo("Search enging provider changed"))
//			.add(new OpRedirect("/login"))
			;
	}
	
	private PluginPOJO getPluginPOJO(List<PluginPOJO> plugins, String name) {
		for (PluginPOJO pluginPOJO : plugins) {
			if (pluginPOJO.getName().equals(name))
				return pluginPOJO;
		}
		return null;
	}
	
}
