package net.beanscode.model.unittests.connectors;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.beanscode.model.connectors.ConnectorList;
import net.beanscode.model.connectors.H5Connector;
import net.beanscode.model.plugins.ColumnDef;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.Watch;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.string.RandomUtils;
import net.hypki.libs5.utils.system.Basher2;

import org.junit.Test;

public class H5ConnectorTestReadingTestCase extends ConnectorTestCase {

	public H5ConnectorTestReadingTestCase() {
		
	}
	
	@Test
	public void testConnector() throws IOException {
		// broken files
		FileExt h5File = new FileExt("./81f873ab247b1e0160952ae4326ee930-d32b8aa9b7c73fda935079cf39d82187.h5");
//		FileExt h5File = new FileExt("./d5f9ce12e0b333b44b8db081ddb196c0-ca1f9b92872f8ab7e1ff7047909c0214.h5");

		// OK files
//		FileExt h5File = new FileExt("$HOME/beans/model/test.h5");
		
//		for (String line : new Basher2()
//				.add("h5dump")
//				.add(h5File.getAbsolutePath())
//				.runIter()) {
//			LibsLogger.info(H5ConnectorTestReadingTestCase.class, line);
//		}
		;
		
		H5Connector conn = new H5Connector(UUID.random(), h5File)
			.setUseCache(true);
		
		try {
			for (Row row : conn.iterateRows(null)) {
				LibsLogger.info(H5ConnectorTestReadingTestCase.class, "Row ", row);
			}
		} catch (Throwable t) {
			LibsLogger.error(H5ConnectorTestReadingTestCase.class, "Sraka", t);
		}
		
		LibsLogger.info(H5ConnectorTestReadingTestCase.class, "Finished");
	}
}
