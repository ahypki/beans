package net.beanscode.web.view.settings;

import java.io.IOException;

import org.rendersnake.HtmlCanvas;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.CardPanel;
import net.beanscode.web.view.ui.Dl;

public class SearchEngineStatusCard extends CardPanel {

	public SearchEngineStatusCard() {
		
	}
	
	public SearchEngineStatusCard(BeansComponent parent) {
		super(parent);
	}

	@Override
	protected void renderTitle(HtmlCanvas html) throws IOException {
		html
			.span()
				.content("Search engine status");
	}

	@Override
	protected void renderContent(HtmlCanvas html) throws IOException {
		ApiCallCommand apiCall = (ApiCallCommand) new ApiCallCommand(getSessionBean())
			.setApi("api/admin/status")
			.run();
		
		html
			.render(new Dl(this)
					.add("Search engine", apiCall.getResponse().getAsJson().getAsJsonObject().get("SearchIndexProvider").getAsString())
					.add("Status", "OK"));
	}

	@Override
	protected void renderFooter(HtmlCanvas html) throws IOException {
		
	}
	
	
}
