package net.beanscode.pojo.tables;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class ConnectorList implements Serializable{
	
	@Expose
	@NotNull
	@NotEmpty
	private List<ConnectorLink> connectorsLinks = null;
	
	public ConnectorList() {
		
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		if (connectorsLinks != null)
			for (ConnectorLink connectorLink : connectorsLinks) {
				sb.append(connectorLink + ";\n");
			}
		return sb.toString();
	}

	public List<ConnectorLink> getConnectorsLinks() {
		if (connectorsLinks == null)
			connectorsLinks = new ArrayList<>();
		return connectorsLinks;
	}

	private void setConnectorsLink(List<ConnectorLink> connectorsLinks) {
		this.connectorsLinks = connectorsLinks;
	}

	private boolean exists(ConnectorLink connectorLink) {
		for (ConnectorLink cl : getConnectorsLinks()) {
			return cl.equals(this);
		}
		return false;
	}

	public int size() {
		return connectorsLinks != null ? connectorsLinks.size() : 0;
	}
}
