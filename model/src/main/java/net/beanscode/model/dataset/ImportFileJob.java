package net.beanscode.model.dataset;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;
import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.File;
import java.io.IOException;

import com.google.gson.annotations.Expose;

import net.beanscode.model.BeansSettings;
import net.beanscode.model.connectors.PlainConnector;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.DateUtils;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.file.FileUtils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.utils.AssertUtils;
import net.hypki.libs5.weblibs.jobs.Job;
import net.sf.oval.constraint.NotNull;

public class ImportFileJob extends Job {
	
	@Expose
	@NotNull
	private UUID tbId = null;
	
	@Expose
	@NotNull
	private String fileToImport = null;

	public ImportFileJob() {
		
	}
	
	public ImportFileJob(UUID tableId, File fileToImport) {
		setTbId(tableId);
		setFileToImport(fileToImport.getAbsolutePath());
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		try {
			Table table = TableFactory.getTable(getTbId());
			
			if (table.getConnectorList().getConnectorsLinks().size() > 0
					&& table.getConnectorList().getConnectorsLinks().get(0).getConnectorClass().equals(PlainConnector.class.getName())) {
				final String plainPath = BeansSettings.getConnectorDefaultFolder().getAbsolutePath();
				final FileExt plainOutFile = new FileExt(plainPath, table.getId().getId());
				
				assertTrue(notEmpty(plainPath), "Default folder for ", PlainConnector.class.getSimpleName(), " is not specified");
				assertTrue(new FileExt(plainPath).exists(), "Folder ", plainPath, " does not exist");
				
				// importing is just moving the file to a different place
				FileUtils.moveFile(new FileExt(getFileToImport()), plainOutFile);
				
				table.getConnectorList().getConnectorsLinks().get(0).setConnectorInitParams(new PlainConnector(plainOutFile).getInitParams());
//				table.updateColumnDefs();
			} else {
				table.importFile(new File(getFileToImport()));
			}
			
			table.save();
		} catch (Exception e) {
			// TODO do I remove the import files?
			LibsLogger.error(ImportFileJob.class, "Cannot import file to DB, cancelling this job", e);
			
//			LibsLogger.error(ImportFileJob.class, "Cannot import file to DB, postponing for 1 min...", e);
//			setPostponeMs(1000 * 60);
		}
	}

	public UUID getTbId() {
		return tbId;
	}

	public void setTbId(UUID tbId) {
		this.tbId = tbId;
	}

	public String getFileToImport() {
		return fileToImport;
	}

	public void setFileToImport(String fileToImport) {
		this.fileToImport = fileToImport;
	}
}
