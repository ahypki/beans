package net.beanscode.web.view.settings;

import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.id;
import static org.rendersnake.HtmlAttributesFactory.name;

import java.io.IOException;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.CardPanel;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.beanscode.web.view.ui.modals.OpModalQuestion;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.pjf.OpsFactory;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.buttons.ButtonGroup;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpList;

import org.rendersnake.HtmlAttributesFactory;
import org.rendersnake.HtmlCanvas;

public class StressTestingPanel extends CardPanel {

	public StressTestingPanel() {
		
	}
	
	private OpList onStart() throws CLIException, IOException {
		final int datasetCount = getParams().getInteger("dataset-count", 1);
		final int tableCount = getParams().getInteger("table-count", 1);
		final int tableRows = getParams().getInteger("table-rows", 1);
		final int tableColumns = getParams().getInteger("table-columns", 1);
		
		new ApiCallCommand(getSessionBean())
			.setApi("api/demo/stress")
			.addParam("datasetCount", datasetCount)
			.addParam("tableCount", tableCount)
			.addParam("tableRows", tableRows)
			.addParam("tableColumns", tableColumns)
			.run();
		
		return new OpList()
			.add(new OpBeansInfo("Stress testing started"));
	}

	@Override
	protected void renderTitle(HtmlCanvas html) throws IOException {
		html
			.h1()
				.content("Stress testing");
	}

	@Override
	protected void renderContent(HtmlCanvas html) throws IOException {
		html
			.div(id(getId()))
				.div()
					.h4()
						.content("Creating datasets")
					.span()
						.content("Generate ")
					.input(name("dataset-count")
							.value(50 + ""))
					.span()
						.content(" datasets, with ")
					.input(name("table-count")
							.value(10 + ""))
					.span()
						.content(" tables per dataset. Every table should have ")
					.input(name("table-rows")
							.value(1_000_000 + ""))
					.span()
						.content(" rows and ")
					.input(name("table-columns")
							.value(10 + ""))
					.span()
						.content(" columns per table.")
				._div()
				.div(class_("strestests-buttons"))
					.render(new ButtonGroup()
						.addButton(new Button("Start")
								.add(new OpModalQuestion("Stress testing", 
										"Are you sure to start stress testing? If you have chosen large values for "
										+ "parameters it can take some time to complete.", 
										new OpComponentClick(this, 
												"onStart",
												null)))))
				._div()
			._div();
	}

	@Override
	protected void renderFooter(HtmlCanvas html) throws IOException {
		html
			.span()
				.content("Stress testing creates objects with prefix [Stress testing]. Test first with low parameters. ");
	}
}
