package net.beanscode.web.view.users;

import static net.beanscode.cli.BeansCliCommand.API_PERMISSION_GET;
import static net.beanscode.cli.BeansCliCommand.API_PERMISSION_SET;
import static org.rendersnake.HtmlAttributesFactory.for_;
import static org.rendersnake.HtmlAttributesFactory.type;

import java.io.IOException;
import java.util.List;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.permissions.Permission;
import net.beanscode.cli.users.Group;
import net.beanscode.cli.users.GroupSearchResults;
import net.beanscode.pojo.NotebookEntry;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.BetaTableComponent;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.beanscode.web.view.ui.modals.OpModalQuestion;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.buttons.ButtonGroup;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpCheck;
import net.hypki.libs5.pjf.op.OpDisable;
import net.hypki.libs5.pjf.op.OpEnable;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.utils.LibsLogger;

import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

public class PermissionBetaTableComponent extends BetaTableComponent {

	@Expose
	private UUID permissionId = null;
	
	private GroupSearchResults groups = null;
	
	private Permission permission = null;
	
	@Expose
	private String permissionClass = null;
	
	public PermissionBetaTableComponent() {
		
	}
	
	public PermissionBetaTableComponent(BeansComponent parent, NotebookEntry notebookEntry) {
		super(parent);
		setPermissionId(notebookEntry.getId());
		setPermissionClass(notebookEntry.getClass().getName());
	}
	
	public PermissionBetaTableComponent(BeansComponent parent, UUID permissionId, String permissionClass) {
		super(parent);
		setPermissionId(permissionId);
		setPermissionClass(permissionClass);
	}
	
	@Override
	protected String getSettingsPrefix() {
		return "permissions-table-";
	}
	
	@Override
	protected long getMaxHits() throws IOException {
		return getGroups()
				.getMaxHits();
	}
	
	@Override
	protected boolean isExapandable() throws IOException {
		return false;
	}
	
	@Override
	protected void renderExpandable(int row, HtmlCanvas html)
			throws IOException {
		
	}

	public GroupSearchResults getGroups() {
		if (groups == null) {
			try {
				ApiCallCommand call = (ApiCallCommand) new ApiCallCommand(this)
					.setApi("api/group/query")
					.addParam("query", getFilter())
					.addParam("from", getPage() * getPerPage())
					.addParam("size", getPerPage())
					.run();
//				groups = new GroupSearch(getSessionBean()).run(filter, 0, 1000);
				
				groups = new GroupSearchResults();
				groups.getGroups().addAll(call.getResponse().getAsList("groups", Group.class));
				groups.setMaxHits(call.getResponse().getAsLong("maxHits"));
			} catch (CLIException | IOException e) {
				LibsLogger.error(PermissionPanel.class, "Cannot search for groups", e);
				groups = new GroupSearchResults();
			}
		}
		return groups;
	}
	
	public Permission getPermission() {
		if (permission == null && permissionId != null) {
			try {
				permission = new ApiCallCommand(this)
									.setApi(API_PERMISSION_GET)
									.addParam("id", getPermissionId().getId())
									.addParam("class", getPermissionClass())
									.run()
									.getResponse()
									.getAsObject(Permission.class);
			} catch (Exception e) {
				LibsLogger.debug(PermissionPanel.class, "Cannot get permission from DB, creating one");
			}
			
//			if (permission == null) {
//				try {
//					LibsLogger.debug(PermissionPanel.class, "Creating the default Permission object for ", getPermissionId());
//					new PermissionSet(getSessionBean()).run(getPermissionId().getId(), getPermissionClass(), null, null);
//					
//					permission = new PermissionGet(getSessionBean()).run(getPermissionId().getId(), getPermissionClass());
//				} catch (CLIException | IOException e) {
//					LibsLogger.error(PermissionPanel.class, "Cannot create the empty Permission object", e);
//				}
//			}
		}
		return permission;
	}
	
	@Override
	protected boolean isHeaderColumnVisible() throws IOException {
		return true;
	}
	
	@Override
	protected void renderTitle(HtmlCanvas html) throws IOException {
		html
			.write("Permissions");
	}
	
	@Override
	protected void renderCell(int row, int col, HtmlCanvas html)
			throws IOException {
		final Group gr = getGroups().getGroups().get(row % getPerPage());
		if (col == 0)
			html
				.span()
					.content(gr.getNameNotNull());
		else if (col == 1)
			html
				.input(type("checkbox")
						.id(getId() + gr.getId().getId() + "-read")
						.name(gr.getId().getId() + "-read")
						.checked(getPermission().isReadAllowed(gr.getId()) ? "checked" : null)
						.onChange(new OpEnable(".perm-btns").toStringOnclick()))
				.span()
					.content(" ")
				.label(for_(getId() + gr.getId().getId() + "-read"))
					.content("read");
		else if (col == 2)
			html
			.input(type("checkbox")
					.id(getId() + gr.getId().getId() + "-write")
					.name(gr.getId().getId() + "-write")
					.checked(getPermission().isWriteAllowed(gr.getId()) ? "checked" : null)
					.onChange(new OpList()
						.add(new OpCheck("#" + gr.getId().getId() + "-read"))
						.add(new OpEnable(".perm-btns"))
						.toStringOnclick()))
			.span()
				.content(" ")
			.label(for_(getId() + gr.getId().getId() + "-write"))
				.content("write")
			;		
	}
	
	@Override
	protected boolean isFilterVisible() {
		return true;
	}
	
	private OpList onPermissionsSave() throws CLIException, IOException {
		boolean recursive = true;// getParams().getBoolean("recursive", false);
//		List<UUID> readPerms = new ArrayList<>();
//		List<UUID> writePerms = new ArrayList<>();
		
		ApiCallCommand api = new ApiCallCommand(this)
			.setApi(API_PERMISSION_SET)
			.addParam("id", getPermissionId().getId())
			.addParam("class", getPermissionClass());
		
		for (String key : getParams().keys()) {
			if (key.endsWith("-read"))
				api.addParam("readGroups", new UUID(key.substring(0, UUID.LENGTH)));
			else if (key.endsWith("-write"))
				api.addParam("writeGroups", new UUID(key.substring(0, UUID.LENGTH)));
		}
	
		api
			.addParam("recursive", recursive)
			.run();
		
		return new OpList()
			.add(new OpBeansInfo("Permissions saved"))
			.add(!recursive, new OpDisable(".save-changes-perm-btn"))
			.add(recursive, new OpDisable(".apply-all-perm-btn"));
	}
	
	@Override
	protected List<Button> getMenu() throws IOException {
		return new ButtonGroup()
				.addButton(new Button("Save")
					.setHtmlClass("save-changes-perm-btn perm-btns")
					.setEnabled(false)
					.add(new OpModalQuestion("Saving permissions", 
							"Are you sure you want to save permissions?",
							new OpComponentClick(this, "onPermissionsSave"))))
				.getButtons();
	}
	
	@Override
	protected String getColumnName(int col) throws IOException {
		if (col == 0)
			return "Group name";
		else if (col == 1)
			return "Read";
		else if (col == 2)
			return "Write";
		else
			return "";
	}
	
	@Override
	protected int getNumberOfColumns() throws IOException {
		return 3;
	}

	private UUID getPermissionId() {
		return permissionId;
	}

	private void setPermissionId(UUID permissionId) {
		this.permissionId = permissionId;
	}

	public String getPermissionClass() {
		return permissionClass;
	}

	public void setPermissionClass(String permissionClass) {
		this.permissionClass = permissionClass;
	}

	@Override
	protected String getTableCss() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}
}