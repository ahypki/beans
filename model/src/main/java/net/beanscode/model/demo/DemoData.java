package net.beanscode.model.demo;

import java.io.File;
import java.io.IOException;

import net.beanscode.model.BeansConst;
import net.beanscode.model.connectors.CassandraConnector;
import net.beanscode.model.connectors.PlainConnector;
import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.plugins.ColumnDef;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileUtils;
import net.hypki.libs5.utils.reflection.SystemUtils;
import net.hypki.libs5.utils.string.RandomUtils;
import net.hypki.libs5.weblibs.user.User;

public class DemoData {
	
	public static void demoTable(final User user, final Dataset ds, final String tableName, final int from, final int to) throws IOException, ValidationException {
		final Table table = new Table(ds, tableName);
		table.getColumnDefs().add(new ColumnDef(DbObject.COLUMN_PK.toLowerCase(), null, ColumnType.LONG));
		table.getColumnDefs().add(new ColumnDef("tbid", null, ColumnType.STRING));
		table.getColumnDefs().add(new ColumnDef("x", null, ColumnType.DOUBLE));
		table.getColumnDefs().add(new ColumnDef("y", null, ColumnType.DOUBLE));
		table.save();
		
		CassandraConnector data = new CassandraConnector(table);
		for (long row = from; row < to; row++) {
			Row r = new Row(UUID.random().getId());
			r.addColumn("tbid", table.getId().getId());
			r.addColumn("x", 1.0 * row);
			r.addColumn("y", RandomUtils.nextDouble() * 100.0);
			data.write(r);
						
			if (row == (100/3)) {
				LibsLogger.debug(DemoData.class, "In total ", row, " saved");
			}
		}
		data.close();
	}
	
	public static void createDemoDatasetsLines(final User user) throws IOException, ValidationException {
		Dataset ds = new Dataset(user.getUserId(), "Demo lines")
							.save();
		
		File tmpFile = new File("demo-data-" + UUID.random().getId() + ".tmp");
		LibsLogger.debug(DemoData.class, "Tmp file created in ", tmpFile);
		
		FileUtils.saveToFile(SystemUtils.getResourceAsStream(BeansConst.class, "testdata/line-1-10.json"), tmpFile);
		new Table(ds, "Line 1 10")
			.addMeta("from", 1)
			.addMeta("to", 10)
			.addConnector(new PlainConnector(new File("line-1-10.json")))
			.save()
			.importFile(tmpFile);
		
		FileUtils.saveToFile(SystemUtils.getResourceAsStream(BeansConst.class, "testdata/line-20-30.json"), tmpFile);
		new Table(ds, "Line 20 30")
			.addMeta("from", 20)
			.addMeta("to", 30)
			.addConnector(new PlainConnector(new File("line-20-30.json")))
			.save()
			.importFile(tmpFile);
		
		FileUtils.saveToFile(SystemUtils.getResourceAsStream(BeansConst.class, "testdata/line-5-25.json"), tmpFile);
		new Table(ds, "Line 5 25")
			.addMeta("from", 5)
			.addMeta("to", 25)
			.addConnector(new PlainConnector(new File("line-5-25.json")))
			.save()
			.importFile(tmpFile);
		
		FileUtils.saveToFile(SystemUtils.getResourceAsStream(BeansConst.class, "testdata/line-square-1-25.json"), tmpFile);
		new Table(ds, "Line square 1 25")
			.addMeta("from", 1)
			.addMeta("to", 25)
			.addConnector(new PlainConnector(new File("square-1-25.json")))
			.save()
			.importFile(tmpFile);
		
		FileUtils.saveToFile(SystemUtils.getResourceAsStream(BeansConst.class, "testdata/bar.json"), tmpFile);
		new Table(ds, "Bar plot points")
			.addConnector(new PlainConnector(new File("bar.json")))
			.save()
			.importFile(tmpFile);
		
		FileUtils.saveToFile(SystemUtils.getResourceAsStream(BeansConst.class, "testdata/parallel.json"), tmpFile);
		new Table(ds, "Parallel plot points")
			.addConnector(new PlainConnector(new File("parallel.json")))
			.save()
			.importFile(tmpFile);
		
		tmpFile.delete();
	}
	
	public static void createDemoDatasetsMOCCA(final User user) throws IOException, ValidationException {
		// import MOCCA simulations
		importMoccaSim(user, "0.1");
		importMoccaSim(user, "0.2");
		importMoccaSim(user, "0.5");
		
		// import Harris catalogue
		Dataset harris3 = new Dataset(user.getUserId(), "Harris catalogue").save();		
		new Table(harris3, "Harris3")
			.save()
			.importFile(new File(SystemUtils.getResourceURL("testdata/harris3.dat").getPath()));
	}
	
	private static void importMoccaSim(User user, String fracb) throws IOException, ValidationException {
		Dataset dsFracb01 = new Dataset().setName("MOCCA n=3000 fracb=" + fracb).setUserId(user.getUserId()).save();
		
		// importing data to the tables
		new Table(dsFracb01, "system")
			.save()
			.importFile(SystemUtils.getResourceURL("testdata/mocca/n=3000/fracb=" + fracb + "/system.dat.beans").getFile());		

		new Table(dsFracb01, "snapshot")
			.save()
			.importFile(SystemUtils.getResourceURL("testdata/mocca/n=3000/fracb=" + fracb + "/snapshot.dat.beans").getFile());
		
		new Table(dsFracb01, "inter-binevol")
			.save()
			.importFile(SystemUtils.getResourceURL("testdata/mocca/n=3000/fracb=" + fracb + "/inter-binevol.dat.beans").getFile());
		
		new Table(dsFracb01, "inter-sinevol")
			.save()
			.importFile(SystemUtils.getResourceURL("testdata/mocca/n=3000/fracb=" + fracb + "/inter-sinevol.dat.beans").getFile());
		
		new Table(dsFracb01, "inter-binsin")
			.save()
			.importFile(SystemUtils.getResourceURL("testdata/mocca/n=3000/fracb=" + fracb + "/inter-binsin.dat.beans").getFile());
		
		new Table(dsFracb01, "inter-binbin")
			.save()
			.importFile(SystemUtils.getResourceURL("testdata/mocca/n=3000/fracb=" + fracb + "/inter-binbin.dat.beans").getFile());
		
		new Table(dsFracb01, "inter-coll")
			.save()
			.importFile(SystemUtils.getResourceURL("testdata/mocca/n=3000/fracb=" + fracb + "/inter-coll.dat.beans").getFile());
		
		new Table(dsFracb01, "inter-binform")
			.save()
			.importFile(SystemUtils.getResourceURL("testdata/mocca/n=3000/fracb=" + fracb + "/inter-binform.dat.beans").getFile());
		
		new Table(dsFracb01, "escape")
			.save()
			.importFile(SystemUtils.getResourceURL("testdata/mocca/n=3000/fracb=" + fracb + "/escape.dat.beans").getFile());
	}
}
