package net.beanscode.model.cass;

import static net.hypki.libs5.utils.sha.SHAManager.getSHA;

import java.io.IOException;

import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

import net.beanscode.model.BeansSettings;
import net.beanscode.model.plugins.ColumnDefList;
import net.beanscode.model.plugins.Connector;
import net.beanscode.model.settings.Clipboard;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.LocalLogger;

public class UniTableRecordWriter extends RecordWriter {
	
	private Connector connector = null;
	
//	private String location = null;
	
	private ColumnDefList columnDefs = null;
	
	private boolean columnsChecked = false;

	public UniTableRecordWriter(Connector connector, ColumnDefList columnDefs) {
		this.connector = connector;
		this.columnDefs = columnDefs;
	}

	@Override
	public void close(TaskAttemptContext arg0) throws IOException, InterruptedException {
		LibsLogger.info(UniTableRecordWriter.class, "RecordWriter closing ", arg0);
//		LocalLogger.debug("RecordWriter closing ", connector.getInitParams().getMeta().getAsString("path"));
		
		connector.close();
	}

	@Override
	public void write(Object key, Object value) throws IOException, InterruptedException {
//		LibsLogger.info(UniTableRecordWriter.class, "Write ", key, value);
		
		if (!columnsChecked) {
			LibsLogger.debug(UniTableRecordWriter.class, "RecordWriter writer first row ", connector.getInitParams(), 
					columnDefs);
//			LocalLogger.debug("RecordWriter writer first row ", connector.getInitParams().getMeta().getAsString("path"));
			
			columnsChecked = true;
			
			// setting columns definitions
			if (connector.getColumnDefs() == null) {
				if (this.columnDefs == null)
					throw new IOException("ColumnsDefs are undefined in the RecordWriter");
				
				// tbid column name is forbidden
//				ColumnDefList colDefsNoTbid = new ColumnDefList();
//				for (ColumnDef columnDef : columnDefs) {
//					if (columnDef.getName().equals(Table.COLUMN_TBID)) {
//						LibsLogger.warn(UniTableRecordWriter.class, "Column name " + Table.COLUMN_TBID + " is reserved by BEANS and cannot be "
//								+ "used, change your script accordingly.");
//						continue;
//					}
//					
//					colDefsNoTbid.add(columnDef);
//				}
				
				connector.setColumnDefs(columnDefs);
//				connector.setColumnDefs(colDefsNoTbid);
			}
			
			if (BeansSettings.TEST_RECORDWRITER_FAILED_ATTEMPTS) {
				Clipboard.set(getSHA(connector.getInitParams().toString()), "STARTED");
			}
			
		} else {
			if (BeansSettings.TEST_RECORDWRITER_FAILED_ATTEMPTS) {
				String status = (String) Clipboard.get(getSHA(connector.getInitParams().toString()));
				if (status != null 
						&& status.equals("STARTED")
						&& status.equals("DONE") == false) {
					if (connector.getInitParams().toString().contains("_000000_0.h5")) {
						Clipboard.set(getSHA(connector.getInitParams().toString()), "DONE");
						LocalLogger.debug("Crashed on purpose ", this.connector.getInitParams());
						
						throw new RuntimeException("Failing TaskAttempt on purpose");
					}
				}
			}
		}
		
		connector.write((Row) key);
	}
	
	public void setColumnsDef(ColumnDefList columnDefs) {
		this.columnDefs = columnDefs;
	}
}
