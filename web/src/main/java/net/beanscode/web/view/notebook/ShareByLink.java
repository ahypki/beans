package net.beanscode.web.view.notebook;

import static org.rendersnake.HtmlAttributesFactory.href;
import static org.rendersnake.HtmlAttributesFactory.id;

import java.io.IOException;

import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

import net.beanscode.cli.BeansCliCommand;
import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.notebooks.MagicKey;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.Link;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpCopyToClipboard;
import net.hypki.libs5.pjf.op.OpJs;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpSet;
import net.hypki.libs5.utils.api.APICallType;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

public class ShareByLink extends BeansComponent {
	
	@Expose
	@NotNull
	@NotEmpty
	private UUID beansObjectId = null;
	
	private MagicKey magicKeyCache = null;

	public ShareByLink() {
		
	}
	
	public ShareByLink(BeansComponent parent, UUID beansObjectId) {
		super(parent);
		setBeansObjectId(beansObjectId);
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		if (getMagicKeyCache() != null) {
			String url = "/notebook/" + getMagicKeyCache().getBeansObjectId() + "?magickey=" + getMagicKeyCache().getId();
			
			html
				.div(id(getId()))
					.h3()
						.content("Share by link")
					.a(href(url)
							.target("_blank")
							.id("link-" + getMagicKeyCache().getId()))
						.content(url)
					.render(new Link("Copy link",
							new OpList()
								.add(new OpCopyToClipboard("link-" + getMagicKeyCache().getId()))
								.add(new OpBeansInfo("Link copied to clipboard"))))
				._div()
				.write(new OpJs("$('#link-" + getMagicKeyCache().getId() + "').prepend(window.location.host);").toStringAdhock(), false);
		} else {
			html
				.div(id(getId()))
					.h3()
						.content("Share by link")
					.render(new Link("Share by link", 
						new OpComponentClick(this, "onCreate")))
				._div();
		}
	}
	
	private OpList onCreate() throws CLIException, IOException {
		setMagicKeyCache(new ApiCallCommand(this)
			.setApi(BeansCliCommand.API_NOTEBOOK_SHARE_MAGICKEY)
			.setApiCallType(APICallType.POST)
			.addParam("notebookId", getBeansObjectId().getId())
			.run()
			.getResponse()
			.getAsObject(MagicKey.class));
		
		HtmlCanvas html = new HtmlCanvas();
		renderOn(html);
		
		return new OpList()
				.add(new OpSet("#" + getId(), html.toHtml()));
	}

	public UUID getBeansObjectId() {
		return beansObjectId;
	}

	public void setBeansObjectId(UUID beansObjectId) {
		this.beansObjectId = beansObjectId;
	}

	private MagicKey getMagicKeyCache() throws CLIException, IOException {
		if (magicKeyCache == null) {
			ApiCallCommand api = new ApiCallCommand(this)
					.setApi(BeansCliCommand.API_NOTEBOOK_SHARE_MAGICKEY)
					.addParam("notebookId", getBeansObjectId().getId())
					.run();
			
			magicKeyCache = api.getResponse().getAsObject(MagicKey.class);
		}
		return magicKeyCache;
	}

	private void setMagicKeyCache(MagicKey magicKeyCache) {
		this.magicKeyCache = magicKeyCache;
	}
}
