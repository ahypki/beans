package net.beanscode.rest.administration;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import mjson.Json;
import net.beanscode.model.BeansDbManager;
import net.beanscode.model.BeansSearchManager;
import net.beanscode.model.backup.BackupJob;
import net.beanscode.model.backup.BackupManager;
import net.beanscode.model.backup.RestoreJob;
import net.beanscode.model.backup.RestoreStrategy;
import net.beanscode.model.cass.ValidateBeansJob;
import net.beanscode.model.rights.Rights;
import net.beanscode.model.settings.RebuildSearchIndexJob;
import net.beanscode.rest.BeansRest;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.db.weblibs.Settings;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.string.StringUtilities;

@Path("api/admin")
public class AdminRest extends BeansRest {
	
	@POST
	@Path("database/provider")
	@RolesAllowed(Rights.ADMIN)
	public void postDatabaseProvider(@Context SecurityContext sc,
			@FormParam("clazz") String clazz) throws IOException, ValidationException {
		validateUnauthorized(isAdmin(sc), "Permission denied");
		
		BeansDbManager.setMainDatabaseProvider(clazz);
	}
	
	@POST
	@Path("search/provider")
	@RolesAllowed(Rights.ADMIN)
	public void postSearchProvider(@Context SecurityContext sc,
			@FormParam("clazz") String clazz) throws IOException, ValidationException {
		validateUnauthorized(isAdmin(sc), "Permission denied");
		
		try {
			BeansSearchManager.setSearchManager(Class.forName(clazz));
		} catch (ClassNotFoundException e) {
			LibsLogger.error(AdminRest.class, "Cannot reload search engine", e);
		}
	}
		
	@POST
	@Path("search/reindex")
	@RolesAllowed(Rights.ADMIN)
	public void postSearchReindex(@Context SecurityContext sc) throws IOException, ValidationException {
		validateUnauthorized(isAdmin(sc), "Permission denied");
		
		new RebuildSearchIndexJob().save();
	}
	
	@POST
	@Path("search/clear")
	@RolesAllowed(Rights.ADMIN)
	public void postSearchClear(@Context SecurityContext sc) throws IOException, ValidationException {
		validateUnauthorized(isAdmin(sc), "Permission denied");
		
		BeansSearchManager.clearIndex();
	}

	@POST
	@Path("validate")
	@RolesAllowed(Rights.ADMIN)
	public void postValidate(@Context SecurityContext sc) throws IOException, ValidationException {
		validateUnauthorized(isAdmin(sc), "Permission denied");
		
		new ValidateBeansJob()
			.setUserId(getUserId(sc))
			.save();
	}
	
	@GET
	@Path("status")
//	@RolesAllowed(Rights.ADMIN)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getStatus(@Context SecurityContext sc) throws IOException, ValidationException {
		return Response
				.ok()
				.entity(Json
					.object()
					.set("ConfigurationFile", Settings.getSettingsPath())
					.set("DatabaseProvider", DbObject.getDatabaseProvider().getClass().getSimpleName())
					.set("SearchIndexProvider", BeansSearchManager.getSearchManager().getClass().getSimpleName())
					.toString())
				.build();
	}
	
	@GET
	@Path("backups/list")
	@RolesAllowed(Rights.ADMIN)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getBackupsList(@Context SecurityContext sc, 
			@QueryParam("filter") String filter) throws IOException, ValidationException {
		validateUnauthorized(isAdmin(sc), "Permission denied");
		
		filter = notEmpty(filter) ? filter.trim() : null; 
		
		Json arr = Json.array();
		List<FileExt> backupFiles = new ArrayList<>();
		
		for (FileExt backupFile : BackupManager.iterateBackupFiles()) {
			if (filter != null && backupFile.getFilenameOnly().contains(filter))
				backupFiles.add(backupFile);
			else
				backupFiles.add(backupFile);
		}
		
		// sort 
		Collections.sort(backupFiles, new Comparator<FileExt>() {
			@Override
			public int compare(FileExt o1, FileExt o2) {
				return ((Long) o1.getCreateDate().getTimeInMillis()).compareTo(o2.getCreateDate().getTimeInMillis());
			}
		});
			
		for (FileExt backupFile : backupFiles)
			arr.add(backupFile.getFilenameOnly());
		
		return Response
				.ok()
				.entity(Json
						.object()
						.set("files", arr)
						.toString())
				.build();
	}
	
	@POST
	@Path("backups/restore")
	@RolesAllowed(Rights.ADMIN)
	public void postBackupsRestore(@Context SecurityContext sc,
			@FormParam("userId") UUID userId,
			@FormParam("file") String file,
			@FormParam("restoreUsers") boolean restoreUsers,
			@FormParam("restoreDatasets") boolean restoreDatasets,
			@FormParam("restoreNotebooks") boolean restoreNotebooks,
			@FormParam("restoreStrategy") int restoreStrategy,
			@FormParam("withPrefix") String withPrefix,
			@FormParam("withSuffix") String withSuffix
			) throws IOException, ValidationException {
		validateUnauthorized(isAdmin(sc), "Permission denied");
				
		assertTrue(restoreStrategy >= 0 && restoreStrategy < RestoreStrategy.values().length, 
				"Restore strategy is wrong, expected value from 0 to " + (RestoreStrategy.values().length - 1));
		
		RestoreStrategy strategy = RestoreStrategy.values()[restoreStrategy];
		
		new RestoreJob()
			.setUserId(userId)
			.setRestoreFile(file)
			.setRestoreUsersGroups(restoreUsers)
			.setRestoreDatasetsTables(restoreDatasets)
			.setRestoreNotebooks(restoreNotebooks)
			.setRestoreStrategy(strategy)
			.setWithPrefix(withPrefix)
			.setWithSuffix(withSuffix)
			.save();
		
		LibsLogger.debug(AdminRest.class, "Restoring file ", file, " for user ", userId,
				" with options users= ", restoreUsers, " datasets= ", restoreDatasets,
				" notebooks= ", restoreNotebooks, " strategy= ", restoreStrategy,
				" scheduled successfully");
	}
	
	@POST
	@Path("backups/backup")
	@RolesAllowed(Rights.ADMIN)
	public void postBackupsBackup(@Context SecurityContext sc,
			@FormParam("userId") UUID userId) throws IOException, ValidationException {
		validateUnauthorized(isAdmin(sc), "Permission denied");
		
		new BackupJob()
			.setUserId(userId)
			.save();
		
		LibsLogger.debug(AdminRest.class, "Backup for the user ", userId, " scheduled successfully");
	}
}
