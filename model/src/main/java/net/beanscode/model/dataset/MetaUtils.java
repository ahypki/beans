package net.beanscode.model.dataset;

import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;

import java.util.ArrayList;
import java.util.List;

import net.beanscode.model.notebook.Notebook;
import net.hypki.libs5.utils.string.Meta;
import net.hypki.libs5.utils.string.MetaList;
import net.hypki.libs5.utils.string.RegexUtils;

public class MetaUtils {

	public static String applyMeta(final String s, final Notebook n) {
		if (s == null || n == null)
			return s;
		
		List<String> groups = RegexUtils.allGroups("\\$([\\w\\d]+)", s);
		String newS = s;
		for (String group : groups) {
			Meta m = n.getMeta().get(group);
			if (m != null) {
				String newVal = m.getAsString();
				if (newVal != null)
					newS = newS.replaceAll("\\$" + group, newVal);
			}
		}
		
		newS = newS.replace("THIS_NOTEBOOK", n.getId().getId());
		
		return newS;
	}
}
