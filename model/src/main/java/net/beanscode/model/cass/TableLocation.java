package net.beanscode.model.cass;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.beanscode.model.dataset.Table;
import net.beanscode.model.dataset.TableFactory;
import net.hypki.libs5.db.db.weblibs.utils.UUID;

import com.google.gson.annotations.Expose;

public class TableLocation {

	@Expose
	private Table table = null;
	
	@Expose
	private List<String> dataIds = null;
	
	@Expose
	private List<String> selectedColumns = null;
	
	public TableLocation() {
		
	}

	public TableLocation(String location) throws IOException {
		setLocation(location);
	}

	public TableLocation setLocation(String location) throws IOException {
		assertTrue(notEmpty(location), "Location in Plot() command is empty, expected: 'tableId,tableId,...'");
//		assertTrue(notEmpty(location), "Location in Plot() command is empty, expected: 'dataset query/table query[/col1,col2,...,colN]'");
		
		for (String tableId : location.split(",")) {
			if (getTable() == null)
				setTable(TableFactory.getTable(new UUID(tableId)));
			
			getDataIds().add(tableId);
		}

//		String[] locationParts = StringUtilities.split(location, '/');
//		assertTrue(locationParts.length <= 3, "Table location is wrong, expected: 'dataset query/table query[/col1,col2,...,colN]'");
//		
//		final String dsQuery = locationParts[0];
//		final String tabQuery = locationParts.length > 1 ? locationParts[1] : "";
//
//		List<TableSearchable> res = BeansSearchManager.searchTables(dsQuery, tabQuery);
//		assertTrue(res.size() > 0, "No datasets found for datasets query: ", dsQuery);
//		assertTrue(res.size() == 1, "Found more than one dataset for datasets query: ", dsQuery);
//			
//		TableSearchable nts = res.get(0);
//		assertTrue(nts.isInDataset(), "Currently, tables can only be read from datasets");
//		
//		Table table = Table.getTable(new UUID(nts.getId()));
//
//		setTable(table);
//		
//		assertTrue(getTable() != null, "No tables found");
//
//		if (locationParts.length == 3) {
//			// columns names are specified
//			for (String colName : StringUtilities.split(locationParts[2], ',')) {
//				getSeletedColumns().add(colName);
//			}
//		}
		return this;
	}

	public List<String> getSeletedColumns() {
		if (selectedColumns == null)
			selectedColumns = new ArrayList<>();
		return selectedColumns;
	}

	public boolean isSelectedColumnsSet() {
		return selectedColumns != null && selectedColumns.size() > 0;
	}

	public Table getTable() {
		return table;
	}

	public void setTable(Table table) {
		this.table = table;
	}

	public List<String> getDataIds() {
		if (dataIds == null)
			dataIds = new ArrayList<>();
		return dataIds;
	}

	private void setDataIds(List<String> dataIds) {
		this.dataIds = dataIds;
	}
}
