package net.beanscode.web.view.notebook;

import static org.rendersnake.HtmlAttributesFactory.class_;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.notebooks.NotebookEntryGateway;
import net.beanscode.pojo.NotebookEntry;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.BetaTableComponent;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.buttons.ButtonGroup;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.utils.LibsLogger;

import org.rendersnake.HtmlCanvas;

import com.google.gson.JsonElement;
import com.google.gson.annotations.Expose;

public class NewEntryPanel extends BetaTableComponent {
	
	private List<NotebookEntry> newEntries = null;
	
	@Expose
	private OpList onClick = null;

	public NewEntryPanel(BeansComponent parent, OpList onClick) {
		super(parent);
		setOnClick(onClick);
	}
	
	@Override
	protected String getSettingsPrefix() {
		return "newentries-table-";
	}
	
	@Override
	protected long getMaxHits() throws IOException {
//		return (getNewEntries().size() / getPerPage()) + 1;
		return getNewEntries().size();
	}
	
	private List<NotebookEntry> getNewEntries() {
		if (newEntries == null) {
			try {
				newEntries = new ArrayList<NotebookEntry>();
						
				ApiCallCommand call = new ApiCallCommand(this)
					.setApi("api/plugin/entry/list")
					.addParam("userId", getUserEmail())
					.addParam("filter", getFilter())
					.run();
				
				for (JsonElement onePlugin : call.getResponse().getAsJson().getAsJsonArray()) {
					newEntries.add(NotebookEntryGateway.getNotebookEntry(onePlugin.getAsJsonObject()));
				}
				
//				call
//					.getResponse()
//					.getAsList("/", NotebookEntry.class);// new PluginNotebookEntryGetList(getSessionBean()).run(getUserEmail());
			} catch (CLIException | IOException e) {
				LibsLogger.error(NewEntryPanel.class, "Cannot get list of new possible entries", e);
				newEntries = new ArrayList<NotebookEntry>();
			}
		}
		return newEntries;
	}
	
	@Override
	protected boolean isExapandable() throws IOException {
		return false;
	}
	
	@Override
	protected boolean isHeaderColumnVisible() throws IOException {
		return false;
	}
	
	@Override
	protected void renderExpandable(int row, HtmlCanvas html) throws IOException {
		
	}
	
	@Override
	protected void renderTitle(HtmlCanvas html) throws IOException {
		html
			.write("New field");
	}
	
	protected void renderCell(int row, int col, HtmlCanvas html) throws IOException {
		if (row == 0 && col == 0) {
			for (int i = 0, k = 0; k < getPerPage() && (getPage() * getPerPage() + i) < getNewEntries().size(); i++, k++) {
				NotebookEntry ne = getNewEntries().get(getPage() * getPerPage() + i);
				
//				if (col == 0)
//					html
//					.write(ne.getDescription());
//				else if (col == 1) {
					html
						.a(class_("beans-link")
								.onClick(getOnClick()
										.addParamParam("sourceNotebookEntryClass", ne.getClass().getName())
										.toStringOnclick()))
							.content(ne.getDescription())
//						._a()
						;
//					.render(new ButtonGroup()
//							.addButton(new Button(ne.getDescription())
//									.add()));
//				}
			}
		}
	};
	
	@Override
	protected boolean isFilterVisible() throws IOException {
		return true;
	}
	
	@Override
	protected List<Button> getMenu() {
		return null;
	}
	
//	@Override
//	protected int getPerPage() {
//		return 20;
//	}
	
	@Override
	protected String getColumnName(int col) throws IOException {
		if (col == 0)
			return "Name";
		else
			return "Options";
	}
	
	@Override
	protected int getNumberOfColumns() throws IOException {
		return 2;
	}

	private OpList getOnClick() {
		return onClick;
	}

	private void setOnClick(OpList onClick) {
		this.onClick = onClick;
	}

	@Override
	protected String getTableCss() throws IOException {
		return "beans-new-entry-table";
	}
}
