package net.beanscode.web.view.summary;

import static net.beanscode.cli.BeansCliCommand.API_AUTOUPDATE_ENTRY_DEFUALT;
import static net.beanscode.cli.BeansCliCommand.API_SETTING_MAIL_TEST;

import java.io.IOException;

import javax.ws.rs.core.SecurityContext;

import net.beanscode.cli.BeansCliCommand;
import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.web.view.ui.BetaButtonGroup;
import net.beanscode.web.view.ui.CardPanel;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.beanscode.web.view.ui.modals.OpModalQuestion;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.utils.url.Params;

import org.rendersnake.HtmlCanvas;

public class EmailSummaryCardPanel extends CardPanel {

	public EmailSummaryCardPanel() {
		
	}
	
	public EmailSummaryCardPanel(Component parent) {
		super(parent);
	}

	@Override
	protected void renderTitle(HtmlCanvas html) throws IOException {
		html
			.write("Email summary");
	}

	@Override
	protected void renderContent(HtmlCanvas html) throws IOException {
		html
			.render(new BetaButtonGroup()
					.add(new Button("Send test email")
							.add(new OpComponentClick(this, 
								"onTestMailQuestion",
//								OpsFactory.opConfirmation("Test email", "Do you want to send test email to check if email configuration is OK?"),
								null))));
	}
	
	public OpList onTestMailQuestion(SecurityContext sc, Params params) throws IOException, ValidationException {
		return new OpList()
			.add(new OpModalQuestion("Test email", 
					"Do you want to send the test email to yourself (" + getUserEmail() + ")?", 
					new OpComponentClick(this, "onTestMail", new Params())));
	}
	
	public OpList onTestMail(SecurityContext sc, Params params) throws IOException, ValidationException {
		new ApiCallCommand(this)
			.setApi(API_SETTING_MAIL_TEST)
			.run();
		
		return new OpList()
			.add(new OpBeansInfo("Test email was just send to your email - please check it."));
	}

	@Override
	protected void renderFooter(HtmlCanvas html) throws IOException {
		
	}
	
	
}
