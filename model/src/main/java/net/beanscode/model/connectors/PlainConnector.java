package net.beanscode.model.connectors;

import static java.lang.String.valueOf;
import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;
import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;
import static net.hypki.libs5.utils.utils.NumberUtils.toInt;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.NotImplementedException;
import org.javatuples.Pair;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.annotations.Expose;

import net.beanscode.model.BeansConst;
import net.beanscode.model.plugins.ColumnDef;
import net.beanscode.model.plugins.ColumnDefList;
import net.beanscode.model.plugins.Connector;
import net.beanscode.model.plugins.ConnectorInitParams;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.Results;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.search.query.Query;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.collections.ArrayUtils;
import net.hypki.libs5.utils.file.BigFile;
import net.hypki.libs5.utils.file.BigFileSplitted;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.file.Line;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.sha.MD5Checksum;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.utils.NumberUtils;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

public class PlainConnector implements Connector, Iterable<Row> {
	
	public final String META_PATH = "path";
	
//	private FileExt sourcePath = null;
	private InputStream sourceInputStream = null;
//	private Iterator<Line> sourceLinesIterator = null;
	
//	private String firstData = null;

	private ColumnDefList columnDefs = null;
	
	private FileWriter writer = null;
	private boolean writerHeaderSaved = false;
	
	private boolean simpleHeader = false;
	
	@Expose
	@NotNull
	@AssertValid
	private ConnectorInitParams connectorInitParams = null;

	public PlainConnector() {
		
	}
	
	public PlainConnector(File sourcePath) {
		setPath(new FileExt(sourcePath));
	}
	
	public PlainConnector(InputStream sourceInputStream) {
		setSourceInputStream(sourceInputStream);
	}
	
	public PlainConnector(File sourcePath, ColumnDefList columnDefs) {
		setPath(sourcePath);
		setColumnDefs(columnDefs);
	}
	
	@Override
	public void validate() throws IOException {
		// do nothing
	}
	
	@Override
	public String getVersion() {
		try {
			StringBuilder sb = new StringBuilder();
			
			sb.append("Plain text file; ");
			
			sb.append(getPath());
			
			sb.append("; " + new FileExt(getPath()).getLastEditDate());
			
			return sb.toString();
		} catch (Throwable t) {
			return "Plain text unknown version";
		}
	}
	
	@Override
	public String getName() {
		return "Plain";
	}
	
	@Override
	public ConnectorInitParams getInitParams() {
		if (connectorInitParams == null)
			connectorInitParams = new ConnectorInitParams();
		return connectorInitParams;
	}
	
	@Override
	public void clear() throws IOException {
		FileExt file = getPath();
		if (file != null) {
			LibsLogger.debug(PlainConnector.class, "Deleting file ", getPath());
			file.deleteIfExists();
		}
	}
		
	@Override
	public Connector setInitParams(ConnectorInitParams initParams) {
		this.connectorInitParams = initParams;
//		getInitParams().getMeta().add(META_PATH, new FileExt(initParams).getAbsolutePath());
		setSourceInputStream(null);
//		this.sourceLinesIterator = null;
//		this.firstData = null;
		this.columnDefs = null;
		this.writer = null;
		this.writerHeaderSaved = false;
		return this;
	}
	
//	private BigFileSplitted.FileIterator sourceLinesIterable() {
//		return new BigFileSplitted.FileIterator() {
//			@Override
//			public Iterator<Line> iterator() {
//				return sourceLinesIterator();
//			}
//		};
//	}
	
	private BigFileSplitted.FileIterator sourceLinesIterator() {
//		if (sourceLinesIterator == null) {
			try {
				LibsLogger.debug(PlainConnector.class, "Trying to open file ", getPath());
				if (getPath() != null) {
					if (new FileExt(getPath().getAbsolutePath()).exists() == false)
						return null;
					return new BigFileSplitted(getPath().getAbsolutePath()).iterator();
				} else if (getSourceInputStream() != null)
					return new BigFileSplitted(getSourceInputStream()).iterator();
				else
					throw new NotImplementedException("Don't know how to read source file");
			} catch (IOException e) {
				LibsLogger.error(PlainConnector.class, "Cannot open iterator for file " + getPath().getAbsolutePath(), e);
				return null;
			}
//		}
//		return sourceLinesIterator;
	}
	
	private boolean isDataFileExist() {
		if (getPath() != null)
			return new FileExt(getPath().getAbsolutePath()).exists();
		
		return false;
	}
	
//	@Override
//	public List<Row> readRows(String ... pk) throws IOException {
//		List<Row> rows = new ArrayList<>();
//		
//		RandomAccessFile reader = null;
//		try {
//			reader = new RandomAccessFile(getPath(), "r");
//			for (String onePK : pk) {
//				long pos = NumberUtils.toLong(onePK.substring(onePK.lastIndexOf(".") + 1));
//				reader.seek(pos);
//				Line l = new Line(reader.readLine());
//				Row row = new Row(pos);
//				for (int i = 0; i < l.size(); i++) {
//					
//					ColumnDef colDef = getColumnDefs().get(i);
//					String colName = colDef.getName();
//					row.addColumn(colName, stringToObject(l.get(i), colDef.getType()));
//					
//	//				// computing CRC
//					
//	//				if (crc.get(colName) == null)
//	//					crc.put(colName, new MD5Checksum());
//	//				crc.get(colName).update(l.get(i));
//				}
//				rows.add(row);
//			}
//		} finally {
//			if (reader != null)
//				reader.close();
//		}
//		
//		return rows;
//	}
	
//	public net.hypki.libs5.db.db.Results iterateRows(String state, int size) {
//		if (isDataFileExist() == false)
//			return new Results();
//		
//		int from = 0;
//		if (notEmpty(state))
//			from = toInt(state);
//		
//		int i = 0;
//		Results res = new Results();
//		for (Row row : this.iterateRows()) {
//			if (i >= from)
//				res.addRow(row);
//			
//			i++;
//			
//			if (res.size() == size)
//				break;
//		}
//		res.setNextPage(valueOf(i));
//		return res;
//	};
	
//	@Override
//	public Iterable<Row> iterateRows() {
//		return new Iterable<Row>() {
//			@Override
//			public Iterator<Row> iterator() {
//				return PlainConnector.this.iterator();
//			}
//		};
//	}
	
	@Override
	public Iterator<Row> iterator() {
		if (isDataFileExist() == false) {
			return new Iterator<Row>() {
				
				@Override
				public Row next() {
					return null;
				}
				
				@Override
				public boolean hasNext() {
					return false;
				}
			};
		}
		
		if (this.columnDefs == null)
			getColumnDefs();
		
		return new Iterator<Row>() {
			private long rowNr = 0;
			private BigFileSplitted.FileIterator iter = sourceLinesIterator();
			private Line currentLine = null;
								
			@Override
			public Row next() {
				Line l = currentLine;

				if (l == null) {					
					hasNext();
					l = currentLine;
				}
				
				if (l == null)
					return null;
				
//				l = iter.next();

				Row row = new Row(iter.getCurrentSeek());
				
				if (getInitParams() != null && getInitParams().getTableId() != null)
					row.addColumn(BeansConst.COLUMN_TBID, getInitParams().getTableId().getId());
				
				try {
					for (int i = 0; i < l.size(); i++) {
						ColumnDef colDef = getColumnDefs().get(i);
						String colName = colDef.getName();
						row.addColumn(colName, stringToObject(l.get(i), colDef.getType()));
						
//							// computing CRC
//							if (crc.get(colName) == null)
//								crc.put(colName, new MD5Checksum());
//							crc.get(colName).update(l.get(i));
					}
				} catch (Throwable t) {
					LibsLogger.error(PlainConnector.class, "Cannot prepare row with nr ", rowNr, " for importing for line " + l.getLine() + " for column definitions " + StringUtilities.toString(getColumnDefs(), ","), t);
					throw t;
				}
				
				rowNr++;
				
				return row;
			}
			
			@Override
			public boolean hasNext() {
//				if (firstData != null) {
//					return true;
//				}
				
				if (iter == null || !iter.hasNext())
					return false;
				
				while ((currentLine = iter.next()) != null) {
					if (currentLine.getLine().startsWith("#")) {
						if (iter.hasNext())
							continue;
					}
					
					break;
				}
				
				return currentLine != null;
			}
		};
	}
	
	@Override
	public void write(Row row) throws IOException {
		if (this.writerHeaderSaved == false) {
			if (isSimpleHeader()) {
				getFileWriter().append("#");
				for (ColumnDef col : columnDefs) {
					getFileWriter().append(" " + col.getName());
				}
				getFileWriter().append("\n");
			} else {
				getFileWriter().append("# ");
				getFileWriter().append(JsonUtils.objectToStringPretty(getColumnDefs()).replaceAll("\\R", "\n# "));
				getFileWriter().append("\n");
			}
			this.writerHeaderSaved = true;
		}
		
		boolean firstCol = true;
		for (ColumnDef columnDef : getColumnDefs()) {
			if (!firstCol)
				getFileWriter().append("\t");
			
			Object o = row.get(columnDef.getName(), "0");
			if (o != null && o instanceof String)
				if (StringUtilities.nullOrEmpty(o))
					o = "NULL";
			
			getFileWriter().append(o.toString());
			
			firstCol = false;
		}
		getFileWriter().append("\n");
		
		// TODO DEBUG
//		getFileWriter().flush();
	}
	
	@Override
	public void close() throws IOException {
		try {
			if (this.writer != null) {
				getFileWriter().flush();
				getFileWriter().close();
				writer = null;
			}
		} catch (IOException e) {
			if (e.getMessage().equals("Stream closed"))
				LibsLogger.warn(PlainConnector.class, "Stream already closed in PlainConnector");
			else
				throw e;
		}
		this.writerHeaderSaved = false;
	}
		
	private FileWriter getFileWriter() throws IOException {
		if (writer == null) {
			if (isDataFileExist()) {
				writer = new FileWriter(getPath(), true);
				writerHeaderSaved = true;
			} else
				writer = new FileWriter(getPath());
		}
		return writer;
	}

	public FileExt getPath() {
		if (getInitParams().getMeta().get(META_PATH) != null)
			return new FileExt(getInitParams().getMeta().get(META_PATH).getAsString());
		else {
			FileExt f = new FileExt("$HOME/.beans-software/connectors/" 
					+ getInitParams().getMeta().get("tableId").getAsString() 
					+ "-" 
					+ getInitParams().getMeta().get("uniTableId").getAsString()
					+ ".dat");
			getInitParams().getMeta().add(META_PATH, f.getAbsolutePath());
			return f;
		}
	}

	public PlainConnector setPath(FileExt path) {
		return setPath((File) path);
	}
	
	public PlainConnector setPath(File path) {
		getInitParams().getMeta().add(META_PATH, path.getAbsolutePath());
		return this;
	}
	
//	@Override
	public void setColumnDefs(ColumnDefList columnDefs) {
		this.columnDefs = columnDefs;
	}
	
	@Override
	public ColumnDefList getColumnDefs() {
		if (columnDefs == null)
			try {
				if (isDataFileExist())
					columnDefs = parseHeader();
			} catch (IOException e) {
				LibsLogger.error(PlainConnector.class, "Cannot parse header for file " + getPath(), e);
			}
		return columnDefs;
	}
	
	public boolean isSourcePathDefined() {
		return getPath() != null;
	}
	
	public boolean isSourceInputStreamDefined() {
		return getSourceInputStream() != null;
	}
	
	private ColumnDefList parseHeader() throws IOException {
		Pair<String, String> headerAndFirstData = readHeaderAndFirstData();
//		firstData = headerAndFirstData.getValue1();
		return parseHeader(headerAndFirstData.getValue0(), headerAndFirstData.getValue1());
	}
	
	private static Object stringToObject(String colValue, ColumnType colType) {
		try {
			switch (colType) {
				case DOUBLE:
					return Double.parseDouble(colValue);
					
				case LONG:
					return Long.parseLong(colValue);
					
				case INTEGER:
					return Integer.parseInt(colValue);
					
				case DATE:
					return colValue;
					
				case STRING:
					return colValue;
		
				default:
					throw new RuntimeException("Column type not implemented " + colType);
			}
		} catch (java.lang.NumberFormatException t) {
			switch (colType) {
				case DOUBLE:
					return Double.parseDouble(colValue);
					
				case LONG:
					return Long.parseLong(colValue);
					
				case INTEGER:
					return (int) Double.parseDouble(colValue);
					
				case DATE:
					return colValue;
					
				case STRING:
					return colValue;
		
				default:
					throw new RuntimeException("Column type not implemented " + colType);
			}
		}
	}

	private static ColumnType determineColumnType(String value) {
		if (value.matches("[-]*[\\d]+"))
			return ColumnType.LONG;
		else if (value.matches("[-]*\\d[\\d\\.eE+-]+"))
			return ColumnType.DOUBLE;
		else
			return ColumnType.STRING;
	}
		
	private static ColumnDefList parseHeader(final String header, final String firstData) throws IOException {
		try {
			final ColumnDefList columnDefs = new ColumnDefList();
			
			// parsing header
			final Set<String> columnNames = new HashSet<>();
			final Line firstDataLine = notEmpty(firstData) ? (firstData.contains(",") ? new Line(firstData, ',') : new Line(firstData)) : null;
			if (JsonUtils.isJsonSyntax(header)) {
				
				columnDefs.addAll(ColumnDef.parse(header.toString()));
				
//				// reading column names from JSON header
//				JsonArray json = (JsonArray) JsonUtils.parseJson(header.toString());
//				for (int i = 0; i < json.size(); i++) {
//					JsonElement column = json.get(i);
//					if (column.getAsJsonObject().get("name") != null) {
//						try {
//							String name = column.getAsJsonObject().get("name").getAsString().trim();
//							JsonElement desc = column.getAsJsonObject().get("description");
//							JsonElement type = column.getAsJsonObject().get("type");
//							
//							assertTrue(columnNames.contains(name) == false, "Column '" + name + "' duplicated in the header of the file");
//							
//							columnNames.add(name);
//							
//							ColumnType columnType = type != null ? ColumnType.parse(type.getAsString().trim()) : ColumnType.UNKNOWN;
//							if (columnType == ColumnType.UNKNOWN && firstDataLine != null)
//								columnType = determineColumnType(firstDataLine.get(i));
//							
//							columnDefs.add(new ColumnDef(name, 
//									desc != null ? desc.getAsString().trim() : null, 
//									columnType));
//						} catch (Exception e) {
//							// TODO Auto-generated catch block
//							e.printStackTrace();
//						}
//					}
//				}
				
			} else if (!nullOrEmpty(header)) {
				
				// reading column names from a simple header
				String [] parts = StringUtilities.split(header);
				for (int colIndex = 0; colIndex < parts.length; colIndex++) {
					String column = parts[colIndex];
					column = column.trim();
					if (notEmpty(column)) {
						assertTrue(columnNames.contains(column) == false, "Column '" + column + "' duplicated in the header of the file");
						
						columnNames.add(column);
						
						ColumnType columnType = ColumnType.UNKNOWN;
						if (firstDataLine != null)
							columnType = determineColumnType(firstDataLine.get(colIndex));
						
						columnDefs.add(new ColumnDef(column, null, columnType));
					}
				}
				
			} else if (nullOrEmpty(header)) {
				
				// header is empty, parsing first line with data
				if (notEmpty(firstData)) {
					for (int colIndex = 0; colIndex < firstDataLine.size(); colIndex++) {
						columnNames.add("c" + (colIndex + 1));
						
						ColumnType columnType = ColumnType.UNKNOWN;
						if (firstDataLine != null)
							columnType = determineColumnType(firstDataLine.get(colIndex));
						
						columnDefs.add(new ColumnDef("c" + (colIndex + 1), null, columnType));
					}
				}
				
			} else {
				throw new NotImplementedException();
			}
			
//			for (ColumnDef colDef : columnDefs) {
//				assertTrue(colDef.getType() != ColumnType.UNKNOWN, "Could not determine the type for a column ", colDef.getName(), " , cannot import this file to BEANS");
//			}
			
			return columnDefs;
		} catch (Exception e) {
			throw new IOException("Cannot read or parse header", e);
		}

	}
	
	private Pair<String, String> readHeaderAndFirstData() {
		if (isDataFileExist() == false)
			return null;
		
		boolean readingHeader = true;
		final StringBuilder headerBuilder = new StringBuilder();
		String firstData = null;
		
		for (Line line : sourceLinesIterator()) {
			if (readingHeader) {
				// reading header line
				if (line.getLine().startsWith("#")) {
					headerBuilder.append(line.getLine().substring(1));
				} else {
					firstData = line.getLine();
					break;
				}
			} else {
				firstData = line.getLine();
				break;
			}
		}
		return new Pair<String, String>(headerBuilder.toString(), firstData);
	}

	public InputStream getSourceInputStream() {
		return sourceInputStream;
	}

	public void setSourceInputStream(InputStream sourceInputStream) {
		this.sourceInputStream = sourceInputStream;
	}

	public void flush() throws IOException {
		getFileWriter().flush();
	}
	
	@Override
	public Iterable<Row> iterateRows(Query filter, int splitNr, int splitCount) {
		// TODO implement it
		if (splitNr == 0)
			return iterateRows(filter);
		else
			return new Iterable<Row>() {
				
				@Override
				public Iterator<Row> iterator() {
					return new Iterator<Row>() {
						
						@Override
						public Row next() {
							return null;
						}
						
						@Override
						public boolean hasNext() {
							return false;
						}
					};
				}
			};
	}
	
	@Override
	public Iterable<Row> iterateRows(Query filter) {
		return new Iterable<Row>() {
			
			@Override
			public Iterator<Row> iterator() {
				return new Iterator<Row>() {
					
					private Iterator<Row> allRowsIter = null;
					private Row row = null;
					
					@Override
					public Row next() {
						try {
							if (allRowsIter == null || row == null)
								hasNext();
							
							return row;
						} finally {
							row = null;
						}
					}
					
					@Override
					public boolean hasNext() {
						if (allRowsIter == null) {
							allRowsIter = PlainConnector.this.iterator();
						}
						
						if (row != null)
							return true;
						
						row = null;
						while (allRowsIter.hasNext()) {
							row = allRowsIter.next();
							
							if (row == null)
								return false;
							else if (filter == null)
								return true;
							else if (filter != null && filter.isFulfilled(row))
								return true;
						}
						
						return row != null;
					}
				};
			}
		};
	}
	
	@Override
	public Results iterateRows(Query filter, String state, int size) {
		if (isDataFileExist() == false)
			return new Results();
		
		int from = 0;
		if (notEmpty(state))
			from = toInt(state);
		
		int i = 0;
		Results res = new Results();
		for (Row row : this.iterateRows(filter)) {
			if (i >= from)
				res.addRow(row);
			
			i++;
			
			if (res.size() == size)
				break;
		}
		res.setNextPage(valueOf(i));
		return res;
	}
	
	@Override
	public long getAproxBytesSize() throws IOException {
		return getPath().length();
	}

	public boolean isSimpleHeader() {
		return simpleHeader;
	}

	public void setSimpleHeader(boolean simpleHeader) {
		this.simpleHeader = simpleHeader;
	}
}
