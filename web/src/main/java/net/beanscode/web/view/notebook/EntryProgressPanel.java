package net.beanscode.web.view.notebook;

import static org.rendersnake.HtmlAttributesFactory.class_;

import java.io.IOException;

import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.pojo.NotebookEntryProgress;
import net.beanscode.pojo.NotebookEntryStatus;
import net.beanscode.web.view.jobs.QueryCheckpoints;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.badge.BadgeError;
import net.beanscode.web.view.ui.badge.BadgeInfo;
import net.beanscode.web.view.ui.badge.BadgeSuccess;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.ops.OpComponentTimer;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpReplace;
import net.hypki.libs5.utils.LibsLogger;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

public class EntryProgressPanel extends BeansComponent {

	public static final int AUTOREFRESH_INITIAL_INTERVAL_MS = 1000;
	public static final int AUTOREFRESH_INTERVAL_MS = 1000;
	
	@Expose
	@NotNull
	@AssertValid
	private UUID entryId = null;
	
	private NotebookEntryProgress notebookEntryProgressCache = null;

	public EntryProgressPanel() {
		
	}
	
	public EntryProgressPanel(BeansComponent parent, NotebookEntryProgress notebookEntryProgress) {
		super(parent);
		setEntryId(notebookEntryProgress.getEntryId());
		this.notebookEntryProgressCache = notebookEntryProgress;
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		renderOn(html, AUTOREFRESH_INITIAL_INTERVAL_MS);
	}
	
	public void renderOn(HtmlCanvas html, int refreshMsTimeout) throws IOException {
		String message = getNotebookEntryProgress() != null 
				&& getNotebookEntryProgress().getMessage() != null ? 
						getNotebookEntryProgress().getMessage() : "";
		
		html
			.div(class_("status inline entry-progress-" + getEntryId().getId())
					.id(getId()));
		
		if (getNotebookEntryProgress() == null
				|| getNotebookEntryProgress().getNotebookEntryStatus() == null) {
			
			html
				.render(new BadgeInfo("INFO: " + message));
			
		} else if (getNotebookEntryProgress().getNotebookEntryStatus() == NotebookEntryStatus.NEW) {
			
			html
				.render(new BadgeInfo("INFO: " + message));
			
		} else if (getNotebookEntryProgress().getNotebookEntryStatus() == NotebookEntryStatus.RUNNING) {
			
			html
				.render(new BadgeInfo("INFO: " + message));
			
		} else if (getNotebookEntryProgress().getNotebookEntryStatus() == NotebookEntryStatus.SUCCESS) {
			
			html
				.render(new BadgeSuccess("Success"));
			
//		} else if (getNotebookEntryProgress().getNotebookEntryStatus() == NotebookEntryStatus.FAIL) {
//			
//			html
//				.span(class_(Badge.WARNING + " label"))
//					.content("Warning" + message);
			
		} else if (getNotebookEntryProgress().getNotebookEntryStatus() == NotebookEntryStatus.FAIL) {
			
			html
				.render(new BadgeError("Failed: " + message))
				.span(class_("message error"))
					.content(getNotebookEntryProgress().getMessage());
			
		} else {
			
			LibsLogger.error(QueryCheckpoints.class, "Unimplemented case for " + getNotebookEntryProgress().getNotebookEntryStatus());
			
		}
		
		// refresh timer
		if (getNotebookEntryProgress() != null 
				&& getNotebookEntryProgress().isRunning()
				&& refreshMsTimeout > 0)
			html
				.write(new OpComponentTimer(this, "onRefresh", refreshMsTimeout).toStringAdhock(), false);
				
		html
			._div();
	}
	
	protected OpList onRefresh() throws IOException {
		HtmlCanvas html = new HtmlCanvas();
		
		renderOn(html, AUTOREFRESH_INTERVAL_MS);
				
		return new OpList()
			.add(new OpReplace(".entry-progress-" + getEntryId().getId(), html.toHtml()));
	}
	
	public NotebookEntryProgress getNotebookEntryProgress() {
		if (notebookEntryProgressCache == null) {
			try {
				notebookEntryProgressCache = new ApiCallCommand(this)
						.setApi("api/notebook/entry/progress")
						.addParam("entryId", getEntryId())
						.run()
						.getResponse()
						.getAsObject(NotebookEntryProgress.class);
			} catch (Exception e) {
				LibsLogger.error(NotebookEntryEditorPanel.class, "Cannot get progress from DB", e);
			}
		}
		return notebookEntryProgressCache;
	}

	public UUID getEntryId() {
		return entryId;
	}

	public void setEntryId(UUID entryId) {
		this.entryId = entryId;
	}
}
