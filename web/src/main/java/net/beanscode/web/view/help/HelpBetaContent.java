package net.beanscode.web.view.help;

import java.io.IOException;

import net.beanscode.web.beta.BetaContent;
import net.beanscode.web.view.ui.BetaButtonGroup;
import net.hypki.libs5.pjf.components.Component;

import org.rendersnake.HtmlCanvas;

public class HelpBetaContent extends BetaContent {

	public HelpBetaContent() {
		
	}
	
	public HelpBetaContent(Component parent) {
		super(parent);
	}
	
	@Override
	protected void renderHeader(HtmlCanvas html) throws IOException {
		html
			.h1()
				.content("Help");
	}

	@Override
	protected void renderContent(HtmlCanvas html) throws IOException {		
		html
			.render(new QuickHelpPanel())
			.render(new UdfHelpPanel())
			;
	}

	@Override
	protected BetaButtonGroup getMenu() throws IOException {
		return null;
	}

	@Override
	protected String getMenuName() throws IOException {
		return null;
	}
}
