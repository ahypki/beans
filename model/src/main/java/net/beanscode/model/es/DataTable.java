package net.beanscode.model.es;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.search.query.StringTerm;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.weblibs.SearchManager;
import net.hypki.libs5.weblibs.WeblibsConst;

import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;

public class DataTable {

	public static final String INDEX_TYPE = "DataTable".toLowerCase();
	
	public static final String TABLE_ID = "tableId";
	public static final String ROW_ID = "rowId";
	
	@Expose
	private UUID tableId = null;
	
	@Expose
	private long rowId = 0;
	
	@Expose
	private HashMap<String, Object> params = new HashMap<>();
	
	public DataTable() {
		
	}

	public DataTable(UUID tableId, long row) {
		this.tableId = tableId;
		this.rowId = row;
	}
	
	@Override
	public String toString() {
		return JsonUtils.objectToString(this);
	}
	
	public DataTable save() throws IOException {
		SearchManager.searchInstance().indexDocument(WeblibsConst.KEYSPACE_LOWERCASE, INDEX_TYPE, getId(), getData());
		return this;
	}
	
	public String getId() {
		return String.format("%s-%d", tableId, rowId);
	}
	
	public String getData() {
		JsonObject obj = new JsonObject();
		obj.addProperty(TABLE_ID, tableId.getId());
		obj.addProperty(ROW_ID, rowId);
		for (Entry<String, Object> e : getParams().entrySet()) {
			obj.addProperty(e.getKey(), (Number) e.getValue());			
		}
		return obj.toString();
	}

	public HashMap<String, Object> getParams() {
		return params;
	}

	public void setParams(HashMap<String, Object> params) {
		this.params = params;
	}
	
	public DataTable add(String name, Object value) {
		getParams().put(name, value);
		return this;
	}
	
	public static List<DataTable> getRows(UUID tableId, int from, int size) throws IOException {
		net.hypki.libs5.search.query.Query q = new net.hypki.libs5.search.query.Query();
		q.addTerm(new StringTerm(TABLE_ID, tableId.getId()));
		return SearchManager.searchInstance().search(DataTable.class, WeblibsConst.KEYSPACE_LOWERCASE, INDEX_TYPE, q, from, size).getObjects();
	}
	
	public static List<DataTable> getPlotRows(UUID tableId, String plotName, int from, int size) throws IOException {
		net.hypki.libs5.search.query.Query q = new net.hypki.libs5.search.query.Query();
		q.addTerm(new StringTerm(TABLE_ID, tableId.getId()));
		return SearchManager.searchInstance().search(DataTable.class, WeblibsConst.KEYSPACE_LOWERCASE, plotName, q, from, size).getObjects();
	}
}
