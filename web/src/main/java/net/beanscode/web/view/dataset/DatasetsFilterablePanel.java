package net.beanscode.web.view.dataset;

import static net.hypki.libs5.pjf.OpsFactory.opsCall;
import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.id;
import static org.rendersnake.HtmlAttributesFactory.type;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.SecurityContext;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.datasets.DatasetSearchResults;
import net.beanscode.pojo.dataset.Dataset;
import net.beanscode.web.view.settings.Setting;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.modals.OpModalQuestion;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.pjf.components.PagerRenderable;
import net.hypki.libs5.pjf.components.buttons.ButtonSize;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.components.ops.OpComponentTimer;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpRemove;
import net.hypki.libs5.pjf.op.OpSet;
import net.hypki.libs5.pjf.op.OpWarn;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.url.Params;

import org.rendersnake.HtmlCanvas;

public class DatasetsFilterablePanel extends BeansComponent {
	
	public static final int PER_PAGE = 10;

//	private List<Dataset> datasets = null;
	
//	private long datasetsCount = 0;
	
	private DatasetSearchResults datasetSearchResults = null;
	
	private int currentPage = 0;
	
	private String keywords = null;
	
	public DatasetsFilterablePanel() {
		
	}
	
	public DatasetsFilterablePanel(BeansComponent parent) {
		super(parent);
	}
	
	public DatasetsFilterablePanel(BeansComponent parent, List<Dataset> datasets, long allDatasetsCount, int currentPage) {
		super(parent);
		setDatasetSearchResults(new DatasetSearchResults());
		getDatasets().setDatasets(datasets);
		getDatasets().setMaxHits(allDatasetsCount);
		setCurrentPage(currentPage);
	}
	
	public OpList remove(SecurityContext sc, Params params) throws IOException, ValidationException {
		Object dId = params.get("datasetId");
		
		if (StringUtilities.nullOrEmpty(dId)) {
			return new OpList()
					.add(new OpWarn("Warning", "No datasets were selected"));
		}
		
		List<String> datasetIds = new ArrayList<>();
		
		if (dId instanceof String)
			datasetIds.add(dId.toString());
		else if (dId instanceof List) {
			for (Object o : ((List) dId)) {
				datasetIds.add(o.toString());
			}
		}
		
//		new DatasetRemove(getSessionBean()).run(datasetIds);
		ApiCallCommand api = new ApiCallCommand(this)
			.setApi("api/dataset/delete");
			
		for (String dsid : datasetIds)
			api.addParam("datasetId", dsid);
			
		api
			.run();
		
		OpList opList = new OpList();
		for (String dsId : datasetIds)
			opList.add(new OpRemove("#tr-" + dsId));
		
		return opList;
	}
	
	public OpList filter(Params params) throws IOException {
		String keywords = params.getString("keywords");
		int page = params.getInteger("page", 0);
		
		setCurrentPage(page);
		setKeywords(keywords);
		
		try {
			new ApiCallCommand(getSessionBean())
				.setApi("api/setting/setSetting")
				.addParam("id", "datasets-filter")
				.addParam("datasets-filter", keywords)
				.addParam("userEmail", getUserId())
				.run();
		} catch (CLIException e) {
			LibsLogger.error(DatasetsFilterablePanel.class, "Cannot save the dataset filter", e);
		}
				
		return new OpList()
//			.add(new OpReplace("#datasets-table", new DatasetTable(this, getDatasets().getDatasets()).toHtml()))
			.add(new OpSet("#pager", new HtmlCanvas().render(new PagerRenderable(this, "filter", 
					getCurrentPage(), PER_PAGE, getDatasets().getMaxHits(), ButtonSize.NORMAL)).toHtml()));
	}
	
//	private DatasetSearchResults findDatasets(String keywords) throws IOException {
//		return new DatasetSearch().run(keywords, getCurrentPage() * PER_PAGE, PER_PAGE);
//	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		try {
			Setting setting = new ApiCallCommand(getSessionBean())
				.setApi("api/setting/getSetting")
				.addParam("id", "datasets-filter")
				.addParam("userEmail", getUserId())
				.run()
				.getResponse()
				.getAsObject(Setting.class);
			
			if (setting != null && setting.getValue("datasets-filter") != null)
				this.keywords = setting.getValue("datasets-filter").getValueAsString();
		} catch (CLIException e) {
			LibsLogger.error(DatasetsFilterablePanel.class, "Cannot get dataset filter", e);
		}
		
		html
			.div(id(getId())
					.class_("filtering-form datasets-filtering-form"))
				.h3()
					.content("Filtering datasets")
				.input(type("hidden")
						.name("dummy")
						.value("1"))
				.input(type("text")
						.name("keywords")
						.value(this.keywords)
						.class_("keywords")
						.onKeyup(opsCall(new OpComponentTimer(this, "filter", 300)))
						.add("placeholder", "Keywords..."))
				.button(type("button")
	            		.class_("btn btn-default btn-sm")
	            		.onClick(opsCall(new OpComponentClick(this, "filter"))))
					.i(class_("fa fa-refresh"))
						.content("")
				._button()
				.button(class_("btn btn-default btn-sm")
							.onClick(new OpModalQuestion("Removing datasets", 
									"Do you really want to delete selected datasets? (That operation cannot be reverted)",
									new OpComponentClick(this, 
											"remove",
											new Params()
												.add("datasetId", "")))
									.toStringOnclick()))
					.content("Remove selected")
			
//				.render(new DatasetTable(this, getDatasets().getDatasets()))
				.div(id("pager")
						.class_("datasets-panel-pager"))
					.render(new PagerRenderable(this, "filter", getCurrentPage(), PER_PAGE, 
							getDatasets().getMaxHits(), ButtonSize.NORMAL))
				._div()
			._div()
			
			;
	}

	public DatasetSearchResults getDatasets() {
		if (datasetSearchResults == null) {
			try {
				datasetSearchResults = DatasetGateway.getDatasetSearchResults(getSessionBean(), 
						keywords, 
						getCurrentPage() * PER_PAGE, 
						PER_PAGE);
//				datasetSearchResults = new ApiCallCommand(getSessionBean())
//										.setApi("api/dataset/query")
//										.addParam("query", keywords)
//										.addParam("from", getCurrentPage() * PER_PAGE)
//										.addParam("size", PER_PAGE)
//										.run()
//										.getResponse()
//										.getAsObject(DatasetSearchResults.class);
			} catch (Exception e) {
				datasetSearchResults = null;
			}
		}
		return datasetSearchResults;
	}
	
	private void setDatasetSearchResults(DatasetSearchResults results) {
		this.datasetSearchResults = results;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

//	public long getDatasetsCount() {
//		return datasetsCount;
//	}
//
//	public void setDatasetsCount(long datasetsCount) {
//		this.datasetsCount = datasetsCount;
//	}
//
//	public void setDatasets(List<Dataset> datasets) {
//		this.datasets = datasets;
//	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
}
