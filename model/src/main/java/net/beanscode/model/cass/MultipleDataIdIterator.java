package net.beanscode.model.cass;

import static net.hypki.libs5.utils.string.StringUtilities.deserialize;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.hypki.libs5.db.cassandra.MapReduceUtils;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.Results;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.WeblibsConst;

import org.apache.commons.lang.NotImplementedException;

public class MultipleDataIdIterator implements Iterator<Row>, Iterable<Row> {
	
	private static final int PER_PAGE = 20000;
	
	private String state = null;
	
	private List<Row> rowsPage = null;
	
	private int rowsPageCounter = 0;
	
//	private int currentDataId = 0;
	
	private List<String> dataIds = null;
	
	private C3Where filter = null;
	
	private boolean defaultIfColumnIsMissing = false;
	
	private long totalRowsReadUnfiltered = 0;
	private long totalRowsReadFiltered = 0;

	public MultipleDataIdIterator(String dataId) {
		getDataIds().add(dataId);
	}
	
	public MultipleDataIdIterator(List<String> dataIds, C3Where filter, boolean defaultIfColumnIsMissing) {
		setDataIds(dataIds);
		this.filter = filter;
		this.defaultIfColumnIsMissing = defaultIfColumnIsMissing;
	}
	
	@Override
	public Iterator<Row> iterator() {
		return this;
	}
	
	@Override
	public void remove() {
		throw new NotImplementedException();
	}
	
	@Override
	public boolean hasNext() {
		try {
			if (rowsPage != null && rowsPageCounter < rowsPage.size())
				return true;
			
			if (rowsPage != null && state == null) {
				LibsLogger.debug(MultipleDataIdIterator.class, "Total rows read: unfiltered= ", totalRowsReadUnfiltered, " / filtered= ", totalRowsReadFiltered);
				return false;
			}
								
			while (true) {
				Results results = DataFactory.getPage(WeblibsConst.KEYSPACE, getDataIds(), state, PER_PAGE);
				
				totalRowsReadUnfiltered += results.size();
				
				if (rowsPage != null)
					rowsPage.clear();
				else
					rowsPage = new ArrayList<>(results.size());
				
				for (Row row : results.getRows()) {					
					if (filter == null || row.isFullfiled(filter, defaultIfColumnIsMissing))
						rowsPage.add(row);
				}
				
				state = results.getNextPage();
				rowsPageCounter = 0;
				totalRowsReadFiltered += rowsPage.size();
				
				if (rowsPage.size() > 0)
					return true;
				
				if (state == null)
					break;
			}
			
			boolean hasNext = rowsPage != null && rowsPage.size() > 0;
			
			if (!hasNext)
				LibsLogger.debug(MultipleDataIdIterator.class, "Total rows read: unfiltered= ", totalRowsReadUnfiltered, " / filtered= ", totalRowsReadFiltered);
			
			return hasNext;
		} catch (Exception e) {
			LibsLogger.error(Data.class, "Cannot iterate over Data", e);
			return false;
		}
	}
	
	@Override
	public Row next() {
		if (rowsPage == null)
			hasNext();
		return rowsPage != null ? rowsPage.get(rowsPageCounter++) : null;
	}

	public List<String> getDataIds() {
		if (dataIds == null)
			dataIds = new ArrayList<>();
		return dataIds;
	}

	public void setDataIds(List<String> dataIds) {
		this.dataIds = dataIds;
	}
}
