package net.beanscode.model.dataset;

import static java.lang.String.format;
import static net.hypki.libs5.db.db.weblibs.ValidationUtils.validateTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import net.beanscode.model.BeansDTNObject;
import net.beanscode.model.BeansSearchManager;
import net.beanscode.model.autostart.IncreaseFieldsCountJob;
import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.rights.Rights;
import net.hypki.libs5.db.db.weblibs.MutationList;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.search.query.Query;
import net.hypki.libs5.search.query.StringTerm;
import net.hypki.libs5.search.query.TermRequirement;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.string.Meta;
import net.hypki.libs5.utils.string.MetaList;
import net.hypki.libs5.utils.string.RegexUtils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.utils.NumberUtils;
import net.hypki.libs5.weblibs.SearchManager;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.db.WeblibsObject;
import net.hypki.libs5.weblibs.search.RemoveFromIndex;
import net.hypki.libs5.weblibs.user.Allowed;
import net.hypki.libs5.weblibs.user.User;
import net.hypki.libs5.weblibs.user.UserFactory;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class Dataset extends BeansDTNObject<Dataset> {

	public static final String COLUMN_FAMILY = "dataset";
	
	private static boolean nrOfFieldsIncreased = false;
	
//	@Expose
//	@NotNull
//	@AssertValid
//	private UUID id = null;
	
//	@Expose
//	@NotNull
//	@AssertValid
//	private UUID userId = null;
	
	@Expose
	@NotNull
	@NotEmpty
//	@CheckWith(value = Dataset.class)
	private String name = null;
	
	@Expose
	@AssertValid
	private MetaList meta = null;
	
	@Expose
	private String description = null;
	
	@Expose
	private long creationMs = 0;
	
	@Expose
	private long editMs = 0;
	
	public Dataset() {
		setId(UUID.random());
		setDescription(null);
		setCreationDate(SimpleDate.now());
	}
	
	public Dataset(UUID userId, String name) {
		setId(UUID.random());
		setUserId(userId);
		setName(name);
		setDescription(null);
		setCreationDate(SimpleDate.now());
	}
	
	public Dataset(UUID userId, String name, String desc, SimpleDate create, Object ... meta) {
		setId(UUID.random());
		setUserId(userId);
		setName(name);
		setDescription(desc);
		setCreationDate(create);
		for (int i = 0; i < meta.length; i += 2)
			getMeta().add((String) meta[i], meta[i + 1]);
	}
		
	@Override
	public String toString() {
		return format("Dataset %s (%s)", getName(), getDescription() != null ? getDescription() : "no description");
	}
	
	@Override
	public String getColumnFamily() {
		return COLUMN_FAMILY;
	}
	
	@Override
	public String getKey() {
		return getId().getId();
	}
		
	public static boolean isDatasetNameValid(String datasetName) {
//		return datasetName.matches("^[\\w\\s\\/\\_\\-\\.\\,\\=\\$\\:\\(\\)\\[\\]]+$");
		return StringUtilities.notEmpty(datasetName) 
				&& datasetName.contains("\r") == false
				&& datasetName.contains("\n") == false;
	}
	
	@Override
	public void additionalValidation() throws ValidationException {
		super.additionalValidation();
		
		validateTrue(isDatasetNameValid(getName()), "Dataset name is invalid: " + getName());
	}
	
	@Override
	public MutationList getSaveMutations() {
		return super.getSaveMutations()
				.addInsertMutations(new ReindexDataset(this))
				.addInsertMutations(nrOfFieldsIncreased == false ? new IncreaseFieldsCountJob() : null);
	}
	
	@Override
	public Dataset save() throws ValidationException, IOException {
		setEditMs(System.currentTimeMillis());
		
		// sometimes from Fortran we have in the data e.g. 1.0d0 (not 1.0e0)
		getMeta().fixD();
		
		super.save();
		
		DatasetFactory.updateCache(this);
		
		try {
			indexThis();
		} catch (Exception e) {
			LibsLogger.error(Dataset.class, "Cannot index dataset ad-hoc in save() method", e);
		}
		
		nrOfFieldsIncreased = true;
		
		return this;
	}
	
	@Override
	public void remove() throws ValidationException, IOException {
		super.remove();
		
		LibsLogger.debug(Notebook.class, "Dataset ", getName(), " removed");
		
		try {
			SearchManager.removeObject(getCombinedKey(), Dataset.COLUMN_FAMILY);
		} catch (Exception e) {
			LibsLogger.error(Notebook.class, "Cannot ad-hoc remove dataset from the search index", e);
		}

		try {
			DatasetFactory.clearCache(getId());
		} catch (Throwable t) {
			LibsLogger.error(Table.class, "Cannot remove ad-hoc Dataset from cache", t);
		}
	}
	
	/**
	 * Removes all tables from this dataset
	 * @throws IOException 
	 * @throws ValidationException 
	 */
	public void clear() throws ValidationException, IOException {
		DatasetClearJob j = new DatasetClearJob();
		for (Table table : iterateTables()) {
//			table.remove();
			j.addToRemove(table.getId());
		}
		
		if (j.getTableToRemove().size() > 0)
			j.save();
		
//		SearchManager.removeObject(id, columnFamily);Object(table.getCombinedKey(), table.getColumnFamily());
//		removeTablesFromIndex();
	}
	
	private void removeTablesFromIndex() throws IOException {
//		BeansSearchManager.getSearchManager().remove(WeblibsConst.KEYSPACE_LOWERCASE, 
//					Table.COLUMN_FAMILY.toLowerCase(), 
//					new Query()
//						.addTerm(new StringTerm("datasetId.id", getId().getId(), TermRequirement.MUST)));
	}
	
	@Override
	public void index() throws IOException {
		indexThis();
		
		indexTables();
		
		indexPermissions();
	}
	
	@Override
	public void deindex() throws IOException {
		// TODO why there is nothing here?
	}
	
	private void indexThis() throws IOException {
		// TODO DEBUG fix 1.0d0 into 1.0e0
		getMeta().fixD();
		
		SearchManager.index(this);
	}
	
	private void indexTables() throws IOException {
		for (Table table : iterateTables()) {
			table.index();
		}
	}
	
	@Override
	public MutationList getRemoveMutations() {
		return super.getRemoveMutations()
				.addInsertMutations(new RemoveDatasetDependencies(this))
				.addInsertMutations(new RemoveFromIndex(this));
	}
		
//	public UUID getId() {
//		return id;
//	}
//
//	public Dataset setId(UUID id) {
//		this.id = id;
//		return this;
//	}
	
	public User getOwner() throws IOException {
		return UserFactory.getUser(getUserId());
	}

//	public UUID getUserId() {
//		return userId;
//	}
//
//	public Dataset setUserId(UUID userId) {
//		this.userId = userId;
//		return this;
//	}

	public String getName() {
		return name;
	}

	public Dataset setName(String name) {
		this.name = name;
		return this;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public SimpleDate getCreationDate() {
		return new SimpleDate(creationMs);
	}

	public void setCreationDate(SimpleDate creationDate) {
//		this.creationDate = creationDate;
		this.creationMs = creationDate.getTimeInMillis();
	}
	
	public Dataset addMeta(Map meta) {
		for (Object key : meta.keySet()) {
			String metaName = null;
			Object metaValue = null;
			
			if (key instanceof String)
				metaName = (String) key;
			else
				metaName = String.valueOf(key);
			
			if (meta.get(key) instanceof List)
				metaValue = ((List) meta.get(key)).get(0).toString();
			else
				metaValue = meta.get(key);
			
			if (metaValue instanceof String) {
				String v = (String) metaValue;
				if (RegexUtils.isDouble(v))
					getMeta().add(metaName, NumberUtils.toDouble(v));
				else if (RegexUtils.isInt(v))
					getMeta().add(metaName, NumberUtils.toLong(v));
				else
					getMeta().add(metaName, metaValue);
			} else if (metaValue instanceof Double) {
				getMeta().add(metaName, (Double) metaValue);
			} else if (metaValue instanceof Float) {
				getMeta().add(metaName, (Float) metaValue);
			} else if (metaValue instanceof Long) {
				getMeta().add(metaName, (Long) metaValue);
			} else if (metaValue instanceof Integer) {
				getMeta().add(metaName, (Integer) metaValue);
			} else {
				LibsLogger.error(Dataset.class, "Unrecognized value for meta: " + metaValue + ", skipping...");
			}
		}
		
		return this;
	}

	public Dataset addMeta(String name, Object value) {
		getMeta().add(new Meta(name, value));
		return this;
	}
	
	public Dataset addMeta(String name, Object value, String description) {
		getMeta().add(new Meta(name, value, description));
		return this;
	}

	public long getCreationMs() {
		return creationMs;
	}

	public void setCreationMs(long creationMs) {
		this.creationMs = creationMs;
//		this.creationDate = new SimpleDate(creationMs);
	}

	public MetaList getMeta() {
		if (meta == null)
			meta = new MetaList();
		return meta;
	}

	public void setMeta(MetaList meta) {
		this.meta = meta;
	}

	public Iterable<Table> iterateTables() {
//		return new Iterable<Table>() {
//			@Override
//			public Iterator<Table> iterator() {
				return Table.iterateTables(getId());// getTables(getId()).iterator();
//			}
//		};
	}

	public void makeReadOnly() throws ValidationException, IOException {
		// TODO bring back permissions
//		new Allowed(getUserId(), Rights.DATASET_WRITE_ENABLED + getId(), false).save();
//		new Allowed(getUserId(), Rights.DATASET_DELETE_ENABLED + getId(), false).save();
	}

	public void rename(String newNameTemplate) throws ValidationException, IOException {
		String newName = newNameTemplate;
		
		while (true) {
			String group = RegexUtils.firstGroup("(\\$[\\w\\d\\_]+)", newName, Pattern.CASE_INSENSITIVE);
			
			if (group == null)
				break;
			
			String paramName = group.substring(1);
			String newName2 = newName.replaceAll("\\$" + paramName, getMeta().get(paramName).getAsString());
			
			if (newName2.equals(newName))
				break;
			
			newName = newName2;
		}
		
		setName(newName);
		
		save();
	}

	public long getEditMs() {
		return editMs;
	}

	public void setEditMs(long editMs) {
		this.editMs = editMs;
	}
}
