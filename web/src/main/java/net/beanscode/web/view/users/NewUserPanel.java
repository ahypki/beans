package net.beanscode.web.view.users;

import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.id;
import static org.rendersnake.HtmlAttributesFactory.name;

import java.io.IOException;

import net.beanscode.cli.users.User;
import net.hypki.libs5.pjf.components.Component;

import org.rendersnake.HtmlCanvas;

public class NewUserPanel extends Component {
	
	private String userEmail = null;

	public NewUserPanel() {
		
	}
	
	public NewUserPanel(User user) {
		this.userEmail = user.getEmail();
	}
	
	public NewUserPanel(String userEmail) {
		this.userEmail = userEmail;
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {		
		html
			.form(id("form-new-user").class_("form-horizontal"))
				.div(class_("form-group"))
					.label(class_("control-label col-lg-5")
							.for_("email"))
						.content("Email")
					.div(class_("col-lg-7"))
						.input(id("email")
								.name("email")
								.type("text")
								.value(userEmail != null ? userEmail : "")
								.disabled(userEmail != null ? "disabled" : null))
						.if_(userEmail != null)
							.input(name("email")
								.type("hidden")
								.value(userEmail != null ? userEmail : ""))
						._if()
					._div()
				._div()
				.div(class_("form-group"))
					.label(class_("control-label col-lg-5")
							.for_("email"))
						.content("Password")
					.div(class_("col-lg-7"))
						.input(id("pass1")
								.name("pass1")
								.type("password"))
					._div()
				._div()
				.div(class_("form-group"))
					.label(class_("control-label col-lg-5")
							.for_("email"))
						.content("Password again")
					.div(class_("col-lg-7"))
						.input(id("pass2")
								.name("pass2")
								.type("password"))
					._div()
				._div()
//				.button(class_("btn btn-default")
//						.type("submit")
//						.onClick(opsCall(opAjaxJs(RegistrationOptionsSave.URL, "$('#form-registration').serialize()")) + "; return false;"))
//					.content("Save changes")
			._form();
	}
	
}
