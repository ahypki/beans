package net.beanscode.model.cass;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.beanscode.model.BeansSettings;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.settings.SettingFactory;
import net.hypki.libs5.weblibs.settings.SettingValue;

import org.apache.hadoop.mapreduce.InputFormat;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.JobContext;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

public class UniTableInputFormat extends InputFormat {

	public UniTableInputFormat() {

	}

	@SuppressWarnings("unchecked")
	@Override
	public RecordReader createRecordReader(InputSplit split, TaskAttemptContext context) throws IOException, InterruptedException {
		return new UniTableRecordReader();
	}
	
	@Override
	public List getSplits(JobContext arg0) throws IOException, InterruptedException {
		List<InputSplit> splits = new ArrayList<>();
		
		int nrSplits = SettingFactory.getSettingValueInt(BeansSettings.SETTING_PIG_MODE, "splits", 8);
		int nrOneTableSplits = SettingFactory.getSettingValueInt(BeansSettings.SETTING_PIG_MODE, "oneTableSplits", 8);
		
		LibsLogger.info(UniTableInputFormat.class, "Number of splits ", nrSplits, 
				", nr of one table splits ", nrOneTableSplits);
		
		for (int beansTableSplit = 0; beansTableSplit < nrSplits; beansTableSplit++) {
			for (int fileSplit = 0; fileSplit < nrOneTableSplits; fileSplit++) {				
				splits.add(new UniTableInputSplit(new String[] {"" + beansTableSplit, "" + fileSplit}));
			}
		}
		
		return splits;
	}
}
