package net.beanscode.web.view.notebook;

import java.io.IOException;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.plugin.EditorClassGet;
import net.beanscode.pojo.NotebookEntry;
import net.beanscode.web.view.ui.BeansComponent;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.pjf.components.jersey.SessionBean;
import net.hypki.libs5.utils.LibsLogger;

public class NotebookEntryEditorFactory {
	
	
	
	

	public static NotebookEntryEditorPanel getEditorPanel(BeansComponent parent, final Class<? extends NotebookEntry> type) throws CLIException, IOException {

//		String editorClassName = 
//				new EditorClassGet(sb).run(type.getSimpleName());
		String editorClassName = new ApiCallCommand(parent)
				.setApi("api/plugin/editorClass")
				.addParam("type", type.getSimpleName())
				.run()
				.getResponse()
				.getAsString("editorClass");
		
		
		if (editorClassName == null) {
			LibsLogger.error(NotebookEntryEditorFactory.class, "Cannot find editor class for type ", type);
			return null;
		}
		
		try {
			Class<? extends NotebookEntryEditorPanel> editorClass = (Class<? extends NotebookEntryEditorPanel>) Class.forName(editorClassName);
			return (NotebookEntryEditorPanel) ((NotebookEntryEditorPanel) editorClass.newInstance())
					.setParams(parent.getParams())
					.setSessionBean(parent.getSessionBean());
		} catch (Exception e) {
			LibsLogger.error(NotebookEntryEditorFactory.class, "Cannot create editor component class " + editorClassName, e);
			return null;
		}
	}
}
