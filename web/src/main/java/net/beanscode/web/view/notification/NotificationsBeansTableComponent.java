package net.beanscode.web.view.notification;

import static net.beanscode.cli.BeansCliCommand.API_NOTIFICATION_ANSWER;
import static net.beanscode.cli.BeansCliCommand.API_NOTIFICATION_ARCHIVE;
import static net.beanscode.cli.BeansCliCommand.API_NOTIFICATION_READ;
import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.id;

import java.io.IOException;
import java.util.List;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.notification.Notification;
import net.beanscode.cli.notification.NotificationSearchResults;
import net.beanscode.cli.notification.NotificationType;
import net.beanscode.cli.notification.Question;
import net.beanscode.web.view.ui.BetaTableComponent;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.buttons.ButtonGroup;
import net.hypki.libs5.pjf.components.labels.Label;
import net.hypki.libs5.pjf.components.labels.MessageType;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpJs;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpRemove;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.url.Params;

import org.rendersnake.HtmlCanvas;

public class NotificationsBeansTableComponent extends BetaTableComponent {
	
	private NotificationSearchResults notifications = null;

	public NotificationsBeansTableComponent() {
		
	}
	
	public NotificationsBeansTableComponent(Component parent) {
		super(parent);
	}
	
	@Override
	protected String getSettingsPrefix() {
		return "notifications-table-";
	}
	
	@Override
	protected long getMaxHits() throws IOException {
		return getNotifications().getMaxHits();
	}
	
	@Override
	protected int getNumberOfColumns() throws IOException {
		return 5;
	}
		
	@Override
	protected boolean isHeaderColumnVisible() throws IOException {
		return true;
	}
	
	@Override
	protected String getColumnName(int col) throws IOException {
		if (col == 0)
			return "Type";
		else if (col == 1)
			return "Status";
		else if (col == 2)
			return "Date";
		else if (col == 3)
			return "Description";
		else if (col == 4)
			return "Options";
		else
			return null;
	}
	
	private OpList onQuestionAnswer() throws IOException, ValidationException {
		String id = getParams().getString("notificationId");
		String questionId = getParams().getString("questionId");
		String answer = getParams().getString("answer");
//		Notification n = NotificationFactory.getNotification(id);
//		Notification n = new NotificationGet().run(id);
		
//		n.questionAnswered(questionId, answer);
		
		new ApiCallCommand(this)
			.setApi(API_NOTIFICATION_ANSWER)
			.addParam("notificationId", id)
			.addParam("questionId", questionId)
			.addParam("answer", answer)
			.run();
		
		return new OpList()
				.add(new OpRemove("." + id));
	}
	
	private OpList onArchive() throws IOException, ValidationException {
		final String notificationId = getParams().getString("notificationId");
		
//		Notification n = new NotificationGet().run(notificationId);
//		Notification n = NotificationFactory.getNotification(notificationId);
		
//		n.setArchived(true);
//		n.save();
		
		new ApiCallCommand(this)
			.setApi(API_NOTIFICATION_ARCHIVE)
			.addParam("notificationId", notificationId)
			.run();
		
		return new OpList()
				.add(new OpJs("$('." + notificationId + "').parent().parent().remove();"))
//				.add(new OpRemove("." + notificationId))
				.add(new OpBeansInfo("Notification archived"))
//				.add(onTotalRowsUpdate())
				;
	}
	
	private OpList onRead() throws IOException, ValidationException {
		final String notificationId = getParams().getString("notificationId");
		
//		Notification n = NotificationFactory.getNotification(notificationId);
//		
//		if (n != null) {
//			n.setRead(!n.isRead());
//			n.save();
//		}
		
		new ApiCallCommand(this)
			.setApi(API_NOTIFICATION_READ)
			.addParam("notificationId", notificationId)
			.run();
		
		Notification n = new ApiCallCommand(this)
			.setApi(ApiCallCommand.API_NOTIFICATION_GET)
			.addParam("id", notificationId)
			.run()
			.getResponse()
			.getAsObject(Notification.class);
		
		return new OpList()
//				.add(new OpReplace("." + notificationId, renderRow(new HtmlCanvas(), n).toHtml()))
//				.add(new OpBeansInfo("Notification marked as read"))
				;
	}
	
	private String typeToBadgeClasses(NotificationType type) {
		if (type == NotificationType.ERROR)
			return "badge badge-danger";
		else if (type == NotificationType.INFO)
			return "badge badge-primary";
		else if (type == NotificationType.OK)
			return "badge badge-success";
		else if (type == NotificationType.WARN)
			return "badge badge-warning";
		else if (type == NotificationType.QUESTION)
			return "badge badge-info";
		return "badge badge-secondary";
	}
	
	@Override
	protected void renderCell(int row, int col, HtmlCanvas html)
			throws IOException {
		Notification n = getNotifications().getNotification().get(row % getPerPage());
		
		if (col == 0) {
			html
				.span(class_("notifications-table badge-" + n.getId().getId() + " " + typeToBadgeClasses(n.getNotificationType())))
					.content(n.getNotificationType().toString());
		} else if (col == 1) {
			html
				.span(id("tr-" + n.getId().getId())
						.class_("notifications-table " + n.getId().getId()))
					.render(new Label(notificationTypeToMessageType(n.getNotificationType()), 
								n.getTitle()))
				._span();
		} else if (col == 2) {
			html
				.span()
					.content(n.getCreationDate().toStringHuman());
		} else if (col == 3) {
			html
				.span()
					.content(n.getDescription());
			
			if (n.isQuestionDefined()) {
				for (Question q : n.getQuestions()) {
					html
						.br()
						.span()
							.content(q.getText(100))
						.br();
					ButtonGroup buttons = new ButtonGroup();
					for (String answer : q.getAnswers()) {
						buttons.addButton(new Button(answer)
								.add(new OpComponentClick(this, "onQuestionAnswer", new Params()
										.add("notificationId", n.getId().getId())
										.add("answer", answer)
										.add("questionId", q.getQuestionId())
										)));
					}
					html
						.render(buttons);
				}
			}
		} else {
			html
				.render(new ButtonGroup()
//						.addButton(new Button()
//								.setAwesomeicon(n.isRead() ? "eye-slash" : "eye")
//								.setTooltip(n.isRead() ? "Mark as unread" : "Mark as read")
//								.add(new OpComponentClick(this, "onRead", new Params().add("notificationId", n.getId().getId()))))
						.addButton(new Button()
								.setName("Archive")
								.setReplacedHtmlClass("btn btn-xs btn-default")
								.setAwesomeicon("archive")
								.setTooltip("Archive this notification")
								.add(new OpComponentClick(this, "onArchive", new Params().add("notificationId", n.getId().getId()))))
//						.addButton(new Button()
//								.setVisible(isAdmin())
//								.setGlyphicon("cog"))
						);
		}
	}
	
	private MessageType notificationTypeToMessageType(NotificationType notificationType) {
		if (notificationType == null)
			return MessageType.DEFAULT;
		
		else if (notificationType == NotificationType.ERROR)
			return MessageType.DANGER;
		
		else if (notificationType == NotificationType.INFO)
			return MessageType.INFO;
		
		else if (notificationType == NotificationType.OK)
			return MessageType.SUCCESS;
		
		else if (notificationType == NotificationType.QUESTION)
			return MessageType.WARNING;
		
		else if (notificationType == NotificationType.WARN)
			return MessageType.WARNING;
		
		else
			return MessageType.DEFAULT;
	}

	@Override
	protected boolean isFilterVisible() {
		return true; //isExtended();
	}

	@Override
	protected List<Button> getMenu() throws IOException {
		return new ButtonGroup()
//			.setButtonSize(isExtended() ? ButtonSize.NORMAL : ButtonSize.SMALL)
//			.addButton(new Button("Refresh")
//					.add(new OpComponentClick(this, "onRefresh", new Params().add("page", 0))))
//			.addButton(false, new Button("Details...")
//					.setGlyphicon("glyphicon glyphicon-list-alt")
//					.add(new OpComponentClick(NotificationContent.class, UUID.random(), "showContent"))
//					)
//			.addButton(new Button("Auto-refresh")
//					.setReplacedHtmlClass("btn btn-xs btn-default notifications-auto-refresh-on")
////					.setGlyphicon("unchecked")
//					.add(new OpShow("#" + getId() + " .notifications-auto-refresh-off"))
//					.add(new OpHide("#" + getId() + " .notifications-auto-refresh-on"))
//					.add(new OpSetVal("#" + getId() + " .timer", 1))
//					.add(new OpComponentTimer(this, "onRefresh", 200))
//					)
//			.addButton(new Button("Auto-refresh")
//					.setReplacedHtmlClass("btn btn-xs btn-default notifications-auto-refresh-off")
////					.setGlyphicon("check")
//					.setVisible(false)
//					.add(new OpShow("#" + getId() + " .notifications-auto-refresh-on"))
//					.add(new OpHide("#" + getId() + " .notifications-auto-refresh-off"))
//					.add(new OpSetVal("#" + getId() + " .timer", 0))
//	//				.add(new OpComponentTimer(this, "onRefresh", 200))
//					)
			.addButton(new Button()
					.setTooltip("Show only errors")
					.setAwesomeicon("fa fa-bug")
					.setReplacedHtmlClass("btn btn-xs btn-default")
					.add(new OpComponentClick(this, 
							"onRefresh", 
							new Params().add("type", NotificationType.ERROR.toString())))
					)
			.addButton(new Button()
					.setTooltip("Show only warnings")
					.setAwesomeicon("fa fa-exclamation-circle")
					.setReplacedHtmlClass("btn btn-xs btn-default")
					.add(new OpComponentClick(this, 
							"onRefresh", 
							new Params().add("type", NotificationType.WARN.toString())))
					)
			.addButton(new Button()
					.setTooltip("Show only question notifications")
					.setAwesomeicon("fa fa-question-circle")
					.setReplacedHtmlClass("btn btn-xs btn-default")
					.add(new OpComponentClick(this, 
							"onRefresh", 
							new Params().add("type", NotificationType.QUESTION.toString())))
					)
			.addButton(new Button()
					.setTooltip("Show only information notifications")
					.setAwesomeicon("fa fa-info-circle")
					.setReplacedHtmlClass("btn btn-xs btn-default")
					.add(new OpComponentClick(this, 
							"onRefresh", 
							new Params().add("type", NotificationType.INFO.toString())))
					)
			.addButton(new Button()
					.setTooltip("Show only successful notifications")
					.setAwesomeicon("fa fa-check-circle")
					.setReplacedHtmlClass("btn btn-xs btn-default")
					.add(new OpComponentClick(this, 
							"onRefresh", 
							new Params().add("type", NotificationType.OK.toString())))
					)
			.addButton(new Button()
					.setTooltip("Close all notifications")
					.setAwesomeicon("fa fa-angle-double-down")
					.setReplacedHtmlClass("btn btn-xs btn-default")
					.add(new OpComponentClick(this, "onCloseAll"))
					)
			.getButtons()
			;
	}
	
	private OpList onCloseAll() throws CLIException, IOException {
		new ApiCallCommand(getSessionBean())
			.setApi("api/notification/archive")
			.addParam("filter", getFilter())
			.run();
		
		return new OpList()
			.add(new OpComponentClick(this, "onRefresh"));
	}
	
	private NotificationSearchResults getNotifications() {
		if (this.notifications == null) {
			try {
				ApiCallCommand call = (ApiCallCommand) new ApiCallCommand(getSessionBean())
					.setApi("api/notification/search")
					.addParam("from", getPage() * getPerPage())
					.addParam("size", getPerPage())
					.addParam("filter", getFilter())
					.addParam("type", getParams().getString("type"))
					.run();
				
				this.notifications = new NotificationSearchResults();
				this.notifications.setNotification((List<Notification>) call.getResponse().getAsList("notifications", Notification.class));
				this.notifications.setMaxHits(call.getResponse().getAsLong("maxHits"));
				
			} catch (CLIException | IOException e) {
				LibsLogger.error(NotificationsBeansTableComponent.class, "Cannot load notifications", e);
				this.notifications = new NotificationSearchResults();
			}
		}
		return this.notifications;
	}

	@Override
	protected void renderTitle(HtmlCanvas html) throws IOException {
		html
			.span()
				.content("Notifications");
	}

	@Override
	protected boolean isExapandable() throws IOException {
		return false;
	}

	@Override
	protected void renderExpandable(int row, HtmlCanvas html) throws IOException {
		
	}

	@Override
	protected String getTableCss() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}
	
}
