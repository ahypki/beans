package net.beanscode.web.view.dataset;

import static org.rendersnake.HtmlAttributesFactory.id;

import java.io.IOException;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.pojo.dataset.Dataset;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.upload.UploadView;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.op.OpJs;
import net.hypki.libs5.utils.LibsLogger;

import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

public class DatasetPanel extends BeansComponent {
	
//	@Expose
	private Dataset dataset = null;
	
	@Expose
	private UUID datasetId = null;
	
	public DatasetPanel() {
		
	}
	
	public DatasetPanel(BeansComponent parent, Dataset dataset) {
		super(parent);
//		setSecurityContext(parent.getSecurityContext());
//		setParams(parent.getParams());
		setDataset(dataset);
	}
	
	private boolean isWriteAllowed() throws CLIException, IOException {
//		return new DatasetIsWriteAllowed(getSessionBean()).run(getUserEmail(), getDataset().getId().getId());
		try {
			return new ApiCallCommand(this)
				.setApi("api/dataset/isWriteAllowed")
				.addParam("datasetId", getDataset().getId().getId())
				.addParam("userId", getUserId().getId())
				.run()
				.getResponse()
				.getAsBoolean("writeAllowed");
		} catch (CLIException | IOException e) {
			LibsLogger.error(DatasetPanel.class, "Cannot check if a dataset is writable", e);
			e.printStackTrace();
			return false;
		}
	}
	
	private boolean isDeleteAllowed() throws CLIException, IOException {
//		return new DatasetIsDeleteAllowed(getSessionBean()).run(getUserEmail(), getDataset().getId().getId());
		return new ApiCallCommand(this)
			.setApi("api/dataset/isDeleteAllowed")
			.addParam("datasetId", getDataset().getId().getId())
			.addParam("userId", getUserId().getId())
			.run()
			.getResponse()
			.getAsBoolean("deleteAllowed");
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		final boolean isWriteAllowed = isWriteAllowed();
		
		html = html
			.div(id(getId()))
//				.render_if(new DatasetBtnGroup(getDataset(), ButtonSize.NORMAL, false), isWriteAllowed)
				.render(new DatasetTabs(this, getDataset()))			
				;
		
		if (isWriteAllowed)
			getOpList().addLast(new OpJs("new Dropzone('#import-multiple-tables', " +
				"{url: '" + UploadView.UPLOAD_URL + "?datasetId=" + getDataset().getId() + "', addRemoveLinks: false, maxFilesize: 100000, clickable: true})"
				+ ".on('success', function(file, response) { pjfOps(response); })"
				+ ".on('error', function(file, response) { pjfOps(response); });"));
		
		html
			._div()
			;
	}

	public Dataset getDataset() {
		if (dataset == null) {
			try {
				dataset = new ApiCallCommand(this)
					.setApi("api/dataset")
					.addParam("dataset-id", getDatasetId().getId())
					.run()
					.getResponse()
					.getAsObject(Dataset.class);
			} catch (CLIException | IOException e) {
				LibsLogger.error(DatasetPanel.class, "Cannot get Dataset from DB", e);
			}
		}
		return dataset;
	}

	public DatasetPanel setDataset(Dataset dataset) {
		this.dataset = dataset;
		setDatasetId(dataset.getId());
		return this;
	}

	public UUID getDatasetId() {
		return datasetId;
	}

	public void setDatasetId(UUID datasetId) {
		this.datasetId = datasetId;
	}
	
}
