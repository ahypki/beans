package net.beanscode.cli.plugin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.beanscode.cli.BeansCliCommand;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.cli.noninteractivecli.CliArg;
import net.hypki.libs5.pjf.RestMethod;
import net.hypki.libs5.pjf.components.jersey.SessionBean;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.url.Params;

import com.google.gson.JsonElement;

public class PluginGetList extends BeansCliCommand {
	
	@CliArg(name = "user",
			description = "User for which the list of Plugins will be checked",
			required = false)
	private String email = null;

	public PluginGetList(SessionBean sb) {
		super(RestMethod.GET, sb);
	}

	@Override
	public String getUrlPath() {
		return "api/plugin/list";
	}

	@Override
	public void processResponse() {
		LibsLogger.debug(PluginGetList.class, "List of plugins for user ", getEmail(), ": ", getResponse().toString());
	}

	@Override
	public Params getRESTParams() {
		return new Params()
			.add("userId", getUserId(getEmail()))
			;
	}

	@Override
	public String getCLICommandName() {
		return "plugin list";
	}

	@Override
	public String getCLICommandDescription() {
		return "Gets the list of plugins for a given user";
	}
	
	public List<PluginPOJO> run(String userEmail) throws CLIException, IOException {
		super.run(new String[] {"--user", userEmail});
		
		List<PluginPOJO> plugins = new ArrayList<PluginPOJO>();
		for (JsonElement onePlugin : getResponse().getAsJson().getAsJsonArray()) {
			plugins.add(JsonUtils.fromJson(onePlugin, PluginPOJO.class));
		}
		return plugins;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
