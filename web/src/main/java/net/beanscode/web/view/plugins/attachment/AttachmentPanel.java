package net.beanscode.web.view.plugins.attachment;

import static org.rendersnake.HtmlAttributesFactory.class_;

import java.io.IOException;

import javax.ws.rs.core.SecurityContext;

import org.rendersnake.HtmlAttributesFactory;
import org.rendersnake.HtmlCanvas;
import org.rendersnake.Renderable;

import com.google.gson.annotations.Expose;

import net.beanscode.cli.BeansCliCommand;
import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.notebooks.AttachmentEntry;
import net.beanscode.web.view.notebook.NotebookEntryEditorPanel;
import net.beanscode.web.view.ui.UploadPanel;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.utils.url.Params;

public class AttachmentPanel extends NotebookEntryEditorPanel {
	
	@Expose
	private UploadPanel uploadPanel = null;

	@Override
	public Renderable getView() {
		return new Renderable() {
			@Override
			public void renderOn(HtmlCanvas html) throws IOException {
				html
					.render(new AttachmentFilesListComponent(AttachmentPanel.this, getAttachmentEntry())
							.setDeleteEnabled(false));
			}
		};
	}

	@Override
	protected Renderable getEditor() {
		return new Renderable() {
			@Override
			public void renderOn(HtmlCanvas html) throws IOException {
				html
					.div(class_("row"))
						.div(class_("col-6"))
							.render(new AttachmentFilesListComponent(AttachmentPanel.this, getAttachmentEntry()))
						._div()
						.div(class_("col-6"))
							.render(getUploadPanel())
						._div()
					._div()
					;
			}
		};
	}
	
	private OpList onRefresh() {
		return new OpList();
	}
	
	protected AttachmentEntry getAttachmentEntry() {
		return (AttachmentEntry) getNotebookEntry();
	}

	@Override
	protected Button getMenu() throws IOException {
		return null;
	}

	@Override
	protected OpList opOnPlay(SecurityContext sc, Params params) throws IOException {
		new ApiCallCommand(this)
			.setApi("api/notebook/entry/start")
			.addParam("entryId", getEntryId())
			.run();
		
		return null;
	}

	@Override
	protected OpList opOnEdit(SecurityContext sc, Params params) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected OpList opOnStop(SecurityContext sc, Params params) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected OpList opOnRemove(SecurityContext sc, Params params) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected OpList onRefresh(SecurityContext sc, Params params) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	private UploadPanel getUploadPanel() {
		if (uploadPanel == null)
			uploadPanel = new UploadPanel(AttachmentPanel.this, "upload" + getId())
					.setUrl("/" + BeansCliCommand.API_NOTEBOOKENTRY_UPLOAD + "?entryId=" + getNotebookEntry().getId().getId())
					.onSuccess(new OpComponentClick(AttachmentPanel.this, "onRefresh"));
		return uploadPanel;
	}

	private void setUploadPanel(UploadPanel uploadPanel) {
		this.uploadPanel = uploadPanel;
	}

}
