package net.beanscode.web.view.plots;

import static net.hypki.libs5.utils.string.RegexUtils.allGroups;
import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.string.StringUtilities.split;
import static net.hypki.libs5.utils.utils.NumberUtils.toDouble;
import static org.rendersnake.HtmlAttributesFactory.add;
import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.for_;
import static org.rendersnake.HtmlAttributesFactory.id;
import static org.rendersnake.HtmlAttributesFactory.name;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.core.SecurityContext;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.datasets.Table;
import net.beanscode.cli.notebooks.Plot;
import net.beanscode.cli.notebooks.PlotSeries;
import net.beanscode.pojo.NotebookEntry;
import net.beanscode.pojo.NotebookEntryProgress;
import net.beanscode.web.BeansAPI;
import net.beanscode.web.BeansWebConst;
import net.beanscode.web.view.notebook.NotebookEntryEditorPanel;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.DatasetTableSelector;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.buttons.ButtonGroup;
import net.hypki.libs5.pjf.components.buttons.InputButton;
import net.hypki.libs5.pjf.components.labels.DangerLabel;
import net.hypki.libs5.pjf.components.labels.WarningLabel;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.components.ops.OpComponentTimer;
import net.hypki.libs5.pjf.op.OpAppend;
import net.hypki.libs5.pjf.op.OpDisable;
import net.hypki.libs5.pjf.op.OpEnable;
import net.hypki.libs5.pjf.op.OpHide;
import net.hypki.libs5.pjf.op.OpJs;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpReplace;
import net.hypki.libs5.pjf.op.OpSet;
import net.hypki.libs5.pjf.op.OpSetAttr;
import net.hypki.libs5.pjf.op.OpSetData;
import net.hypki.libs5.pjf.op.OpSetVal;
import net.hypki.libs5.pjf.op.OpShow;
import net.hypki.libs5.plot.PlotType;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.api.APICallType;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.string.Base64Utils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.url.Params;
import net.hypki.libs5.utils.utils.NumberUtils;

import org.apache.commons.lang3.text.StrBuilder;
import org.rendersnake.HtmlCanvas;
import org.rendersnake.Renderable;

public class PlotPanel extends NotebookEntryEditorPanel {
	
	private static final String MAX_PLOTS = "MAX_PLOTS";
	
	public PlotPanel() {
		
	}
	
	public PlotPanel(BeansComponent parent) {
		super(parent);
	}

	@Override
	public Renderable getView() {
		return new Renderable() {
			@Override
			public void renderOn(HtmlCanvas html) throws IOException {
				html
//					.div(id("ch" class="parcoords" style="height:240px;"></div>

					.div()
						.div(class_("plot-view")
								.id("plot-view-" + getId())
								.style("width: 95%;")
//								.data("onvisible", 
//										new OpList()
//											.add(new OpComponentTimer(PlotPanel.this, 
//												"onRefresh", 
//												RandomUtils.nextInt(1000, 3000),
//												new Params()
//													.add("entryId", getPlot().getId()))
//											)
////											.add(new OpBeansInfo("on visible: " + getPlot().getId()))
////											.add(new OpAddClass("#plot-view-" + getId(), "os-theme-light"))
//											.toStringOnclick())
								)
							.write("Plot '" + getPlot().getName() + "' is not yet ready (" + getPlot().getId() + ")")
//							.write(new OpComponentTimer(PlotPanel.this, 
//												"onRefresh", 
//												RandomUtils.nextInt(1000, 3000),
//												new Params()
//													.add("entryId", getPlot().getId()))
//										.toStringAdhock(), false)
						._div()
						.div(id("plot-legend-" + getId())
								.class_("dygraph-legend-beans"))
						._div()
						.div(id("plot-logbuttons-" + getId())
								.class_("dygraph-legend-beans"))
						._div()
					._div()
//					.if_(getPlot().getPlotType(0) == PlotType.PARALLEL_COORDINATES)
//						.div(id("grid" + getId()))
//	//						.content("grid...")
//						._div()
//					._if()
					
//					.div(class_("chart parcoords")
//							.id("ch" + getPlot().getId().getId())
//							.add("style", "height:440px;")
//							)
//					._div()
					;
			}
		};
	}

	@Override
	protected Renderable getEditor() {
		return new Renderable() {
			@Override
			public void renderOn(HtmlCanvas html) throws IOException {
				String source = getPlot().getFigure().getPlots().size() > 0 ? 
						getPlot().getFigure().getPlots().get(0).getDataSource() : "";
				String[] sourceArr = source != null && source.contains("@@") 
						? split(source, "@@") : new String[] {source, ""};
				String sortBy = getPlot().getFigure().getPlots().size() > 0 ? 
						getPlot().getFigure().getPlots().get(0).getSortBy() : "";
				
				html
					.div(class_("plot-query"))
						.label(for_("plot-title"))
							.content("Title")
						.input(name("plot-title")
								.type("text")
								.value(getPlot().getFigure().getTitle("Plot title"))
								.class_("title form-control"))
						.render(new DatasetTableSelector(PlotPanel.this, 
								sourceArr[0],
								sourceArr[1],
								"plot-ds-query", 
								"plot-tb-query"))
//						.span()
//							.content("plot: ")
//						.br()
						.div(class_("plot-series"))
							.label()
								.content("Plot columns")
						;
				
				if (getPlot().getFigure().getPlots().size() > 0) {
					for (int i = 0; i < getPlot().getFigure().getPlots().size(); i++) {
						net.hypki.libs5.plot.Plot plotSeries = getPlot().getFigure().getPlots().get(i);
						html
							.render(new PlotOneSeries(PlotPanel.this, plotSeries));
					}
				} else {
					html
						.render(new PlotOneSeries(PlotPanel.this));
				}
							
					html
						._div()
						.render(new ButtonGroup()
								.addButton(new Button("Add")
										.add(new OpComponentClick(PlotPanel.this, "onNewPlotSeries", new Params().add("entryId", getPlot().getId())))))
						
						.div()
							.label()
								.content("Options")
										
							.div(class_("row"))
								.div(class_("col-sm-2"))
									.span(class_("name"))
										.content("Sort by column")
								._div()
								.div(class_("col-sm-10"))
									.input(name("plot-sort-by")
											.class_("form-control")
											.value(sortBy))
								._div()
							._div()
							
							.div(class_("row"))
								.div(class_("col-sm-2"))
									.span(class_("name"))
										.content("Color by column")
								._div()
								.div(class_("col-sm-10"))
									.input(name("plot-color-by")
											.class_("form-control")
											.value(getPlot().getFigure().getPlots().size() > 0 ?
													getPlot().getFigure().getPlots().get(0).getColorBy() : ""))
								._div()
							._div()
							
							.div(class_("row"))
								.div(class_("col-sm-2"))
									.span(class_("name"))
										.content("Split by column")
								._div()
								.div(class_("col-sm-10"))
									.input(name("plot-split-by")
											.class_("form-control")
											.value(getPlot().getFigure().getPlots().size() > 0 ?
													getPlot().getFigure().getPlots().get(0).getSplitBy() : ""))
								._div()
							._div()
							
							
							.div(class_("row"))
								.div(class_("col-sm-2"))
									.span(class_("name"))
										.content("X-label")
								._div()
								.div(class_("col-sm-10"))
									.input(name("plot-label-x")
											.class_("form-control")
											.value(getPlot().getFigure().getXLabel("")))
								._div()
							._div()
							
							
							.div(class_("row"))
								.div(class_("col-sm-2"))
									.span(class_("name"))
										.content("Y-label")
								._div()
								.div(class_("col-sm-10"))
									.input(name("plot-label-y")
											.class_("form-control")
											.value(getPlot().getFigure().getYLabel("")))
								._div()
							._div()
						
						._div()
					._div()
					;
			}
		};
	}
	
	
	
	private OpList onNewPlotSeries() throws IOException {
		return new OpList()
				.add(new OpAppend("#" + getId() + " .plot-series", new PlotOneSeries(this).toHtml()));
	}

	@Override
	protected Button getMenu() throws IOException {
		return new Button()
			.addButton(new Button()
				.setHtmlClass("refresh")
				.setAwesomeicon("fa fa-sync")
				.add(new OpComponentClick(this, "onRefresh", new Params().add("entryId", getPlot().getId()).add("plotNr", 0)))
				)
			.addButton(new Button()
				.setHtmlClass("previous")
				.setAwesomeicon("fa fa-backward")
				.add(new OpComponentClick(this, 
						"changePlot", 
						new Params()
							.add("entryId", getPlot().getId())
							.add("plotNr", 0)))
				)
			.addButton(new InputButton("plotNrInput")
				.setHtmlClass("plotNrInput")
				.add(new OpComponentClick(this, 
						"changePlot", 
						new Params()
							.add("entryId", getPlot().getId())
							.add("readPlotNrFromInput", 1)))
				)
			.addButton(new Button()
				.setHtmlClass("next")
				.setAwesomeicon("fa fa-forward")
				.add(new OpComponentClick(this, 
						"changePlot", 
						new Params()
							.add("entryId", getPlot().getId())
							.add("plotNr", 1)))
				)
//			.addButton(new Button()
//				.setGlyphicon("download-alt")
//				.setTooltip("Download")
//					.addButton(new Button("Download as " + Plot.PNG)
//						.setGlyphicon("picture")
//						.add(new OpComponentClick(this, "onDownload", new Params()
//								.add("entryId", getPlot().getId())
//								.add("plotFormat", "PNG"))))
//					)
			;
	}
	
	@Override
	public OpList getInitOpList() {
		return super.getInitOpList()
//				.add(new OpComponentTimer(this, "onRefresh", RandomUtils.nextInt(1, 2000), new Params().add("entryId", getPlot().getId())))
				;
	}
	
	@Override
	protected OpList onRefresh(SecurityContext sc, Params params) throws IOException {
//		final UUID plotId = new UUID(params.getString("entryId"));
		int plotNr = params.getInteger("plotNr", 0);
		OpList ops = new OpList();
		
//		init(plotId);
		
		// check if plotNr is not larger than the number of available plots
		final int maxPlots = getNotebookEntryProgress().getMeta().getAsInteger(MAX_PLOTS, 0);
		if (maxPlots > 0 && plotNr >= maxPlots) {
			return ops
					.add(new OpDisable("#" + getId() + " .next"))
					.add(new OpSet("#" + getId() + " .plot-view", new WarningLabel("There is no plot nr " + (plotNr + 1)).toString()));
		} else
			ops
				.add(new OpEnable("#" + getId() + " .next"));
		
		ops
			.add(new OpSetVal("#" + getId() + " .plotNrInput", plotNr));
		
		// disabling prev and next buttons if needed
		boolean noMorePlots = (plotNr + 1) >= maxPlots ? true : false;
		ops
			.add(getPlot().isSplitBySet() ? new OpEnable("#" + getId() + " .previous") 		: new OpDisable("#" + getId() + " .previous"))
			.add(getPlot().isSplitBySet() && noMorePlots == false ? new OpEnable("#" + getId() + " .next")			: new OpDisable("#" + getId() + " .next"))
			.add(getPlot().isSplitBySet() ? new OpEnable("#" + getId() + " .plotNrInput")	: new OpDisable("#" + getId() + " .plotNrInput"));
			
		if (getNotebookEntryProgress().isFailed()) {
			// check if plot failed
			return ops
				.add(new OpSet("#" + getId() + " .plot-view", 
						new DangerLabel(getNotebookEntryProgress().getMessage()).toString()));
		} else if (getNotebookEntryProgress().isRunning() && !getNotebookEntryProgress().isFinished()) {
			// plot was started but not finished
			return ops
				.add(new OpSet("#" + getId() + " .plot-view", new HtmlCanvas()
																	.img(add("src", "/static/res/ajax-loader-1.gif", false))
																	.write(" ")
																	.span()
																		.content("Plot '" + getPlot().getName() + "' is being prepared...")
																	.toHtml()))
				.add(new OpComponentTimer(this, "onRefresh", BeansWebConst.PLOT_AUTOREFRESH_INTERVAL_MS, new Params().add("entryId", getPlot().getId()).add("plotNr", plotNr)))
				;
		} else if (!getNotebookEntryProgress().isRunning() && !getNotebookEntryProgress().isFinished()) {
//			try {
//				getPlot().getPlotStatus().setStarted(true);
//				getPlot().save();
//				
//				new PlotPrepareJob(getNotebookEntry().getId()).save();
//				
				// plot was started but not finished
				return ops
					.add(new OpSet("#" + getId() + " .plot-view", new HtmlCanvas()
//																		.img(add("src", "/static/res/ajax-loader-1.gif", false))
//																		.write(" ")
																		.span()
																			.content("Plot is not started")
																		.toHtml()))
//					.add(new OpComponentTimer(this, "refresh", BeansConst.PLOT_AUTOREFRESH_INTERVAL_MS, new Params().add("entryId", getPlot().getId()).add("plotNr", plotNr)))
					;
//			} catch (ValidationException e) {
//				LibsLogger.error(PlotPanel.class, "Cannot start preparing plot", e);
//			}
		} 
//		else {
//			throw new IOException("Unexected case");
//		}
				
		final PlotType plotType = getPlot().getFigure().getPlots().size() > 0 
				? getPlot().getFigure().getPlots().get(0).getPlotType() : null;
		if (plotType == PlotType.POINTS || plotType == PlotType.LINES) {
			
			// TODO teraz już nie produkować plików png
//			HtmlCanvas img = new HtmlCanvas()
//				.img(add("src", "/view/image?" + UrlUtilities.toQueryString("entryId", getPlot().getId(), "plotNr", plotNr, "type", "png", "dummy", UUID.random().getId()), false));
			
			// TODO usuwanie tych zmiennych zrobić jeszcze
			String js = String.format("g" + getId() + " = new Dygraph(document.getElementById('plot-view-%s'), \"%s\", %s);", 
					getId(),
					"/view/data?queryId=" + getPlot().getId().getId() + "&plotNr=" + plotNr + "&type=plain",
					new DygraphsOptions()
						.setTitle(getPlot().getName())
						.setLines(plotType == PlotType.LINES)
						.setLabelsDiv("plot-legend-" + getId())
						.setXlabel(getPlot().getFigure().getXLabel(null))
						.setYlabel(getPlot().getFigure().getYLabel(null))
						.setLogscale(getPlot().getFigure().isxLogscale())
						.toString()
			)
//			+ "g" + getId() + ".updateOptions({\"labels\" : [\"X\", \"Y1\", \"Y2\", \"y3\", \"y4\" ]});"
			;
			
			return ops
				.add(new OpJs(js))
				.add(new OpReplace("#plot-logbuttons-" + getId(), new ButtonGroup()
					.addButton(new Button("Linear scale")
							.setEnabled(getPlot().getFigure().isxLogscale())
							.setHtmlClass("linear-scale")
							.add(new OpJs("g" + getId() + ".updateOptions({ logscale: false });"))
							.add(new OpDisable("#" + getId() + " .linear-scale"))
							.add(new OpEnable("#" + getId() + " .log-scale"))
							.add(new OpComponentClick(this, "onLinearscaleOn", new Params().add("entryId", getPlot().getId())))
							)
					.addButton(new Button("Log scale")
							.setEnabled(!getPlot().getFigure().isxLogscale())
							.setHtmlClass("log-scale")
							.add(new OpJs("g" + getId() + ".updateOptions({ logscale: true });"))
							.add(new OpEnable("#" + getId() + " .linear-scale"))
							.add(new OpDisable("#" + getId() + " .log-scale"))
							.add(new OpComponentClick(this, "onLogscaleOn", new Params().add("entryId", getPlot().getId())))
							)
//					.addButton(new Button("Reverse X axis")
//							.setHtmlClass("reverse-x")
//							.add(new OpJs(String.format("g%s.xAxisRange().reverse(););", getId(), getId())))
////							.add(new OpJs("alert(g" + getId() + ".yAxisRange())"))
////							.add(new OpDisable("#" + getId() + " .linear-scale"))
////							.add(new OpEnable("#" + getId() + " .log-scale"))
////							.add(new OpComponentClick(this, "onLinearscaleOn", new Params().add("entryId", getPlot().getId())))
//							)
					.addButton(new Button("Reverse Y axis")
							.setHtmlClass("reverse-y")
							.add(new OpJs(String.format("g%s.updateOptions({ valueRange: g%s.yAxisRange().reverse() });", getId(), getId())))
//							.add(new OpJs("alert(g" + getId() + ".yAxisRange())"))
//							.add(new OpDisable("#" + getId() + " .linear-scale"))
//							.add(new OpEnable("#" + getId() + " .log-scale"))
//							.add(new OpComponentClick(this, "onLinearscaleOn", new Params().add("entryId", getPlot().getId())))
							)
//					.addButton(new Button("gg")
//					.addButton(new Button("Expand legend"))
					.toHtml()))
				.add(new OpSetData("#plot-view-" + getPlot().getId().getId(), "onvisiblefired", "true"))
//				.add(new OpJs())
					;
			
		} else if (plotType == PlotType.BOXES) {
			
//			HtmlCanvas html = new HtmlCanvas();
//			html
//				.div(class_("chart parcoords")
//						.id("ch" + getPlot().getId().getId())
//						.add("style", "height:440px;"))
//				._div();
////			.div(class_("chart parcoords")
////					.id("ch" + getPlot().getId().getId())
////					.add("style", "height:440px;")
////					)
////			._div()
//			
//			String idShort = "ch" + getPlot().getId().getId();
//			return ops
//				.add(new OpAfter("#" + getId() + " .plot-view", html.toHtml()))
//				.add(new OpJs("d3.selectAll(\"#" + idShort + " svg\").remove();"))
//				.add(new OpRemove("#" + idShort + " svg"))
//				.add(new OpRemove("#" + idShort + " .chart-title"))
//				.add(new OpRemove("#" + idShort + " .chart-xlabel"))
//				.add(new OpRemove("#" + idShort + " .chart-ylabel"))
//				.add(new OpSet("#" + getId() + " .plot-view", ""))
//				.add(new OpJs(WebPlotsFactory.createBoxes(getPlot(), plotNr)));
			
			return new OpList();
			
//		} else if (plotType == PlotType.PARALLEL_COORDINATES) {
			
//			HtmlCanvas html = new HtmlCanvas();
//			html
//				.div(class_("chart parcoords")
//						.id("ch" + getPlot().getId().getId())
//						.add("style", "height:440px;"))
//				._div();
//			
//			String idShort = "ch" + getPlot().getId().getId();
//			return ops
//				.add(new OpJs("d3.selectAll(\"#" + idShort + " svg\").remove();"))
//				.add(new OpRemove("#" + idShort + " svg"))
//				.add(new OpRemove("#" + idShort + " "))
//				.add(new OpAfter("#" + getId() + " .plot-view", html.toHtml()))
//				.add(new OpRemove("#" + idShort + " .chart-title"))
//				.add(new OpSet("#" + getId() + " .plot-view", ""))
//				.add(new OpJs(WebPlotsFactory.createParallelCoordinates(getPlot(), plotNr)));
			
//			return new OpList();
//			
		} else
			return null;
//			throw new NotImplementedException();
	}
	
	private void onLinearscaleOn() throws CLIException, IOException {
		getPlot().getFigure().setxLogscale(false);
		
//		new ApiCallCommand(getSessionBean())
//			.setApi("api/plot/logscale")
//			.addParam("--plotId", getPlot().getId().getId())
//			.addParam("--logscaleOn", false)
//			.run();
		
		new ApiCallCommand(getSessionBean())
			.setApi("api/notebook/entry")
			.setApiCallType(APICallType.POST)
			.addParam("entryId", getPlot().getId().getId())
			.addParam("META_LOGSCALE_ON", "false")
			.run();
	}
	
	private void onLogscaleOn() throws CLIException, IOException {
		getPlot().getFigure().setxLogscale(true);

//		new ApiCallCommand(getSessionBean())
//			.setApi("api/plot/logscale")
//			.addParam("--plotId", getPlot().getId().getId())
//			.addParam("--logscaleOn", true)
//			.run();
		
		new ApiCallCommand(getSessionBean())
			.setApi("api/notebook/entry")
			.setApiCallType(APICallType.POST)
			.addParam("entryId", getPlot().getId().getId())
			.addParam("META_LOGSCALE_ON", "true")
			.run();
	}
	
	@Override
	public NotebookEntry getNotebookEntry() {
		if (super.getNotebookEntry() == null) {
			final String plotId = getParams().getString("entryId");
			if (StringUtilities.notEmpty(plotId)) {
				try {
					setNotebookEntry(new ApiCallCommand(getSessionBean())
										.setApi("api/notebook/entry/get")
										.addParam("entryId", plotId)
										.run()
										.getResponse()
										.getAsObject(Plot.class));
				} catch (IOException e) {
					LibsLogger.error(PlotPanel.class, "Cannot get Notebook entry from DB", e);
				}
			}
		}
		return super.getNotebookEntry();
	}
	
	public OpList changePlot(Params params) throws IOException {
//		final String plotId = params.getString("entryId");
		
//		setNotebookEntry(NotebookEntry.getNotebookEntry(new UUID(plotId)));
		
		int plotNr = params.getInteger("plotNr", 0);
		int readPlotNrFromInput = params.getInteger("readPlotNrFromInput", 0);
		
		if (readPlotNrFromInput > 0)
			plotNr = params.getInteger("plotNrInput", 0);
		
		boolean noMorePlots = false;
		if (plotNr >= getNotebookEntryProgress().getMeta().getAsInteger(MAX_PLOTS, 0)) {
			// check if the plotNr actually exists
			LibsLogger.debug(PlotPanel.class, "PlotNr ", plotNr, " does not exist, subtracting by 1");
			plotNr = getNotebookEntryProgress().getMeta().getAsInteger(MAX_PLOTS, 0) - 1;
			noMorePlots = true;
		}
		
		final int previous = plotNr > 0 ? plotNr - 1 : 0;
		final int next = plotNr >= 0 ? plotNr + 1 : 1;
		
		boolean prevBtnEnable = true;
		boolean nextBtnEnable = true;
		
		if (!getPlot().isSplitBySet())
			prevBtnEnable = false;
		if (!getPlot().isSplitBySet() || noMorePlots)
			nextBtnEnable = false;
		
		return new OpList()
			.add(new OpSetAttr("#" + getId() + " .previous", "onclick", 
					new OpComponentClick(this, "changePlot", 
						new Params()
							.add("entryId", getPlot().getId())
							.add("plotNr", previous)
						).toStringOnclick()
						))
			.add(new OpSetAttr("#" + getId() + " .next", "onclick", 
					new OpComponentClick(this, "changePlot", 
						new Params()
							.add("entryId", getPlot().getId())
							.add("plotNr", next)
						).toStringOnclick()
						))
			// enable/disable next/previous buttons
			.add(prevBtnEnable ? new OpEnable("#" + getId() + " .previous") : new OpDisable("#" + getId() + " .previous"))
			.add(nextBtnEnable ? new OpEnable("#" + getId() + " .next")		: new OpDisable("#" + getId() + " .next"))
			.add(noMorePlots ? new OpBeansInfo("There are no more plots in this figure") : null)
			.add(new OpComponentClick(this, "onRefresh", new Params().add("entryId", getPlot().getId()).add("plotNr", plotNr).add("dummy", UUID.random().getId())))
			;
	}

	@Override
	protected OpList opOnPlay(SecurityContext sc, Params params) throws IOException {
		try {
			// TODO bring this back
//			getPlot().getFigure().clearSortBy();
//			for (String sortBy : (List<String>) getParams().getList("plot-sort-by"))
//				getPlot().addSortBy(sortBy);
			
			getPlot().setName(getParams().getString("plot-title"));
			getPlot().getFigure().setTitle(getParams().getString("plot-title"));
			
			getPlot().getFigure().clearLabels();
			getPlot().getFigure().setXLabel(getParams().getString("plot-label-x"));
			getPlot().getFigure().setYLabel(getParams().getString("plot-label-y"));
			
			List<String> types = getParams().getList("plot-type");
			List<String> columnX = getParams().getList("plot-column-x");
			List<String> columnY = getParams().getList("plot-column-y");
			List<String> titles = getParams().getList("plot-series-title");
			List<String> samples = getParams().getList("plot-series-sample");
			List<String> dsQuery = getParams().getList("plot-ds-query");
			List<String> tbQuery = getParams().getList("plot-tb-query");
			List<String> sortBy = getParams().getList("plot-sort-by");
			List<String> colorBy = getParams().getList("plot-color-by");
			List<String> splitBy = getParams().getList("plot-split-by");
			
			getPlot().getFigure().getPlots().clear();
			int i = 0;
			for (String type : types) {
//				List<String> oneColumns = allGroups("([^:\\\"]+)", columns.get(i));

				net.hypki.libs5.plot.Plot onePlot = new net.hypki.libs5.plot.Plot();
				onePlot.setPlotType(PlotType.parse(type, PlotType.POINTS));
				
				onePlot.setDataSource(dsQuery.get(0) + "@@" + tbQuery.get(0));
				
				onePlot.setX(columnX.get(i));
				onePlot.setY(columnY.get(i));
//				for (String col : oneColumns) {
//					if (notEmpty(col))
//						oneSeries. addColumnExpr(col.trim());
//				}
				
				if (i < titles.size()
						&& notEmpty(titles.get(i)))
					onePlot.setLegend(titles.get(i));
				
				if (i < samples.size()
						&& notEmpty(samples.get(i)))
					onePlot.setSample(toDouble(samples.get(i), 1.0));
				
				if (sortBy != null
						&& i < sortBy.size()
						&& notEmpty(sortBy.get(i)))
					onePlot.setSortBy(sortBy.get(i));
				
				if (colorBy != null
						&& i < colorBy.size() 
						&& notEmpty(colorBy.get(i)))
					onePlot.setColorBy(colorBy.get(i));
				
				if (splitBy != null 
						&& i < splitBy.size()
						&& notEmpty(splitBy.get(i)))
					onePlot.setSplitBy(splitBy.get(i));
//			getPlot().setSplitBy(getParams().getString("plot-split-by"));
				
				getPlot().getFigure().addPlot(onePlot);
				
				i++;
			}

			new ApiCallCommand(getSessionBean())
				.setApi("api/notebook/entry")
				.setApiCallType(APICallType.POST)
				.addParam("entryId", getPlot().getId())
				.addParam("name", getParams().getString("plot-title"))
				.addParam("FIGURE", JsonUtils.objectToString(getPlot().getFigure()))
//				.addParam("META_DS_QUERY", getParams().getString("plot-ds-query"))
//				.addParam("META_TB_QUERY", getParams().getString("plot-tb-query"))
//				.addParam("META_SORTBY", getPlot().getFigure().getSortByAsString())
//				.addParam("META_COLORBY", getPlot().getColorBy())
//				.addParam("META_SPLITBY", getPlot().getSplitBy())
//				.addParam("META_LABELS", Base64Utils.encode(getPlot().getLabels() != null ? 
//						getPlot().getLabels().toString() : null))
//				.addParam("META_TYPES", Base64Utils.encode(getPlot().getTypes() != null ? 
//						getPlot().getTypes().toString() : null))
//				.addParam("META_SERIES", Base64Utils.encode(getPlot().getSeries() != null ? 
//						getPlot().getSeries().toString() : null))
//				.addParam("META_SERIES", Base64Utils.encode(getPlot().getSeries() != null ? 
//						getPlot().getSeries().toString() : null))
				.run();
			
			new ApiCallCommand(getSessionBean())
				.setApi("api/notebook/entry/start")
				.addParam("entryId", getPlot().getId())
				.run();
		} catch (IOException e) {
			LibsLogger.error(PlotPanel.class, "Cannot save notebook entry", e);
		}
		
		return new OpList()
			.add(new OpComponentClick(this, "changePlot", new Params().add("entryId", getPlot().getId())))
			.add(new OpShow("#" + getId() + " .refresh"))
			.add(new OpShow("#" + getId() + " .previous"))
			.add(new OpShow("#" + getId() + " .next"))
			.add(new OpShow("#" + getId() + " .plotNrInput"));
	}
	
	@Override
	protected OpList opOnEdit(SecurityContext sc, Params params) throws IOException {
		return new OpList()
			.add(new OpHide("#" + getId() + " .refresh"))
			.add(new OpHide("#" + getId() + " .previous"))
			.add(new OpHide("#" + getId() + " .next"))
			.add(new OpHide("#" + getId() + " .plotNrInput"));
	}

	@Override
	protected OpList opOnStop(SecurityContext sc, Params params) throws IOException {
		boolean refresh = params.get("plot-type") != null;
		return new OpList()
			.add(refresh, new OpComponentClick(this, "onRefresh", new Params().add("entryId", getPlot().getId())))
			.add(new OpShow("#" + getId() + " .refresh"))
			.add(new OpShow("#" + getId() + " .previous"))
			.add(new OpShow("#" + getId() + " .next"))
			.add(new OpShow("#" + getId() + " .plotNrInput"));
	}

	@Override
	protected OpList opOnRemove(SecurityContext sc, Params params) throws IOException {
		try {
			new ApiCallCommand(this)
				.setApi("api/notebook/entry/remove")
				.setApiCallType(APICallType.DELETE)
				.addParam("entryId", params.getString("entryId"))
				.run();
		} catch (IOException e) {
			LibsLogger.error(PlotPanel.class, "Cannot remove Notebook Entry", e);
		}
		
		return new OpList();
	}
	
	public Plot getPlot() {
		return (Plot) getNotebookEntry();
	}
}
