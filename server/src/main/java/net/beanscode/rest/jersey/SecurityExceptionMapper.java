package net.beanscode.rest.jersey;

import java.net.URI;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.url.HttpResponse;

@Provider
public class SecurityExceptionMapper implements ExceptionMapper<SecurityException> {

	public Response toResponse(SecurityException exception) {
		try {
			return Response
					.seeOther(new URI("/login"))
					.build();
			
//			return Response
//				.status(Response.Status.NOT_ACCEPTABLE)
//				.entity(new OpList()
//				.add(new OpRedirect("/login")).toString())
//				.build();
			
//			return Response
//					.status(Response.Status.UNAUTHORIZED)
//					.entity("You must log in")
//					.build();
		} catch (Exception e) {
			LibsLogger.error(SecurityExceptionMapper.class, "Cannot redirect", e);
			return Response
					.status(Response.Status.UNAUTHORIZED)
					.entity("Internal error")
					.build();
		}
		
//		return Response
//				.status(Response.Status.NOT_ACCEPTABLE)
//				.entity(new OpList()
//				.add(new OpRedirect("/login")).toString())
//				.build();
	}

}
