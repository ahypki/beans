package net.beanscode.pojo;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;

import com.google.gson.annotations.Expose;

import net.beanscode.pojo.autoupdate.AutoUpdateSettings;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.api.APICall;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.string.Meta;
import net.hypki.libs5.utils.string.MetaList;
import net.hypki.libs5.utils.string.StringUtilities;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

public class NotebookEntry {
	
	@Expose
	private UUID id = null;

	@Expose
	private UUID userId = null;
	
	@Expose
	private UUID notebookId = null;
	
	@Expose
	private String name = null;
	
	@Expose
	private String notebookName = null;
	
	@Expose
	private String type = null;

	@Expose
	private SimpleDate create = null;
	
	@Expose
	private SimpleDate lastEdit = null;
	
	@Expose
	@NotNull
	@AssertValid
	private AutoUpdateSettings autoUpdateOptions = null;
	
	@Expose
	private String description = null;
	
	@Expose
	private MetaList meta = null;

	// TODO obsolete, remove it
	@Expose
	private NotebookEntryStatus status = NotebookEntryStatus.NEW;
	
	public NotebookEntry() {
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public UUID getNotebookId() {
		return notebookId;
	}

	public void setNotebookId(UUID notebookId) {
		this.notebookId = notebookId;
	}

	public UUID getUserId() {
		return userId;
	}

	public void setUserId(UUID userId) {
		this.userId = userId;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public SimpleDate getCreate() {
		return create;
	}

	public void setCreate(SimpleDate create) {
		this.create = create;
	}

	public SimpleDate getLastEdit() {
		return lastEdit;
	}

	public void setLastEdit(SimpleDate lastEdit) {
		this.lastEdit = lastEdit;
	}

	public String getType() {
		return type;
	}
	
	public String getTypeShort() {
		return StringUtilities.substringLast(getType(), '.');
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public AutoUpdateSettings getAutoUpdateOptions() {
		return autoUpdateOptions;
	}
	
	public void setAutoUpdateOptions(AutoUpdateSettings autoUpdateOptions) {
		this.autoUpdateOptions = autoUpdateOptions;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getNotebookName() {
		return notebookName;
	}

	public void setNotebookName(String notebookName) {
		this.notebookName = notebookName;
	}
	
	public MetaList getMeta() {
		if (meta == null)
			meta = new MetaList();
		return meta;
	}
	
	public String getMetaAsString(String metaName) {
		return getMetaAsString(metaName, null);
	}
	
	public String getMetaAsString(String metaName, String defaultValue) {
		Meta meta = getMeta().get(metaName);
		return meta != null ? meta.getAsString() : defaultValue;
	}
	
	public boolean getMetaAsBoolean(String metaName, boolean defaultValue) {
		Meta m = getMeta().get(metaName);
		if (m != null && m.getValue() != null)
			return m.getAsBoolean();
		return defaultValue;
	}
	
	public Double getMetaAsDouble(String metaName, Double defaultValue) {
		Meta m = getMeta().get(metaName);
		if (m != null && m.getValue() != null)
			return m.getAsDouble();
		return defaultValue;
	}
	
	public UUID getMetaAsUUID(String metaName, UUID defaultValue) {
		Meta m = getMeta().get(metaName);
		if (m != null)
			return notEmpty(m.getAsString()) ? new UUID(m.getAsString()) : defaultValue;
		return defaultValue;
	}
	
	public <T> T getMetaAsObject(String metaName, Class<T> clazz) {
		return getMeta().getAsObject(metaName, clazz);
	}

	public void setMeta(MetaList meta) {
		this.meta = meta;
	}
	
	public <T extends NotebookEntry> T setMeta(String name, Object value) {
		getMeta().add(name, value);
		return (T) this;
	}

	public NotebookEntryStatus getStatus() {
		return status;
	}

	public void setStatus(NotebookEntryStatus status) {
		this.status = status;
	}

}
