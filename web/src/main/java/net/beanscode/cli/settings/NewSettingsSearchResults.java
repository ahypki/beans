package net.beanscode.cli.settings;

import java.util.List;

import net.beanscode.web.view.settings.Setting;

import com.google.gson.annotations.Expose;

public class NewSettingsSearchResults {

	@Expose
	private List<Setting> settings = null;
	
	@Expose
	private int maxHits = 0;
	
	public NewSettingsSearchResults() {
		
	}

	public List<Setting> getSettings() {
		return settings;
	}

	public NewSettingsSearchResults setSettings(List<Setting> settings) {
		this.settings = settings;
		return this;
	}

	public int getMaxHits() {
		return maxHits;
	}

	public NewSettingsSearchResults setMaxHits(int maxHits) {
		this.maxHits = maxHits;
		return this;
	}
	
	public NewSettingsSearchResults setMaxHits(long maxHits) {
		this.maxHits = (int) maxHits;
		return this;
	}
}
