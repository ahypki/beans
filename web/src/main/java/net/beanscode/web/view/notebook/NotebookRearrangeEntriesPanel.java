package net.beanscode.web.view.notebook;

import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.id;
import static org.rendersnake.HtmlAttributesFactory.type;

import java.io.IOException;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.notebooks.Notebook;
import net.beanscode.pojo.NotebookEntry;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.BetaButtonGroup;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.utils.url.Params;

import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

public class NotebookRearrangeEntriesPanel extends BeansComponent {
	
	@Expose
	private UUID notebookId = null;
	
	private Notebook notebook = null;

	public NotebookRearrangeEntriesPanel() {
		
	}
	
	public NotebookRearrangeEntriesPanel(Component parent, UUID notebookId) {
		super(parent);
		setNotebookId(notebookId);
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.render(new BetaButtonGroup()
					.add(new Button("Save order")
							.add(new OpComponentClick(this, "onSave")))
					.add(new Button("Show notebook")
							.add(new OpComponentClick(NotebookContent.class, 
									UUID.random(),
									"onShow",
									new Params()
										.add("notebookId", getNotebook().getId())))));
		
		
		html
			.div()
				.ul(id(getId())
						.class_("notebook-rearrange"));
			
			for (NotebookEntry entry : getNotebook().iterateEntries(getSessionBean())) {
				html
					.li(class_("notebook-entry-dragable callout callout-info")
							.draggable("true"))
						.header()
							.content(entry.getTypeShort() + ": " + entry.getName())
						.input(type("hidden")
								.name("entry")
								.value(entry.getId().getId()))
					._li();
			}
			
			html
				._ul()
				.write("<script type=\"text/javascript\">", false)
				.write("$(function() { $( \".notebook-rearrange\" ).sortable({ connectWith: \".notebook-entry-dragable\" }).disableSelection();});", false)
				.write("</script>", false)
			._div()
			;
	}
	
	private OpList onSave() throws CLIException, IOException {
		ApiCallCommand api = new ApiCallCommand(getSessionBean())
			.setApi("api/notebook/order")
			.addParam("--notebookId", getNotebookId().getId());
		
		for (Object id : getParams().getList("entry")) {
			api.addParam("--entryId", id);
		}
		
		api.run();
		
		return new OpList()
			.add(new OpBeansInfo("Order of the entries saved successfully"));
	}
	
	private Notebook getNotebook() throws CLIException, IOException {
		if (notebook == null) {
			notebook = new ApiCallCommand(this)
							.setApi("api/notebook/get")
							.addParam("notebookId", getNotebookId().getId())
							.run()
							.getResponse()
							.getAsObject(Notebook.class);
		}
		return notebook;
	}

	public UUID getNotebookId() {
		return notebookId;
	}

	public void setNotebookId(UUID notebookId) {
		this.notebookId = notebookId;
	}
}
