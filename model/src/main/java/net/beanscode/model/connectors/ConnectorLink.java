package net.beanscode.model.connectors;

import java.io.Serializable;

import net.beanscode.model.plugins.Connector;
import net.beanscode.model.plugins.ConnectorInitParams;
import net.hypki.libs5.utils.LibsLogger;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class ConnectorLink implements Serializable {

	@Expose
	@NotNull
	@NotEmpty
	private String connectorClass = null;
	
	@Expose
	private ConnectorInitParams connectorInitParams = null;
	
	private Connector connectorInstance = null;
		
	public ConnectorLink() {
		
	}
	
	public ConnectorLink(Connector connector) {
		if (connector != null) {
			setConnectorClass(connector.getClass().getName());
			setConnectorInitParams(connector.getInitParams());
		}
	}
	
	public ConnectorLink(Class connectorClass, ConnectorInitParams initParams) {
		setConnectorClass(connectorClass.getName());
		setConnectorInitParams(initParams);
	}
	
	public ConnectorLink(String connectorClass, ConnectorInitParams initParams) {
		setConnectorClass(connectorClass);
		setConnectorInitParams(initParams);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ConnectorLink) {
			ConnectorLink cl = (ConnectorLink) obj;
			return cl.getConnectorClass().equals(this.getConnectorClass())
					&& cl.getConnectorInitParams().equals(getConnectorInitParams());
		} else if (obj instanceof Connector) {
			Connector c = (Connector) obj;
			return c.getClass().equals(this.getConnectorClass())
					&& c.getInitParams().equals(getConnectorInitParams());
		}
		return false;
	}
	
	@Override
	public String toString() {
		return getConnectorClass() + ":" + getConnectorInitParams();
	}

	public String getConnectorClass() {
		return connectorClass;
	}

	public ConnectorLink setConnectorClass(String connectorClass) {
		this.connectorClass = connectorClass;
		this.connectorInstance = null;
		return this;
	}

	public ConnectorInitParams getConnectorInitParams() {
		return connectorInitParams;
	}

	public ConnectorLink setConnectorInitParams(ConnectorInitParams connectorInitParams) {
		this.connectorInitParams = connectorInitParams;
		this.connectorInstance = null;
		return this;
	}

	public Connector getConnectorInstance() {
		if (connectorInstance == null) {
			try {
				connectorInstance = (Connector) Class.forName(getConnectorClass()).newInstance();
				connectorInstance.setInitParams(getConnectorInitParams());
			} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
				LibsLogger.error(ConnectorLink.class, "Cannot create Connector instance for " + getConnectorClass() + " and init params " + getConnectorInitParams(), e);
			}
		}
		return connectorInstance;
	}

	private void setConnectorInstance(Connector connectorInstance) {
		this.connectorInstance = connectorInstance;
	}
}
