package net.beanscode.model.unittests.connectors;

import java.io.IOException;

import net.beanscode.model.cass.Data;
import net.beanscode.model.connectors.CassandraConnector;
import net.beanscode.model.connectors.PlainConnector;
import net.hypki.libs5.db.cassandra.CassandraProvider;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.weblibs.WeblibsConst;

import org.junit.Test;

public class CassandraConnectorUnitTest extends ConnectorTestCase {

	@Test
	public void testReadWriteCassandraConnector() throws IOException, ValidationException {
		FileExt source = new FileExt("src/main/resources/testdata/line-1-10.json");
		
		CassandraProvider cassProvider = new CassandraProvider();
		cassProvider.createKeyspace(WeblibsConst.KEYSPACE);
		cassProvider.createTable(WeblibsConst.KEYSPACE, new Data().getSchema());
		cassProvider.clearTable(WeblibsConst.KEYSPACE, Data.COLUMN_FAMILY);
		
		UUID tableId = UUID.random();
		UUID tableId2 = UUID.random();
		UUID tableIdMapDb = UUID.random();
		
		CassandraConnector writer = new CassandraConnector(tableId);
		CassandraConnector writer2 = new CassandraConnector(tableId2);
//		MapDbConnector writerMapDb = new MapDbConnector(tableIdMapDb);
		for (Row row : new PlainConnector(source)) {
			LibsLogger.debug(CassandraConnectorUnitTest.class, "Row read from PlainConnector: ", row);
			writer.write(row);
			writer2.write(row);
//			writerMapDb.write(row);
		}
		writer.close();
		writer2.close();
//		writerMapDb.close();
		
		for (Row row : new CassandraConnector(tableId)) {
			LibsLogger.debug(CassandraConnectorUnitTest.class, "Row read from CassandraConnector: ", row);
		}
		for (Row row : new CassandraConnector(tableId2)) {
			LibsLogger.debug(CassandraConnectorUnitTest.class, "Row read from CassandraConnector: ", row);
		}
		
		final double sum1Plain = sumColumn(new PlainConnector(source), "x");
		final double sum1Cass = sumColumn(new CassandraConnector(tableId), "x");
		final double sum2Cass = sumColumn(new CassandraConnector(tableId2), "x");
//		final double sum2MapDb = sumColumn(new MapDbConnector(tableIdMapDb), "x");
		assertTrue(sum1Plain == sum1Cass, "Sum does not match " + sum1Plain + " " + sum1Cass);
		assertTrue(sum1Plain == sum2Cass, "Sum does not match " + sum1Plain + " " + sum2Cass);
//		assertTrue(sum1Plain == sum2MapDb, "Sum does not match " + sum1Plain + " " + sum2MapDb);
		
		LibsLogger.debug(CassandraConnectorUnitTest.class, "Finished!");
	}
}
