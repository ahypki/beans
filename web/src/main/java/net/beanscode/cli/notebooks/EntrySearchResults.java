package net.beanscode.cli.notebooks;

import java.util.List;

import net.beanscode.pojo.NotebookEntry;
import net.beanscode.pojo.dataset.Dataset;

import com.google.gson.annotations.Expose;

public class EntrySearchResults {

	@Expose
	private List<NotebookEntry> notebooks = null;
	
	@Expose
	private int maxHits = 0;
	
	public EntrySearchResults() {
		
	}

	public List<NotebookEntry> getNotebookEntries() {
		return notebooks;
	}

	public EntrySearchResults setNotebookEntries(List<NotebookEntry> notebookEntries) {
		this.notebooks = notebookEntries;
		return this;
	}

	public int getMaxHits() {
		return maxHits;
	}

	public EntrySearchResults setMaxHits(int maxHits) {
		this.maxHits = maxHits;
		return this;
	}
	
	public EntrySearchResults setMaxHits(long maxHits) {
		this.maxHits = (int) maxHits;
		return this;
	}
}
