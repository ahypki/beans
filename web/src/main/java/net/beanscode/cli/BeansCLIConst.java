package net.beanscode.cli;

import net.hypki.libs5.utils.reflection.SystemUtils;

public class BeansCLIConst {

	private static final String SETTINGS_DIR = SystemUtils.getHomePath() + "/.beans-software/";
	private static final String SETTINGS_FILENAME = ".beans-client.json";
	public static final String SETTINGS_FILENAME_PATH = SETTINGS_DIR + SETTINGS_FILENAME;
	
	public static final String MAGIC_KEY = "magickey";
}
