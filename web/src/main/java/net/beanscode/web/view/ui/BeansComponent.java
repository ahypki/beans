package net.beanscode.web.view.ui;

import java.io.IOException;

import javax.ws.rs.core.SecurityContext;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.users.User;
import net.beanscode.web.BeansPage;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.url.Params;

public class BeansComponent extends Component {
	
	private User user = null;

	public BeansComponent() {
		
	}
	
	public BeansComponent(Component initializingComponent) {
		super(initializingComponent);
	}
	
	public BeansComponent(SecurityContext securityContext, Params params) {
		super(securityContext, params);
	}
	
	@Override
	public boolean hasRight(String right) {
		try {
			return new ApiCallCommand(this)
				.setApi("api/user/allowed")
				.addParam("user-email", getUserEmail())
				.addParam("right", right)
				.run()
				.getResponse()
				.getAsBoolean("allowed");
		} catch (CLIException | IOException e) {
			LibsLogger.error(BeansComponent.class, "Cannot check if User has right " + right, e);
			return false;
		}
	}
	
	public boolean isAdmin() {
		if (getUserId() == null)
			return false;
		return hasRight(BeansPage.RIGHT_ADMIN);
	}
	
	public String getUserEmail() throws CLIException, IOException {
		return getUser() != null ? getUser().getEmail() : null;
	}

	protected User getUser() throws CLIException, IOException {
		if (user == null) {
			user = new ApiCallCommand(getSessionBean())
						.setApi("api/user/get")
						.addParam("userId", getUserId().getId())
						.run()
						.getResponse()
						.getAsObject(User.class);
		}
		return user;
	}

	protected void setUser(User user) {
		this.user = user;
	}
}
