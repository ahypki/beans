package net.beanscode.web.view.summary;

import java.io.IOException;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.web.view.ui.BetaButtonGroup;
import net.beanscode.web.view.ui.CardPanel;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.beanscode.web.view.ui.modals.OpModalQuestion;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpList;

import org.rendersnake.HtmlCanvas;

public class DatabaseSummaryCardPanel extends CardPanel {

	public DatabaseSummaryCardPanel() {
		
	}
	
	public DatabaseSummaryCardPanel(Component parent) {
		super(parent);
	}

	@Override
	protected void renderTitle(HtmlCanvas html) throws IOException {
		html
			.write("Database summary");
	}

	@Override
	protected void renderContent(HtmlCanvas html) throws IOException {
		html
			.render(new BetaButtonGroup()
					.add(new Button("Check the integrity of the BEANS database")
//							.setHtmlClass("btn-primary btn-sm")
							.add(new OpModalQuestion("Confirmation", 
									"Do you want to start checking the integrity?", 
									new OpComponentClick(this, 
											"onValidate",
											null)))));
	}
	
	private OpList onValidate() throws CLIException, IOException {
		new ApiCallCommand(this)
			.setApi("api/admin/validate")
			.run();
		
		return new OpList()
			.add(new OpBeansInfo("Integrity job started successfully. You will be notified about the results."));
	}

	@Override
	protected void renderFooter(HtmlCanvas html) throws IOException {
		
	}
	
	
}
