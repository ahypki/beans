package net.beanscode.model.demo;

import java.io.IOException;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.jobs.Job;
import net.hypki.libs5.weblibs.user.User;
import net.hypki.libs5.weblibs.user.UserFactory;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class DemoDataJob extends Job {
	
	@NotNull
	@NotEmpty
	@AssertValid
	@Expose
	private UUID userId = null;

	public DemoDataJob() {
		
	}
	
	public DemoDataJob(UUID userId) {
		setUserId(userId);
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		try {
			final User user = UserFactory.getUser(getUserId());
			
			if (user == null) {
				LibsLogger.error(DemoDataJob.class, "Cannot find user with ID ", getUserId(), ". Creting DEMO data aborted.");
				return;
			}
			
			DemoData.createDemoDatasetsLines(user);
			LibsLogger.debug(DemoDataJob.class, "Demo Data created for user ", user);
		} catch (Exception e) {
			LibsLogger.error(DemoDataJob.class, "Demo data creation failed, consuming job", e);
		}
	}

	public UUID getUserId() {
		return userId;
	}

	public void setUserId(UUID userId) {
		this.userId = userId;
	}
}
