package net.beanscode.model.unittests.table;

import net.beanscode.model.tests.BeansTestCase;
import net.hypki.libs5.utils.LibsLogger;

import org.junit.Test;

import com.google.common.collect.Table;
import com.google.common.collect.TreeBasedTable;

public class GuavaTableTest extends BeansTestCase {

	@Test
	public void testGuavaTable() {
		Table table = TreeBasedTable.create();
		
		table.put(200.0, "tbid", "a");
		table.put(400.0, "a", "b");
		table.put(0.0, "tbid", "c");
		table.put(0.0, "tbid", "d");
		
		for (Object key : table.rowKeySet()) {
			LibsLogger.debug(GuavaTableTest.class, "Key: ", key);
		}
		
		for (Object col : table.columnKeySet()) {
			LibsLogger.debug(GuavaTableTest.class, "Column: ", col);
		}
		
		LibsLogger.debug(GuavaTableTest.class, "Cell: ", table.get(0.0, "tbid"));
	}
}
