package net.beanscode.web.view.dataset;

import static org.rendersnake.HtmlAttributesFactory.class_;

import java.io.IOException;
import java.util.List;

import net.beanscode.cli.BeansCLISettings;
import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.datasets.Table;
import net.beanscode.cli.datasets.TableSearchResults;
import net.beanscode.pojo.dataset.Dataset;
import net.beanscode.web.view.ui.BetaTableComponent;
import net.beanscode.web.view.ui.TabsBetaComponent;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.url.Params;

import org.rendersnake.HtmlCanvas;
import org.rendersnake.Renderable;

public class TablesBetaTableComponent extends BetaTableComponent {
	
	private TableSearchResults tableSearchResults = null;
	
	public TablesBetaTableComponent() {
		
	}
	
	public TablesBetaTableComponent(Component parent) {
		super(parent);
	}
	
	@Override
	protected String getSettingsPrefix() {
		return "tables-table-";
	}

	@Override
	protected void renderTitle(HtmlCanvas html) throws IOException {
		html
			.span()
				.content("Datasets and tables");
	}

	@Override
	protected boolean isFilterVisible() throws IOException {
		return true;
	}

	@Override
	protected boolean isHeaderColumnVisible() throws IOException {
		return true;
	}

	@Override
	protected int getNumberOfColumns() throws IOException {
		return 2;
	}

	@Override
	protected String getColumnName(int col) throws IOException {
		if (col == 0)
			return "Dataset";
		else if (col == 1)
			return "Tables";
		else
			return "";
	}
	
	@Override
	protected boolean isFilter2Visible() {
		return true;
	}
	
	@Override
	protected String getFilterPlaceholder() {
		return "Datasets...";
	}
	
	@Override
	protected String getFilter2Placeholder() {
		return "Tables...";
	}

	@Override
	protected long getMaxHits() throws IOException {
		return getTableSearchResults().getMaxHits();
	}

	@Override
	protected void renderCell(int row, int col, HtmlCanvas html)
			throws IOException {
		Table table = getTableSearchResults().getTables().get(row % getPerPage());
		Dataset ds = table.getDataset(getSessionBean());
		
		if (col == 0)
			html
				.a(class_("beans-link")
						.onClick(new OpComponentClick(DatasetBetaContent.class,
								UUID.random(),
								"onShow",
								new Params()
									.add("datasetId", table.getDatasetId().getId()))
								.toStringOnclick()))
					.content(ds != null ? ds.getName() : "")
				.br()
				.span(class_("beans-dataset-id"))
					.content(table.getDatasetId().toString(" - "));
		else if (col == 1)
			html
				.span()
					.content(table.getName())
				.br()
				.span(class_("beans-table-id"))
					.content(table.getId().toString(" - "));
		else {
			
		}
	}

	private TableSearchResults getTableSearchResults() {
		if (tableSearchResults == null) {
			try {
				tableSearchResults = new ApiCallCommand(this)
										.setApi("api/table/search")
										.addParam("datasetQuery", getFilter())
										.addParam("tableQuery", getFilter2())
										.addParam("from", getPage() * getPerPage())
										.addParam("size", getPerPage())
										.run()
										.getResponse()
										.getAsObject(TableSearchResults.class);
				
				BeansCLISettings.set("tableDsSearch", getFilter());
				BeansCLISettings.set("tableTbSearch", getFilter2());
			} catch (CLIException | IOException e) {
				LibsLogger.error(TablesBetaTableComponent.class, "Cannot search for Datasets", e);
				tableSearchResults = new TableSearchResults();
			}
		}
		return tableSearchResults;
	}
	
	@Override
	protected List<Button> getMenu() throws IOException {
		return null;
	}

	@Override
	protected boolean isExapandable() throws IOException {
		return true;
	}

	@Override
	protected void renderExpandable(int row, HtmlCanvas html) throws IOException {
		Table table = getTableSearchResults().getTables().get(row % getPerPage());
		
		html
			.render(new TabsBetaComponent(this) {
				
				@Override
				public int getTabsCount() {
					return 3;
				}
				
				@Override
				public String getTabName(int tabIndex) {
					if (tabIndex == 0)
						return "Columns";
					else if (tabIndex == 1)
						return "Meta dataset";
					else if (tabIndex == 2)
						return "Meta table";
					else
						return "";
				}
				
				@Override
				public Renderable getTabHtml(int tabIndex) throws IOException {
					return new Renderable() {
						
						@Override
						public void renderOn(HtmlCanvas arg0) throws IOException {
							if (tabIndex == 0)
								html
									.render(new TableColumnsTableComponent(table));
							else if (tabIndex == 1)
								html
									.render(new MetaTableComponent(TablesBetaTableComponent.this, table.getDataset(getSessionBean())));
							else
								html
									.render(new MetaTableComponent(TablesBetaTableComponent.this, table));
						}
					};
				}
			});
	}

	@Override
	protected String getTableCss() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}
}
