package net.beanscode.model.notebook;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.ws.rs.core.StreamingOutput;

import com.google.gson.annotations.Expose;

import net.beanscode.model.BeansSettings;
import net.beanscode.model.autostart.IncreaseFieldsCountJob;
import net.beanscode.model.notebook.autoupdate.AutoUpdateSettings;
import net.hypki.libs5.db.db.weblibs.MutationList;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.string.Meta;
import net.hypki.libs5.utils.string.MetaList;
import net.hypki.libs5.weblibs.SearchManager;
import net.hypki.libs5.weblibs.db.Indexable;
import net.hypki.libs5.weblibs.db.WeblibsObject;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

public class NotebookEntry extends WeblibsObject<NotebookEntry> implements Indexable {
	
	public static final String COLUMN_FAMILY = "notebookentry";
	
	private static boolean nrOfFieldsIncreased = false;
	
	@Expose
	@NotNull
	@AssertValid
	private UUID id = null;

	@Expose
	@NotNull
	@AssertValid
	private UUID userId = null;
	
	@Expose
	@NotNull
	@AssertValid
	private UUID notebookId = null;
	
	@Expose
	@NotNull
	@NotEmpty
	private String type = null;

	@Expose
	private String name = null;

	@Expose
	@NotNull
	@AssertValid
	private SimpleDate create = null;
	
	@Expose
	@NotNull
	@AssertValid
	private SimpleDate lastEdit = null;
	
	@Expose
	@NotNull
	@AssertValid
	private AutoUpdateSettings autoUpdateOptions = null;
	
	@Expose
	@AssertValid
	private MetaList meta = null;

	// TODO obsolete, remove it
	@Expose
	@NotNull
	@AssertValid
	private NotebookEntryStatus status = NotebookEntryStatus.NEW;
	
	private boolean addToNotebookEntry = false;
	private boolean indexedSuccess = false;
	
	private Notebook notebookCache = null;
	
	static {
		JsonUtils.registerTypeAdapter(NotebookEntry.class, new NotebookEntrySerializer());
	}
	
	public NotebookEntry() {
		setId(UUID.random());
		setType(getClass().getName());
		setCreate(SimpleDate.now());
	}
	
	@Override
	public String getColumnFamily() {
		return COLUMN_FAMILY;
	}
	
	@Override
	public String getKey() {
		return getId().getId();
	}
	
	public String getShortDescription() {
		throw new RuntimeException("getShortDescription() not implemented for " + getClass().getSimpleName());
	}

	public String getSummary() {
		throw new RuntimeException("getSummary() not implemented for " + getClass().getSimpleName());
	}

	public boolean isReloadNeeded() {
		throw new RuntimeException("isReloadNeeded() not implemented for " + getClass().getSimpleName());
	}

	public ReloadPropagator reload() throws ValidationException, IOException {
		throw new RuntimeException("reload() not implemented for " + getClass().getSimpleName());
	}

	public String getEditorClass() {
		throw new RuntimeException("getEditorClass() not implemented for " + getClass().getSimpleName());
	}

	public void start() throws ValidationException, IOException {
		throw new RuntimeException("start() not implemented for " + getClass().getSimpleName());
	}

	public boolean isRunning() throws ValidationException, IOException {
		throw new RuntimeException("isRunning() not implemented for " + getClass().getSimpleName());
	}

	public void stop() throws ValidationException, IOException {
		throw new RuntimeException("stop() not implemented for " + getClass().getSimpleName());
	}
	
	public List<String> getSplits() {
		throw new RuntimeException("getSplits() not implemented for " + getClass().getSimpleName());
	}
	
	public InputStream getImageAsStream(String split) throws IOException {
		throw new RuntimeException("getImageAsStream() not implemented for " + getClass().getSimpleName());
	}
	
	public StreamingOutput getDataAsStream(String split, String type) throws IOException {
		throw new RuntimeException("getDataAsStream() not implemented for " + getClass().getSimpleName());
	}
	
	public List<String> getOutputColumns() throws IOException {
		throw new RuntimeException("getOutputColumns() not implemented for " + getClass().getSimpleName());
	}
	
	public NotebookEntry upload(InputStream stream, String name) {
		throw new RuntimeException("upload() not implemented for " + getClass().getSimpleName());
	}
	
	public MutationList getSaveMutations() {
		return super.getSaveMutations()
				.addInsertMutations(addToNotebookEntry ? new AddEntryToNotebookJob(this) : null)
				.addInsertMutations(indexedSuccess == false ? new IndexNotebookEntryJob(getId()) : null)
				.addInsertMutations(nrOfFieldsIncreased == false ? new IncreaseFieldsCountJob() : null)
				.addInsertMutations(NotebookEntryHistory.create(this))
				;
	};
	
	public NotebookEntry clone() {
		return JsonUtils.fromJson(getData(), this.getClass());
	}
	
	public NotebookEntry cloneAsNew(UUID newNotebookId) {
		NotebookEntry clonedEntry = this.clone();
		
		clonedEntry.setCreate(SimpleDate.now());
		clonedEntry.setId(UUID.random());
		clonedEntry.setLastEdit(SimpleDate.now());
		clonedEntry.setNotebookId(newNotebookId);
		
		return clonedEntry;
	}
	
	@Override
	public NotebookEntry save() throws ValidationException, IOException {
		setType(getClass().getName());
		
		if (getAutoUpdateOptions() == null)
			setAutoUpdateOptions(new AutoUpdateSettings().setOption(BeansSettings.getAutoUpdateEntryDefaultOption(getUserId())));
		
		setLastEdit(SimpleDate.now());
		
		if (getCreate() == null)
			setCreate(getLastEdit());
		
		try {
			index();
			indexedSuccess = true;
		} catch (IOException e) {
			LibsLogger.error(NotebookEntry.class, "Cannot index ad-hoc NotebookEntry", e);
			indexedSuccess = false;
		}
		
		super.save();
		
		NotebookEntryFactory.updateCache(this);
		
		nrOfFieldsIncreased = true;
		
		return this;
	}
	
	@Override
	public void remove() throws ValidationException, IOException {
		super.remove();
		
		LibsLogger.debug(NotebookEntry.class, "NotebookEntry ", getName(), " removed");
	}
	
	@Override
	public MutationList getRemoveMutations() {
		return super.getRemoveMutations()
				.addInsertMutations(new RemoveEntryFromNotebookJob(this));
	}
	
	@Override
	public void index() throws IOException {
		SearchManager.index(this);
	}
	
	@Override
	public void deindex() throws IOException {
		SearchManager.removeObject(getCombinedKey(), getColumnFamily());
	}
	
	public NotebookEntryProgress getProgress() throws IOException {
		NotebookEntryProgress p = NotebookEntryProgressFactory.getNotebookEntryProgress(getId());
		if (p == null)
			p = new NotebookEntryProgress(this);
		return p;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public UUID getNotebookId() {
		return notebookId;
	}

	public void setNotebookId(UUID notebookId) {
		if (this.notebookId == null && notebookId != null)
			addToNotebookEntry = true;
		else if (this.notebookId != null 
				&& notebookId != null
				&& this.notebookId.equals(notebookId) == false)
			addToNotebookEntry = true;

		this.notebookId = notebookId;
	}

	public UUID getUserId() {
		return userId;
	}

	public NotebookEntry setUserId(UUID userId) {
		this.userId = userId;
		return this;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public SimpleDate getCreate() {
		return create;
	}

	public void setCreate(SimpleDate create) {
		this.create = create;
	}

	public SimpleDate getLastEdit() {
		return lastEdit;
	}

	public void setLastEdit(SimpleDate lastEdit) {
		this.lastEdit = lastEdit;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public <T extends NotebookEntry> T getAs(Class<T> clazz) {
		return (T) this;
	}

	public Notebook getNotebook() throws IOException {
		if (notebookCache == null)
			notebookCache = Notebook.getNotebook(getNotebookId());
		return notebookCache;
	}
	
	public AutoUpdateSettings getAutoUpdateOptions() {
		return autoUpdateOptions;
	}
	
	public NotebookEntry setAutoUpdateOptions(AutoUpdateSettings autoUpdateOptions) {
		this.autoUpdateOptions = autoUpdateOptions;
		return this;
	}
	
	public void move(Notebook notebook) throws ValidationException, IOException {
		NotebookEntry clonedEntry = this.cloneAsNew(notebook.getId());
		clonedEntry.save();
		
		remove();
	}
	public MetaList getMeta() {
		if (meta == null)
			meta = new MetaList();
		return meta;
	}
	
	public <T extends NotebookEntry> T setMeta(MetaList meta) {
		this.meta = meta;
		return (T) this;
	}
	
	public <T extends NotebookEntry> T setMeta(String name, Object value) {
		getMeta().add(name, value);
		return (T) this;
	}
	
	public boolean getMetaAsBoolean(String metaName, boolean defaultValue) {
		Meta m = getMeta().get(metaName);
		if (m != null && m.getValue() != null)
			return m.getAsBoolean();
		return defaultValue;
	}
	
	public Double getMetaAsDouble(String metaName, Double defaultValue) {
		Meta m = getMeta().get(metaName);
		if (m != null && m.getValue() != null)
			return m.getAsDouble();
		return defaultValue;
	}
	
	public String getMetaAsString(String metaName) {
		return getMetaAsString(metaName, null);
	}
	
	public String getMetaAsString(String metaName, String defaultValue) {
		Meta m = getMeta().get(metaName);
		if (m != null && m.getValue() != null)
			return m.getAsString();
		return defaultValue;
	}
	
	public UUID getMetaAsUUID(String metaName, UUID defaultValue) {
		Meta m = getMeta().get(metaName);
		if (m != null)
			return notEmpty(m.getAsString()) ? new UUID(m.getAsString()) : defaultValue;
		return defaultValue;
	}
	
	public <T> T getMetaAsObject(String metaName, Class<T> clazz) {
		return getMeta().getAsObject(metaName, clazz);
	}
	
	/**
	 * Finished == FAIL or SUCCESS
	 * @return
	 */
	public boolean isFinished() {
		return getStatus() == NotebookEntryStatus.FAIL
				|| getStatus() == NotebookEntryStatus.SUCCESS;
	}
	
	public NotebookEntryStatus getStatus() {
		return status;
	}
	
	public NotebookEntry setStatus(NotebookEntryStatus status) {
		this.status = status;
		return this;
	}
	
//	public static void main(String[] args) {
//		String neJSon = FileUtils.readString(new File("/home/ahypki/temp/ne"));
//		GnuplotEntry ge = JsonUtils.fromJson(JsonUtils.parseJson(neJSon)
//					.getAsJsonObject().get("columns").getAsJsonObject().get("data").getAsString(), 
//				GnuplotEntry.class);
//		LibsLogger.info(NotebookEntry.class, "ge", ge.getMetaAsString("script", null));
//	}
}
