package net.beanscode.rest.notebook;

import java.io.IOException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import mjson.Json;
import net.beanscode.model.notebook.NotebookEntryHistory;
import net.beanscode.model.notebook.NotebookEntryHistoryFactory;
import net.beanscode.rest.BeansRest;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.search.SearchResults;
import net.hypki.libs5.utils.json.JsonUtils;


@Path("api/notebook/entry/history")
public class NotebookEntryHistoryRest extends BeansRest {

	@GET
	@Path("get")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@Context SecurityContext sc, 
			@QueryParam("entryId") UUID entryId) throws IOException, ValidationException {
		final NotebookEntryHistory ne = NotebookEntryHistoryFactory.getNotebookEntryHistory(entryId);
		
		validateNotFound(ne != null, "Notebook Entry is not found");
		
//		final Notebook notebook = ne.getNotebook();
//		
//		validateNotFound(notebook != null, "Notebook is not found");
//		validateUnauthorized(isAdmin(sc) || notebook.isReadAllowed(getUserId(sc)), "Permission denied");
		
//		Json n = Json.read(ne.getData());
				
//		if (ne.getClass().getSimpleName().equals("TextEntry"))
//			n.set("textHtml", ne.getMetaAsString(net.beanscode.cli.notebooks.TextEntry.META_TEXT_HTML, ""));
		
//		n.set("notebookName", ne.getNotebook().getName());
		
		return Response
				.ok()
				.entity(ne.getData())
				.build();
	}
	
	@GET
	@Path("search")
	@Produces(MediaType.APPLICATION_JSON)
	public Response search(@Context SecurityContext sc, 
			@QueryParam("entryId") UUID entryId,
			@QueryParam("from") int from,
			@QueryParam("size") int size) throws IOException, ValidationException {
		
		if (size <= 0)
			size = 20;
		
		SearchResults<NotebookEntryHistory> s = NotebookEntryHistoryFactory
											.searchNotebookEntryHistory(entryId,
													from, 
													size);
				
		return Response
				.ok()
				.entity(JsonUtils.objectToString(s))
				.build();
	}
}
