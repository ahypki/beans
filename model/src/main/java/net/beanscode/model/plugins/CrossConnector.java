package net.beanscode.model.plugins;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.NotImplementedException;

import net.hypki.libs5.db.db.Results;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.search.query.Query;

public class CrossConnector implements Connector {
	
	private ColumnDefList defs = null;
	private List<RewindableConnector> connectors = new ArrayList<>();
	private List<Iterator<Row>> connIter = new ArrayList<>();
	private List<Row> connRows = new ArrayList<>();
	private List<String> connAliasNames = new ArrayList<>();
	
	public CrossConnector() {
		
	}
	
	public CrossConnector(List<RewindableConnector> connectors, List<String> connAliasNames) {
		setConnectors(connectors);
		this.connAliasNames = connAliasNames;
	}

	public List<RewindableConnector> getConnectors() {
		return connectors;
	}

	public void setConnectors(List<RewindableConnector> connectors) {
		this.connectors = connectors;
	}

	@Override
	public String getName() {
		return "CROSS connector";
	}

	@Override
	public ConnectorInitParams getInitParams() {
		return null;
	}

	@Override
	public Connector setInitParams(ConnectorInitParams initParams) {
		return null;
	}

	@Override
	public Iterable<Row> iterateRows(Query filter) {
		return new Iterable<Row>() {
			
			@Override
			public Iterator<Row> iterator() {
				return new Iterator<Row>() {
					private boolean initiatied = false;
					private Row row = null;
					
					@Override
					public Row next() {
						if (!initiatied)
							hasNext();
						return row;
					}
					
					private void buildRow() {
						// building row
						row = new Row();
						for (int i = 0; i < connRows.size(); i++) {
							Row r = connRows.get(i);
							
							if (r == null) {
								row = null;
								break;
							}
							
							for (ColumnDef col : getConnectors().get(i).getColumnDefs()) {
								row.addColumn(connAliasNames.get(i) + "::" + col.getName(), r.get(col.getName()));
							}
						}
					}
					
					@Override
					public boolean hasNext() {
						row = null;
						
						if (!initiatied) {
							initiatied = true;
							
							int i = 0;
							for (RewindableConnector c : getConnectors()) {
								connIter.add(c.iterateRows(null).iterator());
								connRows.add(connIter.get(i).hasNext() ? connIter.get(i).next() : null);
								i++;
							}
							
							boolean buildRow = true;
							for (int j = 0; j < getConnectors().size(); j++) {
								if (connRows.get(j) == null) {
									buildRow = false;
									break;
								}
							}
							
							if (buildRow) {
								buildRow();
								return true;
							} else {
								return false;
							}
						}
						
						// reading the next CROSSed row
						while (true) {
							// moving to the next row(s) - starting from the most right one
							for (int i = getConnectors().size() - 1; i >= 0; i--) {
								connRows.set(i, connIter.get(i).hasNext() ? connIter.get(i).next() : null);
								
								if (connRows.get(i) == null) {
									// going to the left-er iterator
									continue;
								}
								
								break;
							}
							
							// reopening iterators if needed (except the first one)
							for (int j = 1; j < getConnectors().size(); j++) {
								if (connIter.get(j) == null) {
									connIter.set(j, getConnectors().get(j).iterateRows(null).iterator());
								}
							}
							
							// if there is no row to the most left iterator - stop
							if (connRows.get(0) == null) {
								return false;
							}
							
							// go to next rows for other iterators
//							for (int j = 1; j < getConnectors().size(); j++) {
//								if (connRows.get(j) == null) {
//									connRows.set(j, connIter.get(j).next());
//								}
//							}
							
							// check if all rows are available to build CROSS row
							boolean nullPresent = false;
							for (int j = 1; j < getConnectors().size(); j++)
								if (connRows.get(j) == null)
									nullPresent = true;
							if (nullPresent)
								break;
							else {
								buildRow();
								break;
							}
						}
						
						return row != null;
					}
				};
			}
		};
	}

	@Override
	public Iterable<Row> iterateRows(Query filter, int splitNr, int splitCount) {
		if (splitNr == 0)
			return iterateRows(filter);
		else
			return new ArrayList<>();
	}

	@Override
	public Results iterateRows(Query filter, String state, int size) {
		throw new NotImplementedException();
	}

	@Override
	public void write(Row row) throws IOException {
		throw new NotImplementedException();
	}

	@Override
	public void close() throws IOException {
		
	}

	@Override
	public long getAproxBytesSize() throws IOException {
		long b = 0;
		for (Connector c : getConnectors())
			b += c.getAproxBytesSize();
		return b;
	}

	@Override
	public void clear() throws IOException {
		throw new NotImplementedException();
	}

	@Override
	public void setColumnDefs(ColumnDefList defs) {
		this.defs = defs;
	}

	@Override
	public ColumnDefList getColumnDefs() {
		return this.defs;
	}

	@Override
	public void validate() throws IOException {
		throw new NotImplementedException();
	}

	@Override
	public String getVersion() {
		return "0.1.0";
	}

}
