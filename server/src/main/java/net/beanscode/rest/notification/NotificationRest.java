package net.beanscode.rest.notification;

import java.io.IOException;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import mjson.Json;
import net.beanscode.model.cass.notification.Notification;
import net.beanscode.model.cass.notification.NotificationFactory;
import net.beanscode.rest.BeansRest;
import net.beanscode.rest.dataset.DatasetRest;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.search.SearchResults;
import net.hypki.libs5.search.query.BooleanTerm;
import net.hypki.libs5.search.query.TermRequirement;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.url.HttpResponse;

@Path("api/notification")
public class NotificationRest extends BeansRest {

	@GET
	@Path("search")
	@Produces(MediaType.APPLICATION_JSON)
	public Response search(@Context SecurityContext sc, 
			@QueryParam("from") int from,
			@QueryParam("size") int size,
			@QueryParam("filter") String filter,
			@QueryParam("type") String type) throws IOException, ValidationException {
		try {
			validateNotFound(getUserId(sc) != null, "Permission denied");
			
			SearchResults<Notification> res = NotificationFactory
					.searchNotifications(getUserId(sc), from, size, false, filter, type);
			
			return Response
					.ok()
					.entity(Json
							.object()
							.set("notifications", res.getObjectsAsJson())
							.set("maxHits", res.maxHits())
							.toString())
					.build();
		} catch (Exception e) {
			LibsLogger.error(DatasetRest.class, "Cannot search for datasets", e);
			return Response
					.serverError()
					.build();
		}
	}
	
	@GET
	@Path("get")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@Context SecurityContext sc, 
			@QueryParam("id") String id) throws IOException, ValidationException {
		Notification n = NotificationFactory.getNotification(id);
		
		if (n == null)
			return Response
					.noContent()
					.build();
		
		validateNotFound(n != null, "Notification is not found");
//		validateUnauthorized(isAdmin(sc) || n.getUserId().equals(getUserId(sc)), "Permission denied");
		
		return Response
				.ok()
				.entity(n.getData())
				.build();
	}
	
	@POST
	@Path("answer")
	public void answer(@Context SecurityContext sc, 
			@FormParam("notificationId") String notificationId,
			@FormParam("questionId") String questionId,
			@FormParam("answer") String answer) throws IOException, ValidationException {
		Notification n = NotificationFactory.getNotification(notificationId);
		
		validateNotFound(n != null, "Notification is not found");
		validateUnauthorized(isAdmin(sc) || n.getUserId().equals(getUserId(sc)), "Permission denied");
		
		n.questionAnswered(questionId, answer);
	}
	
	@POST
	@Path("archive")
	public void archive(@Context SecurityContext sc, 
			@FormParam("notificationId") String notificationId,
			@FormParam("filter") String filter) throws IOException, ValidationException {
		if (notificationId != null) {
			Notification n = NotificationFactory.getNotification(notificationId);
			
			validateNotFound(n != null, "Notification is not found");
			validateUnauthorized(isAdmin(sc) || n.getUserId().equals(getUserId(sc)), "Permission denied");
			
			n.setArchived(true);
			n.save();
		} else {
			for (Notification n : NotificationFactory.iterateNotifications(getUserId(sc), false, filter)) {
				n.setArchived(true);
				n.save();
			}
		}
	}
	
	@POST
	@Path("read")
	public void read(@Context SecurityContext sc, 
			@FormParam("notificationId") String notificationId) throws IOException, ValidationException {
		Notification n = NotificationFactory.getNotification(notificationId);
		
		validateNotFound(n != null, "Notification is not found");
		validateUnauthorized(isAdmin(sc) || n.getUserId().equals(getUserId(sc)), "Permission denied");
		
		n.setRead(!n.isRead());
		n.save();
	}
}
