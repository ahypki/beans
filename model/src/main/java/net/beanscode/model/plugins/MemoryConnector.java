package net.beanscode.model.plugins;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import net.hypki.libs5.db.db.Results;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.search.query.Query;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.Meta;

public class MemoryConnector implements Connector {
	
	private ColumnDefList defs = null;
	private List<Row> rows = new ArrayList<>();
	
	public MemoryConnector() {
		
	}
	
	public MemoryConnector(ColumnDefList defs) {
		setColumnDefs(defs);
	}

	@Override
	public String getName() {
		return getClass().getSimpleName();
	}

	@Override
	public ConnectorInitParams getInitParams() {
		ConnectorInitParams ini = new ConnectorInitParams();
		ini.getMeta().add(new Meta("rows", rows));
		return ini;
	}

	@Override
	public Connector setInitParams(ConnectorInitParams initParams) {
		this.rows = (List<Row>) initParams.getMeta().get("rows").getValue();
		return this;
	}

	@Override
	public Iterable<Row> iterateRows(Query filter) {
		if (filter == null)
			return rows;
		else
			return rows
				.stream()
				.filter(new Predicate<Row>() {
					@Override
					public boolean test(Row r) {
						return filter.isFulfilled(r);
					}
				})
				.collect(Collectors.toList());
	}

	@Override
	public Iterable<Row> iterateRows(Query filter, int splitNr, int splitCount) {
		if (splitNr == 0)
			return iterateRows(filter);
		else
			return new ArrayList<>();
	}

	@Override
	public Results iterateRows(Query filter, String state, int size) {
		if (state == null)
			return new Results().setRows(rows);
		else
			return new Results();
	}

	@Override
	public void write(Row row) throws IOException {
		rows.add(row);
	}

	@Override
	public void close() throws IOException {
//		rows.clear();
	}

	@Override
	public long getAproxBytesSize() throws IOException {
		return rows.size() > 0 ? 
				rows.size() * rows.get(0).getMemorySize() : 
				0;
	}

	@Override
	public void clear() throws IOException {
		rows.clear();
	}

	@Override
	public void setColumnDefs(ColumnDefList defs) {
		this.defs = defs;
	}

	@Override
	public ColumnDefList getColumnDefs() {
		return defs;
	}

	@Override
	public void validate() throws IOException {
		
	}

	@Override
	public String getVersion() {
		return "0.1.0";
	}

	public long getSize() {
		return rows.size();
	}

	public void sort(List<String> sortColumns) {
		try {
			rows.sort(new Comparator<Row>() {
				@Override
				public int compare(Row o1, Row o2) {
					return o1.compare(sortColumns, o2);
				}
			});
		} catch (Exception e) {
			LibsLogger.error(MemoryConnector.class, "Sorting failed", e);
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
